package br.com.growupge.util;

import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.growupge.dto.UsuarioDTO;

import com.google.gson.stream.JsonReader;

public class JsonReaderTest {

	public static void main(String[] args) throws IOException {
		String json = "{\"access_token\":\"CAAOPGjrgVVwBAFQaxlYwGAKdghn2C13ZCBDZAXrlxDwOIt72WsXsfAc2qyjsUd1DdKYMG7pfXZBZBtJr9tYv9Xm4Bx88RBcJxmvQY5dlcu2ZBalRwXwcGGXhp8oKV7SORn4hRMANF1POze1v2mfoecpyFO0BxtZAvTPsWDwXkfPpfdZAxZBo6TzvEtOpjvOhUjz2qJqIbUK4FZAppzZAEpfie1\",\"token_type\":\"bearer\",\"expires_in\":5178197}";

		UsuarioDTO usr = new UsuarioDTO();
		try (JsonReader reader = new JsonReader(new StringReader(json))) {
			reader.beginObject();
			while (reader.hasNext()) {
				String name = reader.nextName();
				if ("access_token".equals( name )) 
					usr.setNome( reader.nextString() );
				else if ("expires_in".equals( name )) {
					Date dt = new Date( reader.nextLong() ); 
					System.out.println( new SimpleDateFormat("dd/MM/yyyy").format(dt)  );
				} else
					reader.skipValue();
			}
			reader.endObject();
		}

	}

}
