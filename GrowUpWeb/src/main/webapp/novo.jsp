<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta charset="utf-8">
    <title>GrowUp Veículos 2008</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Felipe Pina">
   	<meta name="keywords" CONTENT="Laudo,Pesquisa veicular,Laudo preventivo,Veículos,Consulta de placa,Consulta de procedência,Informações sobre Veículos,Informações cadastrais de Veículos,Dados de Veículos"/>

    <link rel="shortcut icon" href="assets/logo.png">

    <meta http-equiv="cache-control" content="no-store, no-cache, must-revalidate, post-check=0, pre-check=0" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="expires" content="0" />	
    
<!--     <link href="css/login.ec.css" rel="stylesheet" type="text/css" /> -->
    <link href="css/bootstrap-social.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
	
    
    <!-- Scripts -->
    <script src="js/scripts.js"></script>
    
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
    
    <style type="text/css">
 		label.error {
 			font-weight: bold;
 			color: red;
 			padding: 2px 8px; 
 			margin-top: 2px;
 		} 
		
		.popover {
		 	color: red;
		 	background-color:#ffffff;
		}
		
		
	</style>
	
  </head>
  
  <body>

    <header>       	
    	
    	<jsp:include page="templates/novo_menu.jsp">
     		<jsp:param name="activeMenu" value="novoMnu"/>
     	</jsp:include> 
    	
    </header>
    
	<jsp:useBean id="user"  class="br.com.growupge.dto.UsuarioDTO" scope="page"/> 
         
    <div class="row">         
	    <div class="col-md-3 col-md-offset-4">             
		    <form id="novoForm" action="criarUsuario?action=criar" method="post" data-toggle="validator">
			     <c:set var="msgRetorno" value="${msgRetorno}"/>
			     
			     <c:if test="${fn:length(msgRetorno) > 0}">	
			       	  <div class="container alert ${msgClass}">
			       	   <button type="button" class="close" data-dismiss="alert">&times;</button>
			       	  	${msgRetorno}
					  </div>
				  </c:if>
		
		    	<div class="form-signin">
		        	  
		  			<div class="control-group">
					    <label class="control-label" for="inputEmail">Email *</label>
					    <div class="controls">
						     <!-- 
						      <fmt:setBundle basename="Labels" scope="session"/>
						      <fmt:message key="emailDica" var="emailDica" />
						      --> 
						      <input type="email" required  id="inputEmail" placeholder="Email" name="email"
						      		 maxlength="<%= user.getTamanhoMaximoEmail() %>" class="form-control"  
						      		 rel="popover"
						      		 />
								<%-- 		     		 
									data-content="Digite um email funcional!"
						      		 data-toggle="tooltip"
						      		 title="${emailDica}"
		 						--%>	
		 		    	</div>
					  </div>        
					  	  
		        	  <div class="control-group">
					    <label class="control-label" for="inputNome">Nome Completo *</label>
					    <div class="controls">
					      <input type="text" required  id="inputNome" placeholder="Nome completo" name="nome"
					      		 maxlength="100" class="form-control" title="Campo nome completo é obrigatório." />
					    </div>
					  </div>
					  
		<!-- 			  <div class="control-group"> -->
		<!-- 			    <label class="control-label" for="inputPassword">Senha *</label> -->
		<!-- 			    <div class="controls"> -->
		<!-- 			      <input type="password" id="inputPassword" placeholder="Senha"  width="350"> -->
		<!-- 			    </div> -->
		<!-- 			  </div> -->
					  
					 <div class="control-group">
						<label class="control-label" >Idioma *</label>
					 	
					 	<div class="controls" style="text-align:center;height:40px">
							<label>
								<input type="radio" name="idioma" id="radioBR" value="BR" checked/>
								<img src="assets/ico_br.png" class="img-thumbnail"/>
						  	</label>
							<label style="width:10px">
							</label>
							<label>
							  	<input type="radio" name="idioma" id="radioEN" value="EN"/>
							  	<img src="assets/ico_en.png" class="img-thumbnail"/>
						  	</label>
					 	</div>
					  </div>
		
					  <div class="control-group">
					    <div class="controls">
					      	<button type="submit" class="btn btn-primary btn-block">Criar usuário</button>
					      
							<div class="tlg_login_divider">
					        	<div class="divider_text">ou</div>
					        </div>
		<!-- href="/GrowUpWeb/connectFB" -->
					      	<a class="btn btn-block btn-social btn-facebook" onclick="Authentication.loginFB()"  id="fb_button" type="button" >
				            	<i class="fa fa-facebook">
				            	</i>
		                		Login com Facebook
							</a>
					    </div>
					  </div>
					
			    </div><!--/.fluid-container-->
			</form>
		</div>
		
		<div class="col-md-4">
<!-- 			<iframe width="300" height="300"  style="float:right;right:0px;margin-right:50px;margin-top:15px;position:absolute;" -->
			<iframe width="100%" height="300" src="//www.youtube.com/embed/nMwoJeSy7Kk" frameborder="0" allowfullscreen="true">
			</iframe>    	
		</div>
	</div>
	
    <hr>
	
	<div class="row">         
		<!-- Adsense -->	
		<div style="text-align:center">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- cadastrarUsuario -->
			<ins class="adsbygoogle"
			     style="display:inline-block;width:728px;height:90px"
			     data-ad-client="ca-pub-4752317797638493"
			     data-ad-slot="5322176160"></ins>
			<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
	</div>


	<div class="row">     
		<div class="container">
		    <footer>       	
		    	<jsp:include page="rodape.jsp">
		    		<jsp:param name="lines" value="2"/>
		    		<jsp:param name="words" value="5"/>
		    	</jsp:include> 
		    </footer>
	    </div>
    </div>
    
<!-- 	<script src="js/jquery-1.7.1.min.js"></script>  -->
<!-- 	<script src="js/jquery-2.1.1.js"></script>  -->
<!-- 	<script src="js/jquery.validate.js"></script> -->
	 
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<!--     <script src="./fluid_files/jquery.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-transition.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-alert.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-modal.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-dropdown.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-scrollspy.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-tab.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-tooltip.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-popover.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-button.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-collapse.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-carousel.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-typeahead.js"></script> -->

  	<script type="text/javascript">
  	//<![CDATA[
  		$(document).ready(function() {
		  	/*
			$('#inputEmail').tooltip({
			    trigger: 'focus'
			});
		  	*/
			    var showPopover = function () {
			        $(this).popover('show');
			    };
			    
			    var hidePopover = function () {
			        $(this).popover('hide');
			    };
			    
			    $('[rel="popover"]').popover({
			        trigger: 'focus',
			        placement: 'right',
			        title:'',
			        content:'Favor digitar um email funcional!'
			    })
			    .focus(showPopover)
			    .blur(hidePopover)
			    ;

	 			$('#novoForm').validate({
				rules: {	
					
					nome: {
				        required: true
				      },
					
				      codigo: {
				        required: true
				      },
					
				      email: {
				        required: true,
				        email: true
				      },
				      
			    },
			    
			     // Specify the validation error messages
			    messages: {
            		email: "Digite um email funcional!",
            		nome: "Campo nome é obrigatório.",
            		codigo: "Campo usuário é obrigatório."
            	}
				
			  }); 


  		 }); //END $(document).ready()
  	//]]>
  	</script>
  	
	<!-- Google Analytics -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-50218825-1', 'auto');
	  ga('send', 'pageview');
	
	</script>  	

</body></html>