<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=utf-8" %>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Felipe Pina">
  <meta name="keywords" CONTENT="Laudo,Pesquisa veicular,Laudo preventivo,Veículos,Consulta de placa,Consulta de procedência,Informações sobre Veículos,Informações cadastrais de Veículos,Dados de Veículos"/>
  <link rel="shortcut icon" href="assets/logo_site.png">

  <title>Grow Up Veículos</title>

</head>


<body>
  
  <header>       	
    	
    	<jsp:include page="templates/novo_menu.jsp">
     		<jsp:param name="activeMenu" value="duvidaMnu"/>
     	</jsp:include> 
    	
    </header>
  

	<div class="panel-group" id="accordion">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> 
						Prazo de entrega 
					</a><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i>
				</h4>
			</div>
			<div id="collapseOne" class="panel-collapse collapse">
				<div class="panel-body">
					<img src="assets/valid.png" alt="" class="img-rounded"/>
					O prazo é de 24 horas a partir da data da comprovação do pagamento, tanto online ou pelo boleto.
				</div>
			</div>

			<div class="panel-heading">
				<h4 class="panel-title">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseO2"> 
						Em que consiste o serviço ? 
					</a><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i>
				</h4>
			</div>
			<div id="collapseO2" class="panel-collapse collapse">
				<div class="panel-body">
					<img src="assets/valid.png" alt="" class="img-rounded"/>
					O serviço consiste em três linhas:
					<ul >
						<li>Pesquisa dos dados do veículo:
							Listar todos os  débitos, bloqueios, restrições pendências tributárias entre outros descrito na página inicial.
						</li>
						<li>Análise da pesquisa:
							Após 20 anos de experiência, a Grow Up Veículos consegue diagnosticar problemas, que nem sempre descritos 
							nos dados. Com isso é iniciado uma pesquisa mais refinada, para descobrir a real causa que o veículo possui.
						</li>
						<li>Solução para o cliente:
							A empresa fornece os devidos passos para o cliente os resolver os problemas mais diversos, como <b>por exemplo</b> 
							documentação; é fornecido o endereço do local e o processo que o cliente deve seguir para adquirir ou 
							retirar uma segunda via do documento. Existem várias alternativas de solução para problemas diferentes.
							Basta conferir realizando uma pesquisa conosco.
						</li>
					</ul>
				</div>
			</div>

			<div class="panel-heading">
				<h4 class="panel-title">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse03"> 
						Formas de pagamento 
					</a><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i>
				</h4>
			</div>
			<div id="collapse03" class="panel-collapse collapse">
				<div class="panel-body">
					<img src="assets/valid.png" alt="" class="img-rounded"/>
					No momento existem 3 formas:
					<ul>
						<li><b>Boleto bancário:</b>
							O usuário preenche os dados em um formulário, somente para finalidade de impressão do boleto, esses dados para o boleto, 
							<b style="color:red">não são gravados no sistema.</b>
							O sistema vai abrir uma nova aba no navegador, e exibir o site do banco para impressão do boleto. 
						</li> 
						<li><b>Paypal:</b>
							Esta forma de pagamento, consegue receber online a confirmação de pagamento, logo o seu pedido terá um prazo de conclusão antecipado, 
							pelo fato da confirmação de pagamento ser confirmada após o pagamento da pesquisa.
						</li>
							
						<li><b>Transferência bancária:</b>
							O usuário poderá realizar a transferência do valor da pesquisa, através do site do Itaú, identificando o pagamento
							pelo número do pedido, que foi enviado para o email do usuário.
						</li>
					</ul>
				</div>
			</div>

			<div class="panel-heading">
				<h4 class="panel-title">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse04"> 
						Tipos de pesquisa 
					</a><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i>
				</h4>
			</div>
			<div id="collapse04" class="panel-collapse collapse">
				<div class="panel-body">
					 Pesquisas preventivas completas de veículos usados, fornecendo dados, análises e solução para o problema, 
					 caso o veículo possua.
				</div>
			</div>

			<div class="panel-heading">
				<h4 class="panel-title">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse05"> 
						Como recebo a consulta? 
					</a><i class="indicator glyphicon glyphicon-chevron-down  pull-right"></i>
				</h4>
			</div>
			<div id="collapse05" class="panel-collapse collapse">
				<div class="panel-body">
					<img src="assets/valid.png" alt="" class="img-rounded"/>
					A consulta é enviada para o email do usuário, conforme este <b><a style="color:red" href="modelo.jsf">modelo de exemplo</a></b>.
				</div>
			</div>

		</div>
	</div>

	<script type="text/javascript">

		function toggleChevron(e) {
			$(e.target)
				.prev('.panel-heading')
				.find("i.indicator")
				.toggleClass('glyphicon-chevron-down glyphicon-chevron-up')
				;
		
			$(e.target)
				.prev()
				.toggleClass('panel-heading panel-heading > panel-active')
				;
		}
		
		
		$('#accordion').on('hidden.bs.collapse', toggleChevron);
		$('#accordion').on('shown.bs.collapse', toggleChevron);

	</script>

</body>
</html>