<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Felipe Pina">
   	<meta name="keywords" CONTENT="Laudo,Pesquisa veicular,Laudo preventivo,Veículos,Consulta de placa,Consulta de procedência,Informações sobre Veículos,Informações cadastrais de Veículos,Dados de Veículos"/>
	<!-- Fav and touch icons -->    
    <link rel="shortcut icon" href="assets/logo_site.png">

    <title>GrowUp Veículos 2008</title>
    
    <!-- Le styles -->
<!--     <link href="http://getbootstrap.com/2.3.2/assets/css/bootstrap.css" rel="stylesheet"> -->
      <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
     <!-- Loading Flat UI -->
    <link href="css/flat-ui.css" rel="stylesheet">    
    
    <link href="css/slabtext.css" rel="stylesheet" type="text/css" />

      <!-- Estilos Gerais -->
    <link href="css/estilos.css" rel="stylesheet"/>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="cache-control" content="no-store, no-cache, must-revalidate, post-check=0, pre-check=0" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="expires" content="0" />	
    
  </head>

  <body>

    <header>       	
    	
    	<jsp:include page="templates/novo_menu.jsp">
     		<jsp:param name="activeMenu" value="conectarMnu"/>
     	</jsp:include> 
    	
    </header>
	
<!-- 	<div class="espacamento_topo">
	</div> -->
				    
    <form  role="form" action="conectar" method="post" >
		<c:set var="msgRetorno" value="${msgRetorno}"/>
		   	  
		  <c:if test="${fn:length(msgRetorno) > 0}">	
		    <div class="container alert ${msgClass}">
		    <button type="button" class="close" data-dismiss="alert">&times;</button>
		     	  	${msgRetorno}
		  	</div>
		</c:if>

		<div class="form-signin">
        	 <div class="control-group">
			    <label class="control-label" for="inputUsuario">Usuário *</label>
			      <input type="text" required autofocus id="inputUsuario" placeholder="Login" name="codigo" 
			      	maxlength="20"  class="form-control" onkeyup="this.value = this.value.toUpperCase();" /> 
			  </div>
        	  
			  <div class="control-group">
			    <label class="control-label" for="inputPassword">Senha *</label>
			      <input type="password" required autofocus id="inputPwd" placeholder="Senha"  name="senha"
			      		class="form-control">
			  </div>

			  <div style="height:15px">
			  </div>
			  
			  <div class="control-group">
			    <div class="controls">
			      <button type="submit" class="btn btn-primary btn-block">Conectar</button>
			    </div>
			  </div>
			  
	 </div> <!-- /.form-signin  -->
	</form>

     <hr>

	<div class="container">
	    <footer>       	
	    	<jsp:include page="rodape.jsp">
	    		<jsp:param name="lines" value="2"/>
	    		<jsp:param name="words" value="5"/>
	    	</jsp:include> 
	    </footer>
    </div>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./fluid_files/jquery.js"></script>
  

</body></html>