<%@ page isErrorPage="true" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html>

<html>
<head>
	<title>Página de erro</title>
</head>

<body>
	<div style="border:solid; width:100%;">
		<h2 style="color:red;">
			Erro:
			<%= exception.toString() %>
		</h2>
	</div>

</body>
</html>
