<!DOCTYPE html>
<%-- 
<%@page import="br.com.growupge.facade.FacadeFactory"%>
<%@ page import="br.com.growupge.facade.GrowUpFacade" %>
 --%>
 
<%@page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  

<html lang="en">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>GrowUp Veículos 2008</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Felipe Pina">
   	<meta name="keywords" CONTENT="Laudo,Pesquisa veicular,Laudo preventivo,Veículos,Consulta de placa,Consulta de procedência,Informações sobre Veículos,Informações cadastrais de Veículos,Dados de Veículos"/>

   	<!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/logo.png">

 	<!-- Bootstrap core CSS 
    <link href="css/bootstrap.css" rel="stylesheet">
     
     Loading Flat UI 
    <link href="css/flat-ui.css" rel="stylesheet">    
    
    <link href="css/slabtext.css" rel="stylesheet" type="text/css" />
	-->

      <!-- Estilos Gerais 
    <link href="css/estilos.css" rel="stylesheet">
    -->
    
    <meta http-equiv="cache-control" content="no-store, no-cache, must-revalidate, post-check=0, pre-check=0" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="expires" content="0" />	
    
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

  </head>

  <body>
  
 	<header>       	
    	
    	<jsp:include page="../../templates/novo_menu.jsp">
     		<jsp:param name="activeMenu" value="tarifasMnu"/>
     	</jsp:include> 
    	
    </header>
  
    <!-- /.navbar
	<div class="espacamento_topo">
	</div> -->
	
    <div class="container">
<!--       <div class="row-fluid"> -->
<!--         <div class="span3"> -->
<!--           <div class="well sidebar-nav"> -->
<%--           	<%@ include file="menu.html" %> --%>
<!--           </div>/.well -->
<!--         </div>/span -->
        
        <table class="table">
        	<tr> 
        		<td  style="width:800px">
		        	<div >
		        	<ul>
		        		<li><b>Transferência bancária identificada pelo Número Interno do Pedido ( NIP ).</b>
			        	<dl class="dl-horizontal">
			        		<dt>DADOS BANCÁRIOS:</dt>
			        		<dd></dd>
			
			        		<dt>Banco:</dt>
			        		<dd>ITAU</dd>
			
			        		<dt>Agência:</dt>
			        		<dd>5658</dd>
			
			        		<dt>C/C:</dt>
			        		<dd>21543-8</dd>
			
			        		<dt>Pessoa Jurídica:</dt>
			        		<dd>GROWUP 2008 CONS E LEG DE VEÍCULOS LTDA</dd>
			
			        		<dt>Valor Unitário:</dt>
			        		 
		<%-- 	        		
							<jsp:useBean id="bean" class="br.com.growupge.apresentacao.bean.TarifaBean" scope="session">
			        			<jsp:setProperty name="bean" property="session" value="<%=session%>"/>
			        			<jsp:setProperty name="bean" property="facade" value="<%=FacadeFactory.getFacade()%>"/>
			        		</jsp:useBean>
		--%>
		<%-- 					<jsp:useBean id="bean" class="br.com.growupge.apresentacao.bean.TarifaBean" scope="session"> --%>
		<%-- 	        			<jsp:setProperty name="bean" property="session" value="<%=session%>"/> --%>
		<%-- 	        			<jsp:setProperty name="bean" property="facade" value="<%=FacadeFactory.getFacade()%>"/> --%>
		<%-- 	        		</jsp:useBean> --%>
							
		<%-- 					<c:set var="bean" scope="session" value="${tarifaBean}"/> --%>
		<%-- 					<c:set var="bean.session" scope="session" value="<%=session%>"/> --%>
			        		
		<%-- 	        		<c:out value="${tarifaBean.valorUnitario}" /> --%>
			        		
			        		<dd><span class="label label-danger">R$ <fmt:formatNumber type="number"  
			        																	maxFractionDigits="2"
			        																	minFractionDigits="2" 
			        																	value="${valorTarifa}"/>
								</span>
							</dd>
							
						</dl>
						</li>
						
						<li><b>Pagamento seguro com paypal</b>
							<!-- PayPal Logo -->
							<table border="0" cellpadding="10" cellspacing="0" align="text-left">
								<tr>
									<td align="center">
										<a href="#" onclick="javascript:window.open('https://www.paypal.com/br/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside','olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=350');"><img  src="https://www.paypalobjects.com/en_US/BR/i/bnr/bnr_horizontal_solution_PP_166wx80h.gif" border="0" alt="Imagens de solução"></a>
									</td>
									<td>
										<span class="label label-danger">R$ <fmt:formatNumber type="number"  
				        																	maxFractionDigits="2"
				        																	minFractionDigits="2" 
				        																	value="${valorTarifa}"/>
										</span>
									</td>
								</tr>
							</table><!-- PayPal Logo -->
						</li>
						
						<li><b>Boleto bancário</b>
							<table border="0" cellpadding="10" cellspacing="0" align="text-left">
							<tr>
								<td align="center">
									<img alt="Boleto banco Itaú" src="assets/barcode.png"/>
								</td>
								<td>
									<span class="label label-danger">R$ <fmt:formatNumber type="number"  
			        																	maxFractionDigits="2"
			        																	minFractionDigits="2" 
			        																	value="${valorTarifa}"/>
									</span>
								</td>
							</tr>
							</table>
						</li>
					</ul>
					
					<dl>
						<dt>Conteúdo da pesquisa:</dt>
						<dd><ol>
		  					<li>Dívidas ativas de IPVA com informações imediatas e atualizadas;</li>
		  					<li>Dados do veículo, débitos, multas e IPVA;</li>
		  					<li>Bloqueios administrativos, criminais e judiciais;</li>
		  					<li>Ocorrências criminais históricas e ativas;</li>
		  					<li>Ocorrências de leilões e sinistros existentes contra veículos usados, em âmbitos estadual e nacional;</li>
						</ol></dd>
					</dl>
					
					
		        </div><!--/span-->
	        </td>
	        
	        <td style="width: 300px;height: 600px">
	        	<!-- Adsense -->
	       		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<!-- tarifasBloco -->
				<ins class="adsbygoogle"
				     style="display:inline-block;width:300px;height:600px"
				     data-ad-client="ca-pub-4752317797638493"
				     data-ad-slot="9193972569"></ins>
				<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
				</script>
	        </td>
	 	</tr>
     </table>
<!--       </div>/row -->


    </div><!--/.fluid-container-->

    <hr>

	<div class="container">
    	<footer>       	
	    	<jsp:include page="../../rodape.jsp">
	    		<jsp:param name="lines" value="2"/>
	    		<jsp:param name="words" value="5"/>
	    	</jsp:include> 
    	</footer>
    </div>
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<!--     <script src="./fluid_files/jquery.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-transition.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-alert.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-modal.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-dropdown.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-scrollspy.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-tab.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-tooltip.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-popover.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-button.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-collapse.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-carousel.js"></script> -->
<!--     <script src="./fluid_files/bootstrap-typeahead.js"></script> -->
    

</body></html>