<!DOCTYPE html>
<!-- saved from url=(0049)veiculos.html -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=utf-8" %>
<html lang="en">
	<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta charset="utf-8">
    <title>GrowUp Veículos 2008</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="http://getbootstrap.com/2.3.2/assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="http://getbootstrap.com/2.3.2/assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="assets/logo.png">
  </head>

  <body>
<!-- 
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="veiculos.html#">Project name</a>
          <div class="nav-collapse collapse">
            <p class="navbar-text pull-right">
              Logged in as <a href="veiculos.html#" class="navbar-link">Username</a>
            </p>
            <ul class="nav">
              <li class="active"><a href="veiculos.html#">Home</a></li>
              <li><a href="veiculos.html#about">About</a></li>
              <li><a href="veiculos.html#contact">Contact</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
 -->
    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <div class="well sidebar-nav">
          		<%@ include file="menu.html" %>
<!--             <ul class="nav nav-list"> -->
<!--               <li class="nav-header">Menu</li> -->
<!--               <li><a href="conectar.jsp">Conectar</a></li> -->
<!--               <li><a href="novo.jsp">Novo usuário</a></li> -->
<!--               <li><a href="tarifas.jsp">Tarifas</a></li> -->
<!--               <li><a href="faleconosco.jsp">Fale conosco</a></li> -->
<!--             </ul> -->
          </div><!--/.well -->
        </div><!--/span-->
        
        <div class="span9">
          <div class="hero-unit">
            <h1 class="text-center">Grow Up 2008</h1>
            <h2 class="text-center">Consultoria e Legalização de Veículos Ltda.</h2>
            <br><br>
            <p class="text-error">Garantimos nossas informações por até 30 dias.</p>
<!--             <p><a href="veiculos.html#" class="btn btn-primary btn-large">Learn more Â»</a></p> -->
          </div>
          <div class="row-fluid">
            <div class="span4">
              <h2>Dívidas Ativas</h2>
              <p>de IPVA com informações imediatas e atualizadas.</p>
<!--               <p><a class="btn" href="veiculos.html#">View details Â»</a></p> -->
            </div><!--/span-->
            <div class="span4">
              <h2>Dados do veículo</h2>
              <p>Débitos, multas e IPVA</p>
<!--               <p><a class="btn" href="veiculos.html#">View details Â»</a></p> -->
            </div><!--/span-->
            <div class="span4">
              <h2>Bloqueios</h2>
              <p>administrativos, criminais e judiciais</p>
<!--               <p><a class="btn" href="veiculos.html#">View details Â»</a></p> -->
            </div><!--/span-->
          </div><!--/row-->
          <div class="row-fluid">
            <div class="span4">
              <h2>Ocorrências criminais</h2>
              <p>históricas e ativas</p>
<!--               <p><a class="btn" href="veiculos.html#">View details Â»</a></p> -->
            </div><!--/span-->
            <div class="span4">
              <h2>Restrições</h2>
              <p>econômicas e judiciais</p>
<!--               <p><a class="btn" href="veiculos.html#">View details Â»</a></p> -->
            </div><!--/span-->
            <div class="span4">
              <h2>Ocorrências</h2>
              <p>de leilões e Sinistros existentes contra veículos usados, em âmbitos estadual e nacional.</p>
<!--               <p><a class="btn" href="veiculos.html#">View details Â»</a></p> -->
            </div><!--/span-->
          </div><!--/row-->
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>       	
      	<jsp:include page="rodape.jsp">
      		<jsp:param name="lines" value="2"/>
      		<jsp:param name="words" value="7"/>
      	</jsp:include> 
      </footer>

    </div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./fluid_files/jquery.js"></script>
    <script src="./fluid_files/bootstrap-transition.js"></script>
    <script src="./fluid_files/bootstrap-alert.js"></script>
    <script src="./fluid_files/bootstrap-modal.js"></script>
    <script src="./fluid_files/bootstrap-dropdown.js"></script>
    <script src="./fluid_files/bootstrap-scrollspy.js"></script>
    <script src="./fluid_files/bootstrap-tab.js"></script>
    <script src="./fluid_files/bootstrap-tooltip.js"></script>
    <script src="./fluid_files/bootstrap-popover.js"></script>
    <script src="./fluid_files/bootstrap-button.js"></script>
    <script src="./fluid_files/bootstrap-collapse.js"></script>
    <script src="./fluid_files/bootstrap-carousel.js"></script>
    <script src="./fluid_files/bootstrap-typeahead.js"></script>

  

</body></html>