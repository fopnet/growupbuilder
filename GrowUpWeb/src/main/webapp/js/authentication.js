/**
 * Authentication module
 *  
 */

var Authentication = (function() {
	
	function loginFB() {
		var uri = window.location.origin;
		if (uri.indexOf('localhost') == -1) {
			uri = uri.replace(":8080/GrowUpWeb", "");
		} else {
			uri = uri + "/GrowUpWeb";
		}
		uri = encodeURI(uri + '/facebookConnect');
		openWindow(uri);
	}
	
	function openWindow(uri) {
		var minWidth  = 490;
		var minHeight = 360;
	    var left = (screen.width/2- minWidth/2);
	    var top = (screen.height/2 - minHeight/2);
	    window.open(uri,"login connect", "height=360, width=490, top="+top+",left="+left+",status=yes,toolbar=no,menubar=no,location=no");
	}

	function closeWindow() {
		window.opener.location.reload();
	    this.window.close();
	}
	
	return {
		loginFB : loginFB

	} // fim da api publica
		
}) ();

