
var StringUtils = (function() {
	
	function setCaretPosition(ctrl, pos) {
	    if (ctrl.setSelectionRange) {
	        ctrl.focus();
	        ctrl.setSelectionRange(pos,pos);
	    }
	    else if (ctrl.createTextRange) {
	        var range = ctrl.createTextRange();
	        range.collapse(true);
	        range.moveEnd('character', pos);
	        range.moveStart('character', pos);
	        range.select();
	    }
	}
	
	// Thanks http://blog.vishalon.net/index.php/javascript-getting-and-setting-caret-position-in-textarea/
	function getCaretPosition(ctrl) {
	    var CaretPos = 0;    // IE Support
	    if (document.selection) {
	        ctrl.focus();
	        var Sel = document.selection.createRange();
	        Sel.moveStart('character', -ctrl.value.length);
	        CaretPos = Sel.text.length;
	    }
	    // Firefox support
	    else if (ctrl.selectionStart || ctrl.selectionStart == '0') {
	        CaretPos = ctrl.selectionStart;
	    }

	    return CaretPos;
	}
	
	return {
		permitirChars : function (evt, regexArray) {
			var event = evt || window.event;  // get event object
			var keycode = event.keyCode || event.which; // get key crossbrowser
			var regex = new RegExp(regexArray);
			
			return regex.test( String.fromCharCode (keycode) ) ;
		},
		
		toUpperUpToCaret: function (ctrl) {
			  // Remember original caret position
		    var caretPosition = getCaretPosition(ctrl);

		    // Uppercase-ize contents
		    ctrl.value = ctrl.value.toLocaleUpperCase();

		    // Reset caret position
		    // (we ignore selection length, as typing deselects anyway)
		    setCaretPosition(ctrl, caretPosition);	
		}


	} // fim da api publica
		
}) ();

var UIModule = (function() {
	
}) ();


function applyStyle(ctrl, style) {
	ctrl = document.getElementById(ctrl); 
	if ( ctrl != null /* && ctrl.hasOwnProperty('className')*/ )
		ctrl.setAttribute('class', style);//Funciona no FF e Chrome
		// className 
} 

function setFocus(id) {
	if (!id) return;
	
    var element = document.getElementById(id);  
    if (element && element.focus) {  
        element.focus();  
    }  
}  
