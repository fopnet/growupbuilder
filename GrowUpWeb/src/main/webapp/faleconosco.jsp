<!DOCTYPE html>
<%@page contentType="text/html; charset=utf-8" %>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>GrowUp Ve&iacute;culos 2008</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Felipe Pina">
   	<meta name="keywords" CONTENT="Laudo,Pesquisa veicular,Laudo preventivo,VeÃ­culos,Consulta de placa,Consulta de procedÃªncia,InformaÃ§Ãµes sobre VeÃ­culos,InformaÃ§Ãµes cadastrais de VeÃ­culos,Dados de VeÃ­culos"/>
	<!-- Fav and touch icons -->    
    <link rel="shortcut icon" href="assets/logo_site.png">
    
  </head>

  <body>
  
   <header>       	
    	
    	<jsp:include page="templates/novo_menu.jsp">
     		<jsp:param name="activeMenu" value="faleConoscoMnu"/>
     	</jsp:include> 
    	
    </header>

    <!-- /.navbar 
	<div class="espacamento_topo">
	</div>-->
	
    <div class="container">
        
        <div class="span10">
        	<dl class="dl-horizontal">
        		<dt>Fale conosco</dt>
        		<dd>Faça um consulta e adquira seu veículo usado sabendo o que está comprando.
            		<p class="text-danger">Nosso Banco de Dados é o mais abrangente e atualizado do mercado !</p>
            	</dd>
            	
            	<dt><strong>Email</strong>
            	<dd>
	        		<a href="mailto:growupjc@gmail.com">growupjc@gmail.com</a>
	        	</dd>
			</dl>
        </div><!--/span-->

    </div><!--/.fluid-container-->
    
    <hr>

	<div class="container">
	    <footer>       	
	    	<jsp:include page="rodape.jsp">
	    		<jsp:param name="lines" value="3"/>
	    		<jsp:param name="words" value="5"/>
	    	</jsp:include> 
	    </footer>
	</div>
	
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./fluid_files/jquery.js"></script>
    <script src="./fluid_files/bootstrap-transition.js"></script>
    <script src="./fluid_files/bootstrap-alert.js"></script>
    <script src="./fluid_files/bootstrap-modal.js"></script>
    <script src="./fluid_files/bootstrap-dropdown.js"></script>
    <script src="./fluid_files/bootstrap-scrollspy.js"></script>
    <script src="./fluid_files/bootstrap-tab.js"></script>
    <script src="./fluid_files/bootstrap-tooltip.js"></script>
    <script src="./fluid_files/bootstrap-popover.js"></script>
    <script src="./fluid_files/bootstrap-button.js"></script>
    <script src="./fluid_files/bootstrap-collapse.js"></script>
    <script src="./fluid_files/bootstrap-carousel.js"></script>
    <script src="./fluid_files/bootstrap-typeahead.js"></script>

  

</body></html>