<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<!-- <link href="http://getbootstrap.com/2.3.2/assets/css/bootstrap.css" rel="stylesheet"> -->
	<title>Grow Up Ve�culos</title>

    <!-- Bootstrap core CSS -->
<!--     <link href="css/bootstrap.css" rel="stylesheet"> -->
    
    <!-- Estilos Gerais -->
<!--     <link href="css/estilos.css" rel="stylesheet"> -->

<script type="text/javascript">

  	function init() {
  		var words = ["Laudo",
		             "Pesquisa veicular",
		             "Laudo preventivo",
		             "Ve�culos",
		             "Consulta placa",
		             "Consulta de proced�ncia",
		             "Informa��es sobre Ve�culos",
		             "Informa��es cadastrais de Ve�culos",
		             "Dados de Ve�culos",
		             ];		
		
		words = shuffle(words);
		
// 		var nWords = getParameterByName('words');
// 		var nLines = getParameterByName('lines');
		
		var nWords =  <%= request.getParameter("words") %>
		var nLines =  <%= request.getParameter("lines") %>
		
		if (!nWords) {

		  	nWords = <%= request.getAttribute("words") %>
			
			if (!nWords) 
				nWords = 7;
		}

		if (!nLines) {
			nLines = <%= request.getAttribute("lines") %>
			
			if (!nLines) 
				nLines = 2;
		}
		
		addRows('dataTable', words, nWords, nLines);
	}	
	
	function getRandom(min, max) {
		return Math.floor(Math.random()*(max-min+1)+min);			
	}
	
	function addRows(tableID, array, words, lines) {
		 
	    var table = document.getElementById(tableID);
		//var cols = 10;
	    var rowCount = table.rows.length;
	    
	    for (var line=0;line < lines; line++) {
		    var row = table.insertRow(rowCount);
		    
		    array = shuffle(array);
			
		    var lwords = words;

	    	var i= 0;
	    	while (lwords>0) {
		    	var cell = row.insertCell(i);
		    	cell.style.color = '#F0F0F0';
		    	cell.align = "center";
		    	
		    	var header = getRandom(3,6); //sortear o tamanho do header
		    	
	    		cell.innerHTML = "<h" + header + ">" + array[i] + "</h" + header + ">";
    
	    		i = (i == array.length) ? 0 : ++i;
	    		
	    		lwords--;
	    	}
	    }
	
	}
	
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	function shuffle(array) {
	    var counter = array.length, temp, index;

	    // While there are elements in the array
	    while (counter--) {
	        // Pick a random index
	        index = (Math.random() * counter) | 0;

	        // And swap the last element with it
	        temp = array[counter];
	        array[counter] = array[index];
	        array[index] = temp;
	    }

	    return array;
	}

	function deleteRow(tableID) {
	    try {
		    var table = document.getElementById(tableID);
		    var rowCount = table.rows.length;
		
		    for(var i=0; i<rowCount; i++) {
		        var row = table.rows[i];
		        var chkbox = row.cells[0].childNodes[0];
		        if(null != chkbox && true == chkbox.checked) {
		            table.deleteRow(i);
		            rowCount--;
		            i--;
		        }
		
		
		    }
	    } catch(e) {
	        alert(e);
	    }
	}

</script>
 
</head>
<body onload="init()" >
	<div class="container">
		<p class="text-info">GrowUp 2008 �  - Consultoria e Legaliza��o de Ve�culos Ltda.</p>
	
		<table id="dataTable" cellspacing="0" cellpadding="0" style="background-color: #edeff1;" >
	    </table>
	</div>
	
</body>
</html>