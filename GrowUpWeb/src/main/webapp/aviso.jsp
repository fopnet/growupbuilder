<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet" />
    <!-- Estilos Gerais -->
    <link href="css/estilos.css" rel="stylesheet"/>
    
	<script src="js/lib/jquery-2.1.1.js"></script> 

	<title>Avisos</title>
</head>
<body>

    <form id="novoForm" action="criarUsuario?action=criar" method="post" data-toggle="validator">
	     <c:set var="msgRetorno" value="${msgRetorno}"/>
	     
	     <c:if test="${fn:length(msgRetorno) > 0}">	
	       	  <div class="container alert ${msgClass}">
	       	   <button type="button" class="close" data-dismiss="alert" value="Voltar">&times;</button>
	       	  	${msgRetorno}
			  </div>
		  </c:if>
	</form>		  

</body>
</html>