<?xml version="1.0" encoding="UTF-8"?>
<Context path="/ProjetoGrowUp" docBase="ProjetoGrowUp.war" reloadable="true" >
                
  <Resource name="jdbc/SqlsrvGrowupDS" type="javax.sql.DataSource" />
  
  <ResourceParams name="jdbc/SqlsrvGrowupDS">
  
   <!-- Tipo de autorizacao -->
  <parameter>
  <name>auth</name>
  <value>Container</value>
  </parameter>
  
  <!-- Descricao -->
  <parameter>
  <name>description</name>
  <value>Acesso a base GrowUp</value>
  </parameter>
  
  
  <!-- Maximum number of dB connections in pool. Make sure you
  configure your Firebird max_connections large enough to handle
  all of your db connections. Set to 0 for no limit.
  -->
  <parameter>
  <name>maxActive</name>
  <value>4</value>
  </parameter>
  
  <!-- Maximum number of idle dB connections to retain in pool.
  Set to 0 for no limit.
  -->
  <parameter>
  <name>maxIdle</name>
  <value>1</value>
  </parameter>
  
  <!-- Maximum time to wait for a dB connection to become available
  in ms, in this example 10 seconds. An Exception is thrown if
  this timeout is exceeded. Set to -1 to wait indefinitely.
  -->
  <parameter>
  <name>maxWait</name>
  <value>5000</value>
  </parameter>
  
  <!-- Query de validação -->
  <parameter>
  <name>validationQuery</name>
  <value>exec sp_tables</value>
  </parameter>
  
    <!-- Class name for SQlServer JDBC driver -->
  <parameter>
  <name>driverClassName</name>
  <value>com.microsoft.jdbc.sqlserver.SQLServerDriver</value>
  </parameter>
  
  
  <!-- Sql Server dB username and password for dB connections -->
  <parameter>
  <name>username</name>
  <value>growupadmin</value>
  </parameter>
  
  <parameter>
  <name>password</name>
  <value>growupadmin</value>
  </parameter>
  
  <!-- The JDBC connection url for connecting to your MySQL dB.
  The autoReconnect=true argument to the url makes sure that the
  mm.mysql JDBC Driver will automatically reconnect if mysqld closed the
  connection. mysqld by default closes idle connections after 8 hours.
  -->
  
  <parameter>
  <name>url</name>
  <!-- <value>jdbc:microsoft:sqlserver://localhost:1433/growup</value> -->
  <value>jdbc:sqlserver://localhost:1433;DatabaseName=GROWUP</value>
  </parameter>
  
  </ResourceParams>
</Context>             
