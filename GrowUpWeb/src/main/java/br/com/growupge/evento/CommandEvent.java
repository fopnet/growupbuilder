package br.com.growupge.evento;

import br.com.growupge.facade.GrowUpFacadeAdapter;

public interface CommandEvent extends SessionEvent {
	public abstract void execute(GrowUpFacadeAdapter facade);
}