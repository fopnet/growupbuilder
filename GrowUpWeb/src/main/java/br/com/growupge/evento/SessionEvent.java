package br.com.growupge.evento;

import java.io.Serializable;

import javax.servlet.http.HttpSessionEvent;

public interface SessionEvent extends Serializable {
	public abstract HttpSessionEvent getEvent();

}