package br.com.growupge.listener;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import br.com.growupge.evento.CommandEvent;
import br.com.growupge.facade.GrowUpFacadeAdapter;

/**
 * @author Felipe
 *
 */
//@ApplicationScoped
public class InitializationResourceBean {

    private static final Logger LOG = Logger.getLogger(InitializationResourceBean.class);

    @Inject
    private GrowUpFacadeAdapter facade;

    public void listen(@Observes CommandEvent event) {
    	LOG.info("executando evento " + getClass().getSimpleName());
    	event.execute(facade);
    }

    @PostConstruct
    public void init() {
    	LOG.info(getClass().getSimpleName() + " init");
    	
    	LOG.info("facade created" + facade);
    }

    @PreDestroy
    public void destroy() {
    	LOG.info(getClass().getSimpleName() + " destroy");
    }
}