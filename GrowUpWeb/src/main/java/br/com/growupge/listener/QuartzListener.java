package br.com.growupge.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;
import org.quartz.ee.servlet.QuartzInitializerListener;
import org.quartz.impl.StdSchedulerFactory;

import br.com.growupge.job.PeriodoPedidoPagamentoExpiradoJob;
import br.com.growupge.utility.DateUtil;

/**
 * Servlet implementation class JobScheduler
 */
@WebListener
public class QuartzListener implements ServletContextListener {
	private final static Logger logger = Logger.getLogger(QuartzListener.class);

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent ctx) {
		try {
			
			JobDetail job =  JobBuilder.newJob (PeriodoPedidoPagamentoExpiradoJob.class)
					.withIdentity(PeriodoPedidoPagamentoExpiradoJob.class.getSimpleName(), PeriodoPedidoPagamentoExpiradoJob.GROUP_NAME)
					.build();

			final DateUtil dm = DateUtil.getDateManagerInstance();
			dm.addMinutes(2);
			
			CronTrigger trigger = TriggerBuilder.newTrigger()
					.withIdentity("PeriodoPedidoPagamentoExpiradoScheduler", PeriodoPedidoPagamentoExpiradoJob.GROUP_NAME)
					.withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(dm.getHour(), dm.getMinutes()))
					.build();

			Scheduler scheduler = getSchedulerContext(ctx);
            scheduler.scheduleJob(job, trigger);

		} catch (SchedulerException e) {
			logger.error(e, e);
		}

		logger.info(getClass().getSimpleName() + " executado");		
	}

	/**
	 * @param ctx
	 * @return
	 * @throws SchedulerException
	 */
	private Scheduler getSchedulerContext(ServletContextEvent ctx)
			throws SchedulerException {
		return ((StdSchedulerFactory) ctx.getServletContext().getAttribute(QuartzInitializerListener.QUARTZ_FACTORY_KEY)).getScheduler();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent ctx) {
		try {
			Scheduler scheduler = getSchedulerContext(ctx);
			scheduler.getContext().clear();
			scheduler.clear();
			scheduler.shutdown();
		
		} catch (SchedulerException e) {
			logger.error(e, e);
		}
		
	}

}
