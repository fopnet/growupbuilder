package br.com.growupge.listener;


import java.io.File;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;

import br.com.growupge.evento.CommandEvent;
import br.com.growupge.facade.GrowUpFacadeAdapter;
import br.com.growupge.util.FileUtil;
import br.com.growupge.util.MbeanUtil;

/**
 * @author Felipe
 *
 */
@WebListener
public class SessaoExpiradaListener implements HttpSessionListener {
	private static final Logger logger = Logger.getLogger(SessaoExpiradaListener.class);
	
//	@javax.inject.Inject
//	@br.com.growupge.facade.Facade
//	private GrowUpFacade facade;
	
	@Override
	public void sessionCreated(HttpSessionEvent events) {
		File pastaArquivos = FileUtil.getUploadFile("");
		if (!pastaArquivos.exists()) {
			if (!pastaArquivos.mkdirs())
				throw new IllegalStateException("N�o foi poss�vel criar pasta " + pastaArquivos.getAbsolutePath());
			else 
				logger.info("Criando pasta padr�o para upload " + pastaArquivos.getPath());
		}
		
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		logger.info("Sess�o expirada");

/*		// Solucao 1
		BeanFactory.fireEvent(new SessaoExpiradaEvent(event));
		// Solucao 2
//		MbeanUtil.desconectar(event.getSession(), facade);
		
//		Este codigo causa loop, e a re-execu��o do evento.		
//		if (FacesContext.getCurrentInstance() != null) {
//			FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
//		}
*/	
	}
	
	class SessaoExpiradaEvent implements CommandEvent {
		private static final long serialVersionUID = 1L;
		private HttpSessionEvent event;
		
		SessaoExpiradaEvent(HttpSessionEvent event) {
			this.event = event;
		}
		
		/* (non-Javadoc)
		 * @see br.com.growupge.listener.SessionEvent#getEvent()
		 */
		@Override
		public HttpSessionEvent getEvent() {
			return event;
		}
		
		/* (non-Javadoc)
		 * @see br.com.growupge.listener.SessionEvent#execute(br.com.growupge.facade.GrowUpFacadeAdapter)
		 */
		@Override
		public void execute(GrowUpFacadeAdapter facade) {
			MbeanUtil.desconectar(getEvent().getSession(), facade );
		}
    }





}
