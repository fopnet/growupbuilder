package br.com.growupge.listener;


import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import br.com.growupge.gerenciadores.GerenciadorEmail;
import flex.messaging.FlexContext;

public class AppServletContextListener implements ServletContextListener {
	private static final Logger logger = Logger.getLogger(AppServletContextListener.class);
	
	@Override
	public void contextDestroyed(ServletContextEvent ctx) {
		logger.info("Application unloaded.");
		
		/* Desregistrando driver do banco de bancos 
		 * Ap�s algum redeploy ele n�o , o tomcat retorna no suitable driver
    	try {
    		// Tentativa 1 pra evitar problemas com o hot deploy
    		Enumeration<Driver> it = DriverManager.getDrivers();
			while (it.hasMoreElements()) {
    			DriverManager.deregisterDriver(it.nextElement());
    		}
		} catch (SQLException e) {
			logger.error(e,e);
		}
		*/
		
		//Solucao 3
		GerenciadorEmail.destroy();
//		FacesContext fCtx = FacesContext.getCurrentInstance();
//		if (fCtx != null)
//			fCtx.release();
		FlexContext.clearThreadLocalObjects();
		
    	
    	/* Tentativa 2 pra evitar problemas com o hot deploy 
		try {
			 final ClassLoader active = Thread.currentThread().getContextClassLoader();
			 try {
				 //Find the root classloader
				 ClassLoader root = active;
				 logger.info("Active classloadr " + root.getClass().getName());
				 while (root.getParent() != null) {
					 logger.info("Parent classloadr " + root.getClass().getName());
					 root = root.getParent();
				 }
				 //Temporarily make the root class loader the active class loader
				 Thread.currentThread().setContextClassLoader(root);

			 } finally {
				 //restore the class loader
				 Thread.currentThread().setContextClassLoader(active);   
			 }
			} catch ( Throwable t) {
				 //Carry on if we get an error
				 logger.info("Failed to address PermGen leak");
			}
			*/
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		BasicConfigurator.configure();
		logger.info("Application loaded.");

		GerenciadorEmail.getInstance();
		
	}

}
