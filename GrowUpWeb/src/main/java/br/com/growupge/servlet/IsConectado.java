package br.com.growupge.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.growupge.constantes.GCS;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.facade.GrowUpFacadeAdapter;

/**
 * Servlet implementation class IsConectado
 */
@WebServlet(name="IsConectadoServlet", urlPatterns="/isConectado")
public class IsConectado extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	@javax.inject.Inject
	private GrowUpFacadeAdapter facade;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IsConectado() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SessaoDTO sessao = new SessaoDTO();
		sessao.setSid(request.getSession().getId());

		try {
			sessao = (SessaoDTO) facade.executarComando(request.getSession(), 
														GCS.MAPA_SESSAO, 
														GCS.PROCURAR_SESSAO, sessao);
		} catch (GrowUpException e) {
			throw new ServletException(e);
		}

		PrintWriter out = response.getWriter();
		out.print(sessao);
		
	}

}
