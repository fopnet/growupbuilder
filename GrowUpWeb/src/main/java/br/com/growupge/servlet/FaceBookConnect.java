package br.com.growupge.servlet;


import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.URI;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.log4j.Logger;

import br.com.growupge.constantes.GCS;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.facade.GrowUpFacadeAdapter;
import br.com.growupge.util.HttpUtil;
import br.com.growupge.util.MbeanUtil;
import br.com.growupge.util.SdkConfig;

import com.google.gson.stream.JsonReader;



/**
 * @author Felipe
 *
 */
@WebServlet( urlPatterns = {"/facebookConnect"})
public class FaceBookConnect extends HttpServlet {
	

	@javax.inject.Inject
	private GrowUpFacadeAdapter facade;
	
	private static final Logger logger = Logger.getLogger(FaceBookConnect.class);
	
	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1L;

	// app face params
	private static final String APP_ID = SdkConfig.getAppID();
	private static final String APP_SECRET = SdkConfig.getAppSecret();
	
	// face params
	private static final String PARAM_APP_ID = "client_id";
	private static final String PARAM_DISPLAY = "display";
	private static final String PARAM_REDIRECT_URI = "redirect_uri";
	private static final String PARAM_APP_SECRET = "client_secret";
	private static final String PARAM_CODE = "code";
	private static final String PARAM_SCOPE = "scope";
	//private static final String PARAM_FIELDS = "fields";
	private static final String PARAM_ACCESS_TOKEN = "access_token";
	
	// URL's
	private static final String URL_FACE_LOGIN = "https://www.facebook.com/dialog/oauth";
	private static final String URL_AUTH_API = "https://graph.facebook.com/v2.3/oauth/access_token";
	private static final String URL_GRAPH_API = "https://graph.facebook.com/me";

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String post = "";
			String code = request.getParameter("code");
			logger.info( request.getServletPath() );
			
			if (StringUtils.isBlank(code)) {
				logger.info("Passo 1 - verificar se existe o sid no banco como sess�o.");
				SessaoDTO sessao = new SessaoDTO(request.getSession().getId());
				sessao = (SessaoDTO) facade.executarComando(request.getSession(), 
															  GCS.MAPA_SESSAO,
															  GCS.PROCURAR_SESSAO,
															  sessao);
				if (sessao == null) {
					logger.info("Passo 2 -- se n�o estiver conectado. acessar o servico");
					post = new URIBuilder(new URI(URL_FACE_LOGIN))
											.setParameter(PARAM_APP_ID, APP_ID)
											.setParameter(PARAM_DISPLAY, "popup")
//											.setParameter("scope", "id,name,email")
//											.setParameter("auth_type","rerequest")
											.setParameter(PARAM_SCOPE, "email")
											.setParameter(PARAM_REDIRECT_URI, SdkConfig.getRedirectUri())
											.toString();
					//HttpUtil.sendPost(post);
//					response.sendRedirect(request.getServletContext().getRealPath("/novo.jsp"));
					response.sendRedirect(post);
					/*
					response.setContentType("application/json");
					// Get the printwriter object from response to write the required json object to the output stream      
					PrintWriter out = response.getWriter();
					// Assuming your json object is **jsonObject**, perform the following, it will return your json object
					out.print("{ uri: "  +  post  +  "}");
					out.flush();
					*/
				} else 
					closeWindow(response);
			} else {
				
				logger.info("Passo 3 - login e senha j� digitado, e code retornado. getting access_token");
				post = new URIBuilder(new URI(URL_AUTH_API))
								.setParameter(PARAM_APP_ID, APP_ID)
								.setParameter(PARAM_APP_SECRET, APP_SECRET)
								.setParameter(PARAM_REDIRECT_URI, SdkConfig.getRedirectUri())
								.setParameter(PARAM_CODE, code)
								.toString();
				
//				HttpUtil.sendPostAndRedirect(response, post);
				String json = HttpUtil.getPostResponse(response, post);
				
				logger.info("Passo 4 access_token received");
//				String json = HttpUtil.responseToString(request.getInputStream());
				String accessToken = null;
				
				try (JsonReader reader = new JsonReader(new StringReader(json))) {
					reader.beginObject();
					while (reader.hasNext()) {
						String name = reader.nextName();
					
						switch(name) {
							case "access_token":
								accessToken = reader.nextString();
								break;
							default:
								reader.skipValue();
						}
					}
					reader.endObject();
				}
				
				logger.info("Passo 5 Accessing Graph api data with access_token ");
				
				post = new URIBuilder(new URI(URL_GRAPH_API))
//									.setParameter(PARAM_FIELDS, "name,email")
									.setParameter(PARAM_SCOPE, "email")
									.setParameter(PARAM_ACCESS_TOKEN, accessToken)
									.toString();

				json = HttpUtil.getResponse(response, post, "GET");
				UsuarioDTO usr = new UsuarioDTO();
				try (JsonReader reader = new JsonReader(new StringReader(json))) {
					reader.beginObject();
					while (reader.hasNext()) {
						String name = reader.nextName();
						logger.info("name:" + name);
						switch (name) {
							case "name":
								usr.setNome( reader.nextString() );
								logger.info("value:" + String.valueOf( usr.getNome() ));
								break;
							case "email":
								usr.setEmail( reader.nextString() );
								logger.info("value:" + String.valueOf( usr.getEmail() ));
								break;
							default:
								reader.skipValue();
						}
					}
					reader.endObject();
				}
				
				logger.info("Passo 6  creating session and creating / updating user ");
				 
				SessaoDTO sessaoDTO = new SessaoDTO(request.getSession().getId());
				sessaoDTO.setUsuario(usr);
				sessaoDTO.setAccessToken(accessToken);
//				usr.setEmail(null);
				
				sessaoDTO = (SessaoDTO) facade.executarComando(request.getSession(), 
									  GCS.MAPA_SESSAO,
									  GCS.CONECTAR_FACEBOOK,
									  sessaoDTO);
				
				// cria um context para o jsf, armazenar os dados armazenados na sessao 
				MbeanUtil.setUsuarioSessao(sessaoDTO, request.getSession());
				
				closeWindowAndRedirect(response, SdkConfig.getRedirectUri() + "/../pedido.jsf"); 
			}

		} catch (GrowUpException e) {
			request.setAttribute("msgClass", NovoUsuario.CSS_ERROR_ALERT);
			if (MSGCODE.CAMPOS_OBRIGATORIOS.equals(e.getCode()))
				request.setAttribute("msgRetorno", getCausaNaoRetornoEmailHtml());
			else
				request.setAttribute("msgRetorno", isNotBlank(e.getMensagemOriginal()) ? e.getMensagemOriginal() : e.getMensagem());
			
			RequestDispatcher view = request.getRequestDispatcher("aviso.jsp");
			view.forward(request, response);
			
//			response.sendRedirect("aviso.jsp");
			
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
	
/*	private void exibirCausasFaltaEmail(HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();  
		response.setContentType("text/html");
		out.println("<script type=\"text/javascript\">");
		
		out.println(" var divAlert =  this.window.opener.getElementsByClassName('alert')[0] ;");
		out.println(" divAlert.removeChild(  divAlert.lastChild  ); ");  
		out.println(" divAlert.innerHTML =\'" + getCausaNaoRetornoEmailHtml() + "'");
		out.println("</script>");
	}*/
	
	private String getCausaNaoRetornoEmailHtml() {
		return new StringBuilder()
		.append("<p>O endere�o de email n�o retornou do servi�o do facebook. Este email � fundamental o site, enviar o relat�rio do resultado da an�lise requisitada. Logo abaixo s�o listadas algumas poss�veis raz�es:</p>")
		.append("<ul>")
		.append("<li>Nenhum endere�o de email cadastrado no perfil</li>")
		.append("<li>Nenhum endere�o de email confirmado no perfil</li>")
		.append("<li>Nenhum endere�o de email verificado no pergil</li>")
		.append("<li>O usu�rio inseriu uma regra de seguran�a obrigat�ria para reconfirmar o seu email que ainda n�o foi realizado</li>")
		.append("<li>O email do usu�rio est� inacess�vel</li>")
		.append("</ul>")
		.append("Voc� tamb�m precisa marcar a permiss�o extendida \"email\", mesmo para o usu�rio que possui todos os requisitos acima v�lidos.")
		.toString()
		;
		
	}

	private void closeWindow(HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();  
		response.setContentType("text/html");
		out.println("<script type=\"text/javascript\">");  
		out.println("	this.window.opener.location.reload();");
		out.println("   this.window.close();");  
		out.println("</script>");
	}

	private void closeWindowAndRedirect(HttpServletResponse response, String url) throws IOException {
		PrintWriter out = response.getWriter();  
		response.setContentType("text/html");
		out.println("<script type=\"text/javascript\">");  
		out.println("	this.window.opener.location = '" + url + "';");
		out.println("   this.window.close();");  
		out.println("</script>");
	}


/*	private String getURLRoot(HttpServletRequest request, String... relativePaths) {
		String url = request.getRequestURL().toString();
		String uri = request.getRequestURI();
		String root = url.substring( 0, url.indexOf(uri) );
		for (String path : relativePaths) {
			if (!path.startsWith("/") && !path.startsWith("\\"))
				root += "/"; 
			root +=  path; 
		}
		return root;
	}
	*/
}