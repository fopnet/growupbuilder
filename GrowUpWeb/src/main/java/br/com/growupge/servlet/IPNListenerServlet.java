package br.com.growupge.servlet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import br.com.growupge.constantes.GCS;
import br.com.growupge.dto.PagamentoDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.facade.GrowUpFacadeAdapter;
import br.com.growupge.service.paypal.builder.PayPalDTOBuilder;
import br.com.growupge.util.SdkConfig;

import com.paypal.ipn.IPNMessage;

@javax.inject.Named
@javax.enterprise.context.RequestScoped
@WebServlet(name="IPNListenerServlet", urlPatterns="/IPNListener")
public class IPNListenerServlet extends HttpServlet {
	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(IPNListenerServlet.class);

	@javax.inject.Inject
	private GrowUpFacadeAdapter facade;

	/* 
	 * receiver for PayPal ipn call back.
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		// For a full list of configuration parameters refer in wiki page
		// (https://github.com/paypal/sdk-core-java/wiki/SDK-Configuration-Parameters)
		Map<String,String> configurationMap =  SdkConfig.getConfigurationMap();
		IPNMessage 	ipnlistener = new IPNMessage(request,configurationMap);

		boolean isIpnVerified = ipnlistener.validate();
		String transactionType = ipnlistener.getTransactionType();
		Map<String,String> map = ipnlistener.getIpnMap();
		
		
		try {
			PagamentoDTO pg = PayPalDTOBuilder.buildPagamentoDTO(ipnlistener);
			
			facade.executarComando(request.getSession(), 
									GCS.MAPA_PAGAMENTO, 
									GCS.CONFIRMAR_PAGAMENTO, 
									pg);
		} catch (GrowUpException e) {
			logger.error(e.getMensagem());
			throw new ServletException(e.getMensagem());
		} catch (Exception e) {
			logger.error(e, e);
			throw new ServletException(e);
		}
		
		logger.info("******* IPN (name:value) pair : "+ map + "  " +
				"######### TransactionType : "+transactionType + 
				"  ======== IPN verified : "+ isIpnVerified);
	}
}
