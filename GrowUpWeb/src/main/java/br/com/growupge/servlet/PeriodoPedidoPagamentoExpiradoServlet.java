package br.com.growupge.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import br.com.growupge.constantes.GCS;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.facade.GrowUpFacadeAdapter;
import br.com.growupge.util.ServletUtil;

/**
 * Servlet implementation class PeriodoPedidoPagamentoExpiradoServlet
 */
@WebServlet(name="PeriodoPedidoPagamentoExpiradoServlet", urlPatterns="/verificarPagamentosExpirados")
public class PeriodoPedidoPagamentoExpiradoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(PeriodoPedidoPagamentoExpiradoServlet.class);
	
	@Inject
	private GrowUpFacadeAdapter facade;
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			
			facade.executarComando(ServletUtil.getServicoSession(), 
									GCS.MAPA_PEDIDO_VEICULO, 
									GCS.EXPIRAR_PEDIDOS);
			
			logger.info(getClass().getSimpleName() + " executado com sucesso" );
			
		} catch (GrowUpException e) {
			logger.error(e, e);
		}
	}

}
