package br.com.growupge.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import br.com.growupge.apresentacao.bean.TarifaBean;
import br.com.growupge.exception.GrowUpException;

/**
 * @author Felipe
 *
 */
@WebServlet( urlPatterns = {"/tarifas"})
public class Tarifas extends HttpServlet {
	
	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1L;

	@javax.inject.Inject
	private TarifaBean tarifaBean;
	
	private final static Logger logger = Logger.getLogger(Tarifas.class);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
            if (tarifaBean == null) {
            	logger.debug("Error reason tarifaBean == null") ;
            }
			tarifaBean.setSession(request.getSession());
			
			request.setAttribute("valorTarifa", tarifaBean.getValorUnitario());
			getServletContext().getRequestDispatcher("/WEB-INF/pages/tarifas.jsp").forward(request, response);
		
		} catch (GrowUpException e) {
			throw new ServletException(e);
//			getServletContext().getRequestDispatcher("error.jsp").forward(request, response);
		}
	}

}