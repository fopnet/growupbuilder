package br.com.growupge.servlet;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.growupge.constantes.GCS;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.facade.GrowUpFacadeAdapter;
import br.com.growupge.util.MbeanConstantes;

/**
 * Servlet implementation class Conectar
 */
@WebServlet(name="ConectarServlet", urlPatterns="/conectar")
public class Conectar extends HttpServlet implements MbeanConstantes {
	private static final long serialVersionUID = 1L;
//	private final static Logger logger = Logger.getLogger(Conectar.class);

	@javax.inject.Inject
	private GrowUpFacadeAdapter facade;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Conectar() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			SessaoDTO sessao = new SessaoDTO();
			sessao.setUsuario(new UsuarioDTO());
			sessao.getUsuario().setCodigo(request.getParameter("codigo").toString());
			sessao.getUsuario().setSenha(request.getParameter("senha").toString());
			
			sessao = (SessaoDTO) facade.executarComando(request.getSession(), 
														GCS.MAPA_SESSAO, 
														GCS.CONECTAR, sessao);
			
//			 String urlWithSessionID = response.encodeRedirectURL("http://localhost:8080/GrowUpFlex/index.html#sid=" + sessao.getSid());
			 String urlWithSessionID = response.encodeRedirectURL("http://www.growupge.com.br/index.html#sid=" + sessao.getSid());
			 response.sendRedirect( urlWithSessionID );
			    
//			RequestDispatcher view = request.getRequestDispatcher("www.growupge.com.br");
//			view.forward(request, response);
			
		} catch (GrowUpException e) {
			request.setAttribute("msgClass", CSS_ERROR_ALERT);
			request.setAttribute("msgRetorno", isNotBlank(e.getMensagemOriginal()) ? e.getMensagemOriginal() : e.getMensagem());

			RequestDispatcher view = request.getRequestDispatcher("conectar.jsp");
			view.forward(request, response);
		} catch (Exception e) {
			request.setAttribute("msgClass", CSS_ERROR_ALERT);
			request.setAttribute("msgRetorno", e.getMessage());
			
			RequestDispatcher view = request.getRequestDispatcher("conectar.jsp");
			view.forward(request, response);
		}

	}
	

}
