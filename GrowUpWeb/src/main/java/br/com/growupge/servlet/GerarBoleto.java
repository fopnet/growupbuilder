package br.com.growupge.servlet;

import static br.com.growupge.utility.StringUtil.removerCaracteresInvalidos;
import static org.apache.commons.lang3.StringUtils.defaultIfBlank;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Itau.Itaucripto;
import br.com.growupge.constantes.GCS;
import br.com.growupge.dto.PagamentoBoletoDTO;
import br.com.growupge.dto.PagamentoDTO;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.facade.GrowUpFacadeAdapter;
import br.com.growupge.util.ServletUtil;

/**
 * @author Felipe
 *
 */
@WebServlet( urlPatterns = {"/gerarBoleto"})
public class GerarBoleto extends HttpServlet {

	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1L;
	
	@javax.inject.Inject
	private GrowUpFacadeAdapter facade;  

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doGet(req, resp);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public	void doPost(HttpServletRequest	request,	HttpServletResponse	response)	throws	ServletException,	IOException	{
		service(request,	response);
	} 

	@Override
	public	void	init()	{ }

	@Override
	public	void	service(HttpServletRequest	request,	HttpServletResponse	response)	throws	ServletException,	IOException	{
		String	codigoEmpresa;							//	C�digo	de	identifica��o	da	empresa
		String chave; // Chave de criptografia da empresa
		String	pedido;														//	Identifica��o	do	pedido
		String valor; // Valor do pedido
		String	observacao;										//	Observa��es
		String nomeSacado; // Nome do sacado 
		String	codigoInscricao;					//	C�digo	de	Inscri��o:	01->CPF,	02->CNPJ
		String	numeroInscricao;					//	N�mero	de	Inscri��o:	CPF	ou	CNPJ
		String enderecoSacado; // Endereco do Sacado
		String bairroSacado; // Bairro do Sacado
		String cepSacado; // Cep do Sacado
		String cidadeSacado; // Cidade do sacado
		String estadoSacado; // Estado do Sacado
		String dataVencimento; // Vencimento do t�tulo
		String	urlRetorna;														//	URL	do	retorno
		String	obsAdicional1;							//	ObsAdicional1
		String	obsAdicional2;							//	ObsAdicional2
		String	obsAdicional3;							//	ObsAdicional3

		
		PrintWriter	out	=	response.getWriter();	
		String	dados = null;	//Armazena	os	dados	criptografados

		//Inicializa	as	vari�veis
		codigoEmpresa	=	Itaucripto.CODIGO_EMPRESA;	//	<-	Coloque	aqui	seu	C�digo	de	Empresa	(26	posi��es)
		chave =	Itaucripto.CHAVE_ACESSO;	//	<-	Coloque	aqui	sua	chave	de	criptografia	(16	posi��es)

		// Recebe valores da p�gina anterior 
		pedido			=	request.getParameter("pedido");		//	obt�m	o	valor	do	campo	"pedido"	
		valor			=	defaultIfBlank(request.getParameter("valor"),"");		//	obt�m	o	valor	do	campo	"valor"	
		observacao		=	defaultIfBlank(request.getParameter("observacao"),"");//	obt�m	o	valor	do	campo	"observacao"
		nomeSacado		=	defaultIfBlank(request.getParameter("nomeSacado"),"");	//	obt�m	o	valor	do	campo	"nomeSacado"	
		codigoInscricao	=	defaultIfBlank(request.getParameter("codigoInscricao"),"");	//	obt�m	o	valor	do	campo	"codigoInscricao"
		numeroInscricao	=	defaultIfBlank(request.getParameter("numeroInscricao"),""); //	obt�m	o	valor	do	campo	"numeroInscricao"
		enderecoSacado	=	defaultIfBlank(request.getParameter("enderecoSacado") ,"");	 //	obt�m	o	valor	do	campo	"enderecoSacado"
		bairroSacado	=	defaultIfBlank(request.getParameter("bairroSacado") ,"");	//	obt�m	o	valor	do	campo	"bairroSacado"
		cepSacado		=	defaultIfBlank(request.getParameter("cepSacado") ,"");		//	obt�m	o	valor	do	campo	"cepSacado"
		cidadeSacado	=	defaultIfBlank(request.getParameter("cidadeSacado") ,"");	//	obt�m	o	valor	do	campo	"cidadeSacado"
		estadoSacado	=	defaultIfBlank(request.getParameter("estadoSacado") ,"");//	obt�m	o	valor	do	campo"estadoSacado"
		dataVencimento	=	defaultIfBlank(request.getParameter("dataVencimento") ,"");	//	obt�m	o	valor	do	campo	"dataVencimento"
		urlRetorna		=	defaultIfBlank(request.getParameter("urlRetorna") ,"");		//	obt�m	o	valor	do	campo	"urlRetorna"
		obsAdicional1	=	defaultIfBlank(request.getParameter("obsAdicional1") ,"");	//	obt�m	o	valor	do	campo	"obsAdicional1"
		obsAdicional2	=	defaultIfBlank(request.getParameter("obsAdicional2") ,"");	//	obt�m	o	valor	do	campo	"obsAdicional2"
		obsAdicional3	=	defaultIfBlank(request.getParameter("obsAdicional3") ,"");	//	obt�m	o	valor	do	campo	"obsAdicional3"


		PagamentoDTO pgto = new PagamentoBoletoDTO(new PedidoVeiculoDTO(Long.parseLong(pedido)));

		try {
			pgto = (PagamentoDTO) facade.executarComando(request.getSession(), 
														GCS.MAPA_PAGAMENTO, 
														GCS.SOLICITAR_PAGAMENTO,
														pgto);
		} catch (GrowUpException e) {
			throw new ServletException(e);
		}

		// Obtem o valor formatado conforme a api exige.
		valor = pgto.getValorDecimalFormatado();
		dataVencimento = removerCaracteresInvalidos( pgto.getDataVencimentoFormatada() );
		cepSacado = removerCaracteresInvalidos( cepSacado );
		numeroInscricao = removerCaracteresInvalidos ( numeroInscricao );
		
		/* Inicializa	a	classe	de	criptografia	do	Shopline */
		Itaucripto	cripto	=	new	Itaucripto();
		
		//Criptografa	os	dados	chamando	o	m�todo	geraDados	da	classe	Itaucripto
		dados	=	cripto.geraDados(codigoEmpresa,
				pedido,
				valor,
				observacao,
				chave,
				nomeSacado,
				codigoInscricao,
				numeroInscricao,
				enderecoSacado,
				bairroSacado,
				cepSacado,
				cidadeSacado,
				estadoSacado,
				dataVencimento,
				urlRetorna,
				obsAdicional1,
				obsAdicional2,
				obsAdicional3
				);

		//Inicia	a	constru��o	da	p�gina	de	resposta	
		response.setContentType(ServletUtil.CONTENT_TYPE);

		out.println("<html>");
		out.println("<body	bgcolor=\"white\">");
		out.println("<head>");
		out.println("<title>	Grow Up Ve�culos - Gerador Boleto	</title>");
		out.println("</head>");
		out.println("<body>");
//		out.println("<h1>Modelo	Ita�</h1>");

		// Constr�i o formul�rio para pagamento com shopline
//		https://shopline.itau.com.br/shopline/shopline.aspx
//		https://shopline.itau.com.br/shopline/Impressao.aspx
//		https://shopline.itau.com.br/shopline/pagamento.aspx target=\"SHOPLINE\"
		out.println("<FORM 	METHOD=\"POST\"	ACTION=\"https://shopline.itau.com.br/shopline/pagamento.aspx\"	name=\"form\"   onsubmit=\"carregabrw()\"	>");
		out.println("<INPUT	TYPE=\"hidden\"	NAME=\"DC\"	VALUE=\""	+	dados	+	"\">");
//		out.println("<INPUT	TYPE=\"submit\"	name=\"Shopline\" id=\"btn\"  value=\"Ita�	Shopline\">");
		out.println("</FORM>");
		out.println("<script	language=\"JavaScript\">");
//		out.println("<!--");
		out.println("function	carregabrw()	{	");
		out.println("					window.open('\','SHOPLINE','toolbar=yes,menubar=yes,resizable=yes,status=no,scrollbars=yes,width=815,height=575');	");
		out.println("}");
		out.println(" document.form.submit(); ");//chamando o metodo
//		out.println("//-->");
		out.println("</script>");
		out.println("</body>");
		out.println("</html>");
	} 
	
}