package br.com.growupge.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.GCS;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.facade.GrowUpFacadeAdapter;
import br.com.growupge.util.ServletUtil;

/**
 * Servlet implementation class HabilitarUsuario
 */
@WebServlet(name="HabilitarUsuarioServlet", urlPatterns="/habilitar")
public class HabilitarUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@javax.inject.Inject
	private GrowUpFacadeAdapter facade;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HabilitarUsuario() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType(ServletUtil.CONTENT_TYPE);

		UsuarioDTO usr = new UsuarioDTO(request.getParameter(Constantes.PARAM_USUARIO));
		usr.setEmail(request.getParameter(Constantes.PARAM_EMAIL));
		
		PrintWriter out = response.getWriter();
		try {
			usr = (UsuarioDTO) facade.executarComando(request.getSession(), 
													  GCS.MAPA_USUARIO,
													  GCS.HABILITAR_USUARIO,
													  usr);
			
			out.print(usr.getMensagemRetorno());
			
		} catch (GrowUpException e) {
			ServletUtil.printError(out, e.getMensagem());

//			throw new ServletException(e.getMensagem());
		} catch (Exception e) {
			ServletUtil.printError(out, e.getMessage());
			
//			throw new ServletException(e);
		}

	}

}
