package br.com.growupge.servlet;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.GCS;
import br.com.growupge.dto.ComandoDTO;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.facade.GrowUpFacade;
import br.com.growupge.facade.GrowUpFacadeAdapter;
import br.com.growupge.util.MbeanConstantes;

/**
 * Servlet implementation class NovoUsuario
 */
@WebServlet(name="NovoUsuarioServlet",urlPatterns="/criarUsuario")
public class NovoUsuario extends HttpServlet implements MbeanConstantes {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(NovoUsuario.class);
    
	@javax.inject.Inject
	private GrowUpFacadeAdapter facade;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NovoUsuario() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		doPost(request, response);
//	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		logger.info(request.getParameter("action"));
		/* GrowUpFacadeAdapter facade = FacadeFactory.getFacade(); */
		
//		PrintWriter out = response.getWriter();
		try {
			if (request.getParameter("action") != null) {
				UsuarioDTO usr = new UsuarioDTO();
				usr.setCodigo(request.getParameter("codigo"));
				usr.setNome(request.getParameter("nome"));
				usr.setEmail(request.getParameter("email"));
				usr.setIdioma(request.getParameter("idioma"));
					
				usr = criarUsuario(request, usr, facade);

				request.setAttribute("msgRetorno", usr.getMensagemRetorno());
			} else {
				request.setAttribute("msgRetorno", "");
			}
			
			request.setAttribute("msgClass", "alert-success");
			
			
		} catch (GrowUpException e) {
//			out.print("<h2 style=\"color:red;\">ERRO:");
//			out.print(e.getMensagem());
//			out.print("</h2>");
			request.setAttribute("msgClass", CSS_ERROR_ALERT);
			request.setAttribute("msgRetorno", isNotBlank(e.getMensagemOriginal()) ? e.getMensagemOriginal() : e.getMensagem());

		} catch (Exception e) {
//			out.print("<h2 style=\"color:red;\">ERRO:");
//			out.print(e.getMessage());
//			out.print("</h2>");
			request.setAttribute("msgClass", CSS_ERROR_ALERT);
			request.setAttribute("msgRetorno", e.getMessage());
			
		} finally {
			RequestDispatcher view = request.getRequestDispatcher("novo.jsp");
			view.forward(request, response);
		}

	}

	/**
	 * @param usr
	 * @param facade
	 * @return
	 * @throws GrowUpException 
	 */
	private UsuarioDTO criarUsuario(HttpServletRequest request, UsuarioDTO usr, GrowUpFacadeAdapter facade) throws GrowUpException {
		return (UsuarioDTO) facade.executarComando(request.getSession(),
												  GCS.MAPA_USUARIO,
												  GCS.ADICIONAR_USUARIO,
												  usr);
	}


	protected UsuarioDTO procurarUsuario(HttpServletRequest request, GrowUpFacade facade) throws GrowUpException {
		UsuarioDTO usr = new UsuarioDTO();
		usr.setCodigo(request.getParameter(Constantes.PARAM_USUARIO));
		usr.setEmail(request.getParameter(Constantes.PARAM_EMAIL));
		
		ComandoDTO cmd = new ComandoDTO();
		cmd.setSID(request.getSession().getId());
		cmd.setMapaGCS(GCS.MAPA_USUARIO);
		cmd.setPermissao(GCS.PROCURAR);
		cmd.getParametrosLista().add(usr);
		
		return (UsuarioDTO) facade.executarComando(cmd, request.getSession());
	}
	
}
