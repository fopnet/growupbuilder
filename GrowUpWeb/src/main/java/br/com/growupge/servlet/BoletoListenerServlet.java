/**
 * 
 */
package br.com.growupge.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import Itau.Itaucripto;
import br.com.growupge.constantes.GCS;
import br.com.growupge.dto.PagamentoBoletoDTO;
import br.com.growupge.dto.PagamentoDTO;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.facade.GrowUpFacadeAdapter;

/**
 * @author Felipe
 *
 */

@javax.inject.Named
@javax.enterprise.context.RequestScoped
public class BoletoListenerServlet extends HttpServlet {

	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(BoletoListenerServlet.class);
	
	@javax.inject.Inject
	private GrowUpFacadeAdapter facade;

	public void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//Inicializa as vari�veis
		String dados, pedido = null;
		//String tipPag;

		//Inicializa	as	vari�veis
		//Inicializa	as	vari�veis
		//String codigoEmpresa	=	Itaucripto.CODIGO_EMPRESA;	//	<-	Coloque	aqui	seu	C�digo	de	Empresa	(26	posi��es)

		//Recebe valores da p�gina anterior
		dados = request.getParameter("DC");
		//Inicializa a classe de criptografia do Shopline
		Itaucripto cripto = new Itaucripto();
		//Decriptografa os dados chamando o m�todo decripto da classe Itaucripto e recebe os dados
		dados = cripto.decripto(dados, Itaucripto.CHAVE_ACESSO);

//		codEmp = cripto.retornaCodEmp();
		pedido = cripto.retornaPedido();
//		tipPag = cripto.retornaTipPag();

		PagamentoDTO pg = new PagamentoBoletoDTO(new PedidoVeiculoDTO(Long.parseLong(pedido)));
		try {
			facade.executarComando(request.getSession(), 
									GCS.MAPA_PAGAMENTO, 
									GCS.CONFIRMAR_PAGAMENTO, 
									pg);
		} catch (GrowUpException e) {
			logger.error(e.getMensagem());
			throw new ServletException(e.getMensagem());
		}
		
		
		// inicia a constru��o da pagina de resposta
/*		response.setContentType("text/html");

		final ServletOutputStream out = response.getOutputStream();
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Retorno Online</title>");
		out.println("</head>");
		out.println("<body bgcolor=\"white\">");
		out.println("<h1>Retorno Ita�</h1>");
		out.println(dados + "<br>");
		out.println(codEmp + "<br>");
		out.println(pedido + "<br>");
		out.println(tipPag + "<br>");
		out.println("</body>");
		out.println("</html>");*/
	}
}