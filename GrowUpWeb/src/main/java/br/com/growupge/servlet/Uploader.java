package br.com.growupge.servlet;

import static org.apache.commons.lang3.StringUtils.defaultString;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import br.com.growupge.util.FileUtil;


/**
 * This class provides functionality to handle an uploaded file.
 * This class uses code provided by Apache
 *
 * @author geert.zijlmans
 *
 */

@WebServlet("/uploader")
public class Uploader extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(Uploader.class);

	/**
	 * Default constructor.
	 */
	public Uploader() {}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nip = defaultString(request.getParameter("nip"));
		logger.info("nip:" + nip);
		
		PrintWriter out = response.getWriter();
		try {
			// Check that we have a file upload request
			boolean isMultipart = ServletFileUpload.isMultipartContent(request);

			logger.info(request.getContentType());
			if (isMultipart) {
				// Create a factory for disk-based file items
				FileItemFactory factory = new DiskFileItemFactory();

				// Create a new file upload handler
				ServletFileUpload upload = new ServletFileUpload(factory);

				// Parse the request
				List<?> /* FileItem */ items = upload.parseRequest(request);

				// Process the uploaded items
				Iterator<?> iter = items.iterator();
				while (iter.hasNext()) {
					FileItem item = (FileItem) iter.next();

					if (item.isFormField()) {
						processFormField(item);
					} else {
						processUploadedFile(item,  FileUtil.getUploadFile(nip).getAbsolutePath()) ;
					}
				}

				response.setContentType("text/plain; charset=UTF-8");
				out.write("Upload complete");
				response.setStatus(HttpServletResponse.SC_OK);
			}
		}
		catch (Exception e) {
			logger.fatal(e.getMessage());
		} finally {
			out.close();
		}
	}

	private void processFormField(FileItem item) {
		String name = item.getFieldName();
		String value = item.getString();
		logger.info("Item name: " + name + " ; value: " + value);
	}

	private void processUploadedFile(FileItem item, String parent) throws Exception {
//		String fieldName = item.getFieldName();
		String fileName = item.getName();
//		String contentType = item.getContentType();
//		boolean isInMemory = item.isInMemory();
		long sizeInBytes = item.getSize();

		boolean writeToFile = true;
		if (sizeInBytes > (5 * 1024 * 1024)) {
			writeToFile = false;
		}
		// Process a file upload
		if (writeToFile) {
			File parentFile = new File(parent);
			if (!parentFile.exists()) parentFile.mkdirs();
			File uploadedFile = new File(parentFile,fileName);
			if (!uploadedFile.exists()) {
				uploadedFile.createNewFile();
			}
			item.write(uploadedFile);
		}
		else {
			logger.info("Trying to write a large file.");
		}

	}
}