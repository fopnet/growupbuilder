package br.com.growupge.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Itau.Itaucripto;

/**
 * Servlet implementation class consultaBoleto
 */
@WebServlet("/consultaBoleto")
public class ConsultaBoleto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			//Inicializa as vari�veis
			String pedido, formato, dados;
			
			//Inicializa	as	vari�veis
			String codigoEmpresa	=	Itaucripto.CODIGO_EMPRESA;	//	<-	Coloque	aqui	seu	C�digo	de	Empresa	(26	posi��es)
			String chave =	Itaucripto.CHAVE_ACESSO;	//	<-	Coloque	aqui	sua	chave	de	criptografia	(16	posi��es)

			formato = "0"; // <-Coloque aqui o tipo de retorno - 0 ou 1 dependendo do retorno
					
			//Recebe o pedido
			pedido = request.getParameter("nip");
			//Inicializa a classe de criptografia do Shopline
			
			Itaucripto cripto = new Itaucripto();
			//Realiza a criptografia dos dados
			dados = cripto.geraConsulta(codigoEmpresa, pedido, formato, chave);
			
			final ServletOutputStream out = response.getOutputStream();
			// Monta o form para o envio dos dados a consulta
			out.println("<form method=\"post\" action=\"https://shopline.itau.com.br/shopline/consulta.aspx\" name=\"form\" onsubmit=carregabrw() target=\"shopline\">");
			out.println("<input type=\"hidden\" name=\"DC\" value=\"" + dados + "\">");
			out.println("<input type=\"submit\" name=\"Shopline\" value=\"Consulta ao Pagamento\">");
			out.println("</form>");
	}
}
