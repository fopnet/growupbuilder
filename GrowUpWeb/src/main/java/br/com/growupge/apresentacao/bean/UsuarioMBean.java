package br.com.growupge.apresentacao.bean;

import static br.com.growupge.util.MbeanUtil.getJsfSession;
import static br.com.growupge.util.MbeanUtil.isUsuarioConectado;
import br.com.growupge.constantes.GCS;
import br.com.growupge.dto.SenhaDTO;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.util.MbeanUtil;

/**
 * @author Felipe
 *
 */
/* JSF Only */
//@ManagedBean(name="usuarioMB")
//@javax.faces.bean.SessionScoped

//@javax.enterprise.context.SessionScoped
//@org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ConversationScoped
//@javax.faces.bean.ViewScoped  

//@org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ConversationScoped
//@javax.enterprise.inject.Alternative
//@javax.enterprise.inject.Default

/* CDI Scope & bean */
@javax.inject.Named("usuarioMB")
@javax.enterprise.context.SessionScoped // Necessario este escopo para o pr�-destroy
public class UsuarioMBean extends AutenticadorMBean {
	
	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1L;
	private String novaSenha;
	private String confirmaSenha;
	
	/**
	 * @return the novaSenha
	 */
	public String getNovaSenha() {
		return novaSenha;
	}
	
	/**
	 * @param novaSenha the novaSenha to set
	 */
	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}
	
	/**
	 * @return the confirmaSenha
	 */
	public String getConfirmaSenha() {
		return confirmaSenha;
	}

	/**
	 * @param confirmaSenha the confirmaSenha to set
	 */
	public void setConfirmaSenha(String confirmaSenha) {
		this.confirmaSenha = confirmaSenha;
	}

	/**
	 * @throws GrowUpException
	 */
	public void trocarSenha () throws GrowUpException  {
		try {
			if (!isUsuarioConectado()) {
				super.conectar(facade);
			}
		
			SenhaDTO senhaDTO = new SenhaDTO();
			senhaDTO.setUsuario(getUsuarioDTO());
			senhaDTO.setValor(getNovaSenha());
			
			SenhaDTO retorno = (SenhaDTO) facade.executarComando(getJsfSession(), 
																GCS.MAPA_USUARIO, 
																GCS.TROCAR_SENHA,
																senhaDTO);
			
			MbeanUtil.adicionarMensagemSucesso(retorno);
			
		} catch (GrowUpException e) {
			MbeanUtil.adicionarMensagemErro(e);
		}
		
	}

	public String lembrarSenha () {
		
		try {
			
			UsuarioDTO retorno = (UsuarioDTO) facade.executarComando(getJsfSession(), 
																GCS.MAPA_USUARIO, 
																GCS.RECRIAR_SENHA,
																getUsuarioDTO());
			
			MbeanUtil.adicionarMensagemSucesso(retorno);
			
			return MbeanUtil.voltarPaginaAnterior();
			
		} catch (GrowUpException e) {
			MbeanUtil.adicionarMensagemErro(e);
		}
		
		return "lembrarSenha";
	}

	protected UsuarioDTO getUsuarioDTO() {
		final UsuarioDTO usr = new UsuarioDTO();
		usr.setEmail(getEmail());
		usr.setSenha(getSenha());
		return usr;
	}
	

	
}
