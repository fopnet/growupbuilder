package br.com.growupge.apresentacao.bean.validator;

import java.util.Map;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.validate.ClientValidator;

import br.com.growupge.util.MbeanUtil;

/** 
 * Custom JSF Validator for Email input
 * @author Felipe 
 */  
@FacesValidator("custom.emailValidator")  
public class EmailValidator implements Validator, ClientValidator {  
  
	private static String EMAIL_VALIDATION_MESSAGE= "emailValidation";
    private Pattern pattern;  
//    private Matcher matcher;  
   
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"  
                                                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";  
   
    public EmailValidator() {  
        pattern = Pattern.compile(EMAIL_PATTERN);  
    }  
  
    @Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {  
        if(value == null) {  
            return;  
        }  
        //	Usu�rio: Erro de valida��o:          
        if(!pattern.matcher(value.toString()).matches()) {  
        	String msg = MbeanUtil.getLabelsResourceBundle(EMAIL_VALIDATION_MESSAGE);
        	FacesMessage fm = MbeanUtil.createFacesMessage(FacesMessage.SEVERITY_ERROR, msg); 
            throw new ValidatorException(fm);  
        }  
    }  
      
    @Override
	public Map<String, Object> getMetadata() {  
        return null;  
    }  
  
    @Override
	public String getValidatorId() {  
        return "custom.emailValidator";  
    }  
      
}