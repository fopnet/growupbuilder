package br.com.growupge.apresentacao.bean;

import static br.com.growupge.util.MbeanUtil.adicionarMensagemErro;
import static br.com.growupge.util.MbeanUtil.adicionarMensagemSucesso;
import static br.com.growupge.util.MbeanUtil.getJsfSession;
import static br.com.growupge.util.MbeanUtil.isUsuarioConectado;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.List;

import javax.annotation.PostConstruct;

import br.com.growupge.constantes.GCS;
import br.com.growupge.dto.MarcaDTO;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.dto.ServicoDTO;
import br.com.growupge.exception.GrowUpException;

/**
 * @author Felipe
 *
 */
/* JSF Only */
//@javax.faces.bean.ManagedBean(name="pedidoMBean")
//@javax.faces.bean.SessionScoped

//@javax.enterprise.context.SessionScoped
//@org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ConversationScoped
//@javax.faces.bean.ViewScoped  

/* Myfaces extension */
//@org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ConversationScoped
//@javax.enterprise.inject.Alternative
//@javax.enterprise.inject.Default

/* CDI Scope & bean */
@javax.inject.Named("pedidoMBean")
@javax.enterprise.context.SessionScoped //usando o pre-destroy por causa do escopo.
//@javax.faces.view.ViewScoped

/* Myfaces deltaspike  */
//@org.apache.deltaspike.core.api.scope.WindowScoped
//@javax.enterprise.context.Dependent
public class PedidoMBean extends AutenticadorMBean  {
	
	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1L;

	private PedidoVeiculoDTO pedido;
	
	@PostConstruct
	public void init() {
//	    FacesContext.getCurrentInstance().getExternalContext().getSession(true);
	}
	
	/**
	 * @return the pedido
	 */
	public PedidoVeiculoDTO getPedido() {
		if (pedido == null) {
			pedido = new PedidoVeiculoDTO();
			pedido.setServico(ServicoDTO.getPesquisaSimplesServico());
		}
		return pedido;
	}

	@SuppressWarnings("unchecked")
	public List<MarcaDTO> itensMarca(String query) {
		List<MarcaDTO> marcas = null;
		
		if (isNotBlank(query)) {
			MarcaDTO dto = new MarcaDTO();
			dto.setNome("%".concat(query).concat("%"));


			try {
				marcas = (List<MarcaDTO>) 
						facade.executarComando(getJsfSession(), 
								GCS.MAPA_MARCA, 
								GCS.BUSCAR_MARCAS, 
								dto);
			} catch (GrowUpException e) {
				adicionarMensagemErro(e);
			}
		}
		return marcas;
	}

	/////////////////////////////////////////////////////////////////////////////////////////
	// Funcionalidades
	/////////////////////////////////////////////////////////////////////////////////////////
	
	public String solicitar() {
//		if (!validarCamposObrigatorios()) 
//			return "pedido";
		
		try {
			if (!isUsuarioConectado()) {
				conectar(facade);
			}
			
			pedido = (PedidoVeiculoDTO) facade
					.executarComando(getJsfSession(), 
									GCS.MAPA_PEDIDO_VEICULO, 
									GCS.CRIAR_PEDIDO_VEICULO, 
									getPedido());
			
			adicionarMensagemSucesso(pedido);
			
			pedido = null;
			
			return "meusPedidos";

		} catch (GrowUpException e) {
			adicionarMensagemErro(e);
		}
		
		return "pedido";
	}
	
	/*
	protected boolean validarCamposObrigatorios() {
		int counter = 0;
		
		VeiculoDTO veiculo = getPedido().getVeiculo();
		if (isCodigoValido(veiculo.getMarca()))
			counter++;
		if (isNotBlank(veiculo.getChassi()))
			counter++;
		if (isNotBlank(veiculo.getCodigo()))
			counter++;
		if (isNotBlank(veiculo.getRenavam()))
			counter++;
		
		if (counter < 2) {
			adicionarMensagemAlerta("minimoNInfo", 2);
			return false;
		}
		
		return true;
	}*/
	
}
