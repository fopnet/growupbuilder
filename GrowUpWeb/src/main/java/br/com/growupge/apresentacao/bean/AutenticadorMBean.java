package br.com.growupge.apresentacao.bean;

import static br.com.growupge.constantes.Constantes.PARAM_EMAIL;
import static br.com.growupge.util.MbeanUtil.getParameterValue;
import static br.com.growupge.util.MbeanUtil.getUsuarioSessao;

import java.io.Serializable;

import javax.annotation.PreDestroy;

import org.apache.log4j.Logger;

import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.facade.GrowUpFacadeAdapter;
import br.com.growupge.util.MbeanUtil;

/**
 * Classe utilizadada pela tag lib autenticador
 * @author Felipe
 *
 */
public class AutenticadorMBean implements Serializable {
	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(AutenticadorMBean.class);
	
	@javax.inject.Inject
	protected GrowUpFacadeAdapter facade;
	
	private String email;
	private String senha;
	
	
	/////////////////////////////////////////////////////////////////////////////////////////
	// Componente conex�o
	/////////////////////////////////////////////////////////////////////////////////////////
	public String getUsuarioConectado() {
		UsuarioDTO user = getUsuarioSessao();
		if (user != null) 
			return user.getNome();
		
		return null;
	}
	
	/**
	 * @return the login
	 */
	public String getEmail() {
		if (getParameterValue(PARAM_EMAIL) != null)
			setEmail(getParameterValue(PARAM_EMAIL));
		
		return email;
	}
	
	public int getTamanhoMaximoEmail() {
		return UsuarioDTO.TAMANHO_MAXIMO_EMAIL;
	}

	/**
	 * @param login the login to set
	 */
	public void setEmail(String login) {
		this.email = login;
	}

	/**
	 * @return the senha
	 */
	public String getSenha() {
		return senha;
	}

	/**
	 * @param senha the senha to set
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}

	
	public void desconectar() {
		logger.info("desconectar executado");
		
		MbeanUtil.desconectar(MbeanUtil.getJsfSession(), facade);
	}

	protected void conectar(final GrowUpFacadeAdapter facade) throws GrowUpException {
		MbeanUtil.conectarPorEmail(getEmail(), getSenha(), facade);
	}

	/**
	 * Desconectar quando a sess�o expirar.
	 * A desconex�o � realizada por aqui, porque no sesionListener o contexto cdi j� n�o est� mais ativo.
	 */
	@PreDestroy
	public void desconectarListener() {
		desconectar();
	}

	
}
