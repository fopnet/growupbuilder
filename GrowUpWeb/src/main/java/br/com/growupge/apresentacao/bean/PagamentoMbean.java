package br.com.growupge.apresentacao.bean;

import static br.com.growupge.util.MbeanUtil.getJsfSession;

import java.io.IOException;
import java.io.Serializable;

import org.apache.log4j.Logger;

import br.com.growupge.dto.PagamentoDTO;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.facade.GrowUpFacadeAdapter;
import br.com.growupge.service.paypal.decorator.VeiculosPaymentDecorator;
import br.com.growupge.util.MbeanConstantes;
import br.com.growupge.util.MbeanUtil;
import br.com.growupge.util.SdkConfig;

import com.paypal.svcs.services.AdaptivePaymentsService;

/**
 * @author Felipe
 *
 */

@javax.inject.Named("pagamentoMBean")
@javax.enterprise.context.SessionScoped
public class PagamentoMbean implements Serializable  {

	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1L;

	private PedidoVeiculoDTO pedidoSelecionado;
	
	private static final Logger logger = Logger.getLogger(MeusPedidosMBean.class);
	
	@javax.inject.Inject
	private GrowUpFacadeAdapter facade;
	
	/////////////////////////////////////////////////////////////////////////////////////////
	// Getter and Setters
	/////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * @return the pagamentoSelecionado
	 */
	public PedidoVeiculoDTO getPedidoSelecionado() {
		return pedidoSelecionado;
	}
	
	/**
	 * @param value the pagamentoSelecionado to set
	 */
	public void setPedidoSelecionado(PedidoVeiculoDTO value) {
		this.pedidoSelecionado = value;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////
	// Funcionalidades
	/////////////////////////////////////////////////////////////////////////////////////////


	public void pagar() throws IOException {
		logger.info("Botao pagar "  + pedidoSelecionado.getCodigo() );

		try {

			PagamentoDTO pgto = new PagamentoDTO(pedidoSelecionado);
			
			System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
			
			AdaptivePaymentsService paypalService = new AdaptivePaymentsService(SdkConfig.getConfigurationMap());

			Double valorUnitario = getValorUnitario();

			pgto.setValor(valorUnitario);

			VeiculosPaymentDecorator service = new VeiculosPaymentDecorator(facade);

			pgto = service.solicitarPagamento(pgto, paypalService);

			String paymentUrl = SdkConfig.getPaykeyUrl(pgto.getCodigo());

			MbeanUtil.redirect(paymentUrl);

		} catch (GrowUpException e) {
			MbeanUtil.adicionarMensagemErro(e);
		} catch (Exception e) {
			MbeanUtil.adicionarMensagemErro(MbeanConstantes.MSG_ERRO, e.getMessage());
		}
	}
		
	/**
	 * @return
	 * @throws GrowUpException 
	 */
	public Double getValorUnitario() throws GrowUpException {
		TarifaBean tarifaBean = (TarifaBean) getJsfSession().getAttribute("tarifaBean");
		if (tarifaBean == null) {
			tarifaBean = new TarifaBean(getJsfSession(), facade);
		}
		return tarifaBean.getValorUnitario();
	}

}
