package br.com.growupge.apresentacao.bean.converter;

import static br.com.growupge.utility.Reflection.isCodigoValido;

import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.growupge.dto.MarcaDTO;

/**
 * @author Felipe
 *
 */
@FacesConverter("marcaConverter")
public class MarcaConverter implements Converter{
 
	@Override
	public Object getAsObject(FacesContext ctx, UIComponent component, String value) {  
        if (value != null) {  
            return this.getAttributesFrom(component).get(value);  
        }  
        return null;  
    }  
  
    @Override
	public String getAsString(FacesContext ctx, UIComponent component, Object value) {  

    	MarcaDTO entity = (MarcaDTO) value;  
    	
        if (isCodigoValido(entity)) {  
  
            // adiciona item como atributo do componente  
            this.addAttribute(component, entity);  
  
            Long codigo = entity.getCodigo();  
            if (codigo != null) {  
                return String.valueOf(codigo);  
            }  
        }  
  
        return null;  
    }  
  
    protected void addAttribute(UIComponent component, MarcaDTO o) {  
    	if (o.getCodigo() != null) {
	        String key = o.getCodigo().toString();  
	        this.getAttributesFrom(component).put(key, o);
    	}
    }  
  
    protected Map<String, Object> getAttributesFrom(UIComponent component) {  
        return component.getAttributes();  
    }  
}