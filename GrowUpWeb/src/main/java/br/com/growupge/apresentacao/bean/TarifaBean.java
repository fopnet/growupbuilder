package br.com.growupge.apresentacao.bean;

import static br.com.growupge.utility.Reflection.isCodigoValido;

import java.io.Serializable;

import javax.servlet.http.HttpSession;

import br.com.growupge.constantes.GCS;
import br.com.growupge.dto.VariavelDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.facade.GrowUpFacadeAdapter;
import br.com.growupge.util.MbeanUtil;

/**
 * @author Felipe
 *
 */
//@ManagedBean(name="tarifaBean")
//@javax.faces.bean.SessionScoped
  
@javax.inject.Named(value="tarifaBean")
@javax.enterprise.context.SessionScoped
public class TarifaBean implements Serializable {
	
	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1L;

//	private static final Logger logger = Logger.getLogger(TarifaBean.class);

	private transient HttpSession session;
	
	@javax.inject.Inject
	private GrowUpFacadeAdapter facade;
	
	private Double valorUnitario;

	public TarifaBean() {}
	
	public TarifaBean(HttpSession session, GrowUpFacadeAdapter facade){
		this.session = session;

		if (session == null)
			throw new IllegalArgumentException("Session n�o pode ser nula");
		
		session.setAttribute("tarifaBean", this);

		this.facade = facade;
		if (facade == null)
			throw new IllegalArgumentException("Facade n�o pode ser nula");
	}
	
	/**
	 * @param facade the facade to set
	 */
	public void setFacade(GrowUpFacadeAdapter facade) {
		this.facade = facade;
	}

	/**
	 * @param session the session to set
	 */
	public void setSession(HttpSession session) {
		this.session = session;
	}

	/**
	 * Aqui � feito um tipo de cache de sess�o para evitar ida ao banco.
	 * @return the valorUnitario
	 * @throws GrowUpException 
	 */
	public Double getValorUnitario() throws GrowUpException {
		if (valorUnitario==null)
			valorUnitario = buscarValorPedido();
		else if (MbeanUtil.isUsuarioAdmin()) {
			return VariavelDTO.VALOR_UNITARIO_ADMIN.getValorAsDouble();
		}
		return valorUnitario;
	}

	/**
	 * @param valorUnitario the valorUnitario to set
	 */
	public void setValorUnitario(Double valorUnitario) {
		this.valorUnitario = valorUnitario;
	}
	
	/**
	 * @return
	 * @throws GrowUpException
	 */
	private Double buscarValorPedido() throws GrowUpException {
		VariavelDTO var = (VariavelDTO) facade.executarComando(session, 
													GCS.MAPA_VARIAVEL, 
													GCS.BUSCAR_VALOR_UNITARIO);
		if (!isCodigoValido(var))
			throw new IllegalArgumentException("N�o foi poss�vel encontrar o valor Unit�rio do pedido."); 
			
		return var.getValorAsDouble();
	}

	
}
