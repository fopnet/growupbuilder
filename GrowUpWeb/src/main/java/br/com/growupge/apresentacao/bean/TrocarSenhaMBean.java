package br.com.growupge.apresentacao.bean;

import java.io.Serializable;


/* JSF */
//@ManagedBean(name="trocarSenhaMB")

/* CDI Scope & bean */
@javax.inject.Named("trocarSenhaMB")
@javax.enterprise.context.SessionScoped
public class TrocarSenhaMBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String senha;
	private String novaSenha;
	private String confirmaSenha;
	
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNovaSenha() {
		return novaSenha;
	}
	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}
	public String getConfirmaSenha() {
		return confirmaSenha;
	}
	public void setConfirmaSenha(String confirmaSenha) {
		this.confirmaSenha = confirmaSenha;
	}
	
	

}
