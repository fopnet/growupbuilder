package br.com.growupge.apresentacao.bean;

import static br.com.growupge.util.MbeanUtil.getJsfSession;
import static br.com.growupge.util.MbeanUtil.getLabelsResourceBundle;
import static br.com.growupge.util.MbeanUtil.getUsuarioSessao;
import static br.com.growupge.util.MbeanUtil.isUsuarioConectado;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.log4j.Logger;

import br.com.growupge.constantes.GCS;
import br.com.growupge.dto.PagamentoDTO;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.service.paypal.decorator.VeiculosPaymentDecorator;
import br.com.growupge.util.MbeanConstantes;
import br.com.growupge.util.MbeanUtil;
import br.com.growupge.util.SdkConfig;

import com.paypal.svcs.services.AdaptivePaymentsService;

/**
 * @author Felipe
 *
 */

/* JSF Only */
//@ManagedBean(name="meusPedidosMBean")
//@javax.faces.bean.ViewScoped //Nao retirar este scope por causa do init

/* Myfaces extension */
//@javax.inject.Named
//@org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ViewAccessScoped

/* Tomee specification */
//@javax.enterprise.inject.Alternative

/* Myfaces deltaspike  */
@javax.inject.Named("meusPedidosMBean")
//@org.apache.deltaspike.core.api.scope.ViewAccessScoped
@javax.faces.view.ViewScoped //Nao retirar este scope por causa da execucao do pos-contruct apos criar pedido. 
public class MeusPedidosMBean extends AutenticadorMBean  { 
	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1675134238554849781L;

	private static final Logger logger = Logger.getLogger(MeusPedidosMBean.class);

	private List<PedidoVeiculoDTO> meusPedidos = new ArrayList<PedidoVeiculoDTO>();
	
	private PedidoVeiculoDTO pedidoSelecionado;
	
	@PostConstruct
	public void init() {
		listar();// Para este listar funcionar � preciso do ViewScoped
	}
	
	public void onPageLoad() {
//		if (isUsuarioConectado()) {
//			RequestContext.getCurrentInstance().update("dtPedidos");
//		}
	}

	
	/////////////////////////////////////////////////////////////////////////////////////////
	// Getter and Setters
	/////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * @return the pagamentoSelecionado
	 */
	public PedidoVeiculoDTO getPedidoSelecionado() {
		return pedidoSelecionado;
	}


	/**
	 * @param pagamentoSelecionado the pagamentoSelecionado to set
	 */
	public void setPedidoSelecionado(PedidoVeiculoDTO pagamentoSelecionado) {
		this.pedidoSelecionado = pagamentoSelecionado;
	}
	
	/**
	 * @return the meusPedidos
	 */
	public List<PedidoVeiculoDTO> getMeusPedidos() {
		return meusPedidos;
	}


	/////////////////////////////////////////////////////////////////////////////////////////
	// Bundles
	/////////////////////////////////////////////////////////////////////////////////////////
	public String getMensagemGrid() {
		return getUsuarioSessao() != null ? getLabelsResourceBundle("nenhumRegistro") : getLabelsResourceBundle("conexaoRequerida");
	}

	/////////////////////////////////////////////////////////////////////////////////////////
	// Funcionalidades
	/////////////////////////////////////////////////////////////////////////////////////////


	public void pagar() throws IOException {
		logger.info("Botao pagar "  + pedidoSelecionado.getCodigo() );

		try {
			PagamentoDTO pgto = new PagamentoDTO(pedidoSelecionado);
			
			AdaptivePaymentsService paypalService = new AdaptivePaymentsService(SdkConfig.getConfigurationMap());

			Double valorUnitario = getValorUnitario();

			pgto.setValor(valorUnitario);

			VeiculosPaymentDecorator service = new VeiculosPaymentDecorator(facade);

			pgto = service.solicitarPagamento(pgto, paypalService);

			String paymentUrl = SdkConfig.getPaykeyUrl(pgto.getCodigo());

			MbeanUtil.redirect(paymentUrl);

		} catch (GrowUpException e) {
			MbeanUtil.adicionarMensagemErro(e);
		} catch (Exception e) {
			MbeanUtil.adicionarMensagemErro(MbeanConstantes.MSG_ERRO, e.getMessage());
		}

		/*
			PayRequest req = PayPalDTOBuilder.buildPayRequest(getPedidoSelecionado());
			
			// Creating service wrapper object to make an API call by loading
			VeiculosAdaptivePaymentsService service = new VeiculosAdaptivePaymentsService();
			try {
			PaymentDetailsResponse resp = service.requestPayment(req);
			
			if (AckCode.SUCCESS.equals(resp.getResponseEnvelope().getAck())) {
			
			// producao https://www.paypal.com/cgi-bin/webscr?
			String paymentUrl = SdkConfig.getPaykeyUrl(resp.getPayKey());
			
			//MbeanUtil.forward(paymentUrl);
			MbeanUtil.redirect(paymentUrl);
			
			} else {
			MbeanUtil.adicionarMensagemErro(service.getErrorMessage(resp.getError(), 
			"Ack:" + resp.getResponseEnvelope().getAck().toString()));
			}


			} catch (Exception err) {
			logger.error(err.getMessage());
			MbeanUtil.adicionarMensagemErro(err.getMessage());
			}
			*/
	}

	/**
	 * @return
	 * @throws GrowUpException 
	 */
	public Double getValorUnitario() throws GrowUpException {
		TarifaBean tarifaBean = (TarifaBean) getJsfSession().getAttribute("tarifaBean");
		if (tarifaBean == null) {
			 tarifaBean = new TarifaBean(getJsfSession(), facade);
		}
		return tarifaBean.getValorUnitario();
	}

	public void listar() {
		
		try {
			if (!isUsuarioConectado()) {
				super.conectar(facade);
			}

			PedidoVeiculoDTO pedido = new PedidoVeiculoDTO();
			pedido.setUsuarioCadastro(getUsuarioSessao());
//			PagamentoDTO pgto = new PagamentoDTO(pedido);

			meusPedidos = facade.executarComandoLista(getJsfSession(), 
														GCS.MAPA_PEDIDO_VEICULO, 
														GCS.BUSCAR_PAGAMENTOS_PEDIDOS,
														pedido);
		}  catch (GrowUpException ge ) {
			MbeanUtil.adicionarMensagemErro(ge);
		}
	}

	/* Sobrescrito por causa do contexto � de Vis�o.
	 * @see br.com.growupge.apresentacao.bean.AutenticadorMBean#desconectar()
	 */
	@Override
	@PreDestroy
	public void desconectarListener() {
		// Faz nada.
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.apresentacao.bean.AutenticadorMBean#desconectar()
	 */
	public void desconectar() {
		try {
			super.desconectar();
		} finally {
			meusPedidos.clear();
		}

	}
	
}
