/**
 * 
 */
package br.com.growupge.service.paypal.decorator;

import static br.com.growupge.service.paypal.builder.PayPalDTOBuilder.buildPagamentoDTO;
import static br.com.growupge.service.paypal.builder.PayPalDTOBuilder.isDuplicateTrackingIdError;
import static br.com.growupge.util.MbeanUtil.getJsfSession;
import br.com.growupge.constantes.GCS;
import br.com.growupge.dto.PagamentoDTO;
import br.com.growupge.facade.GrowUpFacadeAdapter;
import br.com.growupge.service.paypal.adapter.PaymentResponseAdapter;
import br.com.growupge.service.paypal.builder.PayPalDTOBuilder;

import com.paypal.svcs.services.AdaptivePaymentsService;
import com.paypal.svcs.types.ap.PayRequest;
import com.paypal.svcs.types.ap.PayResponse;

/**
 * @author Felipe
 *
 */
public class VeiculosPaymentDecorator extends AbstractPaymentDecorator<PaymentResponseAdapter> {
	
	/**
	 * @param facade
	 */
	public VeiculosPaymentDecorator(GrowUpFacadeAdapter facade) {
		super(facade);
		super.adicionarTemplateService(new VeiculosPaymentDetailDecorator(facade));
	}

	@Override
	public PagamentoDTO solicitarPagamento(final PagamentoDTO objPgto, 
													  final AdaptivePaymentsService paypalService,
													  final Object... parentRequests) throws Exception {
		PagamentoDTO pgto = objPgto;
		
		PayRequest payRequest = PayPalDTOBuilder.buildPayRequest(pgto);
		
		PayResponse resp = paypalService.pay(payRequest);
		
		PaymentResponseAdapter veiculoPaymentResponse = new PaymentResponseAdapter(resp);

		PagamentoDTO retorno = objPgto;// caso existe erro, que n�o seja dispar�vel
		 if (!isSolicitacaoConcluida(veiculoPaymentResponse)) {
			 if (isDuplicateTrackingIdError(veiculoPaymentResponse))
				 retorno = objPgto;// se o erro for duplicaco do trancking, o response nao contem dado do pedido.
			 else
				 dispararExcecao(veiculoPaymentResponse);
		 } else // Caso n�o exista erro, criar o pagamento de acordo com a resposta 
			 retorno = buildPagamentoDTO(veiculoPaymentResponse);
		

		 // Solicita os detalhes do pagamento
		 retorno = super.solicitarPagamento(retorno, paypalService, payRequest); 

		 return (PagamentoDTO) facade.executarComando(getJsfSession(), 
				 GCS.MAPA_PAGAMENTO, 
				 GCS.SOLICITAR_PAGAMENTO, 
				 retorno);
	}

	
	
}
