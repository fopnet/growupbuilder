package br.com.growupge.service.paypal.decorator;

import static br.com.growupge.util.MbeanUtil.getJsfSession;
import br.com.growupge.constantes.GCS;
import br.com.growupge.dto.PagamentoDTO;
import br.com.growupge.facade.GrowUpFacadeAdapter;
import br.com.growupge.service.paypal.adapter.PaymentDetailResponseAdapter;
import br.com.growupge.service.paypal.adapter.VeiculosPaymentResponse;
import br.com.growupge.service.paypal.builder.PayPalDTOBuilder;

import com.paypal.svcs.services.AdaptivePaymentsService;
import com.paypal.svcs.types.ap.PaymentDetailsRequest;
import com.paypal.svcs.types.ap.PaymentDetailsResponse;

/**
 * @author Felipe
 *
 */
public class VeiculosPaymentDetailDecorator extends AbstractPaymentDecorator<PaymentDetailResponseAdapter> {

	/**
	 * @param facade
	 */
	public VeiculosPaymentDetailDecorator(GrowUpFacadeAdapter facade) {
		super(facade);
	}

	@Override
	public PagamentoDTO solicitarPagamento(final PagamentoDTO dto, 
										  final AdaptivePaymentsService paypalService, 
										  final Object... parentRequests) throws Exception {
//		PayResponse response = (PayResponse) request;
//		PayRequest payRequest = (PayRequest) parentRequests[0];
		
//		PaymentDetailsRequest detailReq = PayPalDTOBuilder.buildPaymentDetails(response, payRequest); 
		PaymentDetailsRequest detailReq = PayPalDTOBuilder.buildPaymentDetails(dto); 
		
		PaymentDetailsResponse detailResp = paypalService.paymentDetails(detailReq);
		
		VeiculosPaymentResponse veiculoPaymentResponse = new PaymentDetailResponseAdapter(detailResp);
		
		PagamentoDTO retorno = null;
		
		 if (!isSolicitacaoConcluida(veiculoPaymentResponse)) {
			
			 if (PayPalDTOBuilder.isStatusExpired(veiculoPaymentResponse)) {
				
				 // Se j� estiver expirado a solicitacao de pagamanto, eu tenho no objeto os dados.
				 retorno = PayPalDTOBuilder.buildPagamentoDTO(detailResp);
				
				facade.executarComando(getJsfSession(), 
						GCS.MAPA_PAGAMENTO, 
						GCS.CONFIRMAR_PAGAMENTO, 
						retorno);
			 }
					
			dispararExcecao(veiculoPaymentResponse);
			
		 } else // Se a solicita��o for nova, teremos os dados completos na resposta.
			 retorno = PayPalDTOBuilder.buildPagamentoDTO(detailResp);
		 
		return retorno;
//		return new PaymentDetailResponseAdapter(detailResp);
//		return super.solicitarPagamento(detailResp, paypalService, payRequest, detailReq);
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.service.paypal.decorator.AbstractPaymentDecorator#isSolicitacaoConcluida(br.com.growupge.service.paypal.adapter.VeiculosPaymentResponse)
	 */
	@Override
	public boolean isSolicitacaoConcluida(VeiculosPaymentResponse response) {
		boolean isResult = super.isSolicitacaoConcluida(response);
		
		return isResult && !PayPalDTOBuilder.isStatusExpired(response);
	}
	
	
}
