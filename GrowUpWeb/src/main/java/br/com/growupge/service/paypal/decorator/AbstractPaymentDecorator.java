package br.com.growupge.service.paypal.decorator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import br.com.growupge.dto.PagamentoDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.facade.GrowUpFacadeAdapter;
import br.com.growupge.service.paypal.adapter.VeiculosPaymentResponse;
import br.com.growupge.service.paypal.builder.PayPalDTOBuilder;

import com.paypal.svcs.services.AdaptivePaymentsService;
import com.paypal.svcs.types.common.AckCode;
import com.paypal.svcs.types.common.ErrorData;

/**
 * @author Felipe
 *
 */
public abstract class AbstractPaymentDecorator<ResponseType extends VeiculosPaymentResponse> {

	protected List<AbstractPaymentDecorator<? extends VeiculosPaymentResponse>> decorators = new ArrayList<AbstractPaymentDecorator<? extends VeiculosPaymentResponse>>(); 
	
	protected GrowUpFacadeAdapter facade;
	
	/**
	 * @param facade
	 */
	public AbstractPaymentDecorator(GrowUpFacadeAdapter facade) {
		this.facade = facade;
	}

	public AbstractPaymentDecorator<? extends VeiculosPaymentResponse> adicionarTemplateService(AbstractPaymentDecorator<? extends VeiculosPaymentResponse> service) {
		 decorators.add(service);
		 return service; 
	}
	
	/**
	 * Template method 
	 * 
	 * @param request
	 * @param paypalService
	 * @param parentRequests
	 * @return
	 * @throws Exception
	 */
	public PagamentoDTO solicitarPagamento(final PagamentoDTO dto ,
													  final AdaptivePaymentsService paypalService,
													  final Object... parentRequests) throws Exception {
//		VeiculosPaymentResponse response = null; 
//		Object objResponse = request;
		PagamentoDTO response = null;
		
		 Iterator<AbstractPaymentDecorator<? extends VeiculosPaymentResponse>> it = decorators.iterator();
		 while (it.hasNext()) {
			 AbstractPaymentDecorator<? extends VeiculosPaymentResponse> service = it.next();
//			 objResponse = service.solicitarPagamento(objResponse, paypalService, parentRequests);
			 response = service.solicitarPagamento(dto, paypalService, parentRequests);
			 
//			 response = (VeiculosPaymentResponse) objResponse;
			 
//			 if (!service.isSolicitacaoConcluida(response))
//				service.dispararExcecao(response);
			 
			 if (!it.hasNext()) {
				 it = service.decorators.iterator();
			 }
		}
		
		return response;
	}	
	
	/**
	 * Template method
	 * 
	 * @param response
	 * @return
	 */
	public boolean isSolicitacaoConcluida(VeiculosPaymentResponse response) {
		// Skipping for Implicit Payments
		return AckCode.SUCCESS.equals(response.getResponseEnvelope().getAck())
				&& !PayPalDTOBuilder.isStatusError(response) ;
	}

	/**
	 * Template method
	 * 
	 * @param response
	 * @throws GrowUpException
	 */
	public void dispararExcecao(VeiculosPaymentResponse response) throws GrowUpException {
		throw new GrowUpException(ErroInterno.ERRO_TRANSACIONAL, 
				  getErrorMessage(response.getError(), 
				  ("Resposta do paypal:").concat(response.getResponseEnvelope().getAck().toString())));
	}

	/**
	 * @param erros 
	 * @return
	 */
	private String getErrorMessage(List<ErrorData> erros, String msg) {
		StringBuilder sb = new StringBuilder(msg);
		sb.append("\t");
		for (ErrorData err : erros) {
			sb.append(err.getMessage());
		}
		return sb.toString();
	}
	 
	
}
