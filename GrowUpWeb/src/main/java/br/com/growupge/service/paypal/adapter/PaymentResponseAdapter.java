/**
 * 
 */
package br.com.growupge.service.paypal.adapter;

import java.util.List;

import com.paypal.svcs.types.ap.PayResponse;
import com.paypal.svcs.types.common.ErrorData;
import com.paypal.svcs.types.common.ResponseEnvelope;

/**
 * @author Felipe
 *
 */
public class PaymentResponseAdapter implements VeiculosPaymentResponse {

	private PayResponse response;
	
	public PaymentResponseAdapter(PayResponse response) {
		this.response = response;
		
	}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.service.paypal.adapter.VeiculosPaymentResponse#getStatus()
	 */
	@Override
	public String getStatus() {
		return response.getPaymentExecStatus();
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.service.paypal.adapter.VeiculosPaymentResponse#getResponseEnvelope()
	 */
	@Override
	public ResponseEnvelope getResponseEnvelope() {
		return response.getResponseEnvelope();
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.service.paypal.adapter.VeiculosPaymentResponse#getError()
	 */
	@Override
	public List<ErrorData> getError() {
		return response.getError();
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.service.paypal.adapter.VeiculosPaymentResponse#getPayKey()
	 */
	@Override
	public String getPayKey() {
		return response.getPayKey();
	}

}
