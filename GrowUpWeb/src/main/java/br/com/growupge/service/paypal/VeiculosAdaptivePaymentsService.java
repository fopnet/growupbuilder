package br.com.growupge.service.paypal;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.growupge.constantes.GCS;
import br.com.growupge.dto.ComandoDTO;
import br.com.growupge.dto.PagamentoDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.facade.GrowUpFacadeAdapter;
import br.com.growupge.factory.BeanFactory;
import br.com.growupge.service.paypal.adapter.PaymentDetailResponseAdapter;
import br.com.growupge.service.paypal.adapter.PaymentResponseAdapter;
import br.com.growupge.service.paypal.builder.PayPalDTOBuilder;
import br.com.growupge.service.paypal.decorator.VeiculosPaymentDecorator;
import br.com.growupge.util.MbeanUtil;
import br.com.growupge.util.SdkConfig;

import com.paypal.exception.ClientActionRequiredException;
import com.paypal.exception.HttpErrorException;
import com.paypal.exception.InvalidCredentialException;
import com.paypal.exception.InvalidResponseDataException;
import com.paypal.exception.MissingCredentialException;
import com.paypal.exception.SSLConfigurationException;
import com.paypal.sdk.exceptions.OAuthException;
import com.paypal.svcs.services.AdaptivePaymentsService;
import com.paypal.svcs.types.ap.PayRequest;
import com.paypal.svcs.types.ap.PayResponse;
import com.paypal.svcs.types.ap.PaymentDetailsRequest;
import com.paypal.svcs.types.ap.PaymentDetailsResponse;
import com.paypal.svcs.types.common.AckCode;
import com.paypal.svcs.types.common.ErrorData;

/**
 * @author Felipe
 * Esta implementacao foi refatoracao {@link VeiculosPaymentDecorator}
 */
@Deprecated
public class VeiculosAdaptivePaymentsService extends AdaptivePaymentsService {

	private static final Logger logger = Logger.getLogger(VeiculosAdaptivePaymentsService.class);
	
/*	@javax.inject.Inject
	@br.com.growupge.facade.Facade
	private GrowUpFacadeAdapter facade;*/
	
	/**
	 * Construtor que recupera as propriedades
	 * @throws IOException
	 */
	public VeiculosAdaptivePaymentsService() throws IOException {
		super(SdkConfig.getConfigurationMap());
	}

	/* (non-Javadoc)
	 * @see com.paypal.svcs.services.AdaptivePaymentsService#pay(com.paypal.svcs.types.ap.PayRequest)
	 */
	public PaymentDetailsResponse requestPayment(PayRequest payRequest) throws GrowUpException, SSLConfigurationException, InvalidCredentialException, UnsupportedEncodingException, HttpErrorException, InvalidResponseDataException, ClientActionRequiredException, MissingCredentialException, OAuthException, IOException, InterruptedException {

		PayResponse resp = super.pay(payRequest);
		
		// Skipping for Implicit Payments
		if (AckCode.SUCCESS.equals(resp.getResponseEnvelope().getAck())
				&& !PayPalDTOBuilder.isStatusCompleted(new PaymentResponseAdapter(resp))) {

			PaymentDetailsRequest detailReq = PayPalDTOBuilder.buildPaymentDetails(resp, payRequest); 
				
			PaymentDetailsResponse detailResp = super.paymentDetails(detailReq);
				
			if (AckCode.SUCCESS.equals(detailResp.getResponseEnvelope().getAck()) 
					&& !PayPalDTOBuilder.isStatusCompleted(new PaymentDetailResponseAdapter(detailResp))) { 

				salvarSolicitacaoPagamento(detailResp);
				
				return detailResp;
			}
		} 

		throw new GrowUpException(ErroInterno.ERRO_TRANSACIONAL, 
								  getErrorMessage(resp.getError(), 
								  "Ack:".concat(resp.getResponseEnvelope().getAck().toString())));
	}

	protected void salvarSolicitacaoPagamento(PaymentDetailsResponse detailResp)
			throws InterruptedException, GrowUpException {
		PagamentoDTO pg = PayPalDTOBuilder.buildPagamentoDTO(detailResp);
		
		ComandoDTO cmd = new ComandoDTO();
		cmd.setSID(MbeanUtil.getSID());
		cmd.setMapaGCS(GCS.MAPA_PAGAMENTO);
		cmd.setPermissao(GCS.SOLICITAR_PAGAMENTO);
		cmd.getParametrosLista().add(pg);
		
		GrowUpFacadeAdapter facade = BeanFactory.getContextualInstance("facade");//FlexFactoryImpl.getFacade();
		try {
			facade.executarComando(MbeanUtil.getJsfSession(), 
									GCS.MAPA_PAGAMENTO, 
									GCS.SOLICITAR_PAGAMENTO, pg);
		} catch (GrowUpException e) {
			logger.error(e.getMensagem());
//			throw new InterruptedException(e.getMensagem());
		}
	}

	/**
	 * @param erros 
	 * @return
	 */
	public String getErrorMessage(List<ErrorData> erros, String msg) {
		StringBuilder sb = new StringBuilder(msg);
		sb.append("\t");
		for (ErrorData err : erros) {
			sb.append(err.getMessage());
		}
		return sb.toString();
	}
	 
	
	
	 

}