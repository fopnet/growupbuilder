package br.com.growupge.service.paypal.builder;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.growupge.dto.PagamentoDTO;
import br.com.growupge.dto.PagamentoPayPalDTO;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.dto.StatusPedido;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.service.paypal.adapter.PaymentDetailResponseAdapter;
import br.com.growupge.service.paypal.adapter.VeiculosPaymentResponse;
import br.com.growupge.util.SdkConfig;
import br.com.growupge.utility.DateUtil;

import com.paypal.ipn.IPNMessage;
import com.paypal.svcs.types.ap.PayRequest;
import com.paypal.svcs.types.ap.PayResponse;
import com.paypal.svcs.types.ap.PaymentDetailsRequest;
import com.paypal.svcs.types.ap.PaymentDetailsResponse;
import com.paypal.svcs.types.ap.PaymentInfo;
import com.paypal.svcs.types.ap.Receiver;
import com.paypal.svcs.types.ap.ReceiverList;
import com.paypal.svcs.types.common.RequestEnvelope;

/**
 * @author Felipe
 *
 */
public class PayPalDTOBuilder {
	/**
	 *  Atributo '<code>PAYPAL_ERROR_DUPLICATE_TRACKING_ID</code>' do tipo int
	 */
	private static final int PAYPAL_ERROR_DUPLICATE_TRACKING_ID = 579042;
	private static final String STATUS_ERROR = "Error";
	private static final String STATUS_PAGAMENTO_COMPLETO = "Completed";
	private static final String STATUS_PAGAMENTO_EXPIRADO = "Expired";
	private static final int PERIODO_VALIDADE_CHAVE = PagamentoDTO.PERIODO_VALIDADE_CHAVE; //dias
	private static final String PERIODO_DATA_TYPE = "P" + PERIODO_VALIDADE_CHAVE+ "D";

	
	public static PagamentoDTO buildPagamentoDTO(VeiculosPaymentResponse resp) {
		PagamentoPayPalDTO pag = new PagamentoPayPalDTO(new PedidoVeiculoDTO());
		pag.setCodigo(resp.getPayKey());
		pag.setCorrelacaoId(resp.getResponseEnvelope().getCorrelationId());
		pag.setDataSolicitacao( resp.getResponseEnvelope().getTimestamp() );
		pag.setStatus ( resp.getStatus() );
		pag.setResposta(resp.getResponseEnvelope().getAck().getValue());
//		pag.setTransacaoId( resp.getPaymentInfoList().getPaymentInfo().get(0).getTransactionId() );
//		pag.setTransacaoStatus( resp.getPaymentInfoList().getPaymentInfo().get(0).getTransactionStatus() );
		
		return pag;
	}

	public static PagamentoDTO buildPagamentoDTO(PaymentDetailsResponse resp) throws GrowUpException {
		PagamentoPayPalDTO pag = new PagamentoPayPalDTO(new PedidoVeiculoDTO(Long.parseLong(resp.getTrackingId())));
		pag.setCodigo(resp.getPayKey());
		pag.setCorrelacaoId(resp.getResponseEnvelope().getCorrelationId());
		pag.setDataSolicitacao( resp.getResponseEnvelope().getTimestamp() );
		pag.setStatus ( resp.getStatus() );
		pag.setResposta(resp.getResponseEnvelope().getAck().getValue());
		
		if (isStatusExpired(new PaymentDetailResponseAdapter(resp))) {
//			setStatus(StatusPedido.EXPIRADO.toString());
			pag.getPedido().getStatus().expirar( pag.getPedido() ); 
		}
		
//		pag.setTrackingId( resp.getTrackingId() );
		pag.setDescricao ( resp.getMemo() );
		
		PaymentInfo paymentInfo = resp.getPaymentInfoList().getPaymentInfo().get(0);
		if (paymentInfo != null) {
			pag.setTransacaoId( paymentInfo.getTransactionId() );
			pag.setTransacaoStatus( paymentInfo.getTransactionStatus() );
			pag.setValor( paymentInfo.getReceiver().getAmount() );
		}
		
		return pag;
	}

	/**
	 * @param ipnlistener
	 * @return
	 * @throws GrowUpException 
	 */
	public static PagamentoDTO buildPagamentoDTO(IPNMessage ipnlistener) throws GrowUpException {
		PagamentoDTO pag = new PagamentoPayPalDTO(new PedidoVeiculoDTO(Long.parseLong(ipnlistener.getIpnValue("tracking_id"))));
		
		pag.setCodigo(ipnlistener.getIpnValue("pay_key"));
//		pag.setDataPagamento(ipnlistener.getIpnValue("payment_request_date"));
		pag.setDataPagamento(DateUtil.getDateManagerInstance().getDateTime());
		pag.setStatus(ipnlistener.getIpnValue("status"));
		pag.setTransacaoId(ipnlistener.getIpnValue("transaction[0].id"));
		pag.setCorrelacaoId(ipnlistener.getIpnValue("transaction[0].id_for_sender_txn"));
		pag.setTransacaoStatus(ipnlistener.getIpnValue("transaction[0].status"));
		pag.setResposta(ipnlistener.getIpnValue("transaction[0].status_for_sender_txn"));
		pag.setDescricao(ipnlistener.getIpnValue("memo"));
		
		String valor = ipnlistener.getIpnValue("transaction[0].amount"); //"BRL 25.00"
		if (isNotBlank(valor)) {
			pag.setValor(getAmountIPNService(valor));
		}
		
		StatusPedido.PENDENTE.pendente(pag.getPedido());
		if (STATUS_PAGAMENTO_COMPLETO.equalsIgnoreCase(pag.getStatus()))
//			pag.getPedido().setStatus(StatusPedido.FATURADO.toString());
			pag.getPedido().getStatus().pagar( pag.getPedido() );
		else if (STATUS_PAGAMENTO_EXPIRADO.equalsIgnoreCase(pag.getStatus()))  
//			pag.getPedido().setStatus(StatusPedido.EXPIRADO.toString());
			pag.getPedido().getStatus().expirar( pag.getPedido() );
		
//		if (ipnlistener.getIpnValue("tracking_id")!= null)
//			pag.setTrackingId(ipnlistener.getIpnValue("tracking_id"));
		return pag;
	}

	/**
	 * Extrair o BRL da quantia
	 * Ex: BRL 25.00
	 * @param valor
	 * @return
	 */
	protected static Double getAmountIPNService(String valor) {
		Pattern onlyNumbers = Pattern.compile("(\\d(\\.\\d)?)+");
		Matcher matcher = onlyNumbers.matcher(valor);
		if (matcher.find())
			return Double.valueOf(matcher.group());
			
		return null;
	}

	public static PayRequest buildPayRequest(PagamentoDTO pagamento) {
		PayRequest req = new PayRequest();
		
		/** (Required) RFC 3066 language in which error messages are returned; 
		 * by default it is en_US, which is the only language currently supported. */
		RequestEnvelope requestEnvelope = new RequestEnvelope("en_US");
		req.setRequestEnvelope(requestEnvelope);
		req.setTrackingId(String.valueOf(pagamento.getPedido().getCodigo()));
		req.setCurrencyCode("BRL");
		req.setActionType("PAY");
		req.setCancelUrl(SdkConfig.getCancelUrl());
		req.setReturnUrl(SdkConfig.getReturnUrl() + pagamento.getPedido().getCodigo());
		req.setIpnNotificationUrl(SdkConfig.getIPNListenerURL());
		req.setSenderEmail(pagamento.getPedido().getUsuarioCadastro().getEmail());
		req.setMemo(pagamento.getDescricao());
		
		//	http://www.w3schools.com/schema/schema_dtypes_date.asp
		req.setPayKeyDuration(PERIODO_DATA_TYPE);
		
		/** (Required) Amount to be paid to the receiver */
		Receiver receiver = new Receiver(pagamento.getValor());

		/**
		 * Receiver's email address. This address can be unregistered with
		 * paypal.com. If so, a receiver cannot claim the payment until a PayPal
		 * account is linked to the email address. The PayRequest must pass
		 * either an email address or a phone number. Maximum length: 127
		 * characters
		 */
		receiver.setEmail(SdkConfig.getEmailReceiver());

		List<Receiver> receiverList = new ArrayList<Receiver>();
		receiverList.add(receiver);
 		req.setReceiverList(new ReceiverList(receiverList));
		
		return req;
	}

	/**
	 * @param resp
	 * @param payRequest 
	 * @return
	 */
	@Deprecated
	public static PaymentDetailsRequest buildPaymentDetails(PayResponse resp, PayRequest payRequest) {
		PaymentDetailsRequest detailReq = new PaymentDetailsRequest();
		detailReq.setRequestEnvelope(new RequestEnvelope("en_US"));
		detailReq.setPayKey(resp.getPayKey());
		detailReq.setTrackingId(payRequest.getTrackingId());
		return detailReq;
	}

	public static PaymentDetailsRequest buildPaymentDetails(PagamentoDTO dto) {
		PaymentDetailsRequest detailReq = new PaymentDetailsRequest();
		detailReq.setRequestEnvelope(new RequestEnvelope("en_US"));
		detailReq.setPayKey(dto.getCodigo());
		detailReq.setTrackingId(String.valueOf(dto.getPedido().getCodigo()));
		return detailReq;
	}

	/**
	 * Verifica se o erro for: The tracking ID {0} already exists and can't be duplicated.
	 * @param response
	 * @return
	 */
	public static boolean isDuplicateTrackingIdError(VeiculosPaymentResponse response) {
		return Integer.valueOf(PAYPAL_ERROR_DUPLICATE_TRACKING_ID).equals(response.getError().get(0).getErrorId());
	}
	
	/**
	 * Verifica se o status se a chave est� expirada.
	 * @param response
	 * @return
	 */
	public static boolean isStatusExpired(VeiculosPaymentResponse response) {
		return  STATUS_PAGAMENTO_EXPIRADO.equalsIgnoreCase(response.getStatus());
	}

	/**
	 * @param response
	 * @return
	 */
	public static boolean isStatusCompleted(VeiculosPaymentResponse response) {
		return STATUS_PAGAMENTO_COMPLETO.equalsIgnoreCase(response.getStatus());
	}

	/**
	 * @param response
	 * @return
	 */
	public static boolean isStatusError(VeiculosPaymentResponse response) {
		return STATUS_ERROR.equalsIgnoreCase(response.getStatus());
	}
	
}
