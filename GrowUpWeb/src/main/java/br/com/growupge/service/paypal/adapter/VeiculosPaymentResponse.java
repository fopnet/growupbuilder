package br.com.growupge.service.paypal.adapter;

import java.util.List;

import com.paypal.svcs.types.common.ErrorData;
import com.paypal.svcs.types.common.ResponseEnvelope;

/**
 * @author Felipe
 *
 */
public interface VeiculosPaymentResponse {
	
	public String getStatus();
	public ResponseEnvelope getResponseEnvelope();
	public List<ErrorData> getError();
	public String getPayKey();
}
