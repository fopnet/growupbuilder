/**
 * 
 */
package br.com.growupge.service.paypal.adapter;

import java.util.List;

import com.paypal.svcs.types.ap.PaymentDetailsResponse;
import com.paypal.svcs.types.common.ErrorData;
import com.paypal.svcs.types.common.ResponseEnvelope;

/**
 * @author Felipe
 *
 */
public class PaymentDetailResponseAdapter implements VeiculosPaymentResponse {

	private PaymentDetailsResponse response;
	
	public PaymentDetailResponseAdapter(PaymentDetailsResponse response) {
		this.response = response;
		
	}
	
	/**
	 * @return
	 */
	@Override
	public String getPayKey() {
		return response.getPayKey();
	}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.service.paypal.adapter.VeiculosPaymentResponse#getStatus()
	 */
	@Override
	public String getStatus() {
		return response.getStatus();
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.service.paypal.adapter.VeiculosPaymentResponse#getResponseEnvelope()
	 */
	@Override
	public ResponseEnvelope getResponseEnvelope() {
		return response.getResponseEnvelope();
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.service.paypal.adapter.VeiculosPaymentResponse#getError()
	 */
	@Override
	public List<ErrorData> getError() {
		return response.getError();
	}

}
