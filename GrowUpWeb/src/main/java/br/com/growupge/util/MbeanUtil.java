package br.com.growupge.util;


import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import br.com.growupge.constantes.GCS;
import br.com.growupge.dto.MensagemRetornoDTO;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.facade.GrowUpFacadeAdapter;


/**
 * @author Felipe
 *
 */
public class MbeanUtil implements MbeanConstantes {
	public static final Logger logger = Logger.getLogger(MbeanUtil.class);
	
	public static <T> List<SelectItem> listToSelectItems(List<T> lista, String value, String label) {

		List<SelectItem> selectItems = new ArrayList<SelectItem>();

		if (lista != null) {

			String valor = null;
			String rotulo = null;
			
			//para cada objeto existente na lista passada como par�metro
			for (T objeto : lista) {

				try {

					//pega o m�todo que retornar� o valor a ser utilizado como chave na lista de SelectItem
					valor = PropertyUtils.getProperty(objeto, value).toString();

					rotulo = PropertyUtils.getProperty(objeto, label).toString();

					// se n�o conseguiu pegar esse valor
				} catch (Exception e) {

					//busca o valor no toString do objeto
					valor = objeto.toString();
					rotulo = objeto.toString();
				}

				//guarda o objeto e sua chave na lista de SelectItems
				selectItems.add(new SelectItem(valor, rotulo));
			}
			
		}

		return selectItems;
	}

	public static String getLabelsResourceBundle(String key, Object... parametros) {
		try {
			String msg = getFacesResources("labels").getString(key);
			msg = String.format(msg, parametros);
			msg = MessageFormat.format(msg, parametros);
			return msg;
		} catch (Exception e) {
			logger.error(e,e);
		}
		return "?" + key + "?";//
	}

	private static ResourceBundle getFacesResources(String resourceName) {
		FacesContext context = getJSFContext();
		return context.getApplication().getResourceBundle(context, resourceName);
	}

	public static void adicionarMensagemSucesso(String msg, Object... parametros) {
		adicionarMensagemJsf(msg, FacesMessage.SEVERITY_INFO, parametros);
	}

	public static void adicionarMensagemSucesso(MensagemRetornoDTO msgDTO) {
		FacesMessage fmsg = createFacesMessage( FacesMessage.SEVERITY_INFO, msgDTO.getMensagemRetorno());
		getJSFContext().addMessage(null, fmsg);
	}

	public static void adicionarMensagemErro(String msg, Object... parametros) {
		adicionarMensagemJsf(msg, FacesMessage.SEVERITY_ERROR, parametros);
	}

	public static void adicionarMensagemErro(GrowUpException e) {
		FacesMessage fmsg = createFacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage());
		getJSFContext().addMessage(null, fmsg);
	}

	public static void adicionarMensagemAlerta(String msg, Object... parametros) {
		adicionarMensagemJsf(msg, FacesMessage.SEVERITY_WARN, parametros);
	}

	protected static void adicionarMensagemJsf(String msg, Severity severidade, Object... parametros) {
		String msgFormatada = getLabelsResourceBundle(msg, parametros);
		FacesMessage fmsg = createFacesMessage(severidade, msgFormatada);
		getJSFContext().addMessage(null, fmsg);
	}

	public static FacesMessage createFacesMessage(Severity severidade, String msgFormatada) {
		return createFacesMessage(severidade, msgFormatada, null);
	}
	
	public static FacesMessage createFacesMessage(Severity severidade, String msgFormatada, String details) {
		return new FacesMessage(severidade, msgFormatada, details);
	}

	/////////////////////////////////////////////////////////////////////////////////
	// Sessions
	/////////////////////////////////////////////////////////////////////////////////
	public static HttpSession getJsfSession() {
		return getJsfSession(true);
	}
	
	private static HttpSession getJsfSession(boolean bln) {
		FacesContext facesContext = getJSFContext();
		if (facesContext ==  null)
			return null;
		else
			return (HttpSession) facesContext.getExternalContext().getSession(bln);
	}
	
	public static Object getSessionValue(String key) {
		FacesContext facesContext = getJSFContext();
		if (facesContext == null) 
			return null;
		
		return getJsfSession().getAttribute(key);
//		return facesContext.getExternalContext().getSessionMap().get(key);
	}

	public static String getParameterValue(String name) {
		return getJsfRequest().getParameter(name);
	}
	
	/**
	 * Avalia se o usu�rio est� conectado na sess�o.
	 * @return
	 */
	public static boolean isUsuarioConectado() {
		return getUsuarioSessao() != null;
	}

	public static boolean isUsuarioAdmin() {
		return getUsuarioSessao() != null && getUsuarioSessao().isAdmin();
	}
	
	public static UsuarioDTO getUsuarioSessao() {
		return (UsuarioDTO) getSessionValue(CHAVE_SESSAO_USUARIO);
	}

	/**
	 * Acessando a sessao atraves de uma servlet
	 * @param dto
	 * @param request
	 * @param response
	public static void setUsuarioSessao(SessaoDTO dto, HttpServletRequest request, HttpServletResponse response) {
		FacesUtil.getFacesContext(request, response);
		setUsuarioSessao(dto, request.getSession());
	}
	 */
	
	/**
	 * @param dto
	 */
	public static void setUsuarioSessao(SessaoDTO dto) {
		setUsuarioSessao(dto, getJsfSession());
	}

	/**
	 * @param dto
	 */
	public static void setUsuarioSessao(SessaoDTO dto, HttpSession session) {
		if (session == null)
			return;
		
		if (dto == null) {
			session.setAttribute(CHAVE_SESSAO_USUARIO, null);
			session.setAttribute(CHAVE_SESSAO_SID, null);
			// session.invalidate(); // Este chama o listener novamente.
		} else {
			session.setAttribute(CHAVE_SESSAO_USUARIO, dto.getUsuario());
			session.setAttribute(CHAVE_SESSAO_SID, dto.getSid());
		}
	}
	
	public static String getSID() {
		return (String) getSessionValue(CHAVE_SESSAO_SID);
	}

	/**
	 * @param url
	 * @throws IOException 
	 */
	public static void redirect(String url) throws IOException {
		getJSFContext().getExternalContext().redirect(url);
	}

	/**
	 * @param url
	 * @throws IOException 
	 */
	public static void forward(String url) throws IOException  {
		getJSFContext().getExternalContext().dispatch(url);
	}

	/**
	 * @param session
	 */
	public static void desconectar(HttpSession httpSession, final GrowUpFacadeAdapter facade) {
		
		try {
			if (httpSession != null) {
				facade.executarComando(httpSession, GCS.MAPA_SESSAO, GCS.DESCONECTAR, new SessaoDTO(httpSession.getId()));
			}
		} catch (GrowUpException e) {
			logger.error(e,e);
		} finally {
			MbeanUtil.setUsuarioSessao(null);	
		}
		
	}

	/**
	 * Efetue chamada remota para conexao no sistema
	 * @throws GrowUpException 
	 * 
	 */
	public static void conectarPorEmail(String email, String senha, final GrowUpFacadeAdapter facade) throws GrowUpException {
		SessaoDTO sessao = new SessaoDTO();
		sessao.setUsuario(new UsuarioDTO());
		sessao.getUsuario().setEmail(email);
		sessao.getUsuario().setSenha(senha);
		
		sessao = (SessaoDTO) facade.executarComando(getJsfSession(), GCS.MAPA_SESSAO, GCS.CONECTAR, sessao);
		
		MbeanUtil.setUsuarioSessao(sessao);
	}

	/**
	 * @return
	 */
	public static String voltarPaginaAnterior() {
		String sUrl = getJsfRequest().getHeader("referer");
		return FilenameUtils.getBaseName(sUrl);
	}

	protected static HttpServletRequest getJsfRequest() {
		return (HttpServletRequest)getJSFContext().getExternalContext().getRequest();
	}

	/**
	 * @return
	 */
	private static FacesContext getJSFContext() {
		FacesContext context = FacesContext.getCurrentInstance();
			
		return context;
	}
}
