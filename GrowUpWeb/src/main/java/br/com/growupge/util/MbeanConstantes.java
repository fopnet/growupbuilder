package br.com.growupge.util;

/**
 * @author Felipe
 *
 */
public interface MbeanConstantes {
	public final static String CHAVE_SESSAO_USUARIO = "usuario";
	public final static String CHAVE_SESSAO_SID = "sid";
	
	public final static String MSG_ERRO = "MSG_ERRO";
	
	public final static String CSS_ERROR_ALERT = "alert-danger";
}
