package br.com.growupge.util;

import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Felipe
 *
 */
public class ServletUtil {

	public static final String CONTENT_TYPE = "text/html; charset=UTF-8";

	public static String getURLWithContextPath(HttpServletRequest request) {
		return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
	}

	public static void printError(PrintWriter out, String msgerro) {
		out.print("<html>");
		out.print("<head>");
		out.print("	<meta http-equiv=Content-Type content=\"");
		out.print(CONTENT_TYPE);
		out.print("\">");
		out.print("	<title>Mensagem de confirmação</title>");
		out.print("</head>");
		out.print("<body>");
		out.print("	<div style=\"border:solid; width: 100%\">");
		out.print("		<h2 style=\"color:red;\">ERRO:");
		out.print(msgerro);
		out.print("		</h2>");
		out.print("	</div>");
		out.print("</body>");
		out.print("</html>");
	}
	
	public static HttpSession getServicoSession() {
		return new HttpSession() {
			
			@Override
			public void setMaxInactiveInterval(int interval) {	}
			
			@Override
			public void setAttribute(String name, Object value) { }
			
			@Override
			public void removeValue(String name) {}
			
			@Override
			public void removeAttribute(String name) { }
			
			@Override
			public void putValue(String name, Object value) { }
			
			@Override
			public boolean isNew() { return false; }
			
			@Override
			public void invalidate() { }
			
			@Override
			public String[] getValueNames() { return null;	}
			
			@Override
			public Object getValue(String name) { return null;	}
			
			@SuppressWarnings("deprecation")
			@Override
			public javax.servlet.http.HttpSessionContext getSessionContext() { return null; 	}
			
			@Override
			public ServletContext getServletContext() { return null; }
			
			@Override
			public int getMaxInactiveInterval() { return 0; }
			
			@Override
			public long getLastAccessedTime() { return 0; }
			
			@Override
			public String getId() {
				return "1";
			}
			
			@Override
			public long getCreationTime() { return 0; }
			
			@Override
			public Enumeration<String> getAttributeNames() { return null;}
			
			@Override
			public Object getAttribute(String name) { return null;	}
		};
	}


}
