package br.com.growupge.util;


import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.paypal.core.SDKUtil;

/**
 * @author Felipe
 *
 */
public class SdkConfig {
	private static Map<String, String> configurationMap = null;
	
	// Sandbox Email Address Key
	public static final String RECEIVER_EMAIL_ADDRESS = "receiver.EmailAddress";
	public static final String CANCEL_URL = "service.cancelURL";
	public static final String RETURN_URL = "service.returnURL";
	public static final String IPN_URL = "service.IPNListenerUrl";
	
	static {
		InputStream is = SdkConfig.class.getResourceAsStream("/sdk_config.properties");
		Properties properties = new Properties();
		try {
			properties.load(is);
		} catch (IOException e) {
			Logger.getLogger(SdkConfig.class).error(e.getMessage());
		}
		configurationMap = SDKUtil.constructMap(properties);
	}
	
	/**
	 * @return
	 */
	static public Map<String, String> getConfigurationMap() {
		return configurationMap;
	}
	
	/**
	 * @return
	 */
	static public String getEmailReceiver() {
		return configurationMap.get(RECEIVER_EMAIL_ADDRESS);
	}
	
	/**
	 * @return the redirectUrl
	 */
	static private String getRedirectUrl() {
		return configurationMap.get(com.paypal.core.Constants.SERVICE_REDIRECT_ENDPOINT);
	}
	
	/**
	 * @return
	 */
	static public String getPaykeyUrl(String payKey) {
		return getRedirectUrl() + "_ap-payment&paykey=" + payKey;
	}

	/**
	 * @return
	 */
	public static String getCancelUrl() {
		return configurationMap.get(CANCEL_URL);
	}

	/**
	 * @return
	 */
	public static String getReturnUrl() {
		return configurationMap.get(RETURN_URL);
	}

	/**
	 * @return
	 */
	public static String getIPNListenerURL() {
		return configurationMap.get(IPN_URL);
	}

	/**
	 * @return
	 */
	public static String getAppID() {
		return configurationMap.get("facebookConnect.appID");
	}

	/**
	 * @return
	 */
	public static String getAppSecret() {
		return configurationMap.get("facebookConnect.appSecret");
	}

	/**
	 * @return
	 */
	public static String getRedirectUri() {
		return configurationMap.get("facebookConnect.redirectURI");
	}

}
