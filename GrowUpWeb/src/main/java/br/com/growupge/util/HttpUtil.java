/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 18/05/2015
 *
 * Historico de Modifica��o:
 * =========================
 * 18/05/2015 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.NameValuePair;
import org.apache.log4j.Logger;


/**
 * @author Felipe
 * 
 */
public class HttpUtil {
	private static final Logger logger = Logger.getLogger(HttpUtil.class);
	
	// HTTP GET request
	public static String sendGet(String url) throws Exception {

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		// add request header
		con.setRequestProperty("User-Agent", "Mozilla/5.0");

		int responseCode = con.getResponseCode();
		logger.info("\nSending 'GET' request to URL : " + url);
		logger.info("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		logger.info(response.toString());
		
		return response.toString();

	}

	/**
	 * @param response
	 * @param post
	 * @return 
	 * @return
	 * @throws Exception
	 */
	public static void sendPostAndRedirect(HttpServletResponse response, String post, NameValuePair... params) throws Exception {
		HttpsURLConnection conn = openPostConnection(post, "POST", params);

		try {
			// Send post request
			conn.setDoOutput(true);
						
			// normally, 3xx is redirect
			int status = conn.getResponseCode();
			if (status != HttpURLConnection.HTTP_OK) {
				if (status == HttpURLConnection.HTTP_MOVED_TEMP
						|| status == HttpURLConnection.HTTP_MOVED_PERM
						|| status == HttpURLConnection.HTTP_SEE_OTHER) {
					
					// get redirect url from "location" header field
					String redirectUri = conn.getHeaderField("Location");

					response.sendRedirect(redirectUri);
					logger.info("N�o foi poss�vel redirecionar para " + redirectUri);

					/*
					// get the cookie if need, for login
					String cookies = conn.getHeaderField("Set-Cookie");
					
					// open the new connnection again
					conn = (HttpsURLConnection) new URL(redirectUri).openConnection();
					conn.setRequestProperty("Cookie", cookies);
					conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
					conn.addRequestProperty("User-Agent", "Mozilla");
//					conn.addRequestProperty("Referer", "google.com");
					
					logger.info("Redirecting to " + redirectUri);
					*/
					
//					responseToString(conn);
				}
				
			}
		} finally {
			conn.disconnect();
		}
		
	}

	/**
	 * @param response
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public static String getPostResponse(HttpServletResponse response, String post, NameValuePair... params) throws Exception {
		return getResponse(response, post, "POST", params);
	}
	
	public static String getResponse(HttpServletResponse response, String post, String requestMethod, NameValuePair... params) throws Exception {
//		NameValuePair params = null;
		HttpsURLConnection conn = openPostConnection(post, requestMethod, params);
		
		try {
			// Send post request
//			conn.setDoOutput(true);
			
			// normally, 3xx is redirect
			int status = conn.getResponseCode();
			if (status != HttpURLConnection.HTTP_OK) {
				
				if (status == HttpURLConnection.HTTP_MOVED_TEMP
						|| status == HttpURLConnection.HTTP_MOVED_PERM
						|| status == HttpURLConnection.HTTP_SEE_OTHER) {

					// get redirect url from "location" header field
					String redirectUri = conn.getHeaderField("Location");

					response.sendRedirect(redirectUri);
					logger.info("N�o foi poss�vel redirecionar para " + redirectUri);

				} else 
					throw new ServletException("Received http status " + status);

				
			} else
				return responseToString(conn.getInputStream());
			
		} finally {
			conn.disconnect();
		}
		
		return null;
		
	}
	
	// HTTP POST request
	public static void sendPost(String url, NameValuePair... params) throws Exception {

		HttpsURLConnection conn = null; 
		try {
			conn = openPostConnection(url, "POST", params);
			
			// Send post request
			conn.setDoOutput(true);
			
			logger.info(responseToString(conn.getInputStream()));
			
		} finally {
			conn.disconnect();
		}

	}

	private static HttpsURLConnection openPostConnection(String url, String requestMethod,
			NameValuePair... params) throws MalformedURLException, IOException,
			ProtocolException, URISyntaxException {
		URL obj = new URL(url);
		HttpsURLConnection conn = (HttpsURLConnection) obj.openConnection();

		// add reuqest header
		conn.setReadTimeout(5000);
//		conn.setInstanceFollowRedirects(isRedirect);
		conn.setRequestMethod(requestMethod);
		conn.setRequestProperty("User-Agent", "Mozilla/5.0");
		conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
//		conn.setRequestProperty("Content-Type", "application/json");
//		conn.setRequestProperty("Accept", "application/json");

		
		/*
		// Send post request
		// conn.setDoOutput(true);
//		String urlParameters = ""; //"sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";
		if (params!= null) {
			URI uri = new URIBuilder()
//				.setScheme("https")
				.setHost(url)
				.setParameters(Arrays.asList(params))
				.build() ;
			
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(uri.toString());
			wr.flush();
			wr.close();
		}

		int responseCode = conn.getResponseCode();
		logger.info("\nSending 'POST' request to URL : " + url);
//		logger.info("Post parameters : " + urlParameters);
		logger.info("Response Code : " + responseCode);
		*/

		//String response = responseToString(conn);

		// print result
	
		return conn;
	}
	
	public static String responseToString(InputStream is) throws IOException {
		StringBuilder response = new StringBuilder();
		BufferedReader in = new BufferedReader(new InputStreamReader(is));

		try {
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
		} finally  {
			in.close();
		}

		
//		logger.info(response.toString());
		return response.toString();
	}
	

	/**
	 * @return
	private static List<NameValuePair> map2NamePairList(Map<String,String> mapToConvert) {
		List<NameValuePair> queryParams = new ArrayList<NameValuePair>();
	    for (Entry<String, String> entry : mapToConvert.entrySet()) {
	        queryParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
	    }
	      
		return queryParams;

	}
	 */
}
