/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 17/08/2014
 *
 * Historico de Modifica��o:
 * =========================
 * 17/08/2014 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.job;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.growupge.constantes.Constantes;

/**
 * @author Felipe
 *
 */
public class PeriodoPedidoPagamentoExpiradoJob implements Job  {

	public static final String GROUP_NAME = "pedidoGrupo";
	private static final Logger logger = Logger.getLogger(PeriodoPedidoPagamentoExpiradoJob.class);

	/* (non-Javadoc)
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {
			HttpPost httpPost = new HttpPost(Constantes.URL_SITE +  "/verificarPagamentosExpirados");
			List <BasicNameValuePair> nvps = new ArrayList <BasicNameValuePair>();
			nvps.add(new BasicNameValuePair("username", "vip"));
			nvps.add(new BasicNameValuePair("password", "secret"));
			httpPost.setEntity(new UrlEncodedFormEntity(nvps));
			CloseableHttpResponse response2 = httpclient.execute(httpPost);

			int status = response2.getStatusLine().getStatusCode();
		    if (status >= 200 && status < 300)  {
		    	logger.info(getClass().getSimpleName() + " executado" );
		    }

		    /*HttpEntity entity2 = response2.getEntity();
		    // do something useful with the response body
		    // and ensure it is fully consumed
		    EntityUtils.consume(entity2);*/
		} catch (UnsupportedEncodingException e) {
			logger.error(e,e);
		} catch (ClientProtocolException e) {
			logger.error(e,e);
		} catch (IOException e) {
			logger.error(e,e);
		} 
			
			
		
		
	}

}
