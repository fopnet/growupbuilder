/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 09/08/2014
 *
 * Historico de Modifica��o:
 * =========================
 * 09/08/2014 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.endpoints;

import flex.messaging.endpoints.AMFEndpoint;
import flex.messaging.io.EnumProxy;
import flex.messaging.io.PedidoVeiculoDTOProxy;

/**
 * @author Felipe
 *
 */
public class J5AMFEndpoint extends AMFEndpoint {

	static {
		EnumProxy.registerPropertyProxy();
		PedidoVeiculoDTOProxy.registerPropertyProxy();
	}
	
	public J5AMFEndpoint() {
		this(false);
	}

	public J5AMFEndpoint(final boolean enableManagement) {
		super(enableManagement);
		deserializerClass = J5AmfMessageDeserializer.class;
		serializerClass   = J5AmfMessageSerializer.class;
	}
}