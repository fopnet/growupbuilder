/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 09/08/2014
 *
 * Historico de Modifica��o:
 * =========================
 * 09/08/2014 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.endpoints;

import java.io.IOException;

import flex.messaging.io.amf.ActionContext;
import flex.messaging.io.amf.ActionMessage;
import flex.messaging.io.amf.AmfMessageDeserializer;
import flex.messaging.io.amf.MessageBody;
import flex.messaging.io.amf.MessageHeader;

/**
 * @author Felipe
 *
 */
public class J5AmfMessageDeserializer extends AmfMessageDeserializer {

	/* (non-Javadoc)
	 * @see flex.messaging.io.amf.AmfMessageDeserializer#readMessage(flex.messaging.io.amf.ActionMessage, flex.messaging.io.amf.ActionContext)
	 */
	@Override
	public void readMessage(ActionMessage m, ActionContext context)
			throws ClassNotFoundException, IOException {
		// TODO Auto-generated method stub
		super.readMessage(m, context);
	}

	/* (non-Javadoc)
	 * @see flex.messaging.io.amf.AmfMessageDeserializer#readHeader(flex.messaging.io.amf.MessageHeader, int)
	 */
	@Override
	public void readHeader(MessageHeader header, int index)
			throws ClassNotFoundException, IOException {
		// TODO Auto-generated method stub
		super.readHeader(header, index);
	}

	/* (non-Javadoc)
	 * @see flex.messaging.io.amf.AmfMessageDeserializer#readBody(flex.messaging.io.amf.MessageBody, int)
	 */
	@Override
	public void readBody(MessageBody body, int index)
			throws ClassNotFoundException, IOException {
		// TODO Auto-generated method stub
		super.readBody(body, index);
	}

	/* (non-Javadoc)
	 * @see flex.messaging.io.amf.AmfMessageDeserializer#readObject()
	 */
	@Override
	public Object readObject() throws ClassNotFoundException, IOException {
		// TODO Auto-generated method stub
		return super.readObject();
	}

}
