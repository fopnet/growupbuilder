/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 09/08/2014
 *
 * Historico de Modifica��o:
 * =========================
 * 09/08/2014 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.endpoints;

import java.io.IOException;
import java.util.IdentityHashMap;

import flex.messaging.io.SerializationContext;
import flex.messaging.io.amf.Amf3Output;

/**
 * @author Felipe
 *
 */
public class J5Amf3Output extends Amf3Output {
	final private IdentityHashMap<Enum<?>, EnumHolder> enumTable = new IdentityHashMap<Enum<?>, EnumHolder>();

	public J5Amf3Output(final SerializationContext context) {
		super(context);
	}

	@Override public void reset() {
		super.reset();
		enumTable.clear();
	}

	@Override public void writeObject(final Object o) throws IOException {
		if (o instanceof Enum) {
			final Enum<?> e = (Enum<?>)o;
			EnumHolder holder = enumTable.get(o);
			if (holder == null) {
				holder = new EnumHolder(e);
				enumTable.put(e, holder);
			}
			super.writeObject( holder );
		}
		else
			super.writeObject(o);
	}

	class EnumHolder {
		Enum<?> clazz;
		String name;

		EnumHolder(Enum<?> clazz) {
			this.clazz = clazz;
		}

	}

}