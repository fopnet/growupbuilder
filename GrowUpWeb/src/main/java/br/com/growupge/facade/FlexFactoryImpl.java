package br.com.growupge.facade;

import flex.messaging.FactoryInstance;
import flex.messaging.config.ConfigMap;


/**
 * Facade do usado pelo flex para realizar o lookup do GrowUpfacade
 * @author Felipe
 *
 */
//@javax.inject.Named
//@javax.enterprise.context.ApplicationScoped
public class FlexFactoryImpl implements flex.messaging.FlexFactory {
//	private static final long serialVersionUID = -7983124973387308703L;
//	 private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FlexFactoryImpl.class);

	private GrowUpFacadeAdapter facade;
	/*	@javax.enterprise.inject.Produces 
	@javax.enterprise.inject.Default
	@br.com.growupge.facade.Facade // Declare the Binding*/
	public GrowUpFacadeAdapter getFacade() {
		return facade;
	}

	
	/* (non-Javadoc)
	 * @see flex.messaging.FlexConfigurable#initialize(java.lang.String, flex.messaging.config.ConfigMap)
	 */
	@Override
	public void initialize(String id, ConfigMap configMap) {
	}

	/* (non-Javadoc)
	 * @see flex.messaging.FlexFactory#createFactoryInstance(java.lang.String, flex.messaging.config.ConfigMap)
	 */
	@Override
	public FactoryInstance createFactoryInstance(String id, ConfigMap properties) {
//		 log.info ("Create FactoryInstance."); 
		 FlexFactoryInstance instance = new FlexFactoryInstance (this, id, properties); 
         instance.setSource (properties.getPropertyAsString (SOURCE, instance.getId ())); 
         return instance; 
	}

	/* (non-Javadoc)
	 * @see flex.messaging.FlexFactory#lookup(flex.messaging.FactoryInstance)
	 */
	@Override
	public Object lookup(FactoryInstance instanceInfo) {
		return instanceInfo.lookup();
	}

}
