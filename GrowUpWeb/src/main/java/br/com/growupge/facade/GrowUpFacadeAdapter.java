package br.com.growupge.facade;

import java.io.Serializable;
import java.util.List;

import javax.servlet.http.HttpSession;

import br.com.growupge.exception.GrowUpException;

/**
 * @author Felipe
 *
 */
public interface GrowUpFacadeAdapter extends Serializable {
	
	/**
	 * Executa o comando e retorna um objecto
	 * @param httpSession
	 * @param mapa
	 * @param permissao
	 * @param params
	 * @return
	 * @throws GrowUpException
	 */
	public Object executarComando(final HttpSession httpSession, 
			final String mapa, String permissao,
			final Object... params) throws GrowUpException;

	/**
	 * Executa um comando e retorno uma lista tipada.
	 * @param httpSession
	 * @param mapa
	 * @param permissao
	 * @param params
	 * @return
	 * @throws GrowUpException
	 */
	public abstract <T> List<T> executarComandoLista(final HttpSession httpSession, final String mapa,
			final String permissao, final T params) throws GrowUpException;

}