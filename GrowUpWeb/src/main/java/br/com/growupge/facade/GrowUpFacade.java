package br.com.growupge.facade;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import br.com.growupge.constantes.GCS;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.ComandoDTO;
import br.com.growupge.dto.EmailDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.HtmlTemplateFactory;
import br.com.growupge.gerenciadores.GerenciadorComandos;
import br.com.growupge.gerenciadores.GerenciadorEmail;
import br.com.growupge.template.HtmlTemplate;
import flex.messaging.FlexContext;

/**
 * @author Felipe
 * 
 */
//@javax.enterprise.inject.Alternative
@javax.inject.Named("facade") //nome utilizado pelo flex
@javax.enterprise.context.SessionScoped
@javax.enterprise.inject.Default
public class GrowUpFacade implements GrowUpFacadeAdapter {

	private static final Logger logger = Logger.getLogger(GrowUpFacade.class);
	
	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private GerenciadorComandos gerenciadorComandos;

	/**
	 * Construtor para esta classe usado pelo Flex.
	 * 
	 */
	public GrowUpFacade() {
		//logger.info("GrowUpFacade created"); 	
	}

	/**
	 * Executa o comando informado no array de objetos.
	 * 
	 * @param parametros
	 *            Um array de objetos contendo os atributos abaixo na seguinte
	 *            ordem: 0- sid 1- Mapa
	 * @see GCS 2- Opera��o/Permiss�o
	 * @see GCS 3- Objeto (dto, classe personalizada, etc..), Mandat�rio que
	 *      seja uma classe. Ex: new Object[] { SID.codigoHash, GCS.MAPA_LOCAL,
	 *      GCS.CRIAR_LOCAL, dto }
	 * @return Objeto retornado pode ser nulo se a opera��o n�o for bem sucedida
	 *         ou populado caso ocorra com sucesso.
	 * @throws GrowUpException
	 *             Exce��o padr�o.
	 * 
	 */
//	private Object executarComando(Object[] parametros) throws GrowUpException {
//
//		final int inicioParametros = 3;
//		try {
//			/* Rearranja os parametros */
//			DTOPadrao[] lista = new DTOPadrao[parametros.length - inicioParametros];
//			System.arraycopy(parametros, inicioParametros, lista, 0, parametros.length);
//			
//			ComandoDTO cmd = new ComandoDTO();
//			cmd.SID = (String)parametros[0];
//			cmd.mapaGCS = (String)parametros[1];
//			cmd.permissao = (String)parametros[2];
//			cmd.parametrosLista = Arrays.asList(lista);
//			 
//			return this.executarComando(cmd);
//		} catch (GrowUpException se) {
//			throw se;
//		} catch (Exception ce) {
//			throw GrowUpException.getErroInterno(ErroInterno.ERRO_DESCONHECIDO, ce);
//		}
//	}

	/**
	 * Chamada pelo flex
	 * 
	 * @param cmd
	 * @return
	 * @throws GrowUpException
	 */
	public /*final*/ Object executarComando(final ComandoDTO cmd) throws GrowUpException {
		return executarComando(cmd, FlexContext.getFlexSession() != null ? FlexContext.getFlexSession().getId() : null);
	}
	
	/**
	 * Chamado por qualquer servlet ou jsp
	 * @param cmd
	 * @return
	 * @throws GrowUpException
	 */
	public /*final*/ Object executarComando(final ComandoDTO cmd, HttpSession httpSession) throws GrowUpException {
		return executarComando(cmd, httpSession != null ? httpSession.getId() : null);
	}
	
	/**
	 * M�todo utilizado para requisitar a execu��o da opera��o definida no array
	 * de objetos 'parametros'.
	 * 
	 * @param dto
	 *            Um DTO contendo Um array de objetos contendo os atributos
	 *            abaixo na seguinte ordem: 0- sid 1- Mapa
	 * @see GCS 2- Opera��o/Permiss�o
	 * @see GCS 3- ArrayList de Objetos
	 * @return Objeto para uso
	 * @throws GrowUpException
	 *             Caso ocorra uma exce��o padr�o durante a execu��o da
	 *             opera��o.
	 */
	private /*final*/ Object executarComando(final ComandoDTO cmd, String sid) throws GrowUpException {
		// UserTransaction ut = null;
		try {
			// ut = ServiceLocator.getInstance().getUserTransaction();
			// ut.begin();

			if (sid == null) {
				logger.info("httpSession is required");
				throw new IllegalArgumentException("httpSession is required");
			}

			cmd.setSID(sid);
			Object o = gerenciadorComandos.invocar(cmd);

			// ut.commit();
			return o;
		} catch (GrowUpException se) {
			throw se;
		} catch (Exception e) {
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
		}
	}

	/**
	 * Gerenciador de Emails. OBS: Estes metodos n�o aceitam sobrecargas no
	 * delegate do flex pelo AMF0
	 * 
	 * @param dto objeto do tipo EmailDTO para encapsular as informa��es de
	 * 			   email.
	 * 
	 * @return EmailDTO
	 * @throws GrowUpException
	 */
	public /*final*/ EmailDTO enviarEmail(final EmailDTO mail) throws GrowUpException {
		try {

			GerenciadorEmail gerenciador = GerenciadorEmail.getInstance();

			// Obtem o Template com base no idioma do usu�rio
			HtmlTemplate templateEmail = HtmlTemplateFactory.getTemplateContato();

			// Adiciona os parametros que dever�o ser substituidos no template
			templateEmail.adicionarParametro(HtmlTemplate.PARAM_ASSUNTO, mail.getAssunto());
			templateEmail.adicionarParametro(HtmlTemplate.PARAM_EMPRESA, mail.getEmpresa().getNome());
			templateEmail.adicionarParametro(HtmlTemplate.PARAM_MSG, mail.getConteudo());

			// Executa o template
			templateEmail.execute();

			gerenciador.enviarEmail(mail.getAssunto(), templateEmail.getHtmlTemplate(), mail.getUsuario(),
									mail.getEmpresa());
			return mail;
		} catch (MessagingException ex) {
			throw new GrowUpException(MSGCODE.ERRO_ENVIO_EMAIL, ex.getMessage(), "", ex
					.getMessage());
		} catch (GrowUpException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new GrowUpException(MSGCODE.ERRO_ENVIO_EMAIL, ex.getMessage(), ex.getMessage(),
					ex.getMessage());
		}

	}
	
	@Override
	public /*final*/ Object executarComando(final HttpSession httpSession, 
			final String mapa, final String permissao,
			final Object... params) throws GrowUpException{
		ComandoDTO cmd = new ComandoDTO();
		cmd.setSID(httpSession.getId());
		cmd.setMapaGCS(mapa);
		cmd.setPermissao(permissao);
		cmd.getParametrosLista().addAll(Arrays.asList(params));
		
		return executarComando(cmd, httpSession);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public /*final*/ <T> List<T> executarComandoLista(final HttpSession httpSession, 
			final String mapa, final String permissao,
			final T params) throws GrowUpException{
				
		List<T> list = (List<T>) executarComando(httpSession, mapa, permissao, params);
		
		return Collections.checkedList(list, (Class<T>) params.getClass());
	}

}
