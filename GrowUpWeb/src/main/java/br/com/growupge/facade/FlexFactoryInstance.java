package br.com.growupge.facade;

import br.com.growupge.factory.BeanFactory;
import flex.messaging.FlexFactory;
import flex.messaging.config.ConfigMap;

/**
 * @author Felipe
 *
 */
class FlexFactoryInstance extends flex.messaging.FactoryInstance { 
//    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(FlexFactoryInstance.class);

    FlexFactoryInstance (FlexFactory factory, String id, ConfigMap properties) { 
        super(factory, id, properties); 
    } 

    @Override
	public Object lookup () { 

    	String beanName = getSource (); 
    	
//    	try {
//			Class<?> type = Class.forName(beanName);

//			logger.info ("Lookup bean from GrowupFacade ApplicationContext:" + beanName); 
			return BeanFactory.getContextualInstance(beanName); 

//    	} catch (ClassNotFoundException e) {
//			throw new IllegalArgumentException(e);
//		}
    } 
} 