package flex.messaging.io;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.reflect.FieldUtils;

import br.com.growupge.dto.StatusPedido;
import flex.messaging.io.AbstractProxy;
import flex.messaging.io.BeanProxy;
import flex.messaging.io.PropertyProxyRegistry;

public class EnumProxy extends BeanProxy {
	private static final long serialVersionUID = 1L;
	static final String ACTION_SCRIPT_KEY_NAME = "name";
	static final String ACTION_SCRIPT_TIPO = "tipo";
	static final String ACTION_SCRIPT_DESCRICAO = "descricao";
	static List<String> propertyNames;

	public EnumProxy(Class<?> enumClass) {
		propertyNames = getEnumFields(enumClass);
	}
	
	List<String> getEnumFields(Class<?> enumClass) {
		List<String> permitidos = new ArrayList<>(); 
		List<Field> fields = FieldUtils.getAllFieldsList( enumClass );
		Iterator<Field> it = fields.iterator();
		while (it.hasNext()) {
			Field fld = it.next();
			if (fld.isEnumConstant() )
				it.remove();
			else if (Modifier.isStatic( fld.getModifiers() ) )
				it.remove();
			else  if (Arrays.asList("ENUM$VALUES", "ordinal").contains( fld.getName() )) 
				it.remove();
			else 
				permitidos.add(fld.getName());
		}
		return permitidos;
	}
	
	/////////////////////////////////////////////
    // Serialization
    /////////////////////////////////////////////
 
	@Override 
	public String getAlias(final Object aInstance) {
		return super.getClassName(aInstance);
	}
	
	public List<String> getPropertyNames(Object instance) {
		if (!(instance instanceof Enum))
			throw new IllegalArgumentException("getPropertyNames called with non Enum object");
		return propertyNames;
	}
	 
	@Override 
	public Class<?> getType(final Object aInstance, final String propertyName) {
		if (propertyNames.contains(propertyName))
			return String.class;
		return null;
	}
	
	public Object getValue(Object instance, String propertyName) {
		if (instance instanceof Enum) {
			if (propertyNames.contains(propertyName))
				try {
					return FieldUtils.readField(instance, propertyName, true);
				} catch (IllegalAccessException e) {
					throw new IllegalArgumentException(e);
				}
			else
				return null;
		} else {
			return super.getValue(instance, propertyName);
		}
	}
	
	
	/////////////////////////////////////////////
    // Deserialization
    /////////////////////////////////////////////

	@SuppressWarnings({"unchecked", "rawtypes"})
	public Object createInstance(String className) {
		Class<?> clazz = AbstractProxy.getClassFromClassName(className);

		if (clazz.isEnum()) {
			return new EnumStub(clazz);
		} else
			throw new IllegalArgumentException("samples.EnumProxy registered for a class which is not an enum: " + clazz.getName());
	}
	
	public void setValue(Object instance, String propertyName, Object value) {
//		EnumStub<?> es = (EnumStub<?>) instance;
		if (propertyNames.contains(propertyName))
			try {
				FieldUtils.writeField(instance, propertyName, value, true);// .setProperty(instance, propertyName, value);
			} catch (IllegalAccessException e) {
				throw new IllegalArgumentException(e);
			}
		else {
			super.setValue(instance, propertyName, value);
		}

	}

	@SuppressWarnings({"unchecked", "rawtypes"})
	public Object instanceComplete(Object instance) {
		EnumStub es = (EnumStub) instance;
		return Enum.valueOf(es.clazz, es.name);
	}


	 /////////////////////////////////////////////
    // Registration
    /////////////////////////////////////////////
 
	static public void registerPropertyProxy() {
		PropertyProxyRegistry.getRegistry().register(Enum.class, new EnumProxy(StatusPedido.class));
	}
	 
	class EnumStub<T extends Enum<T>> {
		EnumStub(Class<T> clazz) {
			this.clazz = clazz;
		}

		Class<T> clazz;
		String name;
		String tipo;
		String descricao;
	}

}