/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 15/08/2014
 *
 * Historico de Modifica��o:
 * =========================
 * 15/08/2014 - In�cio de tudo, por Felipe
 *
 */
package flex.messaging.io;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.dto.StatusPedido;
import br.com.growupge.utility.Reflection;
import flex.messaging.log.Log;
import flex.messaging.log.Logger;
import flex.messaging.util.ExceptionUtil;


/**
 * @author Felipe
 *
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class PedidoVeiculoDTOProxy extends BeanProxy {

	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected Map getBeanProperties(Object instance)
    {
        Class c = instance.getClass();
        Map props;
        if (descriptor == null)
        {
            props = (Map)beanPropertyCache.get(c);
            if (props != null)
                return props;
        }

        props = new HashMap();
        PropertyDescriptor[] pds = getPropertyDescriptors(c);
        if (pds == null)
            return null;

        List excludes = null;
        if (descriptor != null)
        {
            excludes = descriptor.getExcludesForInstance(instance);
            if (excludes == null) // For compatibility with older implementations
                excludes = descriptor.getExcludes();
        }

        // Add standard bean properties first
        for (PropertyDescriptor pd : pds)
        {
            String propertyName = pd.getName();
            Method readMethod = pd.getReadMethod();
            Method writeMethod = pd.getWriteMethod();
            if ( "status".equals(propertyName) && PedidoVeiculoDTO.class.equals(c)) {
            	writeMethod  = Reflection.getMethod(c, "setStatus", StatusPedido.class);
            }

            // If there's a public read method but no writeMethod and includeReadOnly
            // flag is off, then skip the property.
            // Mudan�a pertinente ao modificador setStatus do PedidoVeiculoDTO
            if (readMethod != null && isPublicAccessor(readMethod.getModifiers()) && !includeReadOnly && writeMethod == null)
                continue;

            // Skip excluded and ignored properties as well.
            if ((excludes != null && excludes.contains(propertyName)) || isPropertyIgnored(c, propertyName))
                continue;

            // Ensure we don't include Object getClass() property, possibly returned (incorrectly) by custom BeanInfos
            if (includeReadOnly && writeMethod == null && "class".equals(propertyName))
                continue;

            // Skip any classloader properties
            final Class<?> type = pd.getPropertyType();
            if (type != null && ClassLoader.class.isAssignableFrom(type))
                continue;

            props.put(propertyName, 
            		new BeanProperty(propertyName, pd.getPropertyType(), readMethod, writeMethod, null));
        }

        // Then add public fields to list if property does not already exist
        Field[] fields = instance.getClass().getFields();
        for (Field field : fields)
        {
            String propertyName = field.getName();
            int modifiers = field.getModifiers();
            if (isPublicField(modifiers) && !props.containsKey(propertyName))
            {
                // Skip excluded and ignored properties.
                if ((excludes != null && excludes.contains(propertyName)) || isPropertyIgnored(c, propertyName))
                    continue;

                props.put(propertyName, new BeanProperty(propertyName, field.getType(), null, null, field));
            }
        }

        if (descriptor == null && cacheProperties)
        {
            synchronized(beanPropertyCache)
            {
                Map props2 = (Map)beanPropertyCache.get(c);
                if (props2 == null)
                    beanPropertyCache.put(c, props);
                else
                    props = props2;
            }
        }

        return props;
    }
	
	/***
     * Return an array of JavaBean property descriptors for a class
     * @param c the class to examine.
     * @return an array ot JavaBean PropertyDescriptors.
     */
    private PropertyDescriptor [] getPropertyDescriptors(Class c)
    {
        PropertyDescriptorCacheEntry pce = getPropertyDescriptorCacheEntry(c);
        return pce == null? null : pce.propertyDescriptors;
    }
    
    /**
     * Return an entry from the property descrpitor cache for a class.
     */
    private PropertyDescriptorCacheEntry getPropertyDescriptorCacheEntry(Class c)
    {
        PropertyDescriptorCacheEntry pce = (PropertyDescriptorCacheEntry) propertyDescriptorCache.get(c);

        try
        {
            if (pce == null)
            {
                BeanInfo beanInfo = Introspector.getBeanInfo(c, stopClass);
                pce = new PropertyDescriptorCacheEntry();
                pce.propertyDescriptors = beanInfo.getPropertyDescriptors();
                pce.propertiesByName = createPropertiesByNameMap(pce.propertyDescriptors, c.getFields());
                if (cachePropertiesDescriptors)
                {
                    synchronized(propertyDescriptorCache)
                    {
                        PropertyDescriptorCacheEntry pce2 = (PropertyDescriptorCacheEntry) propertyDescriptorCache.get(c);
                        if (pce2 == null)
                            propertyDescriptorCache.put(c, pce);
                        else
                            pce = pce2;
                    }
                }
            }
        }
        catch (IntrospectionException ex)
        {
            // Log failed property set errors
            if (Log.isError())
            {
                Logger log = Log.getLogger(LOG_CATEGORY);
                log.error("Failed to introspect object of type: " + c + " error: " + ExceptionUtil.toString(ex));
            }

            // Return an empty descriptor rather than crashing
            pce = new PropertyDescriptorCacheEntry();
            pce.propertyDescriptors = new PropertyDescriptor[0];
            pce.propertiesByName = new TreeMap();
        }
        return pce;
    }
    
    private Map createPropertiesByNameMap(PropertyDescriptor [] pds, Field [] fields)
    {
		Map m = new HashMap(pds.length);
        for (int i = 0; i < pds.length; i++)
        {
            PropertyDescriptor pd = pds[i];
            Method readMethod = pd.getReadMethod();
            if (readMethod != null && isPublicAccessor(readMethod.getModifiers()) &&
                (includeReadOnly || pd.getWriteMethod() != null))
                m.put(pd.getName(), pd);
        }
        for (int i = 0; i < fields.length; i++)
        {
            Field field = fields[i];

            if (isPublicField(field.getModifiers()) && !m.containsKey(field.getName()))
                m.put(field.getName(), field);
        }
        return m;
    }


	/**
	 * Registro do mesmo 
	 */
	static public void registerPropertyProxy() {
		PropertyProxyRegistry.getRegistry().register(PedidoVeiculoDTO.class, new PedidoVeiculoDTOProxy());
	}	
}
