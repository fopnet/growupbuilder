// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   Itaucripto.java

package Itau;


public class Itaucripto
{

    public static final String CODIGO_EMPRESA = "J0095787180001340000017169";
	public static final String CHAVE_ACESSO = "P4D6F1H0P4D6F1H0";
	public Itaucripto()
    {
        CHAVE_ITAU = "SEGUNDA12345ITAU";
        TAM_COD_EMP = 26;
        TAM_CHAVE = 16;
        numbers = "0123456789";
        sbox = new int[256];
        key = new int[256];
        numPed = "";
        tipPag = "";
        codEmp = "";
    }

    private String Algoritmo(String s, String s1)
    {
        int k = 0;
        int l = 0;
        String s2 = "";
        Inicializa(s1);
        for(int j = 1; j <= s.length(); j++)
        {
            k = (k + 1) % 256;
            l = (l + sbox[k]) % 256;
            int i = sbox[k];
            sbox[k] = sbox[l];
            sbox[l] = i;
            int i1 = sbox[(sbox[k] + sbox[l]) % 256];
            int j1 = s.charAt(j - 1) ^ i1;
            s2 = s2 + (char)j1;
        }

        return s2;
    }

    private String PreencheBranco(String s, int i)
    {
        String s1;
        for(s1 = s.toString(); s1.length() < i; s1 = s1 + " ");
        return s1.substring(0, i);
    }

    private String PreencheZero(String s, int i)
    {
        String s1;
        for(s1 = s.toString(); s1.length() < i; s1 = "0" + s1);
        return s1.substring(0, i);
    }

    private void Inicializa(String s)
    {
        int i1 = s.length();
        for(int j = 0; j <= 255; j++)
        {
            key[j] = s.charAt(j % i1);
            sbox[j] = j;
        }

        int l = 0;
        for(int k = 0; k <= 255; k++)
        {
            l = (l + sbox[k] + key[k]) % 256;
            int i = sbox[k];
            sbox[k] = sbox[l];
            sbox[l] = i;
        }

    }

    private boolean isNumeric(String s)
    {
        if(s.length() > 1)
        {
            boolean flag = true;
            for(int i = 0; i < s.length(); i++)
            {
                flag = isNumeric(s.substring(i, i + 1));
                if(!flag)
                    return flag;
            }

            return flag;
        }
        return numbers.indexOf(s) >= 0;
    }

    private String Converte(String s)
    {
        char c2 = (char)(int)(26D * Math.random() + 65D);
        String s1 = "" + c2;
        for(int i = 0; i < s.length(); i++)
        {
            char c1 = s.charAt(i);
            char c = c1;
            s1 = s1 + Integer.toString(c);
            char c3 = (char)(int)(26D * Math.random() + 65D);
            s1 = s1 + c3;
        }

        return s1;
    }

    private String Desconverte(String s)
    {
        String s1 = "";
        for(int i = 0; i < s.length(); i++)
        {
            String s2 = "";
            for(char c = s.charAt(i); Character.isDigit(c); c = s.charAt(i))
            {
                s2 = s2 + s.charAt(i);
                i++;
            }

            if(s2.compareTo("") != 0)
            {
                int j = Integer.parseInt(s2);
                s1 = s1 + (char)j;
            }
        }

        return s1;
    }

    public String geraDados(String s, String s1, String s2, String s3, String s4, String s5, String s6, 
            String s7, String s8, String s9, String s10, String s11, String s12, String s13, 
            String s14, String s15, String s16, String s17)
    {
        s = s.toUpperCase();
        s4 = s4.toUpperCase();
        if(s.length() != TAM_COD_EMP)
            return "Erro: tamanho do codigo da empresa diferente de 26 posi\347\365es.";
        if(s4.length() != TAM_CHAVE)
            return "Erro: tamanho da chave da chave diferente de 16 posi\347\365es.";
        if(s1.length() < 1 || s1.length() > 8)
            return "Erro: n\372mero do pedido inv\341lido.";
        if(isNumeric(s1))
            s1 = PreencheZero(s1, 8);
        else
            return "Erro: numero do pedido n\343o \351 num\351rico.";
        if(s2.length() < 1 || s2.length() > 11)
            return "Erro: valor da compra inv\341lido.";
        int i = s2.indexOf(',');
        if(i != -1)
        {
            String s20 = s2.substring(i + 1);
            if(!isNumeric(s20))
                return "Erro: valor decimal n\343o \351 num\351rico.";
            if(s20.length() != 2)
                return "Erro: valor decimal da compra deve possuir 2 posi\347\365es ap\363s a virgula.";
            s2 = s2.substring(0, s2.length() - 3) + s20;
        } else
        {
            if(!isNumeric(s2))
                return "Erro: valor da compra n\343o \351 num\351rico.";
            if(s2.length() > 8)
                return "Erro: valor da compra deve possuir no m\341ximo 8 posi\347\365es antes da virgula.";
            s2 = s2 + "00";
        }
        s2 = PreencheZero(s2, 10);
        s6 = s6.trim();
        if(s6.compareTo("02") != 0 && s6.compareTo("01") != 0 && s6.compareTo("") != 0)
            return "Erro: c\363digo de inscri\347\343o inv\341lido.";
        if(s7.compareTo("") != 0 && !isNumeric(s7) && s7.length() > 14)
            return "Erro: n\372mero de inscri\347\343o inv\341lido.";
        if(s10.compareTo("") != 0 && (!isNumeric(s10) || s10.length() != 8))
            return "Erro: cep inv\341lido.";
        if(s13.compareTo("") != 0 && (!isNumeric(s13) || s13.length() != 8))
            return "Erro: data de vencimento inv\341lida.";
        if(s15 != null && s15.length() > 60)
            return "Erro: observa\347\343o adicional 1 inv\341lida.";
        if(s16 != null && s16.length() > 60)
            return "Erro: observa\347\343o adicional 2 inv\341lida.";
        if(s17 != null && s17.length() > 60)
        {
            return "Erro: observa\347\343o adicional 3 inv\341lida.";
        } else
        {
            s3 = PreencheBranco(s3, 40);
            s5 = PreencheBranco(s5, 30);
            s6 = PreencheBranco(s6, 2);
            s7 = PreencheBranco(s7, 14);
            s8 = PreencheBranco(s8, 40);
            s9 = PreencheBranco(s9, 15);
            s10 = PreencheBranco(s10, 8);
            s11 = PreencheBranco(s11, 15);
            s12 = PreencheBranco(s12, 2);
            s13 = PreencheBranco(s13, 8);
            s14 = PreencheBranco(s14, 60);
            s15 = PreencheBranco(s15, 60);
            s16 = PreencheBranco(s16, 60);
            s17 = PreencheBranco(s17, 60);
            String s18 = Algoritmo(s1 + s2 + s3 + s5 + s6 + s7 + s8 + s9 + s10 + s11 + s12 + s13 + s14 + s15 + s16 + s17, s4);
            String s19 = Algoritmo(s + s18, CHAVE_ITAU);
            return Converte(s19);
        }
    }

    public String geraCripto(String s, String s1, String s2)
    {
        if(s.length() != TAM_COD_EMP)
            return "Erro: tamanho do codigo da empresa diferente de 26 posi\347\365es.";
        if(s2.length() != TAM_CHAVE)
            return "Erro: tamanho da chave da chave diferente de 16 posi\347\365es.";
        s1 = s1.trim();
        if(s1.compareTo("") == 0)
        {
            return "Erro: c\363digo do sacado inv\341lido.";
        } else
        {
            String s3 = Algoritmo(s1, s2);
            String s4 = Algoritmo(s + s3, CHAVE_ITAU);
            return Converte(s4);
        }
    }

    public String geraConsulta(String s, String s1, String s2, String s3)
    {
        if(s.length() != TAM_COD_EMP)
            return "Erro: tamanho do codigo da empresa diferente de 26 posi\347\365es.";
        if(s3.length() != TAM_CHAVE)
            return "Erro: tamanho da chave da chave diferente de 16 posi\347\365es.";
        if(s1.length() < 1 || s1.length() > 8)
            return "Erro: n\372mero do pedido inv\341lido.";
        if(isNumeric(s1))
            s1 = PreencheZero(s1, 8);
        else
            return "Erro: numero do pedido n\343o \351 num\351rico.";
        if(s2.compareTo("0") != 0 && s2.compareTo("1") != 0)
        {
            return "Erro: formato inv\341lido.";
        } else
        {
            String s4 = Algoritmo(s1 + s2, s3);
            String s5 = Algoritmo(s + s4, CHAVE_ITAU);
            return Converte(s5);
        }
    }

    public String decripto(String s, String s1)
    {
        s = Desconverte(s);
        String s2 = Algoritmo(s, s1);
        codEmp = s2.substring(0, 26);
        numPed = s2.substring(26, 34);
        tipPag = s2.substring(34, 36);
        return s2;
    }

    public String retornaCodEmp()
    {
        return codEmp;
    }

    public String retornaPedido()
    {
        return numPed;
    }

    public String retornaTipPag()
    {
        return tipPag;
    }

    public String geraDadosGenerico(String s, String s1, String s2)
    {
        s = s.toUpperCase();
        s2 = s2.toUpperCase();
        if(s.length() != TAM_COD_EMP)
            return "Erro: tamanho do codigo da empresa diferente de 26 posi\347\365es.";
        if(s2.length() != TAM_CHAVE)
            return "Erro: tamanho da chave da chave diferente de 16 posi\347\365es.";
        if(s1.length() < 1)
        {
            return "Erro: sem dados.";
        } else
        {
            String s3 = Algoritmo(s1, s2);
            String s4 = Algoritmo(s + s3, CHAVE_ITAU);
            return Converte(s4);
        }
    }

    private int sbox[];
    private int key[];
    private String codEmp;
    private String numPed;
    private String tipPag;
    private String CHAVE_ITAU;
    private int TAM_COD_EMP;
    private int TAM_CHAVE;
//    private String dados;
    public String numbers;
}
