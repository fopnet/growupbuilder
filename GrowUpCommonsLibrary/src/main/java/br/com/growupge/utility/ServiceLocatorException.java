/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : J2EE 1.4
 * Criado em  : 06/02/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 06/02/2006 - In�cio de tudo, por J�lio Vitorino
 * 
 */

package br.com.growupge.utility;

/**
 * Classe de Exce��o para a classe ServiceLocator
 * 
 * @author J�lio Cesar Vitorino
 * @version 1.0
 * 
 */
public class ServiceLocatorException extends Exception {
	
	private static final long serialVersionUID = 4099933416621370745L;

	/**
	 * @param exc
	 */
	public ServiceLocatorException(Exception exc) {
		super(exc);
	}
}
