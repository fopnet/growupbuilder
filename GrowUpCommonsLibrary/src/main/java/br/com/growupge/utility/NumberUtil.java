package br.com.growupge.utility;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * @author Felipe
 *
 */
public class NumberUtil {

	public static String formatarMoeda(Double dbl) {
		NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
		nf.setMinimumFractionDigits(2);
		return nf.format(dbl);
	}
	
	public static String formatarDecimal(Double dbl) {
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		nf.setMinimumFractionDigits(2);
		return nf.format(dbl);
	}

	public static final boolean isNumeric(final String s) {
		final char[] numbers = s.toCharArray();
		for (int x = 0; x < numbers.length; x++) {
			final char c = numbers[x];
			if ((c >= '0') && (c <= '9'))
				continue;
			return false; // invalid
		}
		return true; // valid
	}

	public static final boolean isDecimal(final String s) {
		return s != null && s.matches("^[-+]?\\d+(\\.\\d+)?$") ? true : false;
	
		/* Modo 2 - para entrada do usu�rio */
		// return
		// ((theNum+'').match(/^(((+|-)?\d+(\.\d*)?)|((+|-)?(\d*\.)?\d+))$/) !=
		// null);
		// Modo 3 - L�gica Algoritmo
		/*
		 * boolean isValid = true;
		 *  // Initialise counters int count = 0; int index = 0;
		 *  // Loop round all characters in the input or until invald character
		 * detected while (index < userInput.length() && isValid) {
		 *  // Get the next character from the user input char c =
		 * userInput.charAt(index);
		 *  // Check if it's a decimal point if (c == 46) { count ++;
		 *  // Invalid if there's been more than one if (count > 1) { isValid =
		 * false; }
		 *  // Check if it's less than the ASCII for '0' or greater than the
		 * ASCII for '9' } else if (c < 48 || c > 57) { isValid = false; }
		 *  // Increment the character counter index ++; }
		 * 
		 * return isValid;
		 */
	}
}
