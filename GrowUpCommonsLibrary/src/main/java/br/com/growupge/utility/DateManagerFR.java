/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 19/08/2005
 *
 * Historico de Modifica��o:
 * =========================
 * 19/08/2005 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.utility;

import java.text.SimpleDateFormat;

public class DateManagerFR extends DateUtil {

	private static final long serialVersionUID = 3882049246882753842L;

	/**
	 * Construtor para esta classe.
	 *
	 */
	public DateManagerFR() {
		super(new SimpleDateFormat());
	}

	public String getDayOfWeekShort() {
		return super.getDayOfWeekShort("DimLunMarMerJeuVenSam");
	}

	public String getDayOfWeekLong() {
		return super.getDayOfWeekLong( "Dimanche Lundi    Mardi    Mercredi Jeudi    Vendredi Samedi   ");
	}

	public String getMonthName() {
		return super.getMonthName("Janvier   F�vrier   Mars      Avril     Mai       Juin      Juillet   Ao�t      Septembre Octobre   Novembre  D�cembre  ");
	}

}
