/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 16/02/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 16/02/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.utility;

import java.awt.Toolkit;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Felipe
 *
 */
public class TimerObserver {

    private Timer timer = null;
    private int miliSegundos;


    /**
     * Construtor para esta classe.
     *
     * @param milliSegundos
     */
    public TimerObserver(int milliSegundos) {
        this.timer 	  = new Timer();
        this.miliSegundos = milliSegundos;
    }

    /**
     *
     * @param tarefa
     * @param milisegundos
     * @throws InterruptedException Comentar aqui.
     */
    public void adicionarTarefa(TimerTask tarefa, int milisegundos) throws InterruptedException {
    	this.timer.schedule(tarefa, 0 , milisegundos);
    }

    /**
     *
     * @param tarefa
     * @throws InterruptedException Comentar aqui.
     */
    public void adicionarTarefa(TimerTask tarefa) throws InterruptedException {
    	this.timer.schedule(tarefa, 0, this.miliSegundos);
    }

    /**
     * Comentar aqui.
     */
    public final void limparTarefas() {
    	this.timer.purge();
    }

    /**
     * Comentar aqui.
     */
    public final void stop(){
    	this.timer.cancel();
    }

    class RemindTask extends TimerTask {
        private int numWarningBeeps = 3;

        /**
         * M�todo sobrescrito.
         * @see java.util.TimerTask#run()
         */
        public void run() {
            if (this.numWarningBeeps > 0) {
            	Toolkit.getDefaultToolkit().beep();
                System.out.println("Beep!");
                this.numWarningBeeps--;
            } else {
            	Toolkit.getDefaultToolkit().beep();
                System.out.println("Time's up!");
                /*
                 * Not necessary because
                 * we call System.exit
                 * Stops the AWT thread
                 * (and everything else)
                 * */
                //timer.cancel();
/*                try {
                	this.wait();
				} catch (InterruptedException e) {
					System.out.println("InterruptedException");
				}*/
                System.exit(0);

            }
        }
    }
}
