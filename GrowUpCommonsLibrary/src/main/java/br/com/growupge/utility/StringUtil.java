package br.com.growupge.utility;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
	private static final String __CARACTERES_INVALIDOS = "!@#$%�&*()�`;:'}{[]^~-.,<>?/\\| ";

	private static final String __CARACTERES_CONVERTER = "���������������������������������������������������������������";

	private static final String __CARACTERES_CONVERTIDOS = "aeiouaeiouaeiouaooucaoAEIOUAEIOUAEIOUAOOUCAOAAEIDNYOPaaeidnyopy";

	/**
	 * Converte vetor de bytes para Hexadecimal
	 * 
	 * @param digestBits
	 * @param len
	 * @return Comentar aqui.
	 */
	public final static String bytetoHex(final byte[] digestBits, final int len) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < len; i++) {
			char c1, c2;

			c1 = (char) ((digestBits[i] >>> 4) & 0xf);
			c2 = (char) (digestBits[i] & 0xf);
			c1 = (char) ((c1 > 9) ? 'a' + (c1 - 10) : '0' + c1);
			c2 = (char) ((c2 > 9) ? 'a' + (c2 - 10) : '0' + c2);
			sb.append(c1);
			sb.append(c2);
		}
		return sb.toString();
	}

	/**
	 *
	 * removerCaracteresInvalidos - Remove caracteres indesejados baseados em
	 *                              __CARACTERES_INVALIDOS
	 *
	 * @param String
	 *
	 * @return String
	 *
	 */
	public static String removerCaracteresInvalidos(String cStringRemover) {
		int nTamanho = cStringRemover.length();
		StringBuffer cRetorno = new StringBuffer(nTamanho);

		// remove os caracteres invalidos e os substitui por undeline
		for (int i = 0; i < nTamanho; i++) {
			int nPosicao = __CARACTERES_INVALIDOS.indexOf(cStringRemover.substring(i, i + 1));
			if (nPosicao == -1)
				cRetorno.append(cStringRemover.substring(i, i + 1));
			else
				cRetorno.append("");
		}

		// Retorna a string somente com caracteres validos
		return (cRetorno.toString());
	}

	/**
	 * @param cStringRemover
	 * @return String
	 */
	public static String removerAcentos(String cStringRemover) {
		int nTamanho = cStringRemover.length();
		StringBuffer cRetorno = new StringBuffer(nTamanho);

		for (int i = 0; i < nTamanho; i++) {
			int nPosicao = __CARACTERES_CONVERTER.indexOf(cStringRemover.substring(i, i + 1));
			if (nPosicao == -1)
				cRetorno.append(cStringRemover.substring(i, i + 1));
			else
				cRetorno.append(__CARACTERES_CONVERTIDOS.substring(nPosicao, nPosicao + 1));
		}

		// Retorna a string somente com caracteres convertidos
		return (cRetorno.toString());
	}

	/**
	 *
	 * rPad - Retorna uma cadeia de caracteres com o Caracter preenchido a direita
	 *
	 * @param String, String, int
	 *
	 * @return String
	 *
	 */
	public static String rPad(String cString, String sCaracter, int iTamanho) {
		StringBuffer sRetorno = new StringBuffer(iTamanho);
		StringBuffer sFillChar = new StringBuffer(iTamanho);

		// Popula o campo sFillChar com o caracter
		for (int i = 0; i < iTamanho; i++) {
			sFillChar.append(sCaracter);
		}

		// Se a string recebida exceder o tamanho desejado para alinhamento
		// devolve a string original
		if (cString.trim().length() > iTamanho) {
			sRetorno.append(cString);
		} else {
			sRetorno.append(cString.trim()).append(
					sFillChar.substring(0, iTamanho - cString.trim().length()));
		}

		// Retorno
		return (sRetorno.toString());
	}

	/**
	 *
	 * lPad - Retorna uma cadeia de caracteres com o Caracter preenchido a esquerda
	 *
	 * @param String, String, int
	 *
	 * @return String
	 *
	 */
	public static String lPad(String cString, String sCaracter, int iTamanho) {
		StringBuffer sRetorno = new StringBuffer(iTamanho);
		StringBuffer sFillChar = new StringBuffer(iTamanho);

		// Popula o campo sFillChar com o caracter
		for (int i = 0; i < iTamanho; i++) {
			sFillChar.append(sCaracter);
		}

		// Se a string recebida exceder o tamanho desejado para alinhamento
		// devolve a string original
		if (cString.trim().length() > iTamanho) {
			sRetorno.append(cString);
		} else {
			sRetorno.append(sFillChar.substring(0, iTamanho - cString.trim().length())).append(
					cString.trim());
		}

		// Retorno
		return (sRetorno.toString());
	}

	/**
	 *
	 * ReplaceInString - Substitui em uma string a ocorrencia de uma string por outra
	 *
	 * @param String, String, String
	 *
	 * @return String
	 *
	 */
	public static String replaceRegex(String cStr, String cRegEx, String conteudo) {
		String cRetorno;
		Pattern p = Pattern.compile(cRegEx);
		Matcher m = p.matcher(cStr);
		cRetorno = m.replaceAll(conteudo);
		return cRetorno;
	}

	public static String replaceString(String cStr, String oldString, String newString) {
		String p1;
		String p2;
		int pos = cStr.indexOf(oldString);

		if (pos == -1)
			return cStr;

		p1 = cStr.substring(0, pos);
		p2 = cStr.substring(pos + oldString.length());

		StringBuffer resultado = new StringBuffer();
		resultado.append(p1).append(newString).append(p2);
		return replaceString(resultado.toString(), oldString, newString);
	}

	public static String extractString(String cstr, String search, int length) {
		int npos = cstr.indexOf(search);
		if (npos > -1) {
			String ret = cstr.substring(npos, npos + search.length() + length);
			return ret;
		} else
			return "";
	}

	public static String cut(String stringCut) {
		return cut(stringCut, 60);
	}

	public static String cut(String stringCut, int maxLen) {
		String retorno = null;

		if (stringCut.length() > maxLen) {
			retorno = stringCut.substring(0, maxLen) + "...";
		} else {
			retorno = stringCut.trim();
		}

		return retorno;
	}

	/*
	 * Converts a byte to hex digit and writes to the supplied buffer
	 */
	private void byte2hex(byte b, StringBuffer buf) {
		char[] hexChars = {
				'0',
				'1',
				'2',
				'3',
				'4',
				'5',
				'6',
				'7',
				'8',
				'9',
				'A',
				'B',
				'C',
				'D',
				'E',
				'F' };
		int high = ((b & 0xf0) >> 4);
		int low = (b & 0x0f);
		buf.append(hexChars[high]);
		buf.append(hexChars[low]);
	}

	/*
	 * Converts a byte array to hex string
	 */
	public String toHexString(byte[] block) {
		StringBuffer buf = new StringBuffer();

		int len = block.length;

		for (int i = 0; i < len; i++) {
			byte2hex(block[i], buf);
			if (i < len - 1) {
				buf.append(":");
			}
		}
		return buf.toString();
	}

	/**
	 * Retorna o stack trace da exception
	 * 
	 * @param ex
	 *            Caso ocorra algum erro na opera��o
	 */
	public static String getStackTrace(final Throwable ex) {
		StringWriter sw = new StringWriter();
		ex.printStackTrace(new PrintWriter(sw));
		return sw.getBuffer().toString();
	}

	/*
	 * Verifica se o character � especial
	 * */
	public static boolean isCharEspecial(char c) {

		if (__CARACTERES_INVALIDOS.indexOf(c) == -1) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * @param nome
	 * @return
	 */
	public static String removerAcentosEspacos(String nome) {
		if (nome == null) return nome;
		return removerAcentos(nome).replaceAll(" ", "");
	}
	
	public static String encodeParam(String param, String val)  {
		try {
			return param + "=" + java.net.URLEncoder.encode(val, "UTF-8") + "&";
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}

	public static String decodeParam(String val)  {
		try {
			return java.net.URLDecoder.decode(val, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}

}
