/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 19/08/2005
 *
 * Historico de Modifica��o:
 * =========================
 * 19/08/2005 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.utility;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

public class DateManagerPayPal extends DateManagerUS {

	private static final long serialVersionUID = 6226560563400652022L;

	/**
	 * Construtor para esta classe.
	 *
	 */
	public DateManagerPayPal() {
		super(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.US));
//		super(new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.US));
		
//		cal = new GregorianCalendar(TimeZone.getTimeZone("MST+7"));
//		cal.setTimeZone(new SimpleTimeZone(7, "GMT+7") );
		cal.setTimeZone(TimeZone.getTimeZone("GMT-7") );
	}

}
