/**
 * 
 */
package br.com.growupge.utility;

import static br.com.growupge.utility.StringUtil.encodeParam;
import br.com.growupge.constantes.Constantes;

/**
 * @author Felipe
 *
 */
public class CodificadorUrl {

	public static ObrigatorioCodigo criarUrlPedido(String url) {
		return new ObrigatorioCodigo(url);
	}
	
	static abstract class UrlPrivado {
		protected String url;
		
		UrlPrivado(String url) {
			this.url = url;
		}
		
	}
	
	public static class ObrigatorioCodigo extends UrlPrivado  {
		
		ObrigatorioCodigo(String url) {
			super(url);
		}
		
		public ObrigatorioEmail codificadorLogin(String login) {
			url += encodeParam(Constantes.PARAM_USUARIO, login);  
			return new ObrigatorioEmail(url);
		}
	}
	
	public static class ObrigatorioEmail extends UrlPrivado {
		
		ObrigatorioEmail(String url) {
			super(url);
		}
		
		public UrlPublico codificadorEmail(String email) {
			url += encodeParam(Constantes.PARAM_EMAIL, email);  
			return new UrlPublico(url);
		}
	}
	
	public static class UrlPublico extends UrlPrivado {
		UrlPublico(String url) {
			super(url);
		}
		
		public String getUrl() {
			return super.url;
		}
	}
}
