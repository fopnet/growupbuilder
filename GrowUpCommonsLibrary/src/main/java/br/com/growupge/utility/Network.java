/*
 * Created on 01/02/2006
 *
 */
package br.com.growupge.utility;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author elmt
 *
 */
public class Network 
{
	public static String getIPNumber()
	{
		try
		{
			InetAddress iAddr = InetAddress.getLocalHost();
			StringBuffer addrStr = new StringBuffer();
			byte[] bytes = iAddr.getAddress();
			for(int cnt = 0; cnt < bytes.length; cnt++)
			{ 
			   int uByte = bytes[cnt] < 0 ? bytes[cnt] + 256 : bytes[cnt]; 
			   addrStr.append(uByte); 
			   if(cnt < 3) addrStr.append('.'); 
			 } 
			 return addrStr.toString();
		} 
		catch (UnknownHostException uhe)
		{
			 return null;
		}
	}
	
	public static String getLocalHostname()
	{
		try 
		{ 
	       InetAddress addr = InetAddress.getLocalHost(); 
	       return addr.getHostName();
		} 
		catch (UnknownHostException e) 
		{ 
			return getIPNumber();
		}

	}

}
