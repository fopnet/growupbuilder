/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 22/01/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 22/01/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.utility;

import br.com.growupge.enums.TipoCaracter;

import java.util.Random;

/**
 * Cria um caracter rand�mico de acordo com a regra da petrobr�s.
 *
 * @author Felipe
 *
 */
public class GrowupRandom extends Random {

	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 7245056049906176245L;

	/**
	 * Atributo '<code>valorMinimo</code>' do tipo short
	 */
	private short valorMinimo;

	/**
	 * Atributo '<code>valorMaximo</code>' do tipo short
	 */
	private short valorMaximo;

	/**
	 * Construtor para esta classe.
	 *
	 * @param tpChar
	 */
	public GrowupRandom(TipoCaracter tpChar) {
		super();

		switch (tpChar) {
		case NUMERICO:
			this.valorMinimo = 0x30;
			this.valorMaximo = 0x39;
			break;
		case MINUSCULO:
			this.valorMinimo = 0x61;
			this.valorMaximo = 0x7A;
			break;
		case MAIUSCULO:
			this.valorMinimo = 0x41;
			this.valorMaximo = 0x5A;
			break;
		}
	}

	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'valorMaximo'.
	 * @return Retorna um objeto do tipo short
	 */
	public final short getValorMaximo() {
		return this.valorMaximo;
	}

	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'valorMinimo'.
	 * @return Retorna um objeto do tipo short
	 */
	public final short getValorMinimo() {
		return this.valorMinimo;
	}

	/**
	 * Retorna um caracter aleat�rio de acordo com o TipoCaracter passado construtor
	 *
	 * @return Retorna um primitivo do tipo char
	 */
	public final char nextChar() {
		return (char) (this.valorMinimo + this.nextInt(this.valorMaximo - this.valorMinimo));
	}

}

