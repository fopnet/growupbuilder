/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 19/08/2005
 *
 * Historico de Modifica��o:
 * =========================
 * 19/08/2005 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.utility;

import java.text.SimpleDateFormat;

public class DateManagerES extends DateUtil {

	private static final long serialVersionUID = 8512491350376489682L;

	/**
	 * Construtor para esta classe.
	 *
	 */
	public DateManagerES() {
		super(new SimpleDateFormat());
	}

	public String getDayOfWeekShort() {
		return super.getDayOfWeekShort( "DomLunMarMieJueVieSab" );
	}

	public String getDayOfWeekLong() {
		return super.getDayOfWeekLong( "Domingo  Lunes    Martes   Mi�rcolesJueves   Viernes  S�bado   ");
	}

	public String getMonthName() {
		return super.getMonthName( "Enero     Febrero   Marzo     Abril     Mayo      Junio     Julio     Agosto    SeptiembreOctubre   Noviembre Deciembre ");
	}

}
