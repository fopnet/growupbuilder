/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : J2EE 1.4
 * Criado em  : 07/05/2004
 *
 * Historico de Modifica��o:
 * =========================
 * 07/05/2004 - In�cio de tudo, por J�lio Vitorino
 *
 */

package br.com.growupge.utility;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.KeyStroke;

/**
 * Classe Utilit�ria
 *
 * @author J�lio Vitorino
 * @version 1.0
 *
 */
public class Diversos {

	/* Constantes */
	private final int ITEM_CHECK = 1;

	private final int ITEM_RADIO = 2;

	/**
	 *
	 * fatorial - Calcula o fatorial de um numero
	 *
	 * @param int fatorial a ser calculado
	 *
	 * @return int resultado com fatorial calculado de nFatorial
	 *
	 */
	public static long fatorial(final int nFatorial) {
		if (nFatorial == 0) {
			return 1;
		}
		return nFatorial * Diversos.fatorial(nFatorial - 1);
	}

	/**
	 *
	 * Combinacao - Combina��o de n N�meros de k em k
	 *
	 * @param int, int
	 *
	 * @return long
	 *
	 */
	public static long combinacao(int n, int k) {
		return fatorial(n) / (fatorial(k) * fatorial(n - k));
	}

	public JMenuItem createMenuItem(
			int iType,
			String sText,
			ImageIcon image,
			KeyStroke acceleratorKey,
			String sToolTip,
			ActionListener action) {
		// create JMenuItem
		JMenuItem menuItem;
		switch (iType) {
		case ITEM_RADIO:
			menuItem = new JRadioButtonMenuItem();
			break;
		case ITEM_CHECK:
			menuItem = new JCheckBoxMenuItem();
			break;
		default:
			menuItem = new JMenuItem();
			break;
		}

		// set text
		menuItem.setText(sText);

		// add an optional icon
		if (image != null)
			menuItem.setIcon(image);

		// add an optional accelerator key
		if (acceleratorKey != null)
			menuItem.setAccelerator(acceleratorKey);
		//			menuItem.setMnemonic( acceleratorKey );

		// add an optional tooltip text
		if (sToolTip != null)
			menuItem.setToolTipText(sToolTip);

		// add event listener
		menuItem.addActionListener(action);

		return menuItem;
	}

	public JButton createButton(
			String caption,
			ImageIcon icon,
			ActionListener al,
			String tooltip) {
		JButton c1;
		if ((icon == null) && (caption != null))
			c1 = new JButton(caption);
		else if ((icon != null) && (caption != null))
			c1 = new JButton(caption, icon);
		else
			c1 = new JButton(icon);

		c1.setMargin(new Insets(0, 0, 0, 0));
		if (al != null)
			c1.addActionListener(al);
		if (tooltip != null)
			c1.setToolTipText(tooltip);
		return c1;
	}

	public static GridBagConstraints getGridBagConstrainst(
			int gridx,
			int gridy,
			int anchor) {
		GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = gridx;
		gridBagConstraints.gridy = gridy;
		gridBagConstraints.anchor = anchor;
		gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 5);
		return gridBagConstraints;
	}

} // end Diversos class
