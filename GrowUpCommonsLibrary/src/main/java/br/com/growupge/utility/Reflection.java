package br.com.growupge.utility;


import static org.apache.commons.lang3.reflect.FieldUtils.readDeclaredField;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.DTOPadrao;
import br.com.growupge.dto.MensagemRetornoDTO;
import br.com.growupge.dto.Parameter;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;

/**
 * Classe de manipulacao de reflex�o
 *
 * @author Felipe
 *
 */
public class Reflection implements Serializable {
	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(Reflection.class);

	/**
	 * Atribiu um valor em um dto 
	 *
	 * @param dto dados
	 * @param param objeto do tipo parameter que possui o valor e o atributo
	 * @throws SigemException Comentar aqui.
	 */
	public static void setFieldValue(MensagemRetornoDTO dto, Parameter param) throws GrowUpException {
		try {
			if (dto != null && param != null) {
				Field fld = dto.getClass().getField(param.getAtributo());
				fld.set(dto, param.getValor());
			}
		} catch (SecurityException e) {
			dto.parametros = new HashMap<String, String>();
			dto.parametros.put(Constantes.P1, param.getAtributo());
			throw new GrowUpException(MSGCODE.ERRO_ATRIBUTO_NAO_ENCONTRADO, dto.parametros);
		} catch (NoSuchFieldException e) {
			dto.parametros = new HashMap<String, String>();
			dto.parametros.put(Constantes.P1, param.getAtributo());
			throw new GrowUpException(MSGCODE.ERRO_ATRIBUTO_NAO_ENCONTRADO, dto.parametros);
		} catch (IllegalAccessException e) {
			dto.parametros = new HashMap<String, String>();
			dto.parametros.put(Constantes.P1, param.getAtributo());
			throw new GrowUpException(MSGCODE.ERRO_ATRIBUTO_NAO_ENCONTRADO, dto.parametros);
		}
	}

	/**
	 * Cria uma nova instancia de qualquer objeto
	 *
	 * @param pckname nome do pacote
	 * @param classname nome da classe
	 * @param parameterTypes 
	 * @return
	 * @throws SigemException Comentar aqui.
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getNewInstance(final String pckname, final String classname, 
										final Object... initargs) throws GrowUpException {
		try {
			Class<?> cls = Class.forName(pckname.concat(".").concat(classname));
			List<Class<?>> parameterTypes = new ArrayList<Class<?>>();
			for (Object object : initargs) {
				parameterTypes.add(object.getClass());
			}
			
			if (parameterTypes.isEmpty())
				return (T) cls.newInstance();
			else {
				Constructor<?> constructor = getConstructor(cls, parameterTypes.toArray(new Class<?>[parameterTypes.size()]));
				return constructor != null ? (T)  constructor.newInstance(initargs) : null;
				
			}

		} catch (InstantiationException e) {
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
		} catch (IllegalAccessException e) {
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
		} catch (Exception e) {
			throw new GrowUpException(MSGCODE.ERRO_CLASSE_NAO_ENCONTRADA);
		} 
	}

	public static Object invocarMetodo(Object obj, String nomeMetodo, Class<?>[] types, Object[] params)
			throws SecurityException, NoSuchMethodException, IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		Method metodo = obj.getClass().getMethod(nomeMetodo, types);
		return metodo.invoke(obj, params);
	}

	public static void traceMethods(Object obj) {
		for (Method m : obj.getClass().getMethods()) {
			System.out.println(m.toString());
		}
	}
	
	/**
	 * Retorna a propriedade do objeto de acordo com o idioma do usu�rio.
	 * Usa a propriedade padrao 'descricao'
	 * 
	 * @param dto
	 * @param usr
	 * @param propertyName
	 * @return
	 */
	public static String getLocaleProperty(DTOPadrao dto, UsuarioDTO usr) {
		return getLocaleProperty(dto, usr, "descricao");
	}
	
	/**
	 * Retorna a propriedade do objeto de acordo com o idioma do usu�rio
	 * 
	 * @param dto
	 * @param usr
	 * @param propertyName
	 * @return
	 */
	public static String getLocaleProperty(DTOPadrao dto, UsuarioDTO usr, String propertyName) {
		return getLocaleProperty(dto, propertyName + usr.getIdioma());
	}

	/**
	 * Retorna a propriedade do objeto de acordo com o idioma do usu�rio
	 * 
	 * @param dto
	 * @param propertyName
	 * @param idioma
	 * @return
	 */
	public static String getLocaleProperty(DTOPadrao dto, String propertyName) {
		String result = null;
		try {
			Object obj = readDeclaredField(dto, propertyName);
			if (obj != null)
				result = obj.toString();
			
		} catch (IllegalAccessException e) {
			result = null;
		}
		return result;
	}
	
	public static boolean isCodigoValido(DTOPadrao bean) {
		return isPropriedadeValida(bean, "codigo");
	}
	
	public static boolean isPropriedadeValida(Object bean, String atributo) {
		if (bean == null)
			return false;
		
		Object value;
		try {
			value = PropertyUtils.getProperty(bean, atributo);

			if (String.class.isInstance(value) && StringUtils.isNotBlank((String) value)) 
				return true;
			else if (Long.class.isInstance(value) && Long.valueOf((Long)value) > 0l) 
				return true;
			
		} catch (Exception e) {
			logger.error(e, e);
			return false;
		}
		
		return false;
	}
	
	public static Object getAcessibleProperty(Object leftObject, String fieldName) {
		Method method = getFieldGetter(leftObject, fieldName);
		if (method != null) {
			try {
				return method.invoke(leftObject, new Object[0]);
			} catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
		
		return null;
	}
	
	public static String getGetterName(String fieldName) {
		return fieldName == null ? null : "get" +  StringUtils.capitalize(fieldName);
	}

	public static boolean isReadable(Object o, String fieldName) {
		return getFieldGetter(o, fieldName) != null;
	}
	
	public static Method getFieldGetter(Object o, String fieldName) {
		if(o==null) {
			throw new IllegalArgumentException("O argumento 'o' n�o pode ser nulo.");
		}
		String methodName = getGetterName(fieldName);
		Method getter = getMethod(o.getClass(), methodName);
		return getter;
	}

	public static Method getMethod(Class<?> clazz, String methodName, Class<?>... parametersType) {
		try {
			Method method = clazz.getDeclaredMethod(methodName, parametersType);
			method.setAccessible(true);
			return method;
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			Class<?> superClass = clazz.getSuperclass(); 
			if(superClass != null) {
				return getMethod(superClass, methodName, parametersType);
			}
			return null;
		}
	}

	public static Constructor<?> getConstructor(Class<?> clazz, Class<?>... parametersType) {
		try {
			for(Constructor<?> c : clazz.getConstructors()) {
				Constructor<?> matched = c;
				
				if (parametersType.length > 0) {
					for (Class<?> pType: parametersType) {
						for(Class<?> cpType : c.getParameterTypes()) {
							if (!cpType.isAssignableFrom(pType)) {
								matched = null;
								break;
							}
						}
					
						// Se tiver algum parametersType, for do mesmo tipo
						if (matched != null)
							return matched;
					}
				
				// Se n�o tiver nenhum parametersType, retorna o primeiro construtor;
				} else if (c.getParameterTypes().length == 0) {
					return matched;
				}
			}
			return null;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
