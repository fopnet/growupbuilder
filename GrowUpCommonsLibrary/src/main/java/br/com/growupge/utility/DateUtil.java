/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 19/08/2005
 *
 * Historico de Modifica��o:
 * =========================
 * 19/08/2005 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.utility;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import br.com.growupge.constantes.Constantes;

/**
 * @author Felipe Pina
 *
 */
public abstract class DateUtil implements Serializable {
	
	private static Logger logger = Logger.getLogger(DateUtil.class);
	
	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = -1920862687945236276L;

	/**
	 * Atributo '<code>df</code>' do tipo SimpleDateFormat
	 */
	private SimpleDateFormat df;

	/**
	 * Atributo '<code>cal</code>' do tipo Calendar
	 */
	protected Calendar cal = new GregorianCalendar();

	/**
	 * Construtor para esta classe.
	 *
	 */
	protected DateUtil(SimpleDateFormat df) {
		this(new Date(System.currentTimeMillis()), df); // get current system date
	}

	/**
	 * Construtor para esta classe.
	 *
	 * @param date
	 */
	protected DateUtil(Date date, SimpleDateFormat df) {
		// get current system date
		this.cal.setTime(date);
		this.df = df;
		this.df.setCalendar(cal);
	}

	public static DateUtil getDateManagerInstance() {
		return new DateManagerBR();
	}

	public static DateUtil getDateManagerInstance(final Date date) {
		return date == null ? null : new DateManagerBR(date);
	}
	
	public static DateUtil getDateManagerInstancePayPal() {
		return new DateManagerPayPal();
	}

	public static DateUtil getDateInstance(String language) {
		if (language.equals(Constantes.LANGUAGE_US))
			return new DateManagerUS();
		if (language.equals(Constantes.LANGUAGE_BR))
			return new DateManagerBR();
		if (language.equals(Constantes.LANGUAGE_ES))
			return new DateManagerES();
		if (language.equals(Constantes.LANGUAGE_FR))
			return new DateManagerFR();
		if (language.equals(Constantes.LANGUAGE_OT))
			return new DateManagerOT();
		return null;
	}

	public Date getDate(int dia, int mes, int ano) {
		return getDate(dia, mes, ano, 0, 0, 0);
	}

	public Date getDate(int dia, int mes, int ano, int hora, int minuto, int segundo) {
		return getDate(dia, mes, ano, hora, minuto, segundo, 0);
	}

	public Date getDate(int dia, int mes, int ano, int hora, int minuto, int segundo, int millis) {
		cal.set(ano, mes-1, dia, hora, minuto, segundo);
		cal.set(Calendar.MILLISECOND, millis);
		return cal.getTime();
	}
	
	public Date getDateTime() {
		return this.cal.getTime();
	}

	public java.sql.Time getSQLTime() {
		return new java.sql.Time(this.cal.getTimeInMillis());
	}
	public java.sql.Timestamp getSQLTimeStamp() {
		return new java.sql.Timestamp(this.cal.getTimeInMillis());
	}
	
	public java.sql.Date getSQLDate() {
		return new java.sql.Date(this.cal.getTimeInMillis());
	}

	public long getTimeInMillis() {
		return cal.getTimeInMillis();
	}

	public long getTimeLong() {
		return cal.getTime().getTime();
	}

	public String getDateFormat() {
		return df.format(cal.getTime());
	}

	public Integer getYear() {
		return cal.get(Calendar.YEAR);
	}

	public Integer getMonth() {
		return cal.get(Calendar.MONTH);
	}

	public Integer getDay() {
		return cal.get(Calendar.DAY_OF_MONTH);
	}

	public Integer getDayOfWeek() {
		return cal.get(Calendar.DAY_OF_WEEK); // 1=Sunday, 2=Monday, ...
	}

	public Integer getHour() {
		return cal.get(Calendar.HOUR_OF_DAY);
	}

	public Integer getMinutes() {
		return cal.get(Calendar.MINUTE);
	}

	public Integer getSeconds() {
		return cal.get(Calendar.SECOND);
	}


	public String toString() {
		return this.getShortDate();
	}

	public String getShortDate() {
		df.applyPattern("dd/MM/yyyy");
		return getDateFormat();
	}

	public String getLongDate() {
		df.applyPattern("dd/MM/yyyy HH:mm:ss:SSS");
		return getDateFormat();
	}

	/*
	 * Veja SimpleDateFormat para mais detalhes 
	 */
	public void setDatePattern(String pattern) throws ParseException {
		df.applyPattern(pattern);
	}

	public Date getDate(String formato) {
		try {
			Date dt = this.df.parse(formato);
			cal.setTime(dt);
			return dt;
		} catch (ParseException e) {
			logger.error(e,e);
		}
		return null;
	}
	
	public void addDays(int daysToAdd) {
		cal.add(GregorianCalendar.DAY_OF_MONTH, daysToAdd);
	}

	public void addMinutes(int minutesToAdd) {
		cal.add(GregorianCalendar.MINUTE, minutesToAdd);
	}

	public void decDays(int daysToDecrement) {
		cal.add(GregorianCalendar.DAY_OF_MONTH, (daysToDecrement * -1));
	}

	/**
	 * Retorna a diferenca em dias.
	 * @param d1
	 * @return
	 */
	public double diff(DateUtil d1) {
		return (Math
				.floor((this.getDateTime().getTime() - d1.getDateTime().getTime()) / 1000.0 / 86400.00) * -1) + 1;
	}
	
	public long diffSecond(Date dt) {
		return diffSecond(getDateTime().getTime() - dt.getTime());
	}

	public long diffMinutes(Date dt) {
		return diffMinutes(getDateTime().getTime() - dt.getTime());
	}

	public long diffHour(Date dt) {
		return diffHour(getDateTime().getTime() - dt.getTime());
	}

	/**
	 * @param l
	 * @return
	 */
	private long diffHour(long dif) {
		return diffMinutes(dif)/60;
	}

	/**
	 * @param l
	 * @return
	 */
	private long diffMinutes(long dif) {
		return diffSecond(dif)/60;
	}

	/**
	 * @param l
	 * @param i
	 * @return
	 */
	private long diffSecond(long dif) {
		return dif / 1000;
	}

	public long elapsedTime(long d2) {
		return (d2 - getTimeLong());
	}

	public abstract String getDayOfWeekLong();

	public abstract String getMonthName();

	/**
	 * @param string
	 * @return
	 */
	protected String getMonthName(String monthsofyear) {
		int start = getMonth() * 10 - 10;
		return monthsofyear.substring(start, start + 9).trim();
	}

	/**
	 * @param string
	 * @return
	 */
	protected String getDayOfWeekLong(String daysofweek) {
		int start = getDayOfWeek() * 9 - 9;
		return daysofweek.substring(start, start + 8).trim();
	}

	/**
	 * @param string
	 * @return
	 */
	protected String getDayOfWeekShort(String daysofweek) {
		int start = getDayOfWeek() * 3 - 3;
		return daysofweek.substring(start, start + 3);
	}


}
