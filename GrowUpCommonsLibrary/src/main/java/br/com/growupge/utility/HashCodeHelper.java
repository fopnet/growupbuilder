package br.com.growupge.utility;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.reflect.FieldUtils;


/**
 * @author EII5
 *
 */
public class HashCodeHelper {
	
	public static int hashCode(Object leftObject, String... fieldsToComparate) {
		if(fieldsToComparate.length == 0) {
			return HashCodeBuilder.reflectionHashCode(leftObject);
		}
		
		if (leftObject == null)
			throw new IllegalArgumentException();
				
		HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
		for (String fieldName : fieldsToComparate) {			
			
			if (FieldUtils.getField(leftObject.getClass(), fieldName, true) ==null){ 
				throw new IllegalArgumentException("O atributo " + fieldName + " n�o existe na classe " + leftObject.getClass().getName());
			}
			
			try {
				hashCodeBuilder.append( FieldUtils.readField(leftObject, fieldName, true) );
			} catch (Exception e) {
				throw new RuntimeException("Field " + fieldName + " n�o existe na classe " + leftObject.getClass());
			}
		}
		return hashCodeBuilder.toHashCode();
	}

}
