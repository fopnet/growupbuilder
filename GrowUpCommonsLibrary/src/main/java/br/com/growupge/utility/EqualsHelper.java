package br.com.growupge.utility;

import static org.apache.commons.beanutils.PropertyUtils.isReadable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.reflect.FieldUtils;

/**
 * @author EII5
 *
 */
public class EqualsHelper {
	
	public static boolean isEquals(Object leftObject, Object rightObject, String... fieldsToComparate) {
		if(fieldsToComparate.length == 0) {
			return EqualsBuilder.reflectionEquals(leftObject, rightObject);
		}
		
		if (leftObject == rightObject)
			return true;
		if (leftObject==null || rightObject == null)
			return false;
		if (!leftObject.getClass().isInstance(rightObject) && !rightObject.getClass().isInstance(leftObject))
			return false;
		
		EqualsBuilder equalsBuilder = new EqualsBuilder();
		for (String fieldName : fieldsToComparate) {
			try {
				if (!isReadable(leftObject, fieldName) || !isReadable(rightObject, fieldName)) {
					if(FieldUtils.getField(leftObject.getClass(), fieldName, true)==null 
							|| FieldUtils.getField(rightObject.getClass(), fieldName, true)==null) {
						return false;
					}
					equalsBuilder.append(FieldUtils.readField(leftObject, fieldName, true), 
										 FieldUtils.readField(rightObject, fieldName, true));
				} else {
					equalsBuilder.append(Reflection.getAcessibleProperty(leftObject, fieldName), 
										Reflection.getAcessibleProperty(rightObject, fieldName));
				}
			} catch (Exception e) {
				return false;
			}
		}
		return equalsBuilder.isEquals();
	}
	
	
	
}
