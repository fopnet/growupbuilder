/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 19/08/2005
 *
 * Historico de Modifica��o:
 * =========================
 * 19/08/2005 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.utility;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class DateManagerUS extends DateUtil {
	private static final long serialVersionUID = -4070545766296268422L;

	/**
	 * Construtor para esta classe.
	 *
	 */
	public DateManagerUS() {
		super(new SimpleDateFormat("MM-dd-yyyy hh:ss:mm", Locale.US));
	}
	
	protected DateManagerUS(SimpleDateFormat df) {
		super(df);
	}

	public String getDayOfWeekShort() {
		return super.getDayOfWeekShort("SunMonTueWedThuFriSat");
	}

	public String getDayOfWeekLong() {
		return super.getDayOfWeekLong("Sunday   Monday   Tuesday  WednesdayThursday Friday   Saturday ");
	}

	public String getMonthName() {
		return super.getMonthName("January   February  March     April     May       June      July      August    September October   November  December  ");
	}

}
