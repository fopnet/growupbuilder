/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 19/08/2005
 *
 * Historico de Modifica��o:
 * =========================
 * 19/08/2005 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.utility;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateManagerBR extends DateUtil {

	private static final long serialVersionUID = 6226560563400652022L;

	/**
	 * Construtor para esta classe.
	 *
	 */
	public DateManagerBR() {
		super(new SimpleDateFormat("dd-MM-yyyy hh:ss:mm", new Locale("pt", "BR")));
	}

	public DateManagerBR(Date date) {
		super(date, new SimpleDateFormat("dd-MM-yyyy hh:ss:mm", new Locale("pt", "BR")));
	}

	public String getDayOfWeekShort() {
		return super.getDayOfWeekShort("DomSegTerQuaQuiSexSab");
	}

	public String getDayOfWeekLong() {
		return super.getDayOfWeekLong( "Domingo Segunda Ter�a   Quarta  Quinta  Sexta   S�bado  ");
	}

	public String getMonthName() {
		return super.getMonthName( "Janeiro   Fevereiro Mar�o     Abril     Maio      Junho     Julho     Agosto    Setembro  Outubro   Novembro  Dezembro  ");
	}

}
