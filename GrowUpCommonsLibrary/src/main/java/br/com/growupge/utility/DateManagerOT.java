/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 19/08/2005
 *
 * Historico de Modifica��o:
 * =========================
 * 19/08/2005 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.utility;

import java.text.SimpleDateFormat;

public class DateManagerOT extends DateUtil {

	private static final long serialVersionUID = 4795564631271068676L;

	/**
	 * Construtor para esta classe.
	 *
	 */
	public DateManagerOT() {
		super(new SimpleDateFormat());
	}

	public String getDayOfWeekShort() {
		return super.getDayOfWeekShort( "DomSegTerQuaQuiSexSab" );
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.utility.DateUtil#getDayOfWeekLong()
	 */
	public String getDayOfWeekLong() {
		return super.getDayOfWeekLong("Domingo Segunda Ter�a   Quarta  Quinta  Sexta   S�bado  ");
	}

	public String getMonthName() {
		return null;
	}

}
