/**
 * 
 */
package br.com.growupge.validador;

/**
 * @author Felipe
 *
 */
public class Validador {

	/**
	 * Este m�todo � respons�vel por validar cpf do usuario.
	 * 
	 * @param value
	 *            Cpf digitado pelo usu�rio.
	 * @return Retorna um objeto do tipo Boolean
	 */
	public static final boolean validarCpf(final String value) {
		int soma = 0;
		String lcpf = retirarSinais(value);

		try {
			Long.parseLong(lcpf);
		} catch (Exception e) {
			return false;
		}

		if (lcpf.length() == 11) {
			for (int i = 0; i < 9; i++) {
				soma += (10 - i) * (lcpf.charAt(i) - '0');
			}

			soma = 11 - (soma % 11);
			if (soma > 9) {
				soma = 0;
			}
			if (soma == (lcpf.charAt(9) - '0')) {
				soma = 0;
				for (int i = 0; i < 10; i++) {
					soma += (11 - i) * (lcpf.charAt(i) - '0');
				}
				soma = 11 - (soma % 11);
				if (soma > 9) {
					soma = 0;
				}
				if (soma == (lcpf.charAt(10) - '0')) {
					return true;
				}
			}
		}
		return false;
	}

	public static String retirarSinais(final String value) {
		String lcpf = value.replace("-", "").replace(".", "");
		return lcpf;
	}

	public static final boolean validarCnpj(final String valor) {
		int soma = 0;
		
		String lcnpj = retirarSinais(valor);
		
		if(lcnpj.length() == 14) { 
			for (int i=0, j = 5; i < 12; i++) { 
				soma += j-- * (lcnpj.charAt(i) - '0'); 
				if (j < 2) j = 9; 
			} 
			soma = 11 - (soma % 11); 
			if (soma > 9) soma = 0; 
			if (soma == (lcnpj.charAt(12) - '0')) { 
				soma = 0; 
				for (int i=0, j = 6; i < 13; i++) { 
					soma += j-- * (lcnpj.charAt(i) - '0'); 
					if (j < 2) j = 9; 
				} 
				soma = 11 - (soma % 11); 
				if (soma > 9) soma = 0; 
				if (soma == (lcnpj.charAt(13) - '0')) { 
					return true; 
				} 
			} 
		} 
		return false;
	}
}
