package br.com.growupge.enums;

public enum TipoSanguineo {
	OPOSITIVO("O+"),
	ONEGATVO("O-"),
	APOSITIVO("A+"),
	ANEGATVO("A-"),
	BPOSITIVO("B+"),
	BNEGATVO("B-"),
	ABPOSITIVO("AB+"),
	ABNEGATVO("AB-");
	
	private String tipo;
	
	private TipoSanguineo (String tipo) {
		this.tipo = tipo.toUpperCase();
	}
	
	public static TipoSanguineo getEnum(String tipo) {
		TipoSanguineo enm = null;
		if (tipo.equals("O-")) {
			enm = TipoSanguineo.ONEGATVO;
		} else if (tipo.equals("O+")) {
			enm = TipoSanguineo.OPOSITIVO;
		} else if (tipo.equals("AB+")) {
				enm = TipoSanguineo.ABPOSITIVO;
		} else if (tipo.equals("AB-")) {
			enm = TipoSanguineo.ABNEGATVO;
		} else if (tipo.equals("A+")) {
			enm = TipoSanguineo.APOSITIVO;
		} else if (tipo.equals("A-")) {
			enm = TipoSanguineo.ANEGATVO;
		} else if (tipo.equals("B+")) {
			enm = TipoSanguineo.BPOSITIVO;
		} else if (tipo.equals("B-")) {
			enm = TipoSanguineo.BNEGATVO;
		}
		return enm;
	}
	
	public String toString() {
		return this.tipo;
	}
}
