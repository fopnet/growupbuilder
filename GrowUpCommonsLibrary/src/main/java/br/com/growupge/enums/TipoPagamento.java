/**
 * 
 */
package br.com.growupge.enums;

/**
 * @author Felipe
 *
 */
public enum TipoPagamento {

	//	Num�rico com 02 posi��es:
	A_VISTA("01"), //para pagamento � vista (TEF ou CDC)
	BOLETO("02"),//para boleto
	CARTAO_CREDITO("03"); // cart�o de cr�dito
	
	private String tipo;

	private TipoPagamento(String tipo) {
		this.tipo = tipo;
	}
	
	public String toString() {
		return this.name() + tipo;
	}
}
