/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : Mar 15, 2007
 *
 * Historico de Modifica��o:
 * =========================
 * Mar 15, 2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.enums;


import static org.apache.commons.lang3.StringUtils.isNoneBlank;
import static org.apache.commons.lang3.StringUtils.strip;
import br.com.growupge.dto.CombustivelDTO;

/*
 * 'GNV' 
 * 'GSO' 
 * 'ALC' 
 * 'FLX'
 * 'FGV'
 * 'GGV'
 * 'AGV'
 * 'TEF'
 * 'CNE'
 * @author Felipe
 *
 */
public enum TipoCombustivel {
	GNV("GNV", "G�s Natural Veicular"), 
	GASOLINA("GSO","Gasolina"), 
	ALCOOL("ALC", "�lcool") , 
	FLEX("FLX","Flex"),
	DIESEL("DIE","Diesel"),
	FLEX_GNV("FGV", "Flex + G�s"),
	GASOLINA_GNV("GGV", "Gasolina + G�s"),
	ALCOOL_GNV("AGV", "�lcool + G�s"),
	TETRA_FUEL("TEF", "Tetra Fuel"),
	NAO_ENCONTRADO("CNE", "N�o encontrado");

	private final String tipo;
	private String descricao;

	/**
	 * Construtor para esta classe.
	 *
	 * @param tipoVisao Parametro do TipoVisao
	 */
	private TipoCombustivel(final String tipoVisao, final String descricao) {
		this.tipo = tipoVisao;
		this.descricao = descricao;
	}

	/**
	 * Metodo retorna o enum TipoUsuario correspondente ao codigo
	 *
	 * @param tipo String identificando o codigo do enum
	 * @return Comentar aqui.
	 */
	public static TipoCombustivel getEnumById(final String tipo) {
		TipoCombustivel enm = null;

		if (tipo != null) {
			for (TipoCombustivel tp : TipoCombustivel.values()) {
				if (tp.toString().equals(tipo)) {
					return tp;
				}
			}
		}

		return enm;
	}

	public static TipoCombustivel getEnumByName(String desc) {
		TipoCombustivel enm = null;
		
		if (isNoneBlank(desc)) {
			for (TipoCombustivel tp : TipoCombustivel.values()) {
				if (strip(tp.descricao).contains(strip(desc))) {
					return tp;
				}
			}
		}
		
		return enm;
	}
	
	public CombustivelDTO toCombustivelDTO() {
		return new CombustivelDTO(this.tipo);
	}

	public String toString() {
		return this.tipo;
	}

	/**
	 * @return
	 */
	public String getDescricao() {
		return descricao;
	}

}
