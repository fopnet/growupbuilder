package br.com.growupge.enums;


/**
 * Os tipos de ordena��o do SQL a ser montado din�micamente.
 *
 * @author Felipe
 *
 */
public enum TipoOperadorDAO {
	IGUAL("="),
	DIFERENTE("<>"),
	MAIOR(">"),
	MENOR("<"),
	MAIOR_IGUAL(">="),
	MENOR_IGUAL("<=");

	private final String tipo;

	/**
	 * Construtor para esta classe.
	 *
	 * @param tipo Parametro do tipo String
	 */
	private TipoOperadorDAO(final String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Metodo retorna o enum TipoUsuario correspondente ao codigo
	 *
	 * @param tipo String identificando o codigo do enum
	 * @return Comentar aqui.
	 */
	public static TipoOperadorDAO getEnum(final String tipo) {
		TipoOperadorDAO enm = null;
		if (tipo.equals("=")) {
			enm = TipoOperadorDAO.IGUAL;
		} else if (tipo.equals("<")) {
			enm = TipoOperadorDAO.MENOR;
		} else if (tipo.equals("<=")) {
			enm = TipoOperadorDAO.MENOR_IGUAL;
		} else if (tipo.equals(">")) {
			enm = TipoOperadorDAO.MAIOR;
		} else if (tipo.equals(">=")) {
			enm = TipoOperadorDAO.MAIOR_IGUAL;
		} else if (tipo.equals("<>")) {
			enm = TipoOperadorDAO.DIFERENTE;
		}
		return enm;
	}

	/**
	 * M�todo sobrescrito.
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return this.tipo;
	}

}