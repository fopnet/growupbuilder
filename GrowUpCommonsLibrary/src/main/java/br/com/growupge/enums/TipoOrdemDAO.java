package br.com.growupge.enums;


/**
 * Os tipos de ordena��o do SQL a ser montado din�micamente.
 *
 * @author Felipe
 *
 */
public enum TipoOrdemDAO {
	ASC, DESC
}