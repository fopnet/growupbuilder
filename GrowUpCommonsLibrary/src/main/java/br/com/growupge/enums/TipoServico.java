/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : Mar 15, 2007
 *
 * Historico de Modifica��o:
 * =========================
 * Mar 15, 2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.enums;

/**TipoStatus
 * Classe de enum que exibe os tipos de Visao
 *
 * @author Felipe
 *
 */
public enum TipoServico {

	COMUNICACAO_VENDA("V"), 
	CONSTULTA_COMPLETA("C"), 
	CONSULTA_SIMPLES("S") , 
	PROCESSO_ADMINISTRATIVO("P");

	private final String tipo;

	/**
	 * Construtor para esta classe.
	 *
	 * @param tipoVisao Parametro do TipoVisao
	 */
	private TipoServico(final String tipoVisao) {
		this.tipo = tipoVisao;
	}

	/**
	 * Metodo retorna o enum TipoUsuario correspondente ao codigo
	 *
	 * @param tipo String identificando o codigo do enum
	 * @return Comentar aqui.
	 */
	public static TipoServico getEnum(final String tipo) {
		TipoServico enm = null;

		if (tipo != null) {
			if (tipo.equals("S")) {
				enm = TipoServico.CONSULTA_SIMPLES;
			} else if (tipo.equals("C")) {
				enm = TipoServico.CONSTULTA_COMPLETA;
			} else if (tipo.equals("V")) {
				enm = TipoServico.COMUNICACAO_VENDA;
			} else if (tipo.equals("P")) {
				enm = TipoServico.PROCESSO_ADMINISTRATIVO;
			}
		}

		return enm;
	}

	public String toString() {
		return this.tipo;
	}

}
