/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : Mar 15, 2007
 *
 * Historico de Modifica��o:
 * =========================
 * Mar 15, 2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.enums;

/**TipoStatus
 * Classe de enum que exibe os tipos de Visao
 *
 * @author Felipe
 *
 */
public enum TipoPerfil {

	ADMINISTRADOR("A"), EMPRESA("E") , USUARIO("U");

	private final String tipo;

	/**
	 * Construtor para esta classe.
	 *
	 * @param tipoVisao Parametro do TipoVisao
	 */
	private TipoPerfil(final String tipoVisao) {
		this.tipo = tipoVisao;
	}

	/**
	 * Metodo retorna o enum TipoUsuario correspondente ao codigo
	 *
	 * @param tipo String identificando o codigo do enum
	 * @return Comentar aqui.
	 */
	public static TipoPerfil getEnum(final String tipo) {
		TipoPerfil enm = null;

		if (tipo != null) {
			if (tipo.equals("A")) {
				enm = TipoPerfil.ADMINISTRADOR;
			} else if (tipo.equals("E")) {
				enm = TipoPerfil.EMPRESA;
			} else if (tipo.substring(0, 1).equals("U")) {
				enm = TipoPerfil.USUARIO;
			}
		}

		return enm;
	}

	public String toString() {
		return this.tipo;
	}

}
