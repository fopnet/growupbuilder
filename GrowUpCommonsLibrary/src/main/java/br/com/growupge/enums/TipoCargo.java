package br.com.growupge.enums;

public enum TipoCargo {
	CARGO1("C1"),
	CARGO2("C2");
	
	private String tipo;
	
	private TipoCargo (String tipo) {
		this.tipo = tipo.toUpperCase();
	}
	
	public static TipoCargo getEnum(String tipo) {
		TipoCargo enm = null;
		if (tipo.equals("C1")) {
			enm = TipoCargo.CARGO1;
		} else if (tipo.equals("C2")) {
			enm = TipoCargo.CARGO2;
		}
		return enm;
	}
	
	public String toString() {
		return this.tipo;
	}
}
