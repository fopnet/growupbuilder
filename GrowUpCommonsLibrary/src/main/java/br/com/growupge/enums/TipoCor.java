package br.com.growupge.enums;

/**
 * @author Felipe
 *
 */
public enum TipoCor {
	Branco, 
	Amarelo,
	Azul,
	Verde,
	Prata,
	Vermelho,
	Preto,
	Cinza,
	Rosa,
	Beje,
	Laranja;
	
	public String toString() {
		return name();
	}
}
