package br.com.growupge.enums;

/**
 * Valores poss�veis para Tipo de Idioma que o Sigem contempla
 *
 * @author Felipe
 *
 */
public enum TipoIdioma {
	IDIOMA_BR("BR"),
	IDIOMA_EN("EN");

	private final String tipo;

	/**
	 * Construtor para esta classe.
	 *
	 * @param tipo Parametro do tipo String
	 */
	private TipoIdioma(final String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Metodo retorna o enum TipoUsuario correspondente ao codigo
	 *
	 * @param tipo String identificando o codigo do enum
	 * @return Comentar aqui.
	 */
	public static TipoIdioma getEnum(final String tipo) {
		TipoIdioma defaultIdioma = TipoIdioma.IDIOMA_BR;
		for (TipoIdioma enm : TipoIdioma.values()) {
			if (enm.tipo.equals(tipo))
				return enm;
		} 
		return defaultIdioma;
	}

	/**
	 * M�todo sobrescrito.
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return this.tipo;
	}

}
