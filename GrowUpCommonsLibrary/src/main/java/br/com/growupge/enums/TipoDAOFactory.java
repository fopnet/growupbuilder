package br.com.growupge.enums;

public enum TipoDAOFactory {
	SqlServer("SQLSERVER"),
	FireBird("FIREBIRD"),
	Oracle("ORACLE");
	
	private String tipo;
	
	private TipoDAOFactory (String tipo) {
		this.tipo = tipo.toUpperCase();
	}
	
	public static TipoDAOFactory getEnum(String tipo) {
		TipoDAOFactory enm = null;
		if (tipo.equals("SQLSERVER")) {
			enm = TipoDAOFactory.SqlServer;
		} else if (tipo.equals("FIREBIRD")) {
			enm = TipoDAOFactory.FireBird;
		} else if (tipo.equals("ORACLE")) {
			enm = TipoDAOFactory.Oracle;
		}
		return enm;
	}
	
	public String toString() {
		return this.tipo;
	}
}
