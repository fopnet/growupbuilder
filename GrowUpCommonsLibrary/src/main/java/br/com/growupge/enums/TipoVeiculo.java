package br.com.growupge.enums;

/**
 * Valores poss�veis para Tipo de Idioma que o Sigem contempla
 *
 * @author Felipe
 *
 */
public enum TipoVeiculo {
	CARRO("Autom�vel"),
	CAMINHAO("Caminh�o"),
	MOTO("Moto");

	private final String tipo;

	/**
	 * Construtor para esta classe.
	 *
	 * @param tipo Parametro do tipo String
	 */
	private TipoVeiculo(final String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Metodo retorna o enum TipoUsuario correspondente ao codigo
	 *
	 * @param tipo String identificando o codigo do enum
	 * @return Comentar aqui.
	 */
	public static TipoVeiculo getEnum(final String tipo) {
		if (tipo != null) {
			for (TipoVeiculo enm : values()) {
				if (enm.tipo.equals(tipo)) {
					return enm;
				}
			}
		}
		return null;
	}

	/**
	 * M�todo sobrescrito.
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return this.tipo;
	}

}
