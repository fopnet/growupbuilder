/**
 * 
 */
package br.com.growupge.enums;

/**
 * @author Felipe
 *
 */
public enum TipoCategoria {
	Particular,
	Aluguel;
	
	public String toString() {
		return super.name();
	}
}



