package br.com.growupge.enums;


/**
 * Os tipos de ordena��o do SQL a ser montado din�micamente.
 *
 * @author Felipe
 *
 */
public enum TipoOperadorSQL {
	JOIN("INNER JOIN"),
	LEFT("LEFT JOIN"),
	RIGHT("RIGHT JOIN"),
	AND("AND"),
	OR("OR"),
	WHERE("WHERE"),
	IN("IN");

	private final String tipo;

	/**
	 * Construtor para esta classe.
	 *
	 * @param tipo Parametro do tipo String
	 */
	private TipoOperadorSQL(final String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Metodo retorna o enum TipoUsuario correspondente ao codigo
	 *
	 * @param tipo String identificando o codigo do enum
	 * @return Comentar aqui.
	 */
	public static TipoOperadorSQL getEnum(final String tipo) {
		TipoOperadorSQL enm = null;
		if (tipo.equals("INNER JOIN")) {
			enm = TipoOperadorSQL.JOIN;
		} else if (tipo.equals("LEFT JOIN")) {
			enm = TipoOperadorSQL.LEFT;
		} else if (tipo.equals("RIGHT JOIN")) {
			enm = TipoOperadorSQL.RIGHT;
		} else if (tipo.equals("AND")) {
			enm = TipoOperadorSQL.AND;
		} else if (tipo.equals("OR")) {
				enm = TipoOperadorSQL.OR;
		} else if (tipo.equals("WHERE")) {
			enm = TipoOperadorSQL.WHERE;
		} else if (tipo.equals("IN")) {
			enm = TipoOperadorSQL.IN;
		}
		return enm;
	}

	/**
	 * M�todo sobrescrito.
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return this.tipo;
	}

}