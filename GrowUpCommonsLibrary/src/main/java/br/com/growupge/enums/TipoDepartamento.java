package br.com.growupge.enums;

public enum TipoDepartamento {
	AREA1("A1"),
	AREA2("A2");
	
	private String tipo;
	
	private TipoDepartamento (String tipo) {
		this.tipo = tipo.toUpperCase();
	}
	
	public static TipoDepartamento getEnum(String tipo) {
		TipoDepartamento enm = null;
		if (tipo.equals("A1")) {
			enm = TipoDepartamento.AREA1;
		} else if (tipo.equals("A2")) {
			enm = TipoDepartamento.AREA2;
		}
		return enm;
	}
	
	public String toString() {
		return this.tipo;
	}
}
