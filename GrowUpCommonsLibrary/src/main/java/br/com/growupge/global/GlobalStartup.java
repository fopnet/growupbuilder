/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : J2EE 1.4
 * Criado em  : 03/10/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 03/10/2006 - In�cio de tudo, por J�lio Vitorino
 * 14/12/2006 - Inclus�o dos atributos de Property, By JCV
 *
 */
package br.com.growupge.global;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.enums.TipoDAOFactory;
import br.com.growupge.utility.StringUtil;

/**
 * Classe para carregar os par�metros iniciais do Sigem.
 *
 * @author Felipe
 * @version 1.0
 *
 */
public final class GlobalStartup extends DefaultHandler implements Serializable {
	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(GlobalStartup.class);
	
	/**
	 * Atributo '<code>instance</code>' do tipo GlobalStartup
	 */
	private static GlobalStartup instance = null;

	/**
	 * Atributo '<code>tempval</code>' do tipo String
	 */
	private String tempval;

	/**
	 * Atributo '<code>properties</code>' do tipo Properties
	 */
	private Properties properties = new Properties();

	/**
	 * Atributo '<code>dicionarioBR</code>' do tipo String
	 */
	private String dicionarioBR;

	/**
	 * Atributo '<code>dicionarioEN</code>' do tipo String
	 */
	private String dicionarioEN;

	//	 ----------  Construtores

	/**
	 * Construtor para esta classe.
	 *
	 * @throws Exception lan�ar Exception
	 */
	private GlobalStartup() throws Exception {
		this("startup.xml");
	}

	/**
	 * Construtor para esta classe.
	 *
	 * @param startupFile Nome do arquivo de startup
	 * @throws Exception Lan�ar exce��o
	 */
	private GlobalStartup(final String startupPath) throws Exception {
		this.parseDocument(startupPath);
	}

	/**
	 * Obter a inst�ncia da classe GlobalStartup
	 *
	 * @return Retornar objeto GlobalStartup
	 * @throws Exception Lan�a Exception
	 */
	public static GlobalStartup getInstance() throws Exception {
		if (GlobalStartup.instance == null) {
			instance = new GlobalStartup();
		}
		return instance;
	}

	// ----------  XML parser

	/**
	 * Executar o parse do documento XML de startup
	 *
	 * @param startupPath Nome do arquivo para configura��o de startup
	 * @throws Exception Lan�ar Exception
	 */
	private void parseDocument(final String startupPath) throws Exception {

		//Obtem uma factory
		SAXParserFactory spf = SAXParserFactory.newInstance();
		InputStream is = null;
		try {

			//Obtem uma nova instancia do parser
			SAXParser sp = spf.newSAXParser();

			is = this.getResourceFile(startupPath);

			//Executa o parse do arquivo e o registra classe para os m�todos de callback
			sp.parse(is, this);

		} catch (SAXException se) {
			logger.error(se, se);
			throw se;
		} catch (ParserConfigurationException pce) {
			logger.error(pce,pce);
			throw pce;
		} catch (IOException ie) {
			// curdir = new File(".");
			logger.error("Arquivo '" + startupPath + "' n�o encontrado no diret�rio " + startupPath);
			throw ie;
		}
	}

	/**
	 * M�todo sobrescrito.
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	public void startElement(
			final String uri,
			final String localName,
			final String qName,
			final Attributes attributes) throws SAXException {

		//reset
		this.tempval = "";
		// verifica tags
//		if (qName.equalsIgnoreCase("ActiveAppMode")) {
//			this.setActiveApplicationMode(attributes.getValue("Active"));
//		}

		if (qName.equalsIgnoreCase("am")) {
//			if (attributes.getValue("name").equalsIgnoreCase(this.activeApplicationMode)) {
//				isActive = true;
//			} else {
//				isActive = false;
//			}
		}
		// verifica tags
		if (qName.equalsIgnoreCase("Property")) {
			this.properties.put(attributes.getValue("name"), attributes.getValue("value"));
		}
	}

	/**
	 * M�todo sobrescrito.
	 * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
	 */
	public void characters(final char[] ch, final int start, final int length) throws SAXException {
		this.tempval = alterarTags(new String(ch, start, length));
	}

	/**
	 * Metodo que altera o conteudo da tag xml
	 *
	 * @param str
	 * @return Comentar aqui.
	 */
	private final String alterarTags(final String str) {
		String retorno = str;

		if (str.indexOf("${") == -1) {
			return str;
		}
		Enumeration<?> e = this.properties.keys();
		for (; e.hasMoreElements();) {
			String propNome = (String) e.nextElement();
			String propValue = (String) this.properties.get(propNome);
			retorno = StringUtil.replaceString(retorno, "${" + propNome + "}", propValue);
		}
		return this.alterarTags(retorno);
	}

	/**
	 * M�todo sobrescrito.
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void endElement(final String uri, final String localName, final String qName)
			throws SAXException {
		if (qName.equalsIgnoreCase("DicionarioBR"))
			this.dicionarioBR = this.tempval;
		else if (qName.equalsIgnoreCase("DicionarioEN"))
			this.dicionarioEN = this.tempval;
	}

	// Getters and Setters

	// ------------- Resources Files
	public InputStream getResourceFile(String resourceName) {
		InputStream is = null;

		//URL url = this.getClass().getClassLoader().getResource(resourceName);
		//String filename = url.getFile().substring(1);
		//is = new FileInputStream(filename);

		is = getClass().getClassLoader().getResourceAsStream(resourceName);

		return is;
	}

	/**
	 * @return String Returns the activedaoFactory.
	 */
	public TipoDAOFactory getActiveDAOFactory() {
		return TipoDAOFactory.getEnum(Constantes.ACTIVE_DAO_FACTORY);
	}

	// ----------- Global Getters and Setters

	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'dicionarioBR'.
	 * @return Retorna um objeto do tipo String
	 */
	public String getDicionarioBR() {
		return this.dicionarioBR;
	}

	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'dicionarioEN'.
	 * @return Retorna um objeto do tipo String
	 */
	public String getDicionarioEN() {
		return this.dicionarioEN;
	}
}
