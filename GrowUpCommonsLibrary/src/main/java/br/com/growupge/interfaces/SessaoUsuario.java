/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 09/10/2008
 *
 * Historico de Modifica��o:
 * =========================
 * 09/10/2008 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.interfaces;

import br.com.growupge.dto.SessaoDTO;

/**
 * Interface para setar sessao do usu�rio
 *
 * @author Felipe
 *
 */
public interface SessaoUsuario {
	
	/**
	 * Este m�todo � respons�vel por configurar o valor de 'sessaordto', que �
	 * um objeto do tipo SessaoDTO.
	 * 
	 * @param sessaodto
	 *            O valor de 'sessaordto' a ser configurado.
	 */
	public void setSessaodto(final SessaoDTO sessaodto);
}

