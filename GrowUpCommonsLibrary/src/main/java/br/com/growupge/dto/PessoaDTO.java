/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 18/12/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 18/12/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * DTO utilizado para transportar os dados do
 * usu�rio.
 *
 * @author Felipe
 *
 */
public class PessoaDTO extends MensagemRetornoDTO implements Serializable {
	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */

	private static final long serialVersionUID = 2549065861160539545L;

	private String tipoDepartamento;
	private String tipoCargo;
	private float pretensao;
	// Usuario
	private UsuarioDTO usuario;
	
	// Habilitacao
	private String catHabilitacao;
	private String orgaoClasse;
	private String orgaoPublico;
	// RG
	private String rg;
	private String orgaoExpeditor;
	// Filiacao
	private String tipoSanguineo;
	private String sexo;
	private String nomePai;
	private String nomeMae;
	private Date nascimento;
	private String estadoCivil;
	// Nativo
	private String ufNatal;
	private String cidadeNatal;
	private String qtdDptoLegais;
	// Contato
	private String cpf;
	private String endereco;
	private String bairro;
	private String cidade;
	private String uf;
	private String cep;
	private String telefone;
	private String celular;
	private String infoAdcional;
	
	/**
	 * Construtor para esta classe.
	 *
	 */
	public PessoaDTO() {
	}

	public PessoaDTO(UsuarioDTO dto) {
		this.setUsuario(dto);
	}

	/**
	 * @return the infoAdcional
	 */
	public String getInfoAdcional() {
		return infoAdcional;
	}

	/**
	 * @param infoAdcional the infoAdcional to set
	 */
	public void setInfoAdcional(String infoAdcional) {
		this.infoAdcional = infoAdcional;
	}

	/**
	 * @return the celular
	 */
	public String getCelular() {
		return celular;
	}

	/**
	 * @param celular the celular to set
	 */
	public void setCelular(String celular) {
		this.celular = celular;
	}

	/**
	 * @return the telefone
	 */
	public String getTelefone() {
		return telefone;
	}

	/**
	 * @param telefone the telefone to set
	 */
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	/**
	 * @return the cep
	 */
	public String getCep() {
		return cep;
	}

	/**
	 * @param cep the cep to set
	 */
	public void setCep(String cep) {
		this.cep = cep;
	}

	/**
	 * @return the uf
	 */
	public String getUf() {
		return uf;
	}

	/**
	 * @param uf the uf to set
	 */
	public void setUf(String uf) {
		this.uf = uf;
	}

	/**
	 * @return the cidade
	 */
	public String getCidade() {
		return cidade;
	}

	/**
	 * @param cidade the cidade to set
	 */
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	/**
	 * @return the bairro
	 */
	public String getBairro() {
		return bairro;
	}

	/**
	 * @param bairro the bairro to set
	 */
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	/**
	 * @return the endereco
	 */
	public String getEndereco() {
		return endereco;
	}

	/**
	 * @param endereco the endereco to set
	 */
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	/**
	 * @return the cpf
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * @param cpf the cpf to set
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**
	 * @return the qtdDptoLegais
	 */
	public String getQtdDptoLegais() {
		return qtdDptoLegais;
	}

	/**
	 * @param qtdDptoLegais the qtdDptoLegais to set
	 */
	public void setQtdDptoLegais(String qtdDptoLegais) {
		this.qtdDptoLegais = qtdDptoLegais;
	}

	/**
	 * @return the cidadeNatal
	 */
	public String getCidadeNatal() {
		return cidadeNatal;
	}

	/**
	 * @param cidadeNatal the cidadeNatal to set
	 */
	public void setCidadeNatal(String cidadeNatal) {
		this.cidadeNatal = cidadeNatal;
	}

	/**
	 * @return the ufNatal
	 */
	public String getUfNatal() {
		return ufNatal;
	}

	/**
	 * @param ufNatal the ufNatal to set
	 */
	public void setUfNatal(String ufNatal) {
		this.ufNatal = ufNatal;
	}

	/**
	 * @return the estadoCivil
	 */
	public String getEstadoCivil() {
		return estadoCivil;
	}

	/**
	 * @param estadoCivil the estadoCivil to set
	 */
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	/**
	 * @return the nascimento
	 */
	public Date getNascimento() {
		return nascimento;
	}

	/**
	 * @param nascimento the nascimento to set
	 */
	public void setNascimento(Date nascimento) {
		this.nascimento = nascimento;
	}

	/**
	 * @return the nomeMae
	 */
	public String getNomeMae() {
		return nomeMae;
	}

	/**
	 * @param nomeMae the nomeMae to set
	 */
	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	/**
	 * @return the nomePai
	 */
	public String getNomePai() {
		return nomePai;
	}

	/**
	 * @param nomePai the nomePai to set
	 */
	public void setNomePai(String nomePai) {
		this.nomePai = nomePai;
	}

	/**
	 * @return the sexo
	 */
	public String getSexo() {
		return sexo;
	}

	/**
	 * @param sexo the sexo to set
	 */
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	/**
	 * @return the tipoSanguineo
	 */
	public String getTipoSanguineo() {
		return tipoSanguineo;
	}

	/**
	 * @param tipoSanguineo the tipoSanguineo to set
	 */
	public void setTipoSanguineo(String tipoSanguineo) {
		this.tipoSanguineo = tipoSanguineo;
	}

	/**
	 * @return the orgaoExpeditor
	 */
	public String getOrgaoExpeditor() {
		return orgaoExpeditor;
	}

	/**
	 * @param orgaoExpeditor the orgaoExpeditor to set
	 */
	public void setOrgaoExpeditor(String orgaoExpeditor) {
		this.orgaoExpeditor = orgaoExpeditor;
	}

	/**
	 * @return the rg
	 */
	public String getRg() {
		return rg;
	}

	/**
	 * @param rg the rg to set
	 */
	public void setRg(String rg) {
		this.rg = rg;
	}

	/**
	 * @return the orgaoPublico
	 */
	public String getOrgaoPublico() {
		return orgaoPublico;
	}

	/**
	 * @param orgaoPublico the orgaoPublico to set
	 */
	public void setOrgaoPublico(String orgaoPublico) {
		this.orgaoPublico = orgaoPublico;
	}

	/**
	 * @return the orgaoClasse
	 */
	public String getOrgaoClasse() {
		return orgaoClasse;
	}

	/**
	 * @param orgaoClasse the orgaoClasse to set
	 */
	public void setOrgaoClasse(String orgaoClasse) {
		this.orgaoClasse = orgaoClasse;
	}

	/**
	 * @return the catHabilitacao
	 */
	public String getCatHabilitacao() {
		return catHabilitacao;
	}

	/**
	 * @param catHabilitacao the catHabilitacao to set
	 */
	public void setCatHabilitacao(String catHabilitacao) {
		this.catHabilitacao = catHabilitacao;
	}

	/**
	 * @return the usuario
	 */
	public UsuarioDTO getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the pretensao
	 */
	public float getPretensao() {
		return pretensao;
	}

	/**
	 * @param pretensao the pretensao to set
	 */
	public void setPretensao(float pretensao) {
		this.pretensao = pretensao;
	}

	/**
	 * @return the tipoCargo
	 */
	public String getTipoCargo() {
		return tipoCargo;
	}

	/**
	 * @param tipoCargo the tipoCargo to set
	 */
	public void setTipoCargo(String tipoCargo) {
		this.tipoCargo = tipoCargo;
	}

	/**
	 * @return the tipoDepartamento
	 */
	public String getTipoDepartamento() {
		return tipoDepartamento;
	}

	/**
	 * @param tipoDepartamento the tipoDepartamento to set
	 */
	public void setTipoDepartamento(String tipoDepartamento) {
		this.tipoDepartamento = tipoDepartamento;
	}
	
	

}
