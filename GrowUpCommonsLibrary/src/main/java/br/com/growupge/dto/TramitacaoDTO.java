/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;
import java.util.Date;

import br.com.growupge.constantes.Constantes;

/**
 * Data Transfer Object para manipular dados da marca.
 *
 * @author Felipe
 *
 */
public class TramitacaoDTO extends MensagemRetornoDTO implements Serializable {

	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = -1675996220054283037L;
	
	/**
	 * Atributo '<code>codigo</code>' do tipo long
	 */
	private long codigo;
	
	/**
	 * Atributo '<code>pedido</code>' do tipo PedidoVeiculoDTO
	 */
	private PedidoVeiculoDTO pedido;
	
	/**
	 * Atributo '<code>isPendente</code>' do tipo String
	 */
	private String isPendente;
	
	/**
	 * Atributo '<code>obs</code>' do tipo String
	 */
	private String obs;
	
	/**
	 * Atributo '<code>dataCadastro</code>' do tipo Date
	 */
	private Date dataCadastro;
	
	/**
	 * Atributo '<code>usuarioCriacao</code>' do tipo UsuarioDTO
	 */
	private UsuarioDTO usuarioCadastro;
	
	/**
	 * Atributo '<code>dataAlteracao</code>' do tipo Date
	 */
	private Date dataAlteracao;
	
	/**
	 * Atributo '<code>usuarioAlteracao</code>' do tipo UsuarioDTO
	 */
	private UsuarioDTO usuarioAlteracao;
	
    // Para fins de filtro
    private Date filtroDataFinal;
    private Date filtroDataInicial;    
	
	/**
	 * Construtor para esta classe.
	 *
	 */
	public TramitacaoDTO() {
	}

	/**
	 * @return the pedido
	 */
	public PedidoVeiculoDTO getPedido() {
		return pedido;
	}

	/**
	 * @param pedido the pedido to set
	 */
	public void setPedido(PedidoVeiculoDTO pedido) {
		this.pedido = pedido;
	}

	/**
	 * @return the codigo
	 */
	public long getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the obs
	 */
	public String getObs() {
		return obs;
	}

	/**
	 * @param obs the obs to set
	 */
	public void setObs(String obs) {
		this.obs = obs;
	}

	/**
	 * @return the isPendente
	 */
	public String getIsPendente() {
		return isPendente;
	}

	/**
	 * @return
	 */
	public boolean isPendente() {
		return Constantes.SIM.equals(getIsPendente());
	}

	/**
	 * @param isPendente the isPendente to set
	 */
	public void setIsPendente(String isPendente) {
		this.isPendente = isPendente;
	}

	/**
	 * @return the dataCadastro
	 */
	public Date getDataCadastro() {
		return dataCadastro;
	}

	/**
	 * @param dataCadastro the dataCadastro to set
	 */
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	/**
	 * @return the dataAlteracao
	 */
	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	/**
	 * @param dataAlteracao the dataAlteracao to set
	 */
	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	/**
	 * @return the usuarioAlteracao
	 */
	public UsuarioDTO getUsuarioAlteracao() {
		return usuarioAlteracao;
	}

	/**
	 * @param usuarioAlteracao the usuarioAlteracao to set
	 */
	public void setUsuarioAlteracao(UsuarioDTO usuarioAlteracao) {
		this.usuarioAlteracao = usuarioAlteracao;
	}

	/**
	 * @return the filtroDataFinal
	 */
	public Date getFiltroDataFinal() {
		return filtroDataFinal;
	}

	/**
	 * @param filtroDataFinal the filtroDataFinal to set
	 */
	public void setFiltroDataFinal(Date filtroDataFinal) {
		this.filtroDataFinal = filtroDataFinal;
	}

	/**
	 * @return the filtroDataInicial
	 */
	public Date getFiltroDataInicial() {
		return filtroDataInicial;
	}

	/**
	 * @param filtroDataInicial the filtroDataInicial to set
	 */
	public void setFiltroDataInicial(Date filtroDataInicial) {
		this.filtroDataInicial = filtroDataInicial;
	}

	/**
	 * @return the usuarioCadastro
	 */
	public UsuarioDTO getUsuarioCadastro() {
		return usuarioCadastro;
	}

	/**
	 * @param usuarioCadastro the usuarioCadastro to set
	 */
	public void setUsuarioCadastro(UsuarioDTO usuarioCadastro) {
		this.usuarioCadastro = usuarioCadastro;
	}


	
}
