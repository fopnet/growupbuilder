/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : J2EE 1.4
 * Criado em  : 25/07/2005
 *
 * Historico de Modifica��o:
 * =========================
 * 26/10/2006 - In�cio de tudo, por Felipe
 *
 */

package br.com.growupge.dto;

import java.io.Serializable;

import br.com.growupge.enums.TipoIdioma;

/**
 * Data Transfer Object para manipular mensagens internacionalizadas.
 *
 * @author Felipe
 *
 */
public class MensagemDTO extends MensagemRetornoDTO implements Serializable {

	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = -6727035464804243767L;

	/**
	 * Atributo '<code>codigo</code>' do tipo String
	 */
	private String codigo;

	/**
	 * Atributo '<code>descricaoBR</code>' do tipo String
	 */
	private String descricaoBR;

	/**
	 * Atributo '<code>descricaoUS</code>' do tipo String
	 */
	private String descricaoEN;

	/**
	 * Construtor para esta classe.
	 *
	 */
	public MensagemDTO() {
	}
	
	/**
	 * Construtor para esta classe.
	 *
	 * @param codigo
	 */
	public MensagemDTO(String codigo) {
		this.setCodigo(codigo);
	}
	
	/**
	 * @param idioma
	 * @return
	 */
	public String getDescricao(String tipo) {
		TipoIdioma enm = TipoIdioma.getEnum(tipo);
		switch (enm) {
		case  IDIOMA_BR:
			return getDescricaoBR();
		case  IDIOMA_EN:
			return getDescricaoEN();
		default :
			return null;
		}
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricaoBR
	 */
	public String getDescricaoBR() {
		return descricaoBR;
	}

	/**
	 * @param descricaoBR the descricaoBR to set
	 */
	public void setDescricaoBR(String descricaoBR) {
		this.descricaoBR = descricaoBR;
	}

	/**
	 * @return the descricaoEN
	 */
	public String getDescricaoEN() {
		return descricaoEN;
	}

	/**
	 * @param descricaoEN the descricaoEN to set
	 */
	public void setDescricaoEN(String descricaoEN) {
		this.descricaoEN = descricaoEN;
	}

	
}
