/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 18/12/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 18/12/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import static br.com.growupge.utility.Reflection.isCodigoValido;
import static br.com.growupge.utility.Reflection.isPropriedadeValida;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.Serializable;
import java.util.Date;

/**
 * DTO para manipular dados de sess�o do usu�rio.
 *
 * @author Felipe
 *
 */
public class SessaoDTO extends MensagemRetornoDTO implements Serializable {

	private static final long serialVersionUID = 6329095024469909923L;
	
	private String sid;
	private String accessToken;
	private Date data;
	private String host;
	private UsuarioDTO usuario;

	public SessaoDTO() {
	}

	public SessaoDTO(String sid) {
		this.sid = sid;
	}

	/**
	 * @param usuarioDTO
	 */
	public SessaoDTO(UsuarioDTO usuarioDTO) {
		setUsuario(usuarioDTO);
	}

	/**
	 * @return the usuario
	 */
	public UsuarioDTO getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the data
	 */
	public Date getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(Date data) {
		this.data = data;
	}

	/**
	 * @return the sid
	 */
	public String getSid() {
		return sid;
	}

	/**
	 * @param sid the sid to set
	 */
	public void setSid(String sid) {
		this.sid = sid;
	}

	/**
	 * @param access_token
	 */
	public void setAccessToken(String access_token) {
		this.accessToken = access_token;
	}
	
	/**
	 * @return
	 */
	public String getAccessToken() {
		return accessToken;
	}
	
	/**
	 * @return
	 */
	public boolean isSessaoValida() {
		return isPropriedadeValida(this, "sid") && isNotBlank(getUsuario().getSenha())
				&& (isCodigoValido(getUsuario()) || isNotBlank(getUsuario().getEmail()));
	}

	/**
	 * @return
	 */
	public boolean isSessaoFacebookValida() {
		return isPropriedadeValida(this, "sid") 
				&& isPropriedadeValida(this, "accessToken")
				&& isPropriedadeValida(getUsuario(), "nome") && isNotBlank(getUsuario().getEmail());
	}

}
