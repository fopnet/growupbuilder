/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;
import java.util.Date;

import br.com.growupge.validador.Validador;

/**
 * Data Transfer Object para manipular dados da marca.
 *
 * @author Felipe
 *
 */
public class ProprietarioDTO extends MensagemRetornoDTO implements Serializable {
	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = -1186783688704357672L;

	/**
	 * Atributo '<code>codigo</code>' do tipo String
	 */
	private long codigo;
	
	/**
	 * Atributo '<code>nome</code>' do tipo String
	 */
	private String nome;
	
	/**
	 * Atributo '<code>cpf</code>' do tipo String
	 */
	private String cpfCnpj;
	
	/**
	 * Atributo '<code>dataAlteracao</code>' do tipo String
	 */
	private Date dataAlteracao;
	
	/**
	 * Atributo '<code>dataCadastro</code>' do tipo String
	 */
	private Date dataCadastro;
	
    private Date filtroDataInicial;
    private Date filtroDataFinal;
	
	/**
	 * Atributo '<code>endereco</code>' do tipo String
	 */
	private String endereco;
	
	/**
	 * Atributo '<code>bairro</code>' do tipo String
	 */
	private String bairro;
	
	/**
	 * Atributo '<code>cidade</code>' do tipo String
	 */
	private String cidade;
	
	/**
	 * Atributo '<code>uf</code>' do tipo String
	 */
	private String uf;
	
	/**
	 * Atributo '<code>cep</code>' do tipo String
	 */
	private String cep;
	
	/**
	 * Atributo '<code>tel</code>' do tipo String
	 */
	private String tel;
	
	/**
	 * Atributo '<code>cel</code>' do tipo String
	 */
	public String cel;
	
	/**
	 * Atributo '<code>obs</code>' do tipo String
	 */
	private String obs;
	
	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'bairro'.
	 * @return Retorna um objeto do tipo String
	 */
	public final String getBairro() {
		return this.bairro;
	}

	/**
	 * Este m�todo � respons�vel por configurar
	 * o valor de 'bairro', que � um objeto do tipo String.
	 * @param bairro O valor de 'bairro' a ser configurado.
	 */
	public final void setBairro(String bairro) {
		this.bairro = bairro;
	}

	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'cel'.
	 * @return Retorna um objeto do tipo String
	 */
	public final String getCel() {
		return this.cel;
	}

	/**
	 * Este m�todo � respons�vel por configurar
	 * o valor de 'cel', que � um objeto do tipo String.
	 * @param cel O valor de 'cel' a ser configurado.
	 */
	public final void setCel(String cel) {
		this.cel = cel;
	}

	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'cep'.
	 * @return Retorna um objeto do tipo String
	 */
	public final String getCep() {
		return this.cep;
	}

	/**
	 * Este m�todo � respons�vel por configurar
	 * o valor de 'cep', que � um objeto do tipo String.
	 * @param value O valor de 'cep' a ser configurado.
	 */
	public final void setCep(String value) {
		if (value != null)
			value = Validador.retirarSinais(value);
		this.cep = value;
	}

	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'cidade'.
	 * @return Retorna um objeto do tipo String
	 */
	public final String getCidade() {
		return this.cidade;
	}

	/**
	 * Este m�todo � respons�vel por configurar
	 * o valor de 'cidade', que � um objeto do tipo String.
	 * @param cidade O valor de 'cidade' a ser configurado.
	 */
	public final void setCidade(String cidade) {
		this.cidade = cidade;
	}

	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'codigo'.
	 * @return Retorna um objeto do tipo String
	 */
	public final long getCodigo() {
		return this.codigo;
	}

	/**
	 * Este m�todo � respons�vel por configurar
	 * o valor de 'codigo', que � um objeto do tipo String.
	 * @param codigo O valor de 'codigo' a ser configurado.
	 */
	public final void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'dataAlteracao'.
	 * @return Retorna um objeto do tipo String
	 */
	public final Date getDataAlteracao() {
		return this.dataAlteracao;
	}

	/**
	 * Este m�todo � respons�vel por configurar
	 * o valor de 'dataAlteracao', que � um objeto do tipo String.
	 * @param dataAlteracao O valor de 'dataAlteracao' a ser configurado.
	 */
	public final void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'dataCadastro'.
	 * @return Retorna um objeto do tipo String
	 */
	public final Date getDataCadastro() {
		return this.dataCadastro;
	}

	/**
	 * Este m�todo � respons�vel por configurar
	 * o valor de 'dataCadastro', que � um objeto do tipo String.
	 * @param dataCadastro O valor de 'dataCadastro' a ser configurado.
	 */
	public final void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'endereco'.
	 * @return Retorna um objeto do tipo String
	 */
	public final String getEndereco() {
		return this.endereco;
	}

	/**
	 * Este m�todo � respons�vel por configurar
	 * o valor de 'endereco', que � um objeto do tipo String.
	 * @param endereco O valor de 'endereco' a ser configurado.
	 */
	public final void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'nome'.
	 * @return Retorna um objeto do tipo String
	 */
	public final String getNome() {
		return this.nome;
	}

	/**
	 * Este m�todo � respons�vel por configurar
	 * o valor de 'nome', que � um objeto do tipo String.
	 * @param nome O valor de 'nome' a ser configurado.
	 */
	public final void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'obs'.
	 * @return Retorna um objeto do tipo String
	 */
	public final String getObs() {
		return this.obs;
	}

	/**
	 * Este m�todo � respons�vel por configurar
	 * o valor de 'obs', que � um objeto do tipo String.
	 * @param obs O valor de 'obs' a ser configurado.
	 */
	public final void setObs(String obs) {
		this.obs = obs;
	}

	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'tel'.
	 * @return Retorna um objeto do tipo String
	 */
	public final String getTel() {
		return this.tel;
	}

	/**
	 * Este m�todo � respons�vel por configurar
	 * o valor de 'tel', que � um objeto do tipo String.
	 * @param tel O valor de 'tel' a ser configurado.
	 */
	public final void setTel(String tel) {
		this.tel = tel;
	}

	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'uf'.
	 * @return Retorna um objeto do tipo String
	 */
	public final String getUf() {
		return this.uf;
	}

	/**
	 * Este m�todo � respons�vel por configurar
	 * o valor de 'uf', que � um objeto do tipo String.
	 * @param uf O valor de 'uf' a ser configurado.
	 */
	public final void setUf(String uf) {
		this.uf = uf;
	}

	/**
	 * Construtor para esta classe.
	 *
	 */
	public ProprietarioDTO() {
	}
	
	/**
	 * M�todo sobrescrito.
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof ProprietarioDTO) {
			ProprietarioDTO prop = (ProprietarioDTO) obj; 
			return prop.getCodigo() == this.getCodigo() ? true : false;
		}
		return true;
	}

	/**
	 * @return the cpfcnpj
	 */
	public String getCpfcnpj() {
		return cpfCnpj;
	}

	/**
	 * @param value the cpfcnpj to set
	 */
	public void setCpfcnpj(String value) {
		if (value != null)
			value = Validador.retirarSinais(value);
		this.cpfCnpj = value;
	}

	/**
	 * @return the filtroDataFinal
	 */
	public Date getFiltroDataFinal() {
		return filtroDataFinal;
	}

	/**
	 * @param filtroDataFinal the filtroDataFinal to set
	 */
	public void setFiltroDataFinal(Date filtroDataFinal) {
		this.filtroDataFinal = filtroDataFinal;
	}

	/**
	 * @return the filtroDataInicial
	 */
	public Date getFiltroDataInicial() {
		return filtroDataInicial;
	}

	/**
	 * @param filtroDataInicial the filtroDataInicial to set
	 */
	public void setFiltroDataInicial(Date filtroDataInicial) {
		this.filtroDataInicial = filtroDataInicial;
	}
}
