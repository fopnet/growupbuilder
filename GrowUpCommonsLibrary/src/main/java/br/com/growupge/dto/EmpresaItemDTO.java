/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;

/**
 * Data Transfer Object para manipular dados de empresa.
 *
 * @author Felipe
 *
 */
public class EmpresaItemDTO extends MensagemRetornoDTO implements Serializable {
	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = -1186783688704357672L;

	public EmpresaDTO empresa;
	public ClienteDTO cliente;
	
	/**
	 * Construtor para esta classe.
	 *
	 */
	public EmpresaItemDTO() {
	}


	public final ClienteDTO getCliente() {
		return cliente;
	}

	public final void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public final EmpresaDTO getEmpresa() {
		return empresa;
	}

	public final void setEmpresa(EmpresaDTO empresa) {
		this.empresa = empresa;
	}

}
