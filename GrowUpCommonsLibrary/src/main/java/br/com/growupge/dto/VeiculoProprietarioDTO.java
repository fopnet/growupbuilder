/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;

/**
 * Data Transfer Object para manipular dados da marca.
 *
 * @author Felipe
 *
 */
public class VeiculoProprietarioDTO extends MensagemRetornoDTO implements Serializable {
	
	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1876829691690881091L;

	/**
	 * Atributo '<code>proprietario</code>' do tipo ProprietarioDTO
	 */
	private ProprietarioDTO proprietario;
	
	/**
	 * Atributo '<code>veiculo</code>' do tipo VeiculoDTO
	 */
	private VeiculoDTO veiculo;
	
	/**
	 * Construtor para esta classe.
	 *
	 */
	public VeiculoProprietarioDTO() {
	}

	/**
	 * Construtor para esta classe.
	 *
	 * @param dto
	 */
	public VeiculoProprietarioDTO(VeiculoDTO dto) {
		this.setVeiculo(dto);
		this.setProprietario(dto.getProprietario());
	}

	/**
	 * @return the proprietario
	 */
	public ProprietarioDTO getProprietario() {
		return proprietario;
	}

	/**
	 * @param proprietario the proprietario to set
	 */
	public void setProprietario(ProprietarioDTO proprietario) {
		this.proprietario = proprietario;
	}

	/**
	 * @return the veiculo
	 */
	public VeiculoDTO getVeiculo() {
		return veiculo;
	}

	/**
	 * @param veiculo the veiculo to set
	 */
	public void setVeiculo(VeiculoDTO veiculo) {
		this.veiculo = veiculo;
	}
	
}
