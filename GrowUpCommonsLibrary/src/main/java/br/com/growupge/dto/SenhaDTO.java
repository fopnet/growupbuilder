/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 18/12/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 18/12/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;

/**
 * Data Transfer Object para manipular dados do usu�rio.
 *
 * @author Felipe
 *
 */
public class SenhaDTO extends MensagemRetornoDTO implements Serializable {

	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = -6637251476547108720L;

	private long codigo;
	private String valor;
	private UsuarioDTO usuario;
	
	public SenhaDTO() {}
	
	public SenhaDTO(UsuarioDTO usr) {
		this.setUsuario(usr);
	}
	
	public final long getCodigo() {
		return this.codigo;
	}
	public final void setCodigo(long codigo) {
		this.codigo = codigo;
	}
	
	public final UsuarioDTO getUsuario() {
		return this.usuario;
	}
	public final void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}
	
	public final String getValor() {
		return this.valor;
	}
	public final void setValor(String valor) {
		this.valor = valor;
	}

	
}
