package br.com.growupge.dto;

import java.io.Serializable;

import br.com.growupge.exception.GrowUpException;
import br.com.growupge.utility.Reflection;

public class Parameter implements Serializable {

	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 6228859946857086513L;

	private String atributo;

	private Object valor;
	
	public Parameter() {

	}

	public Parameter(String atributo, Object valor) {
		this.setAtributo(atributo);
		this.setValor(valor);
	}
	
	/**
	 * Retorna o usuario do contexto
	 *
	 * @return uma instancia do tipo UsuarioDTO
	 */
	public UsuarioDTO getUsuario() {
		UsuarioDTO usr = null;
		
		if (this.getValor() instanceof UsuarioDTO) {
			usr = (UsuarioDTO) this.getValor();
		}
		
		return usr;
	}
	
	public static void setValor(MensagemRetornoDTO dto, Parameter param) throws GrowUpException {
		Reflection.setFieldValue(dto, param);
	}

	/**
	 * @return the atributo
	 */
	public String getAtributo() {
		return atributo;
	}

	/**
	 * @param atributo the atributo to set
	 */
	public void setAtributo(String atributo) {
		this.atributo = atributo;
	}

	/**
	 * @return the valor
	 */
	public Object getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(Object valor) {
		this.valor = valor;
	}
}
