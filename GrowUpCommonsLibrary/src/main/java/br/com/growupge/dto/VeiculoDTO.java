package br.com.growupge.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Data Transfer Object para manipular dados da marca.
 *
 * @author Felipe
 *
 */
public class VeiculoDTO extends MensagemRetornoDTO implements Serializable {
	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = -4677991846228193510L;

	/**
	 * Atributo '<code>placa</code>' do tipo String
	 */
	private String codigo;
	
	/**
	 * Atributo '<code>proprietario</code>' do tipo ProprietarioDTO
	 */
	private ProprietarioDTO proprietario;
	
	/**
	 * Atributo '<code>renavam</code>' do tipo String
	 */
	private String renavam;
	
	/**
	 * Atributo '<code>chassi</code>' do tipo String
	 */
	private String chassi;
	
	/**
	 * Atributo '<code>anoModelo</code>' do tipo String
	 */
	private Integer anoModelo;
	
	/**
	 * Atributo '<code>anoFabric</code>' do tipo String
	 */
	private Integer anoFabric;
	
	/**
	 * Atributo '<code>modelo</code>' do tipo ModeloDTO
	 */
	private ModeloDTO modelo;

	/**
	 * Atributo '<code>marca</code>' do tipo MarcaDTO
	 */
	private MarcaDTO marca;
	
	/**
	 * Atributo '<code>combustivel</code>' do tipo String
	 */
	private CombustivelDTO combustivel;
	
	/**
	 * Atributo '<code>dataCadastro</code>' do tipo Date
	 */
	private Date dataCadastro;
	
	/**
	 * Atributo '<code>usuarioCadastro</code>' do tipo UsuarioDTO
	 */
	private UsuarioDTO usuarioCadastro;
	
	/**
	 * Atributo '<code>dataAlteracao</code>' do tipo Date
	 */
	private Date dataAlteracao;
	
	/**
	 * Atributo '<code>usuarioAlteracao</code>' do tipo UsuarioDTO
	 */
	private UsuarioDTO usuarioAlteracao;
	
	/////// Dados novos 
	
	/**
	 *  Atributo '<code>motor</code>' do tipo String
	 */
	private String motor;
	
	/**
	 *  Atributo '<code>tipo</code>' do tipo String
	 */
	private String tipo; //Automovel, Moto, Caminhao
	
	/**
	 *  Atributo '<code>categoria</code>' do tipo String
	 */
	private String categoria; // //Particular, aluguel
	
	/**
	 *  Atributo '<code>cor</code>' do tipo String
	 */
	private String cor;//Listagem das cores basicas
	
	/**
	 *  Atributo '<code>ultimoAnoLicensiamento</code>' do tipo int
	 */
	private Integer ultimoAnoLicenciamento;//Ano do ultimo licensiamento
	
	/**
	 *  Atributo '<code>ultimaTransferencia</code>' do tipo Date
	 */
	private Date ultimaTransacao;//�ltimo transfer�ncia
	
	/**
	 *  Atributo '<code>obs</code>' do tipo String
	 */
	private String obs;//Obs gerais
	
	/**
	 *  Atributo '<code>srf</code>' do tipo String
	 *  Situacao regularidade
	 */
	private String srf;
	
	/**
	 * Construtor para esta classe.
	 *
	 */
	public VeiculoDTO() {
	}

	/**
	 * Construtor para esta classe recebendo a placa
	 *
	 */
	public VeiculoDTO(String placa) {
		this.setCodigo(placa);
	}

	/**
	 * @return the obs
	 */
	public String getObs() {
		return obs;
	}

	/**
	 * @param obs the obs to set
	 */
	public void setObs(String obs) {
		this.obs = obs;
	}

	/**
	 * @return the ultimaTransacao
	 */
	public Date getUltimaTransacao() {
		return ultimaTransacao;
	}
	
	/**
	 * @param ultimaTransacao the ultimaTransacao to set
	 */
	public void setUltimaTransacao(Date ultimaTransacao) {
		this.ultimaTransacao = ultimaTransacao;
	}
	
	/**
	 * @return the ultimoAnoLicensiamento
	 */
	public Integer getUltimoAnoLicenciamento() {
		return ultimoAnoLicenciamento;
	}

	/**
	 * @param ultimoAnoLicensiamento the ultimoAnoLicensiamento to set
	 */
	public void setUltimoAnoLicenciamento(Integer ultimoAnoLicensiamento) {
		this.ultimoAnoLicenciamento = ultimoAnoLicensiamento;
	}

	/**
	 * @return the cor
	 */
	public String getCor() {
		return cor;
	}

	/**
	 * @param cor the cor to set
	 */
	public void setCor(String cor) {
		this.cor = cor;
	}

	/**
	 * @return the categoria
	 */
	public String getCategoria() {
		return categoria;
	}

	/**
	 * @param categoria the categoria to set
	 */
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the motor
	 */
	public String getMotor() {
		return motor;
	}

	/**
	 * @param motor the motor to set
	 */
	public void setMotor(String motor) {
		this.motor = motor;
	}

	/**
	 * @return the usuarioAlteracao
	 */
	public UsuarioDTO getUsuarioAlteracao() {
		return usuarioAlteracao;
	}

	/**
	 * @param usuarioAlteracao the usuarioAlteracao to set
	 */
	public void setUsuarioAlteracao(UsuarioDTO usuarioAlteracao) {
		this.usuarioAlteracao = usuarioAlteracao;
	}

	/**
	 * @return the dataAlteracao
	 */
	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	/**
	 * @param dataAlteracao the dataAlteracao to set
	 */
	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	/**
	 * @return the usuarioCadastro
	 */
	public UsuarioDTO getUsuarioCadastro() {
		return usuarioCadastro;
	}

	/**
	 * @param usuarioCadastro the usuarioCadastro to set
	 */
	public void setUsuarioCadastro(UsuarioDTO usuarioCadastro) {
		this.usuarioCadastro = usuarioCadastro;
	}

	/**
	 * @return the dataCadastro
	 */
	public Date getDataCadastro() {
		return dataCadastro;
	}

	/**
	 * @param dataCadastro the dataCadastro to set
	 */
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	/**
	 * @return the combustivel
	 */
	public CombustivelDTO getCombustivel() {
		return combustivel;
	}

	/**
	 * @param combustivel the combustivel to set
	 */
	public void setCombustivel(CombustivelDTO combustivel) {
		this.combustivel = combustivel;
	}

	/**
	 * @return the marca
	 */
	public MarcaDTO getMarca() {
		return marca;
	}

	/**
	 * @param marca the marca to set
	 */
	public void setMarca(MarcaDTO marca) {
		this.marca = marca;
	}

	/**
	 * @return the modelo
	 */
	public ModeloDTO getModelo() {
		return modelo;
	}

	/**
	 * @param modelo the modelo to set
	 */
	public void setModelo(ModeloDTO modelo) {
		this.modelo = modelo;
	}

	/**
	 * @return the anoFabric
	 */
	public Integer getAnoFabric() {
		return anoFabric;
	}

	/**
	 * @param anoFabric the anoFabric to set
	 */
	public void setAnoFabric(Integer anoFabric) {
		this.anoFabric = anoFabric;
	}

	/**
	 * @return the anoModelo
	 */
	public Integer getAnoModelo() {
		return anoModelo;
	}

	/**
	 * @param anoModelo the anoModelo to set
	 */
	public void setAnoModelo(Integer anoModelo) {
		this.anoModelo = anoModelo;
	}

	/**
	 * @return the chassi
	 */
	public String getChassi() {
		return chassi;
	}

	/**
	 * @param chassi the chassi to set
	 */
	public void setChassi(String chassi) {
		this.chassi = chassi;
	}

	/**
	 * @return the renavam
	 */
	public String getRenavam() {
		return renavam;
	}

	/**
	 * @param renavam the renavam to set
	 */
	public void setRenavam(String renavam) {
		this.renavam = renavam;
	}

	/**
	 * @return the proprietario
	 */
	public ProprietarioDTO getProprietario() {
		return proprietario;
	}

	/**
	 * @param proprietario the proprietario to set
	 */
	public void setProprietario(ProprietarioDTO proprietario) {
		this.proprietario = proprietario;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 * TODO Colocar privado
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo != null ? codigo.toUpperCase().trim() : codigo;
	}

	/**
	 * @return the srf
	 */
	public String getSrf() {
		return srf;
	}

	/**
	 * @param srf the srf to set
	 */
	public void setSrf(String srf) {
		this.srf = srf;
	}

	
}
