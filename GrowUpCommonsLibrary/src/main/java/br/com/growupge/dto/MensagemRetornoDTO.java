/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 08/01/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 08/01/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.util.Map;

/**
 * Classe utilizada para gerenciar as mensagens internacionalizadas
 * que ser�o retornadas sobre o status das opera��es.
 *
 * @author Felipe
 *
 */
public abstract class MensagemRetornoDTO extends DTOPadrao {

	private static final long serialVersionUID = -8552378641931397241L;

	/**
	 * C�digo da mensagem de retorno
	 */
	public String codMensagem;

	/**
	 * Descri��o da mensagem de retorno na l�ngua do usu�rio.
	 */
	public String mensagemRetorno;

	/**
	 * Atributo '<code>parametros</code>' do tipo Map<String,String>
	 */
	public Map<String, String> parametros = null;

	public final String getCodMensagem() {
		return codMensagem;
	}
	public final void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	public final String getMensagemRetorno() {
		return mensagemRetorno;
	}
	public final void setMensagemRetorno(String mensagemRetorno) {
		this.mensagemRetorno = mensagemRetorno;
	}

	public final Map<String, String> getParametros() {
		return parametros;
	}
	public final void setParametros(Map<String, String> parametros) {
		this.parametros = parametros;
	}
	
	

}