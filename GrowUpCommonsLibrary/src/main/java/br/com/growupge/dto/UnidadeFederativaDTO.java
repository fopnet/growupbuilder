/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;

/**
 * Data Transfer Object para manipular dados da uf.
 *
 * @author Felipe
 *
 */
public class UnidadeFederativaDTO extends MensagemRetornoDTO implements Serializable {

	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = -7930380933301865329L;

	/**
	 * Atributo '<code>codigo</code>' do tipo String
	 */
	public String codigo;
	
	/**
	 * Atributo '<code>nome</code>' do tipo String
	 */
	public String nome;
	
	/**
	 * Construtor para esta classe.
	 *
	 */
	public UnidadeFederativaDTO() {
	}


	
}
