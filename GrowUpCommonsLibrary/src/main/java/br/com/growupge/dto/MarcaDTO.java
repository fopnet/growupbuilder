/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;

/**
 * Data Transfer Object para manipular dados da marca.
 *
 * @author Felipe
 *
 */
public class MarcaDTO extends MensagemRetornoDTO implements Serializable {
	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = -1186783688704357632L;

	private Long codigo;
	private String nome;
	
	/**
	 * Construtor para esta classe.
	 *
	 */
	public MarcaDTO() {
	}
	
	public MarcaDTO(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the codigo
	 */
	public final Long getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public final void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'nome'.
	 * @return Retorna um objeto do tipo String
	 */
	public final String getNome() {
		return this.nome;
	}

	/**
	 * Este m�todo � respons�vel por configurar
	 * o valor de 'nome', que � um objeto do tipo String.
	 * @param nome O valor de 'nome' a ser configurado.
	 */
	public final void setNome(String nome) {
		this.nome = nome;
	}

	
}
