/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : Dec 26, 2006
 *
 * Historico de Modifica��o:
 * =========================
 * Dec 26, 2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;


/**
 * Interface que 
 *
 * @author Felipe
 *
 */
public interface IEmail {

	/**
	 * @return the destinatario
	 */
	public String getEmail();
	
	public String getNome();
	
}