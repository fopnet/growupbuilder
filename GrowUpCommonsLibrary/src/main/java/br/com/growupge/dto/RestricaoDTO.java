/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/01/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 02/01/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.utility.NumberUtil;

/**
 * Data Transfer Object para manipular dados de
 * permiss�es.
 *
 * @author Felipe
 *
 */
public class RestricaoDTO extends MensagemRetornoDTO implements Serializable, Comparable<RestricaoDTO> {

	private static final long serialVersionUID = 1L;
	private static final Double VALOR_UFIR_2013 =  2.5473;
	private static final String MOEDA_UFIR = "UFIR ";
	private static final String MOEDA_DEFAULT = "R$ ";
	
	private Long codigo;
	private String nome; 
	private String moeda; 
	private Object valor;
	private PedidoVeiculoDTO pedido;
	
	
	/**
	 * Construtor para o flex. 
	 */
	public RestricaoDTO() {
	}
	
	/**
	 * @param nome
	 * @param valor
	 */
	public RestricaoDTO(String nome, Object valor) {
		this.nome = nome;
		this.valor = valor;
	}

	public RestricaoDTO(String nome, Double valor) {
		this(nome, (Object) valor);
		this.moeda = MOEDA_DEFAULT;
	}
	
	/**
	 * @param pedido
	 */
	public RestricaoDTO (PedidoVeiculoDTO pedido) {
		this.pedido = pedido;
	}
	
	/**
	 * @return the pedido
	 */
	public PedidoVeiculoDTO getPedido() {
		return pedido;
	}

	/**
	 * @param newDTO
	 */
	public void setPedido(PedidoVeiculoDTO newDTO) {
		this.pedido = newDTO;
	}

	
	/**
	 * @return the codigo
	 */
	public Long getCodigo() {
		return codigo;
	}
	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * @return the moeda
	 */
	public String getMoeda() {
		if (isNotBlank(moeda))
			return moeda.trim();
		
		return moeda;
	}
	/**
	 * @param moeda the moeda to set
	 */
	public void setMoeda(String moeda) {
		this.moeda = moeda;
	}
	
	/**
	 * @return
	 */
	public Double getValorReal() {
		if (valor instanceof Double && !((Double)valor).isNaN()) { 
			if (MOEDA_UFIR.trim().equals(getMoeda().trim())) {
				return (Double)valor * VALOR_UFIR_2013;
			} else
				return (Double) valor;
		}
		return null;
	}
	
	/**
	 * @return the valor
	 */
	public Object getValor() {
		return valor;
	}
	
	/**
	 * @param valor the valor to set
	 */
	public void setValor(Object valor) {
		this.valor = valor;
		if (!NumberUtil.isDecimal(String.valueOf( valor )))
			setMoeda( null );
	}

	/**
	 * @throws GrowUpException 
	 * 
	 */
	public void validate() throws GrowUpException {
		if (isBlank(getNome()))
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof RestricaoDTO)) {
			return false;
		}
		
		RestricaoDTO other = (RestricaoDTO) obj;
		return new EqualsBuilder().append(codigo, other.codigo)
								.isEquals();
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(RestricaoDTO o) {
		if (o.getValor() != null && getValor() != null) {
			//Se for String , Double -> String antes
			if (o.getValor() instanceof String && getValor() instanceof Double)
				return 1;
			//Se for Double , String -> String depois
			else if (o.getValor() instanceof Double && getValor() instanceof String)
				return -1;
			else
				return getValor().toString().compareTo(o.getValor().toString());
		} else if (o.getValor() == null ^ getValor() == null) {
			return  o.getValor() == null ? -1 : 1; 
		}
		else
			return 0;
	}

	


}
