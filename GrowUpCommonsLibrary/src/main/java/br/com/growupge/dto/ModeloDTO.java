/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Data Transfer Object para manipular dados da marca.
 *
 * @author Felipe
 *
 */
public class ModeloDTO extends MensagemRetornoDTO implements Serializable {

	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = -1186783688704357672L;

	/**
	 * Atributo '<code>codigo</code>' do tipo String
	 */
	private Long codigo;
	
	/**
	 * Atributo '<code>nome</code>' do tipo String
	 */
	private String nome;

	/**
	 *  Atributo '<code>codigoFipe</code>' do tipo String
	 */
	private String codigoFipe;
	
	/**
	 * Atributo '<code>marca</code>' do tipo MarcaDTO
	 */
	private MarcaDTO marca;
	
	/**
	 *  Atributo '<code>tipo</code>' do tipo String
	 */
	private String tipo;
	
	/**
	 *  Atributo '<code>anos</code>' do tipo List<AnoModeloDTO>
	 */
	private List<AnoModeloDTO> anos;
	
	/**
	 * Construtor para esta classe.
	 *
	 */
	public ModeloDTO() {
	}

	/**
	 * @param long1
	 */
	public ModeloDTO(Long cd) {
		this.codigo = cd;
	}

	/**
	 * @param nome
	 */
	public ModeloDTO(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}


	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}


	/**
	 * @return the marca
	 */
	public MarcaDTO getMarca() {
		return marca;
	}


	/**
	 * @param marca the marca to set
	 */
	public void setMarca(MarcaDTO marca) {
		this.marca = marca;
	}


	/**
	 * @return the codigo
	 */
	public Long getCodigo() {
		return codigo;
	}


	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the codigoFipe
	 */
	public String getCodigoFipe() {
		return codigoFipe;
	}

	/**
	 * @param codigoFipe the codigoFipe to set
	 */
	public void setCodigoFipe(String codigoFipe) {
		this.codigoFipe = codigoFipe;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the anos
	 */
	public List<AnoModeloDTO> getAnos() {
		return anos;
	}

	/**
	 * @param anos the anos to set
	 */
	public void setAnos(List<AnoModeloDTO> anos) {
		this.anos = anos;
	}

	
}
