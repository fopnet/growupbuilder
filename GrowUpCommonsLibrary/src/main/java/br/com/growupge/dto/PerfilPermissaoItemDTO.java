/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 09/01/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 09/01/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;

/**
 * Data Transfer Object para manipular dados da lista de
 * perfil x permiss�es.
 *
 * @author Felipe
 *
 */
public class PerfilPermissaoItemDTO extends MensagemRetornoDTO implements
		Serializable {

	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 8890841642495312345L;

	/**
	 * Atributo '<code>perfil</code>' do tipo PerfilDTO
	 */
	private PerfilDTO perfil;

	/**
	 * Atributo '<code>permissao</code>' do tipo PermissaoDTO
	 */
	private PermissaoDTO permissao;

	/**
	 * Construtor para esta classe.
	 *
	 */
	public PerfilPermissaoItemDTO() {
	}

	/**
	 * @param dto
	 */
	public PerfilPermissaoItemDTO(PermissaoDTO dto) {
		this.setPermissao(dto);
	}

	/**
	 * @return the permissao
	 */
	public PermissaoDTO getPermissao() {
		return permissao;
	}

	/**
	 * @param permissao the permissao to set
	 */
	public void setPermissao(PermissaoDTO permissao) {
		this.permissao = permissao;
	}

	/**
	 * @return the perfil
	 */
	public PerfilDTO getPerfil() {
		return perfil;
	}

	/**
	 * @param perfil the perfil to set
	 */
	public void setPerfil(PerfilDTO perfil) {
		this.perfil = perfil;
	}

	
	

}
