/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 22/12/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 22/12/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.Serializable;

/**
 * Data Transfer Object para manipular dados de
 * permiss�es.
 *
 * @author Felipe
 *
 */
public class PermissaoDTO extends MensagemRetornoDTO implements Serializable {

	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 3702942919876895179L;

	/**
	 * Atributo '<code>codigo</code>' do tipo String
	 */
	private String codigo;

	/**
	 * Atributo '<code>descricaoBR</code>' do tipo String
	 */
	private String descricao;
	
	/**
	 * Construtor para esta classe.
	 *
	 */
	public PermissaoDTO() {
	}

	public PermissaoDTO(String codigo) {
		setCodigo(codigo);
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
		
		if (isNotBlank(codigo))
			this.codigo = this.codigo.toUpperCase().trim();
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	
	

}
