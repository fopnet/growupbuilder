package br.com.growupge.dto;

import static br.com.growupge.utility.Reflection.isCodigoValido;
import static org.apache.commons.lang3.StringUtils.isBlank;
import br.com.growupge.exception.GrowUpException;


/**
 * @author Felipe
 *
 */
public class PagamentoBoletoDTO extends PagamentoDTO {

	/**
	 *  Atributo '<code>BOLETO_CORRELACAO_ID</code>' do tipo String
	 */
	public static final String BOLETO_CORRELACAO_ID = "Boleto";
	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1L;

	public PagamentoBoletoDTO(PedidoVeiculoDTO pedido) {
		super(pedido);
	}
	
	
	/* (non-Javadoc)
	 * @see br.com.growupge.dto.PagamentoDTO#getCorrelacaoId()
	 */
	@Override
	public String getCorrelacaoId() {
		return BOLETO_CORRELACAO_ID;
	}

	@Override
	public boolean isValido()
			throws GrowUpException {
		if (!isCodigoValido(getPedido())
				//|| isBlank(getCodigo())
				|| isBlank(getDescricao())
				//|| //dto.getDataSolicitacao() == null
//				|| getValor() == null
				|| isBlank(getCorrelacaoId())) {
			return false;
		}
		return true;
	}
}
