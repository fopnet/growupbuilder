/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;

/**
 * Data Transfer Object para manipular dados de empresa.
 *
 * @author Felipe
 *
 */
public class EmpresaDTO extends MensagemRetornoDTO implements Serializable, IEmail {
	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = -1186783688704357672L;
	
	public static long GROWUP_GESTAO_EMPRESARIAL_ID = 1;
	public static long GROWUP_SISTEMAS_ID = 2;
	public static long GROWUP_VEICULOS_ID = 3;

	private long codigo;
	private String nome;
	private String cnpj;
	private String email;
	
	/**
	 * Construtor para esta classe.
	 *
	 */
	public EmpresaDTO() {
	}

	/**
	 * @return the cnpj
	 */
	public final String getCnpj() {
		return cnpj;
	}

	/**
	 * @param cnpj the cnpj to set
	 */
	public final void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	/**
	 * @return the codigo
	 */
	public final long getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public final void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the nome
	 */
	public final String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public final void setNome(String nome) {
		this.nome = nome != null ? nome.toUpperCase() : nome;
	}

	
	/**
	 * @return the email
	 */
	public final String getEmail() {
		return this.email;
	}

	
	/**
	 * @param email the email to set
	 */
	public final void setEmail(String email) {
		this.email = email;
	}

}
