/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.File;
import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;

/**
 * Data Transfer Object para manipular dados dos arquivos para anexos no email
 *
 * @author Felipe
 *
 */
public class AnexoDTO  implements Serializable {

	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = -283029350794434231L;

	/**
	 * Atributo '<code>nome</code>' do tipo String
	 */
	private String nome;
	
	/**
	 * Em bytes
	 *  Atributo '<code>tamanho</code>' do tipo int
	 */
	private long tamanho;
	
	private PedidoVeiculoDTO pedido;

	/**
	 * Construtor obeigatorio por causa do AMF
	 */
	public AnexoDTO() {
	}
	
	/**
	 * Construtor para esta classe.
	 * @param dto 
	 * @param file 
	 *
	 */
	public AnexoDTO(File file, PedidoVeiculoDTO dto) {
		nome = file.getName();
		tamanho = file.length();
		pedido = dto;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the tamanho
	 */
	public long getTamanho() {
		return tamanho;
	}

	/**
	 * @param tamanho the tamanho to set
	 */
	public void setTamanho(long tamanho) {
		this.tamanho = tamanho;
	}

	/**
	 * @return the pedido
	 */
	public PedidoVeiculoDTO getPedido() {
		return pedido;
	}

	/**
	 * @param pedido the pedido to set
	 */
	public void setPedido(PedidoVeiculoDTO pedido) {
		this.pedido = pedido;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + (int) (tamanho ^ (tamanho >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		AnexoDTO other = (AnexoDTO) obj;
		return new EqualsBuilder().append(nome, other.nome)
								.append(tamanho, other.tamanho)
								.isEquals();
	}

}
