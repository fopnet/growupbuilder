/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;

import br.com.growupge.enums.TipoCombustivel;

/**
 * Data Transfer Object para manipular dados da marca.
 *
 * @author Felipe
 *
 */
public class CombustivelDTO extends MensagemRetornoDTO implements Serializable {


	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 3176225640277845011L;

	/**
	 * Atributo '<code>codigo</code>' do tipo String
	 */
	private String codigo;

	/**
	 * Atributo '<code>descricao</code>' do tipo String
	 */
	private String descricao;

	/**
	 * Construtor para esta classe.
	 *
	 * @param tipo
	 */
	public CombustivelDTO(String tipo) {
		TipoCombustivel enm = TipoCombustivel.getEnumById(tipo);
		if (enm != null) {
			this.setCodigo(enm.toString());
			this.setDescricao(enm.getDescricao());
		}
	}
	
	/**
	 * Construtor para esta classe.
	 *
	 */
	public CombustivelDTO() {
		
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
