/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/01/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 02/01/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.Serializable;

/**
 * Data Transfer Object para manipular dados de
 * permiss�es.
 *
 * @author Felipe
 *
 */
public class CargoDTO extends MensagemRetornoDTO implements Serializable {

	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 4291298387509876734L;

	/**
	 * Atributo '<code>codigo</code>' do tipo long
	 */
	private String codigo;

	/**
	 * Atributo '<code>descricaoBR</code>' do tipo String
	 */
	private String descricao;

	/**
	 * Atributo '<code>departamento</code>' do tipo String
	 */
	private DepartamentoDTO departamento;
	

	/**
	 * Construtor para esta classe.
	 *
	 */
	public CargoDTO() {
	}


	public CargoDTO(String codigo) {
		setCodigo(codigo);
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
		if (isNotBlank(this.codigo)) 
			this.codigo = this.codigo.toUpperCase().trim();
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public DepartamentoDTO getDepartamento() {
		return departamento;
	}


	public void setDepartamento(DepartamentoDTO departamento) {
		this.departamento = departamento;
	}


}
