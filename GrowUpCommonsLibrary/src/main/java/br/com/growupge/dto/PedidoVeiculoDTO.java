/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/01/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 02/01/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

/**
 * Data Transfer Object para manipular dados de
 * permiss�es.
 *
 * @author Felipe
 *
 */
public class PedidoVeiculoDTO extends MensagemRetornoDTO implements Serializable /*, Externalizable*/ {

	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 4291298387509876734L;
	
	private long codigo;
	private UsuarioDTO usuarioCadastro;
	private Date dataCadastro;
	private UsuarioDTO usuarioAlteracao;
	private Date dataAlteracao;
	private VeiculoDTO veiculo;
	
    private StatusPedido status;
    private String descricao;
    private ServicoDTO servico;
    private ClienteDTO cliente;
    private int nrGaveta;
    private ArquivoDTO arquivo;

    private Date filtroDataInicial;
    private Date filtroDataFinal;
    
    private List<RestricaoDTO> restricoes;
	private List<AnexoDTO> anexos;

	private List<PagamentoDTO> pagamentos;

	private transient Date dataPagamento;

	private Double valor;

    
	/**
	 * Construtor para esta classe.
	 *
	 */
	public PedidoVeiculoDTO() {
		//this.setStatus(StatusPedido.PENDENTE); //configurando estado inicial do pedido
		this.restricoes = new ArrayList<RestricaoDTO>();
		this.anexos = new ArrayList<AnexoDTO>();
		this.pagamentos = new ArrayList<PagamentoDTO>();
	}
	
	/**
	 * @param long1
	 */
	public PedidoVeiculoDTO(Long codigo) {
		this();
		this.codigo = codigo;
	}

	public VeiculoDTO getVeiculo() {
		if (veiculo == null) {
			setVeiculo(new VeiculoDTO());
			veiculo.setMarca(new MarcaDTO());
		}
		return veiculo;
	}
	
	public void setVeiculo(VeiculoDTO veiculo) {
		this.veiculo = veiculo;
	}
	
	public String getPlaca() { return getVeiculo().getCodigo() ; }
	public void setPlaca(String value) { getVeiculo().setCodigo(value); }
	
	/**
	 * @return the codigo
	 */
	public long getCodigo() {
		return codigo;
	}
	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}
	/**
	 * @return the usuarioCadastro
	 */
	public UsuarioDTO getUsuarioCadastro() {
		return usuarioCadastro;
	}
	
	/**
	 * @param usuarioCadastro the usuarioCadastro to set
	 */
	public void setUsuarioCadastro(UsuarioDTO usuarioCadastro) {
		this.usuarioCadastro = usuarioCadastro;
	}
	
	/**
	 * @return the dataCadastro
	 */
	public Date getDataCadastro() {
		return dataCadastro;
	}
	
	/**
	 * @param dataCadastro the dataCadastro to set
	 */
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	/**
	 * @return the usuarioAlteracao
	 */
	public UsuarioDTO getUsuarioAlteracao() {
		return usuarioAlteracao;
	}
	/**
	 * @param usuarioAlteracao the usuarioAlteracao to set
	 */
	public void setUsuarioAlteracao(UsuarioDTO usuarioAlteracao) {
		this.usuarioAlteracao = usuarioAlteracao;
	}
	/**
	 * @return the dataAlteracao
	 */
	public Date getDataAlteracao() {
		return dataAlteracao;
	}
	/**
	 * @param dataAlteracao the dataAlteracao to set
	 */
	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	/**
	 * @return the status
	 */
	public StatusPedido getStatus() {
		return status;
	}
	
	/**
	 * Escopo de pacote porque somente o enum {@link StatusPedido}
	 * pode alterar o status do pedido atrav�s de seus metodos de troca de estado.
	 * @param status the status to set
	 */
	void setStatus(StatusPedido status) {
		this.status = status;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the servico
	 */
	public ServicoDTO getServico() {
		return servico;
	}

	/**
	 * @param servico the servico to set
	 */
	public void setServico(ServicoDTO servico) {
		this.servico = servico;
	}

	/**
	 * @return the cliente
	 */
	public ClienteDTO getCliente() {
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the nrGaveta
	 */
	public int getNrGaveta() {
		return nrGaveta;
	}

	/**
	 * @param nrGaveta the nrGaveta to set
	 */
	public void setNrGaveta(int nrGaveta) {
		this.nrGaveta = nrGaveta;
	}

	/**
	 * @return the arquivo
	 */
	public ArquivoDTO getArquivo() {
		return arquivo;
	}

	/**
	 * @param arquivo the arquivo to set
	 */
	public void setArquivo(ArquivoDTO arquivo) {
		this.arquivo = arquivo;
	}

	/**
	 * @return the filtroDataInicial
	 */
	public Date getFiltroDataInicial() {
		return filtroDataInicial;
	}

	/**
	 * @param filtroDataInicial the filtroDataInicial to set
	 */
	public void setFiltroDataInicial(Date filtroDataInicial) {
		this.filtroDataInicial = filtroDataInicial;
	}

	/**
	 * @return the filtroDataFinal
	 */
	public Date getFiltroDataFinal() {
		return filtroDataFinal;
	}

	/**
	 * @param filtroDataFinal the filtroDataFinal to set
	 */
	public void setFiltroDataFinal(Date filtroDataFinal) {
		this.filtroDataFinal = filtroDataFinal;
	}

	/**
	 * @return the restricoes
	 */
	public List<RestricaoDTO> getRestricoes() {
		if (restricoes == null)
			restricoes = new ArrayList<RestricaoDTO>();
		return restricoes;
	}

	/**
	 * @param restricoes the restricoes to set
	 */
	public void setRestricoes(List<RestricaoDTO> restricoes) {
		this.restricoes = restricoes;
		if (CollectionUtils.isNotEmpty(restricoes))
			Collections.sort(restricoes);
	}
	
	public List<AnexoDTO> getAnexos() {
		if (anexos == null)
			anexos = new ArrayList<AnexoDTO>();
		return anexos;
	}
	
	/**
	 * @param anexos the anexos to set
	 */
	public void setAnexos(List<AnexoDTO> anexos) {
		this.anexos = anexos;
	}

	/**
	 * @return the pagamentos
	 */
	public List<PagamentoDTO> getPagamentos() {
		return Collections.unmodifiableList(pagamentos);
	}


	public Double getValor() {
		return this.valor;
	}
	
	/**
	 * @return
	 */
	public Date getDataPagamento() {
		//inicia com a menor data, possivel assumindo que n�o h� pagamentos 
		if (dataPagamento == null) 
			calcularDataPagamento();
					
		return this.dataPagamento;
	}

	private void calcularDataPagamento() {
		dataPagamento = getDataCadastro();
		
		// se ja existir um pagamento, recalcular a data pagamento e valor
		if (CollectionUtils.isNotEmpty(pagamentos)) {
			Double menorValor = Double.MAX_VALUE;
			
			for (PagamentoDTO pgto: pagamentos) {
				
				if (pgto.getValor() < menorValor) {
					menorValor = pgto.getValor();
					
					// atribuir a data de pagamento relativo ao menor valor do pagamento existir, atribu�-la
					if( pgto.getDataPagamento()  != null )
						dataPagamento = pgto.getDataPagamento();
				}
			}
			valor = menorValor.equals(Double.MAX_VALUE) ? null : menorValor;
		}
	}

	/**
	 * @param dto
	 */
	public void adicionarPagamentos(PagamentoDTO dto) {
		pagamentos.add(dto);
		
		dto.setPedido(this);
		
		// Se o pedido ainda estiver pendente, atualiza a data de pagamento.
		if (getStatus().isPendente())
			calcularDataPagamento();
	}

	/** 
	 * Deserializes the client state of an instance of ThirdPartyProxy 
	 * by reading in String for the name, a Map of properties 
	 * for the description, and  
	 * a floating point integer (single precision) for the price.  
	 *
//	@SuppressWarnings("unchecked")
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException { 
		// Read in the server properties from the client representation. 

		codigo = Double.valueOf( in.readDouble() ).longValue();
		usuarioCadastro = (UsuarioDTO) in.readObject();
		dataCadastro = (Date) in.readObject();
		usuarioAlteracao = (UsuarioDTO) in.readObject();
		dataAlteracao = (Date) in.readObject();
		veiculo = (VeiculoDTO) in.readObject();
		status = (StatusPedido) in.readObject();
		descricao = (String) in.readObject();
		servico = (ServicoDTO) in.readObject();
		cliente = (ClienteDTO) in.readObject();
		nrGaveta = in.readInt();
		arquivo = (ArquivoDTO) in.readObject();
		filtroDataInicial = (Date) in.readObject();
		filtroDataFinal = (Date) in.readObject();

//		restricoes = (List<RestricaoDTO>) in.readObject();
//		anexos = (List<AnexoDTO>) in.readObject();
		
//		pagamentos = (List<PagamentoDTO>) in.readObject();
		dataPagamento = (Date) in.readObject();
//		valor = Double.valueOf( in.readDouble() );
	} 


	 * 
	 * Serializes the server state of an instance of ThirdPartyProxy 
	 * by sending a String for the name, a Map of properties 
	 * String for the description, and a floating point 
	 * integer (single precision) for the price. Notice that the inventory  
	 * identifier is not sent to external clients.
	 * 
	public void writeExternal(ObjectOutput out) throws IOException { 
		// Write out the client properties from the server representation. 
		out.writeDouble( codigo );
		out.writeObject( usuarioCadastro );
		out.writeObject( dataCadastro );
		out.writeObject( usuarioAlteracao );
		out.writeObject( dataAlteracao );
		out.writeObject( veiculo );
		out.writeObject( status );
		out.writeObject( descricao );
		out.writeObject( servico );
		out.writeObject( cliente );
		out.writeInt( nrGaveta );
		out.writeObject( arquivo );
		out.writeObject( filtroDataInicial );
		out.writeObject( filtroDataFinal );

//		out.writeObject( restricoes );
//		out.writeObject( anexos );
//		out.writeObject( pagamentos );

		out.writeObject( dataPagamento );

//		out.writeObject( valor );
	}*/ 

	
}
