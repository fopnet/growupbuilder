/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;

import br.com.growupge.constantes.Constantes;

/**
 * Data Transfer Object para manipular dados da marca.
 *
 * @author Felipe
 *
 */
public class ServicoDTO extends MensagemRetornoDTO implements Serializable {

	private static long PESQUISA_SIMPLES_ID = 8;
	
	public static ServicoDTO getPesquisaSimplesServico() {
		return new ServicoDTO(PESQUISA_SIMPLES_ID);
	}
	
	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = -1186783688704357672L;

	/**
	 * Atributo '<code>codigo</code>' do tipo String
	 */
	private long codigo;

	/**
	 * Atributo '<code>nome</code>' do tipo String
	 */
	private String nome;
	
	/**
	 * Atributo '<code>descricao</code>' do tipo String
	 */
	private String descricao;
	
	/**
	 * Atributo '<code>isTramitado</code>' do tipo String
	 */
	private String isTramitado;
	
	/**
	 * Construtor para esta classe.
	 *
	 */
	public ServicoDTO() {
	}

	/**
	 * @param codigo
	 */
	public ServicoDTO(long codigo) {
		setCodigo(codigo);
	}

	/**
	 * @return the isTramitado
	 */
	public String getIsTramitado() {
		return isTramitado;
	}

	/**
	 * @return
	 */
	public boolean isTramitado() {
		return Constantes.SIM.equals(getIsTramitado());
	}

	/**
	 * @param isTramitado the isTramitado to set
	 */
	public void setIsTramitado(String isTramitado) {
		this.isTramitado = isTramitado;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the codigo
	 */
	public long getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}


	
}
