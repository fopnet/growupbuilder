package br.com.growupge.dto;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.utility.Reflection;

/**
 * Valores poss�veis para Status dos pedidos
 * 
 * @author Felipe
 * 
 */
public enum StatusPedido implements Serializable, IStatusPedido /* , Externalizable*/ {
	PENDENTE("PD","Pendente"),
	
	A_FATURAR("AF","A faturar"),
	
	CONCLUIDO("PG","Conclu�do"),
	
	FATURADO("FT","Faturado"),
	
	ENVIADO("EV","Enviado"), 
	
	EXPIRADO("EX","Expirado"),
	
	CANCELADO("CL","Cancelado");
	
	
	/////////////////////////////////////////////////////////////////////////////////////
	// Variaveis, Getters and Setters
	/////////////////////////////////////////////////////////////////////////////////////
	
	private String tipo;
	private String descricao;

	StatusPedido(String tipo, String descricao) {
        this.tipo = tipo;
		this.descricao = descricao;
    }
	
	/**
	 * @return the descricao
	 */
	public String getDescricao() {	return descricao;	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {	this.descricao = descricao; }
	
	public String toString() {	return tipo; }

	public String getTipo() {	return tipo; }

	public void setTipo(String value) {
		///faz nada
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	// Metodos delegate
	/////////////////////////////////////////////////////////////////////////////////////
	private IStatusPedido getState() {
		for (AbstractState state : states) {
			if (this.equals(state.sts) )
				return state;
		}
		return null;
	}
	
	@Override
	public void cancelar(PedidoVeiculoDTO pedido) throws GrowUpException {	getState().cancelar(pedido); } 
	
	@Override
	public void concluir(PedidoVeiculoDTO pedido) throws GrowUpException {	getState().concluir(pedido); } 
	
	@Override
	public void pagar(PedidoVeiculoDTO pedido) throws GrowUpException {	getState().pagar(pedido); } 
	
	@Override
	public void enviar(PedidoVeiculoDTO pedido) throws GrowUpException{	getState().enviar(pedido); } 
	
	@Override
	public void expirar(PedidoVeiculoDTO pedido) throws GrowUpException {	getState().expirar(pedido); } 
	
	@Override
	public void pendente(PedidoVeiculoDTO pedido) throws GrowUpException {	getState().pendente(pedido); }

	@Override
	public boolean isEnviado() { return getState().isEnviado();	}

	@Override
	public boolean isPendente() { return getState().isPendente(); }	

	@Override
	public boolean isExpirado() { return getState().isExpirado(); }

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// States. 	Esta implementa��o foi necess�ria usando classes, porque implementado dentro do pr�prio enum,
	// 			o flex n�o reconhece o objeto por algum motivo ainda desconhecido.
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private final static List<AbstractState> states = Arrays.asList(new PendenteState(), 
																	new AFaturarState(),
																	new ConcluidoState(),
																	new FaturadoState(),
																	new EnviadoState(),
																	new ExpiradoState(),
																	new CanceladoState());	
	
	abstract static class AbstractState implements IStatusPedido {

		private StatusPedido sts;

		protected AbstractState(StatusPedido sts) { this.sts = sts; }
		
		@Override
		public void cancelar(PedidoVeiculoDTO pedido) throws GrowUpException {
			pedido.setStatus(CANCELADO);
		}
		
		@Override
		public void concluir(PedidoVeiculoDTO pedido) throws GrowUpException {
			// Forma de conceder de cortesia
			pedido.setStatus(CONCLUIDO);
		}

		@Override
		public void pagar(PedidoVeiculoDTO pedido) throws GrowUpException {
			pedido.setStatus(FATURADO);
		}

		@Override
		public void enviar(PedidoVeiculoDTO pedido) throws GrowUpException {
			pedido.setStatus(ENVIADO);
		}

		@Override
		public void expirar(PedidoVeiculoDTO pedido) throws GrowUpException {
			pedido.setStatus(EXPIRADO);
		}

		@Override
		public void pendente(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "Pedido n�o pode voltar a estar pendente depois do estado ".concat(sts.name()));
		}

		@Override
		public boolean isEnviado() { return false; }

		@Override
		public boolean isPendente() { return false; }

		@Override
		public boolean isExpirado() { return false;	 }
		
	}
	
	private static class PendenteState extends AbstractState {

		private PendenteState() { super(StatusPedido.PENDENTE);	}
		
		@Override
		public void enviar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "Pedido ainda encontra-se pendente de conclus�o.");
		}
		
		public void concluir(PedidoVeiculoDTO pedido) throws GrowUpException {
			// Forma de conceder cortesia
			super.concluir(pedido);
		}
		
		@Override
		public void pendente(PedidoVeiculoDTO pedido) throws GrowUpException {
			if (pedido.getStatus() == null 
					|| PENDENTE.equals(pedido.getStatus()) 
					|| A_FATURAR.equals(pedido.getStatus())) {
				
				if (Reflection.isCodigoValido(pedido.getCliente()))
					pedido.setStatus(A_FATURAR);
				else 
					pedido.setStatus(PENDENTE);
			} else
				throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "Status atual do pedido inv�lido.");
		}

		public boolean isPendente() { return true; }
		
	}
	
	private static class AFaturarState extends AbstractState {
			
		private AFaturarState() { super(StatusPedido.A_FATURAR); }

		@Override
		public void cancelar(PedidoVeiculoDTO pedido) throws GrowUpException {
			if (StringUtils.isBlank(pedido.getMensagemRetorno()))
				throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "� obrigat�rio uma justificativa para cancelar um pedido pendente de pagamento.");
			super.cancelar(pedido);
		}

		public void concluir(PedidoVeiculoDTO pedido) throws GrowUpException {
			// Forma de conceder de cortesia
			super.concluir(pedido);
		}
		
		@Override
		public void pendente(PedidoVeiculoDTO pedido) throws GrowUpException {
			if (Reflection.isCodigoValido(pedido.getCliente()))
				pedido.setStatus(A_FATURAR);
			else 
				pedido.setStatus(PENDENTE);
		}

		public void enviar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "Pedido ainda encontra-se pendente de conclus�o.");
		}

		public boolean isPendente() { return true; }
	}
	
	private static class ConcluidoState extends AbstractState {
		
		private ConcluidoState() { super(StatusPedido.CONCLUIDO);	}
		
		public void cancelar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "Pedido conclu�do n�o pode ser cancelado.");
		}

		public void pagar(PedidoVeiculoDTO pedido) throws GrowUpException {
			// Caso o pedido esteja concluido, ele pode retornar para o estado anterior, se uma nova pendencia for criada
			super.pagar(pedido);
		}

		public void expirar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "N�o � poss�vel expirar um pedido j� conclu�do.");
		}
	}
	
	private static class FaturadoState extends AbstractState {

		private FaturadoState() { super(StatusPedido.FATURADO);	}
		
		public void cancelar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "N�o � poss�vel cancelar um pedido j� faturado.");
		}

		public void pagar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "O pedido j� est� pago.");
		}

		public void expirar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "N�o � poss�vel expirar um pedido faturado.");
		}
	}
	
	private static class EnviadoState extends AbstractState {

		private EnviadoState() { super(StatusPedido.ENVIADO);	}
		
		public void cancelar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "N�o � poss�vel cancelar um pedido j� enviado ao cliente.");
		}

		public void concluir(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "N�o � poss�vel concluir um pedido j� enviado ao cliente.");
		}

		public void pagar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "N�o � poss�vel pagar um pedido j� enviado ao cliente.");
		}

		public void expirar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "N�o � poss�vel expirar um pedido j� enviado.");
		}

		public boolean isEnviado() { return true; }
	}
	
	private static class ExpiradoState extends AbstractState {

		private ExpiradoState() { super(StatusPedido.EXPIRADO);	}
		
		public void cancelar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "N�o � poss�vel cancelar um pedido expirado.");
		}
	
		public void concluir(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "N�o � poss�vel concluir um pedido expirado.");
		}
	
		public void pagar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "N�o � poss�vel pagar um pedido expirado.");
		}
		
		public void enviar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "N�o � poss�vel enviar um pedido expirado.");
		}
		
		public void expirar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "N�o � poss�vel expirar um pedido j� expirado.");
		}
		
		public boolean isExpirado() { return true; }
	}
	
	private static class CanceladoState extends AbstractState {

		private CanceladoState() { super(StatusPedido.CANCELADO);	}
		
		public void cancelar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "N�o � poss�vel cancelar um pedido j� cancelado.");
		}

		public void concluir(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "N�o � poss�vel concluir um pedido j� cancelado.");
		}

		public void pagar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "N�o � poss�vel pagar um pedido j� cancelado.");
		}

		public void enviar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "N�o � poss�vel enviar um pedido j� cancelado.");
		}

		public void expirar(PedidoVeiculoDTO pedido) throws GrowUpException {
			throw new GrowUpException(MSGCODE.MENSAGEM_NAO_CADASTRADA, "N�o � poss�vel expirar um pedido j� cancelado.");
		}
	}

}