/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;

/**
 *  Data Transfer Object para manipular dados da lista de grupo servidor.
 *
 * @author Felipe
 *
 */
public class GrupoServidorItemDTO extends MensagemRetornoDTO implements
		Serializable {
	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 6499354863243826521L;
	
	public String nomeServidor; 	
	
	/**
	 * Atributo '<code>porta</code>' do tipo int
	 */
	public int porta;

	/**
	 * Atributo '<code>login</code>' do tipo String
	 */
	public String login;

	/**
	 * Atributo '<code>senha</code>' do tipo String
	 */
	public String senha;

	/**
	 * Atributo '<code>diretorioRaiz</code>' do tipo String
	 */
	public String diretorioRaiz;

	/**
	 * Atributo '<code>status</code>' do tipo String
	 */
	public String status;

	/**
	 * Construtor para esta classe.
	 *
	 */
	public GrupoServidorItemDTO() {
	}

}
