/**
 * 
 */
package br.com.growupge.dto;

import static br.com.growupge.utility.Reflection.isCodigoValido;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.Date;

import br.com.growupge.exception.GrowUpException;
import br.com.growupge.utility.DateUtil;
import br.com.growupge.utility.NumberUtil;


/**
 * @author Felipe
 *
 */
public class PagamentoDTO extends MensagemRetornoDTO {

	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 3676471134197309974L;

	public static final int PERIODO_VALIDADE_CHAVE = 1;
	public static final String PERIODO_VALIDADE_DESCRICAO = PERIODO_VALIDADE_CHAVE + " dia";
	
	private String codigo;

	private PedidoVeiculoDTO pedido;

	private String descricao;

	private Date dataPagamento;
	
	private Date dataSolicitacao;
	
	private String transacaoStatus;

	private String transacaoId;

	private String status;

	private String resposta;

	private String correlacaoId;
	
	private Double valor;

	/**
	 * Construtor padr�o 
	 */
	PagamentoDTO() {
		
	}
	
	/**
	 * Um pagamento � uma composi��o do pedido.
	 */
	public PagamentoDTO(PedidoVeiculoDTO pedido) {
		this.setPedido(pedido);
	}
	
	/**
	 * @param payKey
	 */
	public void setCodigo(String ipnValue) {
		codigo = ipnValue;
	}

	/**
	 * @param correlationId
	 */
	public void setCorrelacaoId(String correlationId) {
		this.correlacaoId = correlationId;
		
	}

	/**
	 * @return the dataSolicitacao
	 */
	public Date getDataSolicitacao() {
		return dataSolicitacao;
	}
	
	/**
	 * @param timestamp
	 */
	public void setDataSolicitacao(Date timestamp) {
		this.dataSolicitacao = timestamp;
	}

	/**
	 * @param ack
	 */
	public void setResposta(String ack) {
		this.resposta = ack;
		
	}

	/**
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @param transactionId
	 */
	public void setTransacaoId(String transactionId) {
		this.transacaoId = transactionId;
		
	}

	/**
	 * 
	 * @param transactionStatus
	 */
	public void setTransacaoStatus(String transactionStatus) {
		this.transacaoStatus = transactionStatus;
		
	}

	/**
	 * @return
	 */
	public String getCodigo() {
		return codigo;
	}


	/**
	 * @return
	 */
	public PedidoVeiculoDTO getPedido() {
		return pedido;
	}
	
	/**
	 * @param memo
	 */
	public void setDescricao(String memo) {
		this.descricao = memo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		if (isBlank(descricao) && isCodigoValido(getPedido())) {
			return "Pedido nip:" + getPedido().getCodigo();
		} else 
			return descricao;
	}

	/**
	 * @param pedido the pedido to set
	 */
	public void setPedido(PedidoVeiculoDTO pedido) {
		this.pedido = pedido;
	}

	/**
	 * @return the dataPagamento
	 */
	public Date getDataPagamento() {
		return dataPagamento;
	}

	/**
	 * @return
	 */
	public String getDataPagamentoFormatada() {
		final DateUtil dm = DateUtil.getDateManagerInstance(dataPagamento);
		return dm.getShortDate();
	}

	/**
	 * Calcula a data de vencimento a partir de uma data.
	 * @param dt
	 * @return
	 */
	public Date calcularDataVencimento() {
		if (getDataSolicitacao() == null)
			setDataSolicitacao(new Date());
		
		final DateUtil dm = DateUtil.getDateManagerInstance(getDataSolicitacao());
		dm.addDays(PERIODO_VALIDADE_CHAVE);
		return dm.getDateTime();
	}

	public String getDataVencimentoFormatada() {
		final DateUtil dm = DateUtil.getDateManagerInstance(calcularDataVencimento());
		return dm.getShortDate();
	}

	/**
	 * @param ipnValue
	 */
	public void setDataPagamento(Date ipnValue) {
		this.dataPagamento = ipnValue;
	}

	/**
	 * @return the transacaoStatus
	 */
	public String getTransacaoStatus() {
		return transacaoStatus;
	}

	/**
	 * @return the transacaoId
	 */
	public String getTransacaoId() {
		return transacaoId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @return the resposta
	 */
	public String getResposta() {
		return resposta;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double total) {
		this.valor = total;
	}

	public String getCorrelacaoId() {
		return correlacaoId;
	}

	public String getPlaca() {
		return getPedido() != null ? getPedido().getPlaca() : null;
	}

	/**
	 * @return
	 */
	public String getValorMoedaFormatado() {
		return NumberUtil.formatarMoeda(getValor());
	}

	public String getValorDecimalFormatado() {
		return NumberUtil.formatarDecimal(getValor());
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.business.NegocioBase#isValido(java.lang.Object)
	 */
	public boolean isValido() throws GrowUpException {
		return false;
	}

	/**
	 * @param correlacaoId
	 * @param pedido
	 * @return
	 */
	public static PagamentoDTO instanciar(String correlacaoId, PedidoVeiculoDTO pedido) {
		if (PagamentoBoletoDTO.BOLETO_CORRELACAO_ID.equals(correlacaoId))
			return new PagamentoBoletoDTO(pedido);
		else 
			return new PagamentoPayPalDTO(pedido);
	}

	/**
	 * Verifica se existe chave, e se a mesma est� v�lida pelo per�odo estipulado pelo paypal.
	 * @return
	 */
//	public boolean isChaveValida() {
//		return isNotBlank(getCodigo())
//				&& getDataSolicitacao() != null
//				&& StatusPedido.PENDENTE.toString().equals(getPedido().getStatus())
//				&& getDateManagerInstance().diffHour(getDataSolicitacao()) <= (PERIODO_VALIDADE_CHAVE * 24);
//	}
//	
}
