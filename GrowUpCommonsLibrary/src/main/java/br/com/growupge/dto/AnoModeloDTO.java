/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;

/**
 * Data Transfer Object para manipular dados da marca.
 *
 * @author Felipe
 *
 */
public class AnoModeloDTO extends MensagemRetornoDTO implements Serializable {

	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = -1186783688704357672L;

	/**
	 * Atributo '<code>codigo</code>' do tipo String
	 */
	private long codigo;
	
	/**
	 * Atributo '<code>marca</code>' do tipo MarcaDTO
	 */
	private Integer ano;
	
	/**
	 *  Atributo '<code>combustivel</code>' do tipo String
	 */
	private String combustivel;
	
	private String valorFipe;
	
	private ModeloDTO modelo;
	
	/**
	 * Construtor para esta classe.
	 *
	 */
	public AnoModeloDTO() {
	}

	/**
	 * Construtor para esta classe.
	 *
	 */
	public AnoModeloDTO(ModeloDTO modelo) {
		this.modelo = modelo;
	}

	/**
	 * @return the codigo
	 */
	public Long getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the ano
	 */
	public Integer getAno() {
		return ano;
	}

	/**
	 * @param ano the ano to set
	 */
	public void setAno(Integer ano) {
		this.ano = ano;
	}

	/**
	 * @return the combustivel
	 */
	public String getCombustivel() {
		return combustivel;
	}

	/**
	 * @param combustivel the combustivel to set
	 */
	public void setCombustivel(String combustivel) {
		this.combustivel = combustivel;
	}

	/**
	 * @return the valorFipe
	 */
	public String getValorFipe() {
		return valorFipe;
	}

	/**
	 * @param valorFipe the valorFipe to set
	 */
	public void setValorFipe(String valorFipe) {
		this.valorFipe = valorFipe;
	}

	/**
	 * @return the modelo
	 */
	public ModeloDTO getModelo() {
		return modelo;
	}

	/**
	 * @param modelo the modelo to set
	 */
	public void setModelo(ModeloDTO modelo) {
		this.modelo = modelo;
	}

	
}
