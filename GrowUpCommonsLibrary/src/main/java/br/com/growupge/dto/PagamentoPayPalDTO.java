/**
 * 
 */
package br.com.growupge.dto;

import static br.com.growupge.utility.NumberUtil.isDecimal;
import static br.com.growupge.utility.Reflection.isCodigoValido;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.growupge.exception.GrowUpException;
import br.com.growupge.utility.DateUtil;

/**
 * @author Felipe
 *
 */
public class PagamentoPayPalDTO extends PagamentoDTO {

	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1L;
	
	public PagamentoPayPalDTO(PedidoVeiculoDTO pedido) {
		super(pedido);
	}

	/**
	 * @see br.com.growupge.dto.PagamentoDTO#setCodigo(java.lang.String)
	 * 
	 * atribui a chave de pagamento normal ou de uma url  
	 * payKey=${payKey}.
	 */
	@Override
	public void setCodigo(String ipnValue) {
		if (ipnValue == null || !ipnValue.contains("http://"))
			super.setCodigo(ipnValue);
		else {
			Pattern p = Pattern.compile("((?<=paykey=)[\\w-]+(^&)?)");
			Matcher matcher = p.matcher(ipnValue);
			if (matcher.find()) {
				super.setCodigo(matcher.group());
			}
		}
	}

	/**
	 * @param timestamp
	 */
	public void setDataSolicitacao(String timestamp) {
		this.setDataSolicitacao(DateUtil.getDateManagerInstancePayPal().getDate(timestamp));
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.dto.PagamentoDTO#isValido()
	 */
	@Override
	public boolean isValido() throws GrowUpException {
		if (this == null
				|| !isCodigoValido(getPedido())
				|| isBlank(getCodigo())
				|| isDecimal(getCodigo())
				|| isBlank(getDescricao())
				//|| //dto.getDataSolicitacao() == null
//				|| getValor() == null
				|| isBlank(getCorrelacaoId())) {
			return false;
		}
		return true;
	}

	/** 
	 * The status of the payment. Possible values are: CREATED �
	 * The payment request was received; funds will be
	 * transferred once the payment is approved COMPLETED � The
	 * payment was successful INCOMPLETE � Some transfers
	 * succeeded and some failed for a parallel payment or, for
	 * a delayed chained payment, secondary receivers have not
	 * been paid ERROR � The payment failed and all attempted
	 * transfers failed or all completed transfers were
	 * successfully reversed REVERSALERROR � One or more
	 * transfers failed when attempting to reverse a payment
	 * PROCESSING � The payment is in progress PENDING � The
	 * payment is awaiting processing
	 * 
	 * The status of the payment. Possible values are:
	 *	CREATED � The payment request was received; funds will be transferred once the payment is approved
	 *  COMPLETED � The payment was successful
	 *  INCOMPLETE � Some transfers succeeded and some failed for a parallel payment or, for a delayed chained payment, secondary receivers have not been paid
	 *  ERROR � The payment failed and all attempted transfers failed or all completed transfers were successfully reversed
	 *  REVERSALERROR � One or more transfers failed when attempting to reverse a payment
	 *  PROCESSING � The payment is in progress
	 *  PENDING � The payment is awaiting processing
	 */
	@Override
	public void setStatus(String status) {
		super.setStatus(status);
	}

	/** 
	 * Correlation identifier. It is a 13-character,
	 * alphanumeric string (for example, db87c705a910e) that is
	 * used only by PayPal Merchant Technical Support. Note: You
	 * must log and store this data for every response you
	 * receive. PayPal Technical Support uses the information to
	 * assist with reported issues.
	 * */
	@Override
	public void setCorrelacaoId(String correlationId) {
		// TODO Auto-generated method stub
		super.setCorrelacaoId(correlationId);
	}

	/** 
	 * Possible values are:
	 * Completed
	 * Pending
	 * Refunded
	 */
	@Override
	public void setTransacaoStatus(String transactionStatus) {
		super.setTransacaoStatus(transactionStatus);
	}
	
	
	
	
}
