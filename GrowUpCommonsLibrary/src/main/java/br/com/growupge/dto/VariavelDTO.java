/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;

/**
 * Data Transfer Object para manipular dados da variavel.
 *
 * @author Felipe
 *
 */
public class VariavelDTO extends MensagemRetornoDTO implements Serializable {
	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = -1186783688704357632L;

	private String codigo;
	private String valor;

	public static final VariavelDTO VALOR_UNITARIO_PEDIDO = new VariavelDTO( "VAL_UNIT_PEDIDO" );
	public static final VariavelDTO VALOR_UNITARIO_ADMIN = new VariavelDTO( "VAL_UNIT_PEDIDO" );
	static {
		VALOR_UNITARIO_ADMIN.setValor("1");
	}
	
	public VariavelDTO(String cod) {
		this.codigo = cod;
	}

	public VariavelDTO() {
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getValor() {
		return valor;
	}
	
	public Double getValorAsDouble() {
		return Double.parseDouble(getValor());
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
}
