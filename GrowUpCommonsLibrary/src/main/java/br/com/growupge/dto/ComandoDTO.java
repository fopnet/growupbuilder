/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : Dec 26, 2006
 *
 * Historico de Modifica��o:
 * =========================
 * Dec 26, 2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * DTO respons�vel para encapsular os comandos provenientes do Flex
 *
 * @author elmt
 *
 */
public class ComandoDTO implements Serializable {

	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 4036185753790053425L;

	/**
	 * Atributo '<code>SID</code>' do tipo String
	 */
	private String SID;
	
	/**
	 * Atributo '<code>UUID</code>' do tipo String
	 */
//	private String UUID;

	/**
	 * Atributo '<code>mapaGCS</code>' do tipo String
	 */
	private String mapaGCS;

	/**
	 * Atributo '<code>permissao</code>' do tipo String
	 */
	private String permissao;

	/**
	 * Atributo '<code>parametrosLista</code>' do tipo ArrayList<DTOPadrao>
	 */
	private List<Object> parametrosLista;

	public String getMapaGCS() { return mapaGCS;}
	public void setMapaGCS(String value) { this.mapaGCS = value; }

	public String getPermissao() { 	return permissao; }
	public void setPermissao(String value) { this.permissao = value; }
	
	public String getSID() { return SID; }
	public void setSID(String value) { SID = value; }
	
//	public String getUUID() {	return UUID; } 
//	public void setUUID(String value) {	UUID = value;}
	
	public ComandoDTO() {
		this.setParametrosLista(new ArrayList<Object>());
	}
	public List<Object> getParametrosLista() {
		return parametrosLista;
	}
	public void setParametrosLista(List<Object> parametrosLista) {
		this.parametrosLista = parametrosLista;
	}

}
