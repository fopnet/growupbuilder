package br.com.growupge.dto;


public class UserDTO  {
		private String userID; 
		private String name; 
		private String email;
//		private TipoUsuario tipo;
//
//		public String getTipo() { return tipo.toString(); } 
//		public void setTipo(String value) { this.tipo = TipoUsuario.getEnum(value); } 

		public String getEmail() { return email; } 
		public void setEmail(String value) { this.email = value; } 
		
		public String getUserID() { return userID;	}
		public void setUserID(String value) { this.userID = value; }
		
		public String getName() { return name; }
		public void setName(String value) {	this.name = value; }

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			return "Id: " + this.userID + "\n" +
				   "Nome: " + this.name + "\n" +
				  "Email: " + this.email + "\n"	;
		}
	}

