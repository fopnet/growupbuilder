/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 18/12/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 18/12/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.Serializable;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.enums.TipoPerfil;

/**
 * DTO utilizado para transportar os dados do
 * usu�rio.
 *
 * @author Felipe
 *
 */
public class UsuarioDTO extends MensagemRetornoDTO implements Serializable, IEmail {
	
	private static final long serialVersionUID = 2549065861160539545L;

	private String codigo;
	private String senha;
	private String email;
	private String idioma;
	private String nome;
	private String habilitado;
	private Date dataUltimoAcesso;
	private Date dataUltimaAlteracaoSenha;
	private Date dataExpiracaoSenha;
	private String telefone;
	private String obs;
	private EmpresaDTO empresa;
	private PerfilDTO perfil;
	private ClienteDTO cliente; 

	// Preferencias
    private int qtdRegistros;

	// Parametros da senha de acordo com a regra
	public static final short TAMANHO_MINIMO_SENHA = 6;

	public static final int TAMANHO_MINIMO_LOGIN = 4;

	public static final int TAMANHO_MAXIMO_LOGIN = 40;

	public static final int TAMANHO_MINMO_NOME = 3;

	public static final int TAMANHO_MAXIMO_EMAIL = 40;

	public UsuarioDTO() {}
	
	public UsuarioDTO(String login) {
		this.setCodigo(login);
	}
	
	/**
	 * @return the tamanhoMaximoEmail
	 */
	public int getTamanhoMaximoEmail() {
		return TAMANHO_MAXIMO_EMAIL;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = isNotBlank(codigo) ? codigo.trim().toUpperCase() : codigo;
	}

	/**
	 * @return the senha
	 */
	public String getSenha() {
		return senha;
	}

	/**
	 * @param senha the senha to set
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = isNotBlank(email) ? email.trim().toLowerCase() : null;
	}
	
	/**
	 * @return the idioma
	 */
	public String getIdioma() {
		return idioma;
	}

	/**
	 * @param idioma the idioma to set
	 */
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the habilitado
	 */
	public String getHabilitado() {
		return habilitado;
	}

	/**
	 * @param habilitado the habilitado to set
	 */
	public void setHabilitado(String habilitado) {
		this.habilitado = habilitado;
	}

	/**
	 * @return
	 */
	public boolean isUsuarioHabilitado() {
		return Constantes.SIM.equals(habilitado);
	}
	
	/**
	 * @return the dataUltimoAcesso
	 */
	public Date getDataUltimoAcesso() {
		return dataUltimoAcesso;
	}

	/**
	 * @param dataUltimoAcesso the dataUltimoAcesso to set
	 */
	public void setDataUltimoAcesso(Date dataUltimoAcesso) {
		this.dataUltimoAcesso = dataUltimoAcesso;
	}

	/**
	 * @return the dataUltimaAlteracaoSenha
	 */
	public Date getDataUltimaAlteracaoSenha() {
		return dataUltimaAlteracaoSenha;
	}

	/**
	 * @param dataUltimaAlteracaoSenha the dataUltimaAlteracaoSenha to set
	 */
	public void setDataUltimaAlteracaoSenha(Date dataUltimaAlteracaoSenha) {
		this.dataUltimaAlteracaoSenha = dataUltimaAlteracaoSenha;
	}

	/**
	 * @return the dataExpiracaoSenha
	 */
	public Date getDataExpiracaoSenha() {
		return dataExpiracaoSenha;
	}

	/**
	 * @param dataExpiracaoSenha the dataExpiracaoSenha to set
	 */
	public void setDataExpiracaoSenha(Date dataExpiracaoSenha) {
		this.dataExpiracaoSenha = dataExpiracaoSenha;
	}

	/**
	 * @return the telefone
	 */
	public String getTelefone() {
		return telefone;
	}

	/**
	 * @param telefone the telefone to set
	 */
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	/**
	 * @return the obs
	 */
	public String getObs() {
		return obs;
	}

	/**
	 * @param obs the obs to set
	 */
	public void setObs(String obs) {
		this.obs = obs;
	}

	/**
	 * @return the empresa
	 */
	public EmpresaDTO getEmpresa() {
		return empresa;
	}

	/**
	 * @param empresa the empresa to set
	 */
	public void setEmpresa(EmpresaDTO empresa) {
		this.empresa = empresa;
	}

	/**
	 * @return the perfil
	 */
	public PerfilDTO getPerfil() {
		return perfil;
	}

	/**
	 * @param perfil the perfil to set
	 */
	public void setPerfil(PerfilDTO perfil) {
		this.perfil = perfil;
	}

	/**
	 * @return the cliente
	 */
	public ClienteDTO getCliente() {
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the qtdRegistros
	 */
	public int getQtdRegistros() {
		return qtdRegistros;
	}

	/**
	 * @param qtdRegistros the qtdRegistros to set
	 */
	public void setQtdRegistros(int qtdRegistros) {
		this.qtdRegistros = qtdRegistros;
	}

	/**
	 * @return
	 */
	public boolean isLoginValido() {
		return isNotBlank(getCodigo()) && getCodigo().length() >=UsuarioDTO.TAMANHO_MINIMO_LOGIN && getCodigo().length() <=UsuarioDTO.TAMANHO_MAXIMO_LOGIN;
	}

	/**
	 * Este m�todo � respons�vel por validar cpf do usuario.
	 * 
	 * @return Retorna um objeto do tipo boolean
	 */
	public boolean isEmailValido() {
	
		try {
			if (isBlank(getEmail())) return false;
				
			Pattern padrao = Pattern.compile(".+@.+\\.[a-z]+");
			Matcher pesquisa = padrao.matcher(getEmail());
	
			if (pesquisa.matches() && getEmail().length() <= UsuarioDTO.TAMANHO_MAXIMO_EMAIL) {
				return true;
			}
	
			return false;
	
		} catch (Exception e) {
			return false;
		}
	
	}

	public boolean isNomeValido() {
		return getNome().length()  < UsuarioDTO.TAMANHO_MINMO_NOME;
	}

	/**
	 * @return
	 */
	public boolean isAdmin() {
		return TipoPerfil.ADMINISTRADOR.toString().equals(getCodigoPerfil());
	}

	/**
	 * @return
	 */
	public String getCodigoPerfil() {
		return getPerfil() != null ? getPerfil().getCodigo() : null;
	}
	
	
}
