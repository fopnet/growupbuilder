/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/01/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 02/01/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.Serializable;

/**
 * Data Transfer Object para manipular dados de
 * permiss�es.
 *
 * @author Felipe
 *
 */
public class PerfilDTO extends MensagemRetornoDTO implements Serializable {

	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 4291298387509876734L;

	/**
	 * Atributo '<code>codigo</code>' do tipo long
	 */
	private String codigo;

	/**
	 * Atributo '<code>descricaoBR</code>' do tipo String
	 */
	private String descricao;

	/**
	 * Construtor para esta classe.
	 *
	 */
	public PerfilDTO() {
	}

	/**
	 * @param codigo
	 */
	public PerfilDTO(String codigo) {
		setCodigo(codigo);
	}

	/**
	 * @return the codigo
	 */
	public final String getCodigo() {
		return this.codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public final void setCodigo(String codigo) {
		this.codigo = codigo;
		
		if (isNotBlank(codigo))
			this.codigo = codigo.toUpperCase();
	}

	/**
	 * @return the descricao
	 */
	public final String getDescricao() {
		return this.descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public final void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	
}
