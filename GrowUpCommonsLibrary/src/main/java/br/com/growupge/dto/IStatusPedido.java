package br.com.growupge.dto;

import br.com.growupge.exception.GrowUpException;

public interface IStatusPedido {

	public abstract void cancelar(PedidoVeiculoDTO pedido)
			throws GrowUpException;

	public abstract void concluir(PedidoVeiculoDTO pedido)
			throws GrowUpException;

	public abstract void pagar(PedidoVeiculoDTO pedido) throws GrowUpException;

	public abstract void enviar(PedidoVeiculoDTO pedido) throws GrowUpException;

	public abstract void expirar(PedidoVeiculoDTO pedido)
			throws GrowUpException;

	public abstract void pendente(PedidoVeiculoDTO pedido)
			throws GrowUpException;

	public abstract boolean isEnviado();

	public abstract boolean isPendente();

	public abstract boolean isExpirado();

}