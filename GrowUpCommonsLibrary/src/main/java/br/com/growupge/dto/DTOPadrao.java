/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : Jan 17, 2007
 *
 * Historico de Modifica��o:
 * =========================
 * Jan 17, 2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;

import br.com.growupge.utility.EqualsHelper;
import br.com.growupge.utility.HashCodeHelper;

/**
 * @author Felipe
 *
 */
public abstract class DTOPadrao implements Serializable {
	
	private static final long serialVersionUID = -7318709141625591903L;
	
	/**
	 * M�todo sobrescrito.
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return this.getClass().getSimpleName().substring(0, this.getClass().getSimpleName().length() - 3).toUpperCase();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeHelper.hashCode(this, "codigo");
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return EqualsHelper.isEquals(this, obj, "codigo");
	}

}

