/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.dto;

import java.io.Serializable;

/**
 * Data Transfer Object para manipular dados da marca.
 *
 * @author Felipe
 *
 */
public class ArquivoDTO extends MensagemRetornoDTO implements Serializable {

	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 3108508140490679970L;
	
	/**
	 * Atributo '<code>codigo</code>' do tipo long
	 */
	private long codigo;
	
	/**
	 * Atributo '<code>nome</code>' do tipo String
	 */
	private String nome;

	/**
	 * Construtor para esta classe.
	 *
	 */
	public ArquivoDTO() {
	}

	/**
	 * @param id
	 */
	public ArquivoDTO(long id) {
		this.setCodigo(id);
	}

	/**
	 * @return the codigo
	 */
	public long getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
