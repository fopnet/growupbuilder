/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 22/11/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 22/11/2006 - In�cio de tudo, por Felipe
 *
 */

package br.com.growupge.constantes;

/**
 * Classe utilizada para gerenciar todas as mensagens
 * do sistema.
 *
 * @author eiq0
 *
 */
public class MSGCODE {

	//--- Mensagens de Sistema -> SYS-0000
	/**
	 * Atributo '<code>SERVIDOR_OFFLINE</code>' do tipo String
	 */
	public static final String SERVIDOR_OFFLINE = "SYS-XXXX";
	
	/**
	 * Atributo '<code>SERVIDOR_OCUPADO</code>' do tipo String
	 */
	public static final String SERVIDOR_OCUPADO = "SYS-XXXX";
	
	/**
	 * Atributo '<code>SERVIDOR_SEM_ESPACO</code>' do tipo String
	 */
	public static final String SERVIDOR_SEM_ESPACO = "SYS-XXXX";
	
	/**
	 * Atributo '<code>FTP_USUARIO_SENHA_INVALIDO</code>' do tipo String
	 */
	public static final String FTP_USUARIO_SENHA_INVALIDO = "SYS-XXXX";
	
	/**
	 * Atributo '<code>FTP_CONEXAO_RECUSADA</code>' do tipo String
	 */
	public static final String FTP_CONEXAO_RECUSADA = "SYS-XXXX";
	
	public static final String FTP_DIRETORIO_LOCAL_NAO_CONFIGURADO = "SYS-XXXX";
	
	public static final String FTP_NAO_CONECTADO = "SYS-XXXX";
	
	public static final String FTP_SERVIDOR_INVALIDO = "SYS-XXXX";
	
	public static final String FTP_SERVIDOR_FALHA_SOCKET = "SYS-XXXX";
	
	public static final String FTP_DIRETORIO_SERVIDOR_NAO_EXISTE = "SYS-XXXX";
	
	public static final String FTP_ERRO_NAO_DETERMINADO = "SYS-XXXX";
	
	public static final String FTP_NAO_RECUPEROU_LISTA_DE_ARQUIVOS = "SYS-XXXX";
	
	public static final String FTP_DIRETORIO_LOCAL_NAO_ENCONTRADO = "SYS-XXXX";
	
	//--- Mensagens internas -> SYS-0000
	
	public static final String ERRO_CLASSE_NAO_ENCONTRADA = "SIS-XXXX";
	
	public static final String ERRO_ATRIBUTO_NAO_ENCONTRADO = "SIS-XXXX";
	
	public static final String PERMISSAO_NAO_CADASTRADA = "SIS-0001";
	
	public static final String REGISTRO_NAO_ENCONTRADO = "SIS-0002";
	
	public static final String COMANDO_SQL_NAO_EFETUADO = "SIS-0003";

	public static final String REGISTRO_EXCLUIDO_PREVIAMENTE = "SIS-0004";
	
	public static final String REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO = "SIS-0005";
	
	public static final String ARQUIVO_TEMPLATE_DE_EMAIL_NAO_ENCONTRADO = "SIS-0006";

	public static final String GLOBAL_STARTUP_XML_FALHA = "SYS-0007";
	
	public static final String MENSAGEM_CODIGO_NULO = "SIS-0008";

	public static final String MENSAGEM_CODIGO_NAO_ENCONTRADO = "SIS-0009";
	
	public static final String ERRO_ENTRADA_SAIDA = "SIS-0010";

	public static final String ERRO_ARQUIVO_NAO_ENCONTRADO = "SIS-0011";

	public static final String SESSAO_NAO_INICIADA = "SIS-0012";
	
	public static final String ERRO_GERAR_RELATORIO_PDF = "SIS-0013";


	//--- Mensagens internas -> MSG-0000

	public static final String CAMPOS_INVALIDOS = "MSG-0001";
	
	public static final String CAMPOS_OBRIGATORIOS = "MSG-0002";
	
	public static final String CAMPOX_OBRIGATORIOS = "MSG-0003";
	
	public static final String TAMANHO_MAXIMO_EXCEDIDO = "MSG-0004";

	public static final String TAMANHO_MINIMO_REQUERIDO = "MSG-0005";


	//-- Connectar & Desconectar
	public static final String USUARIO_CONECTADO_COM_SUCESSO = "MSG-0006";
	
	public static final String USUARIO_DESCONECTADO_COM_SUCESSO = "MSG-0007";
	
	
	//--Recriar Senha
	public static final String CODIGO_ACESSO_ENVIADO = "MSG-0008";

	public static final String ERRO_ENVIO_EMAIL = "MSG-0009";

	// Usuario
	public static final String USUARIO_INFORMAR_SENHA= "MSG-0010";

	public static final String USUARIO_SENHA_INVALIDA = "MSG-0011";
	
	public static final String USUARIO_EM_USO = "MSG-0012";

	public static final String USUARIO_DESABILITADO = "MSG-0013";
	
	public static final String USUARIO_INCLUIDO_COM_SUCESSO = "MSG-0014";

	public static final String USUARIO_ALTERADO_COM_SUCESSO = "MSG-0015";

	public static final String USUARIO_REMOVIDO_COM_SUCESSO = "MSG-0016";
	
	public static final String USUARIO_INEXISTENTE = "MSG-0017";
	
	public static final String USUARIO_EXISTENTE = "MSG-0018";

	public static final String USUARIO_JA_HABILITADO= "MSG-0019";
	
	public static final String USUARIO_HABILITADO_SUCESSO= "MSG-0026";
	
	public static final String USUARIO_EMAIL_INVALIDO= "MSG-0037";
	
	public static final String USUARIO_EMAIL_EXISTENTE= "MSG-0038";
	
	// Senha
	public static final String SENHA_ALTERADA_COM_SUCESSO = "MSG-0020";

	public static final String CODIGO_ACESSO_EXPIRADO = "MSG-0021";

	public static final String SENHA_IGUAL_ANTERIOR = "MSG-0023";
	
	public static final String SENHA_FORMATO_INVALIDO = "MSG-0024";
	
	public static final String SENHA_NAO_CONFERE = "MSG-0025";
	

	//Empresa

	public static final String EMPRESA_EXISTENTE = "MSG-0030";

	public static final String EMPRESA_INCLUIDO_COM_SUCESSO = "MSG-0031";

	public static final String EMPRESA_ALTERADO_COM_SUCESSO = "MSG-0032";

	public static final String EMPRESA_REMOVIDO_COM_SUCESSO = "MSG-0033";

	public static final String CNPJ_EXISTENTE = "MSG-0034";

	public static final String EMPRESA_EM_USO = "MSG-0036";

	public static final String EMPRESA_ITEM_INCLUIDO_COM_SUCESSO = "MSG-0086";

	public static final String EMPRESA_ITEM_ALTERADO_COM_SUCESSO = "MSG-0087";

	public static final String EMPRESA_ITEM_REMOVIDO_COM_SUCESSO = "MSG-0088";


	// Variavel

	public static final String VARIAVEL_CAMPO_OBRIGATORIO = "MSG-0040";

	public static final String VARIAVEL_DUPLICADA = "MSG-0041";

	public static final String VARIAVEL_INCLUIDO_COM_SUCESSO = "MSG-0042";

	public static final String VARIAVEL_ALTERADO_COM_SUCESSO = "MSG-0043";

	public static final String VARIAVEL_REMOVIDO_COM_SUCESSO = "MSG-0044";

	//Permissao
	public static final String PERMISSAO_NEGADA = "MSG-0050";

	public static final String PERMISSAO_EXISTENTE = "MSG-0051";

	public static final String PERMISSAO_REGISTROS_ASSOCIADOS = "MSG-0052";

	public static final String PERMISSAO_INCLUIDA_COM_SUCESSO = "MSG-0053";

	public static final String PERMISSAO_ALTERADA_COM_SUCESSO = "MSG-0054";

	public static final String PERMISSAO_EXCLUIDA_COM_SUCESSO = "MSG-0055";

	//Perfil

	public static final String PERFIL_EXISTENTE = "MSG-0060";

	public static final String PERFIL_REGISTROS_ASSOCIADOS = "MSG-0061";

	public static final String PERFIL_INEXISTENTE = "MSG-0062";

	public static final String PERFIL_INCLUIDO_COM_SUCESSO = "MSG-0063";

	public static final String PERFIL_ALTERADO_COM_SUCESSO = "MSG-0064";

	public static final String PERFIL_EXCLUIDO_COM_SUCESSO = "MSG-0065";

	public static final String PERFIL_PERMISSAO_INCLUIDO_COM_SUCESSO = "MSG-0066";
	
	public static final String PERFIL_ALTERADO_ALTERADO_COM_SUCESSO = "MSG-0067";
	
	public static final String PERFIL_PERMISSAO_EXCLUIDO_COM_SUCESSO = "MSG-0068";
	
	public static final String PERFIL_PERMISSAO_DUPLICADO = "MSG-0069";


	// Mensagem

	public static final String MENSAGEM_DUPLICADA = "MSG-0070";

	public static final String MENSAGEM_INCLUIDA_COM_SUCESSO = "MSG-0071";

	public static final String MENSAGEM_ALTERADA_COM_SUCESSO = "MSG-0072";

	public static final String MENSAGEM_REMOVIDA_COM_SUCESSO = "MSG-0073";
	

	// Pessoa
	public static final String PESSOA_INCLUIDA_COM_SUCESSO = "MSG-0075";
	
	public static final String PESSOA_INEXISTENTE = "MSG-0076";
	
	public static final String PESSOA_ALTERADA_COM_SUCESSO = "MSG-0077";

	public static final String PESSOA_REMOVIDA_COM_SUCESSO = "MSG-0078";
	
	public static final String PESSOA_EM_USO = "MSG-0079";
	
	// Cliente
	public static final String CLIENTE_INCLUIDO_COM_SUCESSO = "MSG-0080";
	
	public static final String CLIENTE_INEXISTENTE = "MSG-0081";
	
	public static final String CLIENTE_ALTERADO_COM_SUCESSO = "MSG-0082";

	public static final String CLIENTE_REMOVIDO_COM_SUCESSO = "MSG-0083";
	
	public static final String CLIENTE_EM_USO = "MSG-0084";
	
	public static final String CLIENTE_EXISTENTE = "MSG-0085";

	// Pedido
	public static final String PEDIDO_VEICULO_CONCLUIDO_ENVIADO = "MSG-0158";
	
	public static final String PEDIDO_VEICULO_CONCLUIDO_COM_SUCESSO = "MSG-0159";
	
	public static final String PEDIDO_VEICULO_MAIS_1_PENDENCIA = "MSG-0160";
	
	public static final String PEDIDO_NIP_GERADO_SUCESSO = "MSG-0161";

	//Pagamento
	public static final String PAGAMENTO_SOLICITADO_SUCESSO = "MSG-0162";
	
	public static final String PAGAMENTO_CONFIRMADO_SUCESSO = "MSG-0163";
	
	public static final String PEDIDO_VEICULO_INCLUIDO_COM_SUCESSO = "MSG-0090";
	
	public static final String PEDIDO_VEICULO_ALTERADO_COM_SUCESSO = "MSG-0091";

	public static final String PEDIDO_VEICULO_REMOVIDO_COM_SUCESSO = "MSG-0092";
	
	public static final String FATURA_CRIADO_SUCESSO = "MSG-0093";
	
	public static final String PEDIDO_JA_CONCLUIDO = "MSG-0094";
	
	// Cargo
	public static final String CARGO_INCLUIDO_COM_SUCESSO = "MSG-0095";
	
	public static final String CARGO_ALTERADO_COM_SUCESSO = "MSG-0096";
	
	public static final String CARGO_EXCLUIDO_COM_SUCESSO = "MSG-0097";
	
	// Marca
	public static final String MARCA_EM_USO = "MSG-0098";
	
	public static final String MARCA_INCLUIDO_COM_SUCESSO = "MSG-0099";
	
	public static final String MARCA_INEXISTENTE = "MSG-0100";
	
	public static final String MARCA_ALTERADO_COM_SUCESSO = "MSG-0101";

	public static final String MARCA_REMOVIDO_COM_SUCESSO = "MSG-0102";
	
	public static final String MARCA_EXISTENTE = "MSG-0103";

	// Modelo
	
	public static final String MODELO_EM_USO = "MSG-0108";
	
	public static final String MODELO_INCLUIDO_COM_SUCESSO = "MSG-0109";
	
	public static final String MODELO_ALTERADO_COM_SUCESSO = "MSG-0111";

	public static final String MODELO_REMOVIDO_COM_SUCESSO = "MSG-0112";
	
	public static final String MODELO_EXISTENTE = "MSG-0113";
	
	// Propriet�rio
	
	public static final String CPF_CNPJ_INVALIDO = "MSG-0117";
	
	public static final String PROPRIETARIO_INCLUIDO_COM_SUCESSO = "MSG-0118";
	
	public static final String PROPRIETARIO_EM_USO = "MSG-0119";
	
	public static final String PROPRIETARIO_ALTERADO_COM_SUCESSO = "MSG-0120";

	public static final String PROPRIETARIO_REMOVIDO_COM_SUCESSO = "MSG-0121";
	
	public static final String PROPRIETARIO_EXISTENTE = "MSG-0122";
	
	public static final String CPF_EXISTENTE = "MSG-0123";
	
	// Tramitacao 
	public static final String TRAMITACAO_REMOVIDA_COM_SUCESSO = "MSG-0124";
	
	public static final String TRAMITACAO_CONCLUIDA_COM_SUCESSO = "MSG-0125";

	public static final String TRAMITACAO_CRIADA_TELA_PEDIDO = "MSG-0126";

	public static final String TRAMITACAO_AGUARDANDO_PAGAMENTO = "MSG-0127";
	
	public static final String TRAMITACAO_PENDENTE_CONCLUSAO = "MSG-0156";

	public static final String TRAMITACAO_AUTOMATICA = "MSG-0157";
	
	public static final String TRAMITACAO_INCLUIDO_COM_SUCESSO = "MSG-0128";

	public static final String TRAMITACAO_ALTERADO_COM_SUCESSO = "MSG-0129";
	
	// Servi�o
	public static final String SERVICO_EM_USO = "MSG-0130";
	
	public static final String SERVICO_INCLUIDO_COM_SUCESSO = "MSG-0131";
	
	public static final String SERVICO_INEXISTENTE = "MSG-0132";
	
	public static final String SERVICO_ALTERADO_COM_SUCESSO = "MSG-0133";

	public static final String SERVICO_REMOVIDO_COM_SUCESSO = "MSG-0134";
	
	public static final String SERVICO_EXISTENTE = "MSG-0135";

	// Ve�culo
	public static final String VEICULO_EM_USO = "MSG-0140";
	
	public static final String VEICULO_INCLUIDO_COM_SUCESSO = "MSG-0141";
	
	public static final String VEICULO_INEXISTENTE = "MSG-0142";
	
	public static final String VEICULO_ALTERADO_COM_SUCESSO = "MSG-0143";
	
	public static final String VEICULO_REMOVIDO_COM_SUCESSO = "MSG-0144";
	
	public static final String VEICULO_EXISTENTE = "MSG-0145";
	
	public static final String DADOS_IMPORTADO_COM_SUCESSO = "MSG-0146";
	
	public static final String DADOS_INCOMPATIVEIS_COM_REGISTRO = "MSG-0147";

	// Arquivo
	public static final String ARQUIVO_EM_USO = "MSG-0150";
	
	public static final String ARQUIVO_INCLUIDO_COM_SUCESSO = "MSG-0151";
	
	public static final String ARQUIVO_INEXISTENTE = "MSG-0152";
	
	public static final String ARQUIVO_ALTERADO_COM_SUCESSO = "MSG-0153";
	
	public static final String ARQUIVO_REMOVIDO_COM_SUCESSO = "MSG-0154";
	
	public static final String ARQUIVO_EXISTENTE = "MSG-0155";

	// Para restrear mensagens n�o cadastradas.
	public static final String MENSAGEM_NAO_CADASTRADA = "MSG-XXXX";

	
}

