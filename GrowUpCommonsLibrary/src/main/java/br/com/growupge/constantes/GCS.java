
/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : J2EE 1.4
 * Criado em  : 13/11/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 13/11/2006 - In�cio de tudo, por J�lio Vitorino
 *
 */
package br.com.growupge.constantes;

import java.util.Arrays;
import java.util.List;

/**
 *
 * Classe de Constantes para o Gerenciador de Comandos
 *
 * @author J�lio Vitorino
 *
 */

public class GCS {

	// Constantes para identifica��o da classe dentro do mapa.
	public static final String MAPA_USUARIO = "Usuario";
	
	public static final String MAPA_SESSAO = "Sessao";
	
	public static final String MAPA_EMPRESA = "Empresa";

	public static final String MAPA_PERFIL = "Perfil";
	
	public static final String MAPA_PERMISSAO = "Permissao";

	public static final String MAPA_VARIAVEL = "Variavel";

	public static final String MAPA_MENSAGEM = "Mensagem";
	
	public static final String MAPA_CLIENTE = "Cliente";
	
	public static final String MAPA_PEDIDO_VEICULO = "PedidoVeiculo";
	
	public static final String MAPA_PAGAMENTO = "Pagamento";
	
	public static final String MAPA_MARCA = "Marca";
	
	public static final String MAPA_UF = "UnidadeFederativa";
	

	/* Constantes de vetores de permiss�es especiais para identifica��o de comandos 
	 * Estes vetores precisam estar em ordem alfab�tica */
	public static final List<String> METODOS_CONSULTA = Arrays.asList(  GCS.BUSCAR_TODAS,
																		GCS.BUSCAR_TOTAL,
																		GCS.PROCURAR );


	public static final List<String> METODOS_CONEXAO = Arrays.asList( 	GCS.CONECTAR,
																		GCS.CONECTAR_FACEBOOK,
																		GCS.PROCURAR_SESSAO	);
	
	public static final List<String> METODOS_SEM_SID = Arrays.asList(	GCS.BUSCAR_CLIENTES,
																		GCS.BUSCAR_EMPRESAS,
																		GCS.BUSCAR_MARCAS,
																		GCS.BUSCAR_VALOR_UNITARIO,
																		GCS.CONECTAR,
																		GCS.CONECTAR_FACEBOOK,
																		GCS.ADICIONAR_USUARIO,
																		GCS.DESCONECTAR,
																		GCS.PROCURAR_SESSAO,
																		GCS.RECRIAR_SENHA,
																		GCS.PROCURAR_EMPRESA_GROWUP_VEICULOS,
																		GCS.HABILITAR_USUARIO,
																		GCS.CONFIRMAR_PAGAMENTO,
																		GCS.EXPIRAR_PEDIDOS);

	/* Comandos gen�ricos para todos os usu�rios */
	public static final String CONECTAR = "CONECTAR";
	
	public static final String CONECTAR_FACEBOOK = "CONECTAR_FACEBOOK";

	public static final String DESCONECTAR = "DESCONECTAR";

	public static final String PROCURAR = "PROCURAR";

	public static final String PROCURAR_SESSAO = "PROCURAR_SESSAO";
	
	public static final String PROCURAR_EMPRESA_GROWUP_VEICULOS = "PROCURAR_GROWUP_VEICULOS";
	
	public static final String BUSCAR_TODAS = "BUSCAR_TODAS";
	
	public static final String BUSCAR_CLIENTES = "BUSCAR_CLIENTES";

	public static final String BUSCAR_EMPRESAS = "BUSCAR_EMPRESAS";
	
	public static final String BUSCAR_TOTAL = "BUSCAR_TOTAL";

	/* Comandos para usu�rio */
	public static final String ADICIONAR_USUARIO = "CRIAR_USUARIO";

	public static final String ATUALIZAR_USUARIO = "ALTERAR_USUARIO";

	public static final String EXCLUIR_USUARIO = "EXCLUIR_USUARIO";

	public static final String TROCAR_SENHA = "TROCAR_SENHA";

	public static final String RECRIAR_SENHA = "RECRIAR_SENHA";
	
	public static final String HABILITAR_USUARIO = "HABILITAR_USUARIO";

	/* Comandos para Empresa */
	public static final String ADICIONAR_EMPRESA = "CRIAR_EMPRESA";

	public static final String ATUALIZAR_EMPRESA = "ALTERAR_EMPRESA";

	public static final String EXCLUIR_EMPRESA = "EXCLUIR_EMPRESA";

	/* Comandos para Permissao */
	public static final String ADICIONAR_PERMISSAO = "CRIAR_PERMISSAO";

	public static final String EXCLUIR_PERMISSAO = "EXCLUIR_PERMISSAO";

	public static final String ATUALIZAR_PERMISSAO = "ALTERAR_PERMISSAO";

	/* Comandos para Perfil */
	public static final String ADICIONAR_PERMISSAO_PERFIL = "ADICIONAR_PERMISSAO_PERFIL";

	public static final String REMOVER_PERMISSAO_PERFIL = "REMOVER_PERMISSAO_PERFIL";

	public static final String ADICIONAR_PERFIL = "CRIAR_PERFIL";

	public static final String EXCLUIR_PERFIL = "EXCLUIR_PERFIL";

	public static final String ATUALIZAR_PERFIL = "ALTERAR_PERFIL";

	/* Comandos para Mensagem */

	public static final String ADICIONAR_MENSAGEM = "CRIAR_MENSAGEM";

	public static final String ATUALIZAR_MENSAGEM = "ALTERAR_MENSAGEM";

	public static final String EXCLUIR_MENSAGEM = "EXCLUIR_MENSAGEM";

	/* Comandos para Veiculo */
	
	public static final String CRIAR_VEICULO = "CRIAR_VEICULO";
	
	public static final String ATUALIZAR_VEICULO = "ALTERAR_VEICULO";
	
	public static final String EXCLUIR_VEICULO = "EXCLUIR_VEICULO";

	/* Comandos para Veiculo Proprietario */
	
	public static final String ADICIONAR_VEICULO_PROPRIETARIO = "ADICIONAR_VEICULO_PROPRIETARIO";
	
	public static final String ATUALIZAR_VEICULO_PROPRIETARIO = "ALTERAR_VEICULO_PROPRIETARIO";

	/* Comandos para Pedido */
	
	public static final String CONCLUIR_PEDIDO = "CONCLUIR_PEDIDO";
	
	public static final String CONCLUIR_PEDIDO_ENVIAR = "CONCLUIR_PEDIDO_ENVIAR";
	
	public static final String CRIAR_PEDIDO_VEICULO = "CRIAR_PEDIDO_VEICULO";
	
	public static final String ALTERAR_PEDIDO_VEICULO = "ALTERAR_PEDIDO_VEICULO";

	public static final String PROCURAR_PEDIDO = "PROCURAR_PEDIDO";
	
	public static final String BUSCAR_PAGAMENTOS_PEDIDOS = "BUSCAR_PAGAMENTOS_PEDIDOS";
	
	public static final String EXPIRAR_PEDIDOS = "EXPIRAR_PEDIDOS";

	/* Comandos para Pagamento */
	
	public static final String SOLICITAR_PAGAMENTO = "SOLICITAR_PAGAMENTO";
	
	public static final String CONFIRMAR_PAGAMENTO = "CONFIRMAR_PAGAMENTO";

	/* Comandos para Marca */
	public static final String BUSCAR_MARCAS = "BUSCAR_MARCAS";

	/* Comandos para Variavel */
	public static final String BUSCAR_VALOR_UNITARIO = "BUSCAR_VALOR_UNITARIO";

	
	

}
