/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : J2EE 1.4
 * Criado em  : 16/08/2005
 *
 * Historico de Modifica��o:
 * =========================
 * 16/08/2005 - In�cio de tudo, por J�lio Vitorino
 *
 */
package br.com.growupge.constantes;

import java.util.ResourceBundle;

/**
 * Classe de Constantes usadas no Sigem.
 *
 * @author Felipe Pina
 * @version 1.0
 */
public class Constantes {
	
	private static final ResourceBundle rb = ResourceBundle.getBundle(Constantes.class.getSimpleName());

	public static final String VARIAVEL_TEMPO_CRIACAO_SINGLETON = "TEMPO_SINGLETON_CERTIFICADO";

	public static final String VARIAVEL_TEMPO_AVISO_VALIDADE_SENHA = "TEMPO_AVISO_VALIDADE_SENHA";

	public static final String VARIAVEL_CHAVE_AES = "0ED069BC4D49B216C14C5B04D10365F7";

	public static final String VARIAVEL_NUM_DIAS_CODIGO_ACESSO = "NUM_DIAS_CODIGO_ACESSO";

	public static final String VARIAVEL_LINK_CODIGO_ACESSO = "LINK_CODIGO_ACESSO";
	
	public static final int VARIAVEL_TEMPO_VALIDADE_SENHA = 90;
	
	public static final int VARIAVEL_MAX_REGISTROS_RETORNADOS = 5;

	public static final String VARIAVEL_TMPLT_RESP_PEDIDO= "TemplateRespostaPedido.htm";
	
	public static final String VARIAVEL_TMPLT_NOTIFICAR_SENHA = "TemplateNotificarSenha.htm";
	
	public static final String VARIAVEL_TMPLT_CONFIRMAR_EMAIL = "TemplateConfirmacaoEmail.htm";
	
	public static final String VARIAVEL_TMPLT_USUARIO_HABILITADO = "TemplateUsuarioHabilitado.htm";
	
	public static final String VARIAVEL_TMPLT_COBRANCA_USUARIO_COMUM = "TemplateCobrancaUsuarioComum.htm";
	
	public static final String VARIAVEL_TMPLT_NOTIFICAR_ADMIN = "TemplateNotificarAdmin.htm";
	
	public static final String VARIAVEL_TMPLT_USUARIOS_EXPIRADOS = "TemplatePedidosExpirados.htm";
	
	public static final String VARIAVEL_TMPLT_NOTIFICAR_NOVO_PEDIDO = "TemplateNotificarNovoPedido.htm";
	
	public static final String VARIAVEL_TMPLT_CONTATO= "TemplateNotificarContato.htm";

//	public static final String META_INF_STARTUP_PATH = "../../META-INF/";
	
	// Tamanho do Oracle Error Code
	public static final int ORA_CODE = 5;
	
	// Constantes para defini��o de l�nguas para Datas (usado em getDateManagerInstance)
	public static final String LANGUAGE_BR = "BR";

	public static final String LANGUAGE_US = "US";

	public static final String LANGUAGE_ES = "ES";
	
	public static final String LANGUAGE_FR = "FR";

	public static final String LANGUAGE_OT = "OT";

	//Constante de que identificam chaves de status
	public static final String SIM = "S";

	public static final String NAO = "N";

	// Constantes para ambientes, identicas no ambiente de sigem4.dtd
	public static final String DEVELOPMENT_MODE = "DEVELOPMENT";

	public static final String PRODUCTION_MODE = "PRODUCTION";

	// Constantes para defini��o do atributo 'action' na fabrica BaseFactoryAuditing
	public static final int COMMAND_SUCESS = 1;

	public static final int COMMAND_FAILURE = 0;

	// Constantes para factory de security
	public static final int SECURITY_SHA = 0;

	public static final int SECURITY_MD5 = 1;

	public static final int SECURITY_AES = 2;

	// Tamanhos de Cadeia de Encripta��o
	public static final int CHAVE_TAMANHO_128_BITS = 128;

	public static final int CHAVE_TAMANHO_192_BITS = 192;

	public static final int CHAVE_TAMANHO_256_BITS = 256;

	public static final String P1 = "$1";

	public static final String P2 = "$2";

	public static final String P3 = "$3";

	// Idice inical da pagina��o
	public static final Integer DESCONSIDERAR_PAGINACAO = null;

	// --------- Converter pra enum
	
	// Status v�lidos para Servidor
	public static final String STATUS_SERVIDOR_ONLINE = "ON";
	public static final String STATUS_SERVIDOR_OFFLINE = "OF";
	public static final String STATUS_SERVIDOR_SO_LEITURA = "SL";
	public static final String STATUS_SERVIDOR_SEM_ESPACO = "SE";
	public static final String STATUS_SERVIDOR_OCUPADO = "OC";
	
	// Constantes para fabrica de Authenticator
	public static final int AUTHENTICATION_TYPE_SYSTEM = 0;
	public static final int AUTHENTICATION_TYPE_TOKEN = 1;
	public static final int AUTHENTICATION_TYPE_LDAP = 2;
	public static final String FORMA_AUTENTICACAO_SISTEMA = "SIST";
	public static final String FORMA_AUTENTICACAO_LDAP = "LDAP";
	
	//Url confirmacao email
//	public static String CONTEXT_JAVA = "/GrowUpWeb";
	public static final String PARAM_USUARIO = "usr";
	public static final String PARAM_EMAIL = "email";
	public static String URL_SITE = rb.getString("siteUrl");
	public static String URL_PEDIDO_USR = URL_SITE +  "/pedido.jsf?";

//	public static String URL_CONFIRMA_EMAIL = URL_SITE + CONTEXT_JAVA + "/habilitar?" + PARAM_USUARIO + "=";
	public static String URL_CONFIRMA_EMAIL = URL_SITE + "/habilitar?" ;
	
	//Database
	public static String ACTIVE_DAO_FACTORY = "FIREBIRD";
	public static String TRANSACTION_TIMEOUT = "30";
	public static final String ACTIVE_DATA_SOURCE = rb.getString("ActiveDataSource");
	public static final String TEMP_FOLDER = rb.getString("TempFolder");

	

	



}
