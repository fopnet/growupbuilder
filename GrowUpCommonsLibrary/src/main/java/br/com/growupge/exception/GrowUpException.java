/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 30/10/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 30/10/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.exception;

import java.util.Map;

import br.com.growupge.utility.StringUtil;

/**
 * @author elmt
 */
public class GrowUpException extends Exception {

	private static final long serialVersionUID = -3550328822891217714L;

	/**
	 * Atributo '<code>codigo</code>' do tipo String
	 */
	private String code;

	/**
	 * Atributo '<code>mensagem</code>' do tipo String
	 */
	private String mensagem;

	/**
	 * Atributo '<code>acao</code>' do tipo String
	 */
	private String acao;

	/**
	 * Atributo '<code>mensagemOriginal</code>' do tipo String
	 */
	private String mensagemOriginal;

	/**
	 * Atributo '<code>dto</code>' do tipo Object
	 */
	private Object dto;

	/**
	 * Atributo '<code>parametros</code>' do tipo Map<String,String>
	 */
	public Map<String, String> parametros = null;

	/**
	 * Construtor para esta classe.
	 * 
	 */
	public GrowUpException() {
		super();
	}

	/**
	 * Construtor para esta classe.
	 * 
	 * @param codigo
	 *            C�digo da exce��o.
	 */
	public GrowUpException(final String codigo) {
		this(codigo, "");
	}

	/**
	 * Construtor para esta classe.
	 * 
	 * @param codigo
	 *            C�digo da exce��o.
	 * @param mensagem
	 *            Mensagem que representa a exce��o.
	 */
	public GrowUpException(final String codigo, final String mensagem) {
		this(codigo, mensagem, "", "");

	}

	/**
	 * Construtor para esta classe.
	 * 
	 * @param codigo
	 *            C�digo da exce��o.
	 * @param mensagem
	 *            Mensagem que representa a exce��o.
	 * @param acao
	 *            A��o a ser realizada ap�s a exce��o.
	 */
	public GrowUpException(final String codigo, final String mensagem, final String acao) {
		this(codigo, mensagem, acao, "");
	}

	/**
	 * Construtor para esta classe.
	 * 
	 * @param code
	 *            C�digo da exce��o.
	 * @param description
	 *            Mensagem que representa a exce��o.
	 * @param acao
	 *            A��o a ser realizada ap�s a exce��o.
	 * @param mensagemOriginal
	 *            Mensagem original da exce��o.
	 */
	public GrowUpException(
			final String code,
			final String description,
			final String acao,
			final String mensagemOriginal) {
		super(description);
		this.setCode(code);
		this.setMensagem(description);
		this.setAcao(acao);
		this.setMensagemOriginal(mensagemOriginal);
	}

	/**
	 * Construtor para esta classe.
	 * 
	 * @param codigo
	 *            C�digo da exce��o.
	 * @param parametros
	 *            Caso o dto nao for padrao somente esta hash ser� utilizada
	 *            para mensagens.
	 */
	public GrowUpException(
			final String codigo,
			final Map<String, String> parametros) {
		super();
		this.setCode(codigo);
		this.setDto(dto);
		this.parametros = parametros;
	}

	/**
	 * Construtor para esta classe.
	 * 
	 * @param codigo
	 * @param dto
	 * @param msg
	 * @param parametros
	 */
	public GrowUpException(
			final String codigo,
			final Object dto,
			final String msg,
			final Map<String, String> parametros) {
		super(msg);
		this.setCode(codigo);
		this.setDto(dto);
		this.setMensagem(msg);
		this.parametros = parametros;
	}

	/**
	 * Construtor para esta classe.
	 *
	 * @param erroInterno
	 * @param ex
	 */
	public GrowUpException(final String erroInterno, final Throwable ex) {
		super(erroInterno, ex);
		IE ie = ErroInterno.getInstance().getErroInterno(erroInterno);
		this.setAcao(ie.acao);
		this.setMensagem(ie.mensagem);
		this.setMensagemOriginal(StringUtil.getStackTrace(ex));
	}

	// gettes and setters

	public final String getMensagemOriginal() {
		return mensagemOriginal;
	}

	public final void setMensagemOriginal(String mensagemOriginal) {
		this.mensagemOriginal = mensagemOriginal;
	}

	public final String getCode() {
		return code;
	}

	public final void setCode(String codigo) {
		this.code = codigo;
	}

	public final String getDescription() {
		return this.getMensagem();
	}

	public final void setDescription(String details) {
		this.setMensagem(details);
	}

	/**
	 * @return the mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * @param mensagem the mensagem to set
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * @return the acao
	 */
	public String getAcao() {
		return acao;
	}

	/**
	 * @param acao the acao to set
	 */
	public void setAcao(String acao) {
		this.acao = acao;
	}

	/**
	 * @return the dto
	 */
	public Object getDto() {
		return dto;
	}

	/**
	 * @param dto the dto to set
	 */
	public void setDto(Object dto) {
		this.dto = dto;
	}

	@Override
	public String toString() {
		return getMensagem();
	}
	
	

}
