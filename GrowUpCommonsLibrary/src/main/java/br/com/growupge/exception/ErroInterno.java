package br.com.growupge.exception;

import java.util.HashMap;
import java.util.Map;

public class ErroInterno {

	private static ErroInterno instance = null;

	private final Map<String, IE> erros = new HashMap<String, IE>();

	public static final String SOLUCAO_PADRAO = "Entre em contato com o suporte t�cnico.";

	public static final String ERRO_DESCONHECIDO = "IE-0000";

	public static final String COMUNICACAO_COMPONENTE = "IE-0001";

	public static final String SERVICO_LOCALIZACAO_DADOS = "IE-0002";

	public static final String GLOBAL_STARTUP = "IE-0003";

	public static final String REMOTE_SERVER = "IE-0004";

	public static final String USUARIO_SEM_SESSAO = "IE-0005";

	public static final String SIGEMBD_NAO_LOCALIZADO = "IE-0006";

	public static final String SIGEM_FACADE_ACESSO_REMOTO = "IE-0007";

	public static final String SIGEM_FACADE_CRIACAO = "IE-0008";

	public static final String SIGEMBD_ID_REMOTO = "IE-0009";

	public static final String METODO_CONECTAR_INDISPONIVEL = "IE-0010";

	public static final String METODO_DESCONECTAR_INDISPONIVEL = "IE-0011";

	public static final String METODO_EXPIRAR_SENHA_INDISPONIVEL = "IE-0012";

	public static final String METODO_MENSAGEM_INDISPONIVEL = "IE-0013";

	public static final String ALGORITMO_SHA1 = "IE-0014";

	public static final String ALGORITMO_AES = "IE-0022";

	public static final String ALGORITMO_MD5 = "IE-0023";

	public static final String METODO_EXECUTAR_COMANDO_INDISPONIVEL = "IE-0016";

	public static final String ERRO_TRANSACIONAL = "IE-0015";

	public static final String METODO_CONECTAR_CERTIFICADO_INDISPONIVEL = "IE-0016";

	public static final String CERTIFICADO_NAO_ENCONTRADO = "IE-0017";

	public static final String DESAFIO_INVALIDO = "IE-0018";

	public static final String METODO_GET_CHALLENGE_INDISPONIVEL = "IE-0019";

	public static final String MAPA_GERENCIADOR_COMANDOS = "IE-0020";

	public static final String METODO_NAO_ENCONTRADO = "IE-0021";

	public static final String OBJETO_NAO_RECONHECIDO = "IE-0022";

	public static final String ERRO_BANCO = "IE-0023";

	private ErroInterno() {
		/* Monta a lista de erros internos */

		erros.put(ErroInterno.ERRO_DESCONHECIDO, new IE(
				"Erro desconhecido.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.COMUNICACAO_COMPONENTE, new IE(
				"Erro de comunicac�o com componente.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.SERVICO_LOCALIZACAO_DADOS, new IE(
				"Erro no servi�o de localiza��o de dados.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.GLOBAL_STARTUP, new IE(
				"Erro ao inicializar servi�o GlobalStartup.",
				"Verifique arquivo de configura��o [startup.xml]"));
		erros.put(ErroInterno.REMOTE_SERVER, new IE(
				"Erro ao obter IP do computador remoto.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.USUARIO_SEM_SESSAO, new IE(
				"N�o existe sess�o ativa para o usu�rio.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.SIGEMBD_NAO_LOCALIZADO, new IE(
				"Servidor de Aplica��o n�o Inicializado ou servi�o JNDI imcompat�vel.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.SIGEM_FACADE_ACESSO_REMOTO, new IE(
				"Servi�o remoto do Sigem n�o pode ser acessado.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.SIGEM_FACADE_CRIACAO, new IE(
				"N�o foi poss�vel criar o servi�o remoto do Sigem.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.SIGEMBD_ID_REMOTO, new IE(
				"N�o foi poss�vel obter o ID remoto do servi�o Sigem.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.METODO_CONECTAR_INDISPONIVEL, new IE(
				"M�todo conectar() n�o est� dispon�vel no servi�o Sigem.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.METODO_DESCONECTAR_INDISPONIVEL, new IE(
				"M�todo desconectar() n�o est� dispon�vel no servi�o Sigem.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.METODO_EXPIRAR_SENHA_INDISPONIVEL, new IE(
				"M�todo validarExpirarSenha(String) n�o est� dispon�vel no servi�o Sigem.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.METODO_MENSAGEM_INDISPONIVEL, new IE(
				"M�todo getMensagem(String, String) n�o est� dispon�vel no servi�o Sigem.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.METODO_EXECUTAR_COMANDO_INDISPONIVEL, new IE(
				"M�todo executarComando(Object[]) n�o est� dispon�vel no servi�o Sigem.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.ALGORITMO_SHA1, new IE(
				"Erro na composi��o de Algoritmo SHA-1.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.ERRO_TRANSACIONAL, new IE(
				"Erro ao executar alguma transa��o.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.METODO_CONECTAR_CERTIFICADO_INDISPONIVEL, new IE(
				"M�todo conectarCertificado() n�o est� dispon�vel no servi�o Sigem.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.CERTIFICADO_NAO_ENCONTRADO, new IE(
				"O certificado do usu�rio n�o foi encontrado no desafio.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.DESAFIO_INVALIDO, new IE(
				"Desafio inv�lido.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.METODO_GET_CHALLENGE_INDISPONIVEL, new IE(
				"M�todo getChallenge() n�o est� dispon�vel no servi�o Sigem.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.MAPA_GERENCIADOR_COMANDOS, new IE(
				"Classe n�o encontrada no Mapa do Gerenciador de Comandos.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.METODO_NAO_ENCONTRADO, new IE(
				"M�todo a ser executado n�o encontrado na classe.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.OBJETO_NAO_RECONHECIDO, new IE(
				"Objeto passado como par�metro n�o reconhecido pela camada DAO.",
				ErroInterno.SOLUCAO_PADRAO));
		erros.put(ErroInterno.ERRO_BANCO, new IE(
				"Erro de banco de dados.",
				ErroInterno.SOLUCAO_PADRAO));

	}

	public static ErroInterno getInstance() {
		if (instance == null) {
			instance = new ErroInterno();
		}
		return instance;
	}

	public IE getErroInterno(String codigo) {
		return (IE) erros.get(codigo);
	}
}
