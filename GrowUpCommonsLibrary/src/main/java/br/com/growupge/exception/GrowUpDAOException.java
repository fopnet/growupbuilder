/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 30/10/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 30/10/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.exception;

import java.sql.SQLException;

/**
 * @author Felipe
 */
public class GrowUpDAOException extends GrowUpException {

	/**
	 * Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 7200282032333722481L;

	/**
	 * @param codigo C�digo do Erro.
	 * @param mensagem Mensagem de Erro.
	 */
	public GrowUpDAOException(final String codigo, final String mensagem) {
		super(codigo, mensagem);
	}

	/**
	 * @param codigo C�digo do Erro.
	 */
	public GrowUpDAOException(final String codigo) {
		super(codigo);
	}

	/**
	 * Construtor para esta classe.
	 *
	 * @param codigo
	 */
	public GrowUpDAOException(final SQLException ex) {
		super(ErroInterno.ERRO_BANCO, ex);
	}

	/**
	 * Construtor para esta classe.
	 *
	 * @param ge
	 */
	public GrowUpDAOException(final GrowUpException ge) {
		super(ErroInterno.ERRO_BANCO, ge.getMensagem(), ge.getAcao(), ge.getMensagem());
	}

}
