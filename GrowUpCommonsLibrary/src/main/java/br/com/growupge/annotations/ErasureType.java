/**
 * 
 */
package br.com.growupge.annotations;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Felipe
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ErasureType {
	Class<?>[] getErasuresTypes();
}