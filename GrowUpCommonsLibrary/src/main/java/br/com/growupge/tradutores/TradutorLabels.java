/**
 * 
 */
package br.com.growupge.tradutores;

/**
 * @author Felipe
 *
 */
public interface TradutorLabels {
	public static String LABEL_USUARIO = "USUARIO";
	public static String LABEL_NOME = "NOME";
	public static String LABEL_PLACA = "PLACA";

}
