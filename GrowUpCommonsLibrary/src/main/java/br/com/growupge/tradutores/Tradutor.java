package br.com.growupge.tradutores;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.global.GlobalStartup;

/**
 * Classe de tradu��o dos labels da interface
 *
 * @author elmt
 *
 */
public class Tradutor implements Serializable {

	private static final long serialVersionUID = -6436943911470606820L;

	/**
	 * Atributo '<code>DICIONARIO_BR</code>' do tipo int
	 */
	public static final int DICIONARIO_BR = 0;

	/**
	 * Atributo '<code>DICIONARIO_EN</code>' do tipo int
	 */
	public static final int DICIONARIO_EN = 1;

	/**
	 * Atributo '<code>DICIONARIO_ES</code>' do tipo int
	 */
	public static final int DICIONARIO_ES = 2;

	/**
	 * Atributo '<code>DICIONARIO_OT</code>' do tipo int
	 */
	public static final int DICIONARIO_OT = 3;

	/**
	 * Atributo '<code>h</code>' do tipo HashMap<String,Integer>
	 */
	private static Map<String, Integer> h = new HashMap<String, Integer>(); 
	{
		h.put("BR", DICIONARIO_BR);
		h.put("EN", DICIONARIO_EN);
		h.put("ES", DICIONARIO_ES);
		h.put("OT", DICIONARIO_OT);
	}

	/**
	 * Atributo '<code>properties</code>' do tipo Properties
	 */
	protected Properties properties = new Properties();

	/**
	 * Construtor para esta classe.
	 *
	 */
	public Tradutor() {
	}

	/**
	 * Construtor para esta classe.
	 *
	 * @param arquivoPropriedades
	 */
	public Tradutor(String arquivoPropriedades) throws Exception {
		try {
			// System.out.println("Lendo arquivo de propriedades em " + arquivoPropriedades);

			//properties.load(new FileInputStream(arquivoPropriedades));
			properties.load(GlobalStartup.getInstance().getResourceFile(arquivoPropriedades));
		} catch (IOException e) {
			
		}
	}

	/**
	 * Retorna uma hash com o conteudo de um arquivo de property.
	 *
	 * @param s
	 * @return Comentar aqui.
	 */
	public static Properties getInstance(String s) throws GrowUpException {

		GlobalStartup gs = null;
		Tradutor tradutor = null;

		try {
			gs = GlobalStartup.getInstance();


			int i = Integer.parseInt(h.get(s).toString());

			switch (i) {
			case DICIONARIO_BR:
				tradutor = new TradutorBR(gs.getDicionarioBR());
				break;
			case DICIONARIO_EN:
				tradutor = new TradutorEN(gs.getDicionarioEN());
				break;
			default:
				tradutor = new TradutorBR(gs.getDicionarioBR());
				break;
			}

		} catch (Exception e) {
			throw new GrowUpException(MSGCODE.GLOBAL_STARTUP_XML_FALHA, e);
		}

		return tradutor == null ? null : tradutor.properties;
	}
}
