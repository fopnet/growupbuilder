/**
 * 
 */
package br.com.growupge.dto;

import junit.framework.Assert;

import org.junit.Test;

/**
 * @author Felipe
 *
 */
public class PagamentoDTOTest {

	@Test
	public void testChavePagamento() {
		String url = "AP-6HY15119TE670472E";
		PagamentoDTO pag = new PagamentoDTO();
		pag.setCodigo(url);
		Assert.assertEquals("key nao corresponde", "AP-6HY15119TE670472E", pag.getCodigo()) ;
	}

	@Test
	public void testChavePagamentoMeio() {
		String url = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_ap-payment&paykey=AP-6HY15119TE670472E&outro=valor";
		PagamentoDTO pag = new PagamentoDTO();
		pag.setCodigo(url);
		Assert.assertEquals("key nao corresponde", "AP-6HY15119TE670472E", pag.getCodigo()) ;
	}

	@Test
	public void testChavePagamentoFinal() {
		String url = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_ap-payment&paykey=AP-6HY15119TE670472E";
		PagamentoDTO pag = new PagamentoDTO();
		pag.setCodigo(url);
		Assert.assertEquals("key nao corresponde", "AP-6HY15119TE670472E", pag.getCodigo()) ;
	}

}
