package br.com.growupge.dto;

import org.junit.Assert;
import org.junit.Test;

public class StatusPedidoTest {
	
	@Test
	public void testToString() {
		for (StatusPedido sts:StatusPedido.values()) {
			Assert.assertEquals("toString n�o esperado", sts.getTipo(), sts.toString());
		}
	}
	
	/**
	 * Caso exista a sobrescrita dos metodos no proprio enum, o teste passa a 
	 */
	@Test
	public void testClass() {
		for (StatusPedido sts:StatusPedido.values()) {
			Assert.assertEquals(".class n�o confere.", StatusPedido.class, sts.getClass());
		}
		Assert.assertNotSame("toString n�o esperado", StatusPedido.class, Enum.class);
	}
	

}
