package br.com.growupge.dto;

import junit.framework.Assert;

import org.junit.Test;

/**
 * @author Felipe
 *
 */
public class UsuarioDTOTest {

	@Test
	public void testGerarLoginComEmailNulo() {
		UsuarioDTO usr = new UsuarioDTO();
		usr.setEmail(null);
		Assert.assertNull("Login n�o esperado", usr.getCodigo());
	}

	@Test
	public void testGerarLoginDepoisSetEmail() {
		UsuarioDTO usr = new UsuarioDTO();
		usr.setEmail("fop.net@gmail.com");
		usr.setCodigo("fop.net");
		Assert.assertEquals("Login n�o esperado", "fop.net".toUpperCase(), usr.getCodigo());
	}
	
	@Test
	public void testIsLoginNaoValido() {
		UsuarioDTO usr = new UsuarioDTO();
		usr.setCodigo("fop");
		Assert.assertFalse("Login n�o esperado", usr.isLoginValido());
	}

	@Test
	public void testIsLoginValidoNulo() {
		UsuarioDTO usr = new UsuarioDTO();
		usr.setCodigo(null);
		Assert.assertFalse("Login n�o esperado", usr.isLoginValido());
	}

	@Test
	public void testIsLoginValido() {
		UsuarioDTO usr = new UsuarioDTO();
		usr.setCodigo("fop.net");
		Assert.assertTrue("Login n�o esperado", usr.isLoginValido());
	}

}
