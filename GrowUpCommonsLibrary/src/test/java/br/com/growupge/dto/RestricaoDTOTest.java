package br.com.growupge.dto;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.junit.Test;

public class RestricaoDTOTest extends TestCase {

	@Test
	public void testValorString() {
		RestricaoDTO r1 = new RestricaoDTO("String", "B");
		RestricaoDTO r2 = new RestricaoDTO("String", "C");
		RestricaoDTO r3 = new RestricaoDTO("String", "A");
		
		final List<RestricaoDTO> list = Arrays.asList(r1, r2, r3);
		Collections.sort(list);
		
		Assert.assertTrue("Ordem n�o corresponde", list.get(0).getValor().toString().equals("A") );
		Assert.assertTrue("Ordem n�o corresponde", list.get(1).getValor().toString().equals("B") );
		Assert.assertTrue("Ordem n�o corresponde", list.get(2).getValor().toString().equals("C") );
	}

	@Test
	public void testValorDouble() {
		RestricaoDTO r1 = new RestricaoDTO("String", 2.0);
		RestricaoDTO r2 = new RestricaoDTO("String", 3.0);
		RestricaoDTO r3 = new RestricaoDTO("String", 1.0);
		
		final List<RestricaoDTO> list = Arrays.asList(r1, r2, r3);
		Collections.sort(list);
		
		Assert.assertTrue("Ordem n�o corresponde", list.get(0).getValor().toString().equals("1.0") );
		Assert.assertTrue("Ordem n�o corresponde", list.get(1).getValor().toString().equals("2.0") );
		Assert.assertTrue("Ordem n�o corresponde", list.get(2).getValor().toString().equals("3.0") );
	}

	@Test
	public void testValorMix() {
		RestricaoDTO r1 = new RestricaoDTO("String", "B");
		RestricaoDTO r2 = new RestricaoDTO("String", 3.0);
		RestricaoDTO r3 = new RestricaoDTO("String", "A");
		RestricaoDTO r4 = new RestricaoDTO("String", 2.0);
		RestricaoDTO r5 = new RestricaoDTO("String", "C");
		RestricaoDTO r6 = new RestricaoDTO("String", 1.0);
		
		final List<RestricaoDTO> list = Arrays.asList(r1, r2, r3, r4, r5, r6);
		Collections.sort(list);
		
		Assert.assertTrue("Ordem n�o corresponde", list.get(0).getValor().toString().equals("A") );
		Assert.assertTrue("Ordem n�o corresponde", list.get(1).getValor().toString().equals("B") );
		Assert.assertTrue("Ordem n�o corresponde", list.get(2).getValor().toString().equals("C") );
		Assert.assertTrue("Ordem n�o corresponde", list.get(3).getValor().toString().equals("1.0") );
		Assert.assertTrue("Ordem n�o corresponde", list.get(4).getValor().toString().equals("2.0") );
		Assert.assertTrue("Ordem n�o corresponde", list.get(5).getValor().toString().equals("3.0") );
	}
}
