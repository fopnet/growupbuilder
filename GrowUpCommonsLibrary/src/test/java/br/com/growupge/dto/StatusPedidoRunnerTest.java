package br.com.growupge.dto;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;

@RunWith(Parameterized.class)
public class StatusPedidoRunnerTest {

	@Rule
	public ExpectedException expectedException =  ExpectedException.none();

	
	private String msg;
	private String msgCliente;

	private StatusPedido input;
	private StatusPedido expected;

	private StatusPedido expectedCliente;

	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
				//Origem 1- pendente
				{ StatusPedido.PENDENTE, StatusPedido.PENDENTE, null, StatusPedido.A_FATURAR, null }, 
				{ StatusPedido.PENDENTE, StatusPedido.CANCELADO, null, StatusPedido.A_FATURAR, null }, 
				{ StatusPedido.PENDENTE, StatusPedido.CONCLUIDO, null, StatusPedido.CONCLUIDO, null,  }, 
				{ StatusPedido.PENDENTE, StatusPedido.FATURADO, null, StatusPedido.FATURADO, null }, 
				{ StatusPedido.PENDENTE, StatusPedido.ENVIADO, "Pedido ainda encontra-se pendente de conclus�o.", StatusPedido.ENVIADO, "Pedido ainda encontra-se pendente de conclus�o." }, 
				{ StatusPedido.PENDENTE, StatusPedido.EXPIRADO, null, StatusPedido.EXPIRADO, null }, 
				//Origem 2- pendente cliente
				{ StatusPedido.A_FATURAR, StatusPedido.PENDENTE, null, StatusPedido.A_FATURAR, null }, 
				{ StatusPedido.A_FATURAR, StatusPedido.CANCELADO, "� obrigat�rio uma justificativa para cancelar um pedido pendente de pagamento.", StatusPedido.A_FATURAR, null }, 
				{ StatusPedido.A_FATURAR, StatusPedido.CONCLUIDO, null, StatusPedido.CONCLUIDO, null,  }, 
				{ StatusPedido.A_FATURAR, StatusPedido.FATURADO, null, StatusPedido.FATURADO, null }, 
				{ StatusPedido.A_FATURAR, StatusPedido.ENVIADO, "Pedido ainda encontra-se pendente de conclus�o.", StatusPedido.ENVIADO, "Pedido ainda encontra-se pendente de conclus�o." }, 
				{ StatusPedido.A_FATURAR, StatusPedido.EXPIRADO, null, StatusPedido.EXPIRADO, null }, 
				//Origem 3- cancelado
				{ StatusPedido.CANCELADO, StatusPedido.PENDENTE, "Pedido n�o pode voltar a estar pendente depois do estado CANCELADO", StatusPedido.A_FATURAR, "Pedido n�o pode voltar a estar pendente depois do estado CANCELADO" }, 
				{ StatusPedido.CANCELADO, StatusPedido.CANCELADO, "N�o � poss�vel cancelar um pedido j� cancelado.", StatusPedido.A_FATURAR, "Pedido n�o pode voltar a estar pendente depois do estado CANCELADO" }, 
				{ StatusPedido.CANCELADO, StatusPedido.CONCLUIDO, "N�o � poss�vel concluir um pedido j� cancelado.", StatusPedido.CONCLUIDO,  "N�o � poss�vel concluir um pedido j� cancelado." }, 
				{ StatusPedido.CANCELADO, StatusPedido.FATURADO, "N�o � poss�vel pagar um pedido j� cancelado.", StatusPedido.FATURADO, "N�o � poss�vel pagar um pedido j� cancelado." }, 
				{ StatusPedido.CANCELADO, StatusPedido.ENVIADO, "N�o � poss�vel enviar um pedido j� cancelado.", StatusPedido.ENVIADO, "N�o � poss�vel enviar um pedido j� cancelado." }, 
				{ StatusPedido.CANCELADO, StatusPedido.EXPIRADO, "N�o � poss�vel expirar um pedido j� cancelado.", StatusPedido.EXPIRADO, "N�o � poss�vel expirar um pedido j� cancelado." }, 
				//Origem 4- concluido
				{ StatusPedido.CONCLUIDO, StatusPedido.PENDENTE, "Pedido n�o pode voltar a estar pendente depois do estado CONCLUIDO", StatusPedido.A_FATURAR, "Pedido n�o pode voltar a estar pendente depois do estado CONCLUIDO" }, 
				{ StatusPedido.CONCLUIDO, StatusPedido.CANCELADO, "Pedido conclu�do n�o pode ser cancelado.", StatusPedido.A_FATURAR, "Pedido n�o pode voltar a estar pendente depois do estado CONCLUIDO" }, 
				{ StatusPedido.CONCLUIDO, StatusPedido.CONCLUIDO, null, StatusPedido.CONCLUIDO,  null }, 
				{ StatusPedido.CONCLUIDO, StatusPedido.FATURADO, null, StatusPedido.FATURADO, null }, 
				{ StatusPedido.CONCLUIDO, StatusPedido.ENVIADO, null, StatusPedido.ENVIADO, null }, 
				{ StatusPedido.CONCLUIDO, StatusPedido.EXPIRADO, "N�o � poss�vel expirar um pedido j� conclu�do.", StatusPedido.EXPIRADO, "N�o � poss�vel expirar um pedido j� conclu�do." }, 
				//Origem 5- Faturado
				{ StatusPedido.FATURADO, StatusPedido.PENDENTE, "Pedido n�o pode voltar a estar pendente depois do estado FATURADO", StatusPedido.A_FATURAR, "Pedido n�o pode voltar a estar pendente depois do estado FATURADO" }, 
				{ StatusPedido.FATURADO, StatusPedido.CANCELADO, "N�o � poss�vel cancelar um pedido j� faturado.", StatusPedido.A_FATURAR, "Pedido n�o pode voltar a estar pendente depois do estado FATURADO" }, 
				{ StatusPedido.FATURADO, StatusPedido.CONCLUIDO, null, StatusPedido.CONCLUIDO,  null }, 
				{ StatusPedido.FATURADO, StatusPedido.FATURADO, "O pedido j� est� pago.", StatusPedido.FATURADO, "O pedido j� est� pago." }, 
				{ StatusPedido.FATURADO, StatusPedido.ENVIADO, null, StatusPedido.ENVIADO, null }, 
				{ StatusPedido.FATURADO, StatusPedido.EXPIRADO, "N�o � poss�vel expirar um pedido faturado.", StatusPedido.EXPIRADO, "N�o � poss�vel expirar um pedido faturado." }, 
				//Origem 6- enviado
				{ StatusPedido.ENVIADO, StatusPedido.PENDENTE, "Pedido n�o pode voltar a estar pendente depois do estado ENVIADO", StatusPedido.A_FATURAR, "Pedido n�o pode voltar a estar pendente depois do estado ENVIADO" }, 
				{ StatusPedido.ENVIADO, StatusPedido.CANCELADO, "N�o � poss�vel cancelar um pedido j� enviado ao cliente.", StatusPedido.A_FATURAR, "Pedido n�o pode voltar a estar pendente depois do estado ENVIADO" }, 
				{ StatusPedido.ENVIADO, StatusPedido.CONCLUIDO, "N�o � poss�vel concluir um pedido j� enviado ao cliente.", StatusPedido.CONCLUIDO,  "N�o � poss�vel concluir um pedido j� enviado ao cliente." }, 
				{ StatusPedido.ENVIADO, StatusPedido.FATURADO, "N�o � poss�vel pagar um pedido j� enviado ao cliente.", StatusPedido.FATURADO, "N�o � poss�vel pagar um pedido j� enviado ao cliente." }, 
				{ StatusPedido.ENVIADO, StatusPedido.ENVIADO, null, StatusPedido.ENVIADO, null }, 
				{ StatusPedido.ENVIADO, StatusPedido.EXPIRADO, "N�o � poss�vel expirar um pedido j� enviado.", StatusPedido.EXPIRADO, "N�o � poss�vel expirar um pedido j� enviado." }, 
				//Origem 7- enviado
				{ StatusPedido.EXPIRADO, StatusPedido.PENDENTE, "Pedido n�o pode voltar a estar pendente depois do estado EXPIRADO", StatusPedido.A_FATURAR, "Pedido n�o pode voltar a estar pendente depois do estado EXPIRADO" }, 
				{ StatusPedido.EXPIRADO, StatusPedido.CANCELADO, "N�o � poss�vel cancelar um pedido expirado.", StatusPedido.A_FATURAR, "Pedido n�o pode voltar a estar pendente depois do estado EXPIRADO" }, 
				{ StatusPedido.EXPIRADO, StatusPedido.CONCLUIDO, "N�o � poss�vel concluir um pedido expirado.", StatusPedido.CONCLUIDO,  "N�o � poss�vel concluir um pedido expirado." }, 
				{ StatusPedido.EXPIRADO, StatusPedido.FATURADO, "N�o � poss�vel pagar um pedido expirado.", StatusPedido.FATURADO, "N�o � poss�vel pagar um pedido expirado." }, 
				{ StatusPedido.EXPIRADO, StatusPedido.ENVIADO, "N�o � poss�vel enviar um pedido expirado.", StatusPedido.ENVIADO, "N�o � poss�vel enviar um pedido expirado." }, 
				{ StatusPedido.EXPIRADO, StatusPedido.EXPIRADO, "N�o � poss�vel expirar um pedido j� expirado.", StatusPedido.EXPIRADO, "N�o � poss�vel expirar um pedido j� expirado." }, 
		});
	}
	

	public StatusPedidoRunnerTest(StatusPedido input, StatusPedido expected, String msg, StatusPedido expectedCliente, String msgCliente) {
		this.input = input;
		this.expected= expected;
		this.msg = msg;
		this.expectedCliente = expectedCliente == null ? expected : expectedCliente;
		this.msgCliente = msgCliente;
	}

	@Test
	public void testPedidoSemCliente() throws GrowUpException {
		PedidoVeiculoDTO pedido = new PedidoVeiculoDTO();
			
		testar(pedido, input, expected, false);

	}

	@Test 
	public void testPedidoComCliente() throws GrowUpException {
		PedidoVeiculoDTO pedido = new PedidoVeiculoDTO();
		pedido.setCliente(new ClienteDTO());
		pedido.getCliente().setCodigo(1L);
		
		testar(pedido, input, expectedCliente, true);
	}


	protected void testar(PedidoVeiculoDTO pedido, StatusPedido input, StatusPedido output, boolean isPedidoComCliente) throws GrowUpException {
		
		try {
			trocarEstado(pedido, input);
		} catch (Exception e) {
//			Reflection.setFieldValue(pedido, new Parameter("status", input));
			try {
				// Ao troca o input, em alguns estado n�o pode sair do pendente para o estado descrito
				FieldUtils.writeField(pedido, "status", input, true);
			} catch (IllegalAccessException e1) {
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e1);
			}
		}
		
		String msgResultante = isPedidoComCliente? msgCliente : msg;
		
		if ( isNotBlank(msgResultante) ) {
			expectedException.expect(GrowUpException.class);
			expectedException.expectMessage(msgResultante);
		}
		
		trocarEstado(pedido, output);
		
		
		if ( isBlank(msg) ) {
			if (isPedidoComCliente)
				Assert.assertEquals("Estado inesperado do pedido", expectedCliente, pedido.getStatus());
			else
				Assert.assertEquals("Estado inesperado do pedido", expected, pedido.getStatus());
		}
		
	}


	protected void trocarEstado(PedidoVeiculoDTO pedido, StatusPedido sts) throws GrowUpException {
		switch (sts) {
		case CANCELADO:
			pedido.getStatus().cancelar(pedido);
			break;
		case ENVIADO:
			pedido.getStatus().enviar(pedido);
			break;
		case PENDENTE:
			pedido.getStatus().pendente(pedido);
			break;
		case A_FATURAR:
			pedido.getStatus().pendente(pedido);
			break;
		case CONCLUIDO:
			pedido.getStatus().concluir(pedido);
			break;
		case EXPIRADO:
			pedido.getStatus().expirar(pedido);
			break;
		case FATURADO:
			pedido.getStatus().pagar(pedido);
			break;
		}
	}

}
