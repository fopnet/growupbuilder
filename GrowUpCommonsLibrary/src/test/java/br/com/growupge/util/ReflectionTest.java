package br.com.growupge.util;


import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.Test;

import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.dto.StatusPedido;
import br.com.growupge.dto.VeiculoDTO;
import br.com.growupge.utility.Reflection;

/**
 * @author Felipe
 *
 */
public class ReflectionTest {

	@Test
	public void testPropertyName() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		VeiculoDTO v = new VeiculoDTO("placa");
		final Object property = PropertyUtils.getProperty(v, "codigo");
		Assert.assertEquals("Codigo nao esperado", "PLACA" , property);
	}

	@Test
	public void testSetStatus() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		PedidoVeiculoDTO v = new PedidoVeiculoDTO(1L); 
		final Object property = Reflection.getMethod(v.getClass(), "setStatus", StatusPedido.class);
		Assert.assertEquals("Codigo nao esperado", "PLACA" , property);
	}

	@Test
	public void testFieldsName() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		List<String> permitidos = new ArrayList<>(); 
		List<Field> fields = FieldUtils.getAllFieldsList( StatusPedido.class );
		Iterator<Field> it = fields.iterator();
		while (it.hasNext()) {
			Field fld = it.next();
			if (fld.isEnumConstant() )
				it.remove();
			else if (Modifier.isStatic( fld.getModifiers() ) )
				it.remove();
			else  if (Arrays.asList("ENUM$VALUES", "ordinal").contains( fld.getName() )) 
				it.remove();
			else 
				permitidos.add(fld.getName());
		}
		
		
		Assert.assertEquals("fields do enum nao confere", "[tipo, descricao, name]", permitidos.toString());

	}

	@Test
	public void testEnumReflection() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		StatusPedido sts = StatusPedido.CANCELADO;
		
		Assert.assertEquals("Descri��o do status n�o esperada", "Cancelado", PropertyUtils.getSimpleProperty(sts, "descricao") ); 
	}
}
