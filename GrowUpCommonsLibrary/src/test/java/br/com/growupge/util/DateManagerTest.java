package br.com.growupge.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.junit.Test;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.utility.DateUtil;

/**
 * @author EII5
 *
 */
public class DateManagerTest {

	@Test
	public void testDateFormat() throws ParseException {
		DateUtil dm = DateUtil.getDateManagerInstance();
		Date expected = dm.getDate(3, 4, 2012, 02, 33, 35, 774);//Esta � a data do paypal convertida para o timezone local
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSSXXX");
		Date dt = df.parse("2012-04-02T22:33:35.774-07:00");
		
		Assert.assertEquals("Data n�o confere", expected , dt);
	}
	
	@Test
	public void testDateFormatPDT() throws ParseException {
		DateUtil dm = DateUtil.getDateInstance(Constantes.LANGUAGE_US);
		Date expected = dm.getDate(3, 4, 2012, 06, 33, 35, 0);//Esta � a data do paypal convertida para o timezone local
		
		DateFormat df = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
		Date dt = df.parse("Tue Apr 03 02:33:35 PDT 2012");
		
		Assert.assertEquals("Data n�o confere", expected , dt);
	}

	@Test
	public void testDateFormatUsandoDateManagerPayPal() throws ParseException {
		DateUtil dm = DateUtil.getDateManagerInstancePayPal();//usando o timezone sao-paulo UTC -3:00
		Date expected = dm.getDate(3, 4, 2012, 06, 33, 35, 0);//Esta � a data do paypal convertida para o timezone local
		
		DateFormat df = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
//		Date dt = df.parse("2012-04-02T22:33:35.774-07:00");
		Date dt = df.parse("Tue Apr 03 02:33:35 PDT 2012");
		
		Assert.assertEquals("Data n�o confere", expected , dt);
	}
	
	@Test
	public void testDateFormatPayPalParse() throws ParseException {
		DateUtil dm = DateUtil.getDateManagerInstancePayPal();//usando o timezone RiodeJaneiro UTC -3:00
		Date expected = dm.getDate("2012-04-02T22:33:35.774-07:00");
//		Date expected = dm.getDate("Tue Apr 03 02:33:35 PDT 2012");
		
		DateFormat df = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
//		Date dt = df.parse("Tue Apr 03 02:33:35 PDT 2012");
//		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		Date dt = df.parse("2012-04-03T02:33:35.774");
		
		Assert.assertEquals("Data n�o confere", expected , dt);
	}

	@Test
	public void testDiffHour() throws ParseException {
		DateUtil dm = DateUtil.getDateManagerInstance();//usando o timezone RiodeJaneiro UTC -3:00
		dm.getDate(02, 12, 1979, 04, 00, 00);

		DateUtil dm2 = DateUtil.getDateManagerInstance();
		Date menor = dm2.getDate(02, 12, 1979, 01, 00, 00);

		Assert.assertEquals("Data n�o confere", 3 , dm.diffHour(menor));
	}

	@Test
	public void testAddDays() throws ParseException {
		Logger logger = Logger.getLogger(DateManagerTest.class);
		
		DateUtil dm = DateUtil.getDateManagerInstancePayPal();//usando o timezone RiodeJaneiro UTC -3:00
		Date actual = dm.getDate("2012-04-02T22:33:35.000-07:00");
		dm.addDays(2);
		actual = dm.getDateTime();
				
//		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.US);
//		Date expected = df.parse("2012-04-04T22:33:35.000-07:00");
		Date expected = dm.getDate("2012-04-04T22:33:35.000-07:00");

		logger.info("DateFormat: " + dm.getDateFormat());
		Assert.assertEquals("Data n�o confere", expected, actual);
		Assert.assertEquals("Data n�o confere", dm.getDateFormat(), "2012-04-04T22:33:35.000-07:00");
	}
	

	@Test
	public void testDateTime() throws ParseException {
		DateUtil dm = DateUtil.getDateManagerInstance();
		System.out.println(dm.getDateTime());
		
	}

}
