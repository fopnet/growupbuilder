br.com.growupge:GrowUpCommonsLibrary:jar:2.4.9-mail-pattern
+- commons-beanutils:commons-beanutils:jar:1.8.3:compile
|  \- commons-logging:commons-logging:jar:1.1.1:compile
+- log4j:log4j:jar:1.2.17:compile
+- junit:junit:jar:4.8.1:test
+- org.apache.commons:commons-lang3:jar:3.3.2:compile
+- org.apache.commons:commons-collections4:jar:4.0:compile
+- javax.mail:mail:jar:1.4:provided
|  \- javax.activation:activation:jar:1.1:provided
+- org.apache.geronimo.specs:geronimo-atinject_1.0_spec:jar:1.0:compile
+- org.apache.geronimo.specs:geronimo-jcdi_1.1_spec:jar:1.0:compile
+- org.apache.geronimo.specs:geronimo-interceptor_1.2_spec:jar:1.0:compile
+- org.apache.geronimo.specs:geronimo-annotation_1.2_spec:jar:1.0:compile
+- org.apache.geronimo.specs:geronimo-jaxws_2.1_spec:jar:1.0:compile
|  \- org.apache.geronimo.specs:geronimo-activation_1.1_spec:jar:1.0.2:compile
+- dom4j:dom4j:jar:1.6.1:compile
|  \- xml-apis:xml-apis:jar:1.0.b2:compile
+- org.apache.geronimo.specs:geronimo-ejb_3.1_spec:jar:1.0.2:compile
+- org.apache.geronimo.specs:geronimo-validation_1.0_spec:jar:1.1:compile
+- org.apache.openwebbeans:openwebbeans-impl:jar:1.6.2:compile
+- org.apache.openwebbeans:openwebbeans-web:jar:1.6.2:compile
+- org.apache.openwebbeans:openwebbeans-spi:jar:1.6.2:compile
+- org.apache.openwebbeans:openwebbeans-el22:jar:1.6.2:compile
+- org.apache.openwebbeans:openwebbeans-tomcat7:jar:1.6.2:compile
+- org.apache.xbean:xbean-finder-shaded:jar:4.5:compile
+- org.apache.xbean:xbean-asm5-shaded:jar:4.5:compile
+- org.apache.geronimo.bundles:scannotation:jar:1.0.2_1:compile
+- javax.enterprise:cdi-api:jar:1.2:compile
|  +- javax.el:javax.el-api:jar:3.0.0:compile
|  +- javax.interceptor:javax.interceptor-api:jar:1.2:compile
|  \- javax.inject:javax.inject:jar:1:compile
\- org.firebirdsql.jdbc:jaybird-jdk17:jar:2.2.5:provided
   +- javax.resource:connector-api:jar:1.5:provided
   \- org.antlr:antlr-runtime:jar:3.4:provided
      +- org.antlr:stringtemplate:jar:3.2.1:provided
      \- antlr:antlr:jar:2.7.7:provided
