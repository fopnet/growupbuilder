package com.engine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


import org.junit.Assert;
import org.junit.Test;

import com.engine.model.VeiculoDTO;


public class SearchTest {

	private Search<VeiculoDTO> getSearch() {
		final List<String> database = Arrays.asList("Zafira 2.0 Flex", "Zafira 2.0 Gasolina", "Zafira CD 2.0 autom�tica", "Zafira CD 1.6 manual");
		
		Search<VeiculoDTO> search = new Search<VeiculoDTO>();
		
		search.addTargetListener(new TargetListener<MatcherWrapper<VeiculoDTO>>() {
			
			@Override
			public List<MatcherWrapper<VeiculoDTO>> search(List<String> hits) {
				List<MatcherWrapper<VeiculoDTO>> resultado = new ArrayList<MatcherWrapper<VeiculoDTO>>();
				
				for (String hit : database) {
					resultado.add(new MatcherWrapper<VeiculoDTO>("modelo", new VeiculoDTO(hit)));
				}
				return resultado;
			}
			
		});
		return search;
	}
	
	@Test
	public void test() {
		Search<VeiculoDTO> search = getSearch();
		
		List<VeiculoDTO> hits = search.search("ZAFIRA CD");
		
		Collections.sort(hits);
		Assert.assertEquals("Tamanho de hits n�o esperado", 2, hits.size());
		Assert.assertEquals("Tamanho de hits n�o esperado", new VeiculoDTO("Zafira CD 1.6 manual"), hits.get(0));
		Assert.assertEquals("Tamanho de hits n�o esperado", new VeiculoDTO("Zafira CD 2.0 autom�tica"), hits.get(1));
		
		
	}

	@Test
	public void test2() {
		Search<VeiculoDTO> search = getSearch();
		
		List<VeiculoDTO> hits = search.search("ZAFIRA CD 2.0");
		
		Collections.sort(hits);
		Assert.assertEquals("Tamanho de hits n�o esperado", 1, hits.size());
		Assert.assertEquals("Tamanho de hits n�o esperado", new VeiculoDTO("Zafira CD 2.0 autom�tica"), hits.iterator().next());
		
		
	}


}
