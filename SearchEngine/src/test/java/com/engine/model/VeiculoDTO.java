package com.engine.model;

public class VeiculoDTO implements Comparable<VeiculoDTO> {

	private String modelo;

	public VeiculoDTO(String value) {
		this.modelo = value;
	}
	
	public String getModelo() {
		return modelo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((modelo == null) ? 0 : modelo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VeiculoDTO other = (VeiculoDTO) obj;
		if (modelo == null) {
			if (other.modelo != null)
				return false;
		} else if (!modelo.equals(other.modelo))
			return false;
		return true;
	}

	@Override
	public int compareTo(VeiculoDTO o) {
		return modelo.compareTo(o.getModelo());
	}
	
	public String toString() {
		return modelo;
	}
	

}
