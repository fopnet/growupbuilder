package com.engine;

import java.util.List;

public interface RelevanceWordsTransformer {

	public List<String> transform(String text);
}
