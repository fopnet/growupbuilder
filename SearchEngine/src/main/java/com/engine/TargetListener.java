package com.engine;

import java.util.List;

public interface TargetListener<T> {

	public List<T> search(List<String> hits) ;
}
