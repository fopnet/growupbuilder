package com.engine;

import static org.apache.commons.lang3.StringUtils.isNoneBlank;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

public class MatcherWrapper<T> implements Comparable<MatcherWrapper<T>> {

	private List<String> matches;
	private T targetObject;
	private String fieldname;
	
	public MatcherWrapper(final String fieldname, final T obj) {
		this.fieldname = fieldname;
		this.targetObject = obj;
		matches = new ArrayList<>();
	}
	
	public Integer getHits() {
		return matches.size();
	}
	
	public void match(final String hit) {
		String sourceHit = StringUtils.stripAccents(hit).toLowerCase();
		String targetHit = StringUtils.stripAccents(getTargetHit()).toLowerCase();
		
		// Verificar quem faz fronteira com os hits encontrados
		boolean isMatch = Pattern.compile("\\b".concat(sourceHit).concat("\\b"))
												.matcher(targetHit)
												.find();
		if (isMatch)
			matches.add(hit);
	}

	/**
	 * @return
	 */
	private String getTargetHit() {
		String targetHit = "";

		if (isNoneBlank(fieldname)) {
			Object result = null;
			try {
				result = FieldUtils.readField(targetObject, fieldname, true);
//				result = PropertyUtils.getSimpleProperty(targetObject, fieldname);
			} catch (IllegalAccessException e) {
				return targetHit;
			}
			
			if (result != null)
				targetHit = result.toString();
		} else 
			targetHit = targetObject.toString();
		
		return targetHit;
	}

	@Override
	public int compareTo(MatcherWrapper<T> o) {
		return o.getHits().compareTo(getHits()) ;
	}

	public T getTarget() {
		return targetObject;
	}
}
