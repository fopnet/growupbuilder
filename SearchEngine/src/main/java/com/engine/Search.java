package com.engine;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.engine.model.ModeloVeiculoRelevanceWords;





public class Search<T> {

	private List<TargetListener<MatcherWrapper<T>>> listeners; 
	private RelevanceWordsTransformer defaultTransformer;
	
	public Search() {
		listeners= new ArrayList<TargetListener<MatcherWrapper<T>>>();
		defaultTransformer = new ModeloVeiculoRelevanceWords();
	}

	/**
	 * Adiciona uma implementação da interface para criação dos matcher
	 * @param targetListener
	 */
	public void addTargetListener(TargetListener<MatcherWrapper<T>> targetListener) {
		listeners.add(targetListener);
	}
	
	/**
	 * Realiza a busca dos objetos que mais se assemelham de acordo com o texto de entrada.
	 * @param text
	 * @return
	 */
	final public List<T> search(String text) {
		return search(text, defaultTransformer);
	}

		/**
	 * Realiza a busca dos objetos que mais se assemelham de acordo com o texto de entrada.
	 * @param text
	 * @return
	 */
	final public List<T> search(final String text, final RelevanceWordsTransformer transformer) {
		
		if (text == null) 
			return Collections.emptyList();
		
		// Palavras relevantes para busca no banco as melhores amostras possíveis
		final List<String> revelanceSources = transformer.transform(text);
		final List<MatcherWrapper<T>> targets = new ArrayList<>();
		for (TargetListener<MatcherWrapper<T>> listener: listeners) {
			targets.addAll( listener.search(revelanceSources) );
		}
		
		final List<String> sources = Arrays.asList(text.replaceAll("\\/+", "").split("\\s"));
		final List<MatcherWrapper<T>> matches = new ArrayList<>();
		for (MatcherWrapper<T> matcher : targets) {
			for (String hit : sources) {
				 matcher.match(hit);
			}
			matches.add(matcher);
		}

		return getMatcherTopHits(matches);
	}

	/**
	 * Retorna os matches contendo os maiores hits da lista
	 * @param matches
	 * @return
	 */
	private List<T> getMatcherTopHits(final List<MatcherWrapper<T>> matches) {
		Collections.sort(matches);
		
		Integer higher = 0;
		List<T> result = new ArrayList<>();
		
		MatcherWrapper<T> matcher = null;
		for (Iterator<MatcherWrapper<T>> iterator = matches.iterator(); 
				iterator.hasNext() && (matcher = iterator.next()).getHits() >= higher;) {
			result.add(matcher.getTarget());
			higher = matcher.getHits();  
		}
		
		return result;
	}

	

}
