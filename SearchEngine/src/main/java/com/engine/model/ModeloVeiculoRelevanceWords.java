package com.engine.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.engine.RelevanceWordsTransformer;



public class ModeloVeiculoRelevanceWords implements RelevanceWordsTransformer {

	@Override
	public List<String> transform(String text) {
		String[] split = text.replaceAll("(\\d+(c|C)(v|V))|(\\d\\.\\d)|(\\d{1,2}(v|V))", " ")
							.replaceAll("\\/", " ")
							.replaceAll("(?mi)hatch", " ")
							.replaceAll("(?mi)sedan", " ")
							.replaceAll("(?mi)wagon", " ")
							.split("\\s");
		
		List<String> list = Arrays.asList(split);
		if (list.isEmpty())
			return Collections.emptyList();
		
		if (list.size() > 1) {
			return list.subList(0, 2); // retorna o primeiro + o segundo.
		} else 
			return list.subList(0, 1);// retorna o primeiro item
		
	}

}
