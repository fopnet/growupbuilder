/*� necess�rio a cria��o da tabela ve�culos com todos os dados pertinentes*/

/*Normaliza��o Inserts*/

/*Inserindo na tabela de Tipo*/
-- select * from tipo
INSERT INTO tipo (tipo_cd_tipo, tipo_nm_tipo)
SELECT (select GEN_ID(GEN_TIPO_CD_TIPO,1) FROM RDB$DATABASE), vf.tipo
FROM veiculo_fipe vf
GROUP BY tipo
ORDER BY tipo;


/*Inserindo na tabela de Marca*/
INSERT INTO marca_fipe ( marc_cd_marca,
                        marc_nm_marca)
SELECT (select GEN_ID(GEN_MARC_CD_MARCA,1) FROM RDB$DATABASE),
        veiculo_fipe.marca
FROM  veiculo_fipe
WHERE veiculo_fipe.marca != ''
GROUP BY veiculo_fipe.marca
ORDER BY veiculo_fipe.marca ;


/*Inserindo na tabela de Modelos*/
INSERT INTO modelo_fipe
(mode_cd_modelo, mode_nm_modelo, mode_cd_fipe, TIPO_cd_tipo, marc_cd_marca )
SELECT  (select GEN_ID(GEN_MODE_CD_MODELO,1) FROM RDB$DATABASE),
        v.modelo,
        v.cod_fipe,
        tipo.tipo_cd_tipo,
        ma.marc_cd_marca
FROM veiculo_fipe v INNER JOIN marca_fipe ma  ON (v.marca = ma.marc_nm_marca)
                    inner join tipo on tipo.tipo_nm_tipo = v.tipo
group by v.modelo,v.cod_fipe, tipo.tipo_cd_tipo, ma.marc_cd_marca
ORDER BY v.modelo;

 
/*Inserindo na tabela de ano*/
INSERT INTO ano_fipe (ANO_CD_ANO, ANO_NR_ANO, MODE_CD_MODELO,ANO_NM_COMBUSTIVEL, ANO_VL_PRECO )
select (select GEN_ID(GEN_ANO_CD_ANO,1) FROM RDB$DATABASE),
       v.ano,
       modelo.mode_cd_modelo,
       v.combustivel,
       v.preco
from veiculo_fipe v
    inner join modelo_fipe modelo on modelo.mode_nm_modelo = v.modelo
group by v.ano,
       modelo.mode_cd_modelo,
       v.combustivel,
       v.preco 
order by modelo.mode_cd_modelo
