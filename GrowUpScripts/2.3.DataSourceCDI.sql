--------------------------------------------------------------------------------
-- Script executado em 19-08-2014
--------------------------------------------------------------------------------
ALTER TABLE VEICULO DROP CONSTRAINT FK_VEIC_CD_USUARIO_CRIACAO;
ALTER TABLE VEICULO DROP CONSTRAINT FK_VEIC_CD_USUARIO_ALTERACAO;

insert into mensagem (mens_cd_mensagem, mens_ds_mensagem_br, mens_ds_mensagem_en)
values ('MSG-0037', 'O email est� inv�lido.', 'The email field is invalid.');

insert into mensagem (mens_cd_mensagem, mens_ds_mensagem_br, mens_ds_mensagem_en)
values ('MSG-0038', 'O email j� existente.', 'The email already exists.');

-- criar usuario fop.net como `admin
-- update usuario u set u.usua_cd_senha = 'FA473E6C0B75AE83645D2876B41B8406ED175B8F' where u.usua_cd_usuario = 'FOP.NET';

-- depois de apagar os emails iguais criar indice unico no email
CREATE UNIQUE INDEX UN_USUARIO_NM_EMAIL ON USUARIO (USUA_NM_EMAIL);

select u1.usua_nm_email,u1.* from usuario u1
where u1.usua_nm_email in (
select u.usua_nm_email from usuario u
--where u.usua_in_habilitado = 'S'
group by u.usua_nm_email having count(*) >1
)
order by u1.usua_nm_email

ALTER TABLE USUARIO
ADD CONSTRAINT UNQ1_USUARIO
UNIQUE (USUA_NM_EMAIL)

-- tabela pagamento
   CREATE SEQUENCE GEN_PAGT_CD_PAGAMENTO;
   
   -- ATENCAO lembrar de setar a sequence de pedidos para o numero maior do que a do banco local, por causa do boleto.
   -- SET GENERATOR  gen_pedi_cd_pedido to 406;
   -- select max(p.pedi_cd_pedido) from pedido p

   -- Alteracao nome coluna e inclusao de uma nova
   ALTER TABLE PAGAMENTO ALTER PAGT_DH_SOLICITACAO TO PAGT_DH_SOLICITACAO_PAYPAL;
   COMMENT ON COLUMN PAGAMENTO.PAGT_DH_SOLICITACAO IS 'Data da solicita��o do pagamento do paypal.';

   ALTER TABLE PAGAMENTO ADD PAGT_DH_SOLICITACAO_BOLETO TIMESTAMP;
   COMMENT ON COLUMN PAGAMENTO.PAGT_DH_SOLICITACAO_BOLETO IS 'Data da solicita��o do pagamento do boleto.'

   select * from permissao perm where perm.perm_cd_permissao = 'IMPORTAR_DADOS'
   insert into permissao  (perm_cd_permissao, perm_ds_permissao) values ('IMPORTAR_DADOS', 'Realiza o parser de arquivos anexos com os dados de v�rias entidades.');

   select * from perfil_permissao pp where pp.perm_cd_permissao = 'IMPORTAR_DADOS'
    insert into perfil_permissao (perf_cd_perfil, perm_cd_permissao) values ('A','IMPORTAR_DADOS') ;
    insert into perfil_permissao (perf_cd_perfil, perm_cd_permissao) values ('E','IMPORTAR_DADOS') ;

    select * from perfil_permissao pp where pp.perm_cd_permissao = 'BUSCAR_PAGAMENTOS_PEDIDOS'
    insert into permissao  (perm_cd_permissao, perm_ds_permissao) values ('BUSCAR_PAGAMENTOS_PEDIDOS', 'Busca os pedidos com seus pagamentos');
    insert into perfil_permissao (perf_cd_perfil, perm_cd_permissao) values ('A','BUSCAR_PAGAMENTOS_PEDIDOS') ;
    insert into perfil_permissao (perf_cd_perfil, perm_cd_permissao) values ('E','BUSCAR_PAGAMENTOS_PEDIDOS')  ;
    insert into perfil_permissao (perf_cd_perfil, perm_cd_permissao) values ('U0','BUSCAR_PAGAMENTOS_PEDIDOS') ;
    insert into perfil_permissao (perf_cd_perfil, perm_cd_permissao) values ('U9','BUSCAR_PAGAMENTOS_PEDIDOS');

    select * from perfil_permissao pp where pp.perm_cd_permissao = 'EXPIRAR_PEDIDOS' ;
    insert into permissao  (perm_cd_permissao, perm_ds_permissao) values ('EXPIRAR_PEDIDOS', 'Servi�o verificador de pedidos expirados.');
    insert into perfil_permissao (perf_cd_perfil, perm_cd_permissao) values ('A','EXPIRAR_PEDIDOS') ;
    insert into perfil_permissao (perf_cd_perfil, perm_cd_permissao) values ('E','EXPIRAR_PEDIDOS')  ;
    insert into perfil_permissao (perf_cd_perfil, perm_cd_permissao) values ('U0','EXPIRAR_PEDIDOS') ;
    insert into perfil_permissao (perf_cd_perfil, perm_cd_permissao) values ('U9','EXPIRAR_PEDIDOS');
    
    select m.mens_cd_mensagem from mensagem m group by m.mens_cd_mensagem having count(*) > 1

    ALTER TABLE MENSAGEM
    ADD CONSTRAINT PK_MENSAGEM
    PRIMARY KEY (MENS_CD_MENSAGEM)
    USING INDEX UN_MENS_CD_MENSAGEM;

    insert into mensagem (mens_cd_mensagem, mens_ds_mensagem_br, mens_ds_mensagem_en) values ('MSG-0146', 'Dados importados com sucesso.', 'Data imported successfully.');
    insert into mensagem (mens_cd_mensagem, mens_ds_mensagem_br, mens_ds_mensagem_en) values ('MSG-0147', 'Dados incompat�veis com o registro selecionado', 'Inconsistent data with the current record');
    
     -- trocando o tipo do RST_VL_VALOR de float para varchar 100
    update RDB$FIELDS set
    RDB$FIELD_TYPE = 37,
    RDB$FIELD_LENGTH = 800,
    RDB$CHARACTER_LENGTH = 100,
    RDB$CHARACTER_SET_ID = 4,
    RDB$COLLATION_ID = 0
    where RDB$FIELD_NAME = 'RDB$213'
    
    -- trocando tamanho do nome da restricao por causa da multa.
    update RDB$FIELDS set
    RDB$FIELD_LENGTH = 8000,
    RDB$CHARACTER_LENGTH = 2000
    where RDB$FIELD_NAME = 'RDB$212'
    
    -- Aumentando tamanho para 3000 da descricao do pedido
    update RDB$FIELDS set
    RDB$FIELD_LENGTH = 12004,
    RDB$CHARACTER_LENGTH = 3000
    where RDB$FIELD_NAME = 'RDB$186'