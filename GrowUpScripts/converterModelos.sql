CREATE OR ALTER PROCEDURE MIGRAR_MODELO 
as
declare variable v_codigo_marca varchar(255) = '';
declare variable v_retorno varchar(6) = '';
begin
   -- select * from modelo order by 1 ;

for select marc_cd_marca2 from modelo
    into :v_codigo_marca
  do
  begin

    execute procedure retirar_acentos  (:v_codigo_marca) RETURNING_VALUES :v_retorno ;

    v_retorno = replace (v_retorno,  ' ', '') ;

    update modelo set marc_cd_marca2 = :v_retorno where marc_cd_marca2 = :v_codigo_marca;
  end

  suspend;
end ;
