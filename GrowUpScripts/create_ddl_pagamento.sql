CREATE TABLE VARIAVEL (
    VAR_CD_VARIAVEL varchar (20) not NULL,
    VAR_VL_VALOR float NOT NULL
);

ALTER TABLE VARIAVEL ADD 
    CONSTRAINT PK_VARIAVEL PRIMARY KEY  
    (
        VAR_CD_VARIAVEL
    )  
;


CREATE TABLE PAGAMENTO (
    PAGT_CD_PAGAMENTO varchar (20) not NULL , -- Valido por 3 horas
    PEDI_CD_PEDIDO int not NULL,
    PAGT_DS_DESCRICAO varchar (100) not NULL, 
    PAGT_DH_SOLICITACAO timestamp NOT NULL,
    PAGT_VL_VALOR float NOT NULL,
    PAGT_CD_CORRELACAO varchar (13) not null, 
    --response
    PAGT_DH_PAGAMENTO timestamp,
    PAGT_CD_TX varchar(127), 
    PAGT_IN_STATUS_TX varchar(20), 
    PAGT_CD_RESPOSTA varchar(20), 
    PAGT_IN_STATUS_RESPOSTA varchar(20), 
);

comment on column PAGAMENTO.PAGT_CD_PAGAMENTO is 'chave de pagamento do paypal v�lido por um per�odo de 3 horas';
comment on column PAGAMENTO.PEDI_CD_PEDIDO is 'Codigo do pedido que gerou o pagamento';
comment on column PAGAMENTO.PAGT_DS_DESCRICAO is 'Descri��o do pagamento.';
comment on column PAGAMENTO.PAGT_DH_SOLICITACAO is 'Data da solicita��o do pagamento.';
comment on column PAGAMENTO.PAGT_VL_VALOR is 'Valor total do pedido.';
comment on column PAGAMENTO.PAGT_CD_CORRELACAO is 'Identificador do paypal. Este dado deve ser armazenado para cada resposta que receber. Suporte T�cnico PayPal usa esta informa��o para ajudar com problemas relatados.';
comment on column PAGAMENTO.PAGT_DH_PAGAMENTO is 'Data da confirma��o do pagamento pelo servi�o IPN.';
comment on column PAGAMENTO.PAGT_CD_TX is 'Identificador da transa��o.';
comment on column PAGAMENTO.PAGT_IN_STATUS_TX is 'Status da transa��o.[CREATED,COMPLETED,INCOMPLETE,ERROR,REVERSALERROR,PROCESSING,PENDING]';
comment on column PAGAMENTO.PAGT_CD_RESPOSTA is 'Resposta da solicita��o (AckCode) ou da confirma��o de pagamento.[Success,Failure,SuccessWithWarning,FailureWithWarning]';

ALTER TABLE PAGAMENTO ADD 
    CONSTRAINT PK_PAGAMENTO PRIMARY KEY  
    (
        PAGT_CD_PAGAMENTO
    )  
;

CREATE ASC INDEX IDX_PAGT_PEDI_CD_PEDIDO on PAGAMENTO(PEDI_CD_PEDIDO);

alter table PAGAMENTO
  add constraint FK_PAGT_PEDI
  foreign key (PEDI_CD_PEDIDO) references PEDIDO(PEDI_CD_PEDIDO)

-- Mensagens
insert into mensagem (mens_cd_mensagem, mens_ds_mensagem_br, mens_ds_mensagem_en) values ('MSG-0162', 'Pagamento solicitado com sucesso.','Payment requested successfully');
insert into mensagem (mens_cd_mensagem, mens_ds_mensagem_br, mens_ds_mensagem_en) values ('MSG-0163', 'Pagamento confirmado com sucesso.','Payment confirmed successfully');

--Variavel
insert into variavel (var_cd_variavel, var_vl_valor) values ('VAL_UNIT_PEDIDO',25);
