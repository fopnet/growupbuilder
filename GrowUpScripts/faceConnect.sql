/*
delete from sessao s where s.usua_cd_usuario = 'FOP.NET';
delete from pagamento pgto where pgto.pedi_cd_pedido in (select pedi_cd_pedido from pedido p where p.usua_cd_usuario_criacao = 'FOP.NET');
delete from restricao r where r.pedi_cd_pedido in (select pedi_cd_pedido from pedido p where p.usua_cd_usuario_criacao = 'FOP.NET');
delete from tramitacao t where t.usua_cd_usuario_criacao = 'FOP.NET';
delete from pedido p where p.usua_cd_usuario_criacao = 'FOP.NET';
-- delete from veiculo p where p.usua_cd_usuario_criacao = 'FOP.NET';
delete from usuario u where u.usua_cd_usuario = 'FOP.NET';

update usuario u
set u.usua_nm_email = 'bruno@email.com', u.usua_in_habilitado = 'N'
where u.usua_cd_usuario = 'BRUNO';

delete from pedido p where p.pedi_cd_pedido = 442
delete from restricao r where r.pedi_cd_pedido = 442
*/

select * from mensagem m order by 1 desc
select * from perfil p
select s.sess_access_token, char_length(sess_access_token) from sessao s
select * from usuario
select * from permissao per where per.perm_cd_permissao like 'CONECTAR%'
select * from perfil_permissao pp where pp.perm_cd_permissao = 'CONECTAR_FACEBOOK'

-- INSERT INTO PERMISSAO (perm_cd_permissao, perm_ds_permissao) values ('CONECTAR_FACEBOOK','PERMISSÃƒO INTERNA');
-- ALTER TABLE SESSAO ADD SESS_ACCESS_TOKEN VARCHAR(300)
-- insert into perfil_permissao (perf_cd_perfil, perm_cd_permissao) values ('U9','CONECTAR_FACEBOOK');
-- insert into perfil_permissao (perf_cd_perfil, perm_cd_permissao) values ('U0','CONECTAR_FACEBOOK');
-- insert into perfil_permissao (perf_cd_perfil, perm_cd_permissao) values ('E','CONECTAR_FACEBOOK');
-- insert into perfil_permissao (perf_cd_perfil, perm_cd_permissao) values ('A','CONECTAR_FACEBOOK');

