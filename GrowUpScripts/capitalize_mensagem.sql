select upper(SUBSTRING(m.mens_ds_mensagem_br FROM 1 FOR 1)) ||
       lower(SUBSTRING(m.mens_ds_mensagem_br FROM 2 FOR CHAR_LENGTH(m.mens_ds_mensagem_br))),
        m.mens_ds_mensagem_br
 from mensagem m order by 1 desc

update mensagem m
set m.mens_ds_mensagem_br = upper(SUBSTRING(m.mens_ds_mensagem_br FROM 1 FOR 1)) ||
                            lower(SUBSTRING(m.mens_ds_mensagem_br FROM 2 FOR CHAR_LENGTH(m.mens_ds_mensagem_br))),
    m.mens_ds_mensagem_en = upper(SUBSTRING(m.mens_ds_mensagem_en FROM 1 FOR 1)) ||
                            lower(SUBSTRING(m.mens_ds_mensagem_en FROM 2 FOR CHAR_LENGTH(m.mens_ds_mensagem_en)))

select  *
 from mensagem m order by 1 desc
