CREATE SEQUENCE seq_ano;
CREATE SEQUENCE seq_marca ;
CREATE SEQUENCE seq_modelo ;
CREATE SEQUENCE seq_tipo;
CREATE SEQUENCE seq_veiculos;

/*Tabelas*/
CREATE TABLE marca_fipe (
  codigo_marca integer NOT NULL,
  marca VARCHAR(250) NOT NULL, 
  tipo INTEGER NOT NULL, 
  constraint pk_marca_fipe primary key (codigo_marca)
)

CREATE TABLE modelo_fipe (
  codigo_modelo INTEGER NOT NULL,
  modelo VARCHAR(250) NOT NULL, 
  preco VARCHAR(100) NOT NULL, 
  cod_fipe VARCHAR(100) NOT NULL, 
  tipo INTEGER NOT NULL, 
  marca INTEGER NOT NULL, 
  ano VARCHAR(100), 
  CONSTRAINT pk_modelo_fipe PRIMARY KEY(codigo_modelo)
)

CREATE TABLE tipo (
  codigo_tipo INTEGER  NOT NULL,
  tipo VARCHAR(8) NOT NULL, 
  CONSTRAINT pk_tipo_fipe PRIMARY KEY(codigo_tipo)
)

CREATE TABLE ano (
  codigo_ano INTEGER  NOT NULL,
  ano VARCHAR(100) NOT NULL, 
  modelo VARCHAR(250), 
  CONSTRAINT pk_ano_fipe PRIMARY KEY(codigo_ano)
)

CREATE TABLE veiculo_fipe (
  codigo_veiculo INTEGER  NOT NULL,
  mes_referencia VARCHAR(100) NOT NULL, 
  preco VARCHAR(20) NOT NULL, 
  tipo VARCHAR(8) NOT NULL, 
  marca VARCHAR(250) NOT NULL COLLATE UNICODE_CI,
  modelo VARCHAR(250) NOT NULL COLLATE UNICODE_CI,
  data VARCHAR(100) NOT NULL, 
  cod_fipe VARCHAR(100) NOT NULL, 
  ano VARCHAR(50) NOT NULL, 
  CONSTRAINT pk_veiculo_fipe PRIMARY KEY(codigo_veiculo)
)
