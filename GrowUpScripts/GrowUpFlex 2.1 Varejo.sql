select *  from sessao ;
select *  from servico
select * from mensagem where mens_cd_mensagem like  '%MSG-0024%'

select first 17 skip 0     MENS_CD_MENSAGEM,    MENS_DS_MENSAGEM_BR,    MENS_DS_MENSAGEM_EN from MENSAGEM  WHERE UPPER(MENS_CD_MENSAGEM )  LIKE  '%MSG-0024%'  ORDER BY MENS_DS_MENSAGEM_BR ASC

-- alter table MENSAGEM  ALTER column MENS_CD_MENSAGEM type char(8);
-- alter table MENSAGEM drop CONSTRAINT  PK_MENSAGEM

/*
SQL error code = -303
arithmetic exception, numeric overflow, or string truncation
string right truncation
*/
--------------------------------------------------------------------------------
-- Consulta para saber onde uma constraint key � usada.
--------------------------------------------------------------------------------

select * from RDB$RELATION_CONSTRAINTS where rdb$Relation_name = 'MENSAGEM'

SELECT rc.RDB$CONSTRAINT_NAME AS constraint_name,
i.RDB$RELATION_NAME AS table_name,
s.RDB$FIELD_NAME AS field_name,
i.RDB$DESCRIPTION AS description,
rc.RDB$DEFERRABLE AS is_deferrable,
rc.RDB$INITIALLY_DEFERRED AS is_deferred,
refc.RDB$UPDATE_RULE AS on_update,
refc.RDB$DELETE_RULE AS on_delete,
refc.RDB$MATCH_OPTION AS match_type,
i2.RDB$RELATION_NAME AS references_table,
s2.RDB$FIELD_NAME AS references_field,
(s.RDB$FIELD_POSITION + 1) AS field_position
FROM RDB$INDEX_SEGMENTS s
LEFT JOIN RDB$INDICES i ON i.RDB$INDEX_NAME = s.RDB$INDEX_NAME
LEFT JOIN RDB$RELATION_CONSTRAINTS rc ON rc.RDB$INDEX_NAME = s.RDB$INDEX_NAME
LEFT JOIN RDB$REF_CONSTRAINTS refc ON rc.RDB$CONSTRAINT_NAME = refc.RDB$CONSTRAINT_NAME
LEFT JOIN RDB$RELATION_CONSTRAINTS rc2 ON rc2.RDB$CONSTRAINT_NAME = refc.RDB$CONST_NAME_UQ
LEFT JOIN RDB$INDICES i2 ON i2.RDB$INDEX_NAME = rc2.RDB$INDEX_NAME
LEFT JOIN RDB$INDEX_SEGMENTS s2 ON i2.RDB$INDEX_NAME = s2.RDB$INDEX_NAME
WHERE rc.RDB$CONSTRAINT_TYPE = 'PK_MENSAGEM'
ORDER BY s.RDB$FIELD_POSITION
--------------------------------------------------------------------------------
-- Consulta para saber qual o charset do banco e de uma coluna
--------------------------------------------------------------------------------
SELECT a.RDB$CHARACTER_SET_NAME FROM RDB$DATABASE a

SELECT a.RDB$FIELD_NAME, a.RDB$RELATION_NAME, 
       b.RDB$CHARACTER_SET_ID, c.RDB$CHARACTER_SET_NAME
FROM RDB$RELATION_FIELDS a
INNER JOIN RDB$FIELDS b 
   ON b.RDB$FIELD_NAME = a.RDB$FIELD_SOURCE
INNER JOIN RDB$CHARACTER_SETS c 
   ON c.RDB$CHARACTER_SET_ID = b.RDB$CHARACTER_SET_ID
WHERE RDB$RELATION_NAME = 'MENSAGEM'

 -- Migra��o para 2.1 Varejo

     update rdb$relation_fields set
     rdb$null_flag = 0
     where (rdb$field_name = 'CLIE_CD_CLIENTE')
     and (rdb$relation_name = 'PEDIDO');

     update rdb$relation_fields set
     rdb$null_flag = 0
     where (rdb$field_name = 'ARQU_CD_ARQUIVO')
     and (rdb$relation_name = 'PEDIDO');

     update rdb$relation_fields set
     rdb$null_flag = 0
     where (rdb$field_name = 'PROP_CD_CPF')
     and (rdb$relation_name = 'PROPRIETARIO');

     ALTER TABLE PROPRIETARIO DROP CONSTRAINT UNQ1_PROPRIETARIO_CPF;

     update RDB$FIELDS set
    RDB$FIELD_TYPE = 37,
    RDB$FIELD_LENGTH = 14,
    RDB$CHARACTER_LENGTH = 14,
    RDB$COLLATION_ID = 0
    where RDB$FIELD_NAME = 'RDB$96';


    ALTER TABLE PROPRIETARIO ALTER PROP_CD_CPF TO PROP_CD_CPF_CGC ;

    CREATE UNIQUE INDEX UN_PROPRIETARIO_CPF_CGC
    ON PROPRIETARIO (PROP_CD_CPF_CGC);

     ALTER TABLE VEICULO ALTER VEIC_CD_RENAVAM TYPE CHAR(11);
     ALTER TABLE VEICULO ADD  VEIC_CD_MOTOR VARCHAR(20) default NULL;
     ALTER TABLE VEICULO ADD  VEIC_CD_TIPO VARCHAR(20) default NULL;
     ALTER TABLE VEICULO ADD  VEIC_CD_CATEGORIA VARCHAR(20) default NULL;
     ALTER TABLE VEICULO ADD  VEIC_NM_COR VARCHAR(20) default NULL;
     ALTER TABLE VEICULO ADD  VEIC_ULTIMO_ANO_LICENCIAMENTO SMALLINT default NULL;
     ALTER TABLE VEICULO ADD  VEIC_ULTIMA_TRANSACAO DATE default NULL;
     ALTER TABLE VEICULO ADD  VEIC_TX_OBS VARCHAR(500) default NULL;
     ALTER TABLE VEICULO ADD  VEIC_CD_SRF VARCHAR(20) default NULL;

   --- alreando VEIC_CD_ANO_FABRIC de char(4) para smallint
    update RDB$FIELDS set
    RDB$FIELD_TYPE = 7,
    RDB$FIELD_LENGTH = 2,
    RDB$CHARACTER_LENGTH = NULL,
    RDB$CHARACTER_SET_ID = NULL
    where RDB$FIELD_NAME = 'RDB$113'
    ;

    update RDB$RELATION_FIELDS set
    RDB$COLLATION_ID = -1
    where (RDB$FIELD_NAME = 'VEIC_CD_ANO_FABRIC') and
    (RDB$RELATION_NAME = 'VEICULO')

    --- alterando VEIC_CD_ANO_MODELO de char(4) para smallint
      update RDB$FIELDS set
    RDB$FIELD_TYPE = 7,
    RDB$FIELD_LENGTH = 2,
    RDB$CHARACTER_LENGTH = NULL,
    RDB$CHARACTER_SET_ID = NULL
    where RDB$FIELD_NAME = 'RDB$114'
    ;

     update RDB$RELATION_FIELDS set
        RDB$COLLATION_ID = -1
        where (RDB$FIELD_NAME = 'VEIC_CD_ANO_MODELO') and
        (RDB$RELATION_NAME = 'VEICULO')
        ;

   -- Geradores
   CREATE SEQUENCE GEN_ARQU_CD_ARQUIVO;
   CREATE SEQUENCE GEN_CARG_CD_CARGO;
   CREATE SEQUENCE  GEN_CLIE_CD_CLIENTE
   CREATE SEQUENCE GEN_EMPR_CD_EMPRESA;
   CREATE SEQUENCE GEN_MODE_CD_MODELO;
   CREATE SEQUENCE GEN_PEDI_CD_PEDIDO;
   CREATE SEQUENCE GEN_PROP_CD_PROPRIETARIO;
   CREATE SEQUENCE GEN_SERV_CD_SERVICO;
   CREATE SEQUENCE GEN_TRAMI_CD_TRAMITACAO;

    SET GENERATOR GEN_ARQU_CD_ARQUIVO TO 17;
    SET GENERATOR GEN_CARG_CD_CARGO to 0;
    SET GENERATOR GEN_CLIE_CD_CLIENTE TO 39;
    SET GENERATOR GEN_EMPR_CD_EMPRESA TO 3;
    SET GENERATOR GEN_MODE_CD_MODELO TO 10371;
    SET GENERATOR GEN_PEDI_CD_PEDIDO TO 282 ;
    SET GENERATOR GEN_PROP_CD_PROPRIETARIO TO 6;
    SET GENERATOR GEN_SERV_CD_SERVICO TO 16;
    SET GENERATOR GEN_TRAMI_CD_TRAMITACAO TO 368;
