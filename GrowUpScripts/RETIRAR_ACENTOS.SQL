create or alter procedure RETIRAACENTOS (
    PTEXTO varchar(255))
returns (
    OSEM_ACENTOS varchar(255))
as
declare variable V_COM_ACENTO varchar(28) = '����������������������������';
declare variable V_SEM_ACENTO varchar(28) = 'aaeouaoaeioucuAAEOUAOAEIOUCU';
declare variable V_LOOP_1 integer = 1;
declare variable V_LOOP_2 integer = 1;
declare variable V_TEXTO_A varchar(1) = '';
declare variable V_TEXTO_B varchar(100) = '';
begin
  V_LOOP_1 = 1;
 
  /* INICIAR O LOOP NO TEXTO PRINCIPAL ------------ */
  while (V_LOOP_1 <= char_length(:ptexto)) do
  begin
    V_TEXTO_A = '';
    V_TEXTO_A = substring(pTexto from :V_LOOP_1 for 1);
    V_LOOP_2 = 1;

    /* INICIAR O LOOP NO TEXTO COM ACENTO --------- */
    while (V_LOOP_2 <= char_length(V_COM_ACENTO)) do
    begin
      if (V_TEXTO_A = substring(V_COM_ACENTO from :V_LOOP_2 for 1)) then
      begin
        V_TEXTO_A = substring(V_SEM_ACENTO from :V_LOOP_2 for 1);
      end
      V_LOOP_2 = V_LOOP_2 + 1;
    end
    V_TEXTO_B = V_TEXTO_B || V_TEXTO_A;
    V_LOOP_1 = V_LOOP_1 + 1;
  end
  osem_acentos = V_TEXTO_B;
  suspend;
end
