import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import junit.framework.TestCase;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;

/**
 * @author Felipe
 *
 */
public class CollectionsPersistence extends TestCase {

	@Test
	public void testRetain() {
		List<Integer> tela = Arrays.asList(1,2,3);
		List<Integer> banco = Arrays.asList(2,3,4);
		
		Collection<Integer> comum = CollectionUtils.retainAll(tela, banco);
		printCollection(comum);
		
		Collection<Integer> novas = CollectionUtils.subtract(tela, comum);
		printCollection(novas);
		
		Collection<Integer> apagar = CollectionUtils.subtract(banco, comum);
		printCollection(apagar);
	}
	
	public void printCollection(Collection<? extends Object> col) {
		Iterator<? extends Object>  it = col.iterator();
		while (it.hasNext()) {
			System.out.print(it.next());
			System.out.print(it.hasNext() ? "," : "\n"); 
		}
	}
	
}
