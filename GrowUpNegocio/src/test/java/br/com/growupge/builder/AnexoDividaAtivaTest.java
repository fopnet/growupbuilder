package br.com.growupge.builder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.Test;

import br.com.growupge.dto.RestricaoDTO;
import br.com.growupge.exception.GrowUpException;

/**
 * @author Felipe
 *
 */
public class AnexoDividaAtivaTest {
	private final static String ANEXO_NOME = "Anexo DividaAtiva.txt"; 
	
	@Test
	public void testParserAnexo() throws IOException, GrowUpException {
		InputStream is = getClass().getClassLoader().getResourceAsStream(ANEXO_NOME);
		AnexoDividaAtivaBuilder builder = new AnexoDividaAtivaBuilder(is);
		List<RestricaoDTO> restricoes = builder.build();
		
		for (RestricaoDTO restricaoDTO : restricoes) 
			System.out.println("Restricao:".concat(restricaoDTO.getNome()) + " Valor:".concat(restricaoDTO.getValor().toString()) + " Placa:".concat(restricaoDTO.getPedido().getPlaca()) );
	}


}
