package br.com.growupge.builder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.com.growupge.dto.RestricaoDTO;
import br.com.growupge.exception.GrowUpException;

/**
 * @author Felipe
 *
 */
public class AnexoMultasBuilderTest {
	private final static String ANEXO_NOME = "Anexo Multas.txt"; 
	
	@Test
	public void testParserAnexo() throws IOException, GrowUpException {
		InputStream is = getClass().getClassLoader().getResourceAsStream(ANEXO_NOME);
		AnexoRestricaoBuilder builder = new AnexoMultasBuilder(is);
		List<RestricaoDTO> restricoes = builder.build();
		Assert.assertEquals("Tamanho incorreto", 342, restricoes.size());
		int idx=0;
		Assert.assertEquals("Valor n�o esperado", "Receita", restricoes.get(idx).getNome());
		Assert.assertEquals("Valor n�o esperado", "DETRAN", restricoes.get(idx++).getValor());
		
		for (RestricaoDTO restricaoDTO : restricoes) 
			System.out.println("Restricao:".concat(restricaoDTO.getNome()) + " Valor:".concat(restricaoDTO.getValor().toString()));
	}


}
