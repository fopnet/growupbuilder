package br.com.growupge.builder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.com.growupge.dto.RestricaoDTO;
import br.com.growupge.exception.GrowUpException;

/**
 * @author Felipe
 *
 */
public class AnexoLicenciamentoEletronicoBuilderTest {
	private final static String ANEXO_NOME =  "Anexo Licenciamento Eletronico.txt"; 
	
	@Test
	public void testAnexoLicenciamentoEletronico() throws IOException, GrowUpException {
		InputStream is = getClass().getClassLoader().getResourceAsStream(ANEXO_NOME);
		AnexoRestricaoBuilder builder = new AnexoLicenciamentoEletronicoBuilder(is);
		List<RestricaoDTO> restricoes = builder.build();
		Assert.assertEquals("Tamanho incorreto", 2, restricoes.size());
		Assert.assertEquals("Valor n�o esperado", "Ve�culo com Inscri��o na Divida Ativa (P.G.E.)", restricoes.get(0).getValor());
		Assert.assertEquals("Valor n�o esperado", "N�O", restricoes.get(1).getValor());
		
	}


}
