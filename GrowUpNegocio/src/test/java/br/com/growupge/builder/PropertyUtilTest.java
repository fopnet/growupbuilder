package br.com.growupge.builder;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.log4j.Logger;
import org.junit.Test;

import br.com.growupge.dto.VeiculoDTO;
import br.com.growupge.enums.TipoCombustivel;

public class PropertyUtilTest {
	private final Logger logger = Logger.getLogger(PropertyUtilTest.class);
	@Test
	public void test() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		VeiculoDTO dto = new VeiculoDTO("placa");
		Object property = (String) PropertyUtils.getSimpleProperty(dto, "codigo");
		logger.info( property.toString() );
		property = (String) PropertyUtils.getProperty(dto, "codigo");
		logger.info( property.toString() );

		logger.info( FieldUtils.readField(TipoCombustivel.ALCOOL, "name" , true) );
	}

}
