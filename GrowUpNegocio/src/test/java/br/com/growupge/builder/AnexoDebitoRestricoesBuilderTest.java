package br.com.growupge.builder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.com.growupge.dto.RestricaoDTO;
import br.com.growupge.exception.GrowUpException;

/**
 * @author Felipe
 *
 */
public class AnexoDebitoRestricoesBuilderTest {
	private final static String ANEXO_NOME = "Anexo Debitos e Restricoes Veiculos.txt"; 
	
	@Test
	public void testParserAnexo() throws IOException, GrowUpException {
		InputStream is = getClass().getClassLoader().getResourceAsStream(ANEXO_NOME);
		AnexoDebitoRestricoesBuilder builder = new AnexoDebitoRestricoesBuilder(is);
		List<RestricaoDTO> restricoes = builder.build();
		Assert.assertEquals("Tamanho incorreto", 2, restricoes.size());
		int idx=0;

		Assert.assertEquals("Valor n�o esperado", 8395.29d, restricoes.get(idx++).getValor());
		Assert.assertEquals("Valor n�o esperado", 4936.97d, restricoes.get(idx++).getValor());
		
		/*
		Assert.assertEquals("Valor n�o esperado", "BLOQUEIO DE FURTO", restricoes.get(idx).getNome());
		Assert.assertEquals("Valor n�o esperado", "NADA CONSTA", restricoes.get(idx++).getValor());
		
		Assert.assertEquals("Valor n�o esperado", "RESTRI��O ADMINISTRATIVA", restricoes.get(idx).getNome());
		Assert.assertEquals("Valor n�o esperado", "NADA CONSTA", restricoes.get(idx++).getValor());
		
		Assert.assertEquals("Valor n�o esperado", "RESTRI��O TRIBUT�RIA", restricoes.get(idx).getNome());
		Assert.assertEquals("Valor n�o esperado", "NADA CONSTA", restricoes.get(idx++).getValor());
		
		Assert.assertEquals("Valor n�o esperado", "RESTRI��O JUDICI�RIA", restricoes.get(idx).getNome());
		Assert.assertEquals("Valor n�o esperado", "NADA CONSTA", restricoes.get(idx++).getValor());
		
		Assert.assertEquals("Valor n�o esperado", "RESTRI��O FINANCEIRA", restricoes.get(idx).getNome());
		Assert.assertEquals("Valor n�o esperado", "NADA CONSTA", restricoes.get(idx++).getValor());
		
		Assert.assertEquals("Valor n�o esperado", "REGISTRO GUINCHO", restricoes.get(idx).getNome());
		Assert.assertEquals("Valor n�o esperado", "NADA CONSTA", restricoes.get(idx++).getValor());
		
		Assert.assertEquals("Valor n�o esperado", "INSPE��O VEICULAR DO ANO", restricoes.get(idx).getNome());
		Assert.assertEquals("Valor n�o esperado", "NADA CONSTA *", restricoes.get(idx++).getValor());
		
		Assert.assertEquals("Valor n�o esperado", "LICENCIAMENTO DO ANO", restricoes.get(idx).getNome());
		Assert.assertEquals("Valor n�o esperado", "CONSTA 2009*", restricoes.get(idx++).getValor());
*/		
	}


}
