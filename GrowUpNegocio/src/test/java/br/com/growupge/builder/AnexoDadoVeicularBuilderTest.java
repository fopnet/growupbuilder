package br.com.growupge.builder;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Assert;
import org.junit.Test;

import br.com.growupge.dto.VeiculoDTO;
import br.com.growupge.exception.GrowUpException;

/**
 * @author Felipe
 *
 */
public class AnexoDadoVeicularBuilderTest {
	private final static String ANEXO_NOME = "Anexo Dados do Veiculo.txt"; 
	
	@Test
	public void testAnexoDadosVeiculo() throws IOException, GrowUpException {
		InputStream is = getClass().getClassLoader().getResourceAsStream(ANEXO_NOME);
		AnexoDadoVeicularBuilder builder = new AnexoDadoVeicularBuilder(is);
		VeiculoDTO veic = builder.lerTodosCampos()
								.build();
		Assert.assertEquals("Valor n�o esperado", "HAV-8659", veic.getCodigo());
		Assert.assertEquals("Valor n�o esperado", "813231930", veic.getRenavam());
		Assert.assertEquals("Valor n�o esperado", "9BGTT75B04C124572", veic.getChassi());
		Assert.assertNull("Valor n�o esperado", veic.getProprietario().getNome());
		Assert.assertNull("Valor n�o esperado", veic.getProprietario().getCpfcnpj());
		Assert.assertEquals("Valor n�o esperado", "GM", veic.getMarca().getNome());
		Assert.assertEquals("Valor n�o esperado", "ZAFIRA CD", veic.getModelo().getNome());
		Assert.assertEquals("Valor n�o esperado", Integer.valueOf(2003), veic.getAnoFabric());
		Assert.assertEquals("Valor n�o esperado", Integer.valueOf(2004), veic.getAnoModelo());
		Assert.assertEquals("Valor n�o esperado", "PARTICULAR", veic.getCategoria());
		Assert.assertEquals("Valor n�o esperado", "AUTOMOVEL", veic.getTipo());
		Assert.assertEquals("Valor n�o esperado", "PRATA", veic.getCor());
		
	}

	@Test
	public void testAnexoDadosVeiculoSomentePlaca() throws IOException, GrowUpException {
		InputStream is = getClass().getClassLoader().getResourceAsStream(ANEXO_NOME);
		AnexoDadoVeicularBuilder builder = new AnexoDadoVeicularBuilder(is);
		VeiculoDTO veic = builder.lerPlaca()
								.build();
		Assert.assertEquals("Valor n�o esperado", "HAV-8659", veic.getCodigo());
		Assert.assertNull("Valor n�o esperado", veic.getRenavam());
		Assert.assertNull("Valor n�o esperado", veic.getChassi());
		Assert.assertNull("Valor n�o esperado", veic.getProprietario());
		Assert.assertNull("Valor n�o esperado", veic.getMarca());
		Assert.assertNull("Valor n�o esperado", veic.getModelo());
		Assert.assertNull("Valor n�o esperado", veic.getAnoFabric());
		Assert.assertNull("Valor n�o esperado", veic.getAnoModelo());
		Assert.assertNull("Valor n�o esperado", veic.getCategoria());
		Assert.assertNull("Valor n�o esperado", veic.getTipo());
		Assert.assertNull("Valor n�o esperado", veic.getCor());
		
	}


}
