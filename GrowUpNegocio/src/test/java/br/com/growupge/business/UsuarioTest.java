package br.com.growupge.business;

import static br.com.growupge.utility.StringUtil.encodeParam;

import org.junit.Test;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.HtmlTemplateFactory;
import br.com.growupge.template.HtmlTemplate;

/**
 * @author Felipe
 *
 */
public class UsuarioTest {

	@Test
	public void testNotificarConfirmacaoEmail() throws GrowUpException {
		UsuarioDTO usr = new UsuarioDTO();
		usr.setCodigo("FOP");
		usr.setNome("FOP");
		usr.setEmail("fop.net@gmail.com");
		
		
		// Obtem o Template com base no idioma do usu�rio
		HtmlTemplate templateEmail = HtmlTemplateFactory.getTemplateConfirmarEmail();

		String link = Constantes.URL_CONFIRMA_EMAIL;
		link += encodeParam(Constantes.PARAM_USUARIO, usr.getCodigo());
		link += encodeParam(Constantes.PARAM_EMAIL, usr.getEmail());
		
		// Adiciona os parametros que dever�o ser substituidos no template
		templateEmail.adicionarParametro("USR", usr.getNome());
		templateEmail.adicionarParametro("LINK", link);

		// Executa o template
		templateEmail.execute();
		
	
	}

}
