package  br.com.growupge.validador;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Felipe
 *
 */
public class ValidadorTest {

	@Test
	public void testCPFOK() {
		Assert.assertTrue("CPF deveria estar v�lido", Validador.validarCpf("08466334777"));
	}

	@Test
	public void testCPFError() {
		Assert.assertFalse("CPF deveria estar v�lido", Validador.validarCpf("08466334770"));
	}
	
	@Test
	public void testCGCOK() {
		Assert.assertTrue("CGC deveria estar v�lido", Validador.validarCnpj("68718162000101"));
	}
	
	@Test
	public void testCGCError() {
		Assert.assertFalse("CGC deveria estar v�lido", Validador.validarCnpj("68718162000100"));
	}
	
}
