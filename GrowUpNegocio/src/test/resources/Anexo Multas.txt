Multas

Receita	DETRAN
Munic�pio	S�o Paulo
Local	AV DO ESTADO 900
Data	02/10/2009
Hora	15:41
Infra��o	Nao Registrar Veiculo no Prazo de 30 Dias.
N� do A.I.T.	3A1025591
N� da Guia	173871616
Vencimento	14/01/2010
Valor	127,69
________________________________________


Total Multas	127,69




Multas RENAINF

�rg�o Autuador	POLICIA RODOVIARIA FEDERAL
Mun. da Infra��o	MG - BELO HORIZONTE
Local	BR-040 KM-538 UF-MG
Data	30/04/2009
Hora	10:53
Enquadramento	Transitar em at� 20% acima da velocidade permitida
N� do A.I.T.	R206565569
N� da Guia Renainf	1045859710
N� do Renainf	01533740372
Data do Vencimento	04/11/2012
Valor da Infra��o	85,13
	
________________________________________

�rg�o Autuador	POLICIA RODOVIARIA FEDERAL
Mun. da Infra��o	RJ � PETROPOLIS
Local	BR-040 KM-91 UF-RJ
Data	04/05/2009
Hora	15:26
Enquadramento	Transitar em at� 20% acima da velocidade permitida
N� do A.I.T.	R206886071
N� da Guia Renainf	1045869545
N� do Renainf	01534942653
Data do Vencimento	04/11/2012
Valor da Infra��o	85,13
	
________________________________________

�rg�o Autuador	RJ - Pref. de RIO DE JANEIRO
Mun. da Infra��o	RJ - RIO DE JANEIRO
Local	AV PEDROII PROX.RUA F.DE MELLO
Data	04/11/2009
Hora	12:44
Enquadramento	Avancar Sinal Vermelho do Semaforo ou de Parada Obrigatoria.
N� do A.I.T.	B40528072
N� da Guia Renainf	1018519269
N� do Renainf	01567065554
Data do Vencimento	22/03/2010
Valor da Infra��o	191,54
	










________________________________________

�rg�o Autuador	RJ - Pref. de RIO DE JANEIRO
Mun. da Infra��o	RJ - RIO DE JANEIRO
Local	ESTRADA DA SOCA (PROX AO N 46
Data	08/11/2009
Hora	12:03
Enquadramento	Avancar Sinal Vermelho do Semaforo ou de Parada Obrigatoria.
N� do A.I.T.	B40549470
N� da Guia Renainf	1018740340
N� do Renainf	01568085605
Data do Vencimento	29/03/2010
Valor da Infra��o	191,54
	


________________________________________

�rg�o Autuador	RJ - Pref. de RIO DE JANEIRO
Mun. da Infra��o	RJ - RIO DE JANEIRO
Local	CAMPO DE SAO CRISTOVAO PROX. A
Data	22/11/2009
Hora	13:40
Enquadramento	Avancar Sinal Vermelho do Semaforo ou de Parada Obrigatoria.
N� do A.I.T.	B40652494
N� da Guia Renainf	1019126711
N� do Renainf	01570019371
Data do Vencimento	09/04/2010
Valor da Infra��o	191,54
	
________________________________________

�rg�o Autuador	RJ - Pref. de RIO DE JANEIRO
Mun. da Infra��o	RJ - RIO DE JANEIRO
Local	AV FRANCISCO BICALHO EF CEG
Data	23/11/2009
Hora	16:50
Enquadramento	Estacionar no Passeio ou Sobre Faixa de Pedestres/Ciclovias.
N� do A.I.T.	B40673220
N� da Guia Renainf	1019126190
N� do Renainf	01570230102
Data do Vencimento	09/04/2010
Valor da Infra��o	127,69
	
________________________________________

�rg�o Autuador	RJ - Pref. de RIO DAS OSTRAS
Mun. da Infra��o	RJ - RIO DAS OSTRAS
Local	RJ 106 KM 148,5
Data	16/12/2009
Hora	08:44
Enquadramento	Estacionar Junto a Ponto Embarque/Desembarque de Passageiros.
N� do A.I.T.	K30004738
N� da Guia Renainf	1027071193
N� do Renainf	01575102846
Data do Vencimento	17/04/2011
Valor da Infra��o	85,13
	
________________________________________

�rg�o Autuador	RJ - Pref. de RIO DE JANEIRO
Mun. da Infra��o	RJ - RIO DE JANEIRO
Local	RUA EQUADOR 613
Data	23/12/2009
Hora	11:32
Enquadramento	Estacionar em Desacordo com a Sinalizacao.
N� do A.I.T.	B40959675
N� da Guia Renainf	1020057830
N� do Renainf	01576384764
Data do Vencimento	17/05/2010
Valor da Infra��o	53,20
	
________________________________________

�rg�o Autuador	RJ - Pref. de RIO DE JANEIRO
Mun. da Infra��o	RJ - RIO DE JANEIRO
Local	AV.BRASIL KM3,4-PASSARELA 05-S
Data	30/03/2010
Hora	16:08
Enquadramento	Transitar na Faixa/Pista da Esquerda de Circulacao Exclusiva.
N� do A.I.T.	B41832967
N� da Guia Renainf	1022641220
N� do Renainf	01595322477
Data do Vencimento	13/08/2010
Valor da Infra��o	127,69
	
________________________________________

�rg�o Autuador	RJ - Pref. de RIO DE JANEIRO
Mun. da Infra��o	RJ - RIO DE JANEIRO
Local	AV.BRASIL,KM13-PASSARELA 18-SE
Data	30/03/2010
Hora	15:59
Enquadramento	Transitar na Faixa/Pista da Esquerda de Circulacao Exclusiva.
N� do A.I.T.	B41844574
N� da Guia Renainf	1022690450
N� do Renainf	01595566309
Data do Vencimento	16/08/2010
Valor da Infra��o	127,69
	
________________________________________

�rg�o Autuador	RJ - DER - RIO DE JANEIRO
Mun. da Infra��o	RJ - ARMACAO DE BUZIOS
Local	RJ 102 KM 156,9
Data	30/03/2010
Hora	11:45
Enquadramento	Transitar em at� 20% acima da velocidade permitida
N� do A.I.T.	Y30822850
N� da Guia Renainf	1022732778
N� do Renainf	01594902186
Data do Vencimento	16/09/2010
Valor da Infra��o	85,13
	
________________________________________

�rg�o Autuador	RJ - DER - RIO DE JANEIRO
Mun. da Infra��o	RJ - RIO DAS OSTRAS
Local	RJ 106 KM 144,4
Data	10/04/2010
Hora	01:09
Enquadramento	Transitar em at� 20% acima da velocidade permitida
N� do A.I.T.	Y30827755
N� da Guia Renainf	1022738872
N� do Renainf	01595757139
Data do Vencimento	16/09/2010
Valor da Infra��o	85,13
	
________________________________________

�rg�o Autuador	RJ - DER - RIO DE JANEIRO
Mun. da Infra��o	RJ � MACAE
Local	RJ106-TREVO CANCELA PRETA PR.1
Data	02/05/2011
Hora	06:13
Enquadramento	Transitar em at� 20% acima da velocidade permitida
N� do A.I.T.	X34428429
N� da Guia Renainf	1034145137
N� do Renainf	01671716604
Data do Vencimento	16/10/2011
Valor da Infra��o	85,13








	
________________________________________

�rg�o Autuador	RJ - Pref. de MACAE
Mun. da Infra��o	RJ � MACAE
Local	TRV PROFRUBENSCARNEIRO SENTC B
Data	24/05/2011
Hora	09:33
Enquadramento	Avancar Sinal Vermelho do Semaforo ou de Parada Obrigatoria.
N� do A.I.T.	O29482663
N� da Guia Renainf	1034947618
N� do Renainf	01675000441
Data do Vencimento	04/12/2011
Valor da Infra��o	191,54
	
________________________________________

�rg�o Autuador	RJ - DER - RIO DE JANEIRO
Mun. da Infra��o	RJ - CABO FRIO
Local	RJ106 - KM129,5 FX 1 SENTIDO M
Data	29/06/2011
Hora	21:32
Enquadramento	Transitar em at� 20% acima da velocidade permitida
N� do A.I.T.	X34468801
N� da Guia Renainf	1035232911
N� do Renainf	01681291266
Data do Vencimento	14/12/2011
Valor da Infra��o	85,13

	
________________________________________

�rg�o Autuador	RJ - Pref. de MACAE
Mun. da Infra��o	RJ � MACAE
Local	TREVO NOVO CAVALEIROS SENT B C
Data	22/07/2011
Hora	08:20
Enquadramento	Avancar Sinal Vermelho do Semaforo ou de Parada Obrigatoria.
N� do A.I.T.	O29486574
N� da Guia Renainf	1052193316
N� do Renainf	01686874685
Data do Vencimento	09/07/2013
Valor da Infra��o	191,54
	
________________________________________

�rg�o Autuador	RJ - Pref. de RIO DAS OSTRAS
Mun. da Infra��o	RJ - RIO DAS OSTRAS
Local	RJ 106KM 152
Data	01/10/2011
Hora	08:07
Enquadramento	Transitar em Calcadas,Passeios,Passarelas,Ciclovias,Gramados.
N� do A.I.T.	K30128469
N� da Guia Renainf	1044512143
N� do Renainf	01698693788
Data do Vencimento	14/11/2012
Valor da Infra��o	574,62
	
________________________________________

�rg�o Autuador	RJ - Pref. de RIO DAS OSTRAS
Mun. da Infra��o	RJ - RIO DAS OSTRAS
Local	RJ 106NR 146
Data	15/10/2011
Hora	11:08
Enquadramento	Parar na Pista de Rolamento das Vias Dotadas de Acostamento.
N� do A.I.T.	K30125940
N� da Guia Renainf	1046535092
N� do Renainf	01701465930
Data do Vencimento	27/12/2012
Valor da Infra��o	127,69
	
________________________________________

�rg�o Autuador	RJ - DER - RIO DE JANEIRO
Mun. da Infra��o	RJ � MACAE
Local	RJ 106, RIO DAS OSTRAS X MACAE
Data	12/12/2011
Hora	08:10
Enquadramento	Transitar em Calcadas,Passeios,Passarelas,Ciclovias,Gramados.
N� do A.I.T.	Z28788431
N� da Guia Renainf	1040546481
N� do Renainf	01714076440
Data do Vencimento	29/05/2012
Valor da Infra��o	574,62
	
________________________________________

�rg�o Autuador	RJ - Pref. de RIO DAS OSTRAS
Mun. da Infra��o	RJ - RIO DAS OSTRAS
Local	RJ 106 KM.148
Data	25/02/2012
Hora	15:29
Enquadramento	Estacionar em Desacordo com a Sinalizacao.
N� do A.I.T.	K30037117
N� da Guia Renainf	1043275034
N� do Renainf	01731396848
Data do Vencimento	26/09/2012
Valor da Infra��o	53,20
	
________________________________________

�rg�o Autuador	RJ - Pref. de RIO DAS OSTRAS
Mun. da Infra��o	RJ - RIO DAS OSTRAS
Local	RJ 106KM 160
Data	05/03/2012
Hora	07:22
Enquadramento	Transitar em Calcadas,Passeios,Passarelas,Ciclovias,Gramados.
N� do A.I.T.	K30139936
N� da Guia Renainf	1043273955
N� do Renainf	01731064721
Data do Vencimento	26/09/2012
Valor da Infra��o	574,62
	
________________________________________

�rg�o Autuador	RJ - Pref. de MACAE
Mun. da Infra��o	RJ � MACAE
Local	TREVO ACESSO BAIRRO GLORIA C B
Data	07/03/2012
Hora	18:47
Enquadramento	Avancar Sinal Vermelho do Semaforo ou de Parada Obrigatoria.
N� do A.I.T.	O29515727
N� da Guia Renainf	1043629555
N� do Renainf	01732536996
Data do Vencimento	11/10/2012
Valor da Infra��o	191,54
	
________________________________________

�rg�o Autuador	RJ - Pref. de MACAE
Mun. da Infra��o	RJ � MACAE
Local	AV ATL NTICA FRENTE N 1314 FX
Data	11/03/2012
Hora	07:08
Enquadramento	Transitar em at� 20% acima da velocidade permitida
N� do A.I.T.	O29305165
N� da Guia Renainf	1043629919
N� do Renainf	01732544581
Data do Vencimento	11/10/2012
Valor da Infra��o	85,13
	
________________________________________

�rg�o Autuador	RJ - Pref. de RIO DAS OSTRAS
Mun. da Infra��o	RJ - RIO DAS OSTRAS
Local	RJ 106KM 151
Data	15/03/2012
Hora	14:44
Enquadramento	Ultrapassar Veiculo pelo Acostamento.
N� do A.I.T.	K30137926
N� da Guia Renainf	1043629336
N� do Renainf	01732139377
Data do Vencimento	11/10/2012
Valor da Infra��o	127,69

	
________________________________________

�rg�o Autuador	RJ - DER - RIO DE JANEIRO
Mun. da Infra��o	RJ - RIO DAS OSTRAS
Local	RJ106 - KM162 FX 1 SENTIDO RIO
Data	15/04/2012
Hora	02:10
Enquadramento	Transitar em at� 20% acima da velocidade permitida
N� do A.I.T.	X34683596
N� da Guia Renainf	1043018580
N� do Renainf	01737396521
Data do Vencimento	10/09/2012
Valor da Infra��o	85,13
	
________________________________________

�rg�o Autuador	RJ - DER - RIO DE JANEIRO
Mun. da Infra��o	RJ � MACAE
Local	RJ 106 KM 167,1
Data	21/04/2012
Hora	17:32
Enquadramento	Transitar em at� 20% acima da velocidade permitida
N� do A.I.T.	Y31257411
N� da Guia Renainf	1043241061
N� do Renainf	01740651367
Data do Vencimento	25/09/2012
Valor da Infra��o	85,13
	
________________________________________

�rg�o Autuador	RJ - DER - RIO DE JANEIRO
Mun. da Infra��o	RJ - RIO DAS OSTRAS
Local	RJ106 - KM162 FX 1 SENTIDO MAC
Data	24/04/2012
Hora	13:51
Enquadramento	Transitar em at� 20% acima da velocidade permitida
N� do A.I.T.	X34689117
N� da Guia Renainf	1043220124
N� do Renainf	01739153480
Data do Vencimento	24/09/2012
Valor da Infra��o	85,13
	
________________________________________

�rg�o Autuador	RJ - DER - RIO DE JANEIRO
Mun. da Infra��o	RJ - RIO DAS OSTRAS
Local	RJ 106 KM 163,0
Data	28/04/2012
Hora	08:17
Enquadramento	Transitar em at� 20% acima da velocidade permitida
N� do A.I.T.	Y31259933
N� da Guia Renainf	1043241589
N� do Renainf	01742252044
Data do Vencimento	25/09/2012
Valor da Infra��o	85,13
	
________________________________________

�rg�o Autuador	RJ - DER - RIO DE JANEIRO
Mun. da Infra��o	RJ - RIO DAS OSTRAS
Local	RJ 106 KM 163,0
Data	01/05/2012
Hora	13:45
Enquadramento	Transitar em at� 20% acima da velocidade permitida
N� do A.I.T.	Y31260719
N� da Guia Renainf	1043241681
N� do Renainf	01742284345
Data do Vencimento	25/09/2012
Valor da Infra��o	85,13
	
________________________________________

�rg�o Autuador	RJ - DER - RIO DE JANEIRO
Mun. da Infra��o	RJ - RIO DAS OSTRAS
Local	RJ106 - KM162 FX 1 SENTIDO MAC
Data	02/05/2012
Hora	15:24
Enquadramento	Transitar em at� 20% acima da velocidade permitida
N� do A.I.T.	X34697127
N� da Guia Renainf	1043522700
N� do Renainf	01744209855
Data do Vencimento	07/10/2012
Valor da Infra��o	85,13
	
________________________________________


Total Renainf	4.809,77
