package br.com.growupge.autenticador;

import javax.mail.PasswordAuthentication;

public class AutenticadorEmail extends javax.mail.Authenticator {

	private String user;

	private String senha;

	public AutenticadorEmail() {
	}

	public AutenticadorEmail(String user, String senha) {
		this.user = user;
		this.senha = senha;
	}

	@Override
	public PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(user, senha);
	}
}
