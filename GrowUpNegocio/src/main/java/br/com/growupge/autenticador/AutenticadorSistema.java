/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 16/01/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.autenticador;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.enums.TipoIdioma;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.security.BaseFactorySecurity;
import br.com.growupge.util.MensagemBuilder;

/**
 * @version 1.0
 * @created 30-jan-2006 16:35:41
 */
public class AutenticadorSistema extends Autenticador {

	/**
	 * Construtor para esta classe.
	 * 
	 */
	public AutenticadorSistema() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.authority.BaseFactoryAuthenticator#login(br.com.growupge.dto.UsuarioDTO)
	 */
	@Override
	public void autenticar(UsuarioDTO userdto, String pwd) throws GrowUpException {

		BaseFactorySecurity bfs = BaseFactorySecurity.getInstance(Constantes.SECURITY_SHA);
		String senha;
		senha = bfs.criptografar(pwd);

		if (!userdto.getSenha().equals(senha)) {
			throw new GrowUpException(MSGCODE.USUARIO_SENHA_INVALIDA, MensagemBuilder.getMensagem(
					MSGCODE.USUARIO_SENHA_INVALIDA, TipoIdioma.IDIOMA_BR.toString(), null));
		}
	}
}
