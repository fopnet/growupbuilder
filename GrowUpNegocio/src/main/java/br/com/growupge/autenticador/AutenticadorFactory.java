/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 16/01/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.autenticador;


/**
 * @version 1.0
 * @updated 30-jan-2006 16:36:50
 */
public abstract class AutenticadorFactory {

	/**
	 * Construtor para esta classe.
	 * 
	 */
	private AutenticadorFactory() {
	}

	/**
	 * Retorna uma inst�ncia de autentica��o baseada no tipo: 0 =
	 * SigemAutenticador 1 = LDAPAutenticador 2 = TokenAutenticador
	 */
	public static Autenticador getAutenticadorSistema() {
//		switch (authenticatorType) {
//		case Constantes.AUTHENTICATION_TYPE_SYSTEM:
			return new AutenticadorSistema();
//		case Constantes.AUTHENTICATION_TYPE_LDAP:
//			return null;
//		case Constantes.AUTHENTICATION_TYPE_TOKEN:
//			return null;
//		}
//		return null;
	}

}
