/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 16/01/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.autenticador;

import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.exception.GrowUpException;

/**
 * @version 1.0
 * @updated 30-jan-2006 16:36:50
 */
public abstract class Autenticador  {

	/**
	 * Atributo '<code>username</code>' do tipo String
	 */
	protected String username;

	/**
	 * Atributo '<code>password</code>' do tipo String
	 */
	protected String password;

	/**
	 * Construtor para esta classe.
	 * 
	 */
	public Autenticador() {
	}

	/**
	 * M�todo utilizado para autenticar o usu�rio e senha.
	 * 
	 * @param userdto
	 *            Objeto de trasnfer�nci com os dados do usu�rio.
	 * @param pwd
	 *            senha
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o.
	 */
	public abstract void autenticar(UsuarioDTO userdto, String pwd) throws GrowUpException;

}
