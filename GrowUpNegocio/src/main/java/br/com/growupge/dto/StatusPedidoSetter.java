package br.com.growupge.dto;

import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.exception.GrowUpDAOException;

public class StatusPedidoSetter  {

	public static void setStatus(ResultSet rs, boolean isEager, PedidoVeiculoDTO dto) throws GrowUpDAOException {
		try {
			dto.setStatus( getEnum( rs.getString("PEDI_IN_STATUS") ) );
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		}
	}

	/**
	 * Metodo retorna o enum TipoUsuario correspondente ao codigo
	 * 
	 * @param tipo
	 *            String identificando o codigo do enum
	 * @return Comentar aqui.
	 */
	private static StatusPedido getEnum(final String tipo) {
		StatusPedido enm = null;

		if (tipo != null) {
			for (StatusPedido tp : StatusPedido.values()) {
				if (tp.getTipo().equals(tipo.toUpperCase().trim())) {
					return tp;
				}
			}
		}

		return enm;
	}

}
