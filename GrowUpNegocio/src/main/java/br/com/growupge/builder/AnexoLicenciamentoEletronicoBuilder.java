package br.com.growupge.builder;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.dto.RestricaoDTO;
import br.com.growupge.exception.GrowUpException;

/**
 * @author Felipe
 *
 */
public class AnexoLicenciamentoEletronicoBuilder extends AbstractAnexoParser implements AnexoRestricaoBuilder {

	private static final String CONDICAO_LICENCIAMENTO = "VE�CULO J� LICENCIADO EM";
	private static final String LICENCIAMENTO = "Liberado para Licenciamento";
	private static final String PLACA = "Placa";

	private List<RestricaoDTO> restricoes;
	private PedidoVeiculoDTO pedido;
	
	/**
	 * @param is
	 * @param pattern
	 * @throws IOException
	 * @throws GrowUpException 
	 */
	public AnexoLicenciamentoEletronicoBuilder(InputStream is) throws IOException, GrowUpException {
		super(is, "(?mi)([^\\p{Z}\\p{C}]\\s?)+(\\t?\\s{2})(\\s?[^\\p{Z}\\p{C}]\\-?)+\\s?$");
	}
	
	/** � preciso colocar um espa�o no final de cada linha para a express�o regular funcionar corretamente.
	 *  
	 * @see br.com.growupge.builder.AbstractAnexoParser#lerArquivo(java.lang.String)
	@Override
	protected String lerArquivo(String nomeAnexo) throws IOException {
	   try(BufferedReader br = new BufferedReader(new InputStreamReader( getClass().getClassLoader().getResourceAsStream(nomeAnexo) ))) {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line)
	              .append(" ")
	              .append(System.lineSeparator());
	            line = br.readLine();
	        }
	        return sb.toString();
	    }
	}
	 */

	/* (non-Javadoc)
	 * @see br.com.growupge.builder.AbstractAnexoBuilder#readRecord(java.lang.String, java.lang.String)
	 */
	@Override
	void valorRegistroListener(String nome, Object valor, String texto, int start) {
		if (PLACA.equalsIgnoreCase(nome)) {
			pedido = createPedido(valor.toString());
		} else if (LICENCIAMENTO.equalsIgnoreCase(nome)
			|| nome.toUpperCase().startsWith(CONDICAO_LICENCIAMENTO)) { 
			
			final RestricaoDTO restricao = new RestricaoDTO(nome, valor);
			restricao.setPedido(pedido);
			
			build().add( restricao );
		}
	}

	/**
	 * @return
	 */
	public List<RestricaoDTO> build() {
		if (restricoes == null)
			restricoes = new ArrayList<>();
		return restricoes;
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.builder.AbstractAnexoBuilder#getRegexSeparador()
	 */
	@Override
	String getRegexSeparador() {
		return "\\s{3}";
	}
}
