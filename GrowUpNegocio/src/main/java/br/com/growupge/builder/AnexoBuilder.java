package br.com.growupge.builder;


/**
 * @author Felipe
 *
 */
public interface AnexoBuilder<T> {

	/**
	 * @return
	 */
	public abstract T build();

}