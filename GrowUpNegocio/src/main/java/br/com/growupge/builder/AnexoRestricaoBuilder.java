package br.com.growupge.builder;

import java.util.List;

import br.com.growupge.dto.RestricaoDTO;

public interface AnexoRestricaoBuilder extends AnexoBuilder<List<RestricaoDTO>> {

}
