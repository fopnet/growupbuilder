package br.com.growupge.builder;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.dto.RestricaoDTO;
import br.com.growupge.exception.GrowUpException;

/**
 * @author Felipe
 *
 */
public class AnexoDebitoRestricoesBuilder extends AbstractAnexoParser implements AnexoRestricaoBuilder {

	private static final String IPVA = "IPVA";
	private static final String TOTAL = "TOTAL";
	private static final String PLACA = "PLACA";
	private static final String RENAVAM = "RENAVAM";


	private List<RestricaoDTO> restricoes;
	private PedidoVeiculoDTO pedido;
	
	/**
	 * @param is
	 * @param pattern
	 * @throws IOException
	 * @throws GrowUpException 
	 */
	public AnexoDebitoRestricoesBuilder(InputStream is) throws IOException, GrowUpException {
		super(is, "(?mi)(\\x20?[^\\x09\\p{Z}\\p{C}])+(\\s:\\s)(\\x20?[^\\p{Z}\\p{C}])+");
	}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.builder.AbstractAnexoBuilder#readRecord(java.lang.String, java.lang.String)
	 */
	@Override
	void valorRegistroListener(String nome, Object valor, String texto, int start) {
			
			if (PLACA.equals(nome)) {
				pedido = createPedido(valor.toString());
			}

			if (RENAVAM.equals(nome)) {
				this.pedido.getVeiculo().setRenavam( valor.toString() );
			}
			
			Double dbl = null;
			if (TOTAL.equals(nome) || IPVA.equals(nome)) {
				dbl = converterValor2Double(valor);

				if (TOTAL.equals(nome))
					nome = TOTAL.concat(" MULTAS");

				final RestricaoDTO restricao = new RestricaoDTO(nome, dbl);
				restricao.setPedido(pedido);
				
				build().add( restricao );
			}
	}

	/**
	 * @return
	 */
	public List<RestricaoDTO> build() {
		if (restricoes == null)
			restricoes = new ArrayList<>();
		return restricoes;
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.builder.AbstractAnexoBuilder#getRegexSeparador()
	 */
	@Override
	String getRegexSeparador() {
		return "(\\s:\\s)";
	}

}
