package br.com.growupge.builder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.growupge.dto.RestricaoDTO;
import br.com.growupge.exception.GrowUpException;

/**
 * @author Felipe
 *
 */
public class AnexoDividaAtivaBuilder extends AbstractAnexoParser implements AnexoRestricaoBuilder {

	private final static Pattern patternRegistroPlaca = Pattern.compile("(\\D{2,3}\\d{4}\\x09\\d+)");
	
	/**
	 * @param is
	 * @param patternAnoExercicio
	 * @throws IOException
	 * @throws GrowUpException 
	 */
	public AnexoDividaAtivaBuilder(InputStream is) throws IOException, GrowUpException {
		super(is, "(Saldo:\\sR\\$\\s[\\d\\.\\,]+)", new AnexoDividaAtivaAnoExercicioBuilder());
	}
	
	@Override
	void valorRegistroListener(String nome, Object saldo, String texto, int start) throws GrowUpException  {
		// Chain primeiro
		if (nome.trim().toUpperCase().startsWith("SALDO")) {
			
			final RestricaoDTO restricao = new RestricaoDTO("SALDO", converterValor2Double(saldo));
			build().add(restricao);
			
			Matcher matcher = patternRegistroPlaca.matcher(texto);
			
			MatcherTemplate template = super.lerRegistro(matcher, texto, start);
			
			if (template.find()) {
				String registroPlaca = template.getMatcher().group().trim();
				
				registroListener(texto, registroPlaca, template.getMatcher().end(), "[\\x09]+");
			}
			else throw new IllegalArgumentException("N�o foi encontrado a placa do ve�culo:" + saldo.toString());
		} else if(super.parserChain != null)
			super.parserChain.valorRegistroListener(nome, saldo, texto, start);
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.builder.AbstractAnexoBuilder#getRegexSeparador()
	 */
	@Override
	String getRegexSeparador() {
		return "\\t";
	}
	
	/**
	 * @return
	 */
	public List<RestricaoDTO> build() {
		return ((AnexoDividaAtivaAnoExercicioBuilder)parserChain).build();
	}
}
