package br.com.growupge.builder;


import static org.apache.commons.lang3.StringUtils.defaultString;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.Transformer;

import br.com.growupge.business.Marca;
import br.com.growupge.business.Modelo;
import br.com.growupge.dto.MarcaDTO;
import br.com.growupge.dto.ModeloDTO;
import br.com.growupge.dto.ProprietarioDTO;
import br.com.growupge.dto.VeiculoDTO;
import br.com.growupge.enums.TipoCategoria;
import br.com.growupge.enums.TipoCombustivel;
import br.com.growupge.enums.TipoCor;
import br.com.growupge.enums.TipoVeiculo;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.BeanFactory;

import com.engine.MatcherWrapper;
import com.engine.Search;
import com.engine.TargetListener;

public class AnexoDadoVeicularBuilder extends AbstractAnexoParser implements AnexoBuilder<VeiculoDTO> {

	private List<RegistroAnexo> registros;
	private VeiculoDTO dto;

	public AnexoDadoVeicularBuilder(InputStream is) throws IOException, GrowUpException {
//		super( filename , "(\\w+\\.?\\s?)+=+>(\\s{0,3})(\\p{Alnum}+\\/?\\s{0,1},?\\.?)+");
		super( is , "(?mi)([^\\p{Z}\\p{C}]\\.?\\x20?)+(=*>)(\\s{1,3})(\\x20?[^\\p{Z}\\p{C}]+)+");
		this.dto = new VeiculoDTO();
	}
	
	protected String getValorRegistro(String nome) {
		int idx = -1;
		if ((idx  = getRegistros().indexOf(new RegistroAnexo(nome))) >= 0) {
			return getRegistros().get(idx).getValor();
		}
		return null;
	}

	protected Integer getValorRegistroComoInteiro(String nome) {
		String valor = getValorRegistro(nome);
		
		return valor != null ? Integer.valueOf(valor) : null;
	}
	
	String getRegexSeparador() {
		return "(=*)>";
	}
	
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Lertodos
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public AnexoDadoVeicularBuilder lerTodosCampos(){
		return this.lerPlaca()
					.lerRenavam()
					.lerChassi()
					.lerMarca()
					.lerModelo()
					.lerAnoModelo()
					.lerAnoFabricacao()
					.lerTipo()
					.lerCategoria()
					.lerCombustivel()
					.lerCor()
					.lerObs()
					.lerNome()
					.lerCPF();
	}
	
	public AnexoDadoVeicularBuilder lerPlaca() {
		String placa = getValorRegistro("PLACA");
		if (placa != null && placa.length() != 8) {
			placa = hyphenate(placa);
		 }
		dto.setCodigo(placa);
		return this;
	}

	public AnexoDadoVeicularBuilder lerRenavam() {
		dto.setRenavam(getValorRegistro("RENAVAM"));
		return this;
	}
	
	/**
	 * @return
	 */
	private AnexoDadoVeicularBuilder lerCombustivel() {
		dto = lerCombustivelImpl("COMBUST");
		if (dto.getCombustivel() == null)
			dto = lerCombustivelImpl("COMB");
		return this;
	}

	private VeiculoDTO lerCombustivelImpl(String hit) {
		final String descricao = getValorRegistro(hit);
		final TipoCombustivel enm = TipoCombustivel.getEnumByName(descricao);
		if (enm != null)
			dto.setCombustivel(enm.toCombustivelDTO());
		return dto;
	}

	public AnexoDadoVeicularBuilder lerChassi() {
		dto.setChassi(getValorRegistro("CHASSIS"));
		return this;
	}

	public AnexoDadoVeicularBuilder lerMarca() {
		String marcaModelo = getValorRegistro("MARCA");
		int idx = -1;
		if ( (idx = marcaModelo.indexOf("/")) > 0 )
			marcaModelo = marcaModelo.substring(0, idx);
		
		Search<MarcaDTO> search= new Search<MarcaDTO>();
		
		search.addTargetListener(new TargetListener<MatcherWrapper<MarcaDTO>>() {
			
			@Override
			@SuppressWarnings("unchecked")
			public List<MatcherWrapper<MarcaDTO>> search(List<String> hits)  {
				// Criar o parametro com o separador percentual para realizar o like.
				MarcaDTO dto = new MarcaDTO(joinPercent(hits));
				
				try {
					final List<MatcherWrapper<MarcaDTO>> marcas = new ArrayList<MatcherWrapper<MarcaDTO>>();
					// converter uma lista de marcas em MatcherWrapper's
					@SuppressWarnings("rawtypes")
					final List<MatcherWrapper<MarcaDTO>> transformedList = ListUtils.transformedList(marcas, new Transformer() {
						
						@Override
						public Object transform(Object input) {
							return new MatcherWrapper<MarcaDTO>("nome", (MarcaDTO) input);
						}
					});

					final Marca marca = BeanFactory.getContextualInstance(Marca.class);
					return marca.buscarTodas(dto, transformedList);
				} catch (GrowUpException e) {
					throw new RuntimeException(e);
				}
				
		}

		});
		
		final List<MarcaDTO> marcas = search.search(marcaModelo);
		
		if (!marcas.isEmpty())
			dto.setMarca( marcas.iterator().next() );
		
		return this;
	}
	
	public AnexoDadoVeicularBuilder lerModelo() {
		String marcaModelo = getValorRegistro("MARCA");
		int idx = -1;
		if ( (idx = marcaModelo.indexOf("/")) > 0 )
			marcaModelo = marcaModelo.substring(idx+1, marcaModelo.length());
		
		Search<ModeloDTO> search= new Search<ModeloDTO>();
		
		search.addTargetListener(new TargetListener<MatcherWrapper<ModeloDTO>>() {
			
			@Override
			@SuppressWarnings("unchecked")
			public List<MatcherWrapper<ModeloDTO>> search(List<String> hits)  {
				// Criar o parametro com o separador percentual para realizar o like.
				ModeloDTO dto = new ModeloDTO(joinPercent(hits));
				
				List<MatcherWrapper<ModeloDTO>> modelos = new ArrayList<MatcherWrapper<ModeloDTO>>();
				try {
					// converter uma lista de marcas em MatcherWrapper's
					@SuppressWarnings("rawtypes")
					final List<MatcherWrapper<ModeloDTO>> transformedList = ListUtils.transformedList(modelos, new Transformer() {
						
						@Override
						public Object transform(Object input) {
							return new MatcherWrapper<ModeloDTO>("nome", (ModeloDTO) input);
						}
					});
					
					final Modelo modelo = BeanFactory.getContextualInstance(Modelo.class);
					return modelo.buscarTodas(dto, transformedList);
				} catch (GrowUpException e) {
					throw new RuntimeException(e);
				}
			}
		});
		
		final List<ModeloDTO> modelos = search.search(marcaModelo);
		
		if (!modelos.isEmpty())
			dto.setModelo( modelos.iterator().next() );
		
		return this;
	}

	public AnexoDadoVeicularBuilder lerAnoModelo() {
		dto.setAnoModelo( getValorRegistroComoInteiro("MODELO") );
		if (dto.getAnoModelo() == null)
			dto.setAnoModelo( getValorRegistroComoInteiro("ANO MOD") );
		return this;
	}

	public AnexoDadoVeicularBuilder lerAnoFabricacao() {
		dto.setAnoFabric( getValorRegistroComoInteiro("FABRICACAO") );
		if (dto.getAnoFabric() == null)
			dto.setAnoFabric( getValorRegistroComoInteiro("ANO FAB") );
		
		return this;
	}

	public AnexoDadoVeicularBuilder lerTipo() {
		final String hit = getValorRegistro("TIPO");
		
		
		Search<TipoVeiculo> search= new Search<TipoVeiculo>();
		
		search.addTargetListener(new TargetListener<MatcherWrapper<TipoVeiculo>>() {

			@Override
			public List<MatcherWrapper<TipoVeiculo>> search(List<String> hits) {
				List<MatcherWrapper<TipoVeiculo>> retorno = new ArrayList<MatcherWrapper<TipoVeiculo>>();
				for (TipoVeiculo tipo  : TipoVeiculo.values()) {
					retorno.add(new MatcherWrapper<TipoVeiculo>("tipo", tipo));
				}
				return retorno;
			}
			
		});
		
		List<TipoVeiculo> retorno = search.search(hit);
		if (!retorno.isEmpty())
			dto.setTipo( retorno.iterator().next().toString() );
			
		return this;
	}
	
	public AnexoDadoVeicularBuilder lerCategoria() {
		final String hit = getValorRegistro("CATEG.");
		
		lerCategoriaImpl(hit);
		if (dto.getCategoria() == null)
			lerCategoriaImpl("CATEGORIA");
			
		return this;
	}

	private void lerCategoriaImpl(final String hit) {
		Search<TipoCategoria> search= new Search<TipoCategoria>();
		
		search.addTargetListener(new TargetListener<MatcherWrapper<TipoCategoria>>() {

			@Override
			public List<MatcherWrapper<TipoCategoria>> search(List<String> hits) {
				List<MatcherWrapper<TipoCategoria>> retorno = new ArrayList<MatcherWrapper<TipoCategoria>>();
				for (TipoCategoria tipo  : TipoCategoria.values()) {
					retorno.add(new MatcherWrapper<TipoCategoria>("name", tipo));
				}
				return retorno;
			}
			
		});
		
		List<TipoCategoria> retorno = search.search(hit);
		if (!retorno.isEmpty())
			dto.setCategoria( retorno.iterator().next().toString() );
	}

	public AnexoDadoVeicularBuilder lerCor() {
		final String hit = getValorRegistro("COR");
		
		Search<String> search = new Search<String>();
		search.addTargetListener(new TargetListener<MatcherWrapper<String>>() {

			@Override
			public List<MatcherWrapper<String>> search(List<String> hits) {
				List<MatcherWrapper<String>> retorno = new ArrayList<MatcherWrapper<String>>();
				for (TipoCor tipo  : TipoCor.values()) {
					retorno.add(new MatcherWrapper<String>(null, tipo.toString()));
				}
				return retorno;
			}
		});
		
		List<String> cores = search.search(hit);
		if  (!cores.isEmpty())
			dto.setCor( cores.iterator().next() );
		
		return this;
	}

	public AnexoDadoVeicularBuilder lerObs() {
		String obs = defaultString(dto.getObs());
		
		String hit = getValorRegistro("SITUACAO");
		obs = obs.concat( defaultString(hit) );
		
		hit = getValorRegistro("OBSERVACAO");
		obs = obs.concat( defaultString(hit) );
		
		dto.setObs( obs );
		
		return this;
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Dados do proprietario
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	private ProprietarioDTO getOrCreateProprietario() {
		if (dto.getProprietario() == null) dto.setProprietario(new ProprietarioDTO());
		return dto.getProprietario();
	}
	
	public AnexoDadoVeicularBuilder lerNome() {
		getOrCreateProprietario().setNome( getValorRegistro("NOME") );
		return this;
	}

	public AnexoDadoVeicularBuilder lerCPF() {
		getOrCreateProprietario().setCpfcnpj( getValorRegistro("CPF") );
		return this;
	}

	public VeiculoDTO build() {
		return dto;
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.builder.AbstractAnexoBuilder#readRecord(java.lang.String, java.lang.String)
	 */
	@Override
	void valorRegistroListener(String nome, Object valor, String texto, int start) {
		getRegistros().add( new RegistroAnexo( nome, valor.toString() ) );
	}

	/**
	 * @return
	 */
	protected List<RegistroAnexo> getRegistros() {
		if (registros == null)
			registros = new ArrayList<RegistroAnexo>();
		return registros;
	}


	
}
