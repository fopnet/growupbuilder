package br.com.growupge.builder;

import java.util.LinkedList;

import br.com.growupge.dto.RestricaoDTO;
import br.com.growupge.exception.GrowUpException;

class AnexoNomeDividaAtivaBuilder extends AbstractAnexoParser implements AnexoRestricaoBuilder {

	private LinkedList<RestricaoDTO> restricoes;
	
	protected AnexoNomeDividaAtivaBuilder() {
		super(null);
	}
	
	@Override
	void valorRegistroListener(String nome, Object valor, String texto, int start) throws GrowUpException {

		//pegar a ultimo restricao e setar o nome da divida com o ano do exercicio
		RestricaoDTO ultimaRestricao = build().peekLast();
		ultimaRestricao.setNome("D�vida Ativa exerc�cio " + valor.toString());

	}
	
	@Override
	String getRegexSeparador() {
		return "[\\x09]+";
	}

	public LinkedList<RestricaoDTO> build() {
		if (restricoes == null)
			restricoes = new LinkedList<RestricaoDTO>( );
		return restricoes;
	}


}