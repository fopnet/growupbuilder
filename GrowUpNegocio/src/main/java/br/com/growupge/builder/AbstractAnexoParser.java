package br.com.growupge.builder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;

public abstract class AbstractAnexoParser {
	
	protected AbstractAnexoParser parserChain = null;
	
/*	protected AbstractAnexoParser(String nomeAnexo, String pattern) throws IOException, GrowUpException {
		lerRegistros( lerArquivo(nomeAnexo) , pattern);
	}
	protected String lerArquivo(String nomeAnexo) throws IOException {
		try (InputStream is = IOUtils.toInputStream(nomeAnexo)) {
			return lerArquivo(is);
		} catch (IOException e) {
			throw e;
		}
	}
 */
	
	protected AbstractAnexoParser(AbstractAnexoParser parser) {
		this.parserChain = parser;
	}
	
	public AbstractAnexoParser(InputStream is, String pattern, AbstractAnexoParser parser) throws IOException, GrowUpException {
		this(parser);
		lerRegistros( lerArquivo(is) , pattern);
	}
	
	protected AbstractAnexoParser(InputStream is, String pattern) throws IOException, GrowUpException {
		lerRegistros( lerArquivo(is) , pattern);
	}

	protected String lerArquivo(InputStream is) throws IOException {
		try {
			return IOUtils.toString(is) ;
		} catch (IOException e) {
			throw e;
		} finally {
			is.close();
		}
	}
	
	protected void lerRegistros(String texto, String regexRegistro) throws IOException, GrowUpException {
		
		Pattern pattern = Pattern.compile(regexRegistro);
		Matcher matcher = pattern.matcher(texto);
		
		MatcherTemplate template = lerRegistro(matcher, texto);
		while ( template.find()) {
			String registro = template.getMatcher().group();
			
			registroListener(texto, registro, template.getMatcher().start(), getRegexSeparador());
			
		}
	}

	protected MatcherTemplate lerRegistro(Matcher matcher, String texto) {
		return lerRegistro(matcher, texto, null);
	}
	
	public interface MatcherTemplate {
		public boolean find();
		public Matcher getMatcher();
	}
	
	protected MatcherTemplate lerRegistro(final Matcher pMatcher, final String texto, final Integer start) {
		MatcherTemplate template = new MatcherTemplate() {
			private Matcher matcher = pMatcher;
			
			@Override
			public boolean find() {
				return (start == null) ? matcher.find() : matcher.find(start);
			}

			@Override
			public Matcher getMatcher() {
				return matcher;
			}
		};

		return template;
	}
	
	protected void registroListener(String texto, String registro, int start, String regexSeparador) throws GrowUpException {
		String[] regs = registro.split(regexSeparador);
		
		if (regs.length == 2) {
			valorRegistroListener(regs[0].trim(), regs[1].trim(), texto, start);
		} else 
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, new IllegalArgumentException(String.format("Registro \"%s\" com formato incorreto", registro)) );
	}
	
	abstract String getRegexSeparador() ;

	abstract void valorRegistroListener(String nome, Object valor, String texto, int start) throws  GrowUpException;

	protected double converterValor2Double(Object valor) {
		valor = valor.toString().replaceAll("\\.", "")
								 .replaceAll("\\*", "")
								 .replace("R$", "")
								 .replaceAll(",", ".");
		return Double.valueOf(valor.toString());
	}

	protected String joinPercent(List<String> hits) {
		if (hits.isEmpty())
			return "";
		else 
			return StringUtils.join(hits.toArray(), "%").concat("%");
	}

	/** Colocar o hifen na placa. 
	 * @param placa
	 * @return
	 */
	protected String hyphenate(String placa) {
		placa = placa.trim().toUpperCase();
		String letras = StringUtils.removePattern(placa, "\\-?\\d{4}$");
		String numeros = StringUtils.removePattern(placa, "^(\\D{2,3})\\-?");
		placa = letras.concat("-").concat(numeros);
		return placa;
	}
	
	protected PedidoVeiculoDTO createPedido(String placa) {
		PedidoVeiculoDTO pedido = new PedidoVeiculoDTO();
		pedido.getVeiculo().setCodigo( hyphenate(placa.toString()) );
		return pedido;
	}

}
