package br.com.growupge.builder;

import org.apache.commons.lang3.builder.EqualsBuilder;


class RegistroAnexo {
	private String nome;
	private String valor;
	
	RegistroAnexo(String nome) {
		this.nome = nome;
	}
	
	RegistroAnexo(String nome, String valor) {
		this(nome.trim());
		this.valor = valor.trim();
	}

	public String getNome() {
		return nome;
	}

	public String getValor() {
		return valor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		RegistroAnexo rg = (RegistroAnexo) obj;
		return new EqualsBuilder().append(nome, rg.nome).isEquals();
	}

	public String toString() {
		return nome;
	}
	
	
}