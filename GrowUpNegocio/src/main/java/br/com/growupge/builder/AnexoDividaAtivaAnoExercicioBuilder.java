package br.com.growupge.builder;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.dto.RestricaoDTO;
import br.com.growupge.exception.GrowUpException;

class AnexoDividaAtivaAnoExercicioBuilder extends AbstractAnexoParser implements AnexoRestricaoBuilder {

	private final static Pattern patternAnoExercicio = Pattern.compile("(\\x09\\d{4}\\x09\\d{4}\\x09)");
	private PedidoVeiculoDTO pedido;
	
	protected AnexoDividaAtivaAnoExercicioBuilder() {
		super(new AnexoNomeDividaAtivaBuilder());
	}
	
	@Override
	void valorRegistroListener(String nome, Object placa, String texto, int start) throws GrowUpException {

		if (nome.trim().matches("^(\\D{2,3}\\d{4})") ) { //Placa
			pedido = createPedido(nome);

			Matcher matcher = patternAnoExercicio.matcher(texto);

			MatcherTemplate template = super.lerRegistro(matcher, texto, start);

			if (template.find()) {
				String registroAno = template.getMatcher().group().trim();

				final RestricaoDTO restricao = build().peekLast();
				restricao.setPedido(pedido);

				registroListener(texto, registroAno, template.getMatcher().end(), getRegexSeparador());
			}
			else throw new IllegalArgumentException("N�o foi encontrado o ano de exerc�cio para o saldo:" + placa.toString());
		} else if(super.parserChain != null)
			super.parserChain.valorRegistroListener(nome, placa, texto, start);

	}

	@Override
	String getRegexSeparador() {
		return "[\\x09]+";
	}

	@Override
	public LinkedList<RestricaoDTO> build() {
		return ((AnexoNomeDividaAtivaBuilder)parserChain).build();
	}


}