package br.com.growupge.builder;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;

import br.com.growupge.dto.RestricaoDTO;
import br.com.growupge.exception.GrowUpException;

/**
 * @author Felipe
 *
 */
public class AnexoMultasBuilder extends AbstractAnexoParser implements AnexoRestricaoBuilder {

	private LinkedList<RestricaoDTO> restricoes;
	private StringBuilder nomeBuilder = null;
	
	/**
	 * @param is
	 * @param pattern
	 * @throws IOException
	 * @throws GrowUpException 
	 */
	public AnexoMultasBuilder(InputStream is) throws IOException, GrowUpException {
		super(is, "(?mi)(\\x20?[^\\p{Z}\\p{C}])+\\x09(\\s?[^\\p{Z}\\p{C}])+");
	}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.builder.AbstractAnexoBuilder#readRecord(java.lang.String, java.lang.String)
	 */
	@Override
	void valorRegistroListener(String nome, Object valor, String texto, int start) {
		lerDado( nome, (String) valor );
	}

	/**
	 * @param valor
	 */
	private void lerDado(String nome, String valor) {
		nome = nome.trim();
		switch (nome) {
			case "Receita":
			case "�rg�o Autuador":
				nomeBuilder = new StringBuilder();
				nomeBuilder.append("Org�o Autuador: ")
							.append(valor)
							.append("\n");
				break;
			case "Munic�pio":
			case "Mun. da Infra��o":
				nomeBuilder.append("Munic�pio: ")
							.append(valor)
							.append("\n");
				break;
			case "Local":
				nomeBuilder.append("Local: ")
							.append(valor)
							.append("\n");
				break;
			case "Data":
				nomeBuilder.append("Data/Hora: ")
							.append(valor);
				break;
			case "Hora":
				nomeBuilder.append(valor)
							.append("\n");
				break;
			case "Enquadramento":
			case "Infra��o":
				nomeBuilder.append("Infra��o: ")
				.append(valor)
				.append("\n");
				break;
			case "N� do A.I.T.":
				nomeBuilder.append("N� do A.I.T.: ")
							.append(valor)
							.append("\n");
				break;
			case "Valor":
			case "Valor da Infra��o":
				build().add( new RestricaoDTO(nomeBuilder.toString(), converterValor2Double(valor)) );
				break;
		}
		
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.builder.AnexoRestricaoBuilder#build()
	 */
	@Override
	public LinkedList<RestricaoDTO> build() {
		if (restricoes == null)
			restricoes = new LinkedList<RestricaoDTO>();
		return restricoes;
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.builder.AbstractAnexoBuilder#getRegexSeparador()
	 */
	@Override
	String getRegexSeparador() {
		return "(\\x09)";
	}

}
