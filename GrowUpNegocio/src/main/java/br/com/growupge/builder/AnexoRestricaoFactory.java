package br.com.growupge.builder;

import java.io.InputStream;

import br.com.growupge.exception.GrowUpException;
import br.com.growupge.utility.Reflection;

/**
 * @author Felipe
 *
 */
public class AnexoRestricaoFactory {

	public static AnexoRestricaoBuilder getRestricaoBuilder(String classname, InputStream is) throws GrowUpException {
		String pckName = AnexoRestricaoFactory.class.getPackage().getName();
		AnexoRestricaoBuilder builder = Reflection.getNewInstance(pckName, classname, is);
		return builder;
	}
}
