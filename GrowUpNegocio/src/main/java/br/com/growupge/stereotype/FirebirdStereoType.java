/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 25/08/2016
 *
 * Historico de Modifica��o:
 * =========================
 * 25/08/2016 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.stereotype;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.METHOD;
//import javax.inject.Qualifier;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Stereotype;


/**
 * @author Felipe
 *
 */
/*
@Qualifier
@Retention(RUNTIME)
@Target({METHOD, FIELD, PARAMETER, TYPE})
@BindingType
*/
@Stereotype
@Retention(RUNTIME)
@Target({METHOD, FIELD, PARAMETER, TYPE})

@Alternative
public @interface FirebirdStereoType {
}
