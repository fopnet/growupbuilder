/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.fbirdDAO;

import static br.com.growupge.utility.Reflection.isCodigoValido;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

import org.firebirdsql.jdbc.field.TypeConversionException;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.dto.RestricaoDTO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.RestricaoDAO;
import br.com.growupge.interfacesdao.SequenciaDAO;

/**
 * Classe DAO para gerenciar informa��es pertinentes as Restricao
 * 
 * @author Felipe
 * 
 */
public class FbirdRestricaoDAO extends FbirdDAO<RestricaoDTO> implements RestricaoDAO {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            objeto de f�brica.
	 */
	public FbirdRestricaoDAO(final DAOFactory daofactory) {
		super(daofactory);

		sql.append("select");
		sql.append("    RESTRICAO.REST_CD_RESTRICAO,");
		sql.append("    REST_NM_RESTRICAO,");
		sql.append("    REST_CD_MOEDA,");
		sql.append("    REST_VL_VALOR,");
		sql.append("    RESTRICAO.PEDI_CD_PEDIDO ");
		sql.append(" from ");
		sql.append("    RESTRICAO ");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final RestricaoDTO delete(final Connection conn, final RestricaoDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;

		try {
			sql.append("delete from RESTRICAO ");
			sql.append("where REST_CD_RESTRICAO = ?");
			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setLong(1, dto.getCodigo());
			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * @param connection
	 * @param dto
	 * @return 
	 */
	@Override
	public PedidoVeiculoDTO delete(Connection conn, PedidoVeiculoDTO dto) throws GrowUpDAOException {
		
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;
		
		try {
			sql.append("delete from RESTRICAO ");
			sql.append("where PEDI_CD_PEDIDO = ?");
			pstmt = conn.prepareStatement(sql.toString());
			
			pstmt.setLong(1, dto.getCodigo());
			pstmt.executeUpdate();
			
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final RestricaoDTO insert(final Connection conn, final RestricaoDTO dto)
			throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuilder sql = new StringBuilder();

		try {
			
			SequenciaDAO seq = daoFactory.getSequenciaDAO();
			dto.setCodigo(seq.getProximaSequencia("GEN_REST_CD_RESTRICAO"));
			
			sql.append("INSERT INTO RESTRICAO (");
			sql.append("    REST_CD_RESTRICAO,");
			sql.append("    REST_NM_RESTRICAO,");
			sql.append("    REST_CD_MOEDA,");
			sql.append("    REST_VL_VALOR,");
			sql.append("    RESTRICAO.PEDI_CD_PEDIDO )");
			sql.append(" values ");
			sql.append(" (?,?,?,?,?) ");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			pstmt.setLong(inc++, dto.getCodigo());
			pstmt.setString(inc++, dto.getNome());
			pstmt.setString(inc++, dto.getMoeda());
			pstmt.setString(inc++, Objects.toString(dto.getValor(), null));
			pstmt.setLong(inc++, dto.getPedido().getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final RestricaoDTO update(final Connection conn, final RestricaoDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;
		try {

			sql.append("UPDATE RESTRICAO SET ");
			sql.append("    REST_NM_RESTRICAO =?,");
			sql.append("    REST_CD_MOEDA= ?,");
			sql.append("    REST_VL_VALOR=?,");
			sql.append("    PEDI_CD_PEDIDO=? ");
			sql.append(" WHERE REST_CD_RESTRICAO = ? ");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;

			pstmt.setString(inc++, dto.getNome());
			pstmt.setString(inc++, dto.getMoeda());
			pstmt.setString(inc++, dto.getValor().toString());
			pstmt.setLong(inc++, dto.getPedido().getCodigo());
			pstmt.setLong(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final RestricaoDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {

		RestricaoDTO dto = null;

		try {
			PedidoVeiculoDTO pedido = new PedidoVeiculoDTO(rs.getLong("PEDI_CD_PEDIDO"));
			dto = new RestricaoDTO(pedido);

			dto.setCodigo( rs.getLong("REST_CD_RESTRICAO") );
			dto.setNome( rs.getString("REST_NM_RESTRICAO") );
			dto.setMoeda( rs.getString("REST_CD_MOEDA") );
			try {
				dto.setValor( rs.getDouble("REST_VL_VALOR") );
			} catch (TypeConversionException e) {
				dto.setValor( rs.getString("REST_VL_VALOR") );
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final RestricaoDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			param = this.setParametro(param, dto);

			if (param.getParametros().isEmpty()) {
				param.setParam("RESTRICAO.REST_CD_RESTRICAO", ParametroFactory.CODIGO_INEXISTENTE);
			}
		}

		param.setOrdem("REST_CD_RESTRICAO", TipoOrdemDAO.ASC);

		return param;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.fbirdDAO.FbirdDAO#setParametro(br.com.growupge.factory.ParametroFactory, java.lang.Object)
	 */
	@Override
	protected ParametroFactory setParametro(final ParametroFactory param, final RestricaoDTO dto)
			throws GrowUpDAOException {
		if (dto.getCodigo() != null) {
			param.setParam("RESTRICAO.REST_CD_RESTRICAO", dto.getCodigo());
		} else {
			if (isCodigoValido(dto.getPedido()))
				param.setParam("PEDI_CD_PEDIDO", dto.getPedido().getCodigo());
			
			param.setParam("REST_NM_RESTRICAO", dto.getNome());
		}
		return param;
	}
	
	
	

}
