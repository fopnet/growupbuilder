/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.fbirdDAO;

import static br.com.growupge.utility.Reflection.isCodigoValido;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.ArquivoDTO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.interfacesdao.SequenciaDAO;

/**
 * Classe DAO para gerenciar informa��es pertinentes a Empresa.
 * 
 * @author Felipe
 * 
 */
public class FbirdArquivoDAO extends FbirdDAO<ArquivoDTO> implements DAO<ArquivoDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            objeto de f�brica.
	 */
	public FbirdArquivoDAO(final DAOFactory daofactory) {
		super(daofactory);

		sql.append("select");
		sql.append("    ARQUIVO.ARQU_CD_ARQUIVO,");
		sql.append("    ARQU_NM_ARQUIVO ");
		sql.append(" from ");
		sql.append("    ARQUIVO");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final ArquivoDTO delete(final Connection conn, final ArquivoDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;
		try {
			sql.append("delete from ARQUIVO ");
			sql.append("where ARQU_CD_ARQUIVO = ?");
			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setLong(1, dto.getCodigo());
			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final ArquivoDTO insert(final Connection conn, final ArquivoDTO dto)
			throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuilder sql = new StringBuilder();

		try {
			SequenciaDAO seq = daoFactory.getSequenciaDAO();
			dto.setCodigo(seq.getProximaSequencia("GEN_ARQU_CD_ARQUIVO"));

			sql.append("INSERT INTO ARQUIVO (");
			sql.append("    ARQU_CD_ARQUIVO ,");
			sql.append("    ARQU_NM_ARQUIVO )");
			sql.append(" values ");
			sql.append(" (?,?) ");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			pstmt.setLong(inc++, dto.getCodigo());
			pstmt.setString(inc++, dto.getNome());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final ArquivoDTO update(final Connection conn, final ArquivoDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;
		try {

			sql.append("UPDATE arquivo SET ");
			sql.append("    ARQU_NM_ARQUIVO =?");
			sql.append(" WHERE ARQU_CD_ARQUIVO = ? ");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;

			pstmt.setString(inc++, dto.getNome());
			pstmt.setLong(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final ArquivoDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {

		ArquivoDTO dto = null;

		try {
			dto = new ArquivoDTO();

			dto.setCodigo( rs.getLong("ARQU_CD_ARQUIVO") );
			dto.setNome( rs.getString("ARQU_NM_ARQUIVO") );

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final ArquivoDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			param = this.setParametro(param, dto);

			if (param.getParametros().isEmpty()) {
				param.setParam("ARQUIVO.ARQU_CD_ARQUIVO", ParametroFactory.CODIGO_INEXISTENTE);
			}
		}

		param.setOrdem("ARQU_CD_ARQUIVO", TipoOrdemDAO.ASC);

		return param;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.fbirdDAO.FbirdDAO#setParametro(br.com.growupge.factory.ParametroFactory, java.lang.Object)
	 */
	@Override
	protected ParametroFactory setParametro(final ParametroFactory param, final ArquivoDTO dto)
			throws GrowUpDAOException {
		if (isCodigoValido(dto)) {
			param.setParam("ARQUIVO.ARQU_CD_ARQUIVO", dto.getCodigo());
		} else {
			param.setParam("ARQU_NM_ARQUIVO", dto.getNome());
		}
		return param;
	}

}
