/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 05/01/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 05/01/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.fbirdDAO;

import static br.com.growupge.utility.Reflection.isCodigoValido;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.CombustivelDTO;
import br.com.growupge.dto.MarcaDTO;
import br.com.growupge.dto.ModeloDTO;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.dto.VeiculoDTO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;

/**
 * Classe DAO para gerenciar informa��es pertinentes a perfis.
 * 
 * @author Felipe
 * 
 */
public class FbirdVeiculoDAO extends FbirdDAO<VeiculoDTO> implements DAO<VeiculoDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            F�brica de DAO.
	 */
	public FbirdVeiculoDAO(final DAOFactory daofactory) {
		super(daofactory);

		this.sql.append(" select ");
		this.sql.append("  VEICULO.VEIC_CD_PLACA, ");
		this.sql.append("  VEIC_CD_RENAVAM, ");
		this.sql.append("  VEIC_CD_CHASSI, ");
		this.sql.append("  VEIC_CD_ANO_FABRIC, ");
		this.sql.append("  VEIC_CD_ANO_MODELO, ");
		this.sql.append("  VEIC_IN_COMBUSTIVEL, ");
		this.sql.append("  VEIC_DH_CRIACAO, ");
		this.sql.append("  VEIC_DH_ALTERACAO, ");
		this.sql.append("  USUA_CD_USUARIO_CRIACAO, ");
		this.sql.append("  USUA_CD_USUARIO_ALTERACAO, ");
		this.sql.append("  VEIC_CD_RENAVAM, ");
		this.sql.append("  VEIC_CD_MOTOR, ");
		this.sql.append("  VEIC_CD_TIPO, ");
		this.sql.append("  VEIC_CD_CATEGORIA, ");
		this.sql.append("  VEIC_NM_COR, ");
		this.sql.append("  VEIC_ULTIMO_ANO_LICENCIAMENTO, ");
		this.sql.append("  VEIC_ULTIMA_TRANSACAO, ");
		this.sql.append("  VEIC_TX_OBS, ");
		this.sql.append("  VEIC_CD_SRF, ");
		
		this.sql.append("  PROPRIETARIO.PROP_CD_PROPRIETARIO,");
		this.sql.append("  PROP_NM_PROPRIETARIO, ");
		this.sql.append("  PROP_CD_CPF_CGC, ");
		this.sql.append("  PROP_DH_CADASTRO, ");
		this.sql.append("  PROP_DH_ALTERACAO,");
		this.sql.append("  PROP_TX_ENDERECO, ");
		this.sql.append("  PROP_NM_BAIRRO, ");
		this.sql.append("  PROP_NM_CIDADE, ");
		this.sql.append("  PROP_CD_UF, ");
		this.sql.append("  PROP_CD_CEP, ");
		this.sql.append("  PROP_NR_TEL, ");
		this.sql.append("  PROP_NR_CELULAR, ");
		this.sql.append("  PROP_TX_OBS, ");

		this.sql.append("  MODELO_FIPE.MODE_CD_MODELO, ");
		this.sql.append("  MODE_NM_MODELO, ");
		
		this.sql.append("  MARCA_FIPE.MARC_CD_MARCA, ");
		this.sql.append("  MARC_NM_MARCA ");
		
		this.sql.append(" from VEICULO ");
		this.sql.append(" LEFT JOIN PROPRIETARIO ON ");
		this.sql.append("   PROPRIETARIO.PROP_CD_PROPRIETARIO = ");
		this.sql.append("   VEICULO.PROP_CD_PROPRIETARIO");
		this.sql.append(" LEFT JOIN MODELO_FIPE ON ");
		this.sql.append("   MODELO_FIPE.MODE_CD_MODELO = ");
		this.sql.append("   VEICULO.MODE_CD_MODELO");
		this.sql.append(" LEFT JOIN MARCA_FIPE ON ");
		this.sql.append("   MODELO_FIPE.MARC_CD_MARCA = ");
		this.sql.append("   MARCA_FIPE.MARC_CD_MARCA");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.FbirdDAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final VeiculoDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {
		VeiculoDTO dto = null;
		try {
			dto = new VeiculoDTO();
			dto.setCodigo(rs.getString("VEIC_CD_PLACA"));
			dto.setRenavam(rs.getString("VEIC_CD_RENAVAM"));
			dto.setChassi(rs.getString("VEIC_CD_CHASSI"));
			dto.setRenavam(rs.getString("VEIC_CD_RENAVAM"));
			dto.setAnoFabric(rs.getInt("VEIC_CD_ANO_FABRIC"));
			dto.setAnoModelo(rs.getInt("VEIC_CD_ANO_MODELO"));
			dto.setCombustivel(new CombustivelDTO(rs.getString("VEIC_IN_COMBUSTIVEL")));
			
			if (isEager) {
				// novos dados
				dto.setRenavam(rs.getString("VEIC_CD_RENAVAM"));
				dto.setMotor(rs.getString("VEIC_CD_MOTOR"));
				dto.setTipo(rs.getString("VEIC_CD_TIPO"));
				dto.setCategoria(rs.getString("VEIC_CD_CATEGORIA"));
				dto.setCor(rs.getString("VEIC_NM_COR"));
				dto.setUltimoAnoLicenciamento(rs.getInt("VEIC_ULTIMO_ANO_LICENCIAMENTO"));
				dto.setUltimaTransacao(rs.getTimestamp("VEIC_ULTIMA_TRANSACAO"));
				dto.setObs(rs.getString("VEIC_TX_OBS"));
				dto.setSrf(rs.getString("VEIC_CD_SRF"));
			}
			
			
			if (rs.getLong("PROP_CD_PROPRIETARIO") != 0) {
				dto.setProprietario(this.daoFactory.getProprietarioDAO().getDTO(rs, false));
			}
			
			if (rs.getString("MODE_CD_MODELO")!= null) {
				dto.setModelo(new ModeloDTO());
				dto.getModelo().setCodigo(rs.getLong("MODE_CD_MODELO"));
				dto.getModelo().setNome(rs.getString("MODE_NM_MODELO"));
			}

			if (rs.getString("MARC_CD_MARCA")!= null) {
				dto.setMarca(new MarcaDTO());
				dto.getMarca().setCodigo( rs.getLong("MARC_CD_MARCA") );
				dto.getMarca().setNome( rs.getString("MARC_NM_MARCA") );
			}
			
			dto.setDataCadastro(rs.getTimestamp("VEIC_DH_CRIACAO"));
			dto.setUsuarioCadastro(new UsuarioDTO());
			dto.getUsuarioCadastro().setCodigo(rs.getString("USUA_CD_USUARIO_CRIACAO"));

			if (rs.getTimestamp("VEIC_DH_ALTERACAO") != null) {
				dto.setDataAlteracao(rs.getTimestamp("VEIC_DH_ALTERACAO"));
				dto.setUsuarioAlteracao(new UsuarioDTO());
				dto.getUsuarioAlteracao().setCodigo(rs.getString("USUA_CD_USUARIO_ALTERACAO"));
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final VeiculoDTO delete(final Connection conn, final VeiculoDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append("delete from VEICULO ");
			sql.append("where VEIC_CD_PLACA = ?");
			pstmt = conn.prepareStatement(sql.toString());

			/* Limpa valores antigos dos par�metros. */
			pstmt.clearParameters();
			int inc = 1;
			pstmt.setString(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.REGISTRO_EXCLUIDO_PREVIAMENTE);
			}
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}

		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final VeiculoDTO insert(final Connection conn, final VeiculoDTO dto)
			throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {

			sql.append("insert into VEICULO (");
			sql.append("  VEIC_CD_PLACA,");
			sql.append("  VEIC_CD_RENAVAM, ");
			sql.append("  VEIC_CD_CHASSI, ");
			sql.append("  PROP_CD_PROPRIETARIO, ");
			sql.append("  VEIC_CD_ANO_FABRIC, ");
			sql.append("  VEIC_CD_ANO_MODELO, ");
			sql.append("  MARC_CD_MARCA, ");
			sql.append("  MODE_CD_MODELO, ");
			sql.append("  VEIC_IN_COMBUSTIVEL, ");
			sql.append("  VEIC_DH_CRIACAO, ");
			sql.append("  VEIC_DH_ALTERACAO, ");
			sql.append("  USUA_CD_USUARIO_CRIACAO, ");
			sql.append("  VEIC_CD_MOTOR, ");
			sql.append("  VEIC_CD_TIPO, ");
			sql.append("  VEIC_CD_CATEGORIA, ");
			sql.append("  VEIC_NM_COR, ");
			sql.append("  VEIC_ULTIMO_ANO_LICENCIAMENTO, ");
			sql.append("  VEIC_ULTIMA_TRANSACAO, ");
			sql.append("  VEIC_TX_OBS, ");
			sql.append("  VEIC_CD_SRF ");
			sql.append(" ) values (?,?,?,?,?,?,?,?,?, CURRENT_TIMESTAMP,NULL,?,?,?,?,?,?,?,?,? )");
			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			pstmt.setString(inc++, dto.getCodigo());
			pstmt.setString(inc++, dto.getRenavam());
			pstmt.setString(inc++, dto.getChassi());
			if (isCodigoValido(dto.getProprietario())) {
				pstmt.setLong(inc++, dto.getProprietario().getCodigo());
			} else {
				pstmt.setNull(inc++, Types.BIGINT);
			}

			if (dto.getAnoFabric() != null) {
				pstmt.setInt(inc++, dto.getAnoFabric());
			} else {
				pstmt.setNull(inc++, Types.BIGINT);
			}
			if (dto.getAnoModelo() != null) {
				pstmt.setInt(inc++, dto.getAnoModelo());
			} else {
				pstmt.setNull(inc++, Types.BIGINT);
			}
			if (!isCodigoValido(dto.getMarca())) {
				pstmt.setNull(inc++, Types.BIGINT);
			} else {
				pstmt.setLong(inc++, dto.getMarca().getCodigo());
			}
			if (!isCodigoValido(dto.getModelo())) {
				pstmt.setNull(inc++, Types.BIGINT);
			} else {
				pstmt.setLong(inc++, dto.getModelo().getCodigo());
			}
			if (!isCodigoValido(dto.getCombustivel())) {
				pstmt.setNull(inc++, Types.BIGINT);
			} else {
				pstmt.setString(inc++, dto.getCombustivel().getCodigo());
			}
			pstmt.setString(inc++, dto.getUsuarioCadastro().getCodigo());
			pstmt.setString(inc++, dto.getMotor());
			pstmt.setString(inc++, dto.getTipo());
			pstmt.setString(inc++, dto.getCategoria());
			pstmt.setString(inc++, dto.getCor());
			if (dto.getUltimoAnoLicenciamento() != null) {
				pstmt.setInt(inc++, dto.getUltimoAnoLicenciamento());
			} else { 
				pstmt.setNull(inc++, Types.BIGINT);
			}
			if (dto.getUltimaTransacao() == null)
				pstmt.setNull(inc++, Types.TIMESTAMP);
			else
			pstmt.setTimestamp(inc++, new Timestamp(dto.getUltimaTransacao().getTime()));
			pstmt.setString(inc++, dto.getObs());
			pstmt.setString(inc++, dto.getSrf());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final VeiculoDTO update(final Connection conn, final VeiculoDTO dto)
			throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {

			sql.append("UPDATE VEICULO SET ");
			sql.append("  VEIC_CD_RENAVAM=?, ");
			sql.append("  VEIC_CD_CHASSI=?, ");
			sql.append("  PROP_CD_PROPRIETARIO=?, ");
			sql.append("  VEIC_CD_ANO_FABRIC=?, ");
			sql.append("  VEIC_CD_ANO_MODELO=?, ");
			sql.append("  MARC_CD_MARCA=?, ");
			sql.append("  MODE_CD_MODELO=?, ");
			sql.append("  VEIC_IN_COMBUSTIVEL=?, ");
			sql.append("  VEIC_DH_ALTERACAO=CURRENT_TIMESTAMP, ");
			sql.append("  USUA_CD_USUARIO_ALTERACAO=?, ");
			sql.append("  VEIC_CD_MOTOR=?, ");
			sql.append("  VEIC_CD_TIPO=?, ");
			sql.append("  VEIC_CD_CATEGORIA=?, ");
			sql.append("  VEIC_NM_COR=?, ");
			sql.append("  VEIC_ULTIMO_ANO_LICENCIAMENTO=?, ");
			sql.append("  VEIC_ULTIMA_TRANSACAO=?, ");
			sql.append("  VEIC_TX_OBS=?, ");
			sql.append("  VEIC_CD_SRF=? ");
			sql.append(" WHERE VEIC_CD_PLACA = ?");
			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();

			pstmt.setString(inc++, dto.getRenavam());
			pstmt.setString(inc++, dto.getChassi());
			if (isCodigoValido(dto.getProprietario())) {
				pstmt.setLong(inc++, dto.getProprietario().getCodigo());
			} else {
				pstmt.setNull(inc++, Types.BIGINT);
			}
			pstmt.setInt(inc++, dto.getAnoFabric());
			pstmt.setInt(inc++, dto.getAnoModelo());
			if (!isCodigoValido( dto.getMarca() ))
				pstmt.setNull(inc++, Types.INTEGER);
			else
				pstmt.setLong(inc++, dto.getMarca().getCodigo());

			if (!isCodigoValido( dto.getModelo() ))
				pstmt.setNull(inc++, Types.INTEGER);
			else 
				pstmt.setLong(inc++, dto.getModelo().getCodigo());
			
			if (dto.getCombustivel() == null || isBlank(dto.getCombustivel().getCodigo()))
				pstmt.setString(inc++, null);
			else 
				pstmt.setString(inc++, dto.getCombustivel().getCodigo());
			
			pstmt.setString(inc++, dto.getUsuarioAlteracao().getCodigo());
			//novos dados
			pstmt.setString(inc++, dto.getMotor());
			pstmt.setString(inc++, dto.getTipo());
			pstmt.setString(inc++, dto.getCategoria());
			pstmt.setString(inc++, dto.getCor());
			pstmt.setInt(inc++, dto.getUltimoAnoLicenciamento());
			pstmt.setTimestamp(inc++, new Timestamp(dto.getUltimaTransacao().getTime()));
			pstmt.setString(inc++, dto.getObs());
			pstmt.setString(inc++, dto.getSrf());
			
			pstmt.setString(inc++, dto.getCodigo());
			
			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.FbirdDAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final VeiculoDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			param = this.setParametro(param, dto);

			if (param.getParametros().isEmpty()) {
				param.setParam("VEICULO.VEIC_CD_PLACA",
						ParametroFactory.CODIGO_INEXISTENTE);
			}
		}

		return param;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.fbirdDAO.FbirdDAO#setParametro(br.com.growupge.factory.ParametroFactory,
	 *      java.lang.Object)
	 */
	@Override
	protected ParametroFactory setParametro(final ParametroFactory param, final VeiculoDTO dto)
			throws GrowUpDAOException {
		if (isNotBlank( dto.getCodigo() )) {
			param.setParam("UPPER(VEICULO.VEIC_CD_PLACA)", dto.getCodigo());// Campo precisa estar Colacao UNICODE_CI, mas nao precisar do upper 
		} else {
			
			if (dto.getProprietario() != null) {
				param.setParam("PROP_NM_PROPRIETARIO", dto.getProprietario().getNome());
				param.setParam("PROP_CD_CPF_CGC", dto.getProprietario().getCpfcnpj());
			}
			param.setParam("VEIC_CD_CHASSI", dto.getChassi());
			param.setParam("VEIC_CD_RENAVAM", dto.getRenavam());
			
			if (dto.getModelo() != null) {
				param.setParam("MODE_NM_MODELO",  dto.getModelo().getNome());
				if (isCodigoValido(dto.getModelo()))
					param.setParam("MODELO_FIPE.MODE_CD_MODELO",  dto.getModelo().getCodigo());
			}
			
			if (dto.getMarca() != null) {
				param.setParam("MARCA_FIPE.MARC_CD_MARCA", dto.getMarca().getCodigo());
				param.setParam("MARC_NM_MARCA", dto.getMarca().getNome());
			}
			param.setParam("USUA_CD_USUARIO_CRIACAO", dto.getUsuarioCadastro() != null ? dto.getUsuarioCadastro().getCodigo() : null);
			param.setParam("USUA_CD_USUARIO_ALTERACAO", dto.getUsuarioAlteracao() != null ? dto.getUsuarioAlteracao().getCodigo() : null);
			
		}
	

		return param;
	}

}
