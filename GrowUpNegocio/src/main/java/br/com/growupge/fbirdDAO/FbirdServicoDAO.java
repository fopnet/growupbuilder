/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.fbirdDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.ServicoDTO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.interfacesdao.SequenciaDAO;

/**
 * Classe DAO para gerenciar informa��es pertinentes a Empresa.
 * 
 * @author Felipe
 * 
 */
public class FbirdServicoDAO extends FbirdDAO<ServicoDTO> implements DAO<ServicoDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            objeto de f�brica.
	 */
	public FbirdServicoDAO(final DAOFactory daofactory) {
		super(daofactory);
		sql.append("select");
		sql.append("    SERVICO.SERV_CD_SERVICO,");
		sql.append("    SERV_NM_SERVICO, ");
		sql.append("    SERV_DS_SERVICO, ");
		sql.append("    SERV_IN_TRAMITADO ");
		sql.append(" from ");
		sql.append("    SERVICO");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final ServicoDTO delete(final Connection conn, final ServicoDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;

		try {
			sql.append("delete from SERVICO ");
			sql.append("where SERV_CD_SERVICO = ?");
			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setLong(1, dto.getCodigo());
			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final ServicoDTO insert(final Connection conn, final ServicoDTO dto)
			throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuilder sql = new StringBuilder();

		try {
			SequenciaDAO seq = daoFactory.getSequenciaDAO();
			dto.setCodigo(seq.getProximaSequencia("GEN_SERV_CD_SERVICO"));
			
			sql.append("INSERT INTO SERVICO (");
			sql.append("    SERV_CD_SERVICO,");
			sql.append("    SERV_NM_SERVICO, ");
			sql.append("    SERV_DS_SERVICO, ");
			sql.append("    SERV_IN_TRAMITADO )");
			sql.append(" values ");
			sql.append(" (?,?,?,?) ");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			pstmt.setLong(inc++, dto.getCodigo());
			pstmt.setString(inc++, dto.getNome());
			pstmt.setString(inc++, dto.getDescricao());
			pstmt.setString(inc++, dto.getIsTramitado());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final ServicoDTO update(final Connection conn, final ServicoDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;
		try {

			sql.append("UPDATE SERVICO SET ");
			sql.append("    SERV_NM_SERVICO=?, ");
			sql.append("    SERV_DS_SERVICO=?, ");
			sql.append("    SERV_IN_TRAMITADO=? ");
			sql.append(" WHERE SERV_CD_SERVICO = ? ");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;

			pstmt.setString(inc++, dto.getNome());
			pstmt.setString(inc++, dto.getDescricao());
			pstmt.setString(inc++, dto.getIsTramitado());
			pstmt.setLong(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final ServicoDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {

		ServicoDTO dto = null;

		try {
			dto = new ServicoDTO();

			dto.setCodigo(rs.getLong("SERV_CD_SERVICO"));
			dto.setNome(rs.getString("SERV_NM_SERVICO"));
			dto.setDescricao(rs.getString("SERV_DS_SERVICO"));
			dto.setIsTramitado(rs.getString("SERV_IN_TRAMITADO"));

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final ServicoDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			param = this.setParametro(param, dto);

			if (param.getParametros().isEmpty()) {
				param.setParam("SERVICO.SERV_CD_SERVICO", ParametroFactory.CODIGO_INEXISTENTE);
			}
		}
		
		param.setOrdem("SERV_NM_SERVICO", TipoOrdemDAO.ASC);

		return param;
	}
	
	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.fbirdDAO.FbirdDAO#setParametro(br.com.growupge.factory.ParametroFactory, java.lang.Object)
	 */
	@Override
	protected ParametroFactory setParametro(final ParametroFactory param, final ServicoDTO dto) throws GrowUpDAOException {
		if (dto.getCodigo() != 0) {
			param.setParam("SERVICO.SERV_CD_SERVICO", dto.getCodigo());
		} else {
			param.setParam("SERV_NM_SERVICO", dto.getNome());
			param.setParam("SERV_IN_TRAMITADO", dto.getIsTramitado());
		}
		return param;
	}

}
