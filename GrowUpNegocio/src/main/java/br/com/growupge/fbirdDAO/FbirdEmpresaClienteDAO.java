/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.fbirdDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.EmpresaItemDTO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;

/**
 * Classe DAO para gerenciar informa��es pertinentes a Empresa.
 * 
 * @author Felipe
 * 
 */
public class FbirdEmpresaClienteDAO extends FbirdDAO<EmpresaItemDTO> implements DAO<EmpresaItemDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            objeto de f�brica.
	 */
	public FbirdEmpresaClienteDAO(final DAOFactory daofactory) {
		super(daofactory);
		FbirdEmpresaDAO daoEmpresa = (FbirdEmpresaDAO) daoFactory.getEmpresaDAO();// new FbirdEmpresaDAO(this.daofactory);
		StringBuffer colEmpresa = new StringBuffer(daoEmpresa.sql.toString().trim().substring(
				6,
				daoEmpresa.sql.toString().trim().toUpperCase().indexOf("FROM"))).append(",");

		FbirdClienteDAO daoCliente = (FbirdClienteDAO) daoFactory.getClienteDAO();
		StringBuffer colCliente = new StringBuffer(daoCliente.sql.toString().trim().substring(
				6,
				daoCliente.sql.toString().trim().toUpperCase().indexOf("FROM")));

		sql.append("select ");
		sql.append(colEmpresa);
		sql.append(colCliente);
		sql.append(" from CLIENTE_EMPRESA ");
		sql.append(" INNER JOIN EMPRESA ON  ");
		sql.append(" 	CLIENTE_EMPRESA.EMPR_CD_EMPRESA = EMPRESA.EMPR_CD_EMPRESA ");
		sql.append(" INNER JOIN CLIENTE ON ");
		sql.append(" CLIENTE_EMPRESA.CLIE_CD_CLIENTE = CLIENTE.CLIE_CD_CLIENTE ");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final EmpresaItemDTO delete(final Connection conn, final EmpresaItemDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;
		try {
			sql.append("delete from CLIENTE_EMPRESA ");
			sql.append("where EMPR_CD_EMPRESA = ? ");
			sql.append("AND  CLIE_CD_CLIENTE = ?");
			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setLong(1, dto.empresa.getCodigo());
			pstmt.setLong(2, dto.cliente.getCodigo());
			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final EmpresaItemDTO insert(final Connection conn, final EmpresaItemDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;

		try {

			sql.append("INSERT INTO CLIENTE_EMPRESA (");
			sql.append("    EMPR_CD_EMPRESA ,");
			sql.append("    CLIE_CD_CLIENTE )");
			sql.append(" values ");
			sql.append(" (?,?) ");
			int inc = 1;
			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setLong(inc++, dto.empresa.getCodigo());
			pstmt.setLong(inc++, dto.cliente.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final EmpresaItemDTO update(final Connection conn, final EmpresaItemDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;
		try {

			sql.append("UPDATE CLIENTE_EMPRESA SET ");
			sql.append("    CLIE_CD_CLIENTE =?");
			sql.append(" WHERE EMPR_CD_EMPRESA = ? ");
			sql.append(" AND CLIE_CD_CLIENTE = ? ");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			pstmt.setLong(inc++, dto.cliente.getCodigo());
			pstmt.setLong(inc++, dto.empresa.getCodigo());
			pstmt.setLong(inc++, dto.cliente.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final EmpresaItemDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {

		EmpresaItemDTO dto = null;

		dto = new EmpresaItemDTO();

		dto.empresa = this.daoFactory.getEmpresaDAO().getDTO(rs, false);
		dto.cliente = this.daoFactory.getClienteDAO().getDTO(rs, false);

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final EmpresaItemDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			param = this.setParametro(param, dto);

			if (param.getParametros().isEmpty()) {
				param.setParam("EMPRESA.EMPR_CD_EMPRESA", ParametroFactory.CODIGO_INEXISTENTE);
			}
		}

		return param;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.fbirdDAO.FbirdDAO#setParametro(br.com.growupge.factory.ParametroFactory,
	 *      java.lang.Object)
	 */
	@Override
	protected ParametroFactory setParametro(final ParametroFactory param, final EmpresaItemDTO dto)
			throws GrowUpDAOException {
		param.setParam(
				"EMPRESA.EMPR_CD_EMPRESA",
				dto.empresa == null || dto.empresa.getCodigo() == 0 ? null : dto.empresa.getCodigo());
		param.setParam(
				"CLIENTE.CLIE_CD_CLIENTE",
				dto.cliente == null || dto.cliente.getCodigo() == 0 ? null : dto.cliente.getCodigo());

		return param;
	}
}
