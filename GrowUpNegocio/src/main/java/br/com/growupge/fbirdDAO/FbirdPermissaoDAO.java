package br.com.growupge.fbirdDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.PermissaoDTO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;

/**
 * Classe DAO para gerenciar informa��es pertinentes a permiss�es.
 * 
 * @author Felipe
 * 
 */
public class FbirdPermissaoDAO extends FbirdDAO<PermissaoDTO> implements DAO<PermissaoDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            Inst�ncia da F�brica de DAO.
	 */
	public FbirdPermissaoDAO(final DAOFactory daofactory) {
		super(daofactory);

		sql.append(" select ");
		sql.append("  PERM_CD_PERMISSAO, ");
		sql.append("  PERM_DS_PERMISSAO ");
		sql.append(" from ");
		sql.append("   PERMISSAO");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.growupge.sigem.oracledao.OracleDAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final PermissaoDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {
		PermissaoDTO permissaoDTO = null;
		try {
			permissaoDTO = new PermissaoDTO();
			permissaoDTO.setCodigo(rs.getString("PERM_CD_PERMISSAO"));
			permissaoDTO.setDescricao(rs.getString("PERM_DS_PERMISSAO"));

			return permissaoDTO;

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		}

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.growupge.sigem.interfacesdao.DAO#deletePagamentosPayPal(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PermissaoDTO delete(final Connection conn, final PermissaoDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append("delete from PERMISSAO ");
			sql.append("where PERM_CD_PERMISSAO = ?");
			pstmt = conn.prepareStatement(sql.toString());

			/* Limpa valores antigos dos par�metros. */
			pstmt.clearParameters();
			pstmt.setString(1, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.REGISTRO_EXCLUIDO_PREVIAMENTE);
			}

		} catch (SQLException e) {

			if (e.getErrorCode() == 2292)
				throw new GrowUpDAOException(MSGCODE.PERMISSAO_REGISTROS_ASSOCIADOS);
			else
				throw new GrowUpDAOException(e);

		} finally {
			close(pstmt);
		}

		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.growupge.sigem.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PermissaoDTO insert(final Connection conn, final PermissaoDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append("insert into PERMISSAO(");
			sql.append("  PERM_CD_PERMISSAO, ");
			sql.append("  PERM_DS_PERMISSAO) ");
			sql.append("values (?,?)");
			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			pstmt.setString(inc++, dto.getCodigo());
			pstmt.setString(inc++, dto.getDescricao());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.growupge.sigem.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PermissaoDTO update(final Connection conn, final PermissaoDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();

		try {
			sql.append(" update PERMISSAO ");
			sql.append(" set PERM_DS_PERMISSAO =? ");
			sql.append(" where PERM_CD_PERMISSAO= ?");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			pstmt.setString(inc++, dto.getDescricao());
			pstmt.setString(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.growupge.sigem.oracledao.OracleDAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final PermissaoDTO dto) throws GrowUpDAOException {

		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {

			param = this.setParametro(param, dto);

			if (param.getParametros().isEmpty()) {
				param.setParam("PERM_CD_PERMISSAO", String
						.valueOf(ParametroFactory.CODIGO_INEXISTENTE));
			}
		}

		param.setOrdem("PERM_CD_PERMISSAO", TipoOrdemDAO.ASC);

		return param;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.fbirdDAO.FbirdDAO#setParametro(br.com.growupge.factory.ParametroFactory, java.lang.Object)
	 */
	@Override
	protected ParametroFactory setParametro(final ParametroFactory param, final PermissaoDTO dto)
			throws GrowUpDAOException {
		if (dto.getCodigo() != null) {
			param.setParam("PERM_CD_PERMISSAO", dto.getCodigo() == null ? dto.getCodigo() : dto.getCodigo().trim()
					.toUpperCase());
		} else {
			param.setParam("PERM_DS_PERMISSAO", dto.getDescricao() == null ? dto.getDescricao()
					: dto.getDescricao().trim().toUpperCase());
		}
		return param;
	}
}
