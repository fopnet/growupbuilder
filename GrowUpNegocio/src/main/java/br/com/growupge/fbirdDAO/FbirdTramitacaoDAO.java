/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.fbirdDAO;

import static br.com.growupge.utility.Reflection.isCodigoValido;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.dto.TramitacaoDTO;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.enums.TipoOperadorDAO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.SequenciaDAO;
import br.com.growupge.interfacesdao.TramitacaoDAO;
import br.com.growupge.utility.DateUtil;

/**
 * Classe DAO para gerenciar informa��es pertinentes a Empresa.
 * 
 * @author Felipe
 * 
 */
public class FbirdTramitacaoDAO extends FbirdDAO<TramitacaoDTO> implements TramitacaoDAO {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            objeto de f�brica.
	 */
	public FbirdTramitacaoDAO(final DAOFactory daofactory) {
		super(daofactory);

		StringBuffer colunasPedidos = this.getColunasDAO(new FbirdPedidoVeiculoDAO(daofactory));
		
		this.sql.append("select");
		this.sql.append("    TRAMITACAO.TRAMI_CD_TRAMITACAO,");
		this.sql.append("    TRAMI_IN_PENDENTE, ");
		this.sql.append("    TRAMI_TX_OBSERVACAO, ");
		this.sql.append("    TRAMI_DH_CRIACAO,");
		this.sql.append("    TRAMI_DH_ALTERACAO, ");
		this.sql.append("    TRAMITACAO.USUA_CD_USUARIO_CRIACAO, ");
		this.sql.append("    TRAMITACAO.USUA_CD_USUARIO_ALTERACAO, ");
		this.sql.append(colunasPedidos);
		this.sql.append(" from TRAMITACAO ");
		this.sql.append(" 	INNER JOIN PEDIDO ON ");
		this.sql.append(" 	PEDIDO.PEDI_CD_PEDIDO = TRAMITACAO.PEDI_CD_PEDIDO ");
		this.sql.append(" LEFT JOIN ARQUIVO  ON ");
		this.sql.append(" 	ARQUIVO.ARQU_CD_ARQUIVO = PEDIDO.ARQU_CD_ARQUIVO");
		this.sql.append(" LEFT JOIN CLIENTE  ON ");
		this.sql.append(" 	CLIENTE.CLIE_CD_CLIENTE = PEDIDO.CLIE_CD_CLIENTE");
		this.sql.append(" INNER JOIN SERVICO  ON ");
		this.sql.append(" 	SERVICO.SERV_CD_SERVICO = PEDIDO.SERV_CD_SERVICO");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final TramitacaoDTO delete(final Connection conn, final TramitacaoDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;
		try {
			sql.append("delete from TRAMITACAO ");
			sql.append("where TRAMI_CD_TRAMITACAO = ?");
			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setLong(1, dto.getCodigo());
			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * @param conn
	 * @param dto
	 * @return
	 * @throws GrowUpDAOException
	 */
	@Override
	public final PedidoVeiculoDTO delete(final Connection conn, final PedidoVeiculoDTO dto)
			throws GrowUpDAOException {
		
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;
		try {
			sql.append("delete from TRAMITACAO ");
			sql.append("where PEDI_CD_PEDIDO = ?");
			pstmt = conn.prepareStatement(sql.toString());
			
			pstmt.setLong(1, dto.getCodigo());
			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}
			
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final TramitacaoDTO insert(final Connection conn, final TramitacaoDTO dto)
			throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuilder sql = new StringBuilder();

		try {
			SequenciaDAO seq = daoFactory.getSequenciaDAO();
			dto.setCodigo(seq.getProximaSequencia("GEN_TRAMI_CD_TRAMITACAO"));

			sql.append("INSERT INTO TRAMITACAO (");
			sql.append(" TRAMI_CD_TRAMITACAO,");
			sql.append(" PEDI_CD_PEDIDO,");
			sql.append(" TRAMI_IN_PENDENTE, ");
			sql.append(" TRAMI_TX_OBSERVACAO, ");
			sql.append(" TRAMI_DH_CRIACAO,");
			// sql.append(" TRAMI_DH_ALTERACAO, ");
			// sql.append(" USUA_CD_USUARIO_ALTERACAO, ");
			sql.append(" USUA_CD_USUARIO_CRIACAO ) ");
			sql.append(" values ");
			sql.append(" (?,?,?,?,CURRENT_TIMESTAMP,?) ");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			pstmt.setLong(inc++, dto.getCodigo());
			pstmt.setLong(inc++, dto.getPedido().getCodigo());
			pstmt.setString(inc++, dto.getIsPendente());
			pstmt.setString(inc++, dto.getObs());
			pstmt.setString(inc++, dto.getUsuarioCadastro().getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final TramitacaoDTO update(final Connection conn, final TramitacaoDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;
		
		try {

			sql.append("UPDATE TRAMITACAO SET ");
			sql.append(" TRAMI_IN_PENDENTE =?,");
			sql.append(" TRAMI_TX_OBSERVACAO =?,");
			sql.append(" TRAMI_DH_ALTERACAO =CURRENT_TIMESTAMP,");
			sql.append(" USUA_CD_USUARIO_ALTERACAO =?");
			sql.append(" WHERE TRAMI_CD_TRAMITACAO = ?");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;

			pstmt.setString(inc++, dto.getIsPendente());
			pstmt.setString(inc++, dto.getObs());
			pstmt.setString(inc++, dto.getUsuarioAlteracao().getCodigo());
			pstmt.setLong(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final TramitacaoDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {

		TramitacaoDTO dto = null;

		try {
			dto = new TramitacaoDTO();

			dto.setCodigo(rs.getLong("TRAMI_CD_TRAMITACAO"));
			dto.setIsPendente(rs.getString("TRAMI_IN_PENDENTE"));
			dto.setDataCadastro(rs.getTimestamp("TRAMI_DH_CRIACAO"));
			dto.setUsuarioCadastro(new UsuarioDTO());
			dto.getUsuarioCadastro().setCodigo(rs.getString("USUA_CD_USUARIO_CRIACAO"));
			dto.setObs(rs.getString("TRAMI_TX_OBSERVACAO"));

			if (rs.getTimestamp("TRAMI_DH_ALTERACAO") != null) {
				dto.setDataAlteracao(rs.getTimestamp("TRAMI_DH_ALTERACAO"));
				dto.setUsuarioAlteracao(new UsuarioDTO());
				dto.getUsuarioAlteracao().setCodigo(rs.getString("USUA_CD_USUARIO_ALTERACAO"));
			}
			
			dto.setPedido(this.daoFactory.getPedidoVeiculoDAO().getDTO(rs, false));

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final TramitacaoDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			param = this.setParametro(param, dto);

			if (param.getParametros().isEmpty()) {
				param.setParam("TRAMITACAO.TRAMI_CD_TRAMITACAO",
						ParametroFactory.CODIGO_INEXISTENTE);
			}
		}

		param.setOrdem("TRAMI_CD_TRAMITACAO", TipoOrdemDAO.DESC);

		return param;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.fbirdDAO.FbirdDAO#setParametro(br.com.growupge.factory.ParametroFactory,
	 *      java.lang.Object)
	 */
	@Override
	protected ParametroFactory setParametro(final ParametroFactory param, final TramitacaoDTO dto)
			throws GrowUpDAOException {
		if (dto.getCodigo() != 0) {
			param.setParam("TRAMITACAO.TRAMI_CD_TRAMITACAO", dto.getCodigo());
		} else {
			
			if (dto.getPedido() != null) {
				param.setParam("PEDIDO.PEDI_CD_PEDIDO", dto.getPedido().getCodigo() != 0 ? dto.getPedido().getCodigo() : null);
				param.setParam("VEIC_CD_PLACA", dto.getPedido().getPlaca());
				param.setParam("CLIENTE.CLIE_NM_CLIENTE", dto.getPedido().getCliente() != null ? dto.getPedido().getCliente().getNome() : null);
				param.setParam("SERVICO.SERV_NM_SERVICO", dto.getPedido().getServico() != null ? dto.getPedido().getServico().getNome() : null);
			}
			param.setParam("TRAMI_TX_OBSERVACAO", dto.getObs());
			param.setParam("TRAMI_IN_PENDENTE", dto.getIsPendente());
			
			if (dto.getFiltroDataInicial() != null) {
				param.setParam("cast(TRAMI_DH_CRIACAO as DATE)", TipoOperadorDAO.MAIOR_IGUAL, dto.getFiltroDataInicial());
			}
			if (dto.getFiltroDataFinal() != null) {
				//Preciso aumentar um dia por causa da hora que chega 00:00:00
				DateUtil dm = DateUtil.getDateManagerInstance(dto.getFiltroDataFinal());
				dm.addDays(1);
				dto.setFiltroDataFinal(dm.getDateTime()); 
				param.setParam("CAST(TRAMI_DH_CRIACAO AS DATE)", TipoOperadorDAO.MENOR_IGUAL, dto.getFiltroDataFinal());
			}			
			
			if (isCodigoValido(dto.getUsuarioCadastro()))
				param.setParam("TRAMITACAO.USUA_CD_USUARIO_CRIACAO", dto.getUsuarioCadastro().getCodigo());
			if (isCodigoValido(dto.getUsuarioAlteracao()))
				param.setParam("TRAMITACAO.USUA_CD_USUARIO_ALTERACAO", dto.getUsuarioAlteracao().getCodigo());

		}
		return param;
	}

}
