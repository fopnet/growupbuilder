/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 08/01/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 08/01/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.fbirdDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.PerfilPermissaoItemDTO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;

/**
 * Classe DAO para gerenciar informa��es pertinentes a lista de perfis x
 * permiss�o.
 * 
 * @author Felipe
 * 
 */
public class FbirdPerfilPermissaoDAO extends FbirdDAO<PerfilPermissaoItemDTO> implements
		DAO<PerfilPermissaoItemDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            F�brica DAO.
	 */
	public FbirdPerfilPermissaoDAO(final DAOFactory daofactory) {
		super(daofactory);
		sql.append(" select ");
		sql.append("  PERFIL.PERF_CD_PERFIL, ");
		sql.append("  PERF_DS_PERFIL, ");
		sql.append("  PERMISSAO.PERM_CD_PERMISSAO, ");
		sql.append("  PERM_DS_PERMISSAO ");
		sql.append(" from PERFIL_PERMISSAO ");
		sql.append(" INNER JOIN PERFIL ON");
		sql.append(" 	PERFIL_PERMISSAO.PERF_CD_PERFIL = PERFIL.PERF_CD_PERFIL ");
		sql.append(" INNER JOIN PERMISSAO ON");
		sql.append("    PERFIL_PERMISSAO.PERM_CD_PERMISSAO = PERMISSAO.PERM_CD_PERMISSAO ");
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.OracleDAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final PerfilPermissaoItemDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {
		PerfilPermissaoItemDTO perfilPermissaoItemDTO = null;
		perfilPermissaoItemDTO = new PerfilPermissaoItemDTO();
		perfilPermissaoItemDTO.setPerfil(getDaofactory().getPerfilDAO().getDTO(rs, false));
		perfilPermissaoItemDTO.setPermissao(getDaofactory().getPermissaoDAO().getDTO(rs, false));
		return perfilPermissaoItemDTO;
	}

	/**
	 * M�todo utilizado para excluir associa��es de perfis com a permiss�o
	 * informada.
	 * 
	 * @param conn
	 *            Inst�ncia de conex�o com o banco.
	 * @param dto
	 *            Objeto de trasnfer�ncia com os dados da permiss�o.
	 * @return Objeto de transfer�ncia com os dados da permiss�o exclu�da.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro durante a opera��o.
	 */
	@Override
	public final PerfilPermissaoItemDTO delete(
			final Connection conn,
			final PerfilPermissaoItemDTO dto) throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = null;
		try {
			sql = new StringBuffer();
			sql.append("delete from PERFIL_PERMISSAO ");
			sql.append("where PERM_CD_PERMISSAO = ?");
			sql.append("AND PERF_CD_PERFIL = ?");
			pstmt = conn.prepareStatement(sql.toString());

			pstmt.clearParameters();
			pstmt.setString(1, dto.getPermissao().getCodigo());
			pstmt.setString(2, dto.getPerfil().getCodigo());
			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PerfilPermissaoItemDTO insert(
			final Connection conn,
			final PerfilPermissaoItemDTO dto) throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append("insert into PERFIL_PERMISSAO(");
			sql.append("  PERF_CD_PERFIL, ");
			sql.append("  PERM_CD_PERMISSAO) ");
			sql.append("values (?,?)");
			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			pstmt.setString(inc++, dto.getPerfil().getCodigo());
			pstmt.setString(inc++, dto.getPermissao().getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PerfilPermissaoItemDTO update(
			final Connection conn,
			final PerfilPermissaoItemDTO dto) throws GrowUpDAOException {
		// N�o ser� utilizado.
		return null;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.OracleDAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final PerfilPermissaoItemDTO dto)
			throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			param = this.setParametro(param, dto);

			if (param.getParametros().isEmpty()) {
				param.setParam("PERFIL.PERF_CD_PERFIL", ParametroFactory.CODIGO_INEXISTENTE);
			}
		}
		return param;

	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.fbirdDAO.FbirdDAO#setParametro(br.com.growupge.factory.ParametroFactory, java.lang.Object)
	 */
	@Override
	protected ParametroFactory setParametro(
			final ParametroFactory param,
			final PerfilPermissaoItemDTO dto) throws GrowUpDAOException {
		if (dto.getPerfil() != null && dto.getPerfil().getCodigo() != null && dto.getPermissao() != null
				&& dto.getPermissao().getCodigo() != null) {
			param.setParam("PERFIL.PERF_CD_PERFIL", dto.getPerfil().getCodigo());
			param.setParam("PERMISSAO.PERM_CD_PERMISSAO", dto.getPermissao().getCodigo());
		} else {
			param.setParam(
					"PERFIL.PERF_CD_PERFIL",
					dto.getPerfil() == null || dto.getPerfil().getCodigo() == null ? null : dto.getPerfil().getCodigo());
			param.setParam("PERMISSAO.PERM_CD_PERMISSAO", dto.getPermissao() == null
					|| dto.getPermissao().getCodigo() == null ? null : dto.getPermissao().getCodigo());
		}
		return param;
	}
}
