/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.fbirdDAO;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.ProprietarioDTO;
import br.com.growupge.enums.TipoOperadorDAO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.interfacesdao.SequenciaDAO;

/**
 * Classe DAO para gerenciar informa��es pertinentes a Empresa.
 * 
 * @author Felipe
 * 
 */
public class FbirdProprietarioDAO extends FbirdDAO<ProprietarioDTO> implements DAO<ProprietarioDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            objeto de f�brica.
	 */
	public FbirdProprietarioDAO(final DAOFactory daofactory) {
		super(daofactory);

		sql.append("select");
		sql.append("    PROPRIETARIO.PROP_CD_PROPRIETARIO,");
		sql.append("    PROP_NM_PROPRIETARIO, ");
		sql.append("    PROP_CD_CPF_CGC, ");
		sql.append("    PROP_DH_CADASTRO, ");
		sql.append("    PROP_DH_ALTERACAO,");
		sql.append("    PROP_TX_ENDERECO, ");
		sql.append("    PROP_NM_BAIRRO, ");
		sql.append("    PROP_NM_CIDADE, ");
		sql.append("    PROP_CD_UF, ");
		sql.append("    PROP_CD_CEP, ");
		sql.append("    PROP_NR_TEL, ");
		sql.append("    PROP_NR_CELULAR, ");
		sql.append("    PROP_TX_OBS ");
		sql.append(" from ");
		sql.append("    PROPRIETARIO");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final ProprietarioDTO delete(final Connection conn, final ProprietarioDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;

		try {
			sql.append("delete from PROPRIETARIO ");
			sql.append("where PROP_CD_PROPRIETARIO = ?");
			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setLong(1, dto.getCodigo());
			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final ProprietarioDTO insert(final Connection conn, final ProprietarioDTO dto)
			throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuilder sql = new StringBuilder();

		try {
			SequenciaDAO seq = daoFactory.getSequenciaDAO();
			dto.setCodigo(seq.getProximaSequencia("GEN_PROP_CD_PROPRIETARIO"));

			sql.append("INSERT INTO PROPRIETARIO (");
			sql.append("    PROP_CD_PROPRIETARIO, ");
			sql.append("    PROP_NM_PROPRIETARIO, ");
			sql.append("    PROP_CD_CPF_CGC, ");
			sql.append("    PROP_DH_CADASTRO, ");
			//sql.append("    PROP_DH_ALTERACAO,");
			sql.append("    PROP_TX_ENDERECO, ");
			sql.append("    PROP_NM_BAIRRO, ");
			sql.append("    PROP_NM_CIDADE, ");
			sql.append("    PROP_CD_UF, ");
			sql.append("    PROP_CD_CEP, ");
			sql.append("    PROP_NR_TEL, ");
			sql.append("    PROP_NR_CELULAR, ");
			sql.append("    PROP_TX_OBS )");
			sql.append(" values ");
			sql.append(" (?,?,?, CURRENT_TIMESTAMP ,?,?,?,?,?,?,?,?) ");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			pstmt.setLong(inc++, dto.getCodigo());
			pstmt.setString(inc++, dto.getNome());
			pstmt.setString(inc++, dto.getCpfcnpj());
			pstmt.setString(inc++, dto.getEndereco());
			pstmt.setString(inc++, dto.getBairro());
			pstmt.setString(inc++, dto.getCidade());
			pstmt.setString(inc++, dto.getUf());
			pstmt.setString(inc++, dto.getCep());
			pstmt.setString(inc++, dto.getTel());
			pstmt.setString(inc++, dto.cel);
			pstmt.setString(inc++, dto.getObs());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final ProprietarioDTO update(final Connection conn, final ProprietarioDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;
		try {

			sql.append("UPDATE PROPRIETARIO SET ");
			sql.append("    PROP_NM_PROPRIETARIO =?, ");
			sql.append("    PROP_CD_CPF_CGC=?, ");
			//sql.append("    PROP_DH_CADASTRO=?, ");
			sql.append("    PROP_DH_ALTERACAO =CURRENT_TIMESTAMP,");
			sql.append("    PROP_TX_ENDERECO=?, ");
			sql.append("    PROP_NM_BAIRRO=?, ");
			sql.append("    PROP_NM_CIDADE=?, ");
			sql.append("    PROP_CD_UF=?, ");
			sql.append("    PROP_CD_CEP=?, ");
			sql.append("    PROP_NR_TEL=?, ");
			sql.append("    PROP_NR_CELULAR=?, ");
			sql.append("    PROP_TX_OBS =? ");
			sql.append(" WHERE PROP_CD_PROPRIETARIO = ? ");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;

			pstmt.setString(inc++, dto.getNome());
			pstmt.setString(inc++, dto.getCpfcnpj());
			pstmt.setString(inc++, dto.getEndereco());
			pstmt.setString(inc++, dto.getBairro());
			pstmt.setString(inc++, dto.getCidade());
			pstmt.setString(inc++, dto.getUf());
			pstmt.setString(inc++, dto.getCep());
			pstmt.setString(inc++, dto.getTel());
			pstmt.setString(inc++, dto.cel);
			pstmt.setString(inc++, dto.getObs());
			pstmt.setLong(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final ProprietarioDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {

		ProprietarioDTO dto = null;

		try {
			dto = new ProprietarioDTO();

			dto.setCodigo(rs.getLong("PROP_CD_PROPRIETARIO"));
			dto.setNome(rs.getString("PROP_NM_PROPRIETARIO"));
			dto.setDataCadastro(rs.getTimestamp("PROP_DH_CADASTRO"));
			dto.setDataAlteracao(rs.getTimestamp("PROP_DH_ALTERACAO"));
			dto.setEndereco(rs.getString("PROP_TX_ENDERECO"));
			dto.setBairro(rs.getString("PROP_NM_BAIRRO"));
			dto.setCidade(rs.getString("PROP_NM_CIDADE"));
			dto.setUf(rs.getString("PROP_CD_UF"));
			dto.setCep(rs.getString("PROP_CD_CEP"));
			dto.setCpfcnpj(rs.getString("PROP_CD_CPF_CGC"));
			dto.setTel(rs.getString("PROP_NR_TEL"));
			dto.cel = rs.getString("PROP_NR_CELULAR");
			dto.setObs(rs.getString("PROP_TX_OBS"));

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final ProprietarioDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			param = this.setParametro(param, dto);

			if (param.getParametros().isEmpty()) {
				param.setParam(
						"PROPRIETARIO.PROP_CD_PROPRIETARIO",
						ParametroFactory.CODIGO_INEXISTENTE);
			}
		}

		param.setOrdem("PROP_NM_PROPRIETARIO", TipoOrdemDAO.ASC);

		return param;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.fbirdDAO.FbirdDAO#setParametro(br.com.growupge.factory.ParametroFactory, java.lang.Object)
	 */
	@Override
	protected ParametroFactory setParametro(final ParametroFactory param, final ProprietarioDTO dto)
			throws GrowUpDAOException {
		if (dto.getCodigo() != 0) {
			param.setParam("PROPRIETARIO.PROP_CD_PROPRIETARIO", dto.getCodigo());
		} else if (isNotBlank(dto.getCpfcnpj()) &&  
				(dto.getCpfcnpj().trim().length() == 11 || dto.getCpfcnpj().trim().length() == 14)) {
			param.setParam("PROP_CD_CPF_CGC", dto.getCpfcnpj());
		} else {
			param.setParam("PROP_NM_PROPRIETARIO", dto.getNome());
			param.setParam("PROP_NR_TEL", dto.getTel());
			param.setParam("PROP_NR_CELULAR", dto.cel);

			if (dto.getDataCadastro() != null) {
				if (dto.getFiltroDataInicial() != null) {
					param.setParam(
							"PROP_DH_CADASTRO",
							TipoOperadorDAO.MAIOR_IGUAL,
							dto.getDataCadastro());
				}
				if (dto.getFiltroDataFinal() != null) {
					param.setParam(
							"PROP_DH_CADASTRO",
							TipoOperadorDAO.MENOR_IGUAL,
							dto.getDataAlteracao());
				}
			}
			if (dto.getDataAlteracao() != null) {
				if (dto.getFiltroDataInicial() != null) {
					param.setParam(
							"PROP_DH_ALTERACAO",
							TipoOperadorDAO.MAIOR_IGUAL,
							dto.getDataCadastro());
				}
				if (dto.getFiltroDataFinal() != null) {
					param.setParam(
							"PROP_DH_ALTERACAO",
							TipoOperadorDAO.MENOR_IGUAL,
							dto.getDataAlteracao());
				}
			}
		}
		return param;
	}

}
