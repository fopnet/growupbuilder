/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 18/12/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 18/12/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.fbirdDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.business.NegocioBase;
import br.com.growupge.business.Usuario;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.BeanFactory;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.SessaoDAO;
import br.com.growupge.utility.DateUtil;

/**
 * Classe DAO para gerenciar informa��es pertinentes a sess�o do usu�rio.
 * 
 * @author Felipe
 * 
 */
public class FbirdSessaoDAO extends FbirdDAO<SessaoDTO> implements SessaoDAO {

	/**
	 * Construtor para esta classe.
	 * 
	 */
	public FbirdSessaoDAO(final DAOFactory daoFactory) {
		super(daoFactory);

		sql.append("select");
		sql.append("    SESS_CD_HASH,");
		sql.append("    USUA_CD_USUARIO,");
		sql.append("    SESS_NM_COMPUTADOR,");
		sql.append("    SESS_DH_SESSAO, ");
		sql.append("    SESS_ACCESS_TOKEN ");
		sql.append(" from ");
		sql.append("    SESSAO ");
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final SessaoDTO insert(final Connection conn, final SessaoDTO dto)
			throws GrowUpDAOException {
		StringBuffer sql = new StringBuffer();
		PreparedStatement pstmt = null;

		try {
			sql.append("insert into SESSAO (");
			sql.append("  SESS_CD_HASH, ");
			sql.append("  USUA_CD_USUARIO, ");
			sql.append("  SESS_NM_COMPUTADOR, ");
			sql.append("  SESS_DH_SESSAO, ");
			sql.append("  SESS_ACCESS_TOKEN ");
			sql.append(" ) values ");
			sql.append(" (?,?,?,?,?) ");
			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			pstmt.setString(inc++, dto.getSid());
			pstmt.setString(inc++, dto.getUsuario().getCodigo());
			pstmt.setString(inc++, dto.getHost());
			pstmt.setTimestamp(inc++, DateUtil.getDateManagerInstance().getSQLTimeStamp());
			pstmt.setString(inc++, dto.getAccessToken());
			pstmt.executeUpdate();

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		
		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final SessaoDTO delete(final Connection conn, final SessaoDTO dto)
			throws GrowUpDAOException {
		StringBuffer sql = new StringBuffer();
		PreparedStatement pstmt = null;

		try {
			sql.append("delete from SESSAO ");
			sql.append("where SESS_CD_HASH = ?");
			pstmt = conn.prepareStatement(sql.toString());

			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			pstmt.setString(1, dto.getSid());
			pstmt.executeUpdate();

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;

	}
	
	
	/**
	 * Apaga as sess�es de 1 dia de diferenca que ficaram no limbo
	 * @param connection
	 * @throws GrowUpDAOException 
	 */
	@Override
	public void apagarSessoesExpiradas(Connection conn) throws GrowUpDAOException {
		StringBuffer sql = new StringBuffer();
		PreparedStatement pstmt = null;

		try {
			sql.append("delete from SESSAO ");
			sql.append("where sess_dh_sessao <= 'YESTERDAY'");
			pstmt = conn.prepareStatement(sql.toString());

			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
//			pstmt.setString(1, "YESTERDAY");
			pstmt.executeUpdate();

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.OracleDAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final SessaoDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {
		try {
			SessaoDTO dto = new SessaoDTO();

			dto.setSid(rs.getString("SESS_CD_HASH"));
			dto.setHost(rs.getString("SESS_NM_COMPUTADOR"));
			dto.setData(DateUtil.getDateManagerInstance(rs.getDate("SESS_DH_SESSAO")).getDateTime());
			dto.setAccessToken(rs.getString("SESS_ACCESS_TOKEN"));

			UsuarioDTO usrdto = new UsuarioDTO();
			usrdto.setCodigo(rs.getString("USUA_CD_USUARIO"));
			
			//TODO avaliar se a criacao desse bo pode impactar na performance. 
			NegocioBase<UsuarioDTO> usuarioBO = BeanFactory.getContextualInstance(Usuario.class, getSessao());
			dto.setUsuario(usuarioBO.procurar(usrdto));

			return dto;
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} catch (GrowUpException e) {
			throw new GrowUpDAOException(e);
		} 

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final SessaoDTO update(final Connection conn, final SessaoDTO dto)
			throws GrowUpDAOException {
		return null;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.OracleDAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final SessaoDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {

			param = this.setParametro(param, dto);

			if (param.getParametros().isEmpty()) {
				param.setParam("SESS_CD_HASH", ParametroFactory.CODIGO_INEXISTENTE);
			}
		}

		return param;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.fbirdDAO.FbirdDAO#setParametro(br.com.growupge.factory.ParametroFactory,
	 *      java.lang.Object)
	 */
	@Override
	protected ParametroFactory setParametro(final ParametroFactory param, final SessaoDTO dto)
			throws GrowUpDAOException {
		if (dto.getSid() != null) {
			param.setParam("SESS_CD_HASH", dto.getSid());
		} else {
			param.setParam("SESS_NM_COMPUTADOR", dto.getHost());
			param.setParam(
					"USUA_CD_USUARIO",
					dto.getUsuario() == null || dto.getUsuario().getCodigo() == null ? null : dto.getUsuario().getCodigo());
		}
		return param;
	}


}
