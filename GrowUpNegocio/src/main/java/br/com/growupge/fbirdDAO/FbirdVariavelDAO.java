/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.fbirdDAO;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.VariavelDTO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;

/**
 * Classe DAO para gerenciar informa��es pertinentes as Restricao
 * 
 * @author Felipe
 * 
 */
public class FbirdVariavelDAO extends FbirdDAO<VariavelDTO> implements DAO<VariavelDTO>{

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            objeto de f�brica.
	 */
	public FbirdVariavelDAO(final DAOFactory daofactory) {
		super(daofactory);

		sql.append("select");
		sql.append("    VARIAVEL.VAR_CD_VARIAVEL,");
		sql.append("    VAR_VL_VALOR ");
		sql.append(" from ");
		sql.append("    VARIAVEL ");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final VariavelDTO delete(final Connection conn, final VariavelDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;

		try {
			sql.append("delete from VARIAVEL ");
			sql.append("where VAR_CD_VARIAVEL = ?");
			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setString(1, dto.getCodigo());
			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final VariavelDTO insert(final Connection conn, final VariavelDTO dto)
			throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuilder sql = new StringBuilder();

		try {
			
			sql.append("INSERT INTO VAR_CD_VARIAVEL (");
			sql.append("    VAR_CD_VARIAVEL,");
			sql.append("    VAR_VL_VALOR )");
			sql.append(" values ");
			sql.append(" (?,?) ");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			pstmt.setString(inc++, dto.getCodigo());
			pstmt.setString(inc++, dto.getValor());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final VariavelDTO update(final Connection conn, final VariavelDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;
		try {

			sql.append("UPDATE VARIAVEL SET ");
			sql.append("    VAR_VL_VALOR =? ");
			sql.append(" WHERE VAR_CD_VARIAVEL = ? ");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;

			pstmt.setString(inc++, dto.getValor());
			pstmt.setString(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final VariavelDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {

		VariavelDTO dto = null;

		try {
			dto = new VariavelDTO(rs.getString("VAR_CD_VARIAVEL"));

			dto.setValor( rs.getString("VAR_VL_VALOR") );

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final VariavelDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			param = this.setParametro(param, dto);

			if (param.getParametros().isEmpty()) {
				param.setParam("VARIAVEL.VAR_CD_VARIAVEL", ParametroFactory.CODIGO_INEXISTENTE);
			}
		}

		param.setOrdem("VAR_CD_VARIAVEL", TipoOrdemDAO.ASC);

		return param;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.fbirdDAO.FbirdDAO#setParametro(br.com.growupge.factory.ParametroFactory, java.lang.Object)
	 */
	@Override
	protected ParametroFactory setParametro(final ParametroFactory param, final VariavelDTO dto)
			throws GrowUpDAOException {
		if (isNotBlank(dto.getCodigo())) {
			param.setParam("VARIAVEL.VAR_CD_VARIAVEL", dto.getCodigo());
		} else {
			param.setParam("VAR_VL_VALOR", dto.getValor());
		}
		return param;
	}
	
	
	

}
