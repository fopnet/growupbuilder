/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.fbirdDAO;

import static br.com.growupge.utility.Reflection.isCodigoValido;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.AnoModeloDTO;
import br.com.growupge.dto.ModeloDTO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.enums.TipoVeiculo;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.interfacesdao.SequenciaDAO;

/**
 * Classe DAO para gerenciar informa��es pertinentes a Empresa.
 * 
 * @author Felipe
 * 
 */
public class FbirdModeloDAO extends FbirdDAO<ModeloDTO> implements DAO<ModeloDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            objeto de f�brica.
	 */
	public FbirdModeloDAO(final DAOFactory daofactory) {
		super(daofactory);

		sql.append("select");
		sql.append("    MODELO_FIPE.MODE_CD_MODELO,");
		sql.append("    MODE_NM_MODELO, ");
		sql.append("    MODE_CD_FIPE, ");
		sql.append("    MARCA_FIPE.MARC_CD_MARCA,");
		sql.append("    MARC_NM_MARCA ");
		sql.append(" from ");
		sql.append("    MARCA_FIPE INNER JOIN MODELO_FIPE ON");
		sql.append("    MARCA_FIPE.MARC_CD_MARCA = MODELO_FIPE.MARC_CD_MARCA");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final ModeloDTO delete(final Connection conn, final ModeloDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;

		try {
			sql.append("delete from MODELO_FIPE ");
			sql.append("where MODE_CD_MODELO = ?");
			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setLong(1, dto.getCodigo());
			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final ModeloDTO insert(final Connection conn, final ModeloDTO dto)
			throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuilder sql = new StringBuilder();

		try {
			SequenciaDAO seq = daoFactory.getSequenciaDAO();
			dto.setCodigo(seq.getProximaSequencia("GEN_MODE_CD_MODELO"));

			sql.append("INSERT INTO MODELO_FIPE (");
			sql.append("    MODE_CD_MODELO ,");
			sql.append("    MARC_CD_MARCA ,");
			sql.append("    MODE_CD_FIPE ,");
			sql.append("    MODE_NM_MODELO )");
			sql.append(" values ");
			sql.append(" (?,?,?,?) ");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			pstmt.setLong(inc++, dto.getCodigo());
			pstmt.setLong(inc++, dto.getMarca().getCodigo());
			pstmt.setString(inc++, dto.getCodigoFipe());
			pstmt.setString(inc++, dto.getNome());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final ModeloDTO update(final Connection conn, final ModeloDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;
		try {
			sql.append("UPDATE MODELO_FIPE SET ");
			sql.append("    MARC_CD_MARCA =?,");
			sql.append("    MODE_CD_FIPE =?,");
			sql.append("    MODE_NM_MODELO =? ");
			sql.append(" WHERE MODE_CD_MODELO = ? ");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;

			pstmt.setLong(inc++, dto.getMarca().getCodigo());
			pstmt.setString(inc++, dto.getCodigoFipe());
			pstmt.setString(inc++, dto.getNome());
			pstmt.setLong(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final ModeloDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {

		ModeloDTO dto = null;

		try {
			dto = new ModeloDTO();

			dto.setCodigo(rs.getLong("MODE_CD_MODELO"));
			dto.setNome(rs.getString("MODE_NM_MODELO"));
			dto.setCodigoFipe( rs.getString("MODE_CD_FIPE"));
			dto.setTipo(TipoVeiculo.CARRO.toString());

			dto.setMarca( daoFactory.getMarcaDAO().getDTO(rs, isEager) );
			
			if (isEager) {
				AnoModeloDTO anoDTO = new AnoModeloDTO(dto);
				dto.setAnos( daoFactory.getAnoModeloDAO().selectAll(daoFactory.getConnection(), anoDTO) );
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final ModeloDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			param = this.setParametro(param, dto);

			if (param.getParametros().isEmpty()) {
				param.setParam("MODELO_FIPE.MODE_CD_MODELO", ParametroFactory.CODIGO_INEXISTENTE);
			}
		}

		param.setOrdem("MODE_NM_MODELO", TipoOrdemDAO.ASC);

		return param;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.fbirdDAO.FbirdDAO#setParametro(br.com.growupge.factory.ParametroFactory, java.lang.Object)
	 */
	@Override
	protected ParametroFactory setParametro(final ParametroFactory param, final ModeloDTO dto)
			throws GrowUpDAOException {
		if (isCodigoValido(dto)) {
			param.setParam("MODELO_FIPE.MODE_CD_MODELO", dto.getCodigo());
		} else {
			if (dto.getMarca() != null) {
				if (isCodigoValido(dto.getMarca())) {
					param.setParam("MARCA_FIPE.MARC_CD_MARCA", dto.getMarca().getCodigo());
				} else {
					param.setParam("MARC_NM_MARCA", dto.getMarca().getNome());
				}
			}
			param.setParam("MODE_NM_MODELO", dto.getNome());
//			param.setParam("ANO_NR_ANO", dto.getAno());
		}
		return param;
	}

}
