/**
 * 
 */
package br.com.growupge.fbirdDAO;

import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.Connectable;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.util.CollectionSynchronizeData;

/**
 * @author Felipe
 *
 */
public class CollectionSynchronizePersistence<T> extends CollectionSynchronizeData<T> {
	
	private Connectable factory;
	private DAO<T> dao;

	public CollectionSynchronizePersistence(DAOFactory factory,  DAO<T> dao) {
		this.factory = factory;
		this.dao = dao;
	}

	@Override
	protected void update(T dto) throws GrowUpDAOException {
		dao.update(factory.getConnection(), dto);
	}

	@Override
	protected void insert(T dto) throws GrowUpDAOException {
		dao.insert(factory.getConnection(), dto);
	}

	@Override
	protected void delete(T dto) throws GrowUpDAOException {
		dao.delete(factory.getConnection(), dto);
	}
}
