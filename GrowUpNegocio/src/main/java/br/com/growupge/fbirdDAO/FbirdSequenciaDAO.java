/**
 * 
 */
package br.com.growupge.fbirdDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.SequenciaDAO;

/**
 * @author Felipe
 * 
 */
public class FbirdSequenciaDAO extends FbirdDAO<Long> implements SequenciaDAO {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 */
	public FbirdSequenciaDAO(final DAOFactory daofactory) {
		super(daofactory);
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.petrobras.sigem.interfacesdao.SequenciaDAO#getProximaSequencia(java.lang.String)
	 */
	@Override
	public long getProximaSequencia(final String seqname) throws GrowUpDAOException {
		long nextval = 0;
		ResultSet rs = null;
		Statement stmt = null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT GEN_ID(").append(seqname).append(",1) FROM RDB$DATABASE");

			stmt  = getDaofactory().getConnection().createStatement();
			rs = stmt.executeQuery(sql.toString());
			if (rs.next()) {
				nextval = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(stmt,rs);
		}
		return nextval;
	}

	@Override
	public Long getDTO(ResultSet rs, boolean isEager) throws GrowUpDAOException {
		return null;
	}

	@Override
	public ParametroFactory setParam(Long dto) throws GrowUpDAOException {
		return null;
	}

	@Override
	protected ParametroFactory setParametro(ParametroFactory param, Long dto) throws GrowUpDAOException {
		return null;
	}
}
