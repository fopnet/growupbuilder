/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 28/07/2005
 *
 * Historico de Modifica��o:
 * =========================
 * 28/07/2005 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.fbirdDAO;


import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.enums.TipoOperadorDAO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.UsuarioDAO;
import br.com.growupge.utility.DateUtil;

/**
 * Objeto dao para usu�rio.
 */
public class FbirdUsuarioDAO extends FbirdDAO<UsuarioDTO> implements UsuarioDAO {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            F�brica DAO.
	 */
	public FbirdUsuarioDAO(final DAOFactory daofactory) {
		super(daofactory);

		StringBuffer colunaPerfil = this.getColunasDAO(new FbirdPerfilDAO(daofactory)).append(",");

		StringBuffer colunaEmpresa = this.getColunasDAO(new FbirdEmpresaDAO(daofactory))
				.append(",");

		StringBuffer colunaCliente = getColunasDAO(new FbirdClienteDAO(daofactory));

		this.sql.append(" SELECT");
		this.sql.append("    USUARIO.USUA_CD_USUARIO,");
		this.sql.append("    USUA_NM_USUARIO,");
		this.sql.append("    USUA_NM_EMAIL,");
		this.sql.append("    USUA_IN_HABILITADO,");
		this.sql.append("    USUA_CD_SENHA,");
		this.sql.append("    USUA_DH_ALTERACAO_SENHA, ");
		this.sql.append("    USUA_IN_IDIOMA, ");
		this.sql.append("    USUA_TX_OBS, ");
		this.sql.append("    USUA_NR_TELEFONE, ");
		this.sql.append("    USUA_DH_ULTIMO_ACESSO, ");
		this.sql.append("    USUA_NR_REG_PAGINA, ");
		this.sql.append(colunaPerfil);
		this.sql.append(colunaEmpresa);
		this.sql.append(colunaCliente);

		this.sql.append(" FROM ");
		this.sql.append("    USUARIO ");
		this.sql.append("    INNER JOIN PERFIL ");
		this.sql.append("    ON USUARIO.PERF_CD_PERFIL  = PERFIL.PERF_CD_PERFIL");
		this.sql.append("    LEFT JOIN EMPRESA ");
		this.sql.append("    ON USUARIO.EMPR_CD_EMPRESA = EMPRESA.EMPR_CD_EMPRESA");
		this.sql.append("    LEFT JOIN CLIENTE ");
		this.sql.append("    ON USUARIO.CLIE_CD_CLIENTE = CLIENTE.CLIE_CD_CLIENTE");
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final UsuarioDTO insert(final Connection conn, final UsuarioDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		try {
			StringBuffer lsql = new StringBuffer();

			lsql.append("insert into USUARIO ( ");
			lsql.append("    USUA_CD_USUARIO,");
			lsql.append("    USUA_NM_USUARIO,");
			lsql.append("    USUA_NM_EMAIL,");
			lsql.append(" 	 PERF_CD_PERFIL,");
			lsql.append("    USUA_IN_HABILITADO,");
			lsql.append("    USUA_CD_SENHA,");
			lsql.append("    USUA_IN_IDIOMA, ");
			lsql.append("    USUA_TX_OBS, ");
			lsql.append("    USUA_NR_TELEFONE, ");
			lsql.append("    USUA_DH_ALTERACAO_SENHA, ");
			lsql.append("    EMPR_CD_EMPRESA, ");
			lsql.append("    CLIE_CD_CLIENTE, ");
			lsql.append("    USUA_NR_REG_PAGINA ");
			lsql.append(" ) values ");
			lsql.append(" (?,?,?,?,?,?,?,?,?,?,?,?,?) ");

			pstmt = conn.prepareStatement(lsql.toString());
			// limpa valores antigos dos par�metros
			pstmt.clearParameters();
			int inc = 1;
			pstmt.setString(inc++, dto.getCodigo());
			pstmt.setString(inc++, dto.getNome());
			pstmt.setString(inc++, dto.getEmail());
			pstmt.setString(inc++, dto.getPerfil().getCodigo());
			pstmt.setString(inc++, dto.getHabilitado().trim());
			pstmt.setString(inc++, dto.getSenha());
			pstmt.setString(inc++, dto.getIdioma());
			pstmt.setString(inc++, dto.getObs());
			pstmt.setString(inc++, dto.getTelefone());
			pstmt.setTimestamp(inc++, dto.getDataUltimaAlteracaoSenha() == null ? null : DateUtil
					.getDateManagerInstance(dto.getDataUltimaAlteracaoSenha()).getSQLTimeStamp());

			if (dto.getEmpresa() == null) {
				pstmt.setNull(inc++, Types.BIGINT);
			} else {
				pstmt.setLong(inc++, dto.getEmpresa().getCodigo());
			}
			if (dto.getCliente() == null) {
				pstmt.setNull(inc++, Types.BIGINT);
			} else {
				pstmt.setLong(inc++, dto.getCliente().getCodigo());
			}
			pstmt.setInt(inc++, dto.getQtdRegistros());

			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			return dto;
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final UsuarioDTO update(final Connection conn, final UsuarioDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		try {
			StringBuffer lsql = new StringBuffer();
			lsql.append("update USUARIO set ");
			lsql.append("    USUA_NM_USUARIO= ?,");
			lsql.append("    USUA_NM_EMAIL= ?,");
			lsql.append("    USUA_DH_ALTERACAO_SENHA= ?,");
			lsql.append("    USUA_IN_HABILITADO= ?,");
			lsql.append(" 	 PERF_CD_PERFIL= ?,");
			lsql.append(" 	 USUA_NR_TELEFONE= ?,");
			lsql.append(" 	 USUA_IN_IDIOMA= ?,");
			lsql.append(" 	 USUA_TX_OBS= ?,");
			lsql.append("    EMPR_CD_EMPRESA= ?,");
			lsql.append("    CLIE_CD_CLIENTE= ?,");
			lsql.append("    USUA_NR_REG_PAGINA= ?");
			lsql.append(" WHERE USUA_CD_USUARIO = ? ");

			pstmt = conn.prepareStatement(lsql.toString());

			// limpa valores antigos dos par�metros
			pstmt.clearParameters();
			int inc = 1;
			pstmt.setString(inc++, dto.getNome());
			pstmt.setString(inc++, dto.getEmail());
			pstmt.setTimestamp(inc++, dto.getDataUltimaAlteracaoSenha() == null ? null : DateUtil
					.getDateManagerInstance(dto.getDataUltimaAlteracaoSenha()).getSQLTimeStamp());
			pstmt.setString(inc++, dto.getHabilitado().trim());
			pstmt.setString(inc++, dto.getPerfil().getCodigo());
			pstmt.setString(inc++, dto.getTelefone());
			pstmt.setString(inc++, dto.getIdioma());
			pstmt.setString(inc++, dto.getObs());

			if (dto.getEmpresa() == null) {
				pstmt.setNull(inc++, Types.BIGINT);
			} else {
				pstmt.setLong(inc++, dto.getEmpresa().getCodigo());
			}
			if (dto.getCliente() == null) {
				pstmt.setNull(inc++, Types.BIGINT);
			} else {
				pstmt.setLong(inc++, dto.getCliente().getCodigo());
			}
			pstmt.setInt(inc++, dto.getQtdRegistros());

			pstmt.setString(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			return dto;
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final UsuarioDTO delete(final Connection conn, final UsuarioDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		try {
			StringBuffer lsql = new StringBuffer();
			lsql.append("delete from USUARIO ");
			lsql.append("where USUA_CD_USUARIO = ?");
			pstmt = conn.prepareStatement(lsql.toString());

			// limpa valores antigos dos par�metros
			pstmt.clearParameters();
			pstmt.setString(1, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			return dto;
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.OracleDAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final UsuarioDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {

		UsuarioDTO userdto = null;

		try {
			userdto = new UsuarioDTO();

			userdto.setEmail(rs.getString("USUA_NM_EMAIL"));
			userdto.setCodigo(rs.getString("USUA_CD_USUARIO"));
			userdto.setNome(rs.getString("USUA_NM_USUARIO"));
			userdto.setSenha(rs.getString("USUA_CD_SENHA"));
			userdto.setTelefone(rs.getString("USUA_NR_TELEFONE"));
			userdto.setIdioma(rs.getString("USUA_IN_IDIOMA"));
			userdto.setObs(rs.getString("USUA_TX_OBS"));
			userdto.setDataUltimoAcesso(rs.getTimestamp("USUA_DH_ULTIMO_ACESSO"));
			userdto.setDataUltimaAlteracaoSenha(rs.getDate("USUA_DH_ALTERACAO_SENHA") == null ? null
					: DateUtil.getDateManagerInstance(rs.getDate("USUA_DH_ALTERACAO_SENHA"))
							.getDateTime());
			userdto.setHabilitado(rs.getString("USUA_IN_HABILITADO").trim());
			userdto.setPerfil(this.daoFactory.getPerfilDAO().getDTO(rs, false));

			userdto.setQtdRegistros(rs.getInt("USUA_NR_REG_PAGINA"));

			if (rs.getLong("EMPR_CD_EMPRESA") != 0) {
				userdto.setEmpresa(this.daoFactory.getEmpresaDAO().getDTO(rs, false));
			}
			if (rs.getLong("CLIE_CD_CLIENTE") != 0) {
				userdto.setCliente(this.daoFactory.getClienteDAO().getDTO(rs, false));
			}

			return userdto;
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		}

	}

	/**
	 * Metodo chamado pelo especificamento do trocarSenha do BO de Usuario
	 * 
	 * @param conn
	 * @param dto
	 * @return
	 * @throws GrowUpDAOException
	 */
	@Override
	public final UsuarioDTO updatePassword(final Connection conn, final UsuarioDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		try {

			StringBuffer lsql = new StringBuffer();
			lsql.append("update USUARIO set ");
			lsql.append(" USUA_CD_SENHA = ? ,");
			lsql.append(" USUA_DH_ALTERACAO_SENHA = ? ");
			lsql.append(" WHERE USUA_CD_USUARIO = ? ");

			pstmt = conn.prepareStatement(lsql.toString());

			// limpa valores antigos dos par�metros
			pstmt.clearParameters();
			int inc = 1;
			pstmt.setString(inc++, dto.getSenha());
			pstmt.setTimestamp(inc++, DateUtil.getDateManagerInstance(
					dto.getDataUltimaAlteracaoSenha()).getSQLTimeStamp());
			pstmt.setString(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			return dto;
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
	}

	/**
	 * Metodo chamado especificamente para atualizar a data de ultimo acesso do
	 * Usuario no sistema.
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o
	 * @param dto
	 *            Objeto de transfer�ncia com os dados do usu�rio
	 * @return Um objeto de transfer�ncia com os dados atualizados do usu�rio
	 * @throws SigemDAOException
	 *             Caso ocorra algum problema durante a opera��o
	 */
	@Override
	public final UsuarioDTO updateUltimoAcesso(final Connection conn, final UsuarioDTO dto)
			throws GrowUpException {
		PreparedStatement pstmt = null;
		try {
			StringBuffer lsql = new StringBuffer();
			lsql.append("update USUARIO set ");
			lsql.append(" USUA_DH_ULTIMO_ACESSO =  ");
			//lsql.append(" (SELECT CURRENT_TIMESTAMP AS DATETIME FROM RDB$DATABASE) ");
			lsql.append(" CURRENT_TIMESTAMP ");
			lsql.append(" WHERE USUA_CD_USUARIO = ? ");

			pstmt = conn.prepareStatement(lsql.toString());

			// limpa valores antigos dos par�metros
			pstmt.clearParameters();
			int inc = 1;
			pstmt.setString(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			return dto;
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.interfacesdao.UsuarioDAO#selectPage(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParamPage(final UsuarioDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);

		StringBuffer qryPaginacao = new StringBuffer(" first ");
		qryPaginacao.append(Constantes.VARIAVEL_MAX_REGISTROS_RETORNADOS);

		StringBuffer qry = new StringBuffer(this.sql);
		qry
				.insert(
						qry.toString().toUpperCase().indexOf("SELECT") + "SELECT".length(),
						qryPaginacao);

		param.setQuery(qry);
		if (dto != null) {

			param.setParam("USUA_IN_HABILITADO", dto.getHabilitado());
			param.setParam("USUARIO.USUA_CD_USUARIO", TipoOperadorDAO.MAIOR, dto.getCodigo().trim()
					.toUpperCase());

			// Verifica se s�o todas as revis�es
			if (param.getParametros().isEmpty()) {
				param.setParam("USUARIO.USUA_CD_USUARIO", String
						.valueOf(ParametroFactory.CODIGO_INEXISTENTE));
			}
		}

		param.setOrdem("USUARIO.USUA_CD_USUARIO", TipoOrdemDAO.ASC);
		param.setOrdem("USUA_NM_USUARIO", TipoOrdemDAO.ASC);

		return param;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.OracleDAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final UsuarioDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);
		if (dto != null) {

			param = this.setParametro(param, dto);

			// Verifica se s�o todas as revis�es
			if (param.getParametros().isEmpty()) {
				param.setParam("USUARIO.USUA_CD_USUARIO", String
						.valueOf(ParametroFactory.CODIGO_INEXISTENTE));
			}
		}

		param.setOrdem("USUARIO.USUA_CD_USUARIO", TipoOrdemDAO.ASC);
		param.setOrdem("USUA_NM_USUARIO", TipoOrdemDAO.ASC);

		return param;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.fbirdDAO.FbirdDAO#setParametro(br.com.growupge.factory.ParametroFactory,
	 *      java.lang.Object)
	 */
	@Override
	protected ParametroFactory setParametro(final ParametroFactory param, final UsuarioDTO dto)
			throws GrowUpDAOException {
		if (isNotBlank(dto.getCodigo())) {
			param.setParam("USUARIO.USUA_CD_USUARIO", dto.getCodigo());
			param.setParam("USUA_IN_HABILITADO", dto.getHabilitado());
		} else {
			param.setParam("USUA_NM_USUARIO", dto.getNome());
			param.setParam("USUA_NM_EMAIL", dto.getEmail());
			param.setParam("USUA_IN_HABILITADO", dto.getHabilitado());
			param.setParam("PERFIL.PERF_CD_PERFIL", dto.getPerfil() == null ? null : dto.getPerfil().getCodigo());
			param.setParam("EMPRESA.EMPR_CD_EMPRESA", dto.getEmpresa() == null
					|| dto.getEmpresa().getCodigo() == 0 ? null : dto.getEmpresa().getCodigo());
			param.setParam(
					"EMPR_NM_EMPRESA",
					dto.getEmpresa() == null || dto.getEmpresa().getNome() == null ? null : dto.getEmpresa().getNome());
			param.setParam("CLIENTE.CLIE_CD_CLIENTE", dto.getCliente() == null
					|| dto.getCliente().getCodigo() == 0 ? null : dto.getCliente().getCodigo());
			param.setParam(
					"CLIE_NM_CLIENTE",
					dto.getCliente() == null || isBlank(dto.getCliente().getNome()) ? null : dto.getCliente().getNome());
		}
		return param;
	}
}
