package br.com.growupge.fbirdDAO;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.enums.TipoPerfil;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DtoBuilder;
import br.com.growupge.utility.StringUtil;

/**
 * Classe DAO abstrata que implementa funcionalidades comuns as demais classes
 * DAO e define m�todos abstratos que s�o imlementados pelas mesmas.
 * 
 * @author Felipe
 * 
 * @param <DTO>
 */
public abstract class FbirdDAO<DTO> {

	/**
	 * Atributo '<code>sql</code>' do tipo StringBuffer
	 */
	protected StringBuffer sql = null;

	/**
	 * Atributo '<code>daofactory</code>' do tipo DAOFactory
	 */
	protected DAOFactory daoFactory = null;

	/**
	 * Atributo '<code>sessaodto</code>' do tipo SessaoDTO
	protected SessaoDTO sessaodto = null;
	 */

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            Inst�ncia da F�brica de DAO.
	 */
	public FbirdDAO(final DAOFactory daofactory) {
		this.daoFactory = daofactory;
		this.sql = new StringBuffer();
	}

	/**
	 * @return '<code>sessaodto</code>'
	 */
	public final SessaoDTO getSessao() {
		return this.daoFactory.getSessao();
	}

//	/**
//	 * @param sessaodto
//	 */
//	public final void setSessao(final SessaoDTO sessaodto) {
//		this.sessaodto = sessaodto;
//	}

	/**
	 * M�todo utilizado para obter um DTO populado com as informa��es obtidas do
	 * ResultSet informado.
	 * 
	 * @param rs
	 *            ResultSet com as informa��es para popular o DTO.
	 * @param isEager TODO
	 * @return Um DTO populado com as informa��es obtidas do ResultSet
	 *         informado.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro durante a atribui��o de valores do
	 *             ResultSet para o DTO.
	 */
	public abstract DTO getDTO(ResultSet rs, boolean isEager) throws GrowUpDAOException;

	/**
	 * M�todo utilizado para configurar oconte�do das colunas para montar o SQL
	 * din�mico.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia que cont�m os valores a serem
	 *            configurados.
	 * @return A inst�ncia de ParametroFactory para manipular o SQL.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro de banco durante a opera��o.
	 */
	public abstract ParametroFactory setParam(DTO dto) throws GrowUpDAOException;

	/**
	 * Configura para atributo do dto para o atributo da tabela
	 *
	 * @param param 
	 * @param dto
	 * @return
	 * @throws GrowUpDAOException Caso ocorra algum erro na opera��o
	 */
	protected abstract ParametroFactory setParametro(final ParametroFactory param, final DTO dto)
			throws GrowUpDAOException;

	/**
	 * M�todo utilizado para retornar todos os registros que atendem os
	 * crit�rios do objeto de transfer�ncia informado.
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o com o banco.
	 * @param dto
	 *            Cont�m a informa��o do registro a ser encontrado no banco de
	 *            dados. Se o objeto 'dto' for nulo, este m�todo retorna todos
	 *            os valores referentes a este business object, ou seja, ser�
	 *            feito um select * no banco.
	 * @return Uma Collection com todos os dto�s
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum falha durante a pesquisa.
	 */
	public final List<DTO> selectAll(
			final Connection conn,
			final DTO dto) throws GrowUpDAOException {
		ParametroFactory param = this.setParam(dto);
		return this.selectAll(conn, false, Constantes.DESCONSIDERAR_PAGINACAO, param);
	}
	
	/**
	 * M�todo utilizado para retornar todos os registros que atendem os
	 * crit�rios do objeto de transfer�ncia informado.
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o com o banco.
	 * @param dto
	 *            Cont�m a informa��o do registro a ser encontrado no banco de
	 *            dados. Se o objeto 'dto' for nulo, este m�todo retorna todos
	 *            os valores referentes a este business object, ou seja, ser�
	 *            feito um select * no banco.
	 * @param isClausulaLike
	 *            determina se o select usar� a clausula Like ou Where
	 * @param indiceInicial
	 *            Indice inicial para fazer a pagina��o.
	 * @return Uma Collection com todos os dto�s
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum falha durante a pesquisa.
	 */
	public final List<DTO> selectAll(
			final Connection conn,
			final DTO dto,
			final boolean isClausulaLike,
			final Integer indiceInicial) throws GrowUpDAOException {
//		ParametroFactory param = this.setParam(dto);
		return this.selectAll(conn, dto, isClausulaLike, indiceInicial, new ArrayList<DTO>());
	}
	
	public <OUT> List<OUT> selectAll(final Connection conn, 
									final DTO dto, 
									final boolean isClausulaLike,
									final Integer indiceInicial,
									final List<OUT> lista) throws GrowUpDAOException {
		ParametroFactory param = this.setParam(dto);
		return this.selectAll(conn, isClausulaLike, indiceInicial, param, lista);
	}

	/**
	 * Fornece implementacao padrao 
	 * @param conn
	 * @param isClausulaLike
	 * @param indiceInicial
	 * @param param
	 * @return
	 * @throws GrowUpDAOException
	 */
	public final List<DTO> selectAll(
			final Connection conn,
			final boolean isClausulaLike,
			final Integer indiceInicial,
			final ParametroFactory param) throws GrowUpDAOException {
		return selectAll(conn, isClausulaLike, indiceInicial, param, new ArrayList<DTO>());
	}
	
	/**
	 * M�todo utilizado para retornar todos os registros que atendem aos
	 * crit�rios do objeto de transfer�ncia informado (dto) juntamente com o
	 * objeto de parametriza��o da camanda DAO.
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o com o banco.
	 * @param isClausulaLike
	 *            determina se o select usar� a clausula Like ou Where
	 * @param indiceInicial
	 *            Indice inicial para fazer a pagina��o.
	 * @param param
	 *            variavel do tipo ParametroFactory
	 * @return Uma Collection com todos os dto�s.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum falha durante a pesquisa.
	 */
	public final <OUT> List<OUT> selectAll(
			final Connection conn,
			final boolean isClausulaLike,
			final Integer indiceInicial,
			final ParametroFactory param,
			final List<OUT> lista) throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		int MAX_REGISTROS_RETORNADOS = Constantes.VARIAVEL_MAX_REGISTROS_RETORNADOS;
		try {

//			lista = new ArrayList<DTO>();

			if (this.getSessao() != null && this.getSessao().getUsuario()!= null) {
				MAX_REGISTROS_RETORNADOS = this.getSessao().getUsuario().getQtdRegistros();
			}
			param.setPaginacao(indiceInicial, MAX_REGISTROS_RETORNADOS);

			if (isClausulaLike && param.isClausulaLike()) {
				pstmt = param.getLikeStatement(conn);
			} else {
				pstmt = param.getStatement(conn);
			}

			ResultSet rs = pstmt.executeQuery();

			// Encontrou registro?
			@SuppressWarnings("unchecked")
			DtoBuilder<OUT> builder = param.getBuilder();
			while (rs.next()) {
				// Popula informa��es da empresa
				builder.addDto(lista, builder.getDTO(rs, false));
				//lista.add(builder.getDTO(rs, false));
			}

			return lista;

		} catch (SQLException e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(
					e.getMessage(),
					"ORA-",
					Constantes.ORA_CODE), e.getMessage());
		}
	}

	public void addDto(List<DTO> lista, DTO dto) {
		lista.add(dto);
	}
	

	/**
	 * M�todo utilizado para retornar todos os registros que atendem os
	 * crit�rios do objeto de transfer�ncia informado.
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o com o banco.
	 * @param dto
	 *            Cont�m a informa��o do registro a ser encontrado no banco de
	 *            dados. Se o objeto 'dto' for nulo, este m�todo retorna todos
	 *            os valores referentes a este business object, ou seja, ser�
	 *            feito um select * no banco.
	 * @return Quantidade de registros
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum falha durante a pesquisa.
	 */
	public final Long count(final Connection conn, final DTO dto) throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			ParametroFactory param = this.setParam(dto);

			String[] qrySelect = param.getQuery().toString().toUpperCase().split(",");
			StringBuffer qryCount = new StringBuffer();
			qryCount.append(qrySelect[0].replace("SELECT", "SELECT COUNT (").concat(") "));
			qryCount.append(qrySelect[qrySelect.length - 1]
					.substring(qrySelect[qrySelect.length - 1].indexOf("FROM")));
			param.setQuery(qryCount, true);

			if (param.isClausulaLike()) {
				pstmt = param.getLikeStatement(conn);
			} else {
				pstmt = param.getStatement(conn);
			}

			rs = pstmt.executeQuery();
			if (rs.next()) {
				return rs.getLong(1);
			} else {
				return null;
			}

		} catch (SQLException e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(
					e.getMessage(),
					"ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e) {
				throw new GrowUpDAOException(StringUtil.extractString(
						e.getMessage(),
						"ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}
	}

	/**
	 * 
	 * M�todo utilizado para encontrar um registro de acordo com os dados
	 * informados no objeto de transfer�ncia.
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o com o banco.
	 * @param dto
	 *            Cont�m a informa��o do registro a ser encontrado no banco de
	 *            dados.
	 * @return Um transfer object populado com a informa��o do registro a ser
	 *         encontrado.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro durante a pesquisa.
	 */
	public final DTO find(final Connection conn, final DTO dto) throws GrowUpDAOException {
		DTO ldto = null;

		try {

			ParametroFactory param = this.setParam(dto);

			PreparedStatement pstmt = param.getStatement(conn);
			ResultSet rs = pstmt.executeQuery();

			// Encontrou registro?
			if (rs.next()) {
				// Popula informa��es
				ldto = this.getDTO(rs, true);
			}

			return ldto;

		} catch (SQLException e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(
					e.getMessage(),
					"ORA-",
					Constantes.ORA_CODE), e.getMessage());
		}
	}

	protected final Long getIdentity(final Statement pstmt) throws GrowUpDAOException {
		ResultSet rs = null;
		Long identity = null;

		try {
			rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				identity = rs.getLong(1);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(StringUtil.extractString(
					e.getMessage(),
					"ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} catch (Exception e) {
			throw new GrowUpDAOException(StringUtil.extractString(
					e.getMessage(),
					"ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				throw new GrowUpDAOException(StringUtil.extractString(
						e.getMessage(),
						"ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}

		return identity;

	}

	protected final StringBuffer getColunasDAO(final FbirdDAO<?> dao) {
		if (dao != null) {
			return new StringBuffer(dao.sql.toString().trim().substring(
					6,
					dao.sql.toString().trim().toUpperCase().indexOf("FROM")));
		}

		return null;
	}

	/**
	 * Acessa a daoFactory responsabel pela cria��o da Classe FbirdDAO
	 *
	 * @return Caso ocorra algum erro na opera��o
	 */
	protected DAOFactory getDaofactory() {
		return this.daoFactory;
	}

	/**
	 * TODO Comentar aqui.
	 *
	 * @param pstmt
	 * @throws GrowUpDAOException Caso ocorra algum erro na opera��o
	 */
	protected void close(Statement pstmt) throws GrowUpDAOException {
		try {
			if (pstmt != null)
				pstmt.close();
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		}
	}

	/**
	 * TODO Comentar aqui.
	 *
	 * @param pstmt
	 * @throws GrowUpDAOException Caso ocorra algum erro na opera��o
	 */
	protected void close(Statement pstmt, ResultSet rs) throws GrowUpDAOException {
		close(pstmt);

		try {
			if (rs != null)
				rs.close();
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		}
	}
	

	/**
	 * Verifica se o perfil do usuario logado � maior que o perfil passado
	 * 
	 * Exemplo:  Se Peril for Empresa < Perfil Usuario
	 * @param perfil
	 * @return
	 */
	public boolean isPerfilUsuarioMaior(TipoPerfil tipo) {
		return isPerfilUsuarioMaior(tipo, getCodigoPerfil());
	}

	/**
	 * Verifica se o perfil do usuario logado � maior que o perfil passado
	 * 
	 * @param perfil
	 * @param codigoPerfil
	 * @return
	 */
	public boolean isPerfilUsuarioMaior(TipoPerfil perfil, String codigoPerfil) {
		return isNotBlank(codigoPerfil) && codigoPerfil.compareTo(TipoPerfil.EMPRESA.toString()) > 0;
	}

	
	/**
	 * Verifica se o usuario logado � admin
	 * @return
	 */
	public boolean isUsuarioAdmin() {
		return TipoPerfil.ADMINISTRADOR.toString().equals(getCodigoPerfil());
	}
	
	protected String getCodigoPerfil() {
		final UsuarioDTO usuarioSessao = getUsuarioSessao();
		if (usuarioSessao != null)
			return usuarioSessao.getCodigoPerfil();
		return "";
	}

	/**
	 * @return
	 */
	public String getCodigoUsuarioSessao() {
		final UsuarioDTO usuarioSessao = getUsuarioSessao();
		if (usuarioSessao != null)
			return usuarioSessao.getCodigo();
			
		return null;
	}
	
	public UsuarioDTO getUsuarioSessao() {
		if (getSessao() != null)
			if (getSessao().getUsuario() != null)
				return getSessao().getUsuario();
						
		return null;
	}
	
}
