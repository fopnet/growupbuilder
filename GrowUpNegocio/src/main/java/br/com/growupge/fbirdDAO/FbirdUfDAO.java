/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.fbirdDAO;

import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.dto.UnidadeFederativaDTO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.ConsultaDAO;

/**
 * Classe DAO para gerenciar informa��es pertinentes a Empresa.
 * 
 * @author Felipe
 * 
 */
public class FbirdUfDAO extends FbirdDAO<UnidadeFederativaDTO> implements ConsultaDAO<UnidadeFederativaDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            objeto de f�brica.
	 */
	public FbirdUfDAO(final DAOFactory daofactory) {
		super(daofactory);

		sql.append("select");
		sql.append("    UF.UF_CD_UF,");
		sql.append("    UF_NM_UF ");
		sql.append(" from ");
		sql.append("    UF ");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final UnidadeFederativaDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {

		UnidadeFederativaDTO dto = null;

		try {
			dto = new UnidadeFederativaDTO();

			dto.codigo = rs.getString("UF_CD_UF");
			dto.nome = rs.getString("UF_NM_UF");

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final UnidadeFederativaDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			param = this.setParametro(param, dto);

			if (param.getParametros().isEmpty()) {
				param.setParam("UF.UF_CD_UF", ParametroFactory.CODIGO_INEXISTENTE);
			}
		}
		
		param.setOrdem("UF_NM_UF", TipoOrdemDAO.ASC);

		return param;
	}
	
	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.fbirdDAO.FbirdDAO#setParametro(br.com.growupge.factory.ParametroFactory, java.lang.Object)
	 */
	@Override
	protected ParametroFactory setParametro(final ParametroFactory param, final UnidadeFederativaDTO dto) throws GrowUpDAOException {
		if (dto.codigo != null) {
			param.setParam("UF.UF_CD_UF", dto.codigo);
		} else {
			param.setParam("UF_NM_UF", dto.nome);
		}
		return param;
	}


}
