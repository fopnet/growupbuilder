/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 05/01/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 05/01/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.fbirdDAO;

import static br.com.growupge.utility.Reflection.isCodigoValido;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.ArquivoDTO;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.dto.RestricaoDTO;
import br.com.growupge.dto.StatusPedidoSetter;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.dto.VeiculoDTO;
import br.com.growupge.enums.TipoOperadorDAO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.enums.TipoPerfil;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.interfacesdao.DtoBuilder;
import br.com.growupge.interfacesdao.PedidoVeiculoDAO;
import br.com.growupge.interfacesdao.SequenciaDAO;
import br.com.growupge.utility.DateUtil;

/**
 * Classe DAO para gerenciar informa��es pertinentes a perfis.
 * 
 * @author Felipe
 * 
 */
public class FbirdPedidoVeiculoDAO extends FbirdDAO<PedidoVeiculoDTO> implements PedidoVeiculoDAO  {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            F�brica de DAO.
	 */
	public FbirdPedidoVeiculoDAO(final DAOFactory daofactory) {
		super(daofactory);

		// StringBuffer colunasUsuario = this.getColunasDAO(new
		// FbirdUsuarioDAO(daofactory));

		this.sql.append(" select ");
		this.sql.append("  PEDIDO.PEDI_CD_PEDIDO, ");
		this.sql.append("  VEIC_CD_PLACA, ");
		this.sql.append("  PEDIDO.USUA_CD_USUARIO_CRIACAO, ");
		this.sql.append("  PEDIDO.USUA_CD_USUARIO_ALTERACAO, ");
		this.sql.append("  PEDI_DH_CRIACAO, ");
		this.sql.append("  PEDI_DH_ALTERACAO, ");
		this.sql.append("  PEDI_IN_STATUS, ");
		this.sql.append("  PEDI_DS_PEDIDO, ");
		this.sql.append("  PEDI_NR_GAVETA, ");

		this.sql.append("    SERVICO.SERV_CD_SERVICO,");
		this.sql.append("    SERV_NM_SERVICO, ");
		this.sql.append("    SERV_DS_SERVICO, ");
		this.sql.append("    SERV_IN_TRAMITADO, ");

		this.sql.append("    ARQUIVO.ARQU_CD_ARQUIVO,");
		this.sql.append("    ARQU_NM_ARQUIVO, ");

		this.sql.append("    CLIENTE.CLIE_CD_CLIENTE,");
		this.sql.append("    CLIE_NM_CLIENTE, ");
		this.sql.append("    CLIE_DS_CLIENTE, ");
		this.sql.append("    CLIE_NM_ARQUIVO ");

		this.sql.append(" from PEDIDO");
		// this.sql.append(" INNER JOIN USUARIO ON ");
		// this.sql.append(" PEDIDO.USUA_CD_USUARIO = USUARIO.USUA_CD_USUARIO");
		// this.sql.append(" INNER JOIN PERFIL ON ");
		// this.sql.append(" USUARIO.PERF_CD_PERFIL = PERFIL.PERF_CD_PERFIL");
		// this.sql.append(" LEFT JOIN EMPRESA ON ");
		// this.sql.append(" USUARIO.EMPR_CD_EMPRESA =
		// EMPRESA.EMPR_CD_EMPRESA");
		this.sql.append(" LEFT JOIN CLIENTE  ON ");
		this.sql.append(" 	CLIENTE.CLIE_CD_CLIENTE = PEDIDO.CLIE_CD_CLIENTE");
		this.sql.append(" INNER JOIN SERVICO  ON ");
		this.sql.append(" 	SERVICO.SERV_CD_SERVICO = PEDIDO.SERV_CD_SERVICO");
		this.sql.append(" LEFT JOIN ARQUIVO  ON ");
		this.sql.append(" 	ARQUIVO.ARQU_CD_ARQUIVO = PEDIDO.ARQU_CD_ARQUIVO");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.FbirdDAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final PedidoVeiculoDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {
		PedidoVeiculoDTO dto = null;
		try {

			dto = new PedidoVeiculoDTO();
			dto.setCodigo(rs.getLong("PEDI_CD_PEDIDO"));
			dto.setVeiculo(new VeiculoDTO(rs.getString("VEIC_CD_PLACA")));
			dto.setDescricao(rs.getString("PEDI_DS_PEDIDO"));
			dto.setServico(this.daoFactory.getServicoDAO().getDTO(rs, false));
			dto.setArquivo(this.daoFactory.getArquivoDAO().getDTO(rs, false));
			StatusPedidoSetter.setStatus(rs, isEager, dto);
			//dto.setStatus(rs.getString("PEDI_IN_STATUS"));
			dto.setNrGaveta(rs.getInt("PEDI_NR_GAVETA"));
			// novos dados

			if (rs.getLong("ARQU_CD_ARQUIVO") > 0) {
				dto.setArquivo(new ArquivoDTO(rs.getLong("ARQU_CD_ARQUIVO")));
				dto.getArquivo().setNome(rs.getString("ARQU_NM_ARQUIVO"));
			}

			dto.setUsuarioCadastro(new UsuarioDTO());
			dto.getUsuarioCadastro().setCodigo(rs.getString("USUA_CD_USUARIO_CRIACAO"));
			dto.setDataCadastro(rs.getTimestamp("PEDI_DH_CRIACAO"));
			if (rs.getTimestamp("PEDI_DH_ALTERACAO") != null) {
				dto.setUsuarioAlteracao(new UsuarioDTO());
				dto.getUsuarioAlteracao().setCodigo(rs.getString("USUA_CD_USUARIO_CRIACAO"));
				dto.setDataAlteracao(rs.getTimestamp("PEDI_DH_ALTERACAO"));
			}

			dto.setCliente(this.daoFactory.getClienteDAO().getDTO(rs, false));
			
			if (isEager) {
				RestricaoDTO param = new RestricaoDTO(dto);
				DAO<RestricaoDTO> restricaoDAO = daoFactory.getRestricaoDAO();
				dto.setRestricoes(restricaoDAO.selectAll(daoFactory.getConnection(), param));
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PedidoVeiculoDTO delete(final Connection conn, final PedidoVeiculoDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append("delete from PEDIDO ");
			sql.append("where PEDI_CD_PEDIDO = ?");
			pstmt = conn.prepareStatement(sql.toString());

			/* Limpa valores antigos dos par�metros. */
			pstmt.clearParameters();
			pstmt.setLong(1, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.REGISTRO_EXCLUIDO_PREVIAMENTE);
			}
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}

		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PedidoVeiculoDTO insert(final Connection conn, final PedidoVeiculoDTO dto)
			throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {
			SequenciaDAO seq = daoFactory.getSequenciaDAO();
			if (!isCodigoValido(dto))
				dto.setCodigo(seq.getProximaSequencia("GEN_PEDI_CD_PEDIDO"));

			sql.append("insert into PEDIDO (");
			sql.append("  PEDI_CD_PEDIDO,");
			sql.append("  PEDI_DS_PEDIDO,");
			sql.append("  PEDI_DH_CRIACAO,");
			sql.append("  PEDI_DH_ALTERACAO,");
			sql.append("  PEDI_IN_STATUS,");
			sql.append("  VEIC_CD_PLACA,");
			sql.append("  USUA_CD_USUARIO_CRIACAO, ");
			sql.append("  USUA_CD_USUARIO_ALTERACAO, ");
			sql.append("  SERV_CD_SERVICO, ");
			sql.append("  CLIE_CD_CLIENTE, ");
			sql.append("  PEDI_NR_GAVETA, ");
			sql.append("  ARQU_CD_ARQUIVO ");
			sql.append(" ) values (?,?,CURRENT_TIMESTAMP,NULL,?,?,?,NULL,?,?,?,?)");
			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			pstmt.setLong(inc++, dto.getCodigo());
			pstmt.setString(inc++, dto.getDescricao());
			pstmt.setString(inc++, dto.getStatus().getTipo());
			pstmt.setString(inc++, dto.getPlaca());
			pstmt.setString(inc++, dto.getUsuarioCadastro().getCodigo());
			pstmt.setLong(inc++, dto.getServico().getCodigo());
			if (!isCodigoValido(dto.getCliente()))
				pstmt.setNull(inc++, java.sql.Types.INTEGER);
			else 
				pstmt.setLong(inc++, dto.getCliente().getCodigo());
				
			pstmt.setInt(inc++, dto.getNrGaveta());
			if (!isCodigoValido(dto.getArquivo()))
				pstmt.setNull(inc++, java.sql.Types.INTEGER);
			else 
				pstmt.setLong(inc++, dto.getArquivo().getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PedidoVeiculoDTO update(final Connection conn, final PedidoVeiculoDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();

		try {
			sql.append(" update PEDIDO set ");
			sql.append("  PEDI_DS_PEDIDO=?,");
			sql.append("  PEDI_DH_ALTERACAO= CURRENT_TIMESTAMP,");
			sql.append("  PEDI_IN_STATUS=?,");
			sql.append("  VEIC_CD_PLACA=?,");
			sql.append("  USUA_CD_USUARIO_ALTERACAO=?, ");
			sql.append("  SERV_CD_SERVICO=?, ");
			sql.append("  CLIE_CD_CLIENTE=?, ");
			sql.append("  PEDI_NR_GAVETA=?, ");
			sql.append("  ARQU_CD_ARQUIVO=? ");
			sql.append(" where PEDI_CD_PEDIDO = ?");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			pstmt.setString(inc++, dto.getDescricao());
			pstmt.setString(inc++, dto.getStatus().getTipo());
			pstmt.setString(inc++, dto.getPlaca());
			pstmt.setString(inc++, dto.getUsuarioAlteracao().getCodigo());
			pstmt.setLong(inc++, dto.getServico().getCodigo());
			
			if (!isCodigoValido(dto.getCliente()))
				pstmt.setNull(inc++, java.sql.Types.INTEGER);
			else 
				pstmt.setLong(inc++, dto.getCliente().getCodigo());
			
			pstmt.setInt(inc++, dto.getNrGaveta());
			
			if (!isCodigoValido(dto.getArquivo()))
				pstmt.setNull(inc++, java.sql.Types.INTEGER);
			else 
				pstmt.setLong(inc++, dto.getArquivo().getCodigo());
			
			pstmt.setLong(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.FbirdDAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final PedidoVeiculoDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			param = this.setParametro(param, dto);

			if (param.getParametros().isEmpty()) {
				param.setParam("PEDIDO.PEDI_CD_PEDIDO", ParametroFactory.CODIGO_INEXISTENTE);
			}
		}

		// Ordena��o padr�o.
		param.setOrdem("PEDIDO.PEDI_CD_PEDIDO", TipoOrdemDAO.DESC);
		param.setOrdem("PEDI_DH_CRIACAO", TipoOrdemDAO.DESC);

		return param;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.fbirdDAO.FbirdDAO#setParametro(br.com.growupge.factory.ParametroFactory,
	 *      java.lang.Object)
	 */
	@Override
	protected ParametroFactory setParametro(final ParametroFactory param, final PedidoVeiculoDTO dto)
			throws GrowUpDAOException {
		/* Para perfis de usu�rios somente exibe os pedidos feitos pelo pr�prio */
		if (this.getSessao() != null
				&& isPerfilUsuarioMaior(TipoPerfil.EMPRESA)) {
			param.setParam("PEDIDO.USUA_CD_USUARIO_CRIACAO", this.getSessao().getUsuario().getCodigo());
		}/*
		 * else if (dto != null) { param.setParam("USUA_CD_USUARIO_CRIACAO",
		 * dto.usuarioCadastro == null || dto.usuarioCadastro.codigo == null ?
		 * null : dto.usuarioCadastro.codigo); }
		 */

		if (dto.getCodigo() != 0) {
			param.setParam("PEDIDO.PEDI_CD_PEDIDO", dto.getCodigo());
		} else {
			if (dto.getUsuarioCadastro() != null) {
				param.setParam("PEDIDO.USUA_CD_USUARIO_CRIACAO", dto.getUsuarioCadastro().getCodigo());
			}
			param.setParam("PEDI_DS_PEDIDO", dto.getDescricao());
			param.setParam("VEIC_CD_PLACA", dto.getPlaca());
			param.setParam("PEDI_IN_STATUS", dto.getStatus());

			if (dto.getServico() != null) {
				param.setParam(
						"SERVICO.SERV_CD_SERVICO",
						dto.getServico().getCodigo() != 0 ? dto.getServico().getCodigo() : null);
			}
			if (dto.getArquivo() != null) {
				param.setParam(
						"ARQUIVO.ARQU_CD_ARQUIVO",
						dto.getArquivo().getCodigo() != 0 ? dto.getArquivo().getCodigo() : null);
				param.setParam("ARQU_NM_ARQUIVO", dto.getArquivo().getNome());
			}

			if (dto.getFiltroDataInicial() != null) {
				param.setParam(
						"cast(PEDI_DH_CRIACAO as DATE)",
						TipoOperadorDAO.MAIOR_IGUAL,
						dto.getFiltroDataInicial());
			}
			if (dto.getFiltroDataFinal() != null) {
				//Preciso aumentar um dia por causa da hora que chega 00:00:00
				DateUtil dm = DateUtil.getDateManagerInstance(dto.getFiltroDataFinal());
				dm.addDays(1);
				dto.setFiltroDataFinal(dm.getDateTime());
				param.setParam(
						"CAST(PEDI_DH_CRIACAO AS DATE)",
						TipoOperadorDAO.MENOR_IGUAL,
						dto.getFiltroDataFinal());
			}

			if (dto.getCliente() != null) {
				param.setParam("CLIE_NM_CLIENTE", dto.getCliente().getNomeMaiusculo());
			}
		}

		return param;
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.interfacesdao.PedidoVeiculoDAO#getPagamentosPedidos()
	 */
	@Override
	public ParametroFactory getPagamentosPedidos(PedidoVeiculoDTO dto) throws GrowUpDAOException  {
		ParametroFactory param = ParametroFactory.getInstance(getPagamentosPedidosBuilder());
		
		StringBuffer sql = new StringBuffer();
		sql.append(" select ");
		sql.append("  PEDIDO.PEDI_CD_PEDIDO, ");
		sql.append("  VEIC_CD_PLACA, ");
		sql.append("  PEDIDO.USUA_CD_USUARIO_CRIACAO, ");
		sql.append("  PEDIDO.USUA_CD_USUARIO_ALTERACAO, ");
		sql.append("  PEDI_DH_CRIACAO, ");
		sql.append("  PEDI_DH_ALTERACAO, ");
		sql.append("  PEDI_IN_STATUS, ");
		sql.append("  PEDI_DS_PEDIDO, ");
		sql.append("  PEDI_NR_GAVETA, ");
		sql.append("    PAGT_CD_PAGAMENTO,");
		sql.append("    USUARIO.USUA_NM_EMAIL, ");
		sql.append("    PAGT_DS_DESCRICAO, ");
		sql.append("    PAGT_DH_SOLICITACAO, ");
		sql.append("    COALESCE(PAGT_VL_VALOR,");
		sql.append("    		(SELECT VAR_VL_VALOR FROM VARIAVEL");
		sql.append("     		WHERE VAR_CD_VARIAVEL = 'VAL_UNIT_PEDIDO')) PAGT_VL_VALOR,");
		sql.append("    PAGT_DH_PAGAMENTO, ");
		sql.append("    PAGT_CD_TX, ");
		sql.append("    PAGT_IN_STATUS_TX, ");
		sql.append("    PAGT_CD_RESPOSTA, ");
		sql.append("    PAGT_IN_STATUS_RESPOSTA, ");
		sql.append("    PAGT_CD_CORRELACAO ");
		
		sql.append("  FROM PEDIDO  LEFT JOIN PAGAMENTO ON ");
		sql.append("  	PAGAMENTO.PEDI_CD_PEDIDO = PEDIDO.PEDI_CD_PEDIDO ");
		sql.append("    INNER JOIN USUARIO ON ");
		sql.append("    PEDIDO.USUA_CD_USUARIO_CRIACAO = USUARIO.USUA_CD_USUARIO ");
		
		param.setQuery(sql);

		param.setParam("PEDIDO.USUA_CD_USUARIO_CRIACAO", super.getCodigoUsuarioSessao());
		param.setParam("PEDI_IN_STATUS", dto.getStatus());
		
		// Ordena��o padr�o.
		param.setOrdem("PEDIDO.PEDI_CD_PEDIDO", TipoOrdemDAO.DESC);
		param.setOrdem("PEDI_DH_CRIACAO", TipoOrdemDAO.DESC);
		
		return param;
	}

	/**
	 * @return
	 */
	private DtoBuilder<PedidoVeiculoDTO> getPagamentosPedidosBuilder() {
		
		return new DtoBuilder<PedidoVeiculoDTO>() {
			private PedidoVeiculoDTO ultimoPedido = null;
			
			@Override
			public PedidoVeiculoDTO getDTO(ResultSet rs, boolean isEager) throws GrowUpDAOException {
				PedidoVeiculoDTO dto = null;
				try {

					dto = new PedidoVeiculoDTO();
					dto.setCodigo(rs.getLong("PEDI_CD_PEDIDO"));
					dto.setVeiculo(new VeiculoDTO(rs.getString("VEIC_CD_PLACA")));
					dto.setDescricao(rs.getString("PEDI_DS_PEDIDO"));
					
					StatusPedidoSetter.setStatus(rs, isEager, dto);
					//dto.setStatus(rs.getString("PEDI_IN_STATUS"));
					
					dto.setNrGaveta(rs.getInt("PEDI_NR_GAVETA"));
					// novos dados

					dto.setUsuarioCadastro(new UsuarioDTO());
					dto.getUsuarioCadastro().setCodigo(rs.getString("USUA_CD_USUARIO_CRIACAO"));
					dto.setDataCadastro(rs.getTimestamp("PEDI_DH_CRIACAO"));
					if (rs.getTimestamp("PEDI_DH_ALTERACAO") != null) {
						dto.setUsuarioAlteracao(new UsuarioDTO());
						dto.getUsuarioAlteracao().setCodigo(rs.getString("USUA_CD_USUARIO_CRIACAO"));
						dto.setDataAlteracao(rs.getTimestamp("PEDI_DH_ALTERACAO"));
					}

					dto.adicionarPagamentos( daoFactory.getPagamentoDAO().getDTO(rs, true) );

				} catch (SQLException e) {
					throw new GrowUpDAOException(e);
				}
				return dto;
			}

			@Override
			public void addDto(final List<PedidoVeiculoDTO> lista, final PedidoVeiculoDTO dto) {
				
				// se o ultimo pedido for igual ao ultimo, � porque existem  + 1 pagamento para o mesmo
				if (dto.equals(ultimoPedido)) {
					ultimoPedido.adicionarPagamentos( dto.getPagamentos().iterator().next() );
				} else { // caso o ultimo pedido n�o seja igual a corrente, 
					lista.add(dto);
				}
				
				ultimoPedido = dto;
				
			}
			
		};
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PedidoVeiculoDTO updateStatus(final Connection conn, final PedidoVeiculoDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();

		try {
			sql.append(" update PEDIDO set ");
			sql.append("  PEDI_DH_ALTERACAO= CURRENT_TIMESTAMP,");
			sql.append("  PEDI_IN_STATUS=? ");
			sql.append(" where PEDI_CD_PEDIDO = ?");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			pstmt.setString(inc++, dto.getStatus().getTipo());
			pstmt.setLong(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}

		return dto;
	}
}
