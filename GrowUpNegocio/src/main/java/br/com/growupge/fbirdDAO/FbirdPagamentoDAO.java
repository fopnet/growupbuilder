/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 08/04/2014
 *
 * Historico de Modifica��o:
 * =========================
 * 08/04/2014 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.fbirdDAO;

import static br.com.growupge.utility.Reflection.isCodigoValido;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.PagamentoBoletoDTO;
import br.com.growupge.dto.PagamentoDTO;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.dto.StatusPedidoSetter;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.PagamentoDAO;

/**
 * Classe DAO para gerenciar informa��es pertinentes a Empresa.
 * 
 * @author Felipe
 * 
 */
public class FbirdPagamentoDAO extends FbirdDAO<PagamentoDTO> implements PagamentoDAO {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            objeto de f�brica.
	 */
	public FbirdPagamentoDAO(final DAOFactory daofactory) {
		super(daofactory);

		sql.append("select");
		sql.append("    PAGT_CD_PAGAMENTO,");
		sql.append("    PEDIDO.PEDI_CD_PEDIDO, ");
		sql.append("    PEDIDO.VEIC_CD_PLACA, ");
		sql.append("    PEDIDO.USUA_CD_USUARIO_CRIACAO, ");
		sql.append("    PEDIDO.PEDI_DH_CRIACAO, ");
		sql.append("    PEDIDO.PEDI_IN_STATUS, ");
		sql.append("    USUARIO.USUA_NM_EMAIL, ");
		sql.append("    PAGT_DS_DESCRICAO, ");
		sql.append("    PAGT_DH_SOLICITACAO, ");
		sql.append("    COALESCE(PAGT_VL_VALOR,");
		sql.append("    		(SELECT VAR_VL_VALOR FROM VARIAVEL");
		sql.append("     		WHERE VAR_CD_VARIAVEL = 'VAL_UNIT_PEDIDO')) PAGT_VL_VALOR,");
		sql.append("    PAGT_DH_PAGAMENTO, ");
		sql.append("    PAGT_CD_TX, ");
		sql.append("    PAGT_IN_STATUS_TX, ");
		sql.append("    PAGT_CD_RESPOSTA, ");
		sql.append("    PAGT_IN_STATUS_RESPOSTA, ");
		sql.append("    PAGT_CD_CORRELACAO ");
		sql.append(" from  ");
		sql.append("    PAGAMENTO RIGHT JOIN PEDIDO ON");
		sql.append("    PEDIDO.PEDI_CD_PEDIDO = PAGAMENTO.PEDI_CD_PEDIDO ");
		sql.append("    INNER JOIN USUARIO ON ");
		sql.append("    PEDIDO.USUA_CD_USUARIO_CRIACAO = USUARIO.USUA_CD_USUARIO ");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PagamentoDTO delete(final Connection conn, final PagamentoDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;

		try {
			sql.append("delete from PAGAMENTO ");
			sql.append("where PAGT_CD_PAGAMENTO = ?");
			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setString(1, dto.getCodigo());
			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;

	}
	
	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final int deletePagamentosPayPal(final Connection conn, final PedidoVeiculoDTO dto) throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;
		int result = 0;
		
		try {
			sql.append(" delete from PAGAMENTO ");
			sql.append(" where PEDI_CD_PEDIDO = ?");
			sql.append(" and PAGT_CD_CORRELACAO <> ? "); //nao seja boleto
			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setLong(1, dto.getCodigo());
			pstmt.setString(2, PagamentoBoletoDTO.BOLETO_CORRELACAO_ID);
			result = pstmt.executeUpdate();
//			if (result == 0) {
//				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
//			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.interfacesdao.PagamentoDAO#delete(java.sql.Connection, br.com.growupge.dto.PedidoVeiculoDTO)
	 */
	@Override
	public int delete(Connection conn, PedidoVeiculoDTO dto) throws GrowUpDAOException {
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;
		int result = 0;
		
		try {
			sql.append(" delete from PAGAMENTO ");
			sql.append(" where PEDI_CD_PEDIDO = ?");
			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setLong(1, dto.getCodigo());
			result = pstmt.executeUpdate();
//			if (result == 0) {
//				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
//			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return result;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PagamentoDTO insert(final Connection conn, final PagamentoDTO dto)
			throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuilder sql = new StringBuilder();

		try {
			
			sql.append("INSERT INTO PAGAMENTO (");
			sql.append("    PAGT_CD_PAGAMENTO, ");
			sql.append("    PEDI_CD_PEDIDO, ");
			sql.append("    PAGT_DS_DESCRICAO, ");
			sql.append("    PAGT_DH_SOLICITACAO, ");
			sql.append("    PAGT_VL_VALOR, ");
			sql.append("    PAGT_CD_CORRELACAO ) ");
//			sql.append("    PAGT_DH_PAGAMENTO, ");
//			sql.append("    PAGT_CD_TX, ");
//			sql.append("    PAGT_IN_STATUS_TX, ");
//			sql.append("    PAGT_CD_RESPOSTA, ");
//			sql.append("    PAGT_IN_STATUS_RESPOSTA ) ");
			sql.append(" values ");
			sql.append(" (?,?,?, ? ,?,?) "); 
			/*CURRENT_TIMESTAMP nao pode ser data  atual, porque via paypal, 
			 *a data solicitacao ja vem preenchida pelo sistema deles.
			 */

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			//Required
			pstmt.setString(inc++, dto.getCodigo());
			pstmt.setLong(inc++, dto.getPedido().getCodigo());
			pstmt.setString(inc++, dto.getDescricao());
			
			if (dto.getDataSolicitacao() == null)
				pstmt.setNull(inc++, Types.TIMESTAMP);
			else
				pstmt.setTimestamp(inc++, new Timestamp(dto.getDataSolicitacao().getTime()));
			
			pstmt.setDouble(inc++, dto.getValor());
			pstmt.setString(inc++, dto.getCorrelacaoId());
			//Opcional

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PagamentoDTO update(final Connection conn, final PagamentoDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();
		PreparedStatement pstmt = null;
		try {
			sql.append("UPDATE PAGAMENTO SET ");
			sql.append("    PAGT_DH_PAGAMENTO =?,");
			sql.append("    PAGT_CD_TX =?,");
			sql.append("    PAGT_IN_STATUS_TX =?,");
			sql.append("    PAGT_CD_RESPOSTA =?, ");
			sql.append("    PAGT_IN_STATUS_RESPOSTA =? ");
			sql.append(" WHERE PAGT_CD_PAGAMENTO = ? ");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;

			if (dto.getDataPagamento() != null)
				pstmt.setTimestamp(inc++, new Timestamp(dto.getDataPagamento().getTime()));
			else 
				pstmt.setNull(inc++, java.sql.Types.TIMESTAMP);
			
			pstmt.setString(inc++, dto.getTransacaoId());
			pstmt.setString(inc++, dto.getTransacaoStatus());
			pstmt.setString(inc++, dto.getResposta());
			pstmt.setString(inc++, dto.getStatus());
			pstmt.setString(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final PagamentoDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {

		PagamentoDTO dto = null;

		try {
			PedidoVeiculoDTO pedido = new PedidoVeiculoDTO(rs.getLong("PEDI_CD_PEDIDO"));
			
			//dto = new PagamentoDTO(new PedidoVeiculoDTO(rs.getLong("PEDI_CD_PEDIDO")));
			dto = PagamentoDTO.instanciar(rs.getString("PAGT_CD_CORRELACAO"), pedido);
			
			if (isEager) { 
				pedido = daoFactory.getPedidoVeiculoDAO().find(daoFactory.getConnection(), dto.getPedido()); 
				dto.setPedido(pedido);

//				dto.setDataPagamento(rs.getTimestamp("PAGT_DH_PAGAMENTO"));
				dto.setTransacaoId(rs.getString("PAGT_CD_TX"));
				dto.setTransacaoStatus(rs.getString("PAGT_IN_STATUS_TX"));
				dto.setResposta(rs.getString("PAGT_CD_RESPOSTA"));
			} else {
				dto.getPedido().setPlaca(rs.getString("VEIC_CD_PLACA"));
				dto.getPedido().setUsuarioCadastro(new UsuarioDTO(rs.getString("USUA_CD_USUARIO_CRIACAO")));
//				dto.getPedido().getUsuarioCadastro().setEmail(rs.getString("USUA_NM_EMAIL"));
				dto.getPedido().setDataCadastro(rs.getTimestamp("PEDI_DH_CRIACAO"));
				
				StatusPedidoSetter.setStatus(rs, isEager, dto.getPedido());
//				dto.getPedido().setStatus(rs.getString("PEDI_IN_STATUS"));
			}
			
			dto.setCodigo(rs.getString("PAGT_CD_PAGAMENTO"));
			
			if (rs.getTimestamp("PAGT_DH_SOLICITACAO") != null)
				dto.setDataSolicitacao(rs.getTimestamp("PAGT_DH_SOLICITACAO"));
			
			dto.setDataPagamento(rs.getTimestamp("PAGT_DH_PAGAMENTO"));
			if (rs.getDouble("PAGT_VL_VALOR") > 0)
				dto.setValor(rs.getDouble("PAGT_VL_VALOR"));
			dto.setStatus(rs.getString("PAGT_IN_STATUS_RESPOSTA"));
			dto.setDescricao(rs.getString("PAGT_DS_DESCRICAO"));
			
			

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final PagamentoDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			param = this.setParametro(param, dto);

			if (param.getParametros().isEmpty()) {
				param.setParam("PAGT_CD_PAGAMENTO", String.valueOf(ParametroFactory.CODIGO_INEXISTENTE));
			}
		}

		param.setOrdem("PEDIDO.PEDI_CD_PEDIDO", TipoOrdemDAO.DESC);

		return param;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.fbirdDAO.FbirdDAO#setParametro(br.com.growupge.factory.ParametroFactory, java.lang.Object)
	 */
	@Override
	protected ParametroFactory setParametro(final ParametroFactory param, final PagamentoDTO dto)
			throws GrowUpDAOException {
		if (isCodigoValido(dto)) {
			param.setParam("PAGT_CD_PAGAMENTO", dto.getCodigo());
		} else {
			if (dto.getPedido() != null) {
				
				if (isCodigoValido(dto.getPedido())) 
					param.setParam("PAGAMENTO.PEDI_CD_PEDIDO", dto.getPedido().getCodigo());
				
				if (isCodigoValido(dto.getPedido().getUsuarioCadastro()))
					param.setParam("PEDIDO.USUA_CD_USUARIO_CRIACAO", dto.getPedido().getUsuarioCadastro().getCodigo());
				
			}
			param.setParam("PAGT_CD_TX", dto.getTransacaoId());
			param.setParam("PAGT_CD_CORRELACAO", dto.getCorrelacaoId());//tipo do pagamento.// required no banco
		}
		return param;
	}

	

}
