/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 05/01/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 05/01/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.fbirdDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.VeiculoProprietarioDTO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;

/**
 * Classe DAO para gerenciar informa��es pertinentes a perfis.
 * 
 * @author Felipe
 * 
 */
public class FbirdVeiculoProprietarioDAO extends FbirdDAO<VeiculoProprietarioDTO> implements
		DAO<VeiculoProprietarioDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            F�brica de DAO.
	 */
	public FbirdVeiculoProprietarioDAO(final DAOFactory daofactory) {
		super(daofactory);

		StringBuffer colunaVeiculo = this.getColunasDAO(new FbirdVeiculoDAO(daofactory))
				.append(",");
		StringBuffer colunaProprietario = this.getColunasDAO(new FbirdProprietarioDAO(daofactory));

		this.sql.append(" select ");
		this.sql.append(colunaVeiculo);
		this.sql.append(colunaProprietario);
		this.sql.append(" from VEICULO_PROPRIETARIO ");
		this.sql.append(" INNER JOIN PROPRIETARIO ON ");
		this.sql.append("   PROPRIETARIO.PROP_CD_PROPRIETARIO = ");
		this.sql.append("   VEICULO_PROPRIETARIO.PROP_CD_PROPRIETARIO");
		this.sql.append(" INNER JOIN VEICULO ON");
		this.sql.append("   VEICULO_PROPRIETARIO.VEIC_CD_PLACA =");
		this.sql.append("   VEICULO.VEIC_CD_PLACA ");
		this.sql.append(" INNER JOIN MODELO_FIPE ON ");
		this.sql.append("   MODELO_FIPE.MODE_CD_MODELO = ");
		this.sql.append("   VEICULO.MODE_CD_MODELO");
		this.sql.append(" INNER JOIN MARCA_FIPE ON ");
		this.sql.append("   MODELO_FIPE.MARC_CD_MARCA = ");
		this.sql.append("   MARCA_FIPE.MARC_CD_MARCA");
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.FbirdDAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final VeiculoProprietarioDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {
		VeiculoProprietarioDTO dto = null;
		dto = new VeiculoProprietarioDTO();

		dto.setProprietario(getDaofactory().getProprietarioDAO().getDTO(rs, false));
		dto.setVeiculo(getDaofactory().getVeiculoDAO().getDTO(rs, false));

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final VeiculoProprietarioDTO delete(
			final Connection conn,
			final VeiculoProprietarioDTO dto) throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append("delete from VEICULO_PROPRIETARIO ");
			sql.append("where VEIC_CD_PLACA = ?");
			sql.append("AND PROP_CD_PROPRIETARIO = ?");
			pstmt = conn.prepareStatement(sql.toString());

			/* Limpa valores antigos dos par�metros. */
			pstmt.clearParameters();
			int inc = 1;
			pstmt.setString(inc++, dto.getVeiculo().getCodigo());
			pstmt.setLong(inc++, dto.getProprietario().getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.REGISTRO_EXCLUIDO_PREVIAMENTE);
			}
		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}

		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final VeiculoProprietarioDTO insert(
			final Connection conn,
			final VeiculoProprietarioDTO dto) throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {

			sql.append("insert into VEICULO_PROPRIETARIO (");
			sql.append("  PROP_CD_PROPRIETARIO,");
			sql.append("  VEIC_CD_PLACA ");
			sql.append(" ) values (?,?)");
			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			pstmt.setLong(inc++, dto.getProprietario().getCodigo());
			pstmt.setString(inc++, dto.getVeiculo().getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(e);
		} finally {
			close(pstmt);
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 *  @deprecated
	 */
	@Deprecated
	@Override
	public final VeiculoProprietarioDTO update(
			final Connection conn,
			final VeiculoProprietarioDTO dto) throws GrowUpDAOException {
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.FbirdDAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final VeiculoProprietarioDTO dto)
			throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			param = this.setParametro(param, dto);

			if (param.getParametros().isEmpty()) {
				param.setParam(
						"PROPRIETARIO.PROP_CD_PROPRIETARIO",
						ParametroFactory.CODIGO_INEXISTENTE);
			}
		}

		return param;

	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.fbirdDAO.FbirdDAO#setParametro(br.com.growupge.factory.ParametroFactory, java.lang.Object)
	 */
	@Override
	protected ParametroFactory setParametro(
			final ParametroFactory param,
			final VeiculoProprietarioDTO dto) throws GrowUpDAOException {
		if (dto.getProprietario() != null && dto.getProprietario().getCodigo() != 0) {
			param.setParam("PROPRIETARIO.PROP_CD_PROPRIETARIO", dto.getProprietario().getCodigo());
		}
		if (dto.getVeiculo() != null) {
			param.setParam("VEICULO.VEIC_CD_PLACA", dto.getVeiculo().getCodigo());
		}

		return param;
	}

}
