/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 *  Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : J2EE 1.4
 * Criado em  : 03/02/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 03/02/2006 - In�cio de tudo, por J�lio Vitorino
 *
 */

package br.com.growupge.gerenciadores;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import br.com.growupge.annotations.ErasureType;
import br.com.growupge.constantes.GCS;
import br.com.growupge.dto.ComandoDTO;
import br.com.growupge.dto.DTOPadrao;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.exception.IE;
import br.com.growupge.utility.StringUtil;

/**
 * Classe de Utilit�ria
 * 
 * @author Felipe Pina
 */
final class ComandoDTOInvoker {

	/**
	 * @param permissao
	 * @param param
	 * @return
	 * @throws GrowUpException
	 */
	public final static Object invocarMetodo(final Object objeto, final ComandoDTO cmd) throws GrowUpException {

		try {
			Class<?>[] types = new Class[cmd.getParametrosLista().size()];

			boolean isMetodoConsulta = GCS.METODOS_CONSULTA.contains(cmd.getPermissao());
			
			for (int i = 0; i < cmd.getParametrosLista().size(); i++) {
				boolean isTypeFound = false;
				if (isMetodoConsulta && cmd.getParametrosLista().get(i) instanceof DTOPadrao) {
					ErasureType erasureType = objeto.getClass().getAnnotation(ErasureType.class);
					
					ParameterizedType parameterizedType = getParameterType(objeto);
					for (int j = 0; j < parameterizedType.getActualTypeArguments().length; j++) {
						Type genericType = parameterizedType.getActualTypeArguments()[j];
						Type paramType = cmd.getParametrosLista().get(i).getClass();

						if (genericType.equals(paramType)) {
							types[i] = erasureType.getErasuresTypes()[j];
							isTypeFound = true;
							break;
						}
					}

					if (!isTypeFound) {
						Object clazz = cmd.getParametrosLista().get(i);
						types[i] = clazz.getClass();
					}

				} else {
					Object clazz = cmd.getParametrosLista().get(i);
					types[i] = clazz.getClass();
				}
			}

			// traceAllMethods(objeto);
			String nomeMetodo = ComandoDTOInvoker.transformarPermissaoEmMetodo(cmd.getPermissao());

			Method metodo = objeto.getClass().getMethod(nomeMetodo, types);
			return metodo.invoke(objeto, cmd.getParametrosLista().toArray());

		} catch (InvocationTargetException e) {
			Throwable erro = e.getTargetException();
			if (erro instanceof GrowUpException) {
				throw (GrowUpException) erro;
			} else {
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, erro.getCause()
						.getMessage());
			}
		} catch (IllegalAccessException e) {
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
		} catch (NoSuchMethodException me) {
			IE erroInterno = ErroInterno.getInstance().getErroInterno(
					ErroInterno.METODO_NAO_ENCONTRADO);
			GrowUpException se = new GrowUpException(
					ErroInterno.METODO_NAO_ENCONTRADO,
					erroInterno.mensagem + "[" + cmd.getPermissao() + "]",
					erroInterno.acao);
			throw se;
		}
	}

	/**
	 * Verifica o Parameter Type do Objeto de negocio ou do proxy do mesmo.

	 * @param objeto
	 * @return
	 */
	protected static ParameterizedType getParameterType(final Object objeto) {
		//Weld verification - pode ser um proxy que depende da implementa��o
		/* org.jboss.weld.bean.proxy.ProxyFactory.isProxy -> Depedente da implementacao
		if (org.jboss.weld.bean.proxy.ProxyFactory.isProxy(objeto)) {
			org.jboss.weld.bean.proxy.ProxyObject proxy = (ProxyObject) objeto;
			org.jboss.weld.bean.proxy.ProxyMethodHandler handler = (ProxyMethodHandler) proxy.getHandler();
			return (ParameterizedType) handler.getBean().getBeanClass().getGenericSuperclass() ;
		} else */  
		if (objeto instanceof org.apache.webbeans.proxy.OwbNormalScopeProxy)
			return getParameterType(org.apache.webbeans.proxy.NormalScopeProxyFactory.unwrapInstance(objeto));
		else 
			return (ParameterizedType) objeto.getClass().getGenericSuperclass();
	}

	public final static void traceAllMethods(final Object obj) {
		for (Method m : obj.getClass().getMethods()) {
			System.out.println(m);
		}
	}

	/**
	 * M�todo utilizado para transformar a string da permissao de caixa alta no
	 * nome do m�todo a ser invocado.
	 * 
	 * @param permissao
	 *            a String de Permissao.
	 * @return String de permiss�o modificada.
	 */
	public static String transformarPermissaoEmMetodo(final String permissao) {
		String comando = StringUtil.removerCaracteresInvalidos(permissao.toLowerCase());
		char[] comandoArray = comando.toCharArray();
		for (int i = 0; i < comandoArray.length; i++) {
			if (comandoArray[i] == '_') {
				try {
					comandoArray[i + 1] = Character.toUpperCase(comandoArray[i + 1]);
				} catch (ArrayIndexOutOfBoundsException e) {
					
				}
			}
		}
		String metodo = new String(comandoArray);
		metodo = StringUtil.replaceString(metodo, "_", "");
		return metodo;
	}

}
