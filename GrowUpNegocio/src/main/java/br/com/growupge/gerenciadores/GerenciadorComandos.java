/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : Dec 13, 2006
 *
 * Historico de Modifica��o:
 * =========================
 */
package br.com.growupge.gerenciadores;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.HashMap;
import java.util.Map;

import br.com.growupge.business.Mensagem;
import br.com.growupge.business.NegocioBase;
import br.com.growupge.business.Perfil;
import br.com.growupge.business.Permissao;
import br.com.growupge.business.Sessao;
import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.GCS;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.ComandoDTO;
import br.com.growupge.dto.MensagemDTO;
import br.com.growupge.dto.MensagemRetornoDTO;
import br.com.growupge.dto.PerfilPermissaoItemDTO;
import br.com.growupge.dto.PermissaoDTO;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.enums.TipoIdioma;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.exception.IE;
import br.com.growupge.factory.BeanFactory;
import br.com.growupge.interfaces.SessaoUsuario;
import br.com.growupge.util.MensagemBuilder;
import br.com.growupge.utility.Network;
import br.com.growupge.utility.StringUtil;

/**
 * Classe Singleton/FrontController para unificar todos os comandos pass�veis de
 * verifica��o (CheckPermission)
 * 
 * @author Felipe
 */
@javax.inject.Named
@javax.enterprise.context.RequestScoped
public class GerenciadorComandos {

	/**
	 * Atributo '<code>instance</code>' do tipo GerenciadorComandos
	 */
//	private static GerenciadorComandos instance = null;

	/**
	 * Atributo '<code>mapa</code>' do tipo HashMap<String,Object>
	 */
	// private HashMap<String, Object> mapa = new HashMap<String, Object>();
	private final String pacoteNegocio;

	/**
	 * Construtor para esta classe.
	 * 
	 * @throws GrowUpException
	 *             Caso ocorra alguma exce��o durante a opera��o.
	 */
	public GerenciadorComandos() throws GrowUpException {

		try {
			pacoteNegocio = NegocioBase.class.getPackage().getName();

		} catch (Exception e) {
			IE ie = ErroInterno.getInstance().getErroInterno(ErroInterno.ERRO_DESCONHECIDO);
			GrowUpException se = new GrowUpException(
					ErroInterno.ERRO_DESCONHECIDO,
					ie.mensagem,
					ie.acao,
					e.getMessage());
			throw se;
		}
	}

	/**
	 * M�todo utilizado para retornar a inst�ncia do gerenciador de comandos.
	 * 
	 * @return Inst�ncia do gerenciador de comandos.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro na obten��o da inst�ncia do
	 *             gerenciador de comandos.
	public static GerenciadorComandos getInstance() throws GrowUpException {
		if (GerenciadorComandos.instance == null) {
			GerenciadorComandos.instance = new GerenciadorComandos();
		}
		return GerenciadorComandos.instance;
	}
	 */

	/**
	 * Invoca o m�todo requisitado de acordo com o par�metro informado no array
	 * 'parametros'.
	 * 
	 * @param parametros
	 *            Um array de objetos contendo os atributos abaixo na seguinte
	 *            ordem: 0- sid 1- Mapa
	 * @see GCS 2- Opera��o/Permiss�o
	 * @see GCS 3- Objeto (dto, classe personalizada, etc..), Mandat�rio que
	 *      seja uma classe. Ex: new Object[] { SID.codigoHash, GCS.MAPA_LOCAL,
	 *      GCS.ADICIONAR_LOCAL, dto }
	 * @return Objeto retornado pode ser nulo se a opera��o n�o for bem sucedida
	 *         ou populado caso ocorra com sucesso.
	 * @throws GrowUpException
	 *             Caso ocorra uma exce��o padr�o durante a execu��o da
	 *             opera��o.
	 */
	@TransactionalInterceptor
	public Object invocar(final ComandoDTO cmd) throws GrowUpException {

		// Checar se a sess�o do usu�rio est� ativa ou � Logon
		SessaoDTO dtoSessao = this.verificarSessionId(cmd);

		// Obtem parametro que define qual classe resgatar do mapa
		Object businessObject = this.getBusinessObject(cmd.getMapaGCS());

		if (businessObject == null) {
			IE ie = ErroInterno.getInstance().getErroInterno(ErroInterno.MAPA_GERENCIADOR_COMANDOS);
			GrowUpException se = new GrowUpException(
					ErroInterno.MAPA_GERENCIADOR_COMANDOS,
					ie.mensagem,
					ie.acao);
			throw se;
		}

		// Encapsula a sessao do usuario dentro do BO
		if ((dtoSessao != null) && (businessObject instanceof SessaoUsuario)) {
			((SessaoUsuario) businessObject).setSessaodto(dtoSessao);
		}

		// Cria a lista de parametros para chamada do m�todo invocar gen�rico
		//		Object[] lista = new Object[parametros.length - 3];
		//		System.arraycopy(parametros, 3, lista, 0, parametros.length - 3);

		// Invoca m�todo da classe
		Object objetoRetorno = null;
		// int resultadoComando = Constantes.COMMAND_FAILURE;
		try {

			this.checarPermissao(dtoSessao.getUsuario(), cmd);

			// Executa o m�todo correspondente a permiss�o e recebe retorno
			objetoRetorno = ComandoDTOInvoker.invocarMetodo(businessObject, cmd);

			if (objetoRetorno != null && objetoRetorno instanceof MensagemRetornoDTO) {

				// Obtem a mensagem no idioma do usuario
				this.setMensagemRetorno((MensagemRetornoDTO) objetoRetorno, dtoSessao);
			}
			// resultadoComando = Constantes.COMMAND_SUCESS;
		} catch (GrowUpDAOException se) {
			throw se;
		} catch (GrowUpException se) {
			GrowUpException e = null;

			if (isBlank(se.getMensagem())) {
				String idioma = dtoSessao.getUsuario() == null ? null : dtoSessao.getUsuario().getIdioma();
				e = new GrowUpException(se.getCode(), MensagemBuilder.getMensagem(
						se.getCode(),
						idioma,
						se.parametros));
				throw e;
			} else {
				throw se;
			}
		}

		return objetoRetorno;

	}

	/**
	 * Este m�todo faz reflex�o entre dois objetos do mesmo tipo (DTO) para
	 * descobrir a diferen�a de conte�do entre os atributos
	 * 
	 * @param adto
	 * @param objetoAntes
	 * @param objetoRetorno
	 * @throws GrowUpException
	 *             Comentar aqui.
	 
	public void montaAuditoriaParametroItem(final Object objetoAntes, final Object objetoRetorno)
			throws GrowUpException {

		if ((objetoAntes != null) && (objetoRetorno != null)
				&& (objetoAntes instanceof MensagemRetornoDTO)
				&& (objetoAntes.getClass().isAssignableFrom(objetoRetorno.getClass()))) {

			// Obtem todos os atributos dentro do DTO
			Field[] camposRefletirAntes = objetoAntes.getClass().getFields();
			for (int i = 0; i < camposRefletirAntes.length; i++) {
				Field campoRefletirA = null;
				try {
					campoRefletirA = camposRefletirAntes[i];
					String tempA = null;
					try {
						if (campoRefletirA.get(objetoAntes) instanceof MensagemRetornoDTO) {
							// Recursao nos objetos complexos - Incompleto

						} else {
							tempA = campoRefletirA.get(objetoAntes).toString();
						}
					} catch (IllegalArgumentException e) {
						tempA = null;
						
					} catch (IllegalAccessException e) {
						tempA = null;
						
					} catch (NullPointerException e) {
						tempA = null;
					}

					// Procura pelo atributo do DTO A dentro do DTO B
					if (tempA != null) {
						try {
							Field campoRefletirB = objetoRetorno.getClass().getField(
									campoRefletirA.getName());
							String tempB = null;
							try {
								tempB = campoRefletirB.get(objetoRetorno).toString();

								if (!tempA.equals(tempB)) {
//									  adto.parametrosItem .add(new
//									  AuditoriaParametroItem(
//									  campoRefletirA.getName(), tempA,
//									  tempB).getDados());
								}
							} catch (IllegalArgumentException e) {
								
							} catch (IllegalAccessException e) {
								
							} catch (NullPointerException e) {
								tempB = null;
							}
						} catch (NoSuchFieldException e) {
							
						}
					}
				} catch (SecurityException e) {
					
				}

			}
		}
	}*/

	/**
	 * M�todo utilizado para checar se a sess�o do usu�rio tem permiss�o.
	 * 
	 * @param parametros
	 *            Parametros a serem verificados
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a verifica��o de permiss�o.
	 * @return Objeto de transfer�ncia com os dados da sess�o do usu�rio.
	 */
	private final SessaoDTO verificarSessionId(final ComandoDTO cmd) throws GrowUpException {

		// Checar a permiss�o do Usu�rio para executar este comando
		SessaoDTO dtoSessao = new SessaoDTO();
		dtoSessao.setSid(cmd.getSID());

		if (!GCS.METODOS_SEM_SID.contains(cmd.getPermissao())) {
			// Obtem a sess�o do usu�rio atual
//			dtoSessao = new Sessao().procurar(dtoSessao);
			dtoSessao = BeanFactory.getContextualInstance(Sessao.class).procurar(dtoSessao);

			if (dtoSessao == null) {
				throw new GrowUpException(MSGCODE.SESSAO_NAO_INICIADA, MensagemBuilder.getMensagem(
						MSGCODE.SESSAO_NAO_INICIADA,
						TipoIdioma.IDIOMA_BR.toString(),
						null));
			}
		} else {
			// O usuario pode estar conectando.

			if ( GCS.METODOS_CONEXAO.contains(cmd.getPermissao())) {
				dtoSessao = (SessaoDTO) cmd.getParametrosLista().get(0);
				dtoSessao.setSid(cmd.getSID()); 
				dtoSessao.setHost(Network.getLocalHostname());
			} else if (GCS.METODOS_SEM_SID.contains(cmd.getPermissao())) {
				dtoSessao.setHost(Network.getLocalHostname());
			} else {
				throw new GrowUpException(MSGCODE.PERMISSAO_NEGADA, MensagemBuilder.getMensagem(
						MSGCODE.PERMISSAO_NEGADA,
						TipoIdioma.IDIOMA_BR.toString(),
						null));
			}
		}

		return dtoSessao;
	}

	/**
	 * M�todo utilizado para obter as mensagens internacionalizadas.
	 * 
	 * @param objetoRetorno
	 * @param dtoSessao
	 *            sess�o do usu�rio.
	 * @throws SigemException
	 *             Caso ocorra algum problema.
	 */
	private void setMensagemRetorno(
			final MensagemRetornoDTO objetoRetorno,
			final SessaoDTO dtoSessao) throws GrowUpException {

		if ((objetoRetorno.codMensagem != null)) {
			MensagemDTO dtoMsg = new MensagemDTO();

			if (objetoRetorno.codMensagem == null) {
				throw new GrowUpException(MSGCODE.MENSAGEM_CODIGO_NULO);
			}

			dtoMsg.setCodigo(objetoRetorno.codMensagem);
//			dtoMsg = new Mensagem().procurar(dtoMsg);
			dtoMsg = BeanFactory.getContextualInstance(Mensagem.class).procurar(dtoMsg);

			if (dtoMsg == null) {
				throw new GrowUpException(MSGCODE.MENSAGEM_CODIGO_NAO_ENCONTRADO);
			}

			// Existem parametros para serem trocados na mensagem
			if (objetoRetorno.parametros != null) {

//				for (Enumeration<?> e = objetoRetorno.parametros.keys(); e.hasMoreElements();) {
				for (String chave: objetoRetorno.parametros.keySet()) {

//					String chave = (String) e.nextElement();

					if (dtoMsg.getDescricaoBR() != null) {
						dtoMsg.setDescricaoBR(StringUtil.replaceString(
								dtoMsg.getDescricaoBR(),
								chave,
								StringUtil.cut(objetoRetorno.parametros.get(chave))));
					}

					if (dtoMsg.getDescricaoEN() != null) {
						dtoMsg.setDescricaoEN(StringUtil.replaceString(
								dtoMsg.getDescricaoEN(),
								chave,
								StringUtil.cut(objetoRetorno.parametros.get(chave))));
					}

				}
			}
			String idioma = dtoSessao == null || dtoSessao.getUsuario() == null ? null
					: dtoSessao.getUsuario().getIdioma();
			TipoIdioma enm = TipoIdioma.getEnum(idioma);
			switch (enm) {
			case IDIOMA_BR:
				objetoRetorno.mensagemRetorno = dtoMsg.getDescricaoBR();
				break;
			case IDIOMA_EN:
				objetoRetorno.mensagemRetorno = dtoMsg.getDescricaoEN();
				break;
			default:
				objetoRetorno.mensagemRetorno = dtoMsg.getDescricaoBR();
			}

		}
	}

	/**
	 * M�todo que verifica hierarquia da permiss�o do usu�rio com a permiss�o do
	 * m�todo
	 * 
	 * @param paramUsrDTO
	 *            Par�metro do tipo UsuarioDTO
	 * @param permissao
	 *            Par�metro do tipo String que corresponde ao codigo da
	 *            permissao
	 * @param params
	 *            Par�metros vari�veis de acordo com o contexto
	 * 
	 * @throws SigemException
	 *             Comentar aqui.
	 */
	private final void checarPermissao(final UsuarioDTO paramUsrDTO, final ComandoDTO cmd)
			throws GrowUpException {
		Map<String, String> hash = null;
		PermissaoDTO permDTO = new PermissaoDTO();
		permDTO.setCodigo(cmd.getPermissao());
//		permDTO = new Permissao().procurar(permDTO);
		Permissao bo = BeanFactory.getContextualInstance(Permissao.class) ; 
		permDTO = bo.procurar(permDTO);

		PerfilPermissaoItemDTO item = new PerfilPermissaoItemDTO();
		item.setPermissao(permDTO);
		item.setPerfil(paramUsrDTO == null ? null : paramUsrDTO.getPerfil());
//		item = new Perfil().procurar(item);
		item = BeanFactory.getContextualInstance(Perfil.class).procurar(item);

		if (permDTO == null) {
			hash = new HashMap<String, String>();
			hash.put(Constantes.P1, cmd.getPermissao());

			throw new GrowUpException(MSGCODE.PERMISSAO_NAO_CADASTRADA, hash);
		} else if (item == null) {
			hash = new HashMap<String, String>();
			hash.put(Constantes.P1, cmd.getPermissao());

			throw new GrowUpException(
					MSGCODE.PERMISSAO_NEGADA,
					new HashMap<String, String>());
		}
	}

	/**
	 * Retorna o objeto de neg�cio atraves do mapa
	 * 
	 * @param mapa
	 *            Constante identificando o mapa
	 * @param sessaoDTO 
	 * @return Caso ocorra algum erro na opera��o
	 */
	private Object getBusinessObject(final String mapa) throws GrowUpException {
		Object bo = null;
		try {
			//			bo = (ConsultaBase<?>) Reflection.getNewInstance(pacoteNegocio, mapa);
			Class<?> cls = Class.forName(pacoteNegocio.concat(".").concat(mapa));
			
			bo = BeanFactory.getContextualInstance(cls);
			 
			 return bo;

		} catch (ClassNotFoundException e) {
			IE ie = ErroInterno.getInstance().getErroInterno(ErroInterno.MAPA_GERENCIADOR_COMANDOS);
			GrowUpException se = new GrowUpException(
					ErroInterno.MAPA_GERENCIADOR_COMANDOS,
					ie.mensagem,
					ie.acao);
			throw se;
		}
	}
}
