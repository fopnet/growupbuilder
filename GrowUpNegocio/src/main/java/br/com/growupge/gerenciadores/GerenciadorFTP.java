/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : Mar 7, 2007
 *
 * Historico de Modifica��o:
 * =========================
 * Mar 7, 2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.gerenciadores;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.util.ArrayList;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.GrupoServidorItemDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.security.BaseFactorySecurity;

/**
 * 
 * @author elmt
 * 
 */
public class GerenciadorFTP {

	private ArrayList<String> arquivoLista = new ArrayList<String>();

	private GrupoServidorItemDTO gsi = null;

	private FTPClient ftp = null;

	private String diretorioLocal = null;

	/**
	 * Construtor para esta classe.
	 * 
	 * @param gsi
	 * @throws GrowUpException
	 */
	public GerenciadorFTP(final GrupoServidorItemDTO gsi) throws GrowUpException {
		// Verifica o status do servidor
		if (gsi.status.equals(Constantes.STATUS_SERVIDOR_OFFLINE)) {
			throw new GrowUpException(MSGCODE.SERVIDOR_OFFLINE);
		}
		if (gsi.status.equals(Constantes.STATUS_SERVIDOR_OCUPADO)) {
			throw new GrowUpException(MSGCODE.SERVIDOR_OCUPADO);
		}
		if (gsi.status.equals(Constantes.STATUS_SERVIDOR_SEM_ESPACO)) {
			throw new GrowUpException(MSGCODE.SERVIDOR_SEM_ESPACO);
		}

		// Cria um FTP Cliente e seta algumas propriedades
		this.ftp = new FTPClient();

		// Cria o objeto local com base no DTO de par�metro
		this.gsi = new GrupoServidorItemDTO();

		// Encapsula informa��es do servidor
		this.gsi = gsi;

		// Conecta no servidor de ftp
		this.conectar();
	}

	/**
	 * 
	 * @param diretorioLocal
	 *            Comentar aqui.
	 */
	public void setDiretorioLocal(String diretorioLocal) {
		this.diretorioLocal = diretorioLocal;
	}

	/**
	 * 
	 * @return Comentar aqui.
	 */
	public String getDiretorioLocal() {
		return this.diretorioLocal;
	}

	/**
	 * 
	 * @throws GrowUpException
	 *             Comentar aqui.
	 */
	private void conectar() throws GrowUpException {
		try {
			this.ftp.connect(this.gsi.nomeServidor);
		} catch (SocketException e) {
			throw new GrowUpException(MSGCODE.FTP_SERVIDOR_FALHA_SOCKET);
		} catch (IOException e) {
			if (e instanceof java.net.UnknownHostException) {
				throw new GrowUpException(MSGCODE.FTP_SERVIDOR_INVALIDO);
			}
			// Outra exce��o n�o trabalhada
			
		}

		// decriptografa login e senha encapsulados
		BaseFactorySecurity bfs = BaseFactorySecurity.getInstance(Constantes.SECURITY_AES);
		String login = bfs.decriptografar(this.gsi.login);
		String senha = bfs.decriptografar(this.gsi.senha);

		// verifica se conectou com sucesso!
		if (FTPReply.isPositiveCompletion(this.ftp.getReplyCode())) {
			try {
				if (!this.ftp.login(login, senha)) {
					this.ftp.disconnect();
					throw new GrowUpException(MSGCODE.FTP_USUARIO_SENHA_INVALIDO);
				}
				this.ftp.setFileType(FTP.BINARY_FILE_TYPE);
			} catch (IOException e) {
				
			}
		} else {
			// erro ao se conectar
			try {
				this.ftp.disconnect();
			} catch (IOException e) {
				
			}
			throw new GrowUpException(MSGCODE.FTP_CONEXAO_RECUSADA);
		}
	}

	public void desconectar() throws GrowUpException {
		try {
			if (this.ftp != null) {
				this.ftp.disconnect();
				this.ftp = null;
			}
		} catch (IOException e) {
			throw new GrowUpException(MSGCODE.FTP_ERRO_NAO_DETERMINADO);
		}
	}

	/**
	 * 
	 * @param nomeCompleto
	 *            Comentar aqui.
	 */
	public void adicionarArquivoLista(String nomeCompleto) {
		this.arquivoLista.add(nomeCompleto);
	}

	/**
	 * 
	 * @throws GrowUpException
	 *             Comentar aqui.
	 */
	public ArrayList<String> enviar() throws GrowUpException {

		checarConexaoFTP();

		ArrayList<String> arquivosNaoEnviados = new ArrayList<String>();

		// Seta diretorio para envio dos arquivos da lista
		try {
			if (!this.ftp.changeWorkingDirectory(this.gsi.diretorioRaiz)) {
				throw new GrowUpException(MSGCODE.FTP_DIRETORIO_SERVIDOR_NAO_EXISTE);
			}
		} catch (IOException e) {
			throw new GrowUpException(MSGCODE.FTP_ERRO_NAO_DETERMINADO);
		}

		// para cada arquivo informado...
		for (Object item : this.arquivoLista) {
			String arquivo = (String) item;

			// abre um stream com o arquivo a ser enviado
			InputStream is = null;
			try {
				is = new FileInputStream(arquivo);

				// pega apenas o nome do arquivo
				int idx = arquivo.lastIndexOf(File.separator);
				if (idx < 0) {
					idx = 0;
				} else {
					idx++;
				}

				// Este TEM QUE SER o nome do arquivo puro!
				String nomeArquivo = arquivo.substring(idx, arquivo.length());

				// faz o envio do arquivo
				this.ftp.storeFile(nomeArquivo, is);
			} catch (FileNotFoundException e) {
				arquivosNaoEnviados.add(arquivo);
			} catch (IOException e) {
				arquivosNaoEnviados.add(arquivo);
			}
		}

		return arquivosNaoEnviados;
	}

	/**
	 * 
	 * @throws GrowUpException
	 *             Comentar aqui.
	 */
	private void checarConexaoFTP() throws GrowUpException {
		if (this.ftp == null) {
			throw new GrowUpException(MSGCODE.FTP_NAO_CONECTADO);
		}
	}

	/**
	 * Executa o download de todos os arquivos armazenados no servidor de FTP
	 * para o diret�rio local
	 * 
	 * @return ArrayList com arquivos que n�o conseguiram ser enviados
	 * @throws GrowUpException
	 *             Exce��o padr�o do SIGEM
	 */
	public ArrayList<String> receber() throws GrowUpException {

		ArrayList<String> arquivosNaoRecebidos = new ArrayList<String>();

		// Seta diretorio de Download dos arquivos
		try {
			this.ftp.changeWorkingDirectory(this.gsi.diretorioRaiz);
		} catch (IOException e) {
			
		}

		if ((this.getDiretorioLocal() == null) || (this.getDiretorioLocal().length() == 0)) {
			throw new GrowUpException(MSGCODE.FTP_DIRETORIO_LOCAL_NAO_CONFIGURADO);
		}

		// Obtem a lista de arquivos do servidor
		FTPFile[] arquivos = null;
		try {
			arquivos = this.ftp.listFiles();
		} catch (IOException e1) {
			throw new GrowUpException(MSGCODE.FTP_NAO_RECUPEROU_LISTA_DE_ARQUIVOS);
		}
		for (Object item : arquivos) {
			FTPFile itemDownload = (FTPFile) item;
			if (itemDownload.getType() == FTPFile.FILE_TYPE) {
				FileOutputStream fos = null;
				try {
					fos = new FileOutputStream(this.getDiretorioLocal() + File.separator
							+ itemDownload.getName());

					if (!this.ftp.retrieveFile(itemDownload.getName(), fos)) {
						arquivosNaoRecebidos.add(itemDownload + " -> " + this.getDiretorioLocal()
								+ File.separator + itemDownload);
					}
				} catch (FileNotFoundException e) {
					throw new GrowUpException(MSGCODE.FTP_DIRETORIO_LOCAL_NAO_ENCONTRADO);
				} catch (IOException e) {
					throw new GrowUpException(MSGCODE.FTP_ERRO_NAO_DETERMINADO);
				}

			}

		}
		if (arquivosNaoRecebidos.size() == 0) {
			arquivosNaoRecebidos = null;
		}
		return arquivosNaoRecebidos;
	}

	/**
	 * 
	 * @return
	 * @throws GrowUpException
	 *             Comentar aqui.
	 */
	public final String[] getArquivosServidorFTP() throws GrowUpException {
		try {
			return this.ftp.listNames();
		} catch (IOException e) {
			
		}
		return null;
	}
}
