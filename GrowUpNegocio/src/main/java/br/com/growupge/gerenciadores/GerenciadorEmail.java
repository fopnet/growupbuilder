package br.com.growupge.gerenciadores;

import br.com.growupge.autenticador.AutenticadorEmail;
import br.com.growupge.dto.IEmail;
import br.com.growupge.exception.GrowUpException;
import org.apache.log4j.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 * @author elmt
 * 
 */
public final class GerenciadorEmail implements Serializable {
	/**
	 *  Atributo '<code>serialVersionUID</code>' do tipo long
	 */
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(GerenciadorEmail.class);
	
	/**
	 * Atributo '<code>instance</code>' do tipo GerenciadorEmail
	 */
	private static GerenciadorEmail instance;
	
	/**
	 *  Atributo '<code>executor</code>' do tipo ExecutorService
	 */
	private final ExecutorService executor; 

	/**
	 * Atributo '<code>EMAIL_CONTENT</code>' do tipo String
	 */
	private static final String EMAIL_CONTENT = "text/html; charset=UTF-8";


	// Construtores

	/**
	 * Construtor para esta classe.
	 * 
	 * @throws GrowUpException -
	 */
	private GerenciadorEmail() {
//		try {
//			this.session = this.getSession();
			this.executor = Executors.newCachedThreadPool();

//		} catch (Exception ex) {
//			throw new GrowUpException(MSGCODE.ERRO_ENVIO_EMAIL, ex.getMessage());
//		}
	}

	/**
	 * Retorna o Singleton do gerenciador de email
	 * 
	 * @return - Retorna a inst�ncia do Gerenciador do email.
	 * @throws GrowUpException
	 *             Comentar aqui.
	 */
	public static GerenciadorEmail getInstance() {
		if (GerenciadorEmail.instance == null) {
			synchronized (GerenciadorEmail.class)  {
				GerenciadorEmail.instance = new GerenciadorEmail();
			}
		}
		return GerenciadorEmail.instance;
	}

	// Metodos privados

	/**
	 * @param assunto
	 * @param remetente
	 * @param destinatario
	 * @param cc
	 * @param cco
	 * @return
	 * @throws MessagingException
	 */
	private Message criarMensagem(final Session session, final String assunto, final IEmail remetente, final IEmail destinatario,
								 final IEmail cc, final IEmail cco) throws MessagingException {
		
		MimeMessage message = new MimeMessage(session);
		try {
			message.setSubject(assunto);
			message.setSentDate(new Date());
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(destinatario.getEmail(), destinatario.getNome()));
			message.setFrom(new InternetAddress(remetente.getEmail(), remetente.getNome()));

			if (cc != null)
				message.setRecipient(Message.RecipientType.CC, new InternetAddress(cc.getEmail(), cc.getNome()));
		
			if (cco != null)
				message.setRecipient(Message.RecipientType.BCC, new InternetAddress(cco.getEmail(), cco.getNome()));
		
		} catch (UnsupportedEncodingException ex) {
			throw new AddressException(ex.getMessage());
		}
		return message;
	}

	/**
	 * Metodo adiciona o conteudo da mensagem e o anexo.
	 * 
	 * @param conteudo -
	 * @param destinatario -
	 * @param arquivo -
	 * @throws MessagingException -
	 */
	private Multipart adicionarConteudoEAnexo(final String conteudo, final File... files)
			throws MessagingException {

		// Cria um Multipart que ser� composto pelo conteudo e pelo anexo
		Multipart multipart = new MimeMultipart();

		// Cria a parte do corpo da mensagem (texto do conte�do do e-mail)
		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(conteudo, EMAIL_CONTENT);

		// Adiciona o corpo da mensagem ao Multipart
		multipart.addBodyPart(htmlPart);

		if (files != null) {
			for (File arquivo : files) {
				// Cria uma segunda parte do corpo (anexos do cont�udo do e-mail)
				BodyPart attachment = new MimeBodyPart();
				// Cria o anexo
				DataSource source = new FileDataSource(arquivo.getPath());
				// Define o data handler para o anexo
				attachment.setDataHandler(new DataHandler(source));
				// Define o nome do arquivo
				attachment.setFileName(arquivo.getName());
				// Adiciona o anexo ao Multipart
				multipart.addBodyPart(attachment);
			}
		}

		// Adiciona o Multipart a mensagem
		return multipart;

	}

	/**
	 * Obt�m uma sess�o no servidor de e-mail
	 * 
	 * @return - Retorna a instancia da sessao do email
	 * @throws Exception
	 *             Comentar aqui.
	 */
	private Session getSession() throws Exception {
		// Properties props = new Properties();
		// props.put("mail.smtp.host", GerenciadorEmail.SERVIDOR_SMTP);
		// props.put("mail.smtp.auth", "true");
		// props.put("mail.smtp.port", GerenciadorEmail.SERVIDOR_EMAIL_PORTA);
		Session session = null;
		try {
			Context initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			//Session ctx = (Session) envCtx.lookup("mail/Veiculos");
			Session ctx = (Session) envCtx.lookup("mailssl/Veiculos");
			// Session ctx = (Session) obj;
			String smtpUser = ctx.getProperty("mail.smtp.user");
			String smtppwd = ctx.getProperty("mail.smtp.password");

            session = Session.getInstance(ctx.getProperties(), new AutenticadorEmail(smtpUser, smtppwd));
            // session = Session.getInstance(ctx.getProperties());

            /*
            logger.debug("smtpUser " + smtpUser);

            for (Object key : ctx.getProperties().keySet()) {
                //String key = it.nextElement().toString();
                logger.debug(key + " " + ctx.getProperty(key.toString()));
            }

            logger.debug("fim do for");
            session.setDebug(true);
            */
//

		} catch (SecurityException se) {
			throw se;
		} catch (Exception ex) {
			throw ex;
		}

		return session;
	}

	// Metodos publicos

	public final void enviarEmailAssincrono(final String assunto, final String conteudo,
			final IEmail destinatario, final IEmail remetente, final File... file)
			throws Exception {
		Runnable commando = new Runnable() {
			
			@Override
			public void run() {
				try {
					enviarEmail(assunto, conteudo, remetente, destinatario, file);
				} catch (Exception e) {
					logger.error(e,e);
				}
			}
		}; 
		
		executor.execute(commando);
	}
	
	/**
	 * Metodo responsavel por enviar emails
	 * 
	 * @param assunto -
	 * @param conteudo -
	 * @param destinatario -
	 * @param arquivo -
	 * @throws MessagingException -
	 *             Dispara exce��o caso ocorra algum erro no envio
	 */
	public final void enviarEmail(final String assunto, final String conteudo,
			final IEmail remetente, final IEmail destinatario, final File... file)
			throws Exception {
		enviarEmail(assunto, conteudo, remetente, destinatario, null, null, file);
	}

	/**
	 *  Metodo responsavel por enviar emails
	 * 
	 * @param assunto
	 * @param conteudo
	 * @param remetente
	 * @param destinatario
	 * @param cc
	 * @param cco
	 * @param file
	 * @throws Exception
	 */
	public final void enviarEmail(final String assunto, final String conteudo,
			final IEmail remetente, final IEmail destinatario, 
			final IEmail cc, final IEmail cco, final File... file)
					throws Exception {
		
		Session session = getSession();
		
//		Transport transport = session.getTransport();
		try {
//			transport.connect(	session.getProperty("mail.smtp.host"),
//								session.getProperty("mail.smtp.user"), 
//								session.getProperty("mail.smtp.password")
//								);
			
			Message message = criarMensagem(session, assunto, remetente, destinatario, cc, cco);
			
			// Adiciona o Multipart a mensagem
			message.setContent(adicionarConteudoEAnexo(conteudo, file));
			
			message.saveChanges();
			
//			transport.sendMessage(message, message.getAllRecipients());
			Transport.send(message);// as vezes demora por causa do tamanho do arquivo. avaliar redu��o do tamanho.
			
//			Transport.send(message);
			
		} catch (SecurityException se) {
			throw se;
		} catch (MessagingException me) {
			throw me;
		} catch (Exception e) {
			throw e;
		} finally {
//			transport.close();
		}
		
		/*
		 * Transport transport = null; try { transport =
		 * this.session.getTransport(GerenciadorEmail.EMAIL_PROTOCOLO);
		 * transport.send(this.message); } finally { transport.close(); }
		 */
	}
	
	public static void destroy() {
		GerenciadorEmail.instance.executor.shutdownNow();
		GerenciadorEmail.instance = null;
	}

}
