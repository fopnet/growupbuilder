package br.com.growupge.security;

/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 09/01/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 09/01/2007 - In�cio de tudo, por Felipe
 *
 */
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;

/**
 * Classe com a implementa��o do algoritmo de encripta��o AES
 * 
 * @author Felipe
 * 
 */
public class AES {

	/**
	 * Cria o obejto Cipher especificando o algoritmo de encripta��o AES
	 * 
	 * Atributo '<code>aesCipher</code>' do tipo Cipher
	 */
	private Cipher aesCipher = null;

	/**
	 * Atributo '<code>secretKey</code>' do tipo SecretKey
	 * "09CC89EA96661E8D34EA3CE2F1928C8E";
	 */
	private SecretKey secretKey = null;

	/**
	 * Construtor para esta classe.
	 * 
	 * @throws GrowUpException -
	 */
	public AES() throws GrowUpException {

		try {
			this.aesCipher = Cipher.getInstance("AES");
		} catch (NoSuchAlgorithmException e) {
			throw new GrowUpException(ErroInterno.ALGORITMO_AES, e);
		} catch (NoSuchPaddingException e) {
			throw new GrowUpException(ErroInterno.ALGORITMO_AES, e);
		}

	}

	/**
	 * Retorna a Chave para criptografia AES
	 * 
	 * @return - Retorna um objeto do tipo SecretKey
	 * @throws GrowUpException
	 *             Comentar aqui.
	 */
	private final SecretKey getChave() throws GrowUpException {
		String strHexkey = null;

		if (this.secretKey == null) {

			strHexkey = Constantes.VARIAVEL_CHAVE_AES;

			// Criando o objeto chave do tipo SecretKey
			this.secretKey = new SecretKeySpec(decodeHex(strHexkey), "AES");
		}
		return this.secretKey;

	}

	/**
	 * Gera uma nova chave em hexadecimal
	 * 
	 * @return Retorna um objeto do tipo String
	 * @throws NoSuchAlgorithmException -
	 */
	public final String gerarChave() throws NoSuchAlgorithmException {
		KeyGenerator keyGen = null;
		try {
			keyGen = KeyGenerator.getInstance("AES");
			keyGen.init(Constantes.CHAVE_TAMANHO_128_BITS);
			return encodeHex(keyGen.generateKey().getEncoded());
		} catch (NoSuchAlgorithmException noSuchAlgo) {
			// System.out.println(" Algoritmo inexistente " + noSuchAlgo);
			throw noSuchAlgo;
		} finally {
			keyGen = null;
		}

	}

	private String encodeHex(byte[] encoded) {
		return new String(Hex.encodeHex(encoded));
	}

	/**
	 * Decriptogra o dado
	 * 
	 * @param strDado -
	 *            Parametro � o dado a ser criptografado
	 * @return Retorna um objeto do tipo String
	 * @throws GrowUpException -
	 */
	public final String decriptografar(final String strDado) throws GrowUpException {

		// Inicializando o Cipher para decripta��o
		try {
			this.aesCipher.init(Cipher.DECRYPT_MODE, this.getChave());

			// Decriptar o Decrypt the cipher bytes using doFinal method
			byte[] byteDataToEncrypt = decodeHex(strDado);
			byte[] byteDecryptedText;
			byteDecryptedText = this.aesCipher.doFinal(byteDataToEncrypt);

			return new String(byteDecryptedText);

		} catch (InvalidKeyException e) {
			throw new GrowUpException(ErroInterno.ALGORITMO_AES, e);
		} catch (IllegalBlockSizeException e) {
			throw new GrowUpException(ErroInterno.ALGORITMO_AES, e);
		} catch (BadPaddingException e) {
			throw new GrowUpException(ErroInterno.ALGORITMO_AES, e);
		}
	}

	private static  byte[] decodeHex(String strDado) throws GrowUpException {
		try {
			return Hex.decodeHex(strDado.toCharArray());
		} catch (DecoderException e) {
			throw new GrowUpException(ErroInterno.ALGORITMO_AES, e);
		}
	}

	/**
	 * Criptografa o dado
	 * 
	 * @param strDado -
	 * @return Retorna um objeto do tipo String
	 * @throws GrowUpException -
	 */
	public final String criptografar(final String strDado) throws GrowUpException {

		byte[] byteCipherText = null;

		try {

			// Inicializando o Cipher para encripta��o
			this.aesCipher.init(Cipher.ENCRYPT_MODE, this.getChave());

			// Converte o dado para vetor de Bytes
			byte[] byteDataToEncrypt = strDado.getBytes();

			// Criptografa o vetor de bytes usando o metofo doFinal
			byteCipherText = this.aesCipher.doFinal(byteDataToEncrypt);

			return encodeHex(byteCipherText);

		} catch (InvalidKeyException invalidKey) {
			System.out.println(" Invalid Key " + invalidKey);
			throw new GrowUpException(ErroInterno.ALGORITMO_AES, invalidKey);
		} catch (BadPaddingException badPadding) {
			throw new GrowUpException(ErroInterno.ALGORITMO_AES, badPadding);
		} catch (IllegalBlockSizeException illegalBlockSize) {
			throw new GrowUpException(ErroInterno.ALGORITMO_AES, illegalBlockSize);
		}

	}

}
