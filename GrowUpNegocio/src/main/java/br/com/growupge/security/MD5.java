package br.com.growupge.security;

import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.utility.StringUtil;

/**
 * Cria o obejto Cipher especificando o algoritmo de encriptação MD5
 * 
 * @author Felipe
 * 
 */
public final class MD5 {
	/*
	 * public byte[] digestBits;
	 * 
	 * public String bytetoHex(int len) { StringBuffer sb = new StringBuffer();
	 * for (int i = 0; i < len; i++) { char c1, c2;
	 * 
	 * c1 = (char) ((digestBits[i] >>> 4) & 0xf); c2 = (char) (digestBits[i] &
	 * 0xf); c1 = (char) ((c1 > 9) ? 'a' + (c1 - 10) : '0' + c1); c2 = (char)
	 * ((c2 > 9) ? 'a' + (c2 - 10) : '0' + c2); sb.append(c1); sb.append(c2); }
	 * return sb.toString(); }
	 */

	/**
	 * 
	 * @param s -
	 * @return Retorna um objeto do tipo String
	 * @throws GrowUpException -.
	 */
	public String getHashCode(final String s) throws GrowUpException {
		int i;

		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] digestBits = null;

			for (i = 0; i < s.length(); i++) {
				md.update((byte) s.charAt(i));
			}

			digestBits = md.digest();

			return StringUtil.bytetoHex(digestBits, 16).toUpperCase();
		} catch (NoSuchAlgorithmException e) {
			throw new GrowUpException(ErroInterno.ALGORITMO_MD5, e);
		}
	}

	/**
	 * Metodo retorna o dado contido no ImputStream criptografado.
	 * 
	 * @param is -
	 * @return Retorna um objeto do tipo String
	 */
	public String getHashCode(final InputStream is) {
		return ("not implemented");
	}

}
