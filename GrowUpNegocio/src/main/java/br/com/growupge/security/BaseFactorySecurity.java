/*
 * Created on 03/02/2006
 *
 */
package br.com.growupge.security;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.exception.GrowUpException;

/**
 * @author elmt
 * 
 */
public abstract class BaseFactorySecurity {

	public BaseFactorySecurity() {
	}

	public abstract String criptografar(String dado) throws GrowUpException;

	public abstract String decriptografar(String dado) throws GrowUpException;

	public static BaseFactorySecurity getInstance(int factorytype) {
		switch (factorytype) {
		case Constantes.SECURITY_SHA:
			return new SHA1Factory();
		case Constantes.SECURITY_MD5:
			return new MD5Factory();
		case Constantes.SECURITY_AES:
			return new AESFactory();
		default:
			return new SHA1Factory();
		}
	}

}
