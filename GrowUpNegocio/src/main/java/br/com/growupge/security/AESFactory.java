/*
 * Created on 03/02/2006
 *
 */
package br.com.growupge.security;

import br.com.growupge.exception.GrowUpException;

/**
 * @author elmt
 * 
 */
public class AESFactory extends BaseFactorySecurity {

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.security.BaseFactorySecurity#criptografar(java.lang.String)
	 */
	@Override
	public final String criptografar(final String dado) throws GrowUpException {
		AES aes = new AES();
		return aes.criptografar(dado);
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.security.BaseFactorySecurity#decriptografar(java.lang.String)
	 */
	@Override
	public String decriptografar(String dado) throws GrowUpException {
		AES aes = new AES();
		return aes.decriptografar(dado);
	}

}
