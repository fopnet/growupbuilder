/*
 * Created on 03/02/2006
 *
 */
package br.com.growupge.security;

import br.com.growupge.exception.GrowUpException;

/**
 * @author elmt
 * 
 */
public class MD5Factory extends BaseFactorySecurity {

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.security.BaseFactorySecurity#sessionUIDGenerator(java.lang.String)
	 */
	@Override
	public String criptografar(String keyblend) throws GrowUpException {
		MD5 md5 = new MD5();
		return md5.getHashCode(keyblend);
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.security.BaseFactorySecurity#decriptografar(java.lang.String)
	 */
	@Override
	public String decriptografar(String dado) throws GrowUpException {
		return null;
	}

}
