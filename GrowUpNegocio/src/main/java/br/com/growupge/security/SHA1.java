package br.com.growupge.security;

import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.utility.StringUtil;

/**
 * Classe com a implementação do algoritmo de encriptação SHA1
 * 
 * @author Felipe
 * 
 */
public final class SHA1 {

	/**
	 * Retorna a criptografia do dado utilizando o algoritmo SHA1
	 * 
	 * @param s -
	 * @return - Retorna um objeto do tipo String
	 * @throws GrowUpException
	 *             Comentar aqui.
	 */
	public String getHashCode(final String s) throws GrowUpException {
		int i;
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA1");
			byte[] digestBits = null;

			for (i = 0; i < s.length(); i++) {
				md.update((byte) s.charAt(i));
			}

			digestBits = md.digest();

			return StringUtil.bytetoHex(digestBits, 20).toUpperCase();

		} catch (NoSuchAlgorithmException e) {
			throw new GrowUpException(ErroInterno.ALGORITMO_SHA1, e);
		}

	}

	/**
	 * Metodo retorna o dado contido no ImputStream criptografado.
	 * 
	 * @param is -
	 * @return Retorna um objeto do tipo String
	 */
	public String getHashCode(final InputStream is) {
		return ("not implemented");
	}

}
