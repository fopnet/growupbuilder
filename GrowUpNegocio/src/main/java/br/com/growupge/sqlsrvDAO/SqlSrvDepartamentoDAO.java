package br.com.growupge.sqlsrvDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.DepartamentoDTO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.utility.StringUtil;

/**
 * Classe DAO para gerenciar informa��es pertinentes a permiss�es.
 * 
 * @author Felipe
 * 
 */
public class SqlSrvDepartamentoDAO extends SqlSrvDAO<DepartamentoDTO> implements
		DAO<DepartamentoDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            Inst�ncia da F�brica de DAO.
	 */
	public SqlSrvDepartamentoDAO(final DAOFactory daofactory) {
		super(daofactory);

		sql.append(" select ");
		sql.append("  DEPT_CD_DEPARTAMENTO, ");
		sql.append("  DEPT_DS_DEPARTAMENTO ");
		sql.append(" from ");
		sql.append("   DEPARTAMENTO");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.growupge.sigem.oracledao.OracleDAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final DepartamentoDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {
		DepartamentoDTO dto = null;
		try {
			dto = new DepartamentoDTO();
			dto.codigo = rs.getString("DEPT_CD_DEPARTAMENTO");
			dto.descricao = rs.getString("DEPT_DS_DEPARTAMENTO");

			return dto;

		} catch (Exception ex) {
			throw new GrowUpDAOException(StringUtil.extractString(ex.getMessage(), "ORA-",
					Constantes.ORA_CODE), ex.getMessage());
		}

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.growupge.sigem.oracledao.OracleDAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final DepartamentoDTO dto) throws GrowUpDAOException {

		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {

			if (dto.codigo != null) {
				param.setParam("DEPT_CD_DEPARTAMENTO", dto.codigo == null ? dto.codigo : dto.codigo
						.trim().toUpperCase());

			} else {
				param.setParam("DEPT_DS_DEPARTAMENTO", dto.descricao == null ? dto.descricao
						: dto.descricao.trim().toUpperCase());
			}

			if (param.getParametros().size() == 0) {
				param.setParam("DEPT_CD_DEPARTAMENTO", String
						.valueOf(ParametroFactory.CODIGO_INEXISTENTE));
			}
		}

		param.setOrdem("DEPT_DS_DEPARTAMENTO", TipoOrdemDAO.ASC);

		return param;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.growupge.sigem.interfacesdao.DAO#deletePagamentosPayPal(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final DepartamentoDTO delete(final Connection conn, final DepartamentoDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append("delete from DEPARTAMENTO ");
			sql.append("where DEPT_CD_DEPARTAMENTO = ?");
			pstmt = conn.prepareStatement(sql.toString());

			/* Limpa valores antigos dos par�metros. */
			pstmt.clearParameters();
			pstmt.setString(1, dto.codigo);

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.REGISTRO_EXCLUIDO_PREVIAMENTE);
			}

		} catch (SQLException e) {

			if (e.getErrorCode() == 2292) {

				throw new GrowUpDAOException(MSGCODE.PERMISSAO_REGISTROS_ASSOCIADOS);

			}

			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {
			try {
				pstmt.close();
			} catch (Exception e) {
				
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}

		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.growupge.sigem.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final DepartamentoDTO insert(final Connection conn, final DepartamentoDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append("insert into DEPARTAMENTO(");
			sql.append("  DEPT_CD_DEPARTAMENTO, ");
			sql.append("  DEPT_DS_DEPARTAMENTO) ");
			sql.append("values (?,?)");
			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			pstmt.setString(inc++, dto.codigo);
			pstmt.setString(inc++, dto.descricao);

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}
		} catch (SQLException e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {
			try {
				pstmt.close();
			} catch (Exception e) {
				
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.growupge.sigem.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final DepartamentoDTO update(final Connection conn, final DepartamentoDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();

		try {
			sql.append(" update DEPARTAMENTO ");
			sql.append(" set DEPT_DS_DEPARTAMENTO =? ");
			sql.append(" where DEPT_CD_DEPARTAMENTO= ?");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			pstmt.setString(inc++, dto.descricao);
			pstmt.setString(inc++, dto.codigo);

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}
		} catch (SQLException e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {
			try {
				pstmt.close();
			} catch (Exception e) {
				
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}

		return dto;
	}

}
