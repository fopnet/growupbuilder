/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 28/07/2005
 *
 * Historico de Modifica��o:
 * =========================
 * 28/07/2005 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.sqlsrvDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.business.NegocioBase;
import br.com.growupge.business.Usuario;
import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.PessoaDTO;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.BeanFactory;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.utility.DateUtil;
import br.com.growupge.utility.StringUtil;

/**
 * Objeto dao para usu�rio.
 */
public class SqlSrvPessoaDAO extends SqlSrvDAO<PessoaDTO> implements DAO<PessoaDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            F�brica DAO.
	 */
	public SqlSrvPessoaDAO(final DAOFactory daofactory) {
		super(daofactory);

		this.sql.append("select");
		this.sql.append("    ID_USUARIO,");
		this.sql.append("    ID_AREA,");
		this.sql.append("    ID_CARGO,");
		this.sql.append(" 	 PRETENSAO,");
		this.sql.append("    CAT_HABILITACAO,");
		this.sql.append("    ORGAO_CLASSE,");
		this.sql.append("    ORGAO_PUBLICO, ");
		this.sql.append("    CPF, ");
		this.sql.append("    RG, ");
		this.sql.append("    ORGAO_EXPEDITOR, ");
		this.sql.append("    TIPO_SANGUINEO, ");
		this.sql.append("    SEXO, ");
		this.sql.append("    NOME_PAI, ");
		this.sql.append("    NOME_MAE, ");
		this.sql.append("    NASCIMENTO, ");
		this.sql.append("    ESTADO_CIVIL, ");
		this.sql.append("    UF_NATAL, ");
		this.sql.append("    CIDADE_NATAL, ");
		this.sql.append("    QTD_DPTO_LEGAIS, ");
		this.sql.append("    ENDERECO, ");
		this.sql.append("    BAIRRO, ");
		this.sql.append("    CIDADE, ");
		this.sql.append("    UF, ");
		this.sql.append("    CEP, ");
		this.sql.append("    TEL, ");
		this.sql.append("    CELULAR, ");
		this.sql.append("    INFO_ADICIONAL ");
		this.sql.append(" from ");
		this.sql.append("    PESSOA ");
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PessoaDTO insert(final Connection conn, final PessoaDTO dto)
			throws GrowUpDAOException {
		try {
			StringBuffer lsql = new StringBuffer();

			lsql.append("insert into USUARIO ( ");
			lsql.append("    ID_USUARIO,");
			lsql.append("    ID_AREA,");
			lsql.append("    ID_CARGO,");
			lsql.append(" 	 PRETENSAO,");
			lsql.append("    CAT_HABILITACAO,");
			lsql.append("    ORGAO_CLASSE,");
			lsql.append("    ORGAO_PUBLICO, ");
			lsql.append("    CPF, ");
			lsql.append("    RG, ");
			lsql.append("    ORGAO_EXPEDITOR, ");
			lsql.append("    TIPO_SANGUINEO, ");
			lsql.append("    SEXO, ");
			lsql.append("    NOME_PAI, ");
			lsql.append("    NOME_MAE, ");
			lsql.append("    NASCIMENTO, ");
			lsql.append("    ESTADO_CIVIL, ");
			lsql.append("    UF_NATAL, ");
			lsql.append("    CIDADE_NATAL, ");
			lsql.append("    QTD_DPTO_LEGAIS, ");
			lsql.append("    ENDERECO, ");
			lsql.append("    BAIRRO, ");
			lsql.append("    CIDADE, ");
			lsql.append("    UF, ");
			lsql.append("    CEP, ");
			lsql.append("    TEL, ");
			lsql.append("    CELULAR, ");
			lsql.append("    INFO_ADICIONAL ) ");
			lsql.append(" values ");
			lsql.append(" (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");

			PreparedStatement pstmt = conn.prepareStatement(lsql.toString());
			// limpa valores antigos dos par�metros
			pstmt.clearParameters();
			int inc = 1;
			pstmt.setString(inc++, dto.getUsuario().getCodigo());
			pstmt.setString(inc++, dto.getTipoDepartamento());
			pstmt.setString(inc++, dto.getTipoCargo());
			pstmt.setFloat(inc++, dto.getPretensao());
			pstmt.setString(inc++, dto.getCatHabilitacao());
			pstmt.setString(inc++, dto.getOrgaoClasse());
			pstmt.setString(inc++, dto.getOrgaoPublico());
			pstmt.setString(inc++, dto.getCpf());
			pstmt.setString(inc++, dto.getRg());
			pstmt.setString(inc++, dto.getOrgaoExpeditor());
			pstmt.setString(inc++, dto.getTipoSanguineo());
			pstmt.setString(inc++, dto.getSexo());
			pstmt.setString(inc++, dto.getNomePai());
			pstmt.setString(inc++, dto.getNomeMae());
			pstmt.setDate(inc++, dto.getNascimento() != null ? DateUtil.getDateManagerInstance(
					dto.getNascimento()).getSQLDate() : null);
			pstmt.setString(inc++, dto.getEstadoCivil());
			pstmt.setString(inc++, dto.getUfNatal());
			pstmt.setString(inc++, dto.getCidadeNatal());
			pstmt.setString(inc++, dto.getQtdDptoLegais());
			pstmt.setString(inc++, dto.getEndereco());
			pstmt.setString(inc++, dto.getBairro());
			pstmt.setString(inc++, dto.getCidade());
			pstmt.setString(inc++, dto.getUf());
			pstmt.setString(inc++, dto.getCep());
			pstmt.setString(inc++, dto.getTelefone());
			pstmt.setString(inc++, dto.getCelular());
			pstmt.setString(inc++, dto.getInfoAdcional());

			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			pstmt.close();

			return dto;
		} catch (SQLException e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "SQLSRV-",
					Constantes.ORA_CODE), e.getMessage());
		}

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PessoaDTO update(final Connection conn, final PessoaDTO dto)
			throws GrowUpDAOException {
		try {
			StringBuffer lsql = new StringBuffer();
			lsql.append("update PESSOA set ");
			lsql.append("    ID_AREA =?,");
			lsql.append("    ID_CARGO =?,");
			lsql.append(" 	 PRETENSAO =?,");
			lsql.append("    CAT_HABILITACAO =?,");
			lsql.append("    ORGAO_CLASSE =?,");
			lsql.append("    ORGAO_PUBLICO =?,");
			lsql.append("    CPF =?,");
			lsql.append("    RG =?,");
			lsql.append("    ORGAO_EXPEDITOR =?,");
			lsql.append("    TIPO_SANGUINEO =?, ");
			lsql.append("    SEXO =?,");
			lsql.append("    NOME_PAI =?, ");
			lsql.append("    NOME_MAE =?, ");
			lsql.append("    NASCIMENTO =?, ");
			lsql.append("    ESTADO_CIVIL =?, ");
			lsql.append("    UF_NATAL =?,");
			lsql.append("    CIDADE_NATAL =?, ");
			lsql.append("    QTD_DPTO_LEGAIS =?, ");
			lsql.append("    ENDERECO =?,");
			lsql.append("    BAIRRO =?,");
			lsql.append("    CIDADE =?,");
			lsql.append("    UF =?,");
			lsql.append("    CEP =?,");
			lsql.append("    TEL =?,");
			lsql.append("    CELULAR =?, ");
			lsql.append("    INFO_ADICIONAL =? ");
			lsql.append(" WHERE ID_USUARIO = ? ");

			PreparedStatement pstmt = conn.prepareStatement(lsql.toString());

			// limpa valores antigos dos par�metros
			pstmt.clearParameters();
			int inc = 1;
			pstmt.setString(inc++, dto.getTipoDepartamento());
			pstmt.setString(inc++, dto.getTipoCargo());
			pstmt.setFloat(inc++, dto.getPretensao());
			pstmt.setString(inc++, dto.getCatHabilitacao());
			pstmt.setString(inc++, dto.getOrgaoClasse());
			pstmt.setString(inc++, dto.getOrgaoPublico());
			pstmt.setString(inc++, dto.getCpf());
			pstmt.setString(inc++, dto.getRg());
			pstmt.setString(inc++, dto.getOrgaoExpeditor());
			pstmt.setString(inc++, dto.getTipoSanguineo());
			pstmt.setString(inc++, dto.getSexo());
			pstmt.setString(inc++, dto.getNomePai());
			pstmt.setString(inc++, dto.getNomeMae());
			pstmt.setDate(inc++, dto.getNascimento() != null ? DateUtil.getDateManagerInstance(
					dto.getNascimento()).getSQLDate() : null);
			pstmt.setString(inc++, dto.getEstadoCivil());
			pstmt.setString(inc++, dto.getUfNatal());
			pstmt.setString(inc++, dto.getCidadeNatal());
			pstmt.setString(inc++, dto.getQtdDptoLegais());
			pstmt.setString(inc++, dto.getEndereco());
			pstmt.setString(inc++, dto.getBairro());
			pstmt.setString(inc++, dto.getCidade());
			pstmt.setString(inc++, dto.getUf());
			pstmt.setString(inc++, dto.getCep());
			pstmt.setString(inc++, dto.getTelefone());
			pstmt.setString(inc++, dto.getCelular());
			pstmt.setString(inc++, dto.getInfoAdcional());
			pstmt.setString(inc++, dto.getUsuario().getCodigo());

			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			pstmt.close();

			return dto;
		} catch (SQLException e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "SQLSRV-",
					Constantes.ORA_CODE), e.getMessage());
		}

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PessoaDTO delete(final Connection conn, final PessoaDTO dto)
			throws GrowUpDAOException {
		try {
			StringBuffer lsql = new StringBuffer();
			lsql.append("delete from PESSOA ");
			lsql.append("where ID_USUARIO = ?");
			PreparedStatement pstmt = conn.prepareStatement(lsql.toString());

			// limpa valores antigos dos par�metros
			pstmt.clearParameters();
			pstmt.setString(1, dto.getUsuario().getCodigo());

			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			pstmt.close();

			return dto;
		} catch (SQLException e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "SQLSRV-",
					Constantes.ORA_CODE), e.getMessage());
		}

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.OracleDAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final PessoaDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {

		PessoaDTO pessoa = null;

		try {
			pessoa = new PessoaDTO();

			UsuarioDTO usr = new UsuarioDTO();
			usr.setCodigo(rs.getString("ID_USUARIO"));
			
			NegocioBase<UsuarioDTO> usuarioBO = BeanFactory.getContextualInstance(Usuario.class, getSessao());
			pessoa.setUsuario(usuarioBO.procurar(usr));

			pessoa.setTipoDepartamento(rs.getString("ID_AREA"));
			pessoa.setTipoCargo(rs.getString("ID_CARGO"));
			pessoa.setPretensao(rs.getFloat("PRETENSAO"));
			pessoa.setCatHabilitacao(rs.getString("CAT_HABILITACAO"));
			pessoa.setOrgaoClasse(rs.getString("ORGAO_CLASSE"));
			pessoa.setOrgaoPublico(rs.getString("ORGAO_PUBLICO"));
			pessoa.setCpf(rs.getString("CPF"));
			pessoa.setRg(rs.getString("RG"));
			pessoa.setOrgaoExpeditor(rs.getString("ORGAO_EXPEDITOR"));
			pessoa.setTipoSanguineo(rs.getString("TIPO_SANGUINEO"));
			pessoa.setSexo(rs.getString("SEXO"));
			pessoa.setNomePai(rs.getString("NOME_PAI"));
			pessoa.setNomeMae(rs.getString("NOME_MAE"));
			pessoa.setNascimento(rs.getDate("NASCIMENTO"));
			pessoa.setEstadoCivil(rs.getString("ESTADO_CIVIL"));
			pessoa.setUfNatal(rs.getString("UF_NATAL"));
			pessoa.setCidadeNatal(rs.getString("CIDADE_NATAL"));
			pessoa.setQtdDptoLegais(rs.getString("QTD_DPTO_LEGAIS"));
			pessoa.setEndereco(rs.getString("ENDERECO"));
			pessoa.setCidade(rs.getString("CIDADE"));
			pessoa.setBairro(rs.getString("BAIRRO"));
			pessoa.setUf(rs.getString("UF"));
			pessoa.setCep(rs.getString("CEP"));
			pessoa.setTelefone(rs.getString("TEL"));
			pessoa.setCelular(rs.getString("CELULAR"));
			pessoa.setInfoAdcional(rs.getString("INFO_ADICIONAL"));

			return pessoa;
		} catch (Exception ex) {
			throw new GrowUpDAOException(StringUtil.extractString(ex.getMessage(), "SQLSRV-",
					Constantes.ORA_CODE), ex.getMessage());
		}

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.OracleDAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final PessoaDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);
		if (dto != null) {
			param.setParam("ID_USUARIO", dto.getUsuario().getCodigo());
			param.setParam("ID_CARGO", dto.getTipoCargo() == null ? null : dto.getTipoCargo().toString());
			param.setParam("ID_AREA", dto.getTipoDepartamento() == null ? null : dto.getTipoDepartamento()
					.toString());
			param.setParam("UF", dto.getUf());

			// Verifica se s�o todas as revis�es
			if (param.getParametros().size() == 0) {
				param.setParam("ID_USUARIO", String.valueOf(ParametroFactory.CODIGO_INEXISTENTE));
			}
		}

		return param;
	}

}
