/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 05/01/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 05/01/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.sqlsrvDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.business.NegocioBase;
import br.com.growupge.business.Usuario;
import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.dto.StatusPedidoSetter;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.BeanFactory;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.PedidoVeiculoDAO;
import br.com.growupge.utility.DateUtil;
import br.com.growupge.utility.StringUtil;

/**
 * Classe DAO para gerenciar informa��es pertinentes a perfis.
 * 
 * @author Felipe
 * 
 */
public class SqlSrvPedidoVeiculoDAO extends SqlSrvDAO<PedidoVeiculoDTO> implements
PedidoVeiculoDAO {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            F�brica de DAO.
	 */
	public SqlSrvPedidoVeiculoDAO(final DAOFactory daofactory) {
		super(daofactory);

		sql.append(" select ");
		sql.append("  PEDI_CD_PEDIDO, ");
		sql.append("  PEDI_CD_PLACA, ");
		sql.append("  PEDI_DH_DATA, ");
		sql.append("  PEDI_IN_STATUS, ");
		sql.append("  PEDI_DS_PEDIDO, ");
		sql.append("  USUA_CD_USUARIO ");
		sql.append(" from ");
		sql.append("   PEDIDO");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.FbirdDAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final PedidoVeiculoDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {
		PedidoVeiculoDTO dto = null;
		try {
			dto = new PedidoVeiculoDTO();
			dto.setCodigo(rs.getLong("PEDI_CD_PEDIDO"));
			dto.setDescricao(rs.getString("PEDI_DS_PEDIDO"));
			dto.setDataCadastro(DateUtil.getDateManagerInstance(rs.getDate("PEDI_DH_DATA")).getDateTime());
//			dto.setStatus(rs.getString("PEDI_IN_STATUS"));
			StatusPedidoSetter.setStatus(rs, isEager, dto);
			dto.setPlaca( rs.getString("PEDI_CD_PLACA") );

			dto.setUsuarioCadastro(new UsuarioDTO());
			dto.getUsuarioCadastro().setCodigo(rs.getString("USUA_CD_USUARIO"));
			
			NegocioBase<UsuarioDTO> usuarioBO = BeanFactory.getContextualInstance(Usuario.class, getSessao());
			dto.setUsuarioCadastro(usuarioBO.procurar(dto.getUsuarioCadastro()));

		} catch (Exception ex) {
			throw new GrowUpDAOException(StringUtil.extractString(ex.getMessage(), "ORA-",
					Constantes.ORA_CODE), ex.getMessage());
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.FbirdDAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final PedidoVeiculoDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			if (dto.getCodigo() != 0) {
				param.setParam("PEDI_CD_PEDIDO", dto.getCodigo());
			} else {
				param.setParam("PEDI_DS_PEDIDO", dto.getDescricao());
				param.setParam("PEDI_DH_DATA", dto.getDataCadastro());
				param.setParam("USUA_CD_USUARIO", dto.getUsuarioCadastro() == null ? dto.getUsuarioCadastro()
						: dto.getUsuarioCadastro().getCodigo().trim().toUpperCase());
			}

			if (param.getParametros().size() == 0) {
				param.setParam("PEDI_CD_PEDIDO", ParametroFactory.CODIGO_INEXISTENTE);
			}
		}

		// Ordena��o padr�o.
		param.setOrdem("PEDI_DH_DATA", TipoOrdemDAO.DESC);
		param.setOrdem("USUA_CD_USUARIO", TipoOrdemDAO.ASC);

		return param;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PedidoVeiculoDTO delete(final Connection conn, final PedidoVeiculoDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append("delete from PEDIDO ");
			sql.append("where PEDI_CD_PEDIDO = ?");
			pstmt = conn.prepareStatement(sql.toString());

			/* Limpa valores antigos dos par�metros. */
			pstmt.clearParameters();
			pstmt.setLong(1, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.REGISTRO_EXCLUIDO_PREVIAMENTE);
			}
		} catch (SQLException e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {
			try {
				pstmt.close();
			} catch (Exception e) {
				
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}

		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PedidoVeiculoDTO insert(final Connection conn, final PedidoVeiculoDTO dto)
			throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append("insert into PEDIDO (");
			sql.append("  PEDI_DS_PEDIDO,");
			sql.append("  PEDI_DH_DATA,");
			sql.append("  PEDI_IN_STATUS,");
			sql.append("  PEDI_CD_PLACA,");
			sql.append("  USUA_CD_USUARIO ");
			sql.append(" ) values (?,?,?,?,?)");
			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			// pstmt.setLong(inc++, dto.codigo);
			pstmt.setString(inc++, dto.getDescricao());
			pstmt.setTime(inc++, DateUtil.getDateManagerInstance(dto.getDataCadastro()).getSQLTime());
			pstmt.setString(inc++, dto.getStatus().getTipo());
			pstmt.setString(inc++, dto.getPlaca());
			pstmt.setString(inc++, dto.getUsuarioCadastro().getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			dto.setCodigo(this.getIdentity(pstmt));
		} catch (SQLException e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {
			try {
				pstmt.close();
			} catch (Exception e) {
				
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PedidoVeiculoDTO update(final Connection conn, final PedidoVeiculoDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();

		try {
			sql.append(" update PEDIDO set ");
			sql.append("  PEDI_DS_PEDIDO=?,");
			sql.append("  PEDI_DH_DATA=?,");
			sql.append("  PEDI_IN_STATUS=?,");
			sql.append("  PEDI_CD_PLACA=?,");
			sql.append("  USUA_CD_USUARIO=? ");
			sql.append(" where PEDI_CD_PEDIDO = ?");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			pstmt.setString(inc++, dto.getDescricao());
			pstmt.setTime(inc++, DateUtil.getDateManagerInstance(dto.getDataCadastro()).getSQLTime());
			pstmt.setString(inc++, dto.getStatus().getTipo());
			pstmt.setString(inc++, dto.getPlaca());
			pstmt.setString(inc++, dto.getUsuarioCadastro().getCodigo());
			pstmt.setLong(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}
		} catch (SQLException e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {
			try {
				pstmt.close();
			} catch (Exception e) {
				
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}

		return dto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.interfacesdao.PedidoVeiculoDAO#setParamPerfilUsuario(java.lang.Object)
	 */
	public ParametroFactory setParamPerfilUsuario(PedidoVeiculoDTO dto) throws GrowUpDAOException {
		return null;
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.interfacesdao.PedidoVeiculoDAO#getPagamentosPedidos(br.com.growupge.dto.PedidoVeiculoDTO)
	 */
	@Override
	public ParametroFactory getPagamentosPedidos(PedidoVeiculoDTO dto)
			throws GrowUpDAOException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.interfacesdao.PedidoVeiculoDAO#updateStatus(java.sql.Connection, br.com.growupge.dto.PedidoVeiculoDTO)
	 */
	@Override
	public PedidoVeiculoDTO updateStatus(Connection conn, PedidoVeiculoDTO dto)
			throws GrowUpDAOException {
		// TODO Auto-generated method stub
		return null;
	}

}
