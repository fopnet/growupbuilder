/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 05/01/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 05/01/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.sqlsrvDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.PerfilDTO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.PerfilDAO;
import br.com.growupge.utility.StringUtil;

/**
 * Classe DAO para gerenciar informa��es pertinentes a perfis.
 * 
 * @author Felipe
 * 
 */
public class SqlSrvPerfilDAO extends SqlSrvDAO<PerfilDTO> implements PerfilDAO<PerfilDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            F�brica de DAO.
	 */
	public SqlSrvPerfilDAO(final DAOFactory daofactory) {
		super(daofactory);

		sql.append(" select ");
		sql.append("  PERFIL.PERF_CD_PERFIL, ");
		sql.append("  PERF_DS_PERFIL ");
		sql.append(" from ");
		sql.append("   PERFIL");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.FbirdDAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final PerfilDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {
		PerfilDTO perfilDTO = null;
		try {
			perfilDTO = new PerfilDTO();
			perfilDTO.setCodigo(rs.getString("PERF_CD_PERFIL"));
			perfilDTO.setDescricao(rs.getString("PERF_DS_PERFIL"));

		} catch (Exception ex) {
			throw new GrowUpDAOException(StringUtil.extractString(ex.getMessage(), "ORA-",
					Constantes.ORA_CODE), ex.getMessage());
		}
		return perfilDTO;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.FbirdDAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final PerfilDTO dto) throws GrowUpDAOException {
		ParametroFactory parametroFactory = ParametroFactory.getInstance(this);
		parametroFactory.setQuery(this.sql);

		if (dto != null) {
			if (dto.getCodigo() != null) {
				parametroFactory.setParam("PERFIL.PERF_CD_PERFIL", dto.getCodigo());
			} else {
				parametroFactory.setParam("PERF_DS_PERFIL", dto.getDescricao() == null ? dto.getDescricao()
						: dto.getDescricao().trim().toUpperCase());
			}

			if (parametroFactory.getParametros().size() == 0) {
				parametroFactory.setParam("PERF_CD_PERFIL", ParametroFactory.CODIGO_INEXISTENTE);
			}
		}

		// Ordena��o padr�o.
		parametroFactory.setOrdem("PERF_DS_PERFIL", TipoOrdemDAO.ASC);

		return parametroFactory;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PerfilDTO delete(final Connection conn, final PerfilDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append("delete from PERFIL ");
			sql.append("where PERF_CD_PERFIL = ?");
			pstmt = conn.prepareStatement(sql.toString());

			/* Limpa valores antigos dos par�metros. */
			pstmt.clearParameters();
			pstmt.setString(1, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.REGISTRO_EXCLUIDO_PREVIAMENTE);
			}
		} catch (SQLException e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {
			try {
				pstmt.close();
			} catch (Exception e) {
				
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}

		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PerfilDTO insert(final Connection conn, final PerfilDTO dto)
			throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append("insert into PERFIL(");
			sql.append("  PERF_CD_PERFIL, ");
			sql.append("  PERF_DS_PERFIL ) ");
			sql.append("values (?,?)");
			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			pstmt.setString(inc++, dto.getCodigo());
			pstmt.setString(inc++, dto.getDescricao());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}
		} catch (SQLException e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {
			try {
				pstmt.close();
			} catch (Exception e) {
				
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final PerfilDTO update(final Connection conn, final PerfilDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();

		try {
			sql.append(" update PERFIL ");
			sql.append(" set PERF_DS_PERFIL  = ? ");
			sql.append(" where PERF_CD_PERFIL = ?");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			pstmt.setString(inc++, dto.getDescricao());
			pstmt.setString(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}
		} catch (SQLException e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {
			try {
				pstmt.close();
			} catch (Exception e) {
				
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}

		return dto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.interfacesdao.PerfilDAO#setParamPerfil(java.lang.Object)
	 */
	@Override
	public ParametroFactory setParamPerfil(PerfilDTO dto) throws GrowUpDAOException {
		return null;
	}

}
