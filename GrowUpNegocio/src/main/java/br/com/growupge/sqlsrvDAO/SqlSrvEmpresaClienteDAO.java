/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.sqlsrvDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.EmpresaItemDTO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.utility.StringUtil;

/**
 * Classe DAO para gerenciar informa��es pertinentes a Empresa.
 * 
 * @author Felipe
 * 
 */
public class SqlSrvEmpresaClienteDAO extends SqlSrvDAO<EmpresaItemDTO> implements
		DAO<EmpresaItemDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            objeto de f�brica.
	 */
	public SqlSrvEmpresaClienteDAO(final DAOFactory daofactory) {
		super(daofactory);

		sql.append("select");
		sql.append("    EMPRESA.EMPR_CD_EMPRESA,");
		sql.append("    EMPR_NM_EMPRESA, ");

		sql.append("    CLIENTE.CLIE_CD_CLIENTE, ");
		sql.append("    CLIE_NM_CLIENTE, ");
		sql.append("    CLIE_DS_CLIENTE ");
		sql.append(" from ");
		sql
				.append("    CLIENTE_EMPRESA INNER JOIN EMPRESA ON CLIENTE_EMPRESA.EMPR_CD_EMPRESA = EMPRESA.EMPR_CD_EMPRESA");
		sql
				.append("    				INNER JOIN CLIENTE ON CLIENTE_EMPRESA.CLIE_CD_CLIENTE = CLIENTE.CLIE_CD_CLIENTE");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final EmpresaItemDTO delete(final Connection conn, final EmpresaItemDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();

		try {
			sql.append("delete from CLIENTE_EMPRESA ");
			sql.append("where EMPR_CD_EMPRESA = ? ");
			sql.append("AND  CLIE_CD_CLIENTE = ?");
			PreparedStatement pstmt = conn.prepareStatement(sql.toString());

			pstmt.setLong(1, dto.empresa.getCodigo());
			pstmt.setLong(2, dto.cliente.getCodigo());
			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			pstmt.close();

		} catch (Exception e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final EmpresaItemDTO insert(final Connection conn, final EmpresaItemDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();

		sql.append("INSERT INTO CLIENTE_EMPRESA (");
		sql.append("    EMPR_CD_EMPRESA ,");
		sql.append("    CLIE_CD_CLIENTE )");
		sql.append(" values ");
		sql.append(" (?,?) ");

		try {

			PreparedStatement pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;

			pstmt.setLong(inc++, dto.empresa.getCodigo());
			pstmt.setLong(inc++, dto.cliente.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			pstmt.close();

		} catch (Exception e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());

		}
		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final EmpresaItemDTO update(final Connection conn, final EmpresaItemDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();

		try {

			sql.append("UPDATE CLIENTE_EMPRESA SET ");
			sql.append("    CLIE_CD_CLIENTE =?");
			sql.append(" WHERE EMPR_CD_EMPRESA = ? ");
			sql.append(" AND CLIE_CD_CLIENTE = ? ");

			PreparedStatement pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			pstmt.setLong(inc++, dto.cliente.getCodigo());
			pstmt.setLong(inc++, dto.empresa.getCodigo());
			pstmt.setLong(inc++, dto.cliente.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			pstmt.close();
		} catch (Exception e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final EmpresaItemDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {

		EmpresaItemDTO dto = null;

		try {
			dto = new EmpresaItemDTO();

			dto.empresa = this.daoFactory.getEmpresaDAO().getDTO(rs, false);
			dto.cliente = this.daoFactory.getClienteDAO().getDTO(rs, false);
		} catch (Exception ex) {
			throw new GrowUpDAOException(StringUtil.extractString(ex.getMessage(), "ORA-",
					Constantes.ORA_CODE), ex.getMessage());
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final EmpresaItemDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			param.setParam("EMPRESA.EMPR_CD_EMPRESA", dto.empresa == null
					|| dto.empresa.getCodigo() == 0 ? null : dto.empresa.getCodigo());
			param.setParam("CLIENTE.CLIE_CD_CLIENTE", dto.cliente == null
					|| dto.cliente.getCodigo() == 0 ? null : dto.cliente.getCodigo());

			if (param.getParametros().size() == 0) {
				param.setParam("EMPRESA.EMPR_CD_EMPRESA", ParametroFactory.CODIGO_INEXISTENTE);
			}
		}

		return param;
	}
}
