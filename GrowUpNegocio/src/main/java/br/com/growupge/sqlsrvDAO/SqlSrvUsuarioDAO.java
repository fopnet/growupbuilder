/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 28/07/2005
 *
 * Historico de Modifica��o:
 * =========================
 * 28/07/2005 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.sqlsrvDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.UsuarioDAO;
import br.com.growupge.utility.DateUtil;
import br.com.growupge.utility.StringUtil;

/**
 * Objeto dao para usu�rio.
 */
public class SqlSrvUsuarioDAO extends SqlSrvDAO<UsuarioDTO> implements UsuarioDAO{

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            F�brica DAO.
	 */
	public SqlSrvUsuarioDAO(final DAOFactory daofactory)  {
		super(daofactory);

		SqlSrvPerfilDAO daoPerfil = new SqlSrvPerfilDAO(daofactory);
		StringBuffer colunaPerfil = new StringBuffer(daoPerfil.sql.toString().trim().substring(6,
				daoPerfil.sql.toString().trim().toUpperCase().indexOf("FROM"))).append(",");

		SqlSrvEmpresaDAO daoEmpresa = new SqlSrvEmpresaDAO(daofactory);
		StringBuffer colunaEmpresa = new StringBuffer(daoEmpresa.sql.toString().trim().substring(6,
				daoEmpresa.sql.toString().trim().toUpperCase().indexOf("FROM")));

		this.sql.append(" SELECT");
		this.sql.append("    USUA_CD_USUARIO,");
		this.sql.append("    USUA_NM_USUARIO,");
		this.sql.append("    USUA_NM_EMAIL,");
		this.sql.append("    USUA_IN_HABILITADO,");
		this.sql.append("    USUA_CD_SENHA,");
		this.sql.append("    USUA_DH_ALTERACAO_SENHA, ");
		this.sql.append("    USUA_IN_IDIOMA, ");
		this.sql.append(colunaPerfil);
		this.sql.append(colunaEmpresa);

		this.sql.append(" FROM ");
		this.sql.append("    USUARIO ");
		this.sql.append("    INNER JOIN PERFIL ");
		this.sql.append("    ON USUARIO.PERF_CD_PERFIL  = PERFIL.PERF_CD_PERFIL");
		this.sql.append("    INNER JOIN EMPRESA ");
		this.sql.append("    ON USUARIO.EMPR_CD_EMPRESA = EMPRESA.EMPR_CD_EMPRESA");
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final UsuarioDTO insert(final Connection conn, final UsuarioDTO dto)
			throws GrowUpDAOException {
		try {
			StringBuffer lsql = new StringBuffer();

			lsql.append("insert into USUARIO ( ");
			lsql.append("    USUA_CD_USUARIO,");
			lsql.append("    USUA_NM_USUARIO,");
			lsql.append("    USUA_NM_EMAIL,");
			lsql.append(" 	 PERF_CD_PERFIL,");
			lsql.append("    USUA_IN_HABILITADO,");
			lsql.append("    USUA_CD_SENHA,");
			lsql.append("    USUA_IN_IDIOMA, ");
			lsql.append("    USUA_DH_ALTERACAO_SENHA, ");
			lsql.append("    EMPR_CD_EMPRESA ");
			lsql.append(" ) values ");
			lsql.append(" (?,?,?,?,?,?,?,?,?) ");

			PreparedStatement pstmt = conn.prepareStatement(lsql.toString());
			// limpa valores antigos dos par�metros
			pstmt.clearParameters();
			int inc = 1;
			pstmt.setString(inc++, dto.getCodigo());
			pstmt.setLong(inc++, dto.getEmpresa() == null || dto.getEmpresa().getCodigo() == 0 ? null
					: dto.getEmpresa().getCodigo());
			pstmt.setString(inc++, dto.getNome());
			pstmt.setString(inc++, dto.getEmail());
			pstmt.setString(inc++, dto.getPerfil().getCodigo());
			pstmt.setString(inc++, dto.getHabilitado());
			pstmt.setString(inc++, dto.getSenha());
			pstmt.setString(inc++, dto.getIdioma());
			pstmt.setTime(inc++, dto.getDataUltimaAlteracaoSenha() == null ? null : DateUtil
					.getDateManagerInstance(dto.getDataUltimaAlteracaoSenha()).getSQLTime());

			if (dto.getEmpresa() == null) {
				pstmt.setNull(inc++, Types.BIGINT);
			} else {
				pstmt.setLong(inc++, dto.getEmpresa().getCodigo());
			}

			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			pstmt.close();

			return dto;
		} catch (SQLException e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "SQLSRV-",
					Constantes.ORA_CODE), e.getMessage());
		}

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final UsuarioDTO update(final Connection conn, final UsuarioDTO dto)
			throws GrowUpDAOException {
		try {
			StringBuffer lsql = new StringBuffer();
			lsql.append("update USUARIO set ");
			lsql.append("    USUA_NM_USUARIO= ?,");
			lsql.append("    USUA_NM_EMAIL= ?,");
			lsql.append("    USUA_CD_SENHA= ?,");
			lsql.append("    USUA_DH_ALTERACAO_SENHA= ?,");
			lsql.append("    USUA_IN_HABILITADO= ?,");
			lsql.append(" 	 PERF_CD_PERGIL= ?,");
			lsql.append(" 	 USUA_IN_IDIOMA= ?,");
			lsql.append("    EMPR_CD_EMPRESA= ?");
			lsql.append(" WHERE USUA_CD_USUARIO = ? ");

			PreparedStatement pstmt = conn.prepareStatement(lsql.toString());

			// limpa valores antigos dos par�metros
			pstmt.clearParameters();
			int inc = 1;
			pstmt.setString(inc++, dto.getNome());
			pstmt.setString(inc++, dto.getEmail());
			pstmt.setString(inc++, dto.getSenha());
			pstmt.setTime(inc++, DateUtil.getDateManagerInstance(
					dto.getDataUltimaAlteracaoSenha()).getSQLTime());
			pstmt.setString(inc++, dto.getHabilitado());
			pstmt.setString(inc++, dto.getPerfil().getCodigo());
			pstmt.setString(inc++, dto.getIdioma());

			if (dto.getEmpresa() == null) {
				pstmt.setNull(inc++, Types.BIGINT);
			} else {
				pstmt.setLong(inc++, dto.getEmpresa().getCodigo());
			}

			pstmt.setString(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			pstmt.close();

			return dto;
		} catch (SQLException e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "SQLSRV-",
					Constantes.ORA_CODE), e.getMessage());
		}

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final UsuarioDTO delete(final Connection conn, final UsuarioDTO dto)
			throws GrowUpDAOException {
		try {
			StringBuffer lsql = new StringBuffer();
			lsql.append("delete from USUARIO ");
			lsql.append("where USUA_CD_USUARIO = ?");
			PreparedStatement pstmt = conn.prepareStatement(lsql.toString());

			// limpa valores antigos dos par�metros
			pstmt.clearParameters();
			pstmt.setString(1, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			pstmt.close();

			return dto;
		} catch (SQLException e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "SQLSRV-",
					Constantes.ORA_CODE), e.getMessage());
		}

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.OracleDAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final UsuarioDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {

		UsuarioDTO userdto = null;

		try {
			userdto = new UsuarioDTO();

			userdto.setCodigo(rs.getString("USUA_CD_USUARIO"));
			userdto.setNome(rs.getString("USUA_NM_USUARIO"));
			userdto.setSenha(rs.getString("USUA_CD_SENHA"));
			userdto.setEmail(rs.getString("USUA_NM_EMAIL"));
			userdto.setIdioma(rs.getString("USUA_IN_IDIOMA"));
			userdto.setDataUltimaAlteracaoSenha(rs.getDate("USUA_DH_ALTERACAO_SENHA") == null ? null
					: DateUtil.getDateManagerInstance(rs.getDate("USUA_DH_ALTERACAO_SENHA"))
							.getDateTime());
			userdto.setHabilitado(rs.getString("USUA_IN_HABILITADO"));

			userdto.setPerfil(this.daoFactory.getPerfilDAO().getDTO(rs, false));
			userdto.setEmpresa(this.daoFactory.getEmpresaDAO().getDTO(rs, false));

			return userdto;
		} catch (Exception ex) {
			throw new GrowUpDAOException(StringUtil.extractString(ex.getMessage(), "SQLSRV-",
					Constantes.ORA_CODE), ex.getMessage());
		}

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.OracleDAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final UsuarioDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);
		if (dto != null) {

			if (dto.getCodigo() != null) {
				param.setParam("USUA_CD_USUARIO", dto.getCodigo());
			} else {
				param.setParam("USUA_NM_USUARIO", dto.getNome());
				param.setParam("USUA_IN_HABILITADO", dto.getHabilitado());
				param.setParam("PERFIL.PERF_CD_PERFIL", dto.getPerfil() == null ? null
						: dto.getPerfil().getCodigo());
				param.setParam("EMPRESA.EMPR_CD_EMPRESA", dto.getEmpresa() == null ? null
						: dto.getEmpresa().getCodigo());
			}

			// Verifica se s�o todas as revis�es
			if (param.getParametros().size() == 0) {
				param.setParam("USUA_CD_USUARIO", String
						.valueOf(ParametroFactory.CODIGO_INEXISTENTE));
			}
		}

		return param;
	}

	/**
	 * Metodo chamado pelo especificamento do trocarSenha do BO de Usuario
	 * 
	 * @param conn
	 * @param dto
	 * @return
	 * @throws GrowUpDAOException
	 */
	@Override
	public final UsuarioDTO updatePassword(final Connection conn, final UsuarioDTO dto)
			throws GrowUpDAOException {
		try {

			StringBuffer lsql = new StringBuffer();
			lsql.append("update USUARIO set ");
			lsql.append(" USUA_CD_SENHA = ? ,");
			lsql.append(" USUA_DH_ALTERACAO_SENHA = ? ");
			lsql.append(" WHERE USUA_CD_USUARIO = ? ");

			PreparedStatement pstmt = conn.prepareStatement(lsql.toString());

			// limpa valores antigos dos par�metros
			pstmt.clearParameters();
			int inc = 1;
			pstmt.setString(inc++, dto.getSenha());
			pstmt.setTime(inc++, DateUtil.getDateManagerInstance(
					dto.getDataUltimaAlteracaoSenha()).getSQLTime());
			pstmt.setString(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			pstmt.close();

			return dto;
		} catch (SQLException e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "SQLSRV-",
					Constantes.ORA_CODE), e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.interfacesdao.UsuarioDAO#updateUltimoAcesso(java.sql.Connection,
	 *      br.com.growupge.dto.UsuarioDTO)
	 */
	@Override
	public UsuarioDTO updateUltimoAcesso(Connection conn, UsuarioDTO dto) throws GrowUpException {
		return null;
	}

	@Override
	public ParametroFactory setParamPage(UsuarioDTO dto) throws GrowUpDAOException {
		return null;
	}

}
