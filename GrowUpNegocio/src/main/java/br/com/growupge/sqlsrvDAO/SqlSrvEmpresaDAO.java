/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.sqlsrvDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.EmpresaDTO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.utility.StringUtil;

/**
 * Classe DAO para gerenciar informa��es pertinentes a Empresa.
 * 
 * @author Felipe
 * 
 */
public class SqlSrvEmpresaDAO extends SqlSrvDAO<EmpresaDTO> implements DAO<EmpresaDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            objeto de f�brica.
	 */
	public SqlSrvEmpresaDAO(final DAOFactory daofactory) {
		super(daofactory);

		sql.append("select");
		sql.append("    EMPRESA.EMPR_CD_EMPRESA,");
		sql.append("    EMPR_NM_EMPRESA ");
		sql.append(" from ");
		sql.append("    EMPRESA");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final EmpresaDTO delete(final Connection conn, final EmpresaDTO dto)
			throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuilder sql = new StringBuilder();

		try {
			sql.append("delete from EMPRESA ");
			sql.append("where EMPR_CD_EMPRESA = ?");
			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setLong(1, dto.getCodigo());
			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (Exception e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {
			try {
				pstmt.close();
			} catch (SQLException e) {
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final EmpresaDTO insert(final Connection conn, final EmpresaDTO dto)
			throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuilder sql = new StringBuilder();

		sql.append("INSERT INTO EMPRESA (");
		sql.append("    EMPR_NM_EMPRESA )");
		sql.append(" values ");
		// sql.append(" (?) ");
		sql.append("('").append(dto.getNome()).append("')");

		try {

			// pstmt = conn.prepareStatement(sql.toString());
			Statement stmt = conn.createStatement();
			stmt.execute(sql.toString());

			// int inc = 1;

			// pstmt.setLong(inc++, dto.codigo);
			// pstmt.setString(inc++, dto.nome);

			// if (pstmt.executeUpdate() == 0) {
			// pstmt.close();
			// throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			// }

			dto.setCodigo(  this.getIdentity(stmt) );

		} catch (Exception e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());

		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e) {
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}
		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final EmpresaDTO update(final Connection conn, final EmpresaDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();

		try {

			sql.append("UPDATE EMPRESA SET ");
			sql.append("    EMPR_NM_EMPRESA =?");
			sql.append(" WHERE EMPR_CD_EMPRESA = ? ");

			PreparedStatement pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;

			pstmt.setString(inc++, dto.getNome());
			pstmt.setLong(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			pstmt.close();
		} catch (Exception e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final EmpresaDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {

		EmpresaDTO dto = null;

		try {
			dto = new EmpresaDTO();

			dto.setCodigo( rs.getLong("EMPR_CD_EMPRESA") );
			dto.setNome( rs.getString("EMPR_NM_EMPRESA"));
		} catch (Exception ex) {
			throw new GrowUpDAOException(StringUtil.extractString(ex.getMessage(), "ORA-",
					Constantes.ORA_CODE), ex.getMessage());
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final EmpresaDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			if (dto.getCodigo() != 0) {
				param.setParam("EMPRESA.EMPR_CD_EMPRESA", dto.getCodigo());
			} else {
				param.setParam("EMPR_NM_EMPRESA", dto.getNome());
			}

			if (param.getParametros().size() == 0) {
				param.setParam("EMPRESA.EMPR_CD_EMPRESA", ParametroFactory.CODIGO_INEXISTENTE);
			}
		}

		param.setOrdem("EMPR_NM_EMPRESA", TipoOrdemDAO.ASC);

		return param;
	}
}
