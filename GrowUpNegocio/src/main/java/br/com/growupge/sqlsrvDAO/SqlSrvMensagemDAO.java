/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 22/11/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 22/11/2006 - In�cio de tudo, por Felipe
 *
 */

package br.com.growupge.sqlsrvDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.MensagemDTO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.utility.StringUtil;

/**
 * DAO para mensagem.
 * 
 * @author elmt
 * 
 */
public class SqlSrvMensagemDAO extends SqlSrvDAO<MensagemDTO> implements DAO<MensagemDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            Objeto de fabrica dao.
	 */
	public SqlSrvMensagemDAO(final DAOFactory daofactory) {

		super(daofactory);

		this.daoFactory = daofactory;

		this.sql = new StringBuffer();

		this.sql.append("select ");
		this.sql.append("	MENS_CD_MENSAGEM,");
		this.sql.append("	MENS_DS_MENSAGEM_BR,");
		this.sql.append("	MENS_DS_MENSAGEM_EN ");
		this.sql.append("from MENSAGEM ");

	}

	/**
	 * M�todo utilizado para obter uma mensagem de acordo com o c�digo
	 * informado.
	 * 
	 * @param msgcode
	 *            C�digo da Mensagem desejada.
	 * @return Um objeto de transfer�ncia populado com os dados da Mensagem.
	 * @throws GrowUpDAOException
	 *             Exce��o padr�o.
	 */
	public final MensagemDTO getMessage(final String msgcode) throws GrowUpDAOException {
		StringBuffer sql = new StringBuffer();
		MensagemDTO msgretorno = null;

		sql.append("select MENS_CD_MENSAGEM, ");
		sql.append("  MENS_DS_MENSAGEM_BR,");
		sql.append("  MENS_DS_MENSAGEM_EN");
		sql.append("from MENSAGEM where MENS_CD_MENSAGEM = ?");

		try {
			PreparedStatement pstmt = daoFactory.getConnection()
					.prepareStatement(sql.toString());
			pstmt.setString(1, msgcode);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				msgretorno = new MensagemDTO();
				msgretorno.setCodigo(rs.getString("MENS_CD_MENSAGEM"));
				msgretorno.setDescricaoBR(rs.getString("MENS_DS_MENSAGEM_BR"));
				msgretorno.setDescricaoEN(rs.getString("MENS_NM_MENSAGEM_EN"));

			}
		} catch (SQLException e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		}

		return msgretorno;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.OracleDAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final MensagemDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {

		MensagemDTO dto = null;

		try {

			dto = new MensagemDTO();

			dto.setCodigo(rs.getString("MENS_CD_MENSAGEM"));
			dto.setDescricaoBR(rs.getString("MENS_DS_MENSAGEM_BR"));
			dto.setDescricaoEN(rs.getString("MENS_DS_MENSAGEM_EN"));

		} catch (Exception e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());

		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.oracledao.OracleDAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final MensagemDTO dto) throws GrowUpDAOException {

		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {

			param.setParam("MENS_CD_MENSAGEM", dto.getCodigo());
			param.setParam("MENS_DS_MENSAGEM_BR", dto.getDescricaoBR());
			param.setParam("MENS_DS_MENSAGEM_EN", dto.getDescricaoEN());

			if (param.getParametros().size() == 0) {
				param.setParam("MENS_CD_MENSAGEM", ParametroFactory.CODIGO_INEXISTENTE);
			}
		}

		param.setOrdem("MENS_DS_MENSAGEM_BR", TipoOrdemDAO.ASC);

		return param;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final MensagemDTO delete(final Connection conn, final MensagemDTO dto)
			throws GrowUpDAOException {

		PreparedStatement ps = null;

		try {

			StringBuilder sql = new StringBuilder("delete from MENSAGEM where MENS_CD_MENSAGEM = ?");

			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, dto.getCodigo());

			if (ps.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

		} catch (Exception e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());

		} finally {

			try {

				ps.close();

			} catch (Exception e) {
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());

			}
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final MensagemDTO insert(final Connection conn, final MensagemDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();

		sql.append("insert into MENSAGEM ( ");
		sql.append("MENS_CD_MENSAGEM, ");
		sql.append("MENS_DS_MENSAGEM_BR, ");
		sql.append("MENS_DS_MENSAGEM_EN ");
		sql.append(")");
		sql.append("values (?, ?, ?)");

		PreparedStatement pstmt = null;

		try {
			int inc = 1;

			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setString(inc++, dto.getCodigo());
			pstmt.setString(inc++, dto.getDescricaoBR());
			pstmt.setString(inc++, dto.getDescricaoEN());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);

			}

		} catch (Exception e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());

		} finally {

			try {

				pstmt.close();

			} catch (Exception e) {
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final MensagemDTO update(final Connection conn, final MensagemDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();

		sql.append("update MENSAGEM set MENS_DS_MENSAGEM_BR = ?, ");
		sql.append("MENS_DS_MENSAGEM_EN = ? ");
		sql.append("where MENS_CD_MENSAGEM = ?");

		PreparedStatement pstmt = null;

		try {
			int inc = 1;

			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setString(inc++, dto.getDescricaoBR());
			pstmt.setString(inc++, dto.getDescricaoEN());
			pstmt.setString(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);

			}

		} catch (Exception e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {

			try {

				pstmt.close();

			} catch (Exception e) {
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}

		return dto;
	}
}
