/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 02/12/2013
 *
 * Historico de Modifica��o:
 * =========================
 * 02/12/2013 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.sqlsrvDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.ClienteDTO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.utility.StringUtil;

/**
 * Classe DAO para gerenciar informa��es pertinentes a Empresa.
 * 
 * @author Felipe
 * 
 */
public class SqlSrvClienteDAO extends SqlSrvDAO<ClienteDTO> implements DAO<ClienteDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            objeto de f�brica.
	 */
	public SqlSrvClienteDAO(final DAOFactory daofactory) {
		super(daofactory);

		sql.append("select");
		sql.append("    CLIENTE.CLIE_CD_CLIENTE,");
		sql.append("    CLIE_NM_CLIENTE, ");
		sql.append("    CLIE_DS_CLIENTE, ");
		sql.append("    CLIE_NM_ARQUIVO ");
		sql.append(" from ");
		sql.append("    CLIENTE");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#delete(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final ClienteDTO delete(final Connection conn, final ClienteDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();

		try {
			sql.append("delete from CLIENTE ");
			sql.append("where CLIE_CD_CLIENTE = ?");
			PreparedStatement pstmt = conn.prepareStatement(sql.toString());

			pstmt.setLong(1, dto.getCodigo());
			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			pstmt.close();

		} catch (Exception e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final ClienteDTO insert(final Connection conn, final ClienteDTO dto)
			throws GrowUpDAOException {

		PreparedStatement pstmt = null;
		StringBuilder sql = new StringBuilder();

		sql.append("INSERT INTO CLIENTE (");
		sql.append("    CLIE_NM_CLIENTE ,");
		sql.append("    CLIE_DS_CLIENTE ,");
		sql.append("    CLIE_NM_ARQUIVO )");
		sql.append(" values ");
		sql.append(" (?,?,?) ");

		try {

			pstmt = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			int inc = 1;

			pstmt.setLong(inc++, dto.getCodigo());
			pstmt.setString(inc++, dto.getNome());
			pstmt.setString(inc++, dto.getDescricao());
			pstmt.setString(inc++, dto.getArquivo());

			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			dto.setCodigo( this.getIdentity(pstmt) );
			pstmt.close();

		} catch (SQLException e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());

		}
		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final ClienteDTO update(final Connection conn, final ClienteDTO dto)
			throws GrowUpDAOException {

		StringBuilder sql = new StringBuilder();

		try {

			sql.append("UPDATE CLIENTE SET ");
			sql.append("    CLIE_NM_CLIENTE =?,");
			sql.append("    CLIE_DS_CLIENTE =?,");
			sql.append("    CLIE_NM_ARQUIVO =?");
			sql.append(" WHERE CLIE_CD_CLIENTE = ? ");

			PreparedStatement pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;

			pstmt.setString(inc++, dto.getNome());
			pstmt.setString(inc++, dto.getDescricao());
			pstmt.setString(inc++, dto.getArquivo());
			pstmt.setLong(inc++, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				pstmt.close();
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}

			pstmt.close();
		} catch (Exception e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		}
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final ClienteDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {

		ClienteDTO dto = null;

		try {
			dto = new ClienteDTO();

			dto.setCodigo( rs.getLong("CLIE_CD_CLIENTE") );
			dto.setNome( rs.getString("CLIE_NM_CLIENTE") );
			dto.setArquivo( rs.getString("CLIE_NM_ARQUIVO") );
			dto.setDescricao (rs.getString("CLIE_DS_CLIENTE") );

		} catch (Exception ex) {
			throw new GrowUpDAOException(StringUtil.extractString(ex.getMessage(), "ORA-",
					Constantes.ORA_CODE), ex.getMessage());
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.interfacesdao.DAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final ClienteDTO dto) throws GrowUpDAOException {
		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {
			if (dto.getCodigo() != 0) {
				param.setParam("CLIENTE.CLIE_CD_CLIENTE", dto.getCodigo());
			} else {
				param.setParam("CLIE_NM_CLIENTE", dto.getNome());
			}

			if (param.getParametros().size() == 0) {
				param.setParam("CLIENTE.CLIE_CD_CLIENTE", ParametroFactory.CODIGO_INEXISTENTE);
			}
		}

		return param;
	}
}
