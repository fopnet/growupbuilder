package br.com.growupge.sqlsrvDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.CargoDTO;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.utility.StringUtil;

/**
 * Classe DAO para gerenciar informa��es pertinentes a permiss�es.
 * 
 * @author Felipe
 * 
 */
public class SqlSrvCargoDAO extends SqlSrvDAO<CargoDTO> implements DAO<CargoDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            Inst�ncia da F�brica de DAO.
	 */
	public SqlSrvCargoDAO(final DAOFactory daofactory) {
		super(daofactory);

		sql.append(" select ");
		sql.append("  CARG_CD_CARGO, ");
		sql.append("  CARG_DS_CARGO, ");
		sql.append("  DEPARTAMENTO.DEPT_CD_DEPARTAMENTO ");
		sql.append(" from ");
		sql.append("   CARGO");
		sql.append("   INNER JOIN DEPARTAMENTO");
		sql.append("   ON DEPARTAMENTO.DEPT_CD_DEPARTAMENTO = CARGO.DEPT_CD_DEPARTAMENTO");

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.growupge.sigem.oracledao.OracleDAO#getDTO(java.sql.ResultSet, boolean)
	 */
	@Override
	public final CargoDTO getDTO(final ResultSet rs, boolean isEager) throws GrowUpDAOException {
		CargoDTO dto = null;
		try {
			dto = new CargoDTO();
			dto.setCodigo(rs.getString("CARG_CD_CARGO"));
			dto.setDescricao(rs.getString("CARG_DS_CARGO"));
			dto.setDepartamento(this.daoFactory.getDepartamentoDAO().getDTO(rs, false));

			return dto;

		} catch (Exception ex) {
			throw new GrowUpDAOException(StringUtil.extractString(ex.getMessage(), "ORA-",
					Constantes.ORA_CODE), ex.getMessage());
		}

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.growupge.sigem.oracledao.OracleDAO#setParam(java.lang.Object)
	 */
	@Override
	public final ParametroFactory setParam(final CargoDTO dto) throws GrowUpDAOException {

		ParametroFactory param = ParametroFactory.getInstance(this);
		param.setQuery(this.sql);

		if (dto != null) {

			if (dto.getCodigo() != null) {
				param.setParam("CARG_CD_CARGO", dto.getCodigo() == null ? dto.getCodigo() : dto.getCodigo().trim()
						.toUpperCase());

			} else {
				param.setParam("CARG_DS_CARGO", dto.getDescricao() == null ? dto.getDescricao()
						: dto.getDescricao().trim().toUpperCase());
				param.setParam("DEPARTAMENTO.DEPT_CD_DEPARTAMENTO", dto.getDepartamento() == null
						|| dto.getDepartamento().codigo == null ? null : dto.getDepartamento().codigo.trim()
						.toUpperCase());
				param.setParam("DEPT_DS_DEPARTAMENTO", dto.getDepartamento() == null ? null
						: dto.getDepartamento().descricao);
			}

			if (param.getParametros().size() == 0) {
				param
						.setParam("CARG_CD_CARGO", String
								.valueOf(ParametroFactory.CODIGO_INEXISTENTE));
			}
		}

		param.setOrdem("CARG_DS_CARGO", TipoOrdemDAO.ASC);

		return param;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.growupge.sigem.interfacesdao.DAO#deletePagamentosPayPal(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final CargoDTO delete(final Connection conn, final CargoDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append("delete from CARGO ");
			sql.append("where CARG_CD_CARGO = ?");
			pstmt = conn.prepareStatement(sql.toString());

			/* Limpa valores antigos dos par�metros. */
			pstmt.clearParameters();
			pstmt.setString(1, dto.getCodigo());

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.REGISTRO_EXCLUIDO_PREVIAMENTE);
			}

		} catch (SQLException e) {

			if (e.getErrorCode() == 2292) {

				throw new GrowUpDAOException(MSGCODE.PERMISSAO_REGISTROS_ASSOCIADOS);

			}

			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {
			try {
				pstmt.close();
			} catch (Exception e) {
				
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}

		return dto;

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.growupge.sigem.interfacesdao.DAO#insert(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final CargoDTO insert(final Connection conn, final CargoDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();
		try {
			sql.append("insert into CARGO(");
			sql.append("  CARG_CD_CARGO, ");
			sql.append("  CARG_DS_CARGO, ");
			sql.append("  DEPT_CD_DEPARTAMENTO) ");
			sql.append("values (?,?,?)");
			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			pstmt.setString(inc++, dto.getCodigo());
			pstmt.setString(inc++, dto.getDescricao());
			pstmt.setString(inc++, dto.getDepartamento().codigo);

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}
		} catch (SQLException e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {
			try {
				pstmt.close();
			} catch (Exception e) {
				
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}

		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.growupge.sigem.interfacesdao.DAO#update(java.sql.Connection,
	 *      java.lang.Object)
	 */
	@Override
	public final CargoDTO update(final Connection conn, final CargoDTO dto)
			throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer();

		try {
			sql.append(" update CARGO ");
			sql.append(" set CARG_DS_CARGO =?, ");
			sql.append(" 	 DEPT_CD_DEPARTAMENTO =? ");
			sql.append(" where CARG_CD_CARGO= ?");

			pstmt = conn.prepareStatement(sql.toString());

			int inc = 1;
			/* limpa valores antigos dos par�metros */
			pstmt.clearParameters();
			pstmt.setString(inc++, dto.getDescricao());
			pstmt.setString(inc++, dto.getCodigo());
			pstmt.setString(inc++, dto.getDepartamento().codigo);

			if (pstmt.executeUpdate() == 0) {
				throw new GrowUpDAOException(MSGCODE.COMANDO_SQL_NAO_EFETUADO);
			}
		} catch (SQLException e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {
			try {
				pstmt.close();
			} catch (Exception e) {
				
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}

		return dto;
	}


}
