package br.com.growupge.sqlsrvDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.enums.TipoPerfil;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DtoBuilder;
import br.com.growupge.utility.StringUtil;

/**
 * Classe DAO abstrata que implementa funcionalidades comuns as demais classes
 * DAO e define m�todos abstratos que s�o imlementados pelas mesmas.
 * 
 * @author Felipe
 * 
 * @param <DTO>
 */
public abstract class SqlSrvDAO<DTO> {

	/**
	 * Atributo '<code>sql</code>' do tipo StringBuffer
	 */
	protected StringBuffer sql = null;

	protected SessaoDTO sessaoDTO;

	/**
	 * Atributo '<code>daofactory</code>' do tipo DAOFactory
	 */
	protected DAOFactory daoFactory = null;

	/**
	 * Construtor para esta classe.
	 * 
	 * @param daofactory
	 *            Inst�ncia da F�brica de DAO.
	 */
	public SqlSrvDAO(final DAOFactory daofactory) {
		this.daoFactory = daofactory;
		this.sql = new StringBuffer();
	}

	/**
	 * M�todo utilizado para obter um DTO populado com as informa��es obtidas do
	 * ResultSet informado.
	 * 
	 * @param rs
	 *            ResultSet com as informa��es para popular o DTO.
	 * @param isEager TODO
	 * @return Um DTO populado com as informa��es obtidas do ResultSet
	 *         informado.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro durante a atribui��o de valores do
	 *             ResultSet para o DTO.
	 */
	public abstract DTO getDTO(ResultSet rs, boolean isEager) throws GrowUpDAOException;

	/**
	 * M�todo utilizado para configurar oconte�do das colunas para montar o SQL
	 * din�mico.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia que cont�m os valores a serem
	 *            configurados.
	 * @return A inst�ncia de ParametroFactory para manipular o SQL.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro de banco durante a opera��o.
	 */
	public abstract ParametroFactory setParam(DTO dto) throws GrowUpDAOException;

	/**
	 * @param conn
	 * @param dto
	 * @param isClausulaLike
	 * @param indiceInicial
	 * @return
	 * @throws GrowUpDAOException
	 */
	public final List<DTO> selectAll(final Connection conn, final boolean isClausulaLike,
			final Integer indiceInicial, final ParametroFactory param) throws GrowUpDAOException {
		return null;
	}

	public final List<DTO> selectAll(final Connection conn, DTO dto) throws GrowUpDAOException {
		return null;
	}

	/**
	 * M�todo utilizado para retornar todos os registros que atendem os
	 * crit�rios do objeto de transfer�ncia informado.
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o com o banco.
	 * @param dto
	 *            Cont�m a informa��o do registro a ser encontrado no banco de
	 *            dados. Se o objeto 'dto' for nulo, este m�todo retorna todos
	 *            os valores referentes a este business object, ou seja, ser�
	 *            feito um select * no banco.
	 * @param isClausulaLike
	 *            determina se o select usar� a clausula Like ou Where
	 * @param indiceInicial
	 *            Indice inicial para fazer a pagina��o.
	 * @return Uma Collection com todos os locais.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum falha durante a pesquisa.
	 */
	public final List<DTO> selectAll(final Connection conn, final DTO dto,
			final boolean isClausulaLike, final Integer indiceInicial) throws GrowUpDAOException {
		ArrayList<DTO> lista = null;
		PreparedStatement pstmt = null;

		try {
			ParametroFactory param = this.setParam(dto);

			lista = new ArrayList<DTO>();

			param.setPaginacao(indiceInicial, this.getSessao().getUsuario().getQtdRegistros());

			if (isClausulaLike && param.isClausulaLike()) {
				pstmt = param.getLikeStatement(conn);
			} else {
				pstmt = param.getStatement(conn);
			}

			ResultSet rs = pstmt.executeQuery();

			@SuppressWarnings("unchecked")
			DtoBuilder<DTO> builder = param.getBuilder();
			// Encontrou registro?
			while (rs.next()) {
				// Popula informa��es da empresa
				//lista.add(this.getDTO(rs, false));
				builder.addDto(lista, builder.getDTO(rs, false));
			}

			return lista;

		} catch (SQLException e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		}
	}

	/**
	 * M�todo utilizado para retornar todos os registros que atendem os
	 * crit�rios do objeto de transfer�ncia informado.
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o com o banco.
	 * @param dto
	 *            Cont�m a informa��o do registro a ser encontrado no banco de
	 *            dados. Se o objeto 'dto' for nulo, este m�todo retorna todos
	 *            os valores referentes a este business object, ou seja, ser�
	 *            feito um select * no banco.
	 * @return Quantidade de registros
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum falha durante a pesquisa.
	 */
	public final Long count(final Connection conn, final DTO dto) throws GrowUpDAOException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			ParametroFactory param = this.setParam(dto);

			String[] qrySelect = param.getQuery().toString().toUpperCase().split(",");
			StringBuffer qryCount = new StringBuffer();
			qryCount.append(qrySelect[0].replace("SELECT", "SELECT COUNT (").concat(") "));
			qryCount.append(qrySelect[qrySelect.length - 1]
					.substring(qrySelect[qrySelect.length - 1].indexOf("FROM")));
			param.setQuery(qryCount, true);

			if (param.isClausulaLike()) {
				pstmt = param.getLikeStatement(conn);
			} else {
				pstmt = param.getStatement(conn);
			}

			rs = pstmt.executeQuery();
			if (rs.next()) {
				return rs.getLong(1);
			} else {
				return null;
			}

		} catch (SQLException e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e) {
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}
	}

	/**
	 * 
	 * M�todo utilizado para encontrar um registro de acordo com os dados
	 * informados no objeto de transfer�ncia.
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o com o banco.
	 * @param dto
	 *            Cont�m a informa��o do registro a ser encontrado no banco de
	 *            dados.
	 * @return Um transfer object populado com a informa��o do registro a ser
	 *         encontrado.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro durante a pesquisa.
	 */
	public final DTO find(final Connection conn, final DTO dto) throws GrowUpDAOException {
		DTO ldto = null;

		try {

			ParametroFactory param = this.setParam(dto);

			PreparedStatement pstmt = param.getStatement(conn);
			ResultSet rs = pstmt.executeQuery();

			// Encontrou registro?
			if (rs.next()) {
				// Popula informa��es
				ldto = this.getDTO(rs, false);
			}

			return ldto;

		} catch (SQLException e) {
			
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		}
	}

	protected final Long getIdentity(final Statement pstmt) throws GrowUpDAOException {
		ResultSet rs = null;
		Long identity = null;

		try {
			rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				identity = rs.getLong(1);
			}

		} catch (SQLException e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} catch (Exception e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}

		return identity;

	}

	public final SessaoDTO getSessao() {
		return this.sessaoDTO;
	}

	public final void setSessao(SessaoDTO sessao) {
		this.sessaoDTO = sessao;
	}

	public boolean isPerfilUsuarioMaior(TipoPerfil perfil) {
		return false;
	}

	public boolean isUsuarioAdmin() {
		return false;
	}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.interfacesdao.ConsultaDAO#isPerfilUsuarioMaior(br.com.growupge.enums.TipoPerfil, java.lang.String)
	 */
	public boolean isPerfilUsuarioMaior(TipoPerfil perfil, String codigoPerfil) {
		// TODO Auto-generated method stub
		return false;
	}

	public void addDto(List<DTO> lista, DTO dto) {
		lista.add(dto);
	}

	public <OUT> List<OUT> selectAll(final Connection conn, 
			final DTO dto, 
			final boolean isClausulaLike,
			final Integer indiceInicial,
			final List<OUT> lista) throws GrowUpDAOException {
		return null;
	}
}
