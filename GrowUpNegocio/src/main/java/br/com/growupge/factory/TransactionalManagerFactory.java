package br.com.growupge.factory;

import java.sql.Connection;

import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import br.com.growupge.exception.GrowUpDAOException;

@Named("transactional")
@RequestScoped
public class TransactionalManagerFactory implements Transactional  {

	@javax.inject.Inject
	@TransactionalManager
	private Transactional manager; 
	
	@Override
	public Connection getConnection() throws GrowUpDAOException {
		return manager.getConnection();
	}

	@Override
	public void closeCommit() throws GrowUpDAOException {
		manager.closeCommit();
		
	}

	@Override
	public void rollBack() throws GrowUpDAOException {
		manager.rollBack();
	}
	
	@PreDestroy
	public void close()  {
		try {
			manager.close();
		} catch (GrowUpDAOException e) {
			throw new RuntimeException(e);
		}
	}
	
}
