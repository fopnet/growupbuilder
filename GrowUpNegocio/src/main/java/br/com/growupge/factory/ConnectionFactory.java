package br.com.growupge.factory;

import java.sql.Connection;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.growupge.exception.GrowUpDAOException;

@Named("connectable")
@RequestScoped
public class ConnectionFactory implements Connectable {

	@Inject
	@TransactionalManager
	private Transactional manager; 
	
	@Override 
	public Connection getConnection() throws GrowUpDAOException {
		return manager.getConnection(); 
	}


//	@Override
//	public void closeCommit() throws GrowUpDAOException {
//		// nao comitar
//	}
//
//	@Override
//	public void rollBack() throws GrowUpDAOException {
//		// nao fazer rollback
//	}
	
//	@PreDestroy
	public void close() throws GrowUpDAOException {
//		manager.close();
	}
}
