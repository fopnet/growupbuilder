/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 07/02/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 07/02/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.factory;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.template.CobrancaUsuarioComumTemplate;
import br.com.growupge.template.ConfirmarEmailTemplate;
import br.com.growupge.template.ContatoTemplate;
import br.com.growupge.template.EmailTemplateTrocaSenha;
import br.com.growupge.template.HtmlTemplate;
import br.com.growupge.template.NotificarAdminTemplate;
import br.com.growupge.template.RelatorioPedidoTemplate;
import br.com.growupge.template.UsuarioHabilitadoTemplate;
import br.com.growupge.template.UsuariosExpiradosTemplate;

/**
 * F�brica de templates
 * 
 * @author Felipe
 * 
 */
public final class HtmlTemplateFactory {
	/**
	 * Construtor privado para esta classe.
	 * 
	 */
	private HtmlTemplateFactory() {
	}

	/**
	 * Inst�ncia da classe Template que gerencia informa��es qualquer template
	 * de email
	 * 
	 * @return - Retorna um objeto do tipo Template
	 * @throws SigemException -
	 *             Exce��o disparada caso ocorra algum erro
	 */
	public static HtmlTemplate getTemplateNovoUsuario() throws GrowUpException {
		return new NotificarAdminTemplate(Constantes.VARIAVEL_TMPLT_NOTIFICAR_ADMIN);
	}

	/**
	 * Inst�ncia da classe Template para notificar usu�rios expirados
	 * 
	 * @return
	 * @throws GrowUpException
	 */
	public static UsuariosExpiradosTemplate getTemplateUsuarioExpirados() throws GrowUpException {
		return new UsuariosExpiradosTemplate();
	}

	/**
	 * Inst�ncia da classe Template que gerencia informa��es qualquer template
	 * de email
	 * 
	 * @return - Retorna um objeto do tipo Template
	 * @throws SigemException -
	 *             Exce��o disparada caso ocorra algum erro
	 */
	public static HtmlTemplate getTemplateConfirmarEmail() throws GrowUpException {
		return new ConfirmarEmailTemplate();
	}

	public static HtmlTemplate getTemplateUsuarioHabilitado() throws GrowUpException {
		return new UsuarioHabilitadoTemplate();
	}

	public static HtmlTemplate getTemplateCobrancaUsuarioComum() throws GrowUpException {
		return new CobrancaUsuarioComumTemplate();
	}

	public static HtmlTemplate getTemplateNovoPedido() throws GrowUpException {
		return new NotificarAdminTemplate(Constantes.VARIAVEL_TMPLT_NOTIFICAR_NOVO_PEDIDO);
	}

	/**
	 * Inst�ncia da classe Template que gerencia informa��es de template de
	 * email com finalidades espec�ficas.
	 * 
	 * @param finalidade
	 *            designa a finalidade do template
	 * @return - Retorna um objeto do tipo Template
	 * @throws GrowupException -
	 *             Exce��o disparada caso ocorra algum erro
	 */
	public static HtmlTemplate getTemplateNovaSenha() throws GrowUpException {
		return new EmailTemplateTrocaSenha(Constantes.VARIAVEL_TMPLT_NOTIFICAR_SENHA);
	}

	/**
	 * Inst�ncia da classe Template que gerencia informa��es de template de
	 * email com finalidades espec�ficas.
	 * @param folder 
	 * 
	 * @param finalidade
	 *            designa a finalidade do template
	 * @return - Retorna um objeto do tipo Template
	 * @throws GrowupException -
	 *             Exce��o disparada caso ocorra algum erro
	 */
	public static HtmlTemplate getTemplateRespostaPedido() throws GrowUpException {
		return new RelatorioPedidoTemplate();
	}

	/**
	 * Inst�ncia da classe Template que gerencia informa��es de template de
	 * email com finalidades espec�ficas.
	 * 
	 * @param finalidade
	 *            designa a finalidade do template
	 * @return - Retorna um objeto do tipo Template
	 * @throws GrowupException -
	 *             Exce��o disparada caso ocorra algum erro
	 */
	public static HtmlTemplate getTemplateContato() throws GrowUpException {
		return new ContatoTemplate();
	}
}
