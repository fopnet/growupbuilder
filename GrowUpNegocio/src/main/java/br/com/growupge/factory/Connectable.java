/**
 * 
 */
package br.com.growupge.factory;

import java.sql.Connection;

import br.com.growupge.exception.GrowUpDAOException;

/**
 * @author Felipe
 *
 */
public interface Connectable {

	/**
	 * Retorna a conex�o com o BD, tendo a possibilidade de n�o recriar
	 * conex�es.
	 * 
	 * @return Um objeto do tipo Connection.
	 */
	public abstract Connection getConnection() throws GrowUpDAOException;

	/**
	 * @throws GrowUpDAOException 
	 * 
	 */
	public abstract void close() throws GrowUpDAOException;

}