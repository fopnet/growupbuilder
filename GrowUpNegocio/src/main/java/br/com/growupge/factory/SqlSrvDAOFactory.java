package br.com.growupge.factory;

import br.com.growupge.dto.AnoModeloDTO;
import br.com.growupge.dto.ArquivoDTO;
import br.com.growupge.dto.CargoDTO;
import br.com.growupge.dto.ClienteDTO;
import br.com.growupge.dto.DepartamentoDTO;
import br.com.growupge.dto.EmpresaDTO;
import br.com.growupge.dto.EmpresaItemDTO;
import br.com.growupge.dto.MarcaDTO;
import br.com.growupge.dto.MensagemDTO;
import br.com.growupge.dto.ModeloDTO;
import br.com.growupge.dto.PerfilDTO;
import br.com.growupge.dto.PerfilPermissaoItemDTO;
import br.com.growupge.dto.PermissaoDTO;
import br.com.growupge.dto.PessoaDTO;
import br.com.growupge.dto.ProprietarioDTO;
import br.com.growupge.dto.ServicoDTO;
import br.com.growupge.dto.UnidadeFederativaDTO;
import br.com.growupge.dto.VariavelDTO;
import br.com.growupge.dto.VeiculoDTO;
import br.com.growupge.dto.VeiculoProprietarioDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.interfacesdao.ConsultaDAO;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.interfacesdao.PagamentoDAO;
import br.com.growupge.interfacesdao.PedidoVeiculoDAO;
import br.com.growupge.interfacesdao.PerfilDAO;
import br.com.growupge.interfacesdao.RestricaoDAO;
import br.com.growupge.interfacesdao.SequenciaDAO;
import br.com.growupge.interfacesdao.SessaoDAO;
import br.com.growupge.interfacesdao.TramitacaoDAO;
import br.com.growupge.interfacesdao.UsuarioDAO;
import br.com.growupge.sqlsrvDAO.SqlSrvCargoDAO;
import br.com.growupge.sqlsrvDAO.SqlSrvClienteDAO;
import br.com.growupge.sqlsrvDAO.SqlSrvDepartamentoDAO;
import br.com.growupge.sqlsrvDAO.SqlSrvEmpresaClienteDAO;
import br.com.growupge.sqlsrvDAO.SqlSrvEmpresaDAO;
import br.com.growupge.sqlsrvDAO.SqlSrvMensagemDAO;
import br.com.growupge.sqlsrvDAO.SqlSrvPedidoVeiculoDAO;
import br.com.growupge.sqlsrvDAO.SqlSrvPerfilDAO;
import br.com.growupge.sqlsrvDAO.SqlSrvPerfilPermissaoDAO;
import br.com.growupge.sqlsrvDAO.SqlSrvPessoaDAO;
import br.com.growupge.sqlsrvDAO.SqlSrvSessaoDAO;
import br.com.growupge.sqlsrvDAO.SqlSrvUsuarioDAO;

/**
 * F�brica DAO para Banco de Dados Oracle.
 * 
 * @author eii5
 * 
 */
//@javax.enterprise.inject.Alternative
@br.com.growupge.stereotype.SqlSrcStereoType
public class SqlSrvDAOFactory extends DAOFactory {

	/**
	 * Construtor para esta classe. Inicializa uma transa��o isolada para esta
	 * f�brica
	 * 
	 * @throws GrowUpException
	 *             Caso ocorra algum erro que n�o seja de banco.
	 */
	public SqlSrvDAOFactory() throws GrowUpException {
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getUsuarioDAO()
	 */
	@Override
	public final UsuarioDAO getUsuarioDAO() {
		return new SqlSrvUsuarioDAO(this);
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getSessaoDAO()
	 */
	@Override
	public final SessaoDAO getSessaoDAO() {
		return new SqlSrvSessaoDAO(this);
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getEmpresaDAO()
	 */
	@Override
	public final DAO<EmpresaDTO> getEmpresaDAO() {
		return new SqlSrvEmpresaDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getEmpresaItemDAO()
	 */
	@Override
	public DAO<EmpresaItemDTO> getEmpresaItemDAO() {
		return new SqlSrvEmpresaClienteDAO(this);
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getPermissaoDAO()
	 */
	@Override
	public final DAO<PermissaoDTO> getPermissaoDAO() {
		return null;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getPerfilDAO()
	 */
	@Override
	public final DAO<PessoaDTO> getPessoaDAO() {
		return new SqlSrvPessoaDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getMensagemDAO()
	 */
	@Override
	public DAO<MensagemDTO> getMensagemDAO() {
		return new SqlSrvMensagemDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getClienteDAO()
	 */
	@Override
	public DAO<ClienteDTO> getClienteDAO() {
		return new SqlSrvClienteDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getPedidoVeiculoDAO()
	 */
	@Override
	public PedidoVeiculoDAO getPedidoVeiculoDAO() {
		return new SqlSrvPedidoVeiculoDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getPerfilPermissaoDAO()
	 */
	@Override
	public DAO<PerfilPermissaoItemDTO> getPerfilPermissaoDAO() {
		return new SqlSrvPerfilPermissaoDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getPerfilDAO()
	 */
	@Override
	public PerfilDAO<PerfilDTO> getPerfilDAO() {
		return new SqlSrvPerfilDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getAreaDAO()
	 */
	@Override
	public DAO<DepartamentoDTO> getDepartamentoDAO() {
		return new SqlSrvDepartamentoDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getCargoDAO()
	 */
	@Override
	public DAO<CargoDTO> getCargoDAO() {
		return new SqlSrvCargoDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getSequenciaDAO()
	 */
	@Override
	public SequenciaDAO getSequenciaDAO() {
		return null;
	}

	@Override
	public DAO<MarcaDTO> getMarcaDAO() {
		return null;
	}

	@Override
	public DAO<ModeloDTO> getModeloDAO() {
		return null;
	}

	@Override
	public DAO<ProprietarioDTO> getProprietarioDAO() {
		return null;
	}

	@Override
	public DAO<VeiculoDTO> getVeiculoDAO() {
		return null;
	}

	@Override
	public DAO<VeiculoProprietarioDTO> getVeiculoProprietarioDAO() {
		return null;
	}

	@Override
	public DAO<ServicoDTO> getServicoDAO() {
		return null;
	}

	@Override
	public TramitacaoDAO getTramitacaoDAO() {
		return null;
	}

	@Override
	public ConsultaDAO<UnidadeFederativaDTO> getUfDAO() {
		return null;
	}

	@Override
	public DAO<ArquivoDTO> getArquivoDAO() {
		return null;
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.factory.DAOFactory#getRestricaoDAO()
	 */
	@Override
	public RestricaoDAO getRestricaoDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.factory.DAOFactory#getAnoModeloDAO()
	 */
	@Override
	public DAO<AnoModeloDTO> getAnoModeloDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DAO<VariavelDTO> getVariavelDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PagamentoDAO getPagamentoDAO() {
		// TODO Auto-generated method stub
		return null;
	}

}