package br.com.growupge.factory;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.interfacesdao.DtoBuilder;

/**
 * Classe utilit�ria para a manipula��o do SQL din�mico.
 * 
 * @author Felipe Pina
 * 
 */
public final class SqlSrvParametroFactory extends ParametroFactory {

	/**
	 * Construtor para esta classe.
	 * 
	 */
	public SqlSrvParametroFactory(DtoBuilder<?> builder) {
		super(builder);
	}

	/**
	 * M�todo que insere a qry de pagina��o.
	 * 
	 * @param sb
	 *            Par�metro do tipo StringBuffer
	 */
	@Override
	protected void setQryPaginacao(final StringBuffer sb) {
		StringBuffer clausulaOrderBy = this.getClausulaOrdem();

		if ((sb != null) && (this.getIndicePaginacao() != Constantes.DESCONSIDERAR_PAGINACAO)) {
			StringBuffer qryPaginacao = new StringBuffer();

			qryPaginacao.append(" top ");
			qryPaginacao.append(this.getPagina());
			sb.insert(sb.toString().toUpperCase().indexOf("SELECT") + "SELECT".length(),
					qryPaginacao);
			sb.append(sb.toString().toUpperCase().indexOf("WHERE") > 0 ? " AND " : " WHERE ");
			sb.append(this.getNomeChavePrimaria());
			sb.append(" not in ( select top ");
			sb.append(this.getPagina() * this.getIndicePaginacao());
			sb.append(" ".concat(this.getNomeChavePrimaria()));
			sb.append(" from ");
			sb.append(this.getNomeTabelaOrigem());
			sb.append(clausulaOrderBy);
			sb.append(" ) ");

		}

//		if (!isIgnorarClausulaOrderBy) {
//			sb.append(clausulaOrderBy);
//		}

	}

}
