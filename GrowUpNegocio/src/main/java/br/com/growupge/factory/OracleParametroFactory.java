package br.com.growupge.factory;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.interfacesdao.DtoBuilder;

/**
 * Classe utilit�ria para a manipula��o do SQL din�mico.
 * 
 * @author Felipe Pina
 * 
 */
public final class OracleParametroFactory extends ParametroFactory {

	/**
	 * Construtor para esta classe.
	 * 
	 */
	public OracleParametroFactory(DtoBuilder<?> builder) {
		super(builder);
	}

	/**
	 * M�todo que insere a qry de pagina��o.
	 * 
	 * @param sb
	 *            Par�metro do tipo StringBuffer
	 */
	@Override
	protected void setQryPaginacao(final StringBuffer sb) {
		StringBuffer clausulaOrderBy = this.getClausulaOrdem();

		if ((sb != null) && (this.getIndicePaginacao() != Constantes.DESCONSIDERAR_PAGINACAO)) {
			StringBuffer qryPaginacao = new StringBuffer();

			qryPaginacao.append("select * ");
			qryPaginacao.append("from ( select /*+ FIRST_ROWS(n) */ ");
			qryPaginacao.append("topn.*, ROWNUM rnum ");
			qryPaginacao.append("from ( ");
			sb.insert(0, qryPaginacao);
			sb.append(clausulaOrderBy);
			// qryPaginacao.append(sb.append(clausulaOrderBy));
			sb.append(") topn");
			sb.append(" where ROWNUM <= ?");
			sb.append(") ");
			sb.append("where rnum  > ? ");
		}

		sb.append(clausulaOrderBy);

	}

}
