package br.com.growupge.factory;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.utility.OS;
import br.com.growupge.utility.StringUtil;

import javax.enterprise.context.RequestScoped;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author EII5
 *
 */
public class TransactionFactory implements Transactional {

	private Connection connection;
	
	@javax.enterprise.inject.Produces
	@javax.inject.Named
	@TransactionalManager
	@RequestScoped
	public Transactional getInstance () {
		return new TransactionFactory(); 
	}
	
	
	/**
	 * Retorna a conex�o com o BD.
	 * 
	 * @return Um objeto do tipo Connection.
	 */
	private /*final */ Connection getConnectionByDataSource() throws GrowUpDAOException {
		Connection conn = null;
		try {
			Context ctxt = new InitialContext();
			Context ct = (Context) ctxt.lookup("java:/comp/env");

			String dataSourceName = Constantes.ACTIVE_DATA_SOURCE;

            // This reason is the IDE hotswap deploy, it does not run filtering feature maven
            if (dataSourceName.startsWith("${"))
                dataSourceName = "jdbc/" + (OS.isMac() ? "localFirebirdDS_mac"  : "localFirebirdDS");

			DataSource ds = (DataSource) ct.lookup(dataSourceName);

			if (ds == null) {
				throw new GrowUpDAOException("IE-XXXX", "Cannot create database connection");
			}
			
			conn = ds.getConnection();
			
			ctxt.close();
		} catch (GrowUpDAOException e) {
			throw e;
		} catch (Exception ex) {
			throw new GrowUpDAOException(ErroInterno.COMUNICACAO_COMPONENTE, ex.getMessage());
		} finally {
		}
		return conn;
	}
	
	/**
	 * Metodo que repassa a SqlException atrav�s da GrowUpDAOException
	 * @param e
	 * @throws GrowUpDAOException
	 */
	protected void dispararError(SQLException e) throws GrowUpDAOException {
		throw new GrowUpDAOException(StringUtil.extractString(
				e.getMessage(),
				"ORA-",
				Constantes.ORA_CODE), e.getMessage());
	}

	/*****************************************************************************************************
	 * Transactional implementations
	 *****************************************************************************************************/

	/**
	 * Retorna a conex�o com o BD, tendo a possibilidade de n�o recriar
	 * conex�es.
	 * 
	 * @return Um objeto do tipo Connection.
	 */
	@Override
	public /*final*/ Connection getConnection() throws GrowUpDAOException {
		try {
			if (connection == null) {
				connection = getConnectionByDataSource();
				connection.setAutoCommit(false);
			}		
			
		} catch (SQLException e) {
			dispararError(e);
		} finally {
		}
		return connection;
	}
	
	/**
	 * @throws GrowUpDAOException
	 */
	public void close() throws GrowUpDAOException {
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException e) {
			dispararError(e);
		} finally {
			connection = null;
		}
	}


	/**
	 * Fecha a conex�o com o BD.
	 * 
	 * @throws Exception
	 *             Caso ocorra algum erro durante a opera��o.
	 */
	@Override
	public /*final*/ void closeCommit() throws GrowUpDAOException {
		try {
			if (!getConnection().isClosed()) {
				if (!getConnection().getAutoCommit()) {
					getConnection().commit();
				}
			}
		} catch (SQLException e) {
			dispararError(e);
		} finally {
			close();
		}
	}
	
	/**
	 * @throws Exception
	 */
	@Override
	public /*final*/ void rollBack() throws GrowUpDAOException {
		try {
			if (getConnection().getTransactionIsolation() != Connection.TRANSACTION_NONE) {
				getConnection().rollback();
			}
		} catch (SQLException e) {
			dispararError(e);
		} finally {
			close();
		}
	}
	
}
