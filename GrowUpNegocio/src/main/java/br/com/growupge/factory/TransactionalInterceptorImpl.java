package br.com.growupge.factory;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.gerenciadores.TransactionalInterceptor;

@TransactionalInterceptor @Interceptor 
public class TransactionalInterceptorImpl {
	
	@javax.inject.Inject
	@TransactionalManager 
	private Transactional manager;
	
	private final static Logger log = Logger.getLogger(TransactionalInterceptorImpl.class);
	
	@AroundInvoke
    public Object manageTransaction(InvocationContext context) throws GrowUpException {
		try {
			final Object result = context.proceed();
			
			manager.closeCommit();
			return result;
		}
		catch ( GrowUpException e ) {
			log.error( "N�o foi poss�vel comitar a transa��o.", e );
			manager.rollBack();
			throw e;
		} catch (Exception e) {
			log.error( "Exce��o inesperada. N�o foi poss�vel comitar a transa��o.", e );
			manager.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, "Ops.. Ocorreu um erro inesperado. Favor entre em contato com o suporte t�cnico.");
		}
		
	}
}
