package br.com.growupge.factory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.enums.TipoOperadorDAO;
import br.com.growupge.enums.TipoOperadorSQL;
import br.com.growupge.enums.TipoOrdemDAO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.global.GlobalStartup;
import br.com.growupge.interfacesdao.DtoBuilder;
import br.com.growupge.utility.DateUtil;
import br.com.growupge.utility.StringUtil;

/**
 * Classe utilit�ria para a manipula��o do SQL din�mico.
 * 
 * @author Felipe Pina
 * 
 */
public abstract class ParametroFactory {

	/**
	 * Atributo utilizado para representar que na cria��o do select din�mico, se
	 * o conte�do de 'value' passado para o m�todo setParam(String col, String
	 * value) for igual a '-1', significa que ser� tratado como valor
	 * inexistente e n�o ser� feito um 'select *' no banco.
	 */
	public static final int CODIGO_INEXISTENTE = -1;

	/**
	 * Atributo '<code>sql</code>' do tipo StringBuffer
	 */
	private StringBuffer sql;

	/**
	 * Atributo '<code>parametros</code>' do tipo Stack<String>
	 */
	private Stack<Object> parametros;

	/**
	 * Atributo '<code>clausulas</code>' do tipo Stack<String>
	 */
	private Stack<String> clausulaWhere;

	/**
	 * Atributo '<code>orderBy</code>' do tipo Stack<String>
	 */
	private Queue<String> clausulaOrderBy;

	/**
	 * Atributo '<code>operadoresSQL</code>' do tipo Stack<TipoOperadorDAO>
	 */
	private Stack<TipoOperadorSQL> operadoresSQL;

	/**
	 * Atributo '<code>operadoresDAO</code>' do tipo Stack<TipoOperadorDAO>
	 */
	private Stack<TipoOperadorDAO> operadoresDAO;

	/**
	 * Atributo '<code>join</code>' do tipo Stack<String>
	 */
	private Stack<String> clausulaFrom;

	/**
	 * Atributo '<code>colunasParam</code>' do tipo Stack<String>
	 */
	private Stack<String> colunasParam;

	/**
	 * Atributo '<code>ordem</code>' do tipo tpORDEM
	 */
	//private TipoOrdemDAO ordem = TipoOrdemDAO.ASC;

	/**
	 * Atributo '<code>pstmt</code>' do tipo PreparedStatement
	 */
	protected PreparedStatement pstmt = null;

	/**
	 * Atributo '<code>indicePaginacao</code>' do tipo int
	 */
	private Integer indicePaginacao = null;

	/**
	 * Atributo '<code>tamPagina</code>' do tipo int
	 */
	private Integer tamPagina = null;

	/**
	 * Atributo '<code>isClausulaLike</code>' do tipo boolean
	 */
	private boolean isClausulaLike;

	/**
	 * Atributo '<code>isIgnorarClausulaOrderBy</code>' do tipo boolean
	 */
	protected boolean isIgnorarClausulaOrderBy;

	private DtoBuilder<?> builder;

	/**
	 * Construtor para esta classe.
	 * @param builder 
	 * 
	 */
	protected ParametroFactory(DtoBuilder<?> builder) {
		this.builder = builder;
		this.sql = new StringBuffer();
		this.parametros = new Stack<Object>();
		this.clausulaWhere = new Stack<String>();
		this.colunasParam = new Stack<String>();
		this.clausulaOrderBy = new LinkedList<String>();
		this.operadoresDAO = new Stack<TipoOperadorDAO>();
		this.operadoresSQL = new Stack<TipoOperadorSQL>();

		this.isClausulaLike = true;

	}
	
	/**
	 * @return the builder
	 */
	@SuppressWarnings("rawtypes")
	public DtoBuilder getBuilder() {
		return builder;
	}

	/**
	 * Retorna uma inst�ncia do singleton ParametroFactory.
	 * 
	 * @return a inst�ncia de ParametroFactory.
	 */
	public static ParametroFactory getInstance(DtoBuilder<?> builder) throws GrowUpDAOException {

		try {
			switch (GlobalStartup.getInstance().getActiveDAOFactory()) {
			case FireBird:
				return new FbirdSkipParametroFactory(builder);
			case SqlServer:
				return new SqlSrvParametroFactory(builder);
			default:
				return null;
			}

		} catch (Exception e) {
			throw new GrowUpDAOException(ErroInterno.ERRO_TRANSACIONAL, e.getMessage());
		}

	}

	/**
	 * M�todo que seta a coluna padr�o que ser� ordenada.
	 * 
	 * @param col
	 *            Nome da coluna para ordenar
	 * @param ordem
	 *            Tipo da ordem
	 */
	public void setOrdem(final String col, final TipoOrdemDAO ordem) {
		//this.ordem = ordem;
		this.clausulaOrderBy.offer(col.concat(" ").concat(ordem.toString()));
	}

	/**
	 * M�todo utilizado para configurar o valor inicial da SQL.
	 * 
	 * @param value
	 *            Conte�do a ser configurado para a SQl.
	 */
	public void setQuery(final StringBuffer value, boolean isIgnorarClausulaOrderBy) {
		this.sql = value;
		this.isIgnorarClausulaOrderBy = isIgnorarClausulaOrderBy;
	}

	/**
	 * M�todo utilizado para configurar o valor inicial da SQL.
	 * 
	 * @param value
	 *            Conte�do a ser configurado para a SQl.
	 */
	public void setQuery(final StringBuffer value) {
		this.sql = value;
		this.isIgnorarClausulaOrderBy = false;
	}

	/**
	 * Retorna a query designada na camada DAO
	 * 
	 * @return Retorna um objeto do tipo StringBuffer
	 */
	public final StringBuffer getQuery() {
		return this.sql;
	}

	/**
	 * M�todo que retorna se de acordo com os parametros empilhados s�o
	 * satisfat�rios para executar a clausula like.
	 * 
	 * @return Retorna um primitivo do tipo boolean.
	 */
	public final boolean isClausulaLike() {
		return this.isClausulaLike;
	}

	/**
	 * L�gica completa da passagem de parametros
	 * 
	 * @param colunaOuQry
	 * @param operadorDAO
	 * @param values
	 *            Comentar aqui.
	 */
	protected void setParam(final Object colunaOuQry, final TipoOperadorDAO operadorDAO,
			final TipoOperadorSQL operadorSQL, final Object... values) {
		if (values.length > 0) {

			if (colunaOuQry instanceof StringBuffer) {
				this.clausulaWhere.push(colunaOuQry.toString());
			} else {
				this.clausulaWhere.push(colunaOuQry.toString().concat(" ").concat(
						operadorDAO.toString()).concat(" ? "));
				int idxDot = colunaOuQry.toString().indexOf(".");
				if (idxDot > 0) {
					this.colunasParam.push(colunaOuQry.toString().substring(idxDot + 1,
							colunaOuQry.toString().length()));
				} else {
					this.colunasParam.push(colunaOuQry.toString());
				}
			}

			this.operadoresDAO.push(operadorDAO);
			this.operadoresSQL.push(operadorSQL);
			for (int i = 0; i < values.length; i++) {
				this.parametros.push(values[i]);

				if (!(values[i] instanceof String)) {
					this.isClausulaLike = false;
				}
			}

		}
	}

	/**
	 * M�todo utilizado para configurar o conte�do da coluna usada nas cl�usulas
	 * do 'select'.
	 * 
	 * @param col
	 *            Coluna a ser configurada.
	 * @param value
	 *            Conte�do da coluna da cl�usula usada no 'select'.<br>
	 *            O par�metro <code>value</code> deve ser configurado em
	 *            uppercase para cl�usulas like. Para isso basta atribuir no
	 *            setParam da classe OracleXXXXDAO .<br>
	 *            Ver exemplo de OracleServidorDAO.setParam()
	 */
	public void setParam(final String col, final Object value) {
		this.setParam(col, value, TipoOperadorDAO.IGUAL, TipoOperadorSQL.AND);
	}

	/**
	 * M�todo utilizado para configurar o conte�do da coluna usada nas cl�usulas
	 * do 'select' e dizer a ordem em que o select.
	 * 
	 * @param col
	 *            Coluna a ser configurada.
	 * @param value
	 *            Conte�do da coluna da cl�usula usada no 'select'.
	 * @param ordem
	 *            Ordem que o resultado do 'select' deve seguir: asc ou desc.
	 */
	public void setParam(final String col, final Object value, final TipoOperadorDAO operadorDAO,
			final TipoOperadorSQL operadorSQL) {
		if (value != null) {
			this.setParam(col, operadorDAO, operadorSQL, value);
		}
	}

	/**
	 * M�todo utilizado para configurar o conte�do da coluna usada nas cl�usulas
	 * do 'select' e dizer a ordem em que o select.
	 * 
	 * @param col
	 *            Coluna a ser configurada.
	 * @param value
	 *            Conte�do da coluna da cl�usula usada no 'select'.
	 * @param ordem
	 *            Ordem que o resultado do 'select' deve seguir: asc ou desc.
	 */
	public void setParam(final String col, final TipoOperadorDAO operadorDAO, final Object value) {
		if (value != null) {
			this.setParam(col, operadorDAO, TipoOperadorSQL.AND, value);
		}
	}

	/**
	 * M�todo utilizado para configurar o conte�do da coluna usada nas cl�usulas
	 * do 'select' e dizer a ordem em que o select.
	 * 
	 * @param col
	 *            Coluna a ser configurada.
	 * @param value
	 *            Conte�do da coluna da cl�usula usada no 'select'.
	 * @param ordem
	 *            Ordem que o resultado do 'select' deve seguir: asc ou desc.
	 */
	public void setParam(final String col, final TipoOperadorSQL operadorSQL, final Object value) {
		if (value != null) {
			this.setParam(col, TipoOperadorDAO.IGUAL, operadorSQL, value);
		}
	}

	/**
	 * M�todo que empilha os conteudo dos parametros indenpendente da query.
	 * 
	 * @param value
	 *            Valor a ser empilhado
	 * @param operador
	 *            Operador Sql
	 */
	public void setParam(final StringBuffer qry, final TipoOperadorSQL operadorSQL,
			final Object... values) {
		this.setParam((Object) qry, TipoOperadorDAO.IGUAL, operadorSQL, values);
	}

	/**
	 * M�todo que empilha os conteudo dos parametros indenpendente da query.
	 * 
	 * @param value
	 *            Valor a ser empilhado
	 */
	public void setParam(final StringBuffer qry, final Object... values) {
		this.setParam(qry, TipoOperadorSQL.AND, values);
	}

	/**
	 * M�todo que armazena as colunas e tabelas necess�rias para o join
	 * 
	 * @param tabelapontoColuna
	 *            Nome da tabela destino que ser� feito o join
	 * @param colunaDestino
	 *            Nome da coluna de destino para construir o filtro
	 * @param operador
	 *            tipo de operador
	 * @param value
	 *            parametro utilizado para o filtro.
	 * @deprecated
	 */
	@Deprecated
	public void setParam1(final String tabelapontoColuna, final String colunaDestino,
			final TipoOperadorSQL operador, final Object value) {
		if (value != null) {
			if (this.clausulaFrom == null) {
				this.clausulaFrom = new Stack<String>();
				this.operadoresSQL = new Stack<TipoOperadorSQL>();
			}
			this.clausulaFrom.push(tabelapontoColuna);
			this.operadoresSQL.push(operador);
			this.setParam(colunaDestino, value);
		}
	}

	/**
	 * M�todo que insere a qry de pagina��o.
	 * 
	 * @param sb
	 *            Par�metro do tipo StringBuffer
	 */
	protected abstract void setQryPaginacao(final StringBuffer sb);

	/**
	 * M�todo set a indice inicial da pagina��o
	 * 
	 * @param indiceInicial
	 *            Comentar aqui.
	 */
	public void setPaginacao(final Integer indiceInicial, final Integer tamPagina) {
		this.indicePaginacao = indiceInicial;
		this.tamPagina = tamPagina;
	}

	/**
	 * M�todo concatena na qry os joins
	 * 
	 * @param sb
	 *            Parametro so tipo stringBuffer
	 */
	private void setClausulaFrom(final StringBuffer sb) {
		while ((this.clausulaFrom != null) && (!this.clausulaFrom.isEmpty())) {
			String tabelaOrigem = this.getNomeTabelaOrigem();
			String[] temp = this.clausulaFrom.pop().split("\\.");
			String tabelaDestino = temp[0];
			String colunaJoin = temp[1];

			sb.append("\n");
			sb.append(this.operadoresSQL.pop().toString());
			sb.append(" ".concat(tabelaDestino));
			sb.append("\n ON ");
			sb.append(tabelaOrigem.concat(".").concat(colunaJoin));
			sb.append(" = ");
			sb.append(tabelaDestino.concat(".").concat(colunaJoin));
		}
	}

	/**
	 * Configura os operadoresSQL da pilha operadores
	 * 
	 * @return
	 */
	private String getOperadorSQL() {
		return " ".concat(this.operadoresSQL.pop().toString()).concat(" ")
				+ this.clausulaWhere.pop();
	}

	/**
	 * Configura o operadoresSQL passado como parametro.
	 * 
	 * @param operadorSQL
	 * @return
	 */
	private String getOperadorSQL(final TipoOperadorSQL operadorSQL) {
		return " ".concat(operadorSQL.toString()).concat(" ") + this.clausulaWhere.pop();
	}

	// /**
	// * Configura a cl�usula 'AND' do SQl din�mico.
	// *
	// * @return a cl�usula 'AND'.
	// */
	// private String getOperadorAND() {
	// return " AND " + this.clausulaWhere.pop();
	// }
	//
	// /**
	// * Configura a cl�usula 'WHERE' do SQl din�mico.
	// *
	// * @return a cl�usula 'WHERE'.
	// */
	// private String getOperadorWHERE() {
	// return " WHERE " + this.clausulaWhere.pop();
	// }

	/**
	 * Configura a cl�usula 'WHERE' do SQl din�mico usando 'LIKE'.
	 * 
	 * @return A cl�sula 'WHERE' utilizando 'LIKE'.
	 */
	private String getOperadorLike(final TipoOperadorSQL operadorSQL) {
		String str = this.clausulaWhere.pop().replace(
				this.operadoresDAO.pop().toString(), "  LIKE ");
		return " ".concat(operadorSQL.toString()).concat(" ").concat(str);
	}

	/**
	 * Configura o operador 'AND' no SQl din�mico e utiliza tamb�m a cl�sula
	 * 'LIKE'.
	 * 
	 * @return A cl�usula 'AND' usando 'LIKE'.
	 */
	private String getOperadorLIKE() {
		String str = this.clausulaWhere.pop().replace(this.operadoresDAO.pop().toString(), " LIKE ");
		return " ".concat(operadoresSQL.pop().toString()).concat(" ").concat(str);
	}

	/**
	 * Configura o operador ',' no SQl din�mico..
	 * 
	 * @return A string com o operador.
	 */
	private String getOperadorVirgula() {
		return " , " + this.clausulaOrderBy.poll();
	}

	/**
	 * Configura a cl�sula 'ORDER BY' no SQl din�mico..
	 * 
	 * @return A cl�usula 'ORDER BY'.
	 */
	private String getOperadorORDERBY() {
		return " ORDER BY " + this.clausulaOrderBy.poll();
	}

	/**
	 * M�todo utilizado para obter um SQL montado din�micamente utilizando a
	 * cl�sula 'WHERE' e 'LIKE'.
	 * 
	 * @param isClausulaLike
	 *            <code>true</code> se for utilizar o operador Like,
	 *            <code>false</code> para utilizar cl�sula where sem o
	 *            operador Like.
	 * @return A String com o SQL montado din�micamente.
	 * @deprecated
	 */
	@Deprecated
	public String getLikeSQL(final boolean isClausulaLike) {
		boolean isfirstTime = true;
		StringBuffer qry = new StringBuffer(this.sql.toString());

		this.setClausulaFrom(qry);

		while (!this.clausulaWhere.isEmpty()) {
			if (isfirstTime) {
				if (isClausulaLike) {
					qry.append(this.getOperadorLike(TipoOperadorSQL.WHERE));
				} else {
					qry.append(this.getOperadorSQL(TipoOperadorSQL.WHERE));
				}
				isfirstTime = false;
			} else {
				if (isClausulaLike) {
					qry.append(this.getOperadorLIKE());
				} else {
					qry.append(this.getOperadorSQL());
				}
			}
		}

		this.setQryPaginacao(qry);

		return qry.toString();

	}

	/**
	 * M�todo utilizado para obter um SQL montado din�micamente utilizando a
	 * cl�sula 'WHERE' e 'LIKE'.
	 * 
	 * @return A String com o SQL montado din�micamente.
	 */
	private String getLikeSQL() {
		// return this.getLikeSQL(true);
		boolean isfirstTime = true;
		StringBuffer qry = new StringBuffer(this.sql.toString());

		this.setClausulaFrom(qry);

		while (!this.clausulaWhere.isEmpty()) {
			if (isfirstTime) {
				qry.append(this.getOperadorLike(TipoOperadorSQL.WHERE));
				isfirstTime = false;
			} else {
				qry.append(this.getOperadorLIKE());
			}
		}

		this.setQryPaginacao(qry);

		return qry.toString();
	}

	/**
	 * M�todo utilizado para recuperar o SQL din�mico.
	 * 
	 * @return Uma String com o SQL montado din�micamente.
	 */
	private String getSQL() {
		boolean isfirstTime = true;
		StringBuffer qry = new StringBuffer(this.sql.toString());

		this.setClausulaFrom(qry);

		while (!this.clausulaWhere.isEmpty()) {
			if (isfirstTime) {
				qry.append(this.getOperadorSQL(TipoOperadorSQL.WHERE));
				isfirstTime = false;
			} else {
				qry.append(this.getOperadorSQL());
			}
		}

		this.setQryPaginacao(qry);

		return qry.toString();
	}

	/**
	 * M�todo que coloca a clausula ORDER BY dos campos que foram setados.
	 * 
	 * @return Retorna o objeto recebido como parametro com a cl�usula orderby
	 */
	protected StringBuffer getClausulaOrdem() {
		boolean isfirstTime = true;
		StringBuffer lqry = new StringBuffer();

		while (!this.clausulaOrderBy.isEmpty()) {
			if (isfirstTime) {
				lqry.append(this.getOperadorORDERBY());
				isfirstTime = false;
			} else {
				lqry.append(this.getOperadorVirgula());
			}
		}

//		if (!isfirstTime) {
//			lqry.append(" " + this.ordem.toString());
//		}

		return lqry;
	}

	/**
	 * M�todo que retorna os parametros para o metodo selectAll da classe
	 * OracleDAO
	 * 
	 * @return Retorna a pilha com os parametros do select
	 */
	public final Stack<Object> getParametros() {
		return this.parametros;
	}

	/**
	 * M�todo que retorna indice inicial da paginacao *
	 * 
	 * @return Retorna um objeto do tipo Integer
	 */
	public final Integer getIndicePaginacao() {
		return this.indicePaginacao;
	}

	/**
	 * M�todo que retorna o tamanho da p�gina.
	 * 
	 * @return Retorna um objeto do tipo Integer
	 */
	public final Integer getPagina() {
		return this.tamPagina;
	}

	/**
	 * M�todo que retorna o nome da tabela principal
	 * 
	 * @return Retorna um objeto do tipo String
	 */
	public final String getNomeTabelaOrigem() {
		return this.sql.substring(
				this.sql.toString().toUpperCase().lastIndexOf("FROM") + "FROM".length()).trim();
	}

	/**
	 * M�todo que retorna o nome da chave prim�ria da query especificada na
	 * camada acesso a dados. OBS: Deve ser a primeira coluna da query.
	 * 
	 * @return Retorna um objeto do tipo String
	 */
	public final String getNomeChavePrimaria() {
		return this.sql.substring(
				this.sql.toString().toUpperCase().lastIndexOf("SELECT") + "SELECT".length(),
				this.sql.toString().indexOf(",")).trim();
	}

	/**
	 * Configura os par�metros da pagina��o.
	 * 
	 * @param i
	 *            Valor da coluna na tabela.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum problema durante a opera��o.
	 */
	private void empilharParametrosPaginacao(int i) throws GrowUpDAOException {
		if (this.indicePaginacao != Constantes.DESCONSIDERAR_PAGINACAO) {
			try {
				// OBS SQL Server n�o pode empilhar os parametros por causa que
				// o parametro � no TOP do select.
				// this.pstmt.setInt(i++, this.tamPagina);
				// this.pstmt.setInt(i++, this.indicePaginacao);
			} catch (Exception e) {
				throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
						Constantes.ORA_CODE), e.getMessage());
			}
		}

	}

	/**
	 * Empilha os parametros passados e coloca no Preparedstatement
	 * 
	 * @return Retorna o objeto passado como parametro do tipo PreparedStatement
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro durante a pesquisa.
	 */
	private PreparedStatement empilharParametros() throws GrowUpDAOException {

		try {
			int tam = this.getParametros().size();
			int i = 1;
			for (i = 1; i <= tam; i++) {
				Object obj = this.getParametros().pop();
				if (obj instanceof Integer) {
					this.pstmt.setInt(i, ((Integer) obj).intValue());
					continue;
				} else if (obj instanceof String) {
					this.pstmt.setString(i, ((String) obj).toString());
					continue;
				} else if (obj instanceof Enum<?>) {
					this.pstmt.setString(i, obj.toString());
					continue;
				} else if (obj instanceof Long) {
					this.pstmt.setLong(i, ((Long) obj).longValue());
					continue;
				} else if (obj instanceof Short) {
					this.pstmt.setShort(i, ((Short) obj).shortValue());
					continue;
				} else if (obj instanceof Boolean) {
					this.pstmt.setBoolean(i, ((Boolean) obj).booleanValue());
					continue;
				} else if (obj instanceof Date) {
					this.pstmt.setTimestamp(i, DateUtil.getDateManagerInstance(((Date) obj))
							.getSQLTimeStamp());
					continue;
				} else {
					throw new GrowUpDAOException(ErroInterno.OBJETO_NAO_RECONHECIDO);
				}
			}

			this.empilharParametrosPaginacao(i);
		} catch (Exception e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		}

		return this.pstmt;
	}

	/**
	 * Empilha os parametros passados e coloca no Preparedstatement
	 * 
	 * @return Retorna o objeto passado como parametro do tipo PreparedStatement
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro durante a pesquisa.
	 */
	private PreparedStatement empilharParametrosLike() throws GrowUpDAOException {
		try {
			int tam = this.getParametros().size();
			int i = 1;
			for (i = 1; i <= tam; i++) {
				Object obj = this.getParametros().pop();

				String param = String.valueOf(obj);
				
				/* Altera��o ainda sem avaliacao geral do sistema. 
				 * Esta solu��o n�o foi poss�vel por causa deste erro:
				 * "arithmetic exception, numeric overflow, or string truncatiom string right truncation"
				 * Para campos de tamanho fixos pequeno o firebird retorna este erro.
				 * 
				if (param.trim().endsWith("%") || param.trim().startsWith("%"))
					this.pstmt.setString(i, param);
				else
					this.pstmt.setString(i, "%".concat(param).concat("%"));
				 */
				this.pstmt.setString(i, param);

			}

			this.empilharParametrosPaginacao(i);
		} catch (Exception e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		}

		return this.pstmt;
	}

	/**
	 * Retorna o valor do parametro com os percentuais de acordo com o tamando
	 * da coluna. Tem uma restri��o se o tamanho do campo for maior que o
	 * parametro concatenado com 2 % dispara DataTruncation Exception o tamanho
	 * do campo pode ser adquirito em:
	 * this.pstmt.stmt.fields[i].possibleCharLength
	 * 
	 * @param idxParam
	 * @param param
	 * @return
	 * @throws Exception
	 * @Deprecated
	 */
	// private final String getParamLikeStatement(final int idxParam, final
	// String param) throws Exception {
	// String result = null;
	// int columnSize = 1;
	//		
	// String columnParam = this.colunasParam.pop().trim();
	//
	// if (columnParam.indexOf(" ") > -1) {
	// columnParam = columnParam.substring(0, columnParam.indexOf(" "));
	// }
	//		
	// ResultSetMetaData metaParam = this.pstmt.getMetaData();
	// for (int idx=1; idx <= metaParam.getColumnCount(); idx++) {
	// if (metaParam.getColumnName(idx).equalsIgnoreCase(columnParam)) {
	// columnSize = metaParam.getColumnDisplaySize(idx);
	// break;
	// }
	// }
	//		
	// if (param.length() == columnSize) {
	// result = param;
	// } else if (columnSize - param.length() == 1) {
	// result = "%".concat(param);
	// } else if (columnSize - param.length() >= 2) {
	// result = "%".concat(param).concat("%");
	// } else {
	// result = "%".concat(param).substring(0, columnSize);
	// }
	//		
	// return result;
	// }
	/**
	 * Metodo retorno o prepared statement j� preenchido com os parametros
	 * 
	 * @param conn
	 *            inst�ncia de conex�o.
	 * @return Retorna o objeto passado como parametro do tipo PreparedStatement
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro durante a pesquisa.
	 */
	public final PreparedStatement getStatement(final Connection conn) throws GrowUpDAOException {

		try {
			this.pstmt = conn.prepareStatement(this.getSQL());
			return this.empilharParametros();

		} catch (SQLException e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		}
	}

	/**
	 * Metodo retorno o prepared statement j� preenchido com os parametros
	 * 
	 * @param conn
	 *            inst�ncia de conex�o.
	 * @return Retorna o objeto passado como parametro do tipo PreparedStatement
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro durante a pesquisa.
	 */
	public final PreparedStatement getLikeStatement(final Connection conn)
			throws GrowUpDAOException {

		try {
			this.pstmt = conn.prepareStatement(this.getLikeSQL());
			return this.empilharParametrosLike();
		} catch (SQLException e) {
			throw new GrowUpDAOException(StringUtil.extractString(e.getMessage(), "ORA-",
					Constantes.ORA_CODE), e.getMessage());
		}
	}

}
