package br.com.growupge.factory;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.interfacesdao.DtoBuilder;

/**
 * Classe utilit�ria para a manipula��o do SQL din�mico.
 * 
 * @author Felipe Pina
 * 
 */
public final class FbirdSkipParametroFactory extends ParametroFactory {

	/**
	 * Construtor para esta classe.
	 * @param builder 
	 * 
	 */
	public FbirdSkipParametroFactory(DtoBuilder<?> builder) {
		super(builder);
	}

	/**
	 * M�todo que insere a qry de pagina��o.
	 * 
	 * @param sb
	 *            Par�metro do tipo StringBuffer
	 */
	@Override
	protected void setQryPaginacao(final StringBuffer sb) {
		StringBuffer clausulaOrderBy = this.getClausulaOrdem();

		if ((sb != null) && (this.getIndicePaginacao() != Constantes.DESCONSIDERAR_PAGINACAO)) {
			StringBuffer qryPaginacao = new StringBuffer();

			qryPaginacao.append(" first ");
			qryPaginacao.append(this.getPagina());
			qryPaginacao.append(" skip ");
			qryPaginacao.append(this.getPagina() * this.getIndicePaginacao());
			sb.insert(sb.toString().toUpperCase().indexOf("SELECT") + "SELECT".length(),
					qryPaginacao);

		}

		if (!isIgnorarClausulaOrderBy) {
			sb.append(clausulaOrderBy);
		}

	}

}
