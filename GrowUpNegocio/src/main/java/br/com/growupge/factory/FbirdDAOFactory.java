package br.com.growupge.factory;



import br.com.growupge.dto.AnoModeloDTO;
import br.com.growupge.dto.ArquivoDTO;
import br.com.growupge.dto.CargoDTO;
import br.com.growupge.dto.ClienteDTO;
import br.com.growupge.dto.DepartamentoDTO;
import br.com.growupge.dto.EmpresaDTO;
import br.com.growupge.dto.EmpresaItemDTO;
import br.com.growupge.dto.MarcaDTO;
import br.com.growupge.dto.MensagemDTO;
import br.com.growupge.dto.ModeloDTO;
import br.com.growupge.dto.PerfilDTO;
import br.com.growupge.dto.PerfilPermissaoItemDTO;
import br.com.growupge.dto.PermissaoDTO;
import br.com.growupge.dto.PessoaDTO;
import br.com.growupge.dto.ProprietarioDTO;
import br.com.growupge.dto.ServicoDTO;
import br.com.growupge.dto.UnidadeFederativaDTO;
import br.com.growupge.dto.VariavelDTO;
import br.com.growupge.dto.VeiculoDTO;
import br.com.growupge.dto.VeiculoProprietarioDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.fbirdDAO.FbirdAnoModeloDAO;
import br.com.growupge.fbirdDAO.FbirdArquivoDAO;
import br.com.growupge.fbirdDAO.FbirdClienteDAO;
import br.com.growupge.fbirdDAO.FbirdEmpresaClienteDAO;
import br.com.growupge.fbirdDAO.FbirdEmpresaDAO;
import br.com.growupge.fbirdDAO.FbirdMarcaDAO;
import br.com.growupge.fbirdDAO.FbirdMensagemDAO;
import br.com.growupge.fbirdDAO.FbirdModeloDAO;
import br.com.growupge.fbirdDAO.FbirdPagamentoDAO;
import br.com.growupge.fbirdDAO.FbirdPedidoVeiculoDAO;
import br.com.growupge.fbirdDAO.FbirdPerfilDAO;
import br.com.growupge.fbirdDAO.FbirdPerfilPermissaoDAO;
import br.com.growupge.fbirdDAO.FbirdPermissaoDAO;
import br.com.growupge.fbirdDAO.FbirdProprietarioDAO;
import br.com.growupge.fbirdDAO.FbirdRestricaoDAO;
import br.com.growupge.fbirdDAO.FbirdSequenciaDAO;
import br.com.growupge.fbirdDAO.FbirdServicoDAO;
import br.com.growupge.fbirdDAO.FbirdSessaoDAO;
import br.com.growupge.fbirdDAO.FbirdTramitacaoDAO;
import br.com.growupge.fbirdDAO.FbirdUfDAO;
import br.com.growupge.fbirdDAO.FbirdUsuarioDAO;
import br.com.growupge.fbirdDAO.FbirdVariavelDAO;
import br.com.growupge.fbirdDAO.FbirdVeiculoDAO;
import br.com.growupge.fbirdDAO.FbirdVeiculoProprietarioDAO;
import br.com.growupge.interfacesdao.ConsultaDAO;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.interfacesdao.PagamentoDAO;
import br.com.growupge.interfacesdao.PedidoVeiculoDAO;
import br.com.growupge.interfacesdao.PerfilDAO;
import br.com.growupge.interfacesdao.RestricaoDAO;
import br.com.growupge.interfacesdao.SequenciaDAO;
import br.com.growupge.interfacesdao.SessaoDAO;
import br.com.growupge.interfacesdao.TramitacaoDAO;
import br.com.growupge.interfacesdao.UsuarioDAO;
import br.com.growupge.sqlsrvDAO.SqlSrvCargoDAO;
import br.com.growupge.sqlsrvDAO.SqlSrvDepartamentoDAO;
import br.com.growupge.sqlsrvDAO.SqlSrvPessoaDAO;

/**
 * F�brica DAO para Banco de Dados Oracle.
 * 
 * @author eii5
 * 
 */
// @javax.enterprise.inject.Default //Marcando que � a fabrica padrao para todos os pontos de inje��o
@br.com.growupge.stereotype.FirebirdStereoType
public class FbirdDAOFactory extends DAOFactory {

	/**
	 * Construtor para esta classe. Inicializa uma transa��o isolada para esta
	 * f�brica
	 * 
	 * @throws GrowUpException
	 *             Caso ocorra algum erro que n�o seja de banco.
	 */
	public FbirdDAOFactory() throws GrowUpException {
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getUsuarioDAO()
	 */
	@Override
	public final UsuarioDAO getUsuarioDAO()  {
		return new FbirdUsuarioDAO(this);
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getSessaoDAO()
	 */
	@Override
	public final SessaoDAO getSessaoDAO()  {
		return new FbirdSessaoDAO(this);
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getEmpresaDAO()
	 */
	@Override
	public final DAO<EmpresaDTO> getEmpresaDAO() {
		return new FbirdEmpresaDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getEmpresaItemDAO()
	 */
	@Override
	public DAO<EmpresaItemDTO> getEmpresaItemDAO()  {
		return new FbirdEmpresaClienteDAO(this);
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getPermissaoDAO()
	 */
	@Override
	public final DAO<PermissaoDTO> getPermissaoDAO()  {
		return new FbirdPermissaoDAO(this);
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getPerfilDAO()
	 */
	@Override
	public final DAO<PessoaDTO> getPessoaDAO()  {
		return new SqlSrvPessoaDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getMensagemDAO()
	 */
	@Override
	public DAO<MensagemDTO> getMensagemDAO()  {
		return new FbirdMensagemDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getClienteDAO()
	 */
	@Override
	public DAO<ClienteDTO> getClienteDAO() {
		return new FbirdClienteDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getPedidoVeiculoDAO()
	 */
	@Override
	public PedidoVeiculoDAO getPedidoVeiculoDAO()  {
		return new FbirdPedidoVeiculoDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getPerfilPermissaoDAO()
	 */
	@Override
	public DAO<PerfilPermissaoItemDTO> getPerfilPermissaoDAO()  {
		return new FbirdPerfilPermissaoDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getPerfilDAO()
	 */
	@Override
	public PerfilDAO<PerfilDTO> getPerfilDAO()  {
		return new FbirdPerfilDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getAreaDAO()
	 */
	@Override
	public DAO<DepartamentoDTO> getDepartamentoDAO()  {
		return new SqlSrvDepartamentoDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getCargoDAO()
	 */
	@Override
	public DAO<CargoDTO> getCargoDAO()  {
		return new SqlSrvCargoDAO(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.factory.DAOFactory#getCargoDAO()
	 */
	@Override
	public SequenciaDAO getSequenciaDAO()  {
		return new FbirdSequenciaDAO(this);
	}
	
	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.factory.DAOFactory#getMarcaDAO()
	 */
	@Override
	public DAO<MarcaDTO> getMarcaDAO()  {
		return new FbirdMarcaDAO(this);
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.factory.DAOFactory#getModeloDAO()
	 */
	@Override
	public DAO<ModeloDTO> getModeloDAO()  {
		return new FbirdModeloDAO(this);
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.factory.DAOFactory#getAnoModeloDAO()
	 */
	@Override
	public DAO<AnoModeloDTO> getAnoModeloDAO()  {
		return new FbirdAnoModeloDAO(this);
	}
	
	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.factory.DAOFactory#getModeloDAO()
	 */
	@Override
	public DAO<ProprietarioDTO> getProprietarioDAO()  {
		return new FbirdProprietarioDAO(this);
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.factory.DAOFactory#getModeloDAO()
	 */
	@Override
	public DAO<VeiculoDTO> getVeiculoDAO()  {
		return new FbirdVeiculoDAO(this);
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.factory.DAOFactory#getModeloDAO()
	 */
	@Override
	public DAO<VeiculoProprietarioDTO> getVeiculoProprietarioDAO()  {
		return new FbirdVeiculoProprietarioDAO(this);
	}
	
	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.factory.DAOFactory#getModeloDAO()
	 */
	@Override
	public DAO<ServicoDTO> getServicoDAO()  {
		return new FbirdServicoDAO(this);
	}
	
	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.factory.DAOFactory#getModeloDAO()
	 */
	@Override
	public TramitacaoDAO getTramitacaoDAO()  {
		return new FbirdTramitacaoDAO(this);
	}
	
	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.factory.DAOFactory#getModeloDAO()
	 */
	@Override
	public ConsultaDAO<UnidadeFederativaDTO> getUfDAO()  {
		return new FbirdUfDAO(this);
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.factory.DAOFactory#getModeloDAO()
	 */
	@Override
	public DAO<ArquivoDTO> getArquivoDAO()  {
		return new FbirdArquivoDAO(this);
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.factory.DAOFactory#getRestricaoDAO()
	 */
	@Override
	public RestricaoDAO getRestricaoDAO()  {
		return new FbirdRestricaoDAO(this);
	}

	@Override
	public DAO<VariavelDTO> getVariavelDAO()  {
		return new FbirdVariavelDAO(this);
	}

	@Override
	public PagamentoDAO getPagamentoDAO()  {
		return new FbirdPagamentoDAO(this);
	}
	

}