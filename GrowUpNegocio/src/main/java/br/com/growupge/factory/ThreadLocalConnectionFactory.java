package br.com.growupge.factory;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.utility.StringUtil;

/**
 * @author Felipe
 *
 */
@Deprecated // N�o mais utilizada no sistema
class ThreadLocalConnectionFactory implements Transactional {
	private final static ThreadLocal<ThreadLocalConnectionFactory> cache = new ThreadLocal<ThreadLocalConnectionFactory>();
	private Connection conn = null;
	
	/**
	 * Retorna a conex�o com o BD.
	 * 
	 * @return Um objeto do tipo Connection.
	 */
	private final Connection createConnection() throws GrowUpDAOException {
		try {
			InitialContext ctxt = new InitialContext();
			Context ct = (Context) ctxt.lookup("java:/comp/env");

			String dataSourceName = Constantes.ACTIVE_DATA_SOURCE;
			DataSource ds = (DataSource) ct.lookup(dataSourceName);

			if (ds == null) {
				throw new GrowUpDAOException("IE-XXXX", "Cannot create database connection");
			}

			// DataSource ds = (DataSource)ct.lookup("jdbc/FirebirdDS");
			// DataSource ds = (DataSource)ct.lookup("jdbc/SqlsrvDS");
			conn = ds.getConnection();
			ThreadLocalConnectionFactory.cache.set( this );
			
			getConnection().setAutoCommit(false);

		} catch (GrowUpDAOException e) {
			throw e;
		} catch (Exception ex) {
			throw new GrowUpDAOException(ErroInterno.COMUNICACAO_COMPONENTE, ex.getMessage());
		}
		return getConnection();
	}
	
	/**
	 * Retorna a conex�o com o BD, tendo a possibilidade de n�o recriar
	 * conex�es.
	 * 
	 * @return Um objeto do tipo Connection.
	 */
	@Override
	public final Connection getConnection() throws GrowUpDAOException {
		return cache.get() != null ? cache.get().conn : this.createConnection();
	}
	
	/**
	 * @return
	 */
	private boolean isOwner() {
		return ThreadLocalConnectionFactory.this.equals(cache.get());
	}
	
	/**
	 * Fecha a conex�o com o BD.
	 * 
	 * @throws Exception
	 *             Caso ocorra algum erro durante a opera��o.
	 */
	@Override
	public final void closeCommit() throws GrowUpDAOException {
		if (isOwner()) {
			try {
				if (!getConnection().isClosed()) {
					if (!getConnection().getAutoCommit()) {
						getConnection().commit();
					}
				}
			} catch (SQLException e) {
				dispararError(e);
			} finally {
				close();
			}
		}
	}
	
	/**
	 * @throws GrowUpDAOException
	 */
	public final void close() throws GrowUpDAOException {
		try {
			getConnection().close();
		} catch (SQLException e) {
			dispararError(e);
		} finally {
			ThreadLocalConnectionFactory.cache.set( null );
		}
	}

	/**
	 * @throws Exception
	 */
	@Override
	public final void rollBack() throws GrowUpDAOException {
		if (isOwner()) {
			try {
				if (getConnection().getTransactionIsolation() != Connection.TRANSACTION_NONE) {
					getConnection().rollback();
				}
			} catch (SQLException e) {
				dispararError(e);
			} finally {
				close();
			}
		}
	}
	
	/**
	 * Metodo que repassa a SqlException atrav�s da GrowUpDAOException
	 * @param e
	 * @throws GrowUpDAOException
	 */
	protected void dispararError(SQLException e) throws GrowUpDAOException {
		throw new GrowUpDAOException(StringUtil.extractString(
				e.getMessage(),
				"ORA-",
				Constantes.ORA_CODE), e.getMessage());
	}
}
