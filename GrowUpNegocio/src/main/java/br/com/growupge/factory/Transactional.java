package br.com.growupge.factory;


import br.com.growupge.exception.GrowUpDAOException;

public interface Transactional extends Connectable  {

	/**
	 * Fecha a conex�o com o BD.
	 * 
	 * @throws Exception
	 *             Caso ocorra algum erro durante a opera��o.
	 */
	public abstract void closeCommit() throws GrowUpDAOException;

	/**
	 * @throws Exception
	 */
	public abstract void rollBack() throws GrowUpDAOException;

}