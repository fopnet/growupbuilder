/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 28/07/2005
 *
 * Historico de Modifica��o:
 * =========================
 * 28/07/2005 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.factory;

import java.sql.Connection;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;

import br.com.growupge.dto.AnoModeloDTO;
import br.com.growupge.dto.ArquivoDTO;
import br.com.growupge.dto.CargoDTO;
import br.com.growupge.dto.ClienteDTO;
import br.com.growupge.dto.DepartamentoDTO;
import br.com.growupge.dto.EmpresaDTO;
import br.com.growupge.dto.EmpresaItemDTO;
import br.com.growupge.dto.MarcaDTO;
import br.com.growupge.dto.MensagemDTO;
import br.com.growupge.dto.ModeloDTO;
import br.com.growupge.dto.PerfilDTO;
import br.com.growupge.dto.PerfilPermissaoItemDTO;
import br.com.growupge.dto.PermissaoDTO;
import br.com.growupge.dto.PessoaDTO;
import br.com.growupge.dto.ProprietarioDTO;
import br.com.growupge.dto.ServicoDTO;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.dto.UnidadeFederativaDTO;
import br.com.growupge.dto.VariavelDTO;
import br.com.growupge.dto.VeiculoDTO;
import br.com.growupge.dto.VeiculoProprietarioDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.global.GlobalStartup;
import br.com.growupge.interfacesdao.ConsultaDAO;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.interfacesdao.PagamentoDAO;
import br.com.growupge.interfacesdao.PedidoVeiculoDAO;
import br.com.growupge.interfacesdao.PerfilDAO;
import br.com.growupge.interfacesdao.RestricaoDAO;
import br.com.growupge.interfacesdao.SequenciaDAO;
import br.com.growupge.interfacesdao.SessaoDAO;
import br.com.growupge.interfacesdao.TramitacaoDAO;
import br.com.growupge.interfacesdao.UsuarioDAO;

/**
 * F�brica de DAO.
 * 
 * @author Felipe
 * 
 */
public abstract class DAOFactory implements Transactional {

	@javax.inject.Inject
	private ConnectionFactory connectionManager;
	
	protected SessaoDTO sessao;
	
	protected DAOFactory() {
//		this.connectionManager = new ConnectionManagerFactory();
	}
	
	/**
	 * @param sessao the sessao to set
	 */
	public void setSessao(SessaoDTO sessao) {
		this.sessao = sessao;
	}
	
	/**
	 * @return
	 */
	public SessaoDTO getSessao() {
		return sessao;
	}


	public abstract DAO<EmpresaDTO> getEmpresaDAO() ;

	public abstract DAO<EmpresaItemDTO> getEmpresaItemDAO() ;

	public abstract UsuarioDAO getUsuarioDAO() ;

	public abstract SessaoDAO getSessaoDAO() ;

	public abstract DAO<MensagemDTO> getMensagemDAO() ;

	public abstract DAO<PessoaDTO> getPessoaDAO() ;

	public abstract DAO<PermissaoDTO> getPermissaoDAO() ;

	public abstract PerfilDAO<PerfilDTO> getPerfilDAO() ;

	public abstract DAO<PerfilPermissaoItemDTO> getPerfilPermissaoDAO() ;

	public abstract DAO<ClienteDTO> getClienteDAO() ;

	public abstract PedidoVeiculoDAO getPedidoVeiculoDAO() ;

	public abstract DAO<DepartamentoDTO> getDepartamentoDAO() ;

	public abstract DAO<CargoDTO> getCargoDAO() ;

	public abstract SequenciaDAO getSequenciaDAO() ;

	public abstract DAO<MarcaDTO> getMarcaDAO() ;

	public abstract DAO<ArquivoDTO> getArquivoDAO() ;

	public abstract DAO<ModeloDTO> getModeloDAO() ;
	
	public abstract DAO<AnoModeloDTO> getAnoModeloDAO() ;

	public abstract DAO<ProprietarioDTO> getProprietarioDAO() ;

	public abstract DAO<VeiculoDTO> getVeiculoDAO() ;

	public abstract DAO<VeiculoProprietarioDTO> getVeiculoProprietarioDAO()
			;

	public abstract DAO<ServicoDTO> getServicoDAO() ;

	public abstract TramitacaoDAO getTramitacaoDAO() ;

	public abstract ConsultaDAO<UnidadeFederativaDTO> getUfDAO() ;
	
	public abstract RestricaoDAO getRestricaoDAO() ;
	
	public abstract DAO<VariavelDTO> getVariavelDAO() ;

	public abstract PagamentoDAO getPagamentoDAO() ;

	/**
	 * Retorna a instancia das f�bricas DAO de acordo com a configura��o em
	 * properties.
	 * 
	 * @return f�brica DAO.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro de banco.
	 * @throws GrowUpException
	 *             Exce��o padr�o.
	 */
	@Produces
	@RequestScoped
	static DAOFactory getDAOFactory() throws GrowUpDAOException {
		try {
			switch (GlobalStartup.getInstance().getActiveDAOFactory()) {
			case SqlServer:
				return new SqlSrvDAOFactory();
			case FireBird:
				return new FbirdDAOFactory();
			default:
				return null;
			}

		} catch (Exception e) {
			throw new GrowUpDAOException(ErroInterno.ERRO_TRANSACIONAL, e.getMessage());
		}

	}
	
	/********************************************************************************************
	 * Metodo para controle da conexao
	 ********************************************************************************************/

	/* (non-Javadoc)
	 * @see br.com.growupge.factory.Transactional#getConnection()
	 */
	@Override
	public final Connection getConnection() throws GrowUpDAOException {
		return connectionManager.getConnection();
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.factory.Transactional#closeCommit()
	 */
	@Override
	public final void closeCommit() throws GrowUpDAOException {
//		connectionManager.closeCommit();
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.factory.Transactional#rollBack()
	 */
	@Override
	public final void rollBack() throws GrowUpDAOException {
//		connectionManager.rollBack();
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.factory.Transactional#close()
	 */
	@Override
	public final void close() throws GrowUpDAOException {
//		connectionManager.close(conn);
	}

}
