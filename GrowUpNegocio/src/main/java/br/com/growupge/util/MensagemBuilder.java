package br.com.growupge.util;

import java.util.LinkedHashMap;
import java.util.Map;

import br.com.growupge.business.Mensagem;
import br.com.growupge.dto.MensagemDTO;
import br.com.growupge.dto.MensagemRetornoDTO;
import br.com.growupge.enums.TipoIdioma;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.BeanFactory;

public class MensagemBuilder {

	public static <T extends MensagemRetornoDTO> T build(String codMsg, T dto, Object ...args) {
		dto.codMensagem = codMsg;
		dto.parametros = build(args);
		return dto;
	}

	public static Map<String,String> build(Object ...args) {
		Map<String, String> map = null;
		if (args.length > 0) {
			map = new LinkedHashMap<String, String>();
			for (Integer i = 1; i <= args.length; i++) 
				map.put("$".concat(i.toString()), String.valueOf(args[i-1]));
		} 
		return map;
	}

	/**
	 * M�todo que retorna as mensagem de audit�ria do banco de dados.
	 * 
	 * @param msgcode
	 *            c�digo da mensagem.
	 * @param parametros
	 *            parametros da mensagem
	 * @return objeto dto de mensagem.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	public final static String getMensagem(
			final String msgcode,
			String idioma,
			final Map<String, String> parametros) throws GrowUpException {
		MensagemDTO msg = BeanFactory.getContextualInstance(Mensagem.class).getMensagem(msgcode, parametros);
		
		TipoIdioma enm = TipoIdioma.getEnum(idioma);
	
		switch (enm) {
		case IDIOMA_BR:
			return msg.getDescricaoBR();
		case IDIOMA_EN:
			return msg.getDescricaoEN();
		default:
			return msg.getDescricaoBR();
	
		}
	}

}
