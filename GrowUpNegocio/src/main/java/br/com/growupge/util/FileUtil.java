package br.com.growupge.util;

import java.io.File;

import org.apache.commons.io.FileUtils;

import br.com.growupge.constantes.Constantes;

/**
 * @author Felipe
 *
 */
public class FileUtil {

	public static final String CHARSET_ENCODE = "ISO8859_1";
	
	private static String UPLOAD_FOLDER = "arquivos/";
	
	public static File getUploadFile(String folder) {
		File root = FileUtils.getFile(getWebAppPath(), Constantes.TEMP_FOLDER);
//		String root = Constantes.TEMP_FOLDER;
		File file = FileUtils.getFile(root, UPLOAD_FOLDER, folder);
		return file;
	}

	public static String getWebAppPath() {
		return FileUtil.class.getClassLoader().getResource("../../").getPath();
	}

}
