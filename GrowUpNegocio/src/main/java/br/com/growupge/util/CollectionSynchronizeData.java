package br.com.growupge.util;

import java.util.Collection;

import org.apache.commons.collections4.CollectionUtils;

import br.com.growupge.exception.GrowUpException;

/**
 * @author Felipe
 *
 */
public abstract class CollectionSynchronizeData<T> {
	
	public void synchonize(Collection<T> tela, Collection<T> banco) throws GrowUpException {
		Collection<T> update = CollectionUtils.retainAll(tela, banco);
		update (update);
		
		Collection<T> insert = CollectionUtils.subtract(tela, update);
		insert (insert);
		
		Collection<T> delete = CollectionUtils.subtract(banco, update);
		delete (delete);
	}

	/**
	 * @param comum
	 */
	protected void beforeUpdate(T item) throws GrowUpException{}
	protected abstract void update(T update) throws GrowUpException;
	protected void update(Collection<T> update) throws GrowUpException {
		for (T dto : update) {
			beforeUpdate(dto);
			update(dto);
		}
	}

	protected void beforeInsert(T item) throws GrowUpException{}
	protected abstract void insert(T insert) throws GrowUpException;
	protected void insert(Collection<T> insert) throws GrowUpException {
		for (T dto : insert) {
			beforeInsert(dto);
			insert(dto);
		}
	}
	
	protected void beforeDelete(T item) throws GrowUpException {}
	protected abstract void delete(T insert) throws GrowUpException;
	protected void delete(Collection<T> delete) throws GrowUpException {
		for (T dto : delete) {
			beforeDelete(dto);
			delete(dto);
		}
	}
	
	
}
