/**
 * 
 */
package br.com.growupge.util;

import br.com.growupge.business.PedidoVeiculo;
import br.com.growupge.dto.AnexoDTO;
import br.com.growupge.exception.GrowUpException;


/**
 * @author Felipe
 *
 */
public class CollectionSynchronizeFiles extends CollectionSynchronizeData<AnexoDTO> {

	private PedidoVeiculo bo;

	/**
	 * @param pedidoVeiculo
	 */
	public CollectionSynchronizeFiles(PedidoVeiculo pedidoVeiculoBO) {
		this.bo = pedidoVeiculoBO;
	}

	@Override
	protected void update(AnexoDTO dto) throws GrowUpException {
		// um arquivo nunca � alterado, somente incluso e apagado.
	}

	@Override
	protected void insert(AnexoDTO dto) throws GrowUpException {
		// a interface que submete o array de bytes ao servidor
		// e no caso, os novos dto's j� vem preenchido da interface.
	}

	/** 
	 * A lista de itens a apagar, s�o buscados pelo metodo buscarAnexos
	 * que por sua vez, atribui o pedido
	 */
	@Override
	protected void delete(AnexoDTO dto) throws GrowUpException {
		
		bo.removerAnexo(dto.getPedido(), dto);
	}

}
