/**
 * 
 */
package br.com.growupge.interfacesdao;

import br.com.growupge.exception.GrowUpDAOException;

/**
 * @author Felipe
 * 
 */
public interface SequenciaDAO {

	/**
	 * M�todo utilizado para obter a pr�xima sequ�ncia.
	 * 
	 * @param seqname
	 * @return O n�mero da pr�xima sequ�ncia.
	 * @throws SigemDAOException
	 *             Caso ocorra algum problema durante a opera��o.
	 */
	public long getProximaSequencia(String seqname) throws GrowUpDAOException;
}
