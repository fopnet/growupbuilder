/**
 * 
 */
package br.com.growupge.interfacesdao;

import java.sql.ResultSet;
import java.util.List;

import br.com.growupge.exception.GrowUpDAOException;

/**
 * @author Felipe
 *
 * @param <DTO>
 */
public interface DtoBuilder<DTO> {

	/**
	 * M�todo utilizado para retornar o objeto de transfer�ncia populado com os
	 * dados obtidos do banco.
	 * 
	 * @param rs
	 *            Objeto Resultset do registro em quest�o.
	 * @param isEager TODO
	 * @return O objeto de transfer�ncia populado com os valores retornados do
	 *         banco.
	 * @throws GrowUpDAOException
	 *             Caso ocorra alguma exce��o durante a recupera��o dos dados do
	 *             banco.
	 */
	public abstract DTO getDTO(ResultSet rs, boolean isEager)
			throws GrowUpDAOException;

	/**
	 * Estrategia de adi��o do dto na lista;
	 * 
	 * @param lista
	 * @param dto
	 */
	public void addDto(final List<DTO> lista, final DTO dto);
}