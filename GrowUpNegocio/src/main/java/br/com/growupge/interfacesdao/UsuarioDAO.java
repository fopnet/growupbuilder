/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 28/07/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 28/07/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.interfacesdao;

import java.sql.Connection;

import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.ParametroFactory;

/**
 * 
 * Interface utilizada por todas as classes DAO's do sistema para implementar as
 * funcionalidades de manipula��o dos dados no banco.
 * 
 * @author Felipe
 * 
 * @param <DTO>
 *            Objeto de transfer�ncia que ser� manipulado.
 */
public interface UsuarioDAO extends DAO<UsuarioDTO> {

	/**
	 * M�todo utilizado para atualizar somente a senha do usu�rio
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o com o banco.
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es do registro a ser
	 *            atualizado.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro no Oracle durante a atualiza��o do
	 *             registro.
	 * @return Um transfer object populado com a informa��o do registro que foi
	 *         atualizado.
	 */
	public UsuarioDTO updatePassword(final Connection conn, final UsuarioDTO dto) throws GrowUpDAOException;

	/**
	 * Metodo chamado especificamente para atualizar a data de ultimo acesso do
	 * Usuario no sistema.
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o
	 * @param dto
	 *            Objeto de transfer�ncia com os dados do usu�rio
	 * @return Um objeto de transfer�ncia com os dados atualizados do usu�rio
	 * @throws SigemDAOException
	 *             Caso ocorra algum problema durante a opera��o
	 */
	public UsuarioDTO updateUltimoAcesso(final Connection conn, final UsuarioDTO dto)
			throws GrowUpException;

	/**
	 * Seleciona a proxima pagina a partir do codigo do dto passado como
	 * parametro
	 * 
	 * @param conn
	 * @param dto
	 * @return
	 * @throws GrowUpDAOException
	 */
	public ParametroFactory setParamPage(final UsuarioDTO dto) throws GrowUpDAOException;
}
