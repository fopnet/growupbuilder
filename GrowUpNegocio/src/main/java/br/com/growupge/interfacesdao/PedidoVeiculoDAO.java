/**
 * 
 */
package br.com.growupge.interfacesdao;

import java.sql.Connection;

import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.ParametroFactory;

/**
 * @author Felipe
 *
 */
public interface PedidoVeiculoDAO extends DAO<PedidoVeiculoDTO> {

	public ParametroFactory getPagamentosPedidos(PedidoVeiculoDTO dto) throws GrowUpDAOException  ;

	/**
	 * @param conn
	 * @param dto
	 * @return
	 * @throws GrowUpDAOException
	 */
	public PedidoVeiculoDTO updateStatus(Connection conn, PedidoVeiculoDTO dto) throws GrowUpDAOException;
}
