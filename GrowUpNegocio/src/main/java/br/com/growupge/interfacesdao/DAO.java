package br.com.growupge.interfacesdao;

import java.sql.Connection;

import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.ParametroFactory;

/**
 * 
 * Interface utilizada por todas as classes DAO's do sistema para implementar as
 * funcionalidades de manipula��o dos dados no banco.
 * 
 * @author Felipe
 * 
 * @param <DTO>
 *            Objeto de transfer�ncia que ser� manipulado.
 */
public interface DAO<DTO> extends ConsultaDAO<DTO> {

	/**
	 * M�todo utilizado para incluir um novo registro, utilizando os valores do
	 * objeto de transfer�ncia informado.
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o com o banco.
	 * @param dto
	 *            Objeto de transfer�ncia que cont�m os valores do registro a
	 *            ser incluido.
	 * @return O transfer object populado com os dados do registro inserido.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro no Oracle.
	 */
	public DTO insert(Connection conn, DTO dto) throws GrowUpDAOException;

	/**
	 * M�todo utilizado para atualizar as informa��es de um registro de acordo
	 * com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o com o banco.
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es do registro a ser
	 *            atualizado.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro no Oracle durante a atualiza��o do
	 *             registro.
	 * @return Um transfer object populado com a informa��o do registro que foi
	 *         atualizado.
	 */
	public DTO update(Connection conn, DTO dto) throws GrowUpDAOException;

	/**
	 * M�todo utilizado para remover um registro que atende os crit�rios do
	 * objeto de transfer�ncia informado.
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o com o banco.
	 * @param dto
	 *            Objeto de transfer�ncia que cont�m os valores do registro a
	 *            ser excluido.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro no Oracle durante a exclus�o do
	 *             registro.
	 * @return Um transfer object populado com a informa��o do registro que foi
	 *         removido.
	 */
	public DTO delete(Connection conn, DTO dto) throws GrowUpDAOException;

	/**
	 * M�todo utilizado para mapear os valores do campo da tabela em quest�o e o
	 * conte�do do mesmo, auxiliando na constru��o do sql din�mico atrav�s da
	 * classe ParametroFactory.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com os valores a serem mapeados para a
	 *            constru��o do sql din�mico.
	 * @return Uma inst�ncia de ParametroFactory para a manipula��o do SQL.
	 * @throws GrowUpDAOException
	 *             Comentar aqui.
	 */
	@Override
	public ParametroFactory setParam(DTO dto) throws GrowUpDAOException;

	


}
