/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 28/07/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 28/07/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.interfacesdao;

import java.sql.Connection;

import br.com.growupge.dto.PagamentoDTO;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.exception.GrowUpDAOException;

/**
 * 
 * Interface utilizada por todas as classes DAO's do sistema para implementar as
 * funcionalidades de manipula��o dos dados no banco.
 * 
 * @author Felipe
 * 
 * @param <DTO>
 *            Objeto de transfer�ncia que ser� manipulado.
 */
public interface PagamentoDAO extends DAO<PagamentoDTO> {

	public abstract int deletePagamentosPayPal(final Connection conn, final PedidoVeiculoDTO dto) throws GrowUpDAOException;
	public abstract int delete(final Connection conn, final PedidoVeiculoDTO dto) throws GrowUpDAOException;

}
