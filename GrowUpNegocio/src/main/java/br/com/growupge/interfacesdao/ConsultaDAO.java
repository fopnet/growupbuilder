/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 07/02/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 07/02/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.interfacesdao;

import java.sql.Connection;
import java.util.List;

import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.enums.TipoPerfil;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.ParametroFactory;

/**
 * Interface que exp�e os m�todos relacionados a busca de informa��es para a
 * visualiza��o de dados auditados.
 * 
 * @param <DTO>
 *            Classe parametrizada com algum DTO
 * @author Felipe
 * 
 */
public interface ConsultaDAO<DTO> extends DtoBuilder<DTO> {

	/**
	 * Sobrecarga do selectALL
	 * 
	 * @param conn
	 * @param dto
	 * @return
	 * @throws GrowUpDAOException
	 */
	public List<DTO> selectAll(
			final Connection conn,
			final DTO dto) throws GrowUpDAOException;
			
	/**
	 * M�todo utilizado para retornar todos os registros que atendem os
	 * crit�rios do objeto de transfer�ncia informado.
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o com o banco.
	 * @param dto
	 *            Cont�m a informa��o do registro a ser encontrado no banco de
	 *            dados. Se o objeto 'dto' for nulo, este m�todo retorna todos
	 *            os valores referentes a este business object, ou seja, ser�
	 *            feito um select * no banco.
	 * @param isClausulaLike
	 *            true usa a cl�usula like, false usa somente where. Determina
	 *            se o select usar� a clausula Like ou Where.
	 * @param indiceInicial
	 *            Indice inicial para fazer a pagina��o.
	 * @return Uma Collection com todos os locais.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum falha durante a pesquisa.
	 */
	public List<DTO> selectAll(final Connection conn, final DTO dto, final boolean isClausulaLike,
			final Integer indiceInicial) throws GrowUpDAOException;

	public <OUT> List<OUT> selectAll(final Connection conn, final DTO dto, final boolean isClausulaLike,
			final Integer indiceInicial,
			final List<OUT> lista) throws GrowUpDAOException;

	/**
	 * M�todo utilizado para retornar todos os registros que atendem os
	 * crit�rios do objeto de transfer�ncia informado.
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o com o banco.
	 * @param isClausulaLike
	 *            true usa a cl�usula like, false usa somente where. Determina
	 *            se o select usar� a clausula Like ou Where.
	 * @param indiceInicial
	 *            Indice inicial para fazer a pagina��o.
	 * @param param
	 *            objeto que configura a query.
	 * @return Uma Collection com todos os locais.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum falha durante a pesquisa.
	 */
	public List<DTO> selectAll(final Connection conn, final boolean isClausulaLike,
			final Integer indiceInicial, final ParametroFactory param) throws GrowUpDAOException;

	/**
	 * M�todo utilizado para retornar todos os registros que atendem os
	 * crit�rios do objeto de transfer�ncia informado.
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o com o banco.
	 * @param dto
	 *            Cont�m a informa��o do registro a ser encontrado no banco de
	 *            dados. Se o objeto 'dto' for nulo, este m�todo retorna todos
	 *            os valores referentes a este business object, ou seja, ser�
	 *            feito um select * no banco.
	 * @return Uma Collection com todos os locais.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum falha durante a pesquisa.
	 */
	public Long count(final Connection conn, final DTO dto) throws GrowUpDAOException;

	/**
	 * 
	 * M�todo utilizado para encontrar um registro de acordo com os dados
	 * informados no objeto de transfer�ncia.
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o com o banco.
	 * @param dto
	 *            Cont�m a informa��o do registro a ser encontrado no banco de
	 *            dados.
	 * @return Um transfer object populado com a informa��o do registro a ser
	 *         encontrado.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro durante a pesquisa.
	 */
	public DTO find(Connection conn, DTO dto) throws GrowUpDAOException;

	/**
	 * Metodo que seta a sessao do negocioBase
	 * 
	 * @param sessaodto
	 */
	//public void setSessao(final SessaoDTO sessaodto);

	/**
	 * Metodo que retorna a sessao do negocioBase
	 * 
	 * @return sessaoDTO
	 */
	public SessaoDTO getSessao();
	
	/**
	 * Verifica se o perfil do usuario logado � maior que o perfil passado
	 * @param perfil
	 * @return
	 */
	public boolean isPerfilUsuarioMaior(TipoPerfil perfil);
	
	/**
	 * @param perfil
	 * @param codigoPerfil
	 * @return
	 */
	public boolean isPerfilUsuarioMaior(TipoPerfil perfil, String codigoPerfil);
	
	/**
	 * Verifica se o usuario logado � admin
	 * @return
	 */
	public boolean isUsuarioAdmin();
	

	/**
	 * M�todo utilizado para configurar oconte�do das colunas para montar o SQL
	 * din�mico.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia que cont�m os valores a serem
	 *            configurados.
	 * @return A inst�ncia de ParametroFactory para manipular o SQL.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro de banco durante a opera��o.
	 */
	public abstract ParametroFactory setParam(DTO dto) throws GrowUpDAOException;
	
	
}
