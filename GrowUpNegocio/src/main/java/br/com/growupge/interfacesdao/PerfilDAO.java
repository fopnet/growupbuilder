/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 28/07/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 28/07/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.interfacesdao;

import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.factory.ParametroFactory;

/**
 * 
 * Interface utilizada por todas as classes DAO's do sistema para implementar as
 * funcionalidades de manipula��o dos dados no banco.
 * 
 * @author Felipe
 * 
 * @param <DTO>
 *            Objeto de transfer�ncia que ser� manipulado.
 */
public interface PerfilDAO<DTO> extends DAO<DTO> {

	/**
	 * M�todo utilizado para configurar a busca de perfil de acordo com o perfil
	 * do usu�rio
	 * 
	 * @param conn
	 *            Inst�ncia da conex�o com o banco.
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es do registro a ser
	 *            atualizado.
	 * @throws GrowUpDAOException
	 *             Caso ocorra algum erro no Oracle durante a atualiza��o do
	 *             registro.
	 * @return Um transfer object populado com a informa��o do registro que foi
	 *         atualizado.
	 */
	public ParametroFactory setParamPerfil(final DTO dto) throws GrowUpDAOException;

}
