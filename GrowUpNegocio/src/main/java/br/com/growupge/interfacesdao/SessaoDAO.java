/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 28/07/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 28/07/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.interfacesdao;

import java.sql.Connection;

import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.exception.GrowUpDAOException;

/**
 */
public interface SessaoDAO extends DAO<SessaoDTO> {

	public abstract void apagarSessoesExpiradas(Connection conn) throws GrowUpDAOException;


}
