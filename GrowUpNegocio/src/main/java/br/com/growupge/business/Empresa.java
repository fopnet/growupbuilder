package br.com.growupge.business;

import static br.com.growupge.util.MensagemBuilder.build;
import static br.com.growupge.utility.Reflection.isCodigoValido;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.HashMap;
import java.util.List;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.EmpresaDTO;
import br.com.growupge.dto.EmpresaItemDTO;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.stereotype.FirebirdStereoType;
import br.com.growupge.util.MensagemBuilder;

/**
 * Business object para empresa.
 * 
 * @author Felipe
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Empresa extends NegocioGrupoBase<EmpresaDTO, EmpresaItemDTO> {

	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Empresa() {}

	/**
	 * @param sessadto
	 * @throws GrowUpException
	 */
	public Empresa(SessaoDTO sessadto) throws GrowUpException {
		this();
		setSessaodto(sessadto);
	}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getEmpresaDAO();
		super.crudGrupoDAO = daoFactory.getEmpresaItemDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}

	/**
	 * M�todo utilizado para verificar se o nome da empresa existe.
	 * 
	 * @throws GrowUpException
	 *             Caso o nome da empresa n�o exista.
	 */
	private /*final*/ void verificarNome(final EmpresaDTO dto) throws GrowUpException {

		// RN3
		EmpresaDTO ldto = new EmpresaDTO();
		ldto.setNome(dto.getNome());
		ldto = this.procurar(ldto);

		// Nome existe
		if (ldto != null) {

			// Duplica��o ao inserir
			if (dto.getCodigo() == 0 || ldto.getCodigo() != dto.getCodigo()) {
				throw new GrowUpException(MSGCODE.EMPRESA_EXISTENTE);
			}
		}

	}

	/**
	 * M�todo utilizado para verificar se o c�digo da empresa existe.
	 * 
	 * @throws GrowUpException
	 *             Caso o c�digo da empresa n�o exista.
	 */
	private /*final*/ void verificarCodigo(EmpresaDTO dto) throws GrowUpException {
		// FE5
		EmpresaDTO empresaDTO = new EmpresaDTO();
		empresaDTO.setCodigo( dto.getCodigo() );

		if (this.procurar(empresaDTO) == null) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}

	}

	/**
	 * M�todo utilizado para verificar se o cnpj da empresa existe.
	 * 
	 * @throws GrowUpException
	 *             Caso o cnpj j� esteja cadastrado na base.
	 */
	private /*final*/ void verificarCnpj(EmpresaDTO dto) throws GrowUpException {

		// FE6
		EmpresaDTO ldto = new EmpresaDTO();
		ldto.setCnpj( dto.getNome() );

		ldto = this.procurar(ldto);

		// Cnpj existe
		if (ldto != null) {

			// Duplica��o ao inserir ou na altera��o
			if (dto.getCodigo() == 0 || ldto.getCodigo() != dto.getCodigo()) {
				throw new GrowUpException(MSGCODE.CNPJ_EXISTENTE);
			}
		}
	}

	/**
	 * Verificar se os dados da empresa informada est�o cadastrados na base.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia que cont�m as informa��es da empresa.
	 * @throws GrowUpException
	 *             Caso ocorra o dado n�o seja encontrado na base.
	 */
	private /*final*/ void verificarUsoEmpresa(final EmpresaDTO dto) throws GrowUpException {
		// RN1
		EmpresaItemDTO item = new EmpresaItemDTO();
		item.empresa = dto;

		if (!this.buscarTodas(item).isEmpty()) {
			throw new GrowUpException(MSGCODE.EMPRESA_EM_USO, build(dto.getNome()));
		}
	}

	/**
	 * Verifica se o 'cnpj' informado segue o padr�o desejado.
	 * 
	 * @param value
	 *            O cnpj a ser verificado
	 * @return <code>true</code> Caso o 'cnpj' esteja no padr�o e
	 *         <code>false</code> caso contr�rio.
	 */
	private /*final*/ boolean isValidarCnpj(final String value) {

		int soma = 0;
		String cnpj = null;

		try {
			cnpj = value.replace("/", "").replace("-", "");
			Long.parseLong(cnpj);
		} catch (Exception e) {
			return false;
		}

		if (cnpj.length() == 14) {

			for (int i = 0, j = 5; i < 12; i++) {
				soma += j-- * (cnpj.charAt(i) - '0');
				if (j < 2) {
					j = 9;
				}
			}

			soma = 11 - (soma % 11);
			if (soma > 9) {
				soma = 0;
			}
			if (soma == (cnpj.charAt(12) - '0')) {
				soma = 0;
				for (int i = 0, j = 6; i < 13; i++) {
					soma += j-- * (cnpj.charAt(i) - '0');
					if (j < 2) {
						j = 9;
					}
				}
				soma = 11 - (soma % 11);
				if (soma > 9) {
					soma = 0;
				}
				if (soma == (cnpj.charAt(13) - '0')) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.business.NegocioBase#validarCamposObrigatorios(java.lang.Object)
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final EmpresaDTO dto) throws GrowUpException {

		// RN2
		if (isBlank(dto.getNome())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
		
		// RN10
		if (dto.getCnpj() != null) {

			if (!this.isValidarCnpj(dto.getCnpj())) {
				throw new GrowUpException(MSGCODE.CAMPOS_INVALIDOS);
			}

			dto.setCnpj( dto.getCnpj().replace("/", "").replace("-", "") );
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.business.NegocioGrupoBase#validarCamposObrigatorios(br.com.growupge.dto.DTOPadrao)
	 */
	@Override
	protected void validarCamposObrigatorios(EmpresaItemDTO dto) throws GrowUpException {
		// RN2
		if (isCodigoValido(dto.empresa)) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
		if (isCodigoValido(dto.cliente)) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}

	}

	/**
	 * Cria uma nova empresa com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da empresa.
	 * @return Um objeto de transfer�ncia com os dados da empresa criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	public /*final*/ EmpresaDTO criarEmpresa(final EmpresaDTO dto) throws GrowUpException {

		// DAOFactory daofactory = daoFactory.getDAOFactory();
		EmpresaDTO empresaDTO = null;

		try {
			// Se os valores forem v�lidos, atribui a classe.
			this.validarCamposObrigatorios(dto);

			// RN3
			this.verificarNome(dto);

			// RN6
			this.verificarCnpj(dto);

			DAO<EmpresaDTO> dao = daoFactory.getEmpresaDAO();
			empresaDTO = dao.insert(daoFactory.getConnection(), dto);


			empresaDTO = MensagemBuilder.build(	MSGCODE.EMPRESA_INCLUIDO_COM_SUCESSO, 
												empresaDTO, empresaDTO.getNome());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			daoFactory.closeCommit();
		}
		return empresaDTO;
	}

	/**
	 * Alterar a empresa com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da empresa.
	 * @return Um objeto de transfer�ncia com os dados da empresa alterada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de altera��o.
	 */
	public /*final*/ EmpresaDTO alterarEmpresa(final EmpresaDTO dto) throws GrowUpException {

		EmpresaDTO empresaDTO = null;
		// DAOFactory daofactory = daoFactory.getDAOFactory();

		try {

			// RN2
			this.validarCamposObrigatorios(dto);
			
			//RN4
			this.verificarExclusaoPrevia(dto);

			// RN?
			this.verificarCodigo(dto);

			// RN3
			this.verificarNome(dto);

			// RN6
			// this.verificarCnpj();

			DAO<EmpresaDTO> dao = daoFactory.getEmpresaDAO();
			empresaDTO = dao.update(daoFactory.getConnection(), dto);

			empresaDTO.codMensagem = MSGCODE.EMPRESA_ALTERADO_COM_SUCESSO;
			// empresaDTO.dtoAntesAlteracao = dtoAntes;
			empresaDTO.parametros = new HashMap<String, String>();
			empresaDTO.parametros.put(Constantes.P1, empresaDTO.getNome());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {
			daoFactory.closeCommit();
		}

		return empresaDTO;
	}

	/**
	 * Exclui a empresa que cont�m os dados informados no objeto de
	 * transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da empresa.
	 * @return Um objeto de transfer�ncia com os dados da empresa criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 */
	public /*final*/ EmpresaDTO excluirEmpresa(final EmpresaDTO dto) throws GrowUpException {

		//DAOFactory daofactory = daoFactory.getDAOFactory();
		EmpresaDTO empresaDTO = null;

		try {
			//RN4
			this.verificarExclusaoPrevia(dto);

			// RN1
			this.verificarUsoEmpresa(dto);

			DAO<EmpresaDTO> dao = daoFactory.getEmpresaDAO();
			empresaDTO = dao.delete(daoFactory.getConnection(), dto);

			empresaDTO.codMensagem = MSGCODE.EMPRESA_REMOVIDO_COM_SUCESSO;
			empresaDTO.parametros = new HashMap<String, String>();
			empresaDTO.parametros.put(Constantes.P1, empresaDTO.getNome());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			daoFactory.closeCommit();
		}

		return empresaDTO;
	}

	/**
	 * M�todo adiciona um servi�o ao grupo de servidor
	 * 
	 * @param dto
	 *            Parametro do tipo EmpresaItemDTO
	 * @throws SigemException
	 *             Caso ocorra algum erro durante a opera��o.
	 * @return Retorna um objeto do tipo 'EmpresaItemDTO'
	 */
	public /*final*/ EmpresaItemDTO adicionarEmpresaItem(final EmpresaItemDTO dto)
			throws GrowUpException {

		// DAOFactory daofactory = daoFactory.getDAOFactory();
		EmpresaItemDTO itemDTO = null;
		try {

			this.validarCamposObrigatorios(dto);

			itemDTO = new EmpresaItemDTO();
			itemDTO.empresa = dto.getEmpresa();
			itemDTO.cliente = dto.getCliente();

			// Verifica se a associa��o j� existe.
			List<EmpresaItemDTO> itens = this.buscarTodas(itemDTO);
			if (itens.size() > 0) {
				itemDTO.parametros = new HashMap<String, String>();
				itemDTO.parametros.put(Constantes.P1, itemDTO.getCliente().getNome());
				throw new GrowUpException(MSGCODE.CLIENTE_EXISTENTE, itemDTO.parametros);
			}

			DAO<EmpresaItemDTO> dao = daoFactory.getEmpresaItemDAO();
			itemDTO = dao.insert(daoFactory.getConnection(), dto);

			itemDTO =MensagemBuilder.build(	MSGCODE.EMPRESA_ITEM_INCLUIDO_COM_SUCESSO, itemDTO, 
											itemDTO.cliente.getNome(),
											itemDTO.empresa.getNome());

			return itemDTO;

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			daoFactory.closeCommit();
		}
	}

	/**
	 * M�todo utilizado para excluir lista de servidores cujo o c�digo � igual
	 * ao informado no transfer object.
	 * 
	 * @param dto
	 *            Transfer object que cont�m o c�digo a ser excluido.
	 * @throws GrowUpException
	 *             Caso ocorra algum problema durante a opera��o de exclus�o.
	 * @return GrupoServidorDTO Objeto de transfer�ncia com os valores do grupo
	 *         servidor utilizado ou <code>null</code> caso ocorra algum erro
	 *         durante a exclus�o.
	 */
	public /*final*/ EmpresaItemDTO removerEmpresaItem(final EmpresaItemDTO dto) throws GrowUpException {

		// DAOFactory daofactory = daoFactory.getDAOFactory();
		EmpresaItemDTO itemDTO = null;
		try {

			this.validarCamposObrigatorios(dto);

			DAO<EmpresaItemDTO> dao = daoFactory.getEmpresaItemDAO();
			itemDTO = dao.delete(daoFactory.getConnection(), dto);

			itemDTO.codMensagem = MSGCODE.EMPRESA_ITEM_REMOVIDO_COM_SUCESSO;
			itemDTO.parametros = new HashMap<String, String>();
			itemDTO.parametros.put(Constantes.P1, itemDTO.cliente.getNome());
			itemDTO.parametros.put(Constantes.P2, itemDTO.empresa.getNome());

			return itemDTO;

		} catch (GrowUpException e) {
			if (daoFactory != null) {
				daoFactory.rollBack();
			}
			throw e;
		} catch (Exception e) {
			if (daoFactory != null) {
				daoFactory.rollBack();
			}
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			try {
				if (daoFactory != null) {
					daoFactory.closeCommit();
				}
			} catch (Exception e) {
				
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}
	}

	/**
	 * M�todo usado para retornar as empresas sem precisa do SID.
	 * 
	 * @return objeto empresadto
	 * @throws GrowUpException
	 */
	public /*final*/ List<EmpresaDTO> buscarEmpresas() throws GrowUpException {
		return this.buscarTodas();
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public EmpresaDTO verificarExclusaoPrevia(final EmpresaDTO dto) throws GrowUpException {
		EmpresaDTO dtoAntes = null;

		dtoAntes = this.procurar(dto);

		if (dtoAntes == null) {
			dtoAntes = new EmpresaDTO();
			dtoAntes.parametros = new HashMap<String, String>();
			dtoAntes.parametros.put(Constantes.P1, dto.getNome());
			throw new GrowUpException(
					MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,
					dtoAntes.parametros);
		}

		return dtoAntes;
	}

	/**
	 * @return
	 * @throws GrowUpException 
	 */
	public EmpresaDTO procurarGrowupVeiculos() throws GrowUpException {
		EmpresaDTO dto = new EmpresaDTO();
		dto.setCodigo(EmpresaDTO.GROWUP_VEICULOS_ID);
		return procurar(dto);
	}
}
