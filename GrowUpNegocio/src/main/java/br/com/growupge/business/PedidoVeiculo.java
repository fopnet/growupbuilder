/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 22/12/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 22/12/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.business;

import static br.com.growupge.util.FileUtil.getUploadFile;
import static br.com.growupge.util.MensagemBuilder.build;
import static br.com.growupge.utility.Reflection.isCodigoValido;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.MessagingException;

import org.apache.commons.io.FileUtils;

import br.com.growupge.builder.AnexoRestricaoBuilder;
import br.com.growupge.builder.AnexoRestricaoFactory;
import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.AnexoDTO;
import br.com.growupge.dto.EmpresaDTO;
import br.com.growupge.dto.IEmail;
import br.com.growupge.dto.MensagemDTO;
import br.com.growupge.dto.PagamentoDTO;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.dto.RestricaoDTO;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.dto.StatusPedido;
import br.com.growupge.dto.TramitacaoDTO;
import br.com.growupge.dto.VariavelDTO;
import br.com.growupge.dto.VeiculoDTO;
import br.com.growupge.enums.TipoPerfil;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.HtmlTemplateFactory;
import br.com.growupge.fbirdDAO.CollectionSynchronizePersistence;
import br.com.growupge.gerenciadores.GerenciadorEmail;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.interfacesdao.PagamentoDAO;
import br.com.growupge.interfacesdao.PedidoVeiculoDAO;
import br.com.growupge.interfacesdao.RestricaoDAO;
import br.com.growupge.interfacesdao.SequenciaDAO;
import br.com.growupge.interfacesdao.TramitacaoDAO;
import br.com.growupge.stereotype.FirebirdStereoType;
import br.com.growupge.template.HtmlTemplate;
import br.com.growupge.template.UsuariosExpiradosTemplate;
import br.com.growupge.tradutores.Tradutor;
import br.com.growupge.tradutores.TradutorLabels;
import br.com.growupge.util.CollectionSynchronizeFiles;
import br.com.growupge.util.FileUtil;
import br.com.growupge.util.MensagemBuilder;
import br.com.growupge.utility.DateUtil;

/**
 * Business object para controle de permiss�es.
 * 
 * @author Felipe
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class PedidoVeiculo extends NegocioBase<PedidoVeiculoDTO> {

	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected PedidoVeiculo() {}
	
	/**
	 * Construtor para esta classe.
	 * 
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o
	 */
	public PedidoVeiculo(SessaoDTO sessaodto) {
		this();
		this.setSessaodto(sessaodto);
	}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getPedidoVeiculoDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.business.NegocioBase#validarCamposObrigatorios(java.lang.Object)
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final PedidoVeiculoDTO dto)
			throws GrowUpException {
		if (dto == null) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
		VeiculoDTO veic = dto.getVeiculo();

		/* Obrigando a preencher a placa */
		if (isBlank(veic.getCodigo())) {
			Properties rb  = Tradutor.getInstance(getIdiomaUsuario());
			throw new GrowUpException(MSGCODE.CAMPOX_OBRIGATORIOS, build(rb.get(TradutorLabels.LABEL_PLACA)));
		}
		
/*		int counter = 0;
		if (isPerfilUsuarioInferiorAo(TipoPerfil.EMPRESA)) {
			if ( isBlank(veic.getRenavam()) ) counter ++;  
			if ( isBlank(veic.getChassi()) )  counter ++;
			if (!isCodigoValido(veic.getMarca())) counter ++;
		}
		
		if (counter > 2) // pelo menos duas informacoes do ve�culo s�o obrigat�rias
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
*/		
//		if (!isCodigoValido(dto.cliente)) {
//			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
//		}
		if (!isCodigoValido(dto.getServico())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
		
		for (RestricaoDTO item : dto.getRestricoes())
			item.validate();
		
	}

	/**
	 * M�todo utilizado para a cria��o de uma nova permiss�o.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com os dados da permiss�o a ser
	 *            criada.
	 * @return Objeto de transfer�ncia com os dados da permiss�o.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a cria��o da nova permiss�o.
	 */
	public /*final*/ PedidoVeiculoDTO criarPedidoVeiculo(final PedidoVeiculoDTO dto)
			throws GrowUpException {
		PedidoVeiculoDTO newDTO = null;

		// DAOFactory daofactory =  daoFactory.getDAOFactory();
		try {
			/* Valida se campo est� em branco */
			this.validarCamposObrigatorios(dto);

//			dto.setStatus(StatusPedido.PENDENTE.toString());

			//Cadastrando veiculo caso n�o exista
			dto.setUsuarioCadastro(getUsuarioSessao());
			dto.setDataCadastro(DateUtil.getDateManagerInstance().getDateTime());
			dto.setServico(getServicoBO().procurar(dto.getServico()));
			StatusPedido.PENDENTE.pendente(dto);

			Veiculo veic = getVeiculoBO();
			if (veic.procurar(dto.getVeiculo()) == null) { //busca o veiculo pela placa.
				dto.getVeiculo().setUsuarioCadastro(getUsuarioSessao());
				dto.getVeiculo().setDataCadastro(DateUtil.getDateManagerInstance().getDateTime());
				veic.criarVeiculo(dto.getVeiculo());
			}

			DAO<PedidoVeiculoDTO> dao = daoFactory.getPedidoVeiculoDAO();
			newDTO = dao.insert(daoFactory.getConnection(), dto);
			newDTO = build(MSGCODE.PEDIDO_VEICULO_INCLUIDO_COM_SUCESSO, newDTO, 
							newDTO.getPlaca(), 
							DateUtil.getDateManagerInstance(newDTO.getDataCadastro()).getLongDate());

			salvarRestricoesAnexos(newDTO, daoFactory);

			//Criando tramita��o autom�tica
			if (newDTO.getServico().isTramitado()) {
				TramitacaoDTO tram = new TramitacaoDTO();
				tram.setIsPendente(Constantes.SIM);
				tram.setPedido(newDTO);
				tram.setUsuarioCadastro(newDTO.getUsuarioCadastro());

				MensagemDTO msg = new MensagemDTO(MSGCODE.TRAMITACAO_AGUARDANDO_PAGAMENTO);
				tram.setObs(super.getMensagemBO().procurar(msg).getDescricaoBR());

				//Gerando primeira tramita��o pendente e autom�tica
				getTramitacaoBO().criarTramitacao(tram, daoFactory);
			} else {
//				newDTO.setStatus(StatusPedido.A_FATURAR.toString()); //pendente de pagamento
				// Caso o pedido esteja concluido, ele pode retornar para o estado anterior, se uma nova pendencia for criada
				newDTO.getStatus().pendente(newDTO);
				this.alterarPedidoVeiculo(newDTO);
			}
			
			VariavelDTO varDTO = getVariavelBO().buscarValorUnitario();
			
			this.enviarEmailCobranca(newDTO, varDTO.getValorAsDouble());
			
		} catch (GrowUpException e) {				
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			try {
				daoFactory.closeCommit();
			} catch (Exception e) {
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}

		}
		
		if (isPerfilUsuarioInferiorAo(TipoPerfil.EMPRESA)) {
			try {
				notificarNovoPedido(newDTO);
			} catch (Exception e) {
				// Nao impedir a conslus�o do pedido caso aqui retorne alguma excecao.
				getLogger().error(e,e);
			}
		}
		
		return newDTO;

	}
	
	/**
	 * @throws Exception 
	 * 
	 */
	private void notificarNovoPedido(PedidoVeiculoDTO dto) throws Exception {
		EmpresaDTO destinatario = super.getEmpresaBO().procurarGrowupVeiculos();
		
		String assunto = destinatario.getNome().concat(" - Aviso de novo pedido placa ") + dto.getPlaca().toUpperCase();
		
		// Obtem o Template para o determinado fim
		HtmlTemplate templateEmail = HtmlTemplateFactory.getTemplateNovoPedido();

		// Adiciona os parametros que dever�o ser substituidos no template
		templateEmail.adicionarParametro(HtmlTemplate.PARAM_USR, dto.getUsuarioCadastro().getCodigo());
		templateEmail.adicionarParametro(HtmlTemplate.PARAM_NIP, dto.getCodigo());
		templateEmail.adicionarParametro(HtmlTemplate.PARAM_PLACA, dto.getPlaca());

		// Executa o template
		templateEmail.execute();
		
		GerenciadorEmail ge = GerenciadorEmail.getInstance();
		ge.enviarEmailAssincrono(assunto, templateEmail.getHtmlTemplate(), destinatario, dto.getUsuarioCadastro());
	}

	/**
	 * @param valorUnitario 
	 * @throws Exception 
	 * 
	 */
	private void enviarEmailCobranca(PedidoVeiculoDTO dto, Double valorUnitario) throws Exception {
		EmpresaDTO remetente = super.getEmpresaBO().procurarGrowupVeiculos();
		
		String assunto = remetente.getNome().concat(" - Aviso recebimento Pedido ") + dto.getCodigo();
		
		// Obtem o Template para o determinado fim
		HtmlTemplate templateEmail = HtmlTemplateFactory.getTemplateCobrancaUsuarioComum();

		// Adiciona os parametros que dever�o ser substituidos no template
		templateEmail.adicionarParametro(HtmlTemplate.PARAM_NIP, dto.getCodigo());
		templateEmail.adicionarParametro(HtmlTemplate.PARAM_PLACA, dto.getPlaca());
		templateEmail.adicionarParametro(HtmlTemplate.PARAM_PRAZO, PagamentoDTO.PERIODO_VALIDADE_DESCRICAO);
		if (valorUnitario != null)
			templateEmail.adicionarParametro(HtmlTemplate.PARAM_VALOR_UNITARIO, String.format("%.2f", valorUnitario));
		else 
			templateEmail.adicionarParametro(HtmlTemplate.PARAM_VALOR_UNITARIO, "");

		// Executa o template
		templateEmail.execute();
		
		GerenciadorEmail ge = GerenciadorEmail.getInstance();
		ge.enviarEmailAssincrono(assunto, templateEmail.getHtmlTemplate(), dto.getUsuarioCadastro(), remetente);
	}

	/** Compara os anexos existentes no banco e compara com novos 
	 * @param dto
	 * @param daofactory somente para utilizar a mesma variavel, mas poderia nao recebe-la
	 * @throws GrowUpDAOException
	 * @throws GrowUpException
	 */
	protected void salvarRestricoesAnexos(final PedidoVeiculoDTO dto,
			DAOFactory daofactory) throws GrowUpDAOException, GrowUpException {
		DAO<RestricaoDTO> restDAO = daoFactory.getRestricaoDAO();
		List<RestricaoDTO> itensGravados = restDAO.selectAll(daoFactory.getConnection(), 
															new RestricaoDTO(dto));
		CollectionSynchronizePersistence<RestricaoDTO> 
		sync = new CollectionSynchronizePersistence<RestricaoDTO>(daofactory, restDAO) {
			@Override
			protected void beforeInsert(final RestricaoDTO item) throws GrowUpException {
				item.setPedido(dto);
			}
			
		};
		sync.synchonize(dto.getRestricoes(), itensGravados);

		CollectionSynchronizeFiles syncAnexo = new CollectionSynchronizeFiles(this);
		syncAnexo.synchonize(dto.getAnexos(), buscarAnexos(dto));
	}

	/**
	 * M�todo utilizado para a alterar a permiss�o informada.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com os dados da permiss�o a ser
	 *            alterada.
	 * @return PedidoVeiculoDTO Objeto de transfer�ncia com dados da permiss�o.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a altera��o da permiss�o.
	 */
	public /*final*/ PedidoVeiculoDTO alterarPedidoVeiculo(final PedidoVeiculoDTO dto)
			throws GrowUpException {
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {

			PedidoVeiculoDTO newDTO = null;

			/* Valida se campo est� em branco */
			this.validarCamposObrigatorios(dto);

			dto.setUsuarioAlteracao(this.getSessao().getUsuario());
			dto.setDataAlteracao(DateUtil.getDateManagerInstance().getDateTime());
			
			//RN4
			this.verificarExclusaoPrevia(dto);

			salvarRestricoesAnexos(dto, daoFactory);
			
			DAO<PedidoVeiculoDTO> dao = daoFactory.getPedidoVeiculoDAO();
			newDTO = dao.update(daoFactory.getConnection(), dto);

			newDTO = build(MSGCODE.PEDIDO_VEICULO_ALTERADO_COM_SUCESSO,
							newDTO,
							newDTO.getPlaca(),
							DateUtil.getDateManagerInstance(newDTO.getDataCadastro()).getShortDate());

			return newDTO;

		} catch (GrowUpException e) {
				daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
				daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			try {
				daoFactory.closeCommit();
			} catch (Exception e) {
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}
	}

	/**
	 * M�todo utilizado para excluir permiss�o que cont�m o c�digo informado no
	 * transfer object.
	 * 
	 * @param dto
	 *            Transfer object que cont�m a permiss�o a ser excluida.
	 * @throws GrowUpException
	 *             Caso ocorra algum problema durante a opera��o de exclus�o.
	 * @return PedidoVeiculoDTO Objeto de transfer�ncia com os dados da
	 *         permiss�o exclu�da.
	 */
	public /*final*/ PedidoVeiculoDTO excluirPedidoVeiculo(final PedidoVeiculoDTO dto)
			throws GrowUpException {
		PedidoVeiculoDTO newDTO = null;

		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		try {
			//RN4
			this.verificarExclusaoPrevia(dto);

			PagamentoDAO daoPgto = daoFactory.getPagamentoDAO();
			daoPgto.delete(daoFactory.getConnection(), dto);

			TramitacaoDAO daoPend = daoFactory.getTramitacaoDAO();
			daoPend.delete(daoFactory.getConnection(), dto);

			RestricaoDAO daoRestricao = daoFactory.getRestricaoDAO();
			daoRestricao.delete(daoFactory.getConnection(), dto);
			
			DAO<PedidoVeiculoDTO> dao = daoFactory.getPedidoVeiculoDAO();
			newDTO = dao.delete(daoFactory.getConnection(), dto);
			
			File folder = getUploadFile(String.valueOf(dto.getCodigo()));
			if (folder.exists())
				FileUtils.deleteQuietly(folder);

			newDTO = build(MSGCODE.PEDIDO_VEICULO_REMOVIDO_COM_SUCESSO,
					newDTO,
					newDTO.getPlaca(),
					DateUtil.getDateManagerInstance(newDTO.getDataCadastro()).getShortDate());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			try {
				daoFactory.closeCommit();
			} catch (Exception e) {
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}

		}

		return newDTO;
	}

	/**
	 * Gera um arquivo cvs com os pedidos gerados pelo parametro dto e envia por
	 * email.
	 * 
	 * @param dto
	 *            Parametro para buscar os pedidos
	 * @return Retorna objeto do tipo list.
	 * @throws GrowUpException
	 *             caso ocorra algum erro na opera��o.
	 */
	public /*final*/ List<PedidoVeiculoDTO> exportarFatura(final PedidoVeiculoDTO dto)
			throws GrowUpException {

		BufferedWriter output = null;
		List<PedidoVeiculoDTO> lista = new ArrayList<PedidoVeiculoDTO>();
		//= dto.faturas;
		File file = null;
		try {
			file = new File(Constantes.TEMP_FOLDER.concat("fatura.csv"));
			String[] colunas = { "Placa", "Data", "Usu�rio", "Servi�o", "Pre�o" };

			/* FileWriter always assumes default encoding is OK! */
			output = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(file),
					FileUtil.CHARSET_ENCODE));

			for (String coluna : colunas) {
				output.write(coluna.concat(";"));
			}

			//Dto parametrizado com as informa��es do filtro.
			LinkedList<PedidoVeiculoDTO> fila = new LinkedList<PedidoVeiculoDTO>(buscarTodas(dto));
			while (!fila.isEmpty()) {
				PedidoVeiculoDTO item = fila.poll();

				// Somente faturar pedidos conclu�dos de pend�ncias
				if (!item.getStatus().isPendente()) {
					continue;
				}
				lista.add(item);

				//Alterando o status do pedido
//				item.setStatus(StatusPedido.FATURADO.toString());
				item.getStatus().pagar( item );
				this.alterarPedidoVeiculo(item);

				output.newLine();
				output.write(item.getPlaca().concat(";"));
				output.write(DateUtil.getDateManagerInstance(item.getDataCadastro())
						.getLongDate().concat(";"));
				output.write(item.getUsuarioCadastro().getCodigo().concat(";"));
				output.write(item.getServico().getNome().concat(";"));
			}
			output.close();

			String subject = "Fatura do m�s ".concat(DateUtil.getDateInstance(
					Constantes.LANGUAGE_BR).getMonthName());

			if (!lista.isEmpty()) {
				GerenciadorEmail.getInstance().enviarEmail(
						subject,
						"Fatura em anexo no arquivo em formato excel.",
						this.getSessao().getUsuario().getEmpresa(),
						this.getSessao().getUsuario(),
						file);
			}

			if (!file.delete()) {
				Map<String, String> hash = new HashMap<String, String>();
				hash.put(Constantes.P1, file.getName());
				throw new GrowUpException(MSGCODE.ERRO_ARQUIVO_NAO_ENCONTRADO, hash);
			}

		} catch (IOException e) {
			
			Map<String, String> hash = new HashMap<String, String>();
			hash.put(Constantes.P1, file.getAbsolutePath());
			throw new GrowUpException(MSGCODE.ERRO_ENTRADA_SAIDA, hash);
		} catch (MessagingException ex) {
			throw new GrowUpException(MSGCODE.ERRO_ENVIO_EMAIL, ex.getMessage(), "", ex
					.getMessage());
		} catch (GrowUpException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, ex);
		} finally {
			try {
				if (output != null)
					output.close();
			} catch (IOException e) {
				
			}
		}

		return lista;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public PedidoVeiculoDTO verificarExclusaoPrevia(PedidoVeiculoDTO dto) throws GrowUpException {

		PedidoVeiculoDTO dtoAntes = null;

		dtoAntes = this.procurar(dto);

		if (dtoAntes == null) {
			throw new GrowUpException(
					MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,
					build(dto.getCodigo()));
		}

		return dtoAntes;
	}

	public /*final*/ PedidoVeiculoDTO concluirPedido(final PedidoVeiculoDTO dto) throws GrowUpException {
		// DAOFactory daofactory = daoFactory.getDAOFactory();
		try {
			PedidoVeiculoDTO newDTO = null;

			/* Valida se campo est� em branco */
			if (!isCodigoValido(dto)) {
				throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
			}

			//RN4
			newDTO = this.verificarExclusaoPrevia(dto);

			newDTO.setVeiculo( getVeiculoBO().procurar(newDTO.getVeiculo()) );
			newDTO.setAnexos(buscarAnexos(newDTO));
//			newDTO.setStatus(dto.getStatus());

//			StatusPedido sts = StatusPedido.getEnum(newDTO.getStatus());
			if (dto.getStatus().isEnviado())
				newDTO.getStatus().enviar(newDTO);
			else
				newDTO.getStatus().concluir(newDTO);

			newDTO = this.alterarPedidoVeiculo(newDTO);

			// Fechando pendencias
			TramitacaoDTO pend = new TramitacaoDTO();
			pend.setIsPendente(Constantes.SIM);
			pend.setPedido(newDTO);

			Tramitacao pendBo = getTramitacaoBO();
			long pendencias = pendBo.buscarTotal(pend);
			if (pendencias > 1) {
				throw new GrowUpException(MSGCODE.PEDIDO_VEICULO_MAIS_1_PENDENCIA);
			} else if (pendencias == 1) {
				pend = pendBo.procurar(pend);
				pend.setIsPendente(Constantes.NAO);
				pendBo.alterarTramitacao(pend);
			}

			return build(MSGCODE.PEDIDO_VEICULO_CONCLUIDO_COM_SUCESSO, newDTO);
		
		} catch (GrowUpException e) {				
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			try {
				daoFactory.closeCommit();
			} catch (Exception e) {
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}

	}

	/**
	 * Envia o email do pedido e conclui o mesmo
	 * 
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	public /*final*/ PedidoVeiculoDTO concluirPedidoEnviar(final PedidoVeiculoDTO dto) throws Exception {
		
		// DAOFactory daofactory = daoFactory.getDAOFactory();
		try {
				
			PedidoVeiculoDTO pedido = concluirPedido(dto);
				
			IEmail remetente = getEmpresaBO().procurarGrowupVeiculos();
			String assunto = "Resposta Pedido Nip: " + pedido.getCodigo();
			IEmail destinatario = getUsuarioBO().procurar( pedido.getUsuarioCadastro() );
			
			File folder = getUploadFile(String.valueOf(pedido.getCodigo()));
			
			// Obtem o Template com base no idioma do usu�rio
			HtmlTemplate template = HtmlTemplateFactory.getTemplateRespostaPedido();
			template.adicionarParametro(HtmlTemplate.PARAM_NIP, Long.toString(pedido.getCodigo()));
			template.adicionarParametro(HtmlTemplate.PARAM_PLACA, pedido.getPlaca());
			template.adicionarParametro(HtmlTemplate.PARAM_PEDIDO, pedido);
			template.adicionarParametro(HtmlTemplate.PARAM_PASTA, folder);
	
			Collections.sort(  pedido.getRestricoes() );
			// Adiciona os parametros que dever�o ser substituidos no template
	
			// Executa o template
			template.execute();
				
			GerenciadorEmail ge = GerenciadorEmail.getInstance();
			ge.enviarEmail(	assunto, template.getHtmlTemplate(), 
							remetente, destinatario, null, remetente,
							folder.listFiles());
			
			// Trocar o estado do pedido para Enviado caso n�o de erro no envio
//			pedido.setStatus(StatusPedido.ENVIADO.toString());
			pedido.getStatus().enviar(pedido);
			pedido = concluirPedido(pedido);
			
			//Apagar o que foi enviado.
			FileUtils.deleteQuietly(folder);
			
			return build(MSGCODE.PEDIDO_VEICULO_CONCLUIDO_COM_SUCESSO, pedido);

		} catch (GrowUpException e) {				
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			try {
				daoFactory.closeCommit();
			} catch (Exception e) {
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}
		
	}

	/**
	 * Buscar os dados completos do pedido e seus anexos
	 * @param dto
	 * @return
	 * @throws GrowUpException
	 */
	public PedidoVeiculoDTO procurarPedido(PedidoVeiculoDTO dto) throws GrowUpException {
		dto = super.procurar(dto);
		
		if ( dto != null) {
			 dto.setAnexos( buscarAnexos(dto) );
		}
		
		return dto; 
	}

	/**
	 * @param dto
	 * @return 
	 */
	private List<AnexoDTO> buscarAnexos(PedidoVeiculoDTO dto) {
		List<AnexoDTO> anexos = new ArrayList<AnexoDTO>();
		
		File folder = FileUtil.getUploadFile(Long.valueOf(dto.getCodigo()).toString());
		 if (folder.exists()) {
			 for (File file : folder.listFiles()) {
				 anexos.add(new AnexoDTO(file, dto));
			}
		 }
		 return anexos;
	}
	
	/**
	 * Apaga o anexo no servidor
	 * @param pedido
	 * @return
	 * @throws GrowUpException
	 */
	public PedidoVeiculoDTO removerAnexo(PedidoVeiculoDTO pedido, AnexoDTO anexo) throws GrowUpException {
		
		File folder = FileUtil.getUploadFile(Long.valueOf(pedido.getCodigo()).toString());
		 if (folder.exists()) {
			 if (FileUtils.getFile(folder, anexo.getNome()).delete()) {
				 pedido.getAnexos().remove(anexo);
			 }
		 }
		 
		return pedido; 
	}

	/**
	 * M�todo utilizado para a cria��o de uma nova permiss�o.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com os dados da permiss�o a ser
	 *            criada.
	 * @return Objeto de transfer�ncia com os dados da permiss�o.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a cria��o da nova permiss�o.
	 */
	public /*final*/ PedidoVeiculoDTO criarNip() throws GrowUpException {
		PedidoVeiculoDTO dto = new PedidoVeiculoDTO();

		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		try {

			SequenciaDAO seq = daoFactory.getSequenciaDAO();
			dto.setCodigo(seq.getProximaSequencia("GEN_PEDI_CD_PEDIDO"));
			dto = build(MSGCODE.PEDIDO_NIP_GERADO_SUCESSO, dto, 
							dto.getCodigo());

		} catch (GrowUpException e) {				
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			try {
				daoFactory.closeCommit();
			} catch (Exception e) {
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}

		}

		return dto;

	}
	
	/**
	 * Lista os pedidos com os pagamentos carregados.
	 * @param pedido
	 * @return
	 * @throws GrowUpException
	 */
	public List<PedidoVeiculoDTO> buscarPagamentosPedidos(PedidoVeiculoDTO pedido) throws GrowUpException {
		PedidoVeiculoDAO dao = daoFactory.getPedidoVeiculoDAO();
		return dao.selectAll(daoFactory.getConnection(), 
									false, 
									Constantes.DESCONSIDERAR_PAGINACAO, 
									dao.getPagamentosPedidos(pedido));
	}

	/**
	 * Rotina para buscar todos os pedidos que podem estar expirados
	 * e marc�-los conforme.
	 * @return
	 * @throws GrowUpException
	 */
	public List<PedidoVeiculoDTO> expirarPedidos() throws GrowUpException {

		EmpresaDTO remetente = super.getEmpresaBO().procurarGrowupVeiculos();
		
		try {
			PedidoVeiculoDTO pedido = new PedidoVeiculoDTO();
			StatusPedido.PENDENTE.pendente(pedido);
			
			// Quantidade de horas para periodo de validade do pedido pendente
			final int validade =  PagamentoDTO.PERIODO_VALIDADE_CHAVE * 24; 
			final List<PedidoVeiculoDTO> expirados = new ArrayList<PedidoVeiculoDTO>();
			final List<PedidoVeiculoDTO> pendentes = buscarPagamentosPedidos(pedido);
			final UsuariosExpiradosTemplate templateEmail = HtmlTemplateFactory.getTemplateUsuarioExpirados();
			
			final PedidoVeiculoDAO pedidoDAO = daoFactory.getPedidoVeiculoDAO();

			for (PedidoVeiculoDTO  item : pendentes) {

				Date dataPagamento = item.getDataPagamento();

				long periodo = DateUtil.getDateManagerInstance().diffHour(dataPagamento);

				if ( periodo >  validade ) {
					item.getStatus().expirar(item);

					final PedidoVeiculoDTO expirado = pedidoDAO.updateStatus(daoFactory.getConnection(), item);

					expirados.add( expirado );

					templateEmail.adicionarPedidoExpirado(item, periodo);
				}
			}
			
			
			if (!expirados.isEmpty()) {

				// Executa o template
				templateEmail.execute();

				GerenciadorEmail.getInstance()//&aacute;
				.enviarEmail("Lista de pedidos expirados", templateEmail.getHtmlTemplate(), remetente, remetente);
			}
			
			return expirados;

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			super.notificarErroSistema(e, remetente, remetente);
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			super.notificarErroSistema(e, remetente, remetente);
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			try {
				daoFactory.closeCommit();
			} catch (Exception e) {
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}
	}
	
	/**
	 * @param dto
	 * @param nomeArquivo
	 * @param tipoAnexo
	 * @return
	 * @throws GrowUpException
	 */
	public PedidoVeiculoDTO importarDados(final PedidoVeiculoDTO dto, final String nomeArquivo, String classname) throws GrowUpException {
		
		validarCamposObrigatorios(dto);
		
		
		File uploadFile = FileUtil.getUploadFile(nomeArquivo);
		try {
			InputStream is = new FileInputStream(uploadFile);

			AnexoRestricaoBuilder builder = AnexoRestricaoFactory.getRestricaoBuilder(classname, is);
			final List<RestricaoDTO> restricoes = builder.build();
			
			// Removendo o item <Nova informa��o>
			dto.getRestricoes().remove( dto.getRestricoes().size() -1 );
			
			if (!restricoes.isEmpty()) {
				
				// Verificando se a placa do anexo corresponde a dto pedido.
				final PedidoVeiculoDTO pedidoRest = restricoes.iterator().next().getPedido();
				/* Pode ser que o anexo n�o tenha como dianosticar a placa do ve�culo.*/
				if (pedidoRest != null && !dto.getVeiculo().equals(pedidoRest.getVeiculo())) {
					throw new GrowUpException(MSGCODE.DADOS_INCOMPATIVEIS_COM_REGISTRO);
				}
				// Concatenando a lista de restricoes
				dto.getRestricoes().addAll(restricoes);

				// Ordenana as restricoes conforme sua regra. 
				Collections.sort(dto.getRestricoes());

				MensagemBuilder.build(MSGCODE.DADOS_IMPORTADO_COM_SUCESSO, dto);
			}
			
		} catch (IOException e) {
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
		} finally {
			FileUtils.deleteQuietly(uploadFile);
		}
		
		
		return dto;
	}

}
