package br.com.growupge.business;

import static br.com.growupge.util.MensagemBuilder.build;

import java.util.ArrayList;
import java.util.List;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.PessoaDTO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.stereotype.FirebirdStereoType;

/**
 * Business object para manipula��o de dados de Usu�rio.
 * 
 * @author eii5
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Pessoa extends NegocioBase<PessoaDTO> {


	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Pessoa() {}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getPessoaDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}

	/**
	 * Este m�todo � respons�vel por validar cpf do usuario.
	 * 
	 * @param value
	 *            Cpf digitado pelo usu�rio.
	 * @return Retorna um objeto do tipo Boolean
	 */
	private /*final*/ boolean isValidoCpf(final String value) {
		int soma = 0;
		String lcpf = value.replace("-", "").replace(".", "");

		try {
			Long.parseLong(lcpf);
		} catch (Exception e) {
			return false;
		}

		if (lcpf.length() == 11) {
			for (int i = 0; i < 9; i++) {
				soma += (10 - i) * (lcpf.charAt(i) - '0');
			}

			soma = 11 - (soma % 11);
			if (soma > 9) {
				soma = 0;
			}
			if (soma == (lcpf.charAt(9) - '0')) {
				soma = 0;
				for (int i = 0; i < 10; i++) {
					soma += (11 - i) * (lcpf.charAt(i) - '0');
				}
				soma = 11 - (soma % 11);
				if (soma > 9) {
					soma = 0;
				}
				if (soma == (lcpf.charAt(10) - '0')) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Verifica se o c�digo j� existe na base de dados
	 * @param dto 
	 * 
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 */
	private /*final*/ void verificarCodigo(PessoaDTO dto) throws GrowUpException {
		// FE3
		PessoaDTO ldto = new PessoaDTO();
		ldto.setUsuario(dto.getUsuario());

		if (this.procurar(ldto) != null) {
			throw new GrowUpException(MSGCODE.PESSOA_INEXISTENTE);
		}

	}

	/**
	 * Verifica se o usu�rio realizou alguma a��o no sistema
	 * 
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 */
	private /*final*/ void verificarAcaoUsuarioSistema() throws GrowUpException {

		// TODO Verificar todas as tabelas onde existir relacionamento com a
		// pessoa
	}

	/**
	 * Valida todos os campos obrigat�rios est�o preenchidos.
	 * 
	 * @param dto
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final PessoaDTO dto) throws GrowUpException {
		// FE4
		List<String> erros = new ArrayList<String>();

		if (dto.getUsuario() == null || dto.getUsuario().getCodigo() == null) {
			erros.add(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
		if (dto.getTipoCargo() == null || dto.getTipoCargo().equals("")) {
			erros.add(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
		if (dto.getTipoDepartamento() == null || dto.getTipoDepartamento().equals("")) {
			erros.add(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
		if (dto.getCpf() == null || dto.getCpf().equals("")) {
			erros.add(MSGCODE.CAMPOS_OBRIGATORIOS);
		}

		if (erros.size() > 0) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}

	}

	/**
	 * Cria uma novo usuario com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es do usuario.
	 * @return Um objeto de transfer�ncia com os dados do usuario criado.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	public /*final*/ PessoaDTO criarPessoa(final PessoaDTO dto) throws GrowUpException {

		// DAOFactory daofactory = daoFactory.getDAOFactory();
		PessoaDTO ldto = null;

		try {
			this.validarCamposObrigatorios(dto);

			this.verificarCodigo(dto);
			
			dto.setCpf( validarCPF(dto.getCpf()) );

			DAO<PessoaDTO> dao = daoFactory.getPessoaDAO();
			ldto = dao.insert(daoFactory.getConnection(), dto);

			ldto = build(MSGCODE.PESSOA_INCLUIDA_COM_SUCESSO, 
						 ldto, 
						 ldto.getUsuario().getNome() );  

			return ldto;

		} catch (GrowUpDAOException e) {
			throw e;
		} finally {
			daoFactory.closeCommit();
		}
	}

	/**
	 * Valido  o cpf e retira a marcara
	 * @throws GrowUpException 
	 */
	private String validarCPF(String cpf) throws GrowUpException {
		if (cpf != null) {
			if (!this.isValidoCpf(cpf)) {
				throw new GrowUpException(MSGCODE.CAMPOS_INVALIDOS);
			}

			cpf = cpf.replace("-", "").replace(".", "");
		}
		
		return cpf;
	}

	/**
	 * Altera dados do usuario com os dados informados no objeto de
	 * transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es do usuario.
	 * @return Um objeto de transfer�ncia com os dados do usuario criado.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	public /*final*/ PessoaDTO alterarPessoa(final PessoaDTO dto) throws GrowUpException {

		PessoaDTO ldto = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {
			// FE2
			this.validarCamposObrigatorios(dto);

			PessoaDTO dtoAntes = new PessoaDTO(dto.getUsuario());
			dtoAntes = this.procurar(dtoAntes);

			DAO<PessoaDTO> dao = daoFactory.getPessoaDAO();
			ldto = dao.update(daoFactory.getConnection(), dto);

			ldto = build (MSGCODE.PESSOA_ALTERADA_COM_SUCESSO, 
							ldto, 
							ldto.getUsuario().getNome()); 

			return ldto;

		} catch (GrowUpDAOException e) {
			daoFactory.rollBack();
			throw e;
		} finally {
			daoFactory.closeCommit();
		}

	}

	/**
	 * Exclui o usuario que cont�m os dados informados no objeto de
	 * transfer�ncia.
	 * 
	 * @param dto
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 * @return Retorna um objeto do tipo PessoaDTO
	 * 
	 */
	public /*final*/ PessoaDTO excluirPessoa(final PessoaDTO dto) throws GrowUpException {

		PessoaDTO ldto = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		try {

			// Se os valores forem v�lidos, atribui a classe.
			this.validarCamposObrigatorios(dto);

			this.verificarAcaoUsuarioSistema();

			DAO<PessoaDTO> dao = daoFactory.getPessoaDAO();
			ldto = dao.delete(daoFactory.getConnection(), dto);

			ldto = build(MSGCODE.PESSOA_REMOVIDA_COM_SUCESSO, 
						ldto , 
						ldto.getUsuario().getNome() ); 

			return ldto;

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} finally {
			daoFactory.closeCommit();
		}
	}

	@Override
	public PessoaDTO verificarExclusaoPrevia(PessoaDTO dto) throws GrowUpException {
		return null;
	}

}
