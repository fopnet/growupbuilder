/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 22/12/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 22/12/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.business;

import static br.com.growupge.util.MensagemBuilder.build;
import static br.com.growupge.utility.Reflection.isCodigoValido;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.ArrayList;
import java.util.List;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.PerfilDTO;
import br.com.growupge.dto.PerfilPermissaoItemDTO;
import br.com.growupge.dto.PermissaoDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.stereotype.FirebirdStereoType;

/**
 * Business object para controle de permiss�es.
 * 
 * @author Felipe
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Permissao extends NegocioBase<PermissaoDTO> {

	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Permissao() {}

	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getPermissaoDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.business.NegocioBase#validarCamposObrigatorios(java.lang.Object)
	 */
	@Override
	protected void validarCamposObrigatorios(final PermissaoDTO dto) throws GrowUpException {
		if (!isCodigoValido(dto)) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
		if (isBlank(dto.getDescricao())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}

	}

	/**
	 * Metodo que verifica se a permissao existe
	 * 
	 * @throws GrowUpException
	 *             Comentar aqui.
	 */
	private void verificarCodigo(PermissaoDTO dto) throws GrowUpException {


		PermissaoDTO permDTO = this.procurar(new PermissaoDTO(dto.getCodigo()));

		if (permDTO != null) {
			throw new GrowUpException(MSGCODE.PERMISSAO_EXISTENTE, 
										build(permDTO.getCodigo()));

		}
	}

	/**
	 * Verifica se a permiss�o informada possui liga��o com algum perfil
	 * existente.
	 * 
	 * @param dto
	 *            Objeto de Transfer�ncia com os dados da Permiss�o.
	 * @throws SigemException
	 *             Caso ocorra algum erro durante a verifica��o.
	 */
	private void verificaAssociacao(PermissaoDTO dto) throws GrowUpException {

		PerfilPermissaoItemDTO item = new PerfilPermissaoItemDTO(dto);

		if (getPerfilBO().buscarTodas(item).size() > 0) {
			// Existe associa��o desta permissao com perfis existentes
			throw new GrowUpException(MSGCODE.PERMISSAO_REGISTROS_ASSOCIADOS, 
									build(dto.getCodigo()));
		}
	}

	/**
	 * M�todo utilizado para a cria��o de uma nova permiss�o.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com os dados da permiss�o a ser
	 *            criada.
	 * @return Objeto de transfer�ncia com os dados da permiss�o.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a cria��o da nova permiss�o.
	 */
	public PermissaoDTO criarPermissao(final PermissaoDTO dto) throws GrowUpException {
		PermissaoDTO newDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {
			/* Valida se campo est� em branco */
			this.validarCamposObrigatorios(dto);

			this.verificarCodigo(dto);

			DAO<PermissaoDTO> dao = daoFactory.getPermissaoDAO();
			newDTO = dao.insert(daoFactory.getConnection(), dto);

			newDTO = build( MSGCODE.PERMISSAO_INCLUIDA_COM_SUCESSO , 
							newDTO, 
							newDTO.getCodigo() );

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			daoFactory.closeCommit();
		}

		return newDTO;

	}

	/**
	 * M�todo utilizado para a alterar a permiss�o informada.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com os dados da permiss�o a ser
	 *            alterada.
	 * @return PermissaoDTO Objeto de transfer�ncia com dados da permiss�o.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a altera��o da permiss�o.
	 */
	public PermissaoDTO alterarPermissao(final PermissaoDTO dto) throws GrowUpException {

		PermissaoDTO newDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		try {

			/* Valida se campo est� em branco */
			this.validarCamposObrigatorios(dto);

			//RN4
			this.verificarExclusaoPrevia(dto);
			
			DAO<PermissaoDTO> dao = daoFactory.getPermissaoDAO();

			newDTO = dao.update(daoFactory.getConnection(), dto);
			
			newDTO =  build(MSGCODE.PERMISSAO_ALTERADA_COM_SUCESSO, 
							newDTO, 
							newDTO.getCodigo());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			daoFactory.closeCommit();
		}
		return newDTO;

	}

	/**
	 * M�todo utilizado para excluir permiss�o que cont�m o c�digo informado no
	 * transfer object.
	 * 
	 * @param dto
	 *            Transfer object que cont�m a permiss�o a ser excluida.
	 * @throws GrowUpException
	 *             Caso ocorra algum problema durante a opera��o de exclus�o.
	 * @return PermissaoDTO Objeto de transfer�ncia com os dados da permiss�o
	 *         exclu�da.
	 */
	public PermissaoDTO excluirPermissao(final PermissaoDTO dto) throws GrowUpException {

		PermissaoDTO newDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {
			this.validarCamposObrigatorios(dto);

			//RN4
			this.verificarExclusaoPrevia(dto);
			
			this.verificaAssociacao(dto);

			DAO<PermissaoDTO> dao = daoFactory.getPermissaoDAO();
			newDTO = dao.delete(daoFactory.getConnection(), dto);

			newDTO =  build(MSGCODE.PERMISSAO_EXCLUIDA_COM_SUCESSO, 
							newDTO, 
							newDTO.getCodigo());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {
			daoFactory.closeCommit();
		}

		return newDTO;
	}

	/**
	 * Retorna somente os codigos das permiss�es para habilitar os menus 
	 *
	 * @param dto Perfil dto
	 * @return
	 * @throws GrowUpException Caso ocorra algum erro na opera��o
	 */
	public List<String> buscarTodas(final PerfilDTO dto) throws GrowUpException {
		List<String> permissoes = new ArrayList<String>();
		
		PerfilPermissaoItemDTO param = new PerfilPermissaoItemDTO();
		param.setPerfil(dto);
		List<PerfilPermissaoItemDTO> itens = getPerfilBO().buscarTodas(param);
		for (PerfilPermissaoItemDTO itemDTO : itens) {
			permissoes.add(itemDTO.getPermissao().getCodigo());
		}
		return permissoes;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public PermissaoDTO verificarExclusaoPrevia(PermissaoDTO dto) throws GrowUpException {

		PermissaoDTO dtoAntes = null;

		dtoAntes = this.procurar(dto);

		if (dtoAntes == null) {
			throw new GrowUpException(
					MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,
					build(dto.getCodigo()) );
		}

		return dtoAntes;
	}
}
