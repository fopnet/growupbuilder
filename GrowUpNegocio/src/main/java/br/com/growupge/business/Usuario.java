package br.com.growupge.business;

import static br.com.growupge.constantes.Constantes.URL_CONFIRMA_EMAIL;
import static br.com.growupge.constantes.Constantes.URL_PEDIDO_USR;
import static br.com.growupge.tradutores.TradutorLabels.LABEL_NOME;
import static br.com.growupge.tradutores.TradutorLabels.LABEL_USUARIO;
import static br.com.growupge.util.MensagemBuilder.build;
import static br.com.growupge.utility.Reflection.isCodigoValido;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.EmailDTO;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.dto.PerfilDTO;
import br.com.growupge.dto.SenhaDTO;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.dto.TramitacaoDTO;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.enums.TipoIdioma;
import br.com.growupge.enums.TipoPerfil;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.BeanFactory;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.HtmlTemplateFactory;
import br.com.growupge.gerenciadores.GerenciadorEmail;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.interfacesdao.UsuarioDAO;
import br.com.growupge.security.BaseFactorySecurity;
import br.com.growupge.stereotype.FirebirdStereoType;
import br.com.growupge.template.HtmlTemplate;
import br.com.growupge.tradutores.Tradutor;
import br.com.growupge.util.MensagemBuilder;
import br.com.growupge.utility.CodificadorUrl;
import br.com.growupge.utility.DateUtil;

/**
 * Business object para manipula��o de dados de Usu�rio.
 * 
 * @author elmt
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Usuario extends NegocioBase<UsuarioDTO> {

	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Usuario() throws GrowUpException {}

	/**
	 * @param dto
	 * @throws GrowUpException
	 */
	public Usuario(final SessaoDTO dto) throws GrowUpException {
		this();
		this.setSessaodto(dto);
	}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getUsuarioDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}

//	public final DateUtil getDataExpiracaoSenha() {
//		// VariavelDTO ldto = (VariavelDTO)
//		// VariavelCache.getInstance().getVariavel(Constantes.VARIAVEL_TEMPO_VALIDADE_SENHA);
//		DateUtil data = this.dataUltimaAlteracaoSenha;
//		if (data != null) {
//			data.add(Constantes.VARIAVEL_TEMPO_VALIDADE_SENHA);
//			return data;
//		} else {
//			return null;
//		}
//	}

	/**
	 * Verifica se o c�digo j� existe na base de dados
	 * 
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 */
	private /*final*/ void verificarLogin(UsuarioDTO dto) throws GrowUpException {
		
		if (!dto.isLoginValido()) {
			Properties t = Tradutor.getInstance(Constantes.LANGUAGE_BR);
			if (dto.getCodigo() == null) {
				throw new GrowUpException(MSGCODE.CAMPOX_OBRIGATORIOS, t.getProperty(LABEL_USUARIO));
			
			} else if (dto.getCodigo().length() < UsuarioDTO.TAMANHO_MINIMO_LOGIN) {
					throw new GrowUpException(MSGCODE.TAMANHO_MINIMO_REQUERIDO, 
											 build(t.getProperty(LABEL_USUARIO), UsuarioDTO.TAMANHO_MINIMO_LOGIN));

			} else if (dto.getCodigo().length() > UsuarioDTO.TAMANHO_MAXIMO_LOGIN) {
				throw new GrowUpException(MSGCODE.TAMANHO_MAXIMO_EXCEDIDO, 
											build(t.getProperty(LABEL_USUARIO), UsuarioDTO.TAMANHO_MAXIMO_LOGIN));
			}
		}

		// FE3
		UsuarioDTO ldto = new UsuarioDTO();
		ldto.setCodigo(dto.getCodigo());

		if (this.procurar(ldto) != null) {
			throw new GrowUpException(MSGCODE.USUARIO_EXISTENTE);
		}

	}

	/**
	 * M�todo que verifica se o usu�rio existe e est� habilitado
	 * 
	 * @param dto
	 * @return Retorna um PessoaDTO completo
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o.
	 */
	public /*final*/ UsuarioDTO verificarUsuario(final UsuarioDTO dto) throws GrowUpException {

		UsuarioDTO usr = getUsuarioBO().procurar(dto);

		// RN2 , RN3 - Recriar Senha
		// RN1 , RN2 - Trocar senha via codigo acesso
		if (usr == null) {
			throw new GrowUpException(MSGCODE.USUARIO_SENHA_INVALIDA);
		} else if (!usr.isUsuarioHabilitado()) {
			throw new GrowUpException(MSGCODE.USUARIO_DESABILITADO, MensagemBuilder.build(dto.getCodigo()));
		}

		return usr;

	}

	/**
	 * Verifica se o usu�rio realizou alguma a��o no sistema
	 * @param dto 
	 * 
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 */
	private /*final*/ void verificarAcaoUsuarioSistema(final UsuarioDTO dto) throws GrowUpException {
		// FE5
		SessaoDTO sessao = new SessaoDTO(new UsuarioDTO(dto.getCodigo()));

		sessao = getSessaoBO().procurar(sessao);
		if (sessao != null) {
			throw new GrowUpException(MSGCODE.USUARIO_EM_USO,
										build(sessao.getUsuario().getCodigo(), 
												"Sess�o:".concat(sessao.getSid())));
		}

		PedidoVeiculoDTO pedido = new PedidoVeiculoDTO();
		pedido.setUsuarioCadastro(dto);
		pedido = getPedidoBO().procurar(pedido);
		if (pedido != null) {
			throw new GrowUpException(MSGCODE.USUARIO_EM_USO, build(pedido.getUsuarioCadastro().getCodigo(), 
																	"Pedido:".concat(String.valueOf(pedido.getCodigo()))));
		}

		/* Usuario criador do veiculo -- A Foreign Key foi exclu�da.
		VeiculoDTO veiculo = new VeiculoDTO();
		veiculo.setUsuarioCadastro(dto);
		veiculo = getVeiculoBO().procurar(veiculo);
		if (veiculo != null) {
			throw new GrowUpException(MSGCODE.USUARIO_EM_USO, build(dto.getCodigo(), 
					"Veiculo:".concat(String.valueOf(veiculo.getCodigo()))));
		}

		// Usuario alteracao do veiculo
		veiculo = new VeiculoDTO();
		veiculo.setUsuarioAlteracao(dto);
		veiculo = getVeiculoBO().procurar(veiculo);
		if (veiculo != null) {
			throw new GrowUpException(MSGCODE.USUARIO_EM_USO, build(dto.getCodigo(), 
					"Veiculo:".concat(String.valueOf(veiculo.getCodigo()))));
		} */
		
		// Usuario criador do veiculo
		TramitacaoDTO pend = new TramitacaoDTO();
		pend.setUsuarioCadastro(dto);
		pend = getTramitacaoBO().procurar(pend);
		if (pend != null) {
			throw new GrowUpException(MSGCODE.USUARIO_EM_USO, build(dto.getCodigo(), 
					"Pend�ncia:".concat(String.valueOf(pend.getCodigo()))));
		}

		pend = new TramitacaoDTO();
		pend.setUsuarioAlteracao(dto);
		pend = getTramitacaoBO().procurar(pend);
		if (pend != null) {
			throw new GrowUpException(MSGCODE.USUARIO_EM_USO, build(dto.getCodigo(), 
					"Pend�ncia:".concat(String.valueOf(pend.getCodigo()))));
		}
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @param dto
	 *            Parametro do tipo SenhaDTO
	 * @throws SigemException
	 *             Caso ocorra algum erro durante a valida��o.
	 */
	protected /*final*/ void validarCamposObrigatorios(final SenhaDTO dto) throws GrowUpException {

		if (dto == null || dto.getUsuario() == null 
				|| isBlank(dto.getValor()) 
				|| isBlank(dto.getUsuario().getSenha())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}

	}

	/**
	 * Valida todos os campos obrigat�rios est�o preenchidos.
	 * 
	 * @param dto
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final UsuarioDTO dto) throws GrowUpException {
		// FE4
		List<String> erros = new ArrayList<String>();

		if (isBlank(dto.getCodigo())) {
			getLogger().info("Codigo do usu�rio est� nulo");
			erros.add(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
		if (isBlank(dto.getNome())) {
			getLogger().info("Nome do usu�rio est� nulo");
			erros.add(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
		if (isBlank(dto.getEmail())) {
			getLogger().info("Email do usu�rio est� nulo");
			erros.add(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
//		if (isBlank(dto.getHabilitado())) {
//			erros.add(MSGCODE.CAMPOS_OBRIGATORIOS);
//		}
//		if (isBlank(dto.getIdioma())) {
//			erros.add(MSGCODE.CAMPOS_OBRIGATORIOS);
//		}
		
		if (erros.size() > 0) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}

	}

	/**
	 * Gera o login a partir do email.
	 */
	private void gerarLoginPeloEmail(UsuarioDTO dto) {

		if (isNotBlank(dto.getEmail()) && !dto.isLoginValido()) {
			Pattern p = Pattern.compile("(.){3,"+ UsuarioDTO.TAMANHO_MAXIMO_LOGIN  +"}(?=@)");
			Matcher matcher = p.matcher(dto.getEmail());
			if (matcher.find()) {
				dto.setCodigo(matcher.group().trim());
				getLogger().info("Codigo do usu�rio gerado:" + dto.getCodigo());
			}
		}
	}
	
	/**
	 * Cria uma novo usuario com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es do usuario.
	 * @return Um objeto de transfer�ncia com os dados do usuario criado.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	public /*final*/ UsuarioDTO criarUsuario(final UsuarioDTO dto) throws GrowUpException {

		//DAOFactory daofactory = daoFactory.getDAOFactory();
		UsuarioDTO retorno = null;
		HistoricoSenha historico = null;
		boolean isConfirmarEmail = false;

		try {
			
			gerarLoginPeloEmail(dto);
			
			if (dto.getEmpresa() == null) {
				dto.setEmpresa(getEmpresaBO().procurarGrowupVeiculos());
			}
			
			// RN 3 - Se os valores forem v�lidos, atribui a classe.
			this.validarCamposObrigatorios(dto);
			
			verificarEmail(dto);
			
			if (isBlank(dto.getIdioma()))
				dto.setIdioma(TipoIdioma.IDIOMA_BR.toString());
			
			// RN 5
			this.verificarLogin(dto);
			
			if (dto.isNomeValido() ) {
				Properties t = Tradutor.getInstance(Constantes.LANGUAGE_BR);
				throw new GrowUpException(MSGCODE.TAMANHO_MINIMO_REQUERIDO, 
						build(t.getProperty(LABEL_NOME), UsuarioDTO.TAMANHO_MINMO_NOME));
			}
			

//			historico = new HistoricoSenha(dto);
			historico = getHistoricoSenhaBO();
			historico.setUsuario(dto);
			
			dto.setSenha(historico.gerarSenha());
			dto.setQtdRegistros(Constantes.VARIAVEL_MAX_REGISTROS_RETORNADOS);
			dto.setDataExpiracaoSenha(DateUtil
					.getDateInstance(Constantes.LANGUAGE_BR).getDate("01-01-1900 00:00:00"));
			/*
			 * Se o usu�rio n�o possuir perfil, � porque veio da tela do portal
			 * e foi criado pelo pr�prio usu�rio
			 */
			if (dto.getPerfil() == null) {
				dto.setPerfil(new PerfilDTO());
				// Adiciona o nivel mais baixo do perfil de usuario
				dto.getPerfil().setCodigo(TipoPerfil.USUARIO.toString().concat("9"));
				dto.setHabilitado(Constantes.NAO);// o usu�rio ainda precisa confirmar o email.
				
				isConfirmarEmail = true;
			}

			DAO<UsuarioDTO> dao = daoFactory.getUsuarioDAO();
			retorno = dao.insert(daoFactory.getConnection(), dto);

			/* Caso o usu�rio logado tenha algum perfil de usu�rio, notificar o administrador da sua empresa.
			if (isPerfilUsuarioInferiorAo(TipoPerfil.EMPRESA, dto.getPerfil())) {
				EmailDTO mail = new EmailDTO();
				mail.empresa = dto.getEmpresa();
				mail.usuario = dto;
				mail.assunto = dto.getEmpresa().getNome().concat(" - Notifica��o de novo usu�rio");
				this.notificarAdmin(mail);
			}*/
			
			if (isConfirmarEmail) {
				EmailDTO mail = new EmailDTO(retorno);
				mail.setAssunto(retorno.getEmpresa().getNome().concat(" - Confirma��o do endere�o de email"));
				this.notificarConfirmacaoEmail(mail);
			} else {
				// Gerando o historico da senha gerada acima
				historico.enviarSenhaPorEmail();
			}
				

			retorno = MensagemBuilder.build(MSGCODE.USUARIO_INCLUIDO_COM_SUCESSO 
											,retorno 
											,retorno.getNome() 
											,retorno.getCodigo());

			return retorno;

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(MSGCODE.ERRO_ENVIO_EMAIL, e);
		} finally {
			daoFactory.closeCommit();
		}
	}

	private void verificarEmail(final UsuarioDTO dto) throws GrowUpException {
		if (!dto.isEmailValido()) {
			throw new GrowUpException(MSGCODE.USUARIO_EMAIL_INVALIDO);
		}
		
		// FE3
		UsuarioDTO ldto = new UsuarioDTO();
		ldto.setEmail(dto.getEmail());
		ldto = procurar(ldto);
		
		if (ldto != null) {
			// Duplica��o ao inserir
			if (!ldto.getCodigo().equals(dto.getCodigo())) {
				throw new GrowUpException(MSGCODE.USUARIO_EMAIL_EXISTENTE);
			}
		}
	}

	/**
	 * Altera dados do usuario com os dados informados no objeto de
	 * transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es do usuario.
	 * @return Um objeto de transfer�ncia com os dados do usuario criado.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	public /*final*/ UsuarioDTO alterarUsuario(final UsuarioDTO dto) throws GrowUpException {

		// DAOFactory daofactory =  daoFactory.getDAOFactory();
		UsuarioDTO dtoGravado = null;

		try {
			// FE2
			this.validarCamposObrigatorios(dto);

			if (!isCodigoValido(dto.getPerfil())) {
				throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
			}

			verificarEmail(dto);
			
			//RN4
			this.verificarExclusaoPrevia(dto);

			DAO<UsuarioDTO> dao = daoFactory.getUsuarioDAO();
			dtoGravado = dao.update(daoFactory.getConnection(), dto);

			dtoGravado = MensagemBuilder.build(MSGCODE.USUARIO_ALTERADO_COM_SUCESSO, dtoGravado, 
										 dtoGravado.getNome(), dtoGravado.getCodigo());

			return dtoGravado;

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);

		} finally {
			daoFactory.closeCommit();
		}

	}

	/**
	 * Exclui o usuario que cont�m os dados informados no objeto de
	 * transfer�ncia.
	 * 
	 * @param dto
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 * @return Retorna um objeto do tipo PessoaDTO
	 * 
	 */
	public /*final*/ UsuarioDTO excluirUsuario(final UsuarioDTO dto) throws GrowUpException {

		// DAOFactory daofactory =  daoFactory.getDAOFactory();
		UsuarioDTO dtoGravado = null;
		try {

			// Se os valores forem v�lidos, atribui a classe.
			if (isBlank(dto.getCodigo())) {
				throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
			}
			
			//RN4
			this.verificarExclusaoPrevia(dto);

			//RN5
			this.verificarAcaoUsuarioSistema(dto);

			DAO<UsuarioDTO> dao = daoFactory.getUsuarioDAO();
			dtoGravado = dao.delete(daoFactory.getConnection(), dto);

			dtoGravado = MensagemBuilder.build(MSGCODE.USUARIO_REMOVIDO_COM_SUCESSO, dtoGravado, 
											dtoGravado.getNome(), 
											dtoGravado.getCodigo());

			return dtoGravado;

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			daoFactory.closeCommit();
		}
	}

	// Senha

	/**
	 * Metodo responsavel por reCriar uma nova senha de acesso ao Sigem para o
	 * usu�rio.
	 * 
	 * @param dto
	 *            parametro do tipo PessoaDTO
	 * @return Retorna um objeto do tipo SenhaDTO
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o.
	 */
	public /*final*/ UsuarioDTO recriarSenha(final UsuarioDTO dto) throws GrowUpException {
		// DAOFactory daofactory = daoFactory.getDAOFactory();
		UsuarioDTO usr = null;
		HistoricoSenha historico = null;

		try {
			if (!dto.isEmailValido() && !dto.isLoginValido()) {
				throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
			}

			usr = this.verificarUsuario(dto);

//			historico = new HistoricoSenha(usr);
			historico = getHistoricoSenhaBO();
			historico.setUsuario(usr);
			
			usr.setSenha(historico.gerarSenha());
			usr.setDataUltimaAlteracaoSenha(DateUtil.getDateManagerInstance().getSQLTime());

			
			UsuarioDAO dao = daoFactory.getUsuarioDAO();
			usr = dao.updatePassword(daoFactory.getConnection(), usr);
			/* Alterando ultima data altera��o senha */
			usr = dao.update(daoFactory.getConnection(), usr);

			// Enviando Senha
			historico.enviarSenha(usr.getEmpresa().getNome().concat(" - Senha Recriada"), null);

			usr.codMensagem = MSGCODE.SENHA_ALTERADA_COM_SUCESSO;

			return usr;

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(MSGCODE.ERRO_ENVIO_EMAIL, e.getMessage(), e.getMessage(), e
					.getMessage());
		} finally {
			daoFactory.closeCommit();
		}
	}

	/**
	 * verifica se senha atual confere com a armazenada no registro do usu�rio
	 * verifica se senha nova confere com a armazenada no registro do usu�rio
	 * 
	 * @param dto
	 *            parametro do tipo SenhaDTO
	 * @return 
	 * @throws SigemException
	 *             Caso ocorra algum erro durante a opera��Ro.
	 */
	private UsuarioDTO validarSenha(final SenhaDTO dto) throws GrowUpException {
		// verifica se senha atual confere com a armazenada no registro do usuario
		//Buscar o usuario por codigo ou email
		UsuarioDTO retorno = new UsuarioDTO(dto.getUsuario().getCodigo());
		retorno.setEmail(dto.getUsuario().getEmail());  
		retorno = this.procurar(retorno);

		BaseFactorySecurity bfs = BaseFactorySecurity.getInstance(Constantes.SECURITY_SHA);
		String senhaAtualDigitada = bfs.criptografar(dto.getUsuario().getSenha());

		if (retorno != null) {
			if (!retorno.getSenha().equals(senhaAtualDigitada)) {
				throw new GrowUpException(MSGCODE.SENHA_NAO_CONFERE);
			}

			String senhaNova = bfs.criptografar(dto.getValor());
			if (retorno.getSenha().equals(senhaNova)) {
				throw new GrowUpException(MSGCODE.SENHA_IGUAL_ANTERIOR);
			}
		}
		
		return retorno;
	}

	/**
	 * Metodo responsavel pela altera��o da senha do usuario
	 * 
	 * @param dto
	 *            parametro do tipo SenhaDTO
	 * @return Retorna um objeto do tipo SenhaDTO
	 * @throws SigemException
	 *             Caso ocorra algum erro durante a opera��o.
	 */
	public /*final*/ SenhaDTO trocarSenha(final SenhaDTO dto) throws GrowUpException {

		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		SenhaDTO senhaDTO = null;

		try {
			this.validarCamposObrigatorios(dto);
			UsuarioDTO usrConectado = this.validarSenha(dto);

			// Criptografar a senha do usuario
			dto.getUsuario().setSenha(usrConectado.getSenha());

			// Criptografar a nova senha
//			senhaDTO = historico.alterarHistorico(dto);

			dto.setUsuario(usrConectado);
			dto.getUsuario().setDataUltimaAlteracaoSenha(DateUtil.getDateManagerInstance().getDateTime());

//			HistoricoSenha historico = new HistoricoSenha(dto.getUsuario());
			HistoricoSenha historico = BeanFactory.getContextualInstance(HistoricoSenha.class);;
			historico.setUsuario(dto.getUsuario());
			
			// Atribuir a senha n�o criptografada, para enviar por email
			historico.setSenha(dto.getValor());
			// Atribui nova senha criptograda para alterar no banco 
			dto.getUsuario().setSenha(historico.getSenhaCript());
			
			UsuarioDAO dao = daoFactory.getUsuarioDAO();
			dao.updatePassword(daoFactory.getConnection(), dto.getUsuario());

			senhaDTO = build(MSGCODE.SENHA_ALTERADA_COM_SUCESSO, new SenhaDTO(dto.getUsuario()));
//			senhaDTO.setUsuario(dao.update(daoFactory.getConnection(), dto.getUsuario()));
			
			historico.enviarSenhaPorEmail();

			return senhaDTO;
		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			try {
				daoFactory.closeCommit();
			} catch (Exception e) {
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}

	}

	/**
	 * M�todo envia email para o administrador da empresa, solicitando
	 * habilita��o do usu�rio
	 * 
	 * @param dto
	 *            Parametro do tipo EmailDTO
	 * @return
	 * @throws GrowUpException
	 */
	public /*final*/ EmailDTO notificarUsuarioNaoHabilitado(final EmailDTO mail) throws GrowUpException {

		GerenciadorEmail gerenciador = GerenciadorEmail.getInstance();

		// Obtem o Template com base no idioma do usu�rio
		HtmlTemplate templateEmail = HtmlTemplateFactory.getTemplateNovoUsuario();

		// Adiciona os parametros que dever�o ser substituidos no template
		templateEmail.adicionarParametro(HtmlTemplate.PARAM_USR, mail.getUsuario().getCodigo());

		// Executa o template
		templateEmail.execute();

		try {
			gerenciador.enviarEmail(
					mail.getAssunto(),
					templateEmail.getHtmlTemplate(),
					mail.getUsuario(),
					mail.getEmpresa());
			return mail;
		} catch (MessagingException ex) {
			throw new GrowUpException(MSGCODE.ERRO_ENVIO_EMAIL, ex.getMessage(), "", ex
					.getMessage());
		} catch (GrowUpException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new GrowUpException(
					MSGCODE.ERRO_ENVIO_EMAIL,
					ex.getMessage(),
					ex.getMessage(),
					ex.getMessage());
		}
	}

	/**
	 * M�todo envia email para o administrador da empresa, solicitando
	 * habilita��o do usu�rio
	 * @param dto
	 *            Parametro do tipo EmailDTO
	 * @return
	 * @throws GrowUpException
	 */
	public /*final*/ EmailDTO notificarConfirmacaoEmail(final EmailDTO mail) throws GrowUpException {

		GerenciadorEmail gerenciador = GerenciadorEmail.getInstance();

		// Obtem o Template com base no idioma do usu�rio
		HtmlTemplate templateEmail = HtmlTemplateFactory.getTemplateConfirmarEmail();
		// Adiciona os parametros que dever�o ser substituidos no template
		templateEmail.adicionarParametro(HtmlTemplate.PARAM_USR, mail.getUsuario().getNome());
		
		String link = CodificadorUrl.criarUrlPedido(URL_CONFIRMA_EMAIL)
									.codificadorLogin( mail.getUsuario().getCodigo())
									.codificadorEmail( mail.getUsuario().getEmail())
									.getUrl();

		templateEmail.adicionarParametro(HtmlTemplate.PARAM_LINK, link);

		// Executa o template
		templateEmail.execute();

		try {
			gerenciador.enviarEmail(
					mail.getAssunto(),
					templateEmail.getHtmlTemplate(),
					mail.getEmpresa(),
					mail.getUsuario());
			return mail;
		} catch (MessagingException ex) {
			throw new GrowUpException(MSGCODE.ERRO_ENVIO_EMAIL, ex.getMessage(), 
										"", 
										ex.getMessage());
		} catch (GrowUpException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new GrowUpException(
					MSGCODE.ERRO_ENVIO_EMAIL,
					ex.getMessage(),
					ex.getMessage(),
					ex.getMessage());
		}
	}

	public /*final*/ List<UsuarioDTO> buscarProximas(final UsuarioDTO dto) throws GrowUpException {
		// DAOFactory daoFactory = daoFactory.getDAOFactory();

		try {
			
			UsuarioDAO dao = daoFactory.getUsuarioDAO();
			return dao.selectAll(
					daoFactory.getConnection(),
					false,
					Constantes.DESCONSIDERAR_PAGINACAO,
					dao.setParamPage(dto));

		} catch (GrowUpDAOException e) {
			throw e;
		} catch (Exception e) {
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
		} finally {
			daoFactory.closeCommit();
		}
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public UsuarioDTO verificarExclusaoPrevia(UsuarioDTO dto) throws GrowUpException {
		UsuarioDTO dtoAntes = null;

		dtoAntes = this.procurar(new UsuarioDTO(dto.getCodigo()));

		if (dtoAntes == null) {
			throw new GrowUpException(
					MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,
					MensagemBuilder.build(dto.getCodigo()));
		}

		return dtoAntes;
	}
	
	/**
	 * @param dto
	 * @return
	 * @throws GrowUpException
	 */
	public UsuarioDTO habilitarUsuario(UsuarioDTO dto) throws GrowUpException {
		UsuarioDTO dtoGravado = null;
		UsuarioDTO dtoAntes = null;

		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		try {
			// FE2
			if (isBlank(dto.getCodigo())) {
				Properties t = Tradutor.getInstance(Constantes.LANGUAGE_BR);
				throw new GrowUpException(MSGCODE.CAMPOX_OBRIGATORIOS,
										  build(t.getProperty(LABEL_USUARIO)));
			}
			
			//RN4
			dtoAntes = this.verificarExclusaoPrevia(dto);
			
			if (dtoAntes.isUsuarioHabilitado()) {
				throw new GrowUpException(MSGCODE.USUARIO_JA_HABILITADO);
			}
			
			dtoAntes.setHabilitado(Constantes.SIM);
			
//			HistoricoSenha historico = new HistoricoSenha(dtoAntes);
			HistoricoSenha historico = getHistoricoSenhaBO();
			historico.setUsuario(dtoAntes);
			
			dtoAntes.setSenha(historico.gerarSenha());
			dtoAntes.setDataUltimaAlteracaoSenha(DateUtil.getDateManagerInstance().getDateTime());
			dtoAntes.setQtdRegistros(Constantes.VARIAVEL_MAX_REGISTROS_RETORNADOS);
			dtoAntes.setDataExpiracaoSenha(DateUtil
					.getDateInstance(Constantes.LANGUAGE_BR).getDate("01-01-1900 00:00:00"));

			UsuarioDAO dao = daoFactory.getUsuarioDAO();
			dtoGravado = dao.update(daoFactory.getConnection(), dtoAntes);
			dtoGravado = dao.updatePassword(daoFactory.getConnection(), dtoGravado);

			// Gerando o historico da senha gerada acima
			historico.enviarSenhaPorEmail();
			
			// Obtem o Template para o determinado fim
			HtmlTemplate templateEmail = HtmlTemplateFactory.getTemplateUsuarioHabilitado();

			// Adiciona os parametros que dever�o ser substituidos no template
			templateEmail.adicionarParametro(HtmlTemplate.PARAM_USR, dtoGravado.getCodigo());
			templateEmail.adicionarParametro(HtmlTemplate.PARAM_PERFIL, dtoGravado.getPerfil().getDescricao());
			
			String link = CodificadorUrl.criarUrlPedido(URL_PEDIDO_USR)
							.codificadorLogin(dtoGravado.getCodigo())
							.codificadorEmail(dtoGravado.getEmail())
							.getUrl();
			
			templateEmail.adicionarParametro(HtmlTemplate.PARAM_LINK_PEDIDO_USR, link );
			templateEmail.adicionarParametro(HtmlTemplate.PARAM_LINK, Constantes.URL_SITE);

			// Executa o template
			templateEmail.execute();

//			dtoGravado = MessageBuilder.build(MSGCODE.USUARIO_HABILITADO_SUCESSO, dtoGravado, dtoGravado.getCodigo());
			dtoGravado.mensagemRetorno = templateEmail.getHtmlTemplate();

			return dtoGravado;

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			try {
				daoFactory.closeCommit();
			} catch (Exception e) {
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}
	}

}
