package br.com.growupge.business;

import static br.com.growupge.util.MensagemBuilder.build;
import static org.apache.commons.lang3.StringUtils.isBlank;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.dto.VariavelDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.stereotype.FirebirdStereoType;

/**
 * Business object para Variavel.
 * 
 * @author Felipe
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Variavel extends NegocioBase<VariavelDTO> {

	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Variavel() {}

	/**
	 * @param sessaodto
	 * @throws GrowUpException 
	 */
	public Variavel(SessaoDTO sessaodto) throws GrowUpException {
		this();
		setSessaodto(sessaodto);
	}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getVariavelDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}


	/**
	 * Método utilizado para verificar se o nome da Variavel existe.
	 * @param dto2 
	 * 
	 * @throws GrowUpException
	 *             Caso o nome da Variavel não exista.
	 */
	private /*final*/ void verificarCodigo(VariavelDTO dto) throws GrowUpException {

		// RN3
		VariavelDTO ldto = new VariavelDTO(dto.getCodigo());
		ldto = this.procurar(dto);

		// Nome existe
		if (ldto != null) {
			// Duplicação ao inserir
			throw new GrowUpException(MSGCODE.MARCA_EXISTENTE, build(dto.getCodigo()));
		}

	}

	/**
	 * Método sobrescrito.
	 * 
	 * @see br.com.growupge.business.NegocioBase#validarCamposObrigatorios(java.lang.Object)
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final VariavelDTO dto) throws GrowUpException {

		// RN2
		if (isBlank(dto.getCodigo())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
	}

	/**
	 * Cria uma nova Variavel com os dados informados no objeto de transferência.
	 * 
	 * @param dto
	 *            Objeto de transferência com as informações da Variavel
	 * @return Um objeto de transferência com os dados da Variavel criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a operação de criação.
	 */
	public /*final*/ VariavelDTO criarVariavel(final VariavelDTO dto) throws GrowUpException {

		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		VariavelDTO ret = null;

		try {
			// Se os valores forem válidos, atribui a classe.
			this.validarCamposObrigatorios(dto);

			// RN3
			this.verificarCodigo(dto);

			DAO<VariavelDTO> dao = daoFactory.getVariavelDAO();
			ret = dao.insert(daoFactory.getConnection(), dto);

			ret = build(MSGCODE.MARCA_INCLUIDO_COM_SUCESSO, ret, ret.getCodigo()); 

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			daoFactory.closeCommit();
		}
		return ret;
	}

	/**
	 * Alterar a Variavel com os dados informados no objeto de transferência.
	 * 
	 * @param dto
	 *            Objeto de transferência com as informações da Variavel
	 * @return Um objeto de transferência com os dados da Variavel alterada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a operação de alteração.
	 */
	public /*final*/ VariavelDTO alterarMarca(final VariavelDTO dto) throws GrowUpException {

		VariavelDTO ret = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {

			// RN2
			this.validarCamposObrigatorios(ret);

			//RN4
			this.verificarExclusaoPrevia(ret);

			DAO<VariavelDTO> dao = daoFactory.getVariavelDAO();
			ret = dao.update(daoFactory.getConnection(), ret);

			ret = build(MSGCODE.MARCA_ALTERADO_COM_SUCESSO, ret, ret.getCodigo()); 

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			daoFactory.closeCommit();
		}

		return ret;
	}

	/**
	 * Exclui a Variavel que contém os dados informados no objeto de
	 * transferência.
	 * 
	 * @param dto
	 *            Objeto de transferência com as informações da Variavel
	 * @return Um objeto de transferência com os dados da Variavel criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a operação de exclusão.
	 */
	public /*final*/ VariavelDTO excluirVariavel(final VariavelDTO dto) throws GrowUpException {

		VariavelDTO retDTO = null;
		// DAOFactory daofactory = daoFactory.getDAOFactory();

		try {

			//RN4
			this.verificarExclusaoPrevia(dto);
			
			// RN1
//			this.verificarUso(dto);

			
			DAO<VariavelDTO> dao = daoFactory.getVariavelDAO();
			retDTO = dao.delete(daoFactory.getConnection(), dto);

			retDTO = build(MSGCODE.MARCA_REMOVIDO_COM_SUCESSO, retDTO, retDTO.getCodigo()) ;

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {
			daoFactory.closeCommit();
		}

		return retDTO;
	}

	/**
	 * Método sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public VariavelDTO verificarExclusaoPrevia(VariavelDTO dto) throws GrowUpException {
		VariavelDTO dtoAntes = null;

		dtoAntes = this.procurar(dto);

		if (dtoAntes == null) {
			throw new GrowUpException(
					MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,
					build(dto.getCodigo()));
		}

		return dtoAntes;
	}
	
	/**
	 * @return
	 * @throws GrowUpException
	 */
	public VariavelDTO buscarValorUnitario() throws GrowUpException {
		if (super.isUsuarioAdmin()) {
			return VariavelDTO.VALOR_UNITARIO_ADMIN;
		}
			
		return super.procurar(VariavelDTO.VALOR_UNITARIO_PEDIDO);
	}
	
}
