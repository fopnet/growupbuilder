package br.com.growupge.business;

import static br.com.growupge.util.MensagemBuilder.build;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.HashMap;
import java.util.List;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.MarcaDTO;
import br.com.growupge.dto.ModeloDTO;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.stereotype.FirebirdStereoType;
import br.com.growupge.util.MensagemBuilder;

/**
 * Business object para Marca.
 * 
 * @author Felipe
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Marca extends NegocioBase<MarcaDTO> {

	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Marca() {}

	/**
	 * @param sessaodto
	 * @throws GrowUpException 
	 */
	public Marca(SessaoDTO sessaodto) throws GrowUpException {
		this();
		setSessaodto(sessaodto);
	}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getMarcaDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}

	/**
	 * M�todo utilizado para verificar se o nome da Marca existe.
	 * @param dto2 
	 * 
	 * @throws GrowUpException
	 *             Caso o nome da Marca n�o exista.
	 */
	private /*final*/ void verificarNome(MarcaDTO dto) throws GrowUpException {

		// RN3
		MarcaDTO ldto = new MarcaDTO();
		ldto.setNome( dto.getNome() );
		ldto = this.procurar(dto);

		// Nome existe
		if (ldto != null) {
			// Duplica��o ao inserir
			throw new GrowUpException(MSGCODE.MARCA_EXISTENTE, build(dto.getNome()));
		}

	}

	/**
	 * Verificar se os dados da Marca informada est�o cadastrados na base.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia que cont�m as informa��es da Marca
	 * @throws GrowUpException
	 *             Caso ocorra o dado n�o seja encontrado na base.
	 */
	private /*final*/ void verificarUso(final MarcaDTO dto) throws GrowUpException {
		// RN1
		ModeloDTO mod = new ModeloDTO();
		mod.setMarca(dto);

		if (getModeloBO().buscarTodas(mod).size() > 0) {
			throw new GrowUpException(MSGCODE.MARCA_EM_USO, MensagemBuilder.build(dto.getNome()));
		}
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.business.NegocioBase#validarCamposObrigatorios(java.lang.Object)
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final MarcaDTO dto) throws GrowUpException {

		// RN2
		if (isBlank(dto.getNome())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
	}

	/**
	 * Cria uma nova Marca com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Marca
	 * @return Um objeto de transfer�ncia com os dados da Marca criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	public /*final*/ MarcaDTO criarMarca(final MarcaDTO dto) throws GrowUpException {

		// DAOFactory daofactory = daoFactory.getDAOFactory();
		MarcaDTO dtoCliente = null;

		try {
			// Se os valores forem v�lidos, atribui a classe.
			this.validarCamposObrigatorios(dto);

			// RN3
			this.verificarNome(dto);

			DAO<MarcaDTO> dao = daoFactory.getMarcaDAO();
			dtoCliente = dao.insert(daoFactory.getConnection(), dto);

			dtoCliente.codMensagem = MSGCODE.MARCA_INCLUIDO_COM_SUCESSO;
			dtoCliente.parametros = new HashMap<String, String>();
			dtoCliente.parametros.put(Constantes.P1, dtoCliente.getNome());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
		} finally {
			daoFactory.closeCommit();
		}
		return dtoCliente;
	}

	/**
	 * Alterar a Marca com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Marca
	 * @return Um objeto de transfer�ncia com os dados da Marca alterada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de altera��o.
	 */
	public /*final*/ MarcaDTO alterarMarca(final MarcaDTO dto) throws GrowUpException {

		MarcaDTO MarcaDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {

			// RN2
			this.validarCamposObrigatorios(dto);

			//RN4
			this.verificarExclusaoPrevia(dto);

			DAO<MarcaDTO> dao = daoFactory.getMarcaDAO();
			MarcaDTO = dao.update(daoFactory.getConnection(), dto);

			MarcaDTO.codMensagem = MSGCODE.MARCA_ALTERADO_COM_SUCESSO;
			MarcaDTO.parametros = new HashMap<String, String>();
			MarcaDTO.parametros.put(Constantes.P1, MarcaDTO.getNome());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {

			try {

				daoFactory.closeCommit();

			} catch (Exception e) {
				
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}

		return MarcaDTO;
	}

	/**
	 * Exclui a Marca que cont�m os dados informados no objeto de
	 * transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Marca
	 * @return Um objeto de transfer�ncia com os dados da Marca criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 */
	public /*final*/ MarcaDTO excluirMarca(final MarcaDTO dto) throws GrowUpException {

		// DAOFactory daofactory = daoFactory.getDAOFactory();
		MarcaDTO retDTO = null;

		try {

			//RN4
			this.verificarExclusaoPrevia(dto);
			
			// RN1
			this.verificarUso(dto);

			DAO<MarcaDTO> dao = daoFactory.getMarcaDAO();
			retDTO = dao.delete(daoFactory.getConnection(), dto);

			retDTO.codMensagem = MSGCODE.MARCA_REMOVIDO_COM_SUCESSO;
			retDTO.parametros = new HashMap<String, String>();
			retDTO.parametros.put(Constantes.P1, retDTO.getNome());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {
			daoFactory.closeCommit();
		}

		return retDTO;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public MarcaDTO verificarExclusaoPrevia(MarcaDTO dto) throws GrowUpException {
		MarcaDTO dtoAntes = null;

		dtoAntes = this.procurar(dto);

		if (dtoAntes == null) {
			throw new GrowUpException(
					MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,
					build(dto.getNome()));
		}

		return dtoAntes;
	}

	/**
	 * Metodo sem sid para ao solicitar o pedido, poder escolher a marca
	 * @param dto
	 * @return
	 * @throws GrowUpException
	 */
	public List<MarcaDTO> buscarMarcas(MarcaDTO dto) throws GrowUpException {
		return super.buscarTodas(dto);
	}
	
	

}
