package br.com.growupge.business;

import br.com.growupge.dto.DTOPadrao;
import br.com.growupge.exception.GrowUpException;

public abstract class NegocioGrupoBase<DTO, DTORel extends DTOPadrao> extends
		ConsultaGrupoBase<DTO, DTORel> {

//	protected NegocioGrupoBase(ConsultaDAO<DTO> daoCrud, ConsultaDAO<DTORel> daoGrupoCrud) {
//		super(daoCrud, daoGrupoCrud);
//	}

	/**
	 * M�todo utilizado para verificar se os valores informados no objeto de
	 * transfer�ncia est�o com dados v�lidos.
	 * 
	 * @param dto
	 *            Objecto de transfer�ncia que cont�m os valores que ser�o
	 *            validados.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a verifica��o dos dados do
	 *             dto.
	 */
	protected abstract void validarCamposObrigatorios(DTORel dto) throws GrowUpException;

	/**
	 * Popula os atributos da classe relacionada ao objeto de transfer�ncia
	 * informado, com os dados que s�o fornecidos no dto.
	 * 
	 * @param dto
	 *            Inst�ncia do transfer object que cont�m os valores a serem
	 *            configurados.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a atribui��o dos valores
	 *             informados.
	 */
//	protected abstract void setDados(DTORel dto) throws GrowUpException;

	/**
	 * M�todo utilizado para obter dados de um objeto de neg�cio espec�fico.
	 * 
	 * @return Objeto de transfer�ncia populado com os dados validados.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o.
	 */
//	public abstract DTORel getDadosItem() throws GrowUpException;

}
