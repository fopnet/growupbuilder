package br.com.growupge.business;

import static br.com.growupge.util.MensagemBuilder.build;
import static br.com.growupge.utility.Reflection.isCodigoValido;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.List;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.AnoModeloDTO;
import br.com.growupge.dto.ModeloDTO;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.dto.VeiculoDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.fbirdDAO.CollectionSynchronizePersistence;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.stereotype.FirebirdStereoType;

/**
 * Business object para Modelo.
 * 
 * @author Felipe
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Modelo extends NegocioBase<ModeloDTO> {

	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Modelo() {}
	
	/**
	 * @param sessaodto
	 * @throws GrowUpException 
	 */
	public Modelo(SessaoDTO sessaodto) throws GrowUpException {
		this();
		setSessaodto(sessaodto);
	}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getModeloDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}

	/**
	 * M�todo utilizado para verificar se o nome da Modelo existe.
	 * @param dto2 
	 * 
	 * @throws GrowUpException
	 *             Caso o nome da Modelo n�o exista.
	 */
	private /*final*/ void verificarNome(ModeloDTO dto) throws GrowUpException {

		// RN3
		ModeloDTO ldto = new ModeloDTO();
		ldto.setNome(dto.getNome());
		ldto = this.procurar(dto);

		// Nome existe
		if (ldto != null) {

			// Duplica��o ao inserir
			if (!isCodigoValido( dto ) || !ldto.getCodigo().equals(dto.getCodigo())) {
				throw new GrowUpException(MSGCODE.MODELO_EXISTENTE, build(dto.getNome()));
			}
		}

	}

	/**
	 * Verificar se os dados da Modelo informada est�o cadastrados na base.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia que cont�m as informa��es da Modelo
	 * @throws GrowUpException
	 *             Caso ocorra o dado n�o seja encontrado na base.
	 */
	private /*final*/ void verificarUso(final ModeloDTO dto) throws GrowUpException {
		// RN1
		VeiculoDTO ldto = new VeiculoDTO();
		ldto.setModelo( new ModeloDTO());
		ldto.getModelo().setCodigo(dto.getCodigo());

		if (getVeiculoBO().procurar(ldto) != null) {
			throw new GrowUpException(MSGCODE.MODELO_EM_USO, build(dto.getNome()));
		}
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.business.NegocioBase#validarCamposObrigatorios(java.lang.Object)
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final ModeloDTO dto) throws GrowUpException {

		// RN2
		if (isBlank(dto.getNome())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
		
	}

	/**
	 * Cria uma nova Modelo com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Modelo
	 * @return Um objeto de transfer�ncia com os dados da Modelo criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	public /*final*/ ModeloDTO criarModelo(final ModeloDTO dto) throws GrowUpException {

		ModeloDTO retDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {
			// Se os valores forem v�lidos, atribui a classe.
			this.validarCamposObrigatorios(dto);

			// RN3
			this.verificarNome(dto);

			DAO<ModeloDTO> dao = daoFactory.getModeloDAO();
			retDTO = dao.insert(daoFactory.getConnection(), dto);
			
			salvarAnos(retDTO, daoFactory);

			retDTO = build(	MSGCODE.MARCA_INCLUIDO_COM_SUCESSO, 
					retDTO,
					retDTO.getNome());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {

			try {

				daoFactory.closeCommit();

			} catch (Exception e) {
				
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}
		return retDTO;
	}
	
	/**
	 * @param dto
	 * @param daofactory
	 * @throws GrowUpDAOException
	 * @throws GrowUpException
	 */
	protected void salvarAnos(final ModeloDTO dto, DAOFactory daofactory) throws GrowUpDAOException, GrowUpException {
		DAO<AnoModeloDTO> anoDAO = daoFactory.getAnoModeloDAO();
		List<AnoModeloDTO> itensGravados = anoDAO.selectAll(daoFactory.getConnection(), 
															new AnoModeloDTO(dto));
		CollectionSynchronizePersistence<AnoModeloDTO> 
		sync = new CollectionSynchronizePersistence<AnoModeloDTO>(daofactory, anoDAO) {
			@Override
			protected void beforeInsert(final AnoModeloDTO item) throws GrowUpException {
				item.setModelo(dto);
			}
			
		};
		sync.synchonize(dto.getAnos(), itensGravados);

	}

	/**
	 * Alterar a Modelo com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Modelo
	 * @return Um objeto de transfer�ncia com os dados da Modelo alterada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de altera��o.
	 */
	public /*final*/ ModeloDTO alterarModelo(final ModeloDTO dto) throws GrowUpException {

		ModeloDTO retDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {

			// RN2
			this.validarCamposObrigatorios(dto);

			//RN4
			this.verificarExclusaoPrevia(dto);

			// RN3
			this.verificarNome(dto);
			
			DAO<ModeloDTO> dao = daoFactory.getModeloDAO();
			retDTO = dao.update(daoFactory.getConnection(), dto);

			salvarAnos(retDTO, daoFactory);

			retDTO = build(	MSGCODE.MARCA_ALTERADO_COM_SUCESSO, 
							retDTO,
							retDTO.getNome());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {

			try {

				daoFactory.closeCommit();

			} catch (Exception e) {
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}

		return retDTO;
	}

	/**
	 * Exclui a Modelo que cont�m os dados informados no objeto de
	 * transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Modelo
	 * @return Um objeto de transfer�ncia com os dados da Modelo criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 */
	public /*final*/ ModeloDTO excluirModelo(final ModeloDTO dto) throws GrowUpException {

		ModeloDTO retDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {

			//RN4
			this.verificarExclusaoPrevia(dto);

			// RN1
			this.verificarUso(dto);

			DAO<AnoModeloDTO> daoAno = daoFactory.getAnoModeloDAO();
			List<AnoModeloDTO> anos = daoAno.selectAll(daoFactory.getConnection(), new AnoModeloDTO(dto));
			for (AnoModeloDTO anoDTO : anos) {
				daoAno.delete(daoFactory.getConnection(), anoDTO);
			}
			
			DAO<ModeloDTO> dao = daoFactory.getModeloDAO();
			retDTO = dao.delete(daoFactory.getConnection(), dto);

			retDTO = build(	MSGCODE.MARCA_REMOVIDO_COM_SUCESSO, 
							retDTO,
							retDTO.getNome(),
							retDTO.getMarca().getNome());
			

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {

			try {

				daoFactory.closeCommit();

			} catch (Exception e) {
				
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}

		return retDTO;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public ModeloDTO verificarExclusaoPrevia(ModeloDTO dto) throws GrowUpException {
		ModeloDTO dtoAntes = null;

		dtoAntes = this.procurar(dto);

		if (dtoAntes == null) {
			throw new GrowUpException(
					MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,
					build(dto.getNome()));
		}

		return dtoAntes;
	}

}
