/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 22/11/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 22/11/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.business;

import static br.com.growupge.util.MensagemBuilder.build;
import static br.com.growupge.utility.Reflection.isCodigoValido;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.Iterator;
import java.util.Map;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.MensagemDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.stereotype.FirebirdStereoType;
import br.com.growupge.util.MensagemBuilder;
import br.com.growupge.utility.StringUtil;

/**
 * Business object para mensagem.
 * 
 * @author elmt
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Mensagem extends NegocioBase<MensagemDTO> {

	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Mensagem() {}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getMensagemDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}

	/**
	 * M�todo que retorna as mensagem de audit�ria do banco de dados.
	 * 
	 * @param msgcode
	 *            c�digo da mensagem.
	 * @param parametros
	 *            parametros da mensagem
	 * @return objeto dto de mensagem.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	public /*final*/ MensagemDTO getMensagem(
			final String msgcode,
			final Map<String, String> parametros) throws GrowUpException {

		MensagemDTO msgretorno = null;
		// DAOFactory daoFactory = DAOFactory.getDAOFactory();

		try {

			DAO<MensagemDTO> msgdao = daoFactory.getMensagemDAO();

			msgretorno = msgdao.find(daoFactory.getConnection(), new MensagemDTO(msgcode));

			// Existem parametros para serem trocados na mensagem
			if (parametros != null) {

				for (Iterator<String> e = parametros.keySet().iterator(); e.hasNext();) {

					String chave = e.next();

					if (msgretorno.getDescricaoBR() != null) {
						msgretorno.setDescricaoBR(StringUtil.replaceString(
								msgretorno.getDescricaoBR(),
								chave,
								parametros.get(chave)));
					}

					if (msgretorno.getDescricaoEN() != null) {
						msgretorno.setDescricaoEN(StringUtil.replaceString(
								msgretorno.getDescricaoEN(),
								chave,
								parametros.get(chave)));
					}
				}
			}

		} catch (GrowUpException e) {
			throw e;

		} finally {
			daoFactory.closeCommit();
		}

		return msgretorno;
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.business.NegocioBase#validarCamposObrigatorios(java.lang.Object)
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final MensagemDTO dto) throws GrowUpException {

		if (!isCodigoValido(dto)) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);

		} else if (isBlank(dto.getDescricaoBR())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);

		} else if (isBlank(dto.getDescricaoEN())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);

		}

	}
	
	/**
	 * Cria uma nova mensagem com os dados informados no objeto de
	 * transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da mensagem.
	 * @return Um objeto de transfer�ncia com os dados da mensagem criado.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	public /*final*/ MensagemDTO criarMensagem(final MensagemDTO dto) throws GrowUpException {

		MensagemDTO dtoMensagem = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {

			this.validarCamposObrigatorios(dto);

			this.verficarCodigo(dto);
			
			DAO<MensagemDTO> dao = daoFactory.getMensagemDAO();
			dtoMensagem = dao.insert(daoFactory.getConnection(), dto);

			dtoMensagem = build( MSGCODE.MENSAGEM_INCLUIDA_COM_SUCESSO, 
								dtoMensagem, 
								dtoMensagem.getCodigo()); 

		} catch (GrowUpException e) {				
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			daoFactory.closeCommit();
		}

		return dtoMensagem;
	}

	/**
	 * M�todo que avalia se j� existe uma ocorr�ncia de mensagem cadastrada no
	 * banco.
	 * 
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	private void verficarCodigo(MensagemDTO dto) throws GrowUpException {

		MensagemDTO mensagemDTO = this.procurar(new MensagemDTO(dto.getCodigo()));

		if (mensagemDTO != null) {
			throw new GrowUpException(
					MSGCODE.MENSAGEM_DUPLICADA,
					MensagemBuilder.build(mensagemDTO.getCodigo()));
		}
		
		dto.setCodigo(dto.getCodigo().toUpperCase());
	}

	/**
	 * Alterar mensagem com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da mensagem.
	 * @return Objeto com as informa��es da mensagem alterado.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de altera��o.
	 */
	public /*final*/ MensagemDTO alterarMensagem(final MensagemDTO dto) throws GrowUpException {

		MensagemDTO retDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {

			this.validarCamposObrigatorios(dto);

			//RN4
			this.verificarExclusaoPrevia(dto);
			
			DAO<MensagemDTO> dao = daoFactory.getMensagemDAO();
			retDTO = dao.update(daoFactory.getConnection(), dto);

			retDTO = build( MSGCODE.MENSAGEM_ALTERADA_COM_SUCESSO, 
							retDTO, 
							retDTO.getCodigo() ); 

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			daoFactory.closeCommit();
		}

		return retDTO;
	}

	/**
	 * Exclui uma mensagem que cont�m os dados informados no objeto de
	 * transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da mensagem.
	 * @return Objeto com as informa��es da mensagem exclu�do.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 */
	public /*final*/ MensagemDTO excluirMensagem(final MensagemDTO dto) throws GrowUpException {

		MensagemDTO retDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {
			//RN4
			this.verificarExclusaoPrevia(dto);

			DAO<MensagemDTO> dao = daoFactory.getMensagemDAO();
			retDTO = dao.delete(daoFactory.getConnection(), dto);

			retDTO = build( MSGCODE.MENSAGEM_REMOVIDA_COM_SUCESSO , 
							retDTO, 
							retDTO.getCodigo()); 

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {
			daoFactory.closeCommit();
		}

		return retDTO;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public MensagemDTO verificarExclusaoPrevia(MensagemDTO dto) throws GrowUpException {
		MensagemDTO dtoAntes = null;

		dtoAntes = this.procurar(dto);

		if (dtoAntes == null) {
			throw new GrowUpException(
					MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,
					build(dto.getDescricaoBR()));
		}

		return dtoAntes;
	}

}
