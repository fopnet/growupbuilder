/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 22/12/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 22/12/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.business;

import static br.com.growupge.util.MensagemBuilder.build;
import static br.com.growupge.utility.Reflection.isCodigoValido;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.Date;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.EmpresaDTO;
import br.com.growupge.dto.PagamentoBoletoDTO;
import br.com.growupge.dto.PagamentoDTO;
import br.com.growupge.dto.PagamentoPayPalDTO;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.interfacesdao.PagamentoDAO;
import br.com.growupge.interfacesdao.SequenciaDAO;
import br.com.growupge.stereotype.FirebirdStereoType;

/**
 * Business object para controle de pagamentos
 * 
 * @author Felipe
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Pagamento extends NegocioBase<PagamentoDTO> {

	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Pagamento() {}

	/**
	 * Construtor para esta classe.
	 * 
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o
	 */
	public Pagamento(SessaoDTO sessaodto) throws GrowUpException {
		this();
		this.setSessaodto(sessaodto);
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getPagamentoDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}
	
	/**
	 * @param dto
	 * @return
	 * @throws GrowUpException 
	 */
	protected PagamentoDTO gravar(final PagamentoDTO dto) throws GrowUpException {
		PagamentoDTO retDTO;
		
		EmpresaDTO remetente = super.getEmpresaBO().procurarGrowupVeiculos();
		try {
			
			PagamentoDAO dao = daoFactory.getPagamentoDAO();
			
			retDTO = dao.insert(daoFactory.getConnection(), dto);
			
			retDTO = build(MSGCODE.PAGAMENTO_SOLICITADO_SUCESSO, 
							retDTO, 
							retDTO.getPedido().getPlaca());

		} catch (GrowUpException e) {				
			daoFactory.rollBack();
			super.notificarErroSistema(e, remetente, remetente);
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			super.notificarErroSistema(e, remetente, remetente);
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			try {
				daoFactory.closeCommit();
			} catch (Exception e) {
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}

		}
		
		return retDTO;
		
	}

	/**
	 * M�todo utilizado gravar a solicitacao de pagamento
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com os dados da permiss�o a ser
	 *            criada.
	 * @return Objeto de transfer�ncia com os dados da permiss�o.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a cria��o da nova permiss�o.
	 */
	public /*final*/ PagamentoDTO solicitarPagamento(final PagamentoPayPalDTO dto)
			throws GrowUpException {
		PagamentoDTO retDTO = null;
		
		/* Valida se campo est� em branco */
		validarCamposObrigatorios(dto);
		
		PagamentoDAO dao = daoFactory.getPagamentoDAO();

		// Caso haja, algum pagamento do pedido, apague e grave novamente.
		dao.deletePagamentosPayPal(daoFactory.getConnection(), dto.getPedido());

		retDTO = gravar(dto);

		return retDTO;

	}

	/**
	 * @param dto
	 * @return
	 * @throws GrowUpException
	 */
	public /*final*/ PagamentoDTO solicitarPagamento(final PagamentoBoletoDTO dto)
			throws GrowUpException {

		validarCamposObrigatorios(dto);
		
		
		PagamentoDTO pgto = super.procurar(dto);
		
		// Se nao achou um pr�-pagamento de boleto, criar um novo
		if (!isCodigoValido(pgto)) {
			
			// � preciso criar o codigo aqui, por que o codigo do boleto � gerado pelo sistema.
			// e o codigo do paypal � trazido pelo paypal
			SequenciaDAO seq = daoFactory.getSequenciaDAO();
			dto.setCodigo(String.valueOf( seq.getProximaSequencia("GEN_PAGT_CD_PAGAMENTO") ));
			dto.setDataSolicitacao(new Date());
			
			dto.setValor( super.getVariavelBO().buscarValorUnitario().getValorAsDouble() );
			dto.calcularDataVencimento();
			
			pgto = gravar(dto); 
		} 

		return pgto; 
	}

	/**
	 * @param dto
	 * @throws GrowUpException
	 */
	protected void validarCamposObrigatorios(final PagamentoDTO dto) throws GrowUpException {
		/* Valida se campo est� em branco */
		if (!dto.isValido())
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
	}
	
	/**
	 * Confirmar pagamento de boleto.
	 * @param dto
	 * @return
	 */
	public PagamentoDTO confirmarPagamento(final PagamentoBoletoDTO dto) throws GrowUpException {
	//	validarCamposObrigatorios(dto);
		
		return confirmarPagamentoImpl(dto);
	}
	
	/**
	 * M�todo utilizado para a alterar a permiss�o informada.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com os dados da permiss�o a ser
	 *            alterada.
	 * @return PagamentoDTO Objeto de transfer�ncia com dados da permiss�o.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a altera��o da permiss�o.
	 */
	public PagamentoDTO confirmarPagamento(final PagamentoPayPalDTO dto)
			throws GrowUpException {

		EmpresaDTO remetente = super.getEmpresaBO().procurarGrowupVeiculos();
		
		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		try {

			PagamentoDTO newDTO = null;

			/* Valida se campo est� em branco */
			validarCamposObrigatorios(dto);
			
			if (isBlank(dto.getResposta())
					|| isBlank(dto.getStatus())) {
				throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
			}
			
			if (!dto.getPedido().getStatus().isExpirado()   // Expirado nao possui tx_id 
				&& (isBlank(dto.getTransacaoId()) || isBlank(dto.getTransacaoStatus()))) {
				throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
			}

			newDTO = confirmarPagamentoImpl(dto);

			return newDTO;

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			super.notificarErroSistema(e, remetente, remetente);
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			super.notificarErroSistema(e, remetente, remetente);
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			try {
				daoFactory.closeCommit();
			} catch (Exception e) {
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}
	}

	private PagamentoDTO confirmarPagamentoImpl(final PagamentoDTO dto) throws GrowUpException, GrowUpDAOException {
		PagamentoDTO newDTO = null;
		
		this.verificarExclusaoPrevia(dto);

		DAO<PagamentoDTO> dao = daoFactory.getPagamentoDAO();
		newDTO = dao.update(daoFactory.getConnection(), dto);

		// Buscar os dados completos do pedido.
		PedidoVeiculoDTO pedido = getPedidoBO().procurar(dto.getPedido());
		// o status do pedido j� foi configurado de acordo com o retorno do servi�o, no mbean.
//		pedido.setStatus(dto.getPedido().getStatus());
		switch (dto.getPedido().getStatus()) {
		case EXPIRADO:
			pedido.getStatus().expirar(pedido);
			break;
		default:
			pedido.getStatus().pagar(pedido);
		}
		pedido.setUsuarioAlteracao(pedido.getUsuarioCadastro());

		DAO<PedidoVeiculoDTO> daoPedido = daoFactory.getPedidoVeiculoDAO();
		daoPedido.update(daoFactory.getConnection(), pedido);
		
		newDTO = build(MSGCODE.PAGAMENTO_CONFIRMADO_SUCESSO, 
						newDTO, 
						newDTO.getPedido().getCodigo());
		return newDTO;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public PagamentoDTO verificarExclusaoPrevia(PagamentoDTO dto) throws GrowUpException {

		PagamentoDTO dtoAntes = null;

		dtoAntes = this.procurar(dto);

		if (dtoAntes == null) {
			throw new GrowUpException(
					MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,
					build(dto.getCodigo()));
		}

		return dtoAntes;
	}

	

}
