/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : J2EE 1.4
 * Criado em  : 24/11/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 24/11/2006 - In�cio de tudo, por Felipe Pina
 *
 */
package br.com.growupge.business;

import java.util.ArrayList;

import javax.mail.MessagingException;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.SenhaDTO;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.enums.TipoCaracter;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.Connectable;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.HtmlTemplateFactory;
import br.com.growupge.gerenciadores.GerenciadorEmail;
import br.com.growupge.security.BaseFactorySecurity;
import br.com.growupge.stereotype.FirebirdStereoType;
import br.com.growupge.template.HtmlTemplate;
import br.com.growupge.util.MensagemBuilder;
import br.com.growupge.utility.GrowupRandom;
import br.com.growupge.utility.StringUtil;

/**
 * Classe
 * 
 * @author Felipe Pina
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class HistoricoSenha extends NegocioBase<SenhaDTO> {

	/**
	 * Atributo '<code>codigo</code>' do tipo long
	 */
	private long codigo;

	/**
	 * Atributo '<code>usuario</code>' do tipo Usuario
	 */
	private UsuarioDTO usuario;

	/**
	 * Atributo '<code>senhaCorr</code>' do tipo String
	 */
	private String senhaDecript;

	/**
	 * Atributo '<code>senhaCript</code>' do tipo String
	 */
	private String senhaCript;

	/**
	 * Construtor para esta classe.
	 * 
	 * @throws GrowUpException
	 *             Caso ocorra algum problema durante a opera��o.
	 */
	public HistoricoSenha() {}

	/**
	 * Construtor da classe
	 * 
	 * @param usr
	 *            Par�metro do tipo Usuario
	 * @throws GrowUpException
	 *             Caso ocorra algum problema durante a opera��o.
	public HistoricoSenha(final UsuarioDTO usr) throws GrowUpException {
		this();
		this.usuario = usr;
	}
	 */
	
	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
	}

	/**
	 * Este m�todo � respons�vel por retornar o conte�do de 'codigo'.
	 * 
	 * @return Retorna um objeto do tipo long
	 */
	public /*final*/ long getCodigo() {
		return this.codigo;
	}

	/**
	 * Este m�todo � respons�vel por configurar o valor de 'codigo', que � um
	 * objeto do tipo long.
	 * 
	 * @param codigo
	 *            O valor de 'codigo' a ser configurado.
	 */
	public /*final*/ void setCodigo(final long codigo) {
		this.codigo = codigo;
	}

	/**
	 * Este m�todo � respons�vel por retornar o conte�do de 'senha'.
	 * 
	 * @return Retorna um objeto do tipo String
	 */
	public /*final*/ String getSenhaCript() {
		return this.senhaCript;
	}

	/**
	 * Este m�todo � respons�vel por configurar o valor de 'senha', que � um
	 * objeto do tipo String.
	 * 
	 * @param value
	 *            O valor de 'senha' a ser configurado.
	 * @throws GrowUpException
	 *             Caso ocorra algum problema durante a opera��o.
	 */
	protected /*final*/ void setSenha(final String value) throws GrowUpException {

		if (value == null) {
			getLogger().info("Senha do hist�rio est� nula");
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}

		BaseFactorySecurity bfs = BaseFactorySecurity.getInstance(Constantes.SECURITY_SHA);
		this.senhaDecript = value;
		this.senhaCript = bfs.criptografar(value);

		// this.senhaCript = this.senhaCorr != null ? bfs.criptografar(value) :
		// null;
	}

	/**
	 * Este m�todo � respons�vel por configurar o valor de 'usuario', que � um
	 * objeto do tipo UsuarioDTO.
	 * 
	 * @param usr
	 *            O valor de 'usuario' a ser configurado.
	 * @throws GrowUpException
	 *             Comentar aqui.
	 */
	public /*final*/ void setUsuario(final UsuarioDTO usr) {
		this.usuario = usr;
	}

	public /*final*/ String getSenhaCorr() throws GrowUpException {
		return this.senhaDecript;
	}

	/**
	 * Este m�todo � respons�vel por retornar o conte�do de 'usuario'.
	 * 
	 * @return Retorna um objeto do tipo UsuarioDTO
	 */
	public /*final*/ UsuarioDTO getUsuario() {
		return this.usuario;
	}

	/**
	 * Metodo para validar uma senha gerada.
	 * 
	 * @param valor
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	private void validarRegraFormacaoSenha(final String valor) throws GrowUpException {

		short i = 0;
		boolean isFound = false;

		try {

			if (valor == null || valor.equals("")) {
				throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
			}

			/* Senha de possuir tamanho no m�nimo 6 */
			 if (valor.length() < UsuarioDTO.TAMANHO_MINIMO_SENHA)
				 throw new GrowUpException(MSGCODE.SENHA_FORMATO_INVALIDO);
			/*
			 * Os tr�s primeiros caracteres devem ser diferentes dos tr�s
			 * primeiros do login
			 if
			 (valor.substring(0,3).toUpperCase().equals(this.getUsuario().getDados().codigo.substring(0,3).toUpperCase()))
			 {
			 	throw new GrowUpException(MSGCODE.SENHA_FORMATO_INVALIDO);
			 }
			 */
			 
			/* Primeiro caracter deve ser maiusculo 
			if (!Character.isLowerCase(valor.charAt(0))) {
				throw new GrowUpException(MSGCODE.SENHA_FORMATO_INVALIDO);
			}
			*/

			/* As tr�s primeiras letras devem ser diferentes */
			// if (valor.toUpperCase().charAt(0) ==
			// valor.toUpperCase().charAt(1)
			// && valor.toUpperCase().charAt(1) ==
			// valor.toUpperCase().charAt(2)) {
			// throw new GrowUpException(MSGCODE.SENHA_FORMATO_INVALIDO);
			// }
			if (this.usuario != null
					&& this.usuario.getCodigo().length() < UsuarioDTO.TAMANHO_MINIMO_LOGIN) {
				throw new GrowUpException(MSGCODE.SENHA_FORMATO_INVALIDO);
			}

			/* Nova senha deve ser diferente da ultima */
			if (this.usuario != null
					&& this.usuario.getCodigo() != null
					&& valor.substring(UsuarioDTO.TAMANHO_MINIMO_LOGIN).toUpperCase().equals(
							this.usuario.getCodigo().toUpperCase().substring(
									UsuarioDTO.TAMANHO_MINIMO_LOGIN))) {
				throw new GrowUpException(MSGCODE.SENHA_FORMATO_INVALIDO);
			}

			/* Deve possuir no minimo 1 maiusculo 
			for (i = 0; i < valor.length(); i++) {
				if (Character.isUpperCase(valor.charAt(i))) {
					isFound = true;
					break;
				}
			}
			if (!isFound) {
				throw new GrowUpException(MSGCODE.SENHA_FORMATO_INVALIDO);
			} */

			/* Deve possuir no minimo 1 n�mero */
			isFound = false;
			for (i = 0; i < valor.length(); i++) {
				if (Character.isDigit(valor.charAt(i))) {
					isFound = true;
					break;
				}
			}
			if (!isFound) {
				throw new GrowUpException(MSGCODE.SENHA_FORMATO_INVALIDO);
			}
			/* N�o pode ter caracteres especiais */
			for (i = 0; i < valor.length(); i++) {
				if (StringUtil.isCharEspecial(valor.charAt(i))) {
					throw new GrowUpException(MSGCODE.SENHA_FORMATO_INVALIDO);
				}
			}

		} catch (GrowUpException ex) {
			throw ex;
		}

	}

	/**
	 * Metodo para validar uma senha alterada
	 * 
	 * @param dto
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o.
	 */
	private void validarRegrasSegurancaSenha(final SenhaDTO dto) throws GrowUpException {

		this.validarCamposObrigatorios(dto);

		// Validando senha decriptografada
		try {
			this.validarRegraFormacaoSenha(dto.getValor());
		} catch (GrowUpException se) {
			se.setMensagem(MensagemBuilder.getMensagem(se.getCode(), dto.getUsuario().getIdioma(), null));
			throw se;
		}

		SenhaDTO param = new SenhaDTO();
		param.setUsuario(dto.getUsuario());
		// param.usuario = this.getUsuario().getDados();

		// Carregando todas as senhas do usu�rio
		// List<SenhaDTO> senhas = this.buscarTodas(param);

		/*
		 * Verificando se a nova senha criptografa, pelo m�todo validarSenha, �
		 * igual ultima senha do usu�rio.
		 */
		/*
		 * if (senhas.size() > 0) { if (senhas.get(senhas.size() -
		 * 1).valor.equals(this.senhaCript)) { // A ultima senha � a nova senha.
		 * throw new GrowUpException(MSGCODE.SENHA_IGUAL_ANTERIOR,
		 * Mensagem.getMensagem(MSGCODE.SENHA_IGUAL_ANTERIOR,
		 * dto.usuario.idioma, null)); }
		 *  // Verificando se a nova senha criptografa � igual a algumas das
		 * ultimas // doze senhas. for (short i = 0; i < senhas.size(); i++) {
		 * if (this.senhaCript.equals(senhas.get(i).valor)) { throw new
		 * GrowUpException(MSGCODE.SENHA_FORMATO_INVALIDO,
		 * Mensagem.getMensagem(MSGCODE.SENHA_FORMATO_INVALIDO,
		 * dto.usuario.idioma, null)); } } }
		 */
		this.setDados(dto);
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.business.BusinessObject#validarCamposObrigatorios(java.lang.Object)
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final SenhaDTO dto) throws GrowUpException {

		// RN1 - Recriar Senha
		if (dto.getUsuario() == null || dto.getValor() == null || this.senhaCript == null
				|| this.senhaDecript == null) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}

	}

	/**
	 * Metodo gera uma senha ao criar um novo usuario. Este regra de cria��o da
	 * primeira senha difere da regra ao alterar a senha. Pode ser usado tamb�m
	 * para gerar outro codigo de acesso, caso o usuario tenha esquecido da
	 * senha atual.
	 * 
	 * @return Retorna objeto do tipo String
	 * @throws GrowUpException
	 *             Caso ocorra algum problema durante a opera��o.
	 */
	public /*final*/ String gerarSenha() throws GrowUpException {

		String pwd = "";
		GrowupRandom rnd = new GrowupRandom(TipoCaracter.MINUSCULO);
		pwd += rnd.nextChar();

		ArrayList<GrowupRandom> lista = new ArrayList<GrowupRandom>();
		lista.add(rnd);
		lista.add(new GrowupRandom(TipoCaracter.NUMERICO));
		lista.add(new GrowupRandom(TipoCaracter.MAIUSCULO));

		rnd = lista.remove(rnd.nextInt(lista.size()));

		for (short i = 0; i < 3; i++) {
			pwd += rnd.nextChar();
		}

		rnd = lista.remove(rnd.nextInt(lista.size()));

		for (short i = 0; i < 3; i++) {
			pwd += rnd.nextChar();
		}

		rnd = lista.remove(0);
		pwd += rnd.nextChar();

		try {
			this.validarRegraFormacaoSenha(pwd);

			this.setSenha(pwd);

		} catch (GrowUpException ex) {
			this.gerarSenha();
		} catch (Exception ex) {
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, ex);
		}

		return this.getSenhaCript();

	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.business.BusinessObject#setDados(java.lang.Object)
	 * @deprecated
	 */
	public /*final*/ void setDados(final SenhaDTO dto) throws GrowUpException {
		this.setUsuario(dto.getUsuario());
		this.setSenha(dto.getValor());
	}

	/**
	 * Cria uma nova senha com os dados informados no objeto de transfer�ncia. e
	 * grava na tabela de historico com a chave do usuario
	 * 
	 * @return Um objeto de transfer�ncia com os dados da senha criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	public /*final*/ SenhaDTO enviarSenhaPorEmail() throws GrowUpException {
		// Senha pronta gerada pela chamada do m�todo gerarSenha()
		SenhaDTO dto = this.getDados();

		this.validarCamposObrigatorios(dto);

		// Enviando senha por email
		this.enviarSenha(this.usuario.getEmpresa().getNome().concat(" - Acesso ao sistema"), null);

		return dto;
	}

	/**
	 * Este m�todo cria uma nova senha, associa ao usuario passado no objeto de
	 * transferencia e delete a ultima senha caso a quantidade passe de 12
	 * senhas no historico.
	 * 
	 * @param dto
	 *            Um objeto do tipo SenhaDTO
	 * @return Um objeto de transfer�ncia com os dados da senha alterada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o.
	 */
	public /*final*/ SenhaDTO alterarHistorico(final SenhaDTO dto) throws GrowUpException {

		this.validarCamposObrigatorios(dto);

		// Se a senha estiver v�lida, chama o setDados()
		this.validarRegrasSegurancaSenha(dto);

		// dto com a senha criptografada
		SenhaDTO senhaDTO = this.getDados();

		return senhaDTO;
	}

	/**
	 * M�todo que envia os par�metros passados concatenados com a senha
	 * decriptografada em negrito.
	 * 
	 * @param assunto
	 *            Parametros do tipo String
	 * @param dados
	 *            Algum dados adicionado ao email.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o.
	 * 
	 */
	public /*final*/ void enviarSenha(final String assunto, final StringBuffer dados)
			throws GrowUpException {

		try {
			GerenciadorEmail gerenciador = GerenciadorEmail.getInstance();

			// Obtem o Template com base no idioma do usu�rio
			HtmlTemplate templateEmail = HtmlTemplateFactory.getTemplateNovaSenha();

			// Adiciona os parametros que dever�o ser substituidos no template
			templateEmail.adicionarParametro(HtmlTemplate.PARAM_USR, this.getUsuario().getCodigo());
			templateEmail.adicionarParametro(HtmlTemplate.PARAM_EMAIL, this.getUsuario().getEmail());
			templateEmail.adicionarParametro(HtmlTemplate.PARAM_PWD, this.senhaDecript);

			// Executa o template
			templateEmail.execute();

			/* Envia email para o usu�rio com a senha dele */
			gerenciador.enviarEmail(assunto, templateEmail.getHtmlTemplate(), 
									this.getUsuario().getEmpresa(), 
									this.getUsuario());

		} catch (MessagingException ex) {
			throw new GrowUpException(MSGCODE.ERRO_ENVIO_EMAIL, ex.getMessage(), "", ex
					.getMessage());
		} catch (GrowUpException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new GrowUpException(MSGCODE.ERRO_ENVIO_EMAIL, ex.getMessage(), ex.getMessage(),
					ex.getMessage());
		}
	}

	/**
	 * Este m�todo exclui ultima senha do usu�rio. parte do neg�cio, somente �
	 * usado nos caso de teste para poder retornar ao estado base.
	 * 
	 * @param dto
	 *            Objeto do tipo SenhaDTO
	 * @return Um objeto de transfer�ncia com os dados da senha exclu�da.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o.
	 */
	public /*final*/ SenhaDTO excluirSenha(final SenhaDTO dto) throws GrowUpException {
		Connectable daofactory = null;
		try {

			// daofactory = daoFactory.getDAOFactory();
			//
			// DAO<SenhaDTO> dao = daoFactory.getSenhaDAO();
			// SenhaDTO senhaDTO = dao.delete(daoFactory.getConnection(),
			// this.getDados());

			// return senhaDTO;
			return null;

		} finally {
			try {
				if (daofactory != null) {
					daoFactory.closeCommit();
				}
			} catch (Exception e) {
				
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.business.BusinessObject#getDados()
	 */
	public /*final*/ SenhaDTO getDados() throws GrowUpException {
		SenhaDTO dto = new SenhaDTO();
		dto.setCodigo(this.getCodigo());
		dto.setValor(this.getSenhaCript());
		dto.setUsuario(this.getUsuario());
		return dto;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public SenhaDTO verificarExclusaoPrevia(SenhaDTO dto) throws GrowUpException {
		return null;
	}

}
