package br.com.growupge.business;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.growupge.annotations.ErasureType;
import br.com.growupge.constantes.Constantes;
import br.com.growupge.dto.IEmail;
import br.com.growupge.dto.PerfilDTO;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.enums.TipoPerfil;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.gerenciadores.GerenciadorEmail;
import br.com.growupge.interfaces.SessaoUsuario;
import br.com.growupge.interfacesdao.ConsultaDAO;
import br.com.growupge.utility.StringUtil;

/**
 * Classe usada pelos objetos de neg�cios para consulta
 * 
 * @author Felipe
 * 
 */
@ErasureType(getErasuresTypes = { Object.class })
public abstract class ConsultaBase<DTO> implements Consulta<DTO>, SessaoUsuario {
	private Logger logger = null;
	
	/**
	 *  Atributo '<code>daoCrud</code>' do tipo ConsultaDAO<DTO>
	 */
	protected ConsultaDAO<DTO> crudDAO = null;

	/**
	 *  Atributo '<code>daofactory</code>' do tipo DAOFactory
	 */
	protected DAOFactory daoFactory = null;

	/**
	 * Atributo '<code>consulta</code>' do tipo ConsultaImpl<DTO>
	 */
//	protected ConsultaImpl consulta;

	/**
	 * Atributo '<code>sessaodto</code>' do tipo SessaoDTO
	 */
//	protected SessaoDTO sessaodto = null;

	/**
	 * Construtor para esta classe.
	 * 
	 * @param <DTO_LOCAL>
	 *            M�todo parametrizado com algum dto
	 * @param dao
	 *            Fabrica padr�o do BO
	 */
//	protected ConsultaBase(final ConsultaDAO<DTO> dao) {
//		this.daoCrud = dao;
//		this.setConsulta(new ConsultaImpl());
//	}
	
	/**
	 * Ponto obrigatorio para inje��o da fabrica de dao
	 * Atrav�s desta f�brica se configura o classe dao padr�o da entidade
	 * @param daofactory
	 */
	protected abstract void setDaoFactory(DAOFactory daofactory);

	/**
	 * Este m�todo � respons�vel por configurar o valor de 'sessaordto', que �
	 * um objeto do tipo SessaoDTO.
	 * 
	 * @param sessaodto
	 *            O valor de 'sessaordto' a ser configurado.
	 */
	@Override
	public void setSessaodto(final SessaoDTO sessao) {
//		this.sessaodto = sessaodto;
//		if (crudDAO != null)
//			crudDAO.setSessao(sessaodto);
		/* Atribui a sessao do usuario, na fabrica que � a mesma que todos os negocios e ConsultaDAO
		 * 
		 */
		daoFactory.setSessao(sessao); 
	}

	//Getters and setters

	/**
	 * Este m�todo � respons�vel por configurar
	 * o valor de 'consulta', que � um objeto do tipo ConsultaImpl.
	 * @param consulta O valor de 'consulta' a ser configurado.
	 */
//	protected void setConsulta(ConsultaImpl consulta) {
//		this.consulta = consulta;
//	}

	/**
	 * Este m�todo � respons�vel por retornar o
	 * conte�do de 'consulta'.
	 * @return Retorna um objeto do tipo ConsultaImpl
	 */
//	protected ConsultaImpl getConsultaBase() {
//		return consulta;
//	}

	// M�todos buscarTodas

	/**
	 * M�todo utilizado para pesquisar todos os registros
	 * 
	 * @return Uma lista de todos os registros que atendem ao crit�rio de
	 *         pesquisa.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a pesquisa.
	 */
	@Override
	public /*final*/ List<DTO> buscarTodas() throws GrowUpException {
		return buscarTodas(
				null,
				true,
				Constantes.DESCONSIDERAR_PAGINACAO,
				this.crudDAO);
	}

	/**
	 * M�todo utilizado para pesquisar todos os registros
	 * 
	 * @param indiceInicial
	 *            Buscar� a quantidade de registros retornados a partir do
	 *            indice inicial
	 * @return Uma lista de todos os registros que atendem ao crit�rio de
	 *         pesquisa.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a pesquisa.
	 */
	@Override
	public /*final*/ List<DTO> buscarTodas(final Integer indiceInicial) throws GrowUpException {
		return buscarTodas(null, true, indiceInicial, this.crudDAO);
	}

	/**
	 * M�todo utilizado para pesquisar todos os registros que contenham as
	 * informa��es extraidas do dto.
	 * 
	 * @param dto
	 *            Transfer object que cont�m as informa��es de pesquisa.
	 * @return Uma lista de todos os registros que atendem ao crit�rio de
	 *         pesquisa.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a pesquisa.
	 * 
	 * PS: Este m�todo n�o � final porque precisa ser sobreescrito nas classes
	 * de grupo
	 */
	@Override
	public List<DTO> buscarTodas(final DTO dto) throws GrowUpException {
		return buscarTodas(
				dto,
				true,
				Constantes.DESCONSIDERAR_PAGINACAO,
				this.crudDAO);
	}

	/**
	 * M�todo utilizado para pesquisar todos os registros que contenham as
	 * informa��es extraidas do dto.
	 * 
	 * @param dto
	 *            Transfer object que cont�m as informa��es de pesquisa.
	 * @param indiceInicial
	 *            Buscar� a quantidade de registros retornados a partir do
	 *            indice inicial
	 * @param <DTO>
	 *            M�todo parametrizado com algum dto
	 * @return Uma lista de todos os registros que atendem ao crit�rio de
	 *         pesquisa.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a pesquisa.
	 * 
	 * PS: Este m�todo n�o � final porque precisa ser sobreescrito nas classes
	 * de grupo
	 */
	@Override
	public List<DTO> buscarTodas(final DTO dto, final Integer indiceInicial) throws GrowUpException {
		return buscarTodas(dto, true, indiceInicial, this.crudDAO);
	}

	// M�todos buscarTotais

	/**
	 * M�todo que retorna a quantidade de registros de acordo com objeto de
	 * negocio
	 * 
	 * @return Retorna um primitivo do tipo long
	 * @throws GrowUpException
	 *             Caso ocorra algum erro na opera��o.
	 */
	@Override
	public /*final*/ Long buscarTotal() throws GrowUpException {
		return buscarTotal(null, this.crudDAO);
	}

	/**
	 * M�todo que retorna a quantidade de registros baseado no dto
	 * 
	 * @param <DTO>
	 *            DTO parametrizado de qualquer tipo
	 * @param dto
	 *            Parametro do tipo DTO
	 * @return Retorna um primitivo do tipo long1
	 * @throws GrowUpException
	 *             Caso ocorra algum erro na opera��o.
	 */
	@Override
	public Long buscarTotal(DTO dto) throws GrowUpException {
		return buscarTotal(dto, this.crudDAO);
	}

	// Metodo procurar

	/**
	 * M�todo utilizado para pesquisar por um registro que esteja de acordo com
	 * as informa��es contidas no dto.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia que cont�m as informa��es de pesquisa.
	 * @return Retorna um transfer object com os dados do registro encontrado.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a pesquisa.
	 */
	@Override
	public DTO procurar(DTO dto) throws GrowUpException {
		return procurar(dto, this.crudDAO);
	}
	
	/**
	 * Verifica se o perfil do usuario logado � maior que o perfil passado
	 * @param perfil
	 * @return
	 */
	public boolean isPerfilUsuarioInferiorAo(TipoPerfil perfil) {
		return crudDAO.isPerfilUsuarioMaior(perfil);
	}

	/**
	 * Verifica se o perfil do usuario logado � maior que o perfil passado
	 * 
	 * @param perfil
	 * @param codigoPerfil
	 * @return
	 */
	public boolean isPerfilUsuarioInferiorAo(TipoPerfil perfil, PerfilDTO perfilDTO) {
		String codigoPerfil = perfilDTO != null && isNotBlank(perfilDTO.getCodigo()) ? perfilDTO.getCodigo() : ""; 
		return crudDAO.isPerfilUsuarioMaior(perfil, codigoPerfil);
	}
	
	/**
	 * Verifica se o usuario logado � admin
	 * @return
	 */
	public boolean isUsuarioAdmin() {
		return crudDAO.isUsuarioAdmin();
	}
	
	protected Logger getLogger () {
		if (logger == null)
			logger = Logger.getLogger(getClass());
		return logger;
	}
	
	/**
	 * @param ex
	 * @param remetente
	 * @param destinatario
	 */
	protected void notificarErroSistema(Throwable ex, IEmail remetente, IEmail destinatario) {
		String assunto = "Erro de sistema na Classe".concat(getClass().getName());

		GerenciadorEmail ge;
		try {
			ge = GerenciadorEmail.getInstance();
			ge.enviarEmail(assunto, StringUtil.getStackTrace(ex), remetente, destinatario);

		} catch (Exception e) {
			getLogger().error(e,e);
		}
	}
	
	/******************************************************************************************************************************
	 * Implementa��o dos metodos de consulta 
	******************************************************************************************************************************/
	public <OUT> List<OUT> buscarTodas(final DTO dto, List<OUT> lista) throws GrowUpException {
		return buscarTodas(dto, true, Constantes.DESCONSIDERAR_PAGINACAO, this.crudDAO, lista);
	}
	
	protected /*final*/ List<DTO> buscarTodas(final DTO dto,
			final boolean isClausulaLike, final Integer indiceInicial,
			final ConsultaDAO<DTO> dao) throws GrowUpException {
		return buscarTodas(dto, isClausulaLike, indiceInicial, dao, new ArrayList<DTO>());
	}
	
	/**
	 * M�todo buscarTodas usado somente internamente, chamado pelas classes de
	 * grupo, para evitar de fazer o like.
	 * 
	 * @param dto
	 *            Parametro do tipo DTO
	 * @param isClausulaLike
	 *            determina se o select usar� a clausula Like ou Where
	 * @param indiceInicial
	 *            Indice inicial para fazer a pagina��o.
	 * @param dao
	 *            objeto de acesso a dados enviado pelo objeto de negocio.
	 * @return Uma lista com todos os dto�s
	 * @throws GrowUpException
	 *             Caso ocorra algum falha durante a pesquisa.
	 * 
	 */
	protected /*final*/ <IN, OUT> List<OUT> buscarTodas(final IN dto,
			final boolean isClausulaLike, final Integer indiceInicial,
			final ConsultaDAO<IN> dao,
			final List<OUT> lista) throws GrowUpException {
		// DAOFactory daofactory = daoFactory.getDAOFactory();

		try {
			return dao.selectAll(daoFactory.getConnection(), dto, isClausulaLike, indiceInicial, lista);

		} catch (GrowUpDAOException e) {
			throw e;
		} catch (Exception e) {
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
		} finally {
			daoFactory.closeCommit();
		}
	}

	// M�todos buscarTotais

	/**
	 * M�todo que retorna a quantidade de registros baseado no dto
	 * 
	 * @param <DTO>
	 *            DTO parametrizado de qualquer tipo
	 * @param dto
	 *            Parametro do tipo DTO
	 * @return Retorna um primitivo do tipo long1
	 * @throws GrowUpException
	 *             Caso ocorra algum erro na opera��o.
	 */
	protected /*final*/ <DTO_LOCAL> Long buscarTotal(final DTO_LOCAL dto, final ConsultaDAO<DTO_LOCAL> dao)
			throws GrowUpException {
		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		try {
			return dao.count(daoFactory.getConnection(), dto);

		} catch (GrowUpDAOException e) {
			throw e;
		} catch (Exception e) {
			
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
		} finally {
			daoFactory.closeCommit();
		}
	}

	/**
	 * M�todo utilizado para pesquisar por um registro que esteja de acordo com
	 * as informa��es contidas no dto.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia que cont�m as informa��es de pesquisa.
	 * @return Retorna um transfer object com os dados do registro encontrado.
	 * @throws SigemException
	 *             Caso ocorra algum erro durante a pesquisa.
	 */
	protected /*final*/ <DTO_LOCAL> DTO_LOCAL procurar(final DTO_LOCAL dto,
			final ConsultaDAO<DTO_LOCAL> dao) throws GrowUpException {
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {

			return dao.find(daoFactory.getConnection(), dto);

		} catch (GrowUpDAOException e) {
			throw e;

		} finally {
			daoFactory.closeCommit();
		}

	}
}
