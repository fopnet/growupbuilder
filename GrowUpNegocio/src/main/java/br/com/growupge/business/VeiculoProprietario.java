package br.com.growupge.business;

import static br.com.growupge.util.MensagemBuilder.build;
import static br.com.growupge.utility.Reflection.isCodigoValido;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.dto.VeiculoProprietarioDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.stereotype.FirebirdStereoType;

/**
 * Business object para VeiculoProprietario
 * 
 * @author Felipe
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class VeiculoProprietario extends NegocioBase<VeiculoProprietarioDTO> {

	/**
	 * Construtor para esta classe.
	 * 
	 */
	public VeiculoProprietario() {}

	public VeiculoProprietario(final SessaoDTO dto) throws GrowUpException {
		this();
		this.setSessaodto(dto);
	}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getVeiculoProprietarioDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}

	/**
	 * Método sobrescrito.
	 * 
	 * @see br.com.growupge.business.NegocioBase#validarCamposObrigatorios(java.lang.Object)
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final VeiculoProprietarioDTO dto)
			throws GrowUpException {

		// RN2
		if (!isCodigoValido(dto.getVeiculo())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}

		if (!isCodigoValido(dto.getProprietario())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}

	}


	/**
	 * Cria uma nova Modelo com os dados informados no objeto de transferência.
	 * 
	 * @param dto
	 *            Objeto de transferência com as informações da VeiculoProprietario
	 * @return Um objeto de transferência com os dados da Modelo criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a operação de criação.
	 */
	public /*final*/ VeiculoProprietarioDTO adicionarVeiculoProprietario(final VeiculoProprietarioDTO dto) throws GrowUpException {

		VeiculoProprietarioDTO retDTO = null;
		VeiculoProprietarioDTO dtoAntes = null;

		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		try {

			// Se os valores forem válidos, atribui a classe.
			this.validarCamposObrigatorios(dto);

			if ((dtoAntes = this.procurar(dto)) != null) {
				return dtoAntes;
			}

			DAO<VeiculoProprietarioDTO> dao = daoFactory.getVeiculoProprietarioDAO();
			retDTO = dao.insert(daoFactory.getConnection(), dto);
			return retDTO;

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {
			daoFactory.closeCommit();
		}
	}

	/**
	 * Cria uma nova Modelo com os dados informados no objeto de transferência.
	 * 
	 * @param dto
	 *            Objeto de transferência com as informações da VeiculoProprietario
	 * @return Um objeto de transferência com os dados da Modelo criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a operação de criação.
	 */
	public /*final*/ VeiculoProprietarioDTO alterarVeiculoProprietario(final VeiculoProprietarioDTO dto) throws GrowUpException {

		VeiculoProprietarioDTO retDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {

			// Se os valores forem válidos, atribui a classe.
			this.validarCamposObrigatorios(dto);

			//RN4
			this.verificarExclusaoPrevia(dto);

			DAO<VeiculoProprietarioDTO> dao = daoFactory.getVeiculoProprietarioDAO();
			retDTO = dao.update(daoFactory.getConnection(), dto);

			return retDTO;

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {
			daoFactory.closeCommit();
		}
	}

	/**
	 * Método sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public VeiculoProprietarioDTO verificarExclusaoPrevia(VeiculoProprietarioDTO dto) throws GrowUpException {

		VeiculoProprietarioDTO dtoAntes = null;

		dtoAntes = this.procurar(dto);

		if (dtoAntes == null) {
			throw new GrowUpException(
					MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,
					build(dto.getVeiculo().getCodigo()));
		}

		return dtoAntes;
	}

}
