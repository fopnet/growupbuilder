package br.com.growupge.business;

import java.util.ArrayList;
import java.util.List;

import br.com.growupge.annotations.ErasureType;
import br.com.growupge.constantes.Constantes;
import br.com.growupge.dto.DTOPadrao;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.interfacesdao.ConsultaDAO;

/**
 * Classe usada pelos objetos de neg�cios para consulta
 * 
 * @author Felipe
 * 
 */
@ErasureType(getErasuresTypes = { Object.class, DTOPadrao.class })
public abstract class ConsultaGrupoBase<DTO, DTORel extends DTOPadrao> extends NegocioBase<DTO> {

	/**
	 * Atributo '<code>defaultDAOFactory</code>' do tipo DAO<DTO>
	 */
	protected ConsultaDAO<DTORel> crudGrupoDAO = null;

	/**
	 * Construtor para esta classe.
	 * 
	 * @param <DTO_LOCAL>
	 *            M�todo parametrizado com algum dto
	 * @param dao
	 *            Fabrica padr�o do BO
	 */
//	protected ConsultaGrupoBase(final ConsultaDAO<DTO> daoCrud,
//			final ConsultaDAO<DTORel> daoComposta) {
//		super(daoCrud);
//		this.daoGrupoCrud = daoComposta;
//	}
	
	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.ConsultaBase#setSessaodto(br.com.growupge.dto.SessaoDTO)
	 */
	/*@Override
	public void setSessaodto(SessaoDTO dto) {
		super.setSessaodto(dto);
		if (this.crudGrupoDAO != null) {
			this.crudGrupoDAO.setSessao(dto);
		}
	}*/

	// M�todos buscarTodas

	/**
	 * M�todo utilizado para pesquisar todos os registros que contenham as
	 * informa��es extraidas do dto.
	 * 
	 * @param dto
	 *            Transfer object que cont�m as informa��es de pesquisa.
	 * @return Uma lista de todos os registros que atendem ao crit�rio de
	 *         pesquisa.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a pesquisa.
	 * 
	 * PS: Este m�todo n�o � final porque precisa ser sobreescrito nas classes
	 * de grupo
	 */
	public List<DTORel> buscarTodas(final DTORel dto) throws GrowUpException {
		return super.buscarTodas(dto, false, Constantes.DESCONSIDERAR_PAGINACAO, this.crudGrupoDAO, new ArrayList<DTORel>());
	}

	/**
	 * M�todo utilizado para pesquisar todos os registros que contenham as
	 * informa��es extraidas do dto.
	 * 
	 * @param dto
	 *            Transfer object que cont�m as informa��es de pesquisa.
	 * @param indiceInicial
	 *            Buscar� a quantidade de registros retornados a partir do
	 *            indice inicial
	 * @param <DTO>
	 *            M�todo parametrizado com algum dto
	 * @return Uma lista de todos os registros que atendem ao crit�rio de
	 *         pesquisa.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a pesquisa.
	 * 
	 * PS: Este m�todo n�o � final porque precisa ser sobreescrito nas classes
	 * de grupo
	 */
	public List<DTORel> buscarTodas(final DTORel dto, final Double indiceInicial)
			throws GrowUpException {
		return super.buscarTodas(dto, false, indiceInicial.intValue(), this.crudGrupoDAO, new ArrayList<DTORel>());
	}
	
	// M�todos buscarTotais

	/**
	 * M�todo que retorna a quantidade de registros baseado no dto
	 * 
	 * @param <DTO>
	 *            DTO parametrizado de qualquer tipo
	 * @param dto
	 *            Parametro do tipo DTO
	 * @return Retorna um primitivo do tipo long1
	 * @throws GrowUpException
	 *             Caso ocorra algum erro na opera��o.
	 */
	public /*final*/ Long buscarTotal(DTORel dto) throws GrowUpException {
		return super.buscarTotal(dto, this.crudGrupoDAO);
	}

	// Metodo procurar

	/**
	 * M�todo utilizado para pesquisar por um registro que esteja de acordo com
	 * as informa��es contidas no dto.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia que cont�m as informa��es de pesquisa.
	 * @return Retorna um transfer object com os dados do registro encontrado.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a pesquisa.
	 */
	public /*final*/ DTORel procurar(DTORel dto) throws GrowUpException {
		return super.procurar(dto, this.crudGrupoDAO);
	}

}
