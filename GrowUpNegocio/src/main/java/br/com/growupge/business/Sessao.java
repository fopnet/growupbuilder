/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 22/11/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 22/11/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.business;

import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;

import br.com.growupge.autenticador.Autenticador;
import br.com.growupge.autenticador.AutenticadorFactory;
import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.PerfilDTO;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.enums.TipoIdioma;
import br.com.growupge.enums.TipoPerfil;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.interfacesdao.SessaoDAO;
import br.com.growupge.interfacesdao.UsuarioDAO;
import br.com.growupge.security.BaseFactorySecurity;
import br.com.growupge.stereotype.FirebirdStereoType;
import br.com.growupge.util.MensagemBuilder;
import br.com.growupge.utility.DateUtil;

/**
 * Business object para manipula��o de dados da sess�o.
 * 
 * @author Felipe
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Sessao extends NegocioBase<SessaoDTO> {
	

	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Sessao() throws GrowUpException {}

	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getSessaoDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}

	/**
	 * Este m�todo cria uma sessao para o usuario vinculado a sess�o.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com os dados da sess�o.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante o registro da sess�o.
	 */
	public /*final*/ void registrar(final SessaoDTO dto) throws GrowUpException {
		// DAOFactory daofactory = daoFactory.getDAOFactory();
		try {
			try {
				DAO<SessaoDTO> dao = daoFactory.getSessaoDAO();
				dao.insert(daoFactory.getConnection(), dto);
			} catch (GrowUpDAOException e) {
				
				throw e;
			}
		} finally {
			daoFactory.closeCommit();
		}
	}


	/**
	 * Este m�todo � respons�vel por verificar se o usuario est� habilitado no
	 * sistema.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com os dados de usu�rio.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a valida��o.
	 */
	public /*final*/ void validarUsuarioHabilitado(UsuarioDTO dto) throws GrowUpException {

		if (!dto.isUsuarioHabilitado()) {
			dto.parametros = new HashMap<String, String>();
			dto.parametros.put(Constantes.P1, dto.getCodigo());
			throw new GrowUpException(MSGCODE.USUARIO_DESABILITADO, dto, MensagemBuilder.getMensagem(
					MSGCODE.USUARIO_DESABILITADO,
					TipoIdioma.IDIOMA_BR.toString(),
					dto.parametros), dto.parametros);
		}
	}

	/**
	 * Este m�todo gera uma SID de acordo com o parametros passados e dos
	 * milisegundos do sistema
	 * 
	 * @param user
	 *            O usu�rio de conex�o.
	 * @param hostname
	 *            O Host de conex�o.
	 * @return Uma String com o SID representando a sess�o do usu�rio.
	 */
	@SuppressWarnings("unused")
	private /*final*/ String calculateUserSession(final SessaoDTO dto) {
		String hashcode;
		BaseFactorySecurity bfs = BaseFactorySecurity.getInstance(Constantes.SECURITY_SHA);
		DateUtil dm = DateUtil.getDateManagerInstance();

		StringBuffer keyblend = new StringBuffer();
		keyblend.append(dto.getUsuario().getCodigo())
				.append(dto.getHost())
				.append(Long.toString(dm.getTimeInMillis()))
				.append(dto.getSid())
				.append(Math.random());
				;

		try {
			hashcode = bfs.criptografar(keyblend.toString());
		} catch (GrowUpException e) {
			hashcode = null;
		}
		return hashcode;
	}

	/**
	 * M�todo que conecta o usu�rio atrav�s da forma de autentica��o LDAP ou
	 * SIST retornando o objeto de transfer�ncia do tipo 'SessaoDTO'.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com alguns dados da sess�o.
	 * @return Objeto de transfer�ncia com os dados da sess�o obtida.
	 * @throws GrowUpException -
	 */
	public /*final*/ SessaoDTO conectar(final SessaoDTO dto) throws GrowUpException {
		UsuarioDTO usrdto = null;

		/* Preenchimento obrigat�rio */
		if (!dto.isSessaoValida())
			throw new GrowUpException(MSGCODE.USUARIO_INFORMAR_SENHA, MensagemBuilder.getMensagem(
					MSGCODE.USUARIO_INFORMAR_SENHA,
					TipoIdioma.IDIOMA_BR.toString(),
					null));

		
		dto.setData(DateUtil.getDateManagerInstance().getDateTime());
		
		/* pesquisa usuario, pelo login ou email e popula membros da classe */
		usrdto = super.getUsuarioBO().procurar(dto.getUsuario());

		if (usrdto == null) {
			throw new GrowUpException(MSGCODE.USUARIO_SENHA_INVALIDA, dto, 
					MensagemBuilder.getMensagem(MSGCODE.USUARIO_SENHA_INVALIDA, TipoIdioma.IDIOMA_BR.toString(),null)
					, null);
		}
		

		try {
			this.validarUsuarioHabilitado(usrdto);
		} catch (GrowUpException e) {
			dto.codMensagem = e.getCode();
			dto.mensagemRetorno = e.getMensagem();
			e.setDto(dto);
			throw e;
		}
		
		/* Busca uma factory para autenticacao */
		Autenticador autenticador = AutenticadorFactory
				.getAutenticadorSistema();
		/* Autentica na factory solicitada */
		autenticador.autenticar(usrdto, dto.getUsuario().getSenha());

		// Grava uma nova sess�o para o usuario
		SessaoDTO novaSessao = this.gerarSessaoUsuario(dto, usrdto);

		novaSessao = this.verificarExclusaoPrevia(novaSessao);

		// Insere a mensagem de conexao bem sucedida.
		novaSessao.mensagemRetorno = MensagemBuilder.getMensagem(
				MSGCODE.USUARIO_CONECTADO_COM_SUCESSO,
				TipoIdioma.IDIOMA_BR.toString(), null);

		this.atualizaDataConexaoUsuario(novaSessao.getUsuario());

		return novaSessao;

	}
	
	/**
	 * @param dto
	 * @return
	 * @throws GrowUpException
	 */
	public /*final*/ SessaoDTO conectarFacebook(SessaoDTO dto)  throws GrowUpException {
		UsuarioDTO usrdto = null;

		/* Preenchimento obrigat�rio */
		if (!dto.isSessaoFacebookValida())
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);

		dto.setData(DateUtil.getDateManagerInstance().getDateTime());
		
		/* pesquisa usuario, pelo login ou email e popula membros da classe */
		usrdto = new UsuarioDTO();
		usrdto.setEmail(dto.getUsuario().getEmail());
		usrdto = super.getUsuarioBO().procurar(usrdto);

		if (usrdto == null) {
			dto.getUsuario().setPerfil(new PerfilDTO(TipoPerfil.USUARIO.toString() +"9"));
			dto.getUsuario().setHabilitado(Constantes.SIM);
			usrdto =  getUsuarioBO().criarUsuario(dto.getUsuario()) ;
		} else {
			
			try {
				this.validarUsuarioHabilitado(usrdto);
			} catch (GrowUpException e) {
				dto.codMensagem = e.getCode();
				dto.mensagemRetorno = e.getMensagem();
				e.setDto(dto);
				throw e;
			}
		}
		
		
		// Grava uma nova sess�o para o usuario
		SessaoDTO novaSessao = this.gerarSessaoUsuario(dto, usrdto);

		novaSessao = this.verificarExclusaoPrevia(novaSessao);

		// Insere a mensagem de conexao bem sucedida.
		novaSessao.mensagemRetorno = MensagemBuilder.getMensagem(
				MSGCODE.USUARIO_CONECTADO_COM_SUCESSO,
				TipoIdioma.IDIOMA_BR.toString(), null);

		this.atualizaDataConexaoUsuario(novaSessao.getUsuario());

		return novaSessao;
	}

	/**
	 * M�todo utilizado para atualizar a data de conex�o do usu�rio.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com os dados do usu�rio.
	 * @return Objeto de transfer�ncia com os dados do usu�rio atualizados.
	 * @throws SigemException
	 *             Caso ocorra algum erro.
	 */
	private UsuarioDTO atualizaDataConexaoUsuario(final UsuarioDTO dto) throws GrowUpException {
		// DAOFactory daofactory = daoFactory.getDAOFactory();
		UsuarioDTO usr = dto;

		try {

			UsuarioDAO dao = daoFactory.getUsuarioDAO();
			usr = dao.updateUltimoAcesso(daoFactory.getConnection(), usr);

			return usr;
		} catch (GrowUpException e) {
			throw e;
		} finally {
			daoFactory.closeCommit();
		}
	}

	/**
	 * Este m�todo � respons�vel por fechar a conexao do usuario
	 * 
	 * @param dto
	 *            Objeto de trasnfer�ncia com os dados da sess�o.
	 * @return Objeto de transfer�ncia com os dados da sess�o.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a alter��o.
	 */
	public /*final*/ SessaoDTO desconectar(final SessaoDTO dto) throws GrowUpException {
		//DAOFactory daofactory = daoFactory.getDAOFactory();
		SessaoDTO dtoAntes = null;
		try {

			//RN4
//			dtoAntes = this.verificarExclusaoPrevia(dto);
			
			SessaoDAO dao = daoFactory.getSessaoDAO();
			dtoAntes = dao.delete(daoFactory.getConnection(), dto);
			dao.apagarSessoesExpiradas(daoFactory.getConnection());

			// Insere a mensagem de conexao bem sucedida.
			UsuarioDTO usr = dto.getUsuario();
			String idioma = (usr == null ? TipoIdioma.IDIOMA_BR.toString() : usr.getIdioma().toString());
			dtoAntes.codMensagem = MSGCODE.USUARIO_DESCONECTADO_COM_SUCESSO;
			dtoAntes.mensagemRetorno = MensagemBuilder.getMensagem(MSGCODE.USUARIO_DESCONECTADO_COM_SUCESSO,
																	idioma,
																	null);

			return dtoAntes;

		} catch (GrowUpDAOException e) {
			throw new GrowUpException(e.getCode(), e.getMensagem());
		} finally {
			daoFactory.closeCommit();
		}

	}

	/**
	 * Este m�todo retorna um objeto do tipo 'SessaoDTO' com uma nova Sessao
	 * criada de acordo com a chave do usuario, o hostname e a hora corrente.
	 * @param usrdto 
	 * 
	 * @param usuariodto
	 *            Objeto de transfer�ncia com os dados do usu�rio.
	 * @param hostname
	 *            Host de conex�o
	 * @return Objeto de transfer�ncia com os dados da sess�o.
	 * @throws GrowUpException
	 */
	public /*final*/ SessaoDTO gerarSessaoUsuario(final SessaoDTO dto, UsuarioDTO usrdto)
			throws GrowUpException {
		if (dto == null || StringUtils.isBlank(dto.getSid()))
			throw new IllegalArgumentException("sid � obrigat�rio");
	
		SessaoDTO sessaodto = procurar(dto);

		// Grava uma nova sess�o para o usuario
//		sessaodto.codigoHash = this.calculateUserSession(dto);

		if (sessaodto == null) {
			sessaodto = new SessaoDTO();
			sessaodto.setSid(dto.getSid());
			sessaodto.setHost(dto.getHost());
			sessaodto.setUsuario(usrdto);
			sessaodto.setAccessToken(dto.getAccessToken());

			registrar(sessaodto);
		} else 
			sessaodto.setUsuario(usrdto);

		return sessaodto;

	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public SessaoDTO verificarExclusaoPrevia(SessaoDTO dto) throws GrowUpException {

		if (this.procurar(dto) == null) {
			//throw GrowUpException.getErroInterno(ErroInterno.USUARIO_SEM_SESSAO);
			throw new GrowUpException(MSGCODE.SESSAO_NAO_INICIADA);
		}

		return dto;
	}
	
	/**
	 * Para categorizar metodo sem sid
	 * 
	 * @param dto
	 * @return
	 * @throws GrowUpException
	 */
	public SessaoDTO procurarSessao(SessaoDTO dto) throws GrowUpException {
		if (dto == null || StringUtils.isBlank(dto.getSid())) 
			throw new IllegalArgumentException("sid is required");
		
		dto = super.procurar(dto);
		
		if (dto != null) {
			dto.setUsuario(super.getUsuarioBO().procurar(dto.getUsuario())); 
		}
		return dto;
	}


}
