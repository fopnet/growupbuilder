package br.com.growupge.business;

import br.com.growupge.dto.UnidadeFederativaDTO;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.stereotype.FirebirdStereoType;

/**
 * Business object para UF
 * 
 * @author Felipe
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class UnidadeFederativa extends ConsultaBase<UnidadeFederativaDTO> {

	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected UnidadeFederativa() {}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getUfDAO();
	}


}
