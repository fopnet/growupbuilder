package br.com.growupge.business;

import static br.com.growupge.util.MensagemBuilder.build;
import static org.apache.commons.lang3.StringUtils.isBlank;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.ArquivoDTO;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.stereotype.FirebirdStereoType;

/**
 * Business object para Marca
 * 
 * @author Felipe
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Arquivo extends NegocioBase<ArquivoDTO> {

	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Arquivo() {}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getArquivoDAO();
	}

	/**
	 * M�todo utilizado para verificar se o nome da Marca existe.
	 * 
	 * @throws GrowUpException
	 *             Caso o nome da Marca n�o exista.
	 */
	private /*final*/ void verificarNome(ArquivoDTO dto) throws GrowUpException {
		ArquivoDTO ldto = null;

		// RN3
		ldto = new ArquivoDTO();
		ldto.setNome(dto.getNome());
		ldto = this.procurar(ldto);

		// Nome existe
		if (ldto != null) {
			// Duplica��o ao inserir
			throw new GrowUpException(MSGCODE.ARQUIVO_EXISTENTE, 
										build(dto.getNome()));
		}

	}

	/**
	 * Verificar se os dados da Marca informada est�o cadastrados na base.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia que cont�m as informa��es da Marca.
	 * @throws GrowUpException
	 *             Caso ocorra o dado n�o seja encontrado na base.
	 */
	private /*final*/ void verificarUso(final ArquivoDTO dto) throws GrowUpException {
		// RN1
		PedidoVeiculoDTO ped = new PedidoVeiculoDTO();
		ped.setArquivo(dto);

		if (getPedidoBO().procurar(ped) !=  null) {
			throw new GrowUpException(MSGCODE.ARQUIVO_EM_USO, 
									build(dto.getNome()));
		}
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.business.NegocioBase#validarCamposObrigatorios(java.lang.Object)
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final ArquivoDTO dto) throws GrowUpException {

		// RN2
		if (isBlank(dto.getNome())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}

	}

	/**
	 * Cria uma nova Marca com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Marca.
	 * @return Um objeto de transfer�ncia com os dados da Marca criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	public /*final*/ ArquivoDTO criarArquivo(final ArquivoDTO dto) throws GrowUpException {

		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		ArquivoDTO retDTO = null;

		try {
			// Se os valores forem v�lidos, atribui a classe.
			this.validarCamposObrigatorios(dto);

			// RN3
			this.verificarNome(dto);

			DAO<ArquivoDTO> dao = daoFactory.getArquivoDAO();
			retDTO = dao.insert(daoFactory.getConnection(), dto);

			retDTO = build(MSGCODE.ARQUIVO_INCLUIDO_COM_SUCESSO, 
							retDTO, 
							retDTO.getNome());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {

			try {
				daoFactory.closeCommit();

			} catch (Exception e) {
				
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}
		return retDTO;
	}

	/**
	 * Alterar a Marca com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Marca.
	 * @return Um objeto de transfer�ncia com os dados da Marca alterada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de altera��o.
	 */
	public /*final*/ ArquivoDTO alterarArquivo(final ArquivoDTO dto) throws GrowUpException {

		ArquivoDTO retDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {

			// RN2
			this.validarCamposObrigatorios(dto);

			//RN4
			this.verificarExclusaoPrevia(dto);

			DAO<ArquivoDTO> dao = daoFactory.getArquivoDAO();
			retDTO = dao.update(daoFactory.getConnection(), dto);

			retDTO = build(MSGCODE.ARQUIVO_ALTERADO_COM_SUCESSO, 
					retDTO, 
					retDTO.getNome());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {

			try {

				daoFactory.closeCommit();

			} catch (Exception e) {
				
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}

		return retDTO;
	}

	/**
	 * Exclui a Marca que cont�m os dados informados no objeto de
	 * transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Marca.
	 * @return Um objeto de transfer�ncia com os dados da Marca criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 */
	public /*final*/ ArquivoDTO excluirArquivo(final ArquivoDTO dto) throws GrowUpException {

		ArquivoDTO retDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {
			this.validarCamposObrigatorios(dto);
			
			//RN4
			this.verificarExclusaoPrevia(dto);
			
			// RN1
			this.verificarUso(dto);

			DAO<ArquivoDTO> dao = daoFactory.getArquivoDAO();
			retDTO = dao.delete(daoFactory.getConnection(), dto);
			
			retDTO = build(MSGCODE.ARQUIVO_REMOVIDO_COM_SUCESSO, 
					retDTO, 
					retDTO.getNome());


		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {

			try {

				daoFactory.closeCommit();

			} catch (Exception e) {
				
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}

		return retDTO;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public ArquivoDTO verificarExclusaoPrevia(ArquivoDTO dto) throws GrowUpException {
		ArquivoDTO dtoAntes = null;

		dtoAntes = this.procurar(dto);

		if (dtoAntes != null) {
			throw new GrowUpException(
					MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,
					build(dto.getNome()));
		}

		return dtoAntes;
	}

}
