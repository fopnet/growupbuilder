package br.com.growupge.business;

import static br.com.growupge.tradutores.TradutorLabels.LABEL_NOME;
import static br.com.growupge.util.MensagemBuilder.build;
import static br.com.growupge.utility.Reflection.isCodigoValido;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.List;
import java.util.Properties;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.ProprietarioDTO;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.dto.VeiculoProprietarioDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.stereotype.FirebirdStereoType;
import br.com.growupge.tradutores.Tradutor;
import br.com.growupge.validador.Validador;

/**
 * Business object para Proprietario
 * 
 * @author Felipe
 * 
 */
/**
 * @author Felipe
 *
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Proprietario extends NegocioBase<ProprietarioDTO> {

	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Proprietario() {}
	
	/**
	 * Construtor para esta classe.
	 * @param dto
	 * @throws GrowUpException
	 */
	public Proprietario(final SessaoDTO dto) throws GrowUpException {
		this();
		this.setSessaodto(dto);
	}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getProprietarioDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}

	/**
	 * M�todo utilizado para verificar se o nome da Modelo existe.
	 * @param dto 
	 * 
	 * @throws GrowUpException
	 *             Caso o nome da Modelo n�o exista.
	 */
	private /*final*/ void verificarNome(ProprietarioDTO dto) throws GrowUpException {

		// RN3
		ProprietarioDTO ldto = new ProprietarioDTO();
		ldto.setNome(dto.getNome());
		ldto = this.procurar(ldto);

		// Nome existe
		if (ldto != null) {

			// Duplica��o ao inserir
			if (!isCodigoValido(dto) || ldto.getCodigo() != dto.getCodigo()) {
				throw new GrowUpException(MSGCODE.PROPRIETARIO_EXISTENTE, build(ldto.getNome()));
			}
		}

	}

	/**
	 * M�todo utilizado para verificar se o nome da Modelo existe.
	 * 
	 * @throws GrowUpException
	 *             Caso o nome da Modelo n�o exista.
	 */
	public /*final*/ void verificarCpfCnpj(final ProprietarioDTO dto) throws GrowUpException {

		if (isBlank(dto.getCpfcnpj()))
			return;
		
		if (!Validador.validarCnpj(dto.getCpfcnpj()) 
				&& !Validador.validarCpf(dto.getCpfcnpj()))
			throw new GrowUpException(MSGCODE.CPF_CNPJ_INVALIDO, build(dto.getCpfcnpj()));
		
		// RN3
		ProprietarioDTO ldto = new ProprietarioDTO();
		ldto.setCpfcnpj(dto.getCpfcnpj());
		ldto = this.procurar(dto);

		// Nome existe
		if (ldto != null) {

			// Duplica��o ao inserir
			if (!isCodigoValido(dto)|| ldto.getCodigo() != dto.getCodigo()) {
				throw new GrowUpException(MSGCODE.CPF_EXISTENTE, build(dto.getCpfcnpj()));
			}
		}

	}

	/**
	 * Verificar se os dados da Proprietario informado est� vinculado a mais de uma placa
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia que cont�m as informa��es da Modelo
	 * @throws GrowUpException
	 *             Caso ocorra o dado n�o seja encontrado na base.
	 */
	private /*final*/ void verificarUso(final ProprietarioDTO dto) throws GrowUpException {
		List<VeiculoProprietarioDTO> associacoes = null;

		// RN1
		VeiculoProprietarioDTO param = new VeiculoProprietarioDTO();
		param.setProprietario(dto);

		associacoes = new VeiculoProprietario().buscarTodas(param);
		if (associacoes.size() > 1) {
			StringBuffer veiculos = new StringBuffer();
			for (VeiculoProprietarioDTO item : associacoes) {
				if (!item.getProprietario().equals(dto)) {
					veiculos.append(",");
					veiculos.append(item.getVeiculo().getCodigo());
				}
			}
			throw new GrowUpException(MSGCODE.MARCA_EM_USO, 
									build( dto.getNome(), veiculos.substring(1).toString()));
		}
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.business.NegocioBase#validarCamposObrigatorios(java.lang.Object)
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final ProprietarioDTO dto)
			throws GrowUpException {

		// RN2
		if (isBlank(dto.getNome())) {
			Properties rb = Tradutor.getInstance(getIdiomaUsuario());
			throw new GrowUpException(MSGCODE.CAMPOX_OBRIGATORIOS, build(rb.getProperty(LABEL_NOME)));
		}
		if (isBlank(dto.getCpfcnpj())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}


	}

	/**
	 * Cria uma nova Modelo com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Modelo
	 * @return Um objeto de transfer�ncia com os dados da Modelo criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	public /*final*/ ProprietarioDTO criarProprietario(final ProprietarioDTO dto)
			throws GrowUpException {

		ProprietarioDTO retDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {
			// RN4
			this.verificarCpfCnpj(dto);

			// Se os valores forem v�lidos, atribui a classe.
			this.validarCamposObrigatorios(dto);

			// RN3
			this.verificarNome(dto);


			DAO<ProprietarioDTO> dao = daoFactory.getProprietarioDAO();
			retDTO = dao.insert(daoFactory.getConnection(), dto);

			retDTO = build(MSGCODE.PROPRIETARIO_INCLUIDO_COM_SUCESSO, 
							retDTO,  retDTO.getNome());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
		} finally {
			daoFactory.closeCommit();
		}
		return retDTO;
	}

	/**
	 * Alterar a Modelo com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Modelo
	 * @return Um objeto de transfer�ncia com os dados da Modelo alterada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de altera��o.
	 */
	public /*final*/ ProprietarioDTO alterarProprietario(final ProprietarioDTO dto)
			throws GrowUpException {

		ProprietarioDTO retDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		try {

			// RN2
			this.validarCamposObrigatorios(dto);
			
			//RN4
			this.verificarExclusaoPrevia(dto);
			
			// RN3
			this.verificarNome(dto);
			
			//RN4
			this.verificarCpfCnpj(dto);

			DAO<ProprietarioDTO> dao = daoFactory.getProprietarioDAO();
			retDTO = dao.update(daoFactory.getConnection(), dto);

			retDTO = build(MSGCODE.PROPRIETARIO_ALTERADO_COM_SUCESSO, 
							retDTO, retDTO.getNome());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {
			daoFactory.closeCommit();
		}

		return retDTO;
	}

	/**
	 * Exclui a Modelo que cont�m os dados informados no objeto de
	 * transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Modelo
	 * @return Um objeto de transfer�ncia com os dados da Modelo criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 */
	public /*final*/ ProprietarioDTO excluirProprietario(final ProprietarioDTO dto)
			throws GrowUpException {

		ProprietarioDTO retDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {
			//RN4
			this.verificarExclusaoPrevia(dto);
			
			// RN1
			this.verificarUso(dto);

			DAO<ProprietarioDTO> dao = daoFactory.getProprietarioDAO();
			retDTO = dao.delete(daoFactory.getConnection(), dto);

			retDTO = build(MSGCODE.PROPRIETARIO_REMOVIDO_COM_SUCESSO, 
							retDTO, retDTO.getNome());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {
			daoFactory.closeCommit();
		}

		return retDTO;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public ProprietarioDTO verificarExclusaoPrevia(ProprietarioDTO dto) throws GrowUpException {
		ProprietarioDTO dtoAntes = null;

		dtoAntes = this.procurar(dto);

		if (dtoAntes == null) {
			throw new GrowUpException(
					MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,
					build(dto.getNome()));
		}

		return dtoAntes;
	}

}
