package br.com.growupge.business;

import static br.com.growupge.util.MensagemBuilder.build;
import static br.com.growupge.utility.Reflection.isCodigoValido;
import static br.com.growupge.utility.Reflection.isPropriedadeValida;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;

import br.com.growupge.builder.AnexoDadoVeicularBuilder;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.dto.ProprietarioDTO;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.dto.VeiculoDTO;
import br.com.growupge.dto.VeiculoProprietarioDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.stereotype.FirebirdStereoType;
import br.com.growupge.util.FileUtil;
import br.com.growupge.util.MensagemBuilder;

/**
 * Business object para Veiculo
 * 
 * @author Felipe
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Veiculo extends NegocioBase<VeiculoDTO> {


	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Veiculo() {}

	/**
	 * @param sessaodto
	 * @throws GrowUpException 
	 */
	public Veiculo(SessaoDTO sessaodto) throws GrowUpException {
		this();
		setSessaodto(sessaodto);
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getVeiculoDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}


	/**
	 * M�todo utilizado para verificar se o nome da Modelo existe.
	 * 
	 * @throws GrowUpException
	 *             Caso o nome da Modelo n�o exista.
	 */
	private /*final*/ void verificarPlaca(VeiculoDTO dto) throws GrowUpException {
		// RN3
		VeiculoDTO ldto = new VeiculoDTO();
		ldto.setCodigo(dto.getCodigo());
		ldto = this.procurar(ldto);

		// Nome existe
		if (ldto != null) {
			throw new GrowUpException(MSGCODE.VEICULO_EXISTENTE, MensagemBuilder.build(ldto.getCodigo()));
		}

	}

	/**
	 * Verificar se os dados da Proprietario informado est� vinculado a mais de
	 * uma placa
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia que cont�m as informa��es da Veiculo
	 * @throws GrowUpException
	 *             Caso ocorra o dado n�o seja encontrado na base.
	 */
	private /*final*/ void verificarUso(final VeiculoDTO dto) throws GrowUpException {

		// RN1
		PedidoVeiculoDTO param = new PedidoVeiculoDTO();
		param.setPlaca( dto.getCodigo() );

		if ((param = getPedidoBO().procurar(param)) != null) {
			throw new GrowUpException(MSGCODE.VEICULO_EM_USO, 
										build(dto.getCodigo(), param.getCodigo()));
		}
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.business.NegocioBase#validarCamposObrigatorios(java.lang.Object)
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final VeiculoDTO dto) throws GrowUpException {

		// RN2
		if ((dto == null) || isBlank(dto.getCodigo())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}

	}


	/**
	 * Buscar o id, caso n�o encontre, cria
	 * @param dto
	 * @throws GrowUpException 
	 */
	private void buscarCriarProprietario(VeiculoDTO dto) throws GrowUpException {
		if (isPropriedadeValida(dto.getProprietario(), "cpfcnpj")) {
			Proprietario prop = getProprietarioBO();
			ProprietarioDTO lprop = prop.procurar(dto.getProprietario());
			if (lprop == null) {
				dto.setProprietario(prop.criarProprietario(dto.getProprietario()));
			} else {
				dto.setProprietario(lprop);
			}
		}
	}

	/**
	 * Cria uma nova Modelo com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Veiculo
	 * @return Um objeto de transfer�ncia com os dados da Modelo criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	public /*final*/ VeiculoDTO criarVeiculo(final VeiculoDTO dto) throws GrowUpException {
		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		try {

			VeiculoDTO retDTO = null;

			// Se os valores forem v�lidos, atribui a classe.
			this.validarCamposObrigatorios(dto);

			// RN3
			this.verificarPlaca(dto);
			
			this.buscarCriarProprietario(dto);
			
			DAO<VeiculoDTO> dao = daoFactory.getVeiculoDAO();
			retDTO = dao.insert(daoFactory.getConnection(), dto);

			retDTO = build(MSGCODE.VEICULO_INCLUIDO_COM_SUCESSO,
							retDTO, 
							retDTO.getCodigo());

			// Caso o proprietario esteja preenchido
			if (isCodigoValido(retDTO.getProprietario()))
				getVeiculoProprietario().adicionarVeiculoProprietario(new VeiculoProprietarioDTO(retDTO));
			
			
			return retDTO;

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			daoFactory.closeCommit();
		}

	}

	/**
	 * Alterar a Modelo com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Veiculo
	 * @return Um objeto de transfer�ncia com os dados da Modelo alterada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de altera��o.
	 */
	public /*final*/ VeiculoDTO alterarVeiculo(final VeiculoDTO dto) throws GrowUpException {

		VeiculoDTO retDTO = null;

		// RN2
		this.validarCamposObrigatorios(dto);

		//RN4
		this.verificarExclusaoPrevia(dto);
		
		this.buscarCriarProprietario(dto);
		
		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		try {
			DAO<VeiculoDTO> dao = daoFactory.getVeiculoDAO();
			retDTO = dao.update(daoFactory.getConnection(), dto);

			retDTO = build(MSGCODE.VEICULO_ALTERADO_COM_SUCESSO, retDTO, retDTO.getCodigo());

			if (dto.getProprietario() != null && dto.getProprietario().getCodigo() > 0) {
			//Adiciona o relaciomento aqui pela Interface o proprietario eh obrigatorio
				getVeiculoProprietario().adicionarVeiculoProprietario(new VeiculoProprietarioDTO(retDTO));
			}

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {
			daoFactory.closeCommit();
		}

		return retDTO;
	}

	/**
	 * Exclui a Modelo que cont�m os dados informados no objeto de
	 * transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Veiculo
	 * @return Um objeto de transfer�ncia com os dados da Modelo criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 */
	public /*final*/ VeiculoDTO excluirVeiculo(final VeiculoDTO dto) throws GrowUpException {

		VeiculoDTO retDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {
			//RN4
			this.verificarExclusaoPrevia(dto);

			// RN1
			this.verificarUso(dto);

			DAO<VeiculoDTO> dao = daoFactory.getVeiculoDAO();
			retDTO = dao.delete(daoFactory.getConnection(), dto);

			retDTO = build(MSGCODE.VEICULO_REMOVIDO_COM_SUCESSO, 
							retDTO, 
							retDTO.getCodigo());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			daoFactory.closeCommit();
		}

		return retDTO;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public VeiculoDTO verificarExclusaoPrevia(VeiculoDTO dto) throws GrowUpException {
		VeiculoDTO dtoAntes = null;

		dtoAntes = this.procurar(dto);

		if (dtoAntes == null) {
			throw new GrowUpException(
					MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,
					build(dto.getCodigo()));
		}

		return dtoAntes;
	}
	
	/**
	 * @param nomeArquivo
	 * @return
	 * @throws GrowUpException
	 */
	public VeiculoDTO importarDados(final VeiculoDTO dto, final String nomeArquivo) throws GrowUpException {
		
		validarCamposObrigatorios(dto);
		
		File uploadFile = FileUtil.getUploadFile(nomeArquivo);
		try {
			InputStream is = new FileInputStream(uploadFile);
			final VeiculoDTO retorno = new AnexoDadoVeicularBuilder(is)
											.lerTodosCampos()
											.build();
			
			if (!dto.equals(retorno)) {
				throw new GrowUpException(MSGCODE.DADOS_INCOMPATIVEIS_COM_REGISTRO);
			}
			
			MensagemBuilder.build(MSGCODE.DADOS_IMPORTADO_COM_SUCESSO, retorno);
			
			
			return retorno;
		} catch (IOException e) {
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
		} finally {
			FileUtils.deleteQuietly(uploadFile);
		}
	}

}
