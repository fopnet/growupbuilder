package br.com.growupge.business;

import static br.com.growupge.util.MensagemBuilder.build;
import static br.com.growupge.utility.Reflection.isCodigoValido;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.List;

import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.ClienteDTO;
import br.com.growupge.dto.EmpresaItemDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.stereotype.FirebirdStereoType;

/**
 * Business object para empresa.
 * 
 * @author Felipe
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Cliente extends NegocioBase<ClienteDTO> {

	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Cliente() {}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getClienteDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}

	/**
	 * M�todo utilizado para verificar se o nome da empresa existe.
	 * 
	 * @throws GrowUpException
	 *             Caso o nome da empresa n�o exista.
	 */
	private /*final*/ void verificarNome(ClienteDTO dto) throws GrowUpException {

		// RN3
		ClienteDTO ldto = new ClienteDTO();
		dto.setNome(dto.getNome());
		ldto = this.procurar(dto);

		// Nome existe
		if (ldto != null) {

			// Duplica��o ao inserir
			if (!isCodigoValido(ldto) || ldto.getCodigo() != dto.getCodigo()) {
				throw new GrowUpException(MSGCODE.CLIENTE_EXISTENTE);
			}
		}

	}

	/**
	 * Verificar se os dados da empresa informada est�o cadastrados na base.
	 * 
	 * @param dtoCliente
	 *            Objeto de transfer�ncia que cont�m as informa��es da empresa.
	 * @throws GrowUpException
	 *             Caso ocorra o dado n�o seja encontrado na base.
	 */
	private /*final*/ void verificarUsoCliente(final ClienteDTO dtoCliente) throws GrowUpException {
		// RN1
		EmpresaItemDTO item = new EmpresaItemDTO();
		item.cliente = dtoCliente;

		if (!getEmpresaBO().buscarTodas(item).isEmpty()) {
			throw new GrowUpException(MSGCODE.CLIENTE_EM_USO, build(dtoCliente.getNome()));
		}
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.business.NegocioBase#validarCamposObrigatorios(java.lang.Object)
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final ClienteDTO dto) throws GrowUpException {

		// RN2
		if (isBlank(dto.getNome())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
	}

	/**
	 * Cria uma nova empresa com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da empresa.
	 * @return Um objeto de transfer�ncia com os dados da empresa criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	public /*final*/ ClienteDTO criarCliente(final ClienteDTO dto) throws GrowUpException {

		ClienteDTO retorno = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {
			// Se os valores forem v�lidos, atribui a classe.
			this.validarCamposObrigatorios(dto);

			// RN3
			this.verificarNome(dto);

			
			DAO<ClienteDTO> dao = daoFactory.getClienteDAO();
			retorno = dao.insert(daoFactory.getConnection(), dto);

			retorno = build(MSGCODE.CLIENTE_INCLUIDO_COM_SUCESSO, 
							retorno, 
							retorno.getNome());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			daoFactory.closeCommit();
		}
		return retorno;
	}

	/**
	 * Alterar a empresa com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da empresa.
	 * @return Um objeto de transfer�ncia com os dados da empresa alterada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de altera��o.
	 */
	public /*final*/ ClienteDTO alterarCliente(final ClienteDTO dto) throws GrowUpException {

		ClienteDTO clienteDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {

			// RN2
			this.validarCamposObrigatorios(dto);

			// RN4
			this.verificarExclusaoPrevia(dto);

			// RN3
			this.verificarNome(dto);

			DAO<ClienteDTO> dao = daoFactory.getClienteDAO();
			clienteDTO = dao.update(daoFactory.getConnection(), dto);

			clienteDTO = build( MSGCODE.CLIENTE_ALTERADO_COM_SUCESSO, 
								clienteDTO, 
								clienteDTO.getNome());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {
			daoFactory.closeCommit();
		}

		return clienteDTO;
	}

	/**
	 * Exclui a empresa que cont�m os dados informados no objeto de
	 * transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da empresa.
	 * @return Um objeto de transfer�ncia com os dados da empresa criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 */
	public /*final*/ ClienteDTO excluirCliente(final ClienteDTO dto) throws GrowUpException {

		ClienteDTO dtoCliente = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {
			// RN4
			this.verificarExclusaoPrevia(dto);

			// RN1
			this.verificarUsoCliente(dto);
			
			DAO<ClienteDTO> dao = daoFactory.getClienteDAO();
			dtoCliente = dao.delete(daoFactory.getConnection(), dto);
			dtoCliente = build(MSGCODE.CLIENTE_REMOVIDO_COM_SUCESSO, 
								dtoCliente, 
								dtoCliente.getNome());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {
			daoFactory.closeCommit();
		}

		return dtoCliente;
	}

	/**
	 * Retorna lista de clientes para exibir na interface.
	 * 
	 * @return
	 * @throws GrowUpException
	 */
	public /*final*/ List<ClienteDTO> buscarClientes() throws GrowUpException {
		return this.buscarTodas();
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public ClienteDTO verificarExclusaoPrevia(final ClienteDTO dto) throws GrowUpException {
		ClienteDTO dtoAntes = null;

		dtoAntes = this.procurar(dto);

		if (dtoAntes == null) {
			throw new GrowUpException(
					MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,
					build(dto.getNome()));
		}

		return dtoAntes;
	}
}
