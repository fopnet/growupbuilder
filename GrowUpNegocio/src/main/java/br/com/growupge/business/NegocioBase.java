package br.com.growupge.business;

import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.BeanFactory;

/**
 * Classe abstrata usada por qualquer objeto de neg�cio
 * 
 * @author Felipe
 * 
 * @param <DTO>
 *            Classe parametrizada com algum DTo
 */
public abstract class NegocioBase<DTO> extends ConsultaBase<DTO> {

	/**
	 * Atributo '<code>nomeObjetoDTOAuditar</code>' do tipo String
	 */
	//private String nomeObjetoDTOAuditar = null;
	
	/**
	 * Construtor para esta classe.
	 * 
	 * @param dao
	 *            Classe de acesso a dados para consulta
	 */
//	protected NegocioBase(final ConsultaDAO<DTO> dao) {
//		super(dao);
//	}

	/**
	 * M�todo utilizado para verificar se os valores informados no objeto de
	 * transfer�ncia est�o com dados v�lidos.
	 * 
	 * @param dto
	 *            Objecto de transfer�ncia que cont�m os valores que ser�o
	 *            validados.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a verifica��o dos dados do
	 *             dto.
	 */
	protected void validarCamposObrigatorios(DTO dto) throws GrowUpException {}

	/**
	 * Este m�todo � respons�vel por retornar o conte�do de
	 * 'nomeObjetoDTOAuditar'.
	 * 
	 * @return Retorna um objeto do tipo String
	public final String getNomeObjetoDTOAuditar() {
		return this.nomeObjetoDTOAuditar;
	}
	 */

	/**
	 * Este m�todo � respons�vel por retornar o conte�do de 'sessaordto'.
	 * 
	 * @return Retorna um objeto do tipo SessaoDTO
	 */
	protected /*final*/ SessaoDTO getSessao() {
		return this.daoFactory.getSessao();
	}
	
	protected UsuarioDTO getUsuarioSessao() {
		return getSessao().getUsuario();
	}
	
	protected String getIdiomaUsuario() {
		return getSessao().getUsuario().getIdioma();
	}
	

	/**
	 * Verifica se o item jah foi exclu�do
	 *
	 * @param dto
	 * @return
	 * @throws GrowUpException Caso ocorra algum erro na opera��o
	 */
	public abstract DTO verificarExclusaoPrevia(DTO dto) throws GrowUpException;

	//////////////////////// Fabrica de Objetos de negocio
	protected Sessao getSessaoBO() throws GrowUpException {
		return BeanFactory.getContextualInstance(Sessao.class, getSessao());
	}

	protected PedidoVeiculo getPedidoBO() throws GrowUpException {
		return BeanFactory.getContextualInstance(PedidoVeiculo.class, getSessao());
	}

	protected Usuario getUsuarioBO() throws GrowUpException {
		return BeanFactory.getContextualInstance(Usuario.class, getSessao());
	}

	protected Veiculo getVeiculoBO() throws GrowUpException {
		return BeanFactory.getContextualInstance(Veiculo.class, getSessao());
	}

	protected Tramitacao getTramitacaoBO() throws GrowUpException {
		return BeanFactory.getContextualInstance(Tramitacao.class, getSessao());
	}
	
	protected VeiculoProprietario getVeiculoProprietario() throws GrowUpException {
		return BeanFactory.getContextualInstance(VeiculoProprietario.class, getSessao());
	}

	protected Modelo getModeloBO() throws GrowUpException {
		return BeanFactory.getContextualInstance(Modelo.class, getSessao());
	}

	protected Empresa getEmpresaBO() throws GrowUpException {
		return BeanFactory.getContextualInstance(Empresa.class, getSessao());
	}

	protected Proprietario getProprietarioBO() throws GrowUpException {
		return BeanFactory.getContextualInstance(Proprietario.class, getSessao());
	}

	protected Variavel getVariavelBO() throws GrowUpException {
		return BeanFactory.getContextualInstance(Variavel.class, getSessao());
	}
	
	protected Permissao getPermissaoBO() {
		return BeanFactory.getContextualInstance(Permissao.class);
	}

	protected Perfil getPerfilBO() {
		return BeanFactory.getContextualInstance(Perfil.class, getSessao());
	}

	protected Servico getServicoBO() {
		return BeanFactory.getContextualInstance(Servico.class, getSessao());
	}

	protected Mensagem getMensagemBO() {
		return BeanFactory.getContextualInstance(Mensagem.class);
	}

	protected HistoricoSenha getHistoricoSenhaBO() {
		return  BeanFactory.getContextualInstance(HistoricoSenha.class, getSessao());
	}

}
