/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 22/12/2006
 *
 * Historico de Modifica��o:
 * =========================
 * 22/12/2006 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.business;

import static br.com.growupge.util.MensagemBuilder.build;
import static br.com.growupge.utility.Reflection.isCodigoValido;
import static org.apache.commons.lang3.StringUtils.isBlank;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.CargoDTO;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.stereotype.FirebirdStereoType;

/**
 * Business object para controle de permiss�es.
 * 
 * @author Felipe
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Cargo extends NegocioBase<CargoDTO> {


	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Cargo() {}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getCargoDAO();
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.business.NegocioBase#validarCamposObrigatorios(java.lang.Object)
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final CargoDTO dto) throws GrowUpException {
		if (!isCodigoValido(dto)) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
		if (isBlank(dto.getDescricao())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
		if (!isCodigoValido(dto.getDepartamento())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}

	}

	/**
	 * Metodo que verifica se a permissao existe
	 * 
	 * @throws GrowUpException
	 *             Comentar aqui.
	 */
	private /*final*/ void verificarCodigo(CargoDTO dto) throws GrowUpException {

		CargoDTO cargoDTO = this.procurar(new CargoDTO(dto.getCodigo()));

		if (cargoDTO != null) {

			if (dto.getCodigo().equals(cargoDTO.getCodigo())) {
				throw new GrowUpException(MSGCODE.PERMISSAO_EXISTENTE);

			}
		}
	}

	/**
	 * M�todo utilizado para a cria��o de uma nova permiss�o.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com os dados da permiss�o a ser
	 *            criada.
	 * @return Objeto de transfer�ncia com os dados da permiss�o.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a cria��o da nova permiss�o.
	 */
	public /*final*/ CargoDTO criarCargo(final CargoDTO dto) throws GrowUpException {
		//DAOFactory daofactory = daoFactory.getDAOFactory();
		CargoDTO newDTO = null;

		try {
			/* Valida se campo est� em branco */
			this.validarCamposObrigatorios(dto);

			this.verificarCodigo(dto);

			DAO<CargoDTO> dao = daoFactory.getCargoDAO();

			newDTO = dao.insert(daoFactory.getConnection(), dto);
			newDTO = build(MSGCODE.CARGO_INCLUIDO_COM_SUCESSO, newDTO , newDTO.getCodigo());  

		} catch (GrowUpDAOException e) {
			throw e;
		} finally {
			daoFactory.closeCommit();
		}

		return newDTO;

	}

	/**
	 * M�todo utilizado para a alterar a permiss�o informada.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com os dados da permiss�o a ser
	 *            alterada.
	 * @return CargoDTO Objeto de transfer�ncia com dados da permiss�o.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a altera��o da permiss�o.
	 */
	public /*final*/ CargoDTO alterarCargo(final CargoDTO dto) throws GrowUpException {
		// DAOFactory daofactory = daoFactory.getDAOFactory();
		CargoDTO newDTO = null;
		try {

			/* Valida se campo est� em branco */
			this.validarCamposObrigatorios(dto);

			
			DAO<CargoDTO> dao = daoFactory.getCargoDAO();

			newDTO = dao.update(daoFactory.getConnection(), dto);
			newDTO = build(MSGCODE.CARGO_ALTERADO_COM_SUCESSO, newDTO , newDTO.getCodigo());  

		} catch (GrowUpDAOException e) {
			daoFactory.rollBack();
			throw e;
		} finally {
			daoFactory.closeCommit();
		}
		return newDTO;

	}

	/**
	 * M�todo utilizado para excluir permiss�o que cont�m o c�digo informado no
	 * transfer object.
	 * 
	 * @param dto
	 *            Transfer object que cont�m a permiss�o a ser excluida.
	 * @throws GrowUpException
	 *             Caso ocorra algum problema durante a opera��o de exclus�o.
	 * @return CargoDTO Objeto de transfer�ncia com os dados da permiss�o
	 *         exclu�da.
	 */
	public /*final*/ CargoDTO excluirCargo(final CargoDTO dto) throws GrowUpException {
		CargoDTO newDTO = null;
		//DAOFactory daofactory = daoFactory.getDAOFactory();
		try {

			
			DAO<CargoDTO> dao = daoFactory.getCargoDAO();

			// TODO Verificar associa��o no curriculo
			// this.verificaAssociacao(dto);

			newDTO = dao.delete(daoFactory.getConnection(), dto);

			newDTO = build(MSGCODE.CARGO_EXCLUIDO_COM_SUCESSO, newDTO , newDTO.getCodigo());  

		} catch (GrowUpDAOException e) {
			daoFactory.rollBack();
			throw e;
		} finally {
			daoFactory.closeCommit();
		}

		return newDTO;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public CargoDTO verificarExclusaoPrevia(CargoDTO dto) throws GrowUpException {
		return null;
	}

}
