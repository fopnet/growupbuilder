package br.com.growupge.business;

import static br.com.growupge.util.MensagemBuilder.build;
import static br.com.growupge.utility.Reflection.isCodigoValido;

import java.util.List;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.PerfilDTO;
import br.com.growupge.dto.PerfilPermissaoItemDTO;
import br.com.growupge.dto.PermissaoDTO;
import br.com.growupge.dto.UsuarioDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpDAOException;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.factory.ParametroFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.interfacesdao.PerfilDAO;
import br.com.growupge.stereotype.FirebirdStereoType;

/**
 * Business object para empresa.
 * 
 * @author Felipe
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Perfil extends NegocioGrupoBase<PerfilDTO, PerfilPermissaoItemDTO> {

	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Perfil() {}

	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getPerfilDAO();
		super.crudGrupoDAO = daoFactory.getPerfilPermissaoDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}

	/**
	 * M�todo utilizado para verificar se o c�digo da empresa existe.
	 * 
	 * @throws GrowUpException
	 *             Caso o c�digo da empresa n�o exista.
	 */
	private /*final*/ void verificarCodigo(PerfilDTO dto) throws GrowUpException {
		// FE5
		PerfilDTO ldto = new PerfilDTO();
		ldto.setCodigo(dto.getCodigo());

		if (this.procurar(ldto) != null) {
			throw new GrowUpException(MSGCODE.PERFIL_EXISTENTE, build( dto.getCodigo() ));
		}

	}

	/**
	 * Verificar se os dados da empresa informada est�o cadastrados na base.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia que cont�m as informa��es da empresa.
	 * @throws GrowUpException
	 *             Caso ocorra o dado n�o seja encontrado na base.
	 */
	private /*final*/ void verificarUso(PerfilDTO dto) throws GrowUpException {
		UsuarioDTO usr = new UsuarioDTO();
		usr.setPerfil(dto);

		usr = getUsuarioBO().procurar(usr);
		if (usr != null) {
			throw new GrowUpException(MSGCODE.PERFIL_REGISTROS_ASSOCIADOS, build( usr.getCodigo() ));
		}
	}

	/**
	 * Verifica se o item j� existe.
	 * 
	 * @throws SigemException
	 *             Comentar aqui.
	 */
	private /*final*/ void verificarItemDuplicado(PerfilPermissaoItemDTO dto) throws GrowUpException {
		PerfilPermissaoItemDTO ldto = new PerfilPermissaoItemDTO();
		ldto.setPermissao(dto.getPermissao());
		ldto.setPerfil(dto.getPerfil());

		if (this.procurar(ldto) != null) {

			throw new GrowUpException(MSGCODE.PERFIL_PERMISSAO_DUPLICADO, 
									build(ldto.getPermissao().getCodigo(), ldto.getPerfil().getCodigo()));

		}
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.business.NegocioBase#validarCamposObrigatorios(java.lang.Object)
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final PerfilDTO dto) throws GrowUpException {

		// RN2
		if (!isCodigoValido(dto)) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.growupge.business.NegocioGrupoBase#validarCamposObrigatorios(br.com.growupge.dto.DTOPadrao)
	 */
	@Override
	protected void validarCamposObrigatorios(PerfilPermissaoItemDTO dto) throws GrowUpException {
		// RN2
		if (!isCodigoValido( dto.getPerfil() )) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
		if (!isCodigoValido( dto.getPermissao() )) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}

	}

	/**
	 * Cria uma nova empresa com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da empresa.
	 * @return Um objeto de transfer�ncia com os dados da empresa criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	public /*final*/ PerfilDTO criarPerfil(final PerfilDTO dto) throws GrowUpException {

		PerfilDTO dtoPerfil = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {
			// Se os valores forem v�lidos, atribui a classe.
			this.validarCamposObrigatorios(dto);

			// RN3
			this.verificarCodigo(dto);

			DAO<PerfilDTO> dao = daoFactory.getPerfilDAO();
			dtoPerfil = dao.insert(daoFactory.getConnection(), dto);

			dtoPerfil = build( MSGCODE.PERFIL_INCLUIDO_COM_SUCESSO, dtoPerfil, dtoPerfil.getCodigo());  

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			daoFactory.closeCommit();
		}
		return dtoPerfil;
	}

	/**
	 * Alterar a empresa com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da empresa.
	 * @return Um objeto de transfer�ncia com os dados da empresa alterada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de altera��o.
	 */
	public /*final*/ PerfilDTO alterarPerfil(final PerfilDTO dto) throws GrowUpException {

		PerfilDTO dtoPerfil = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {

			// RN2
			this.validarCamposObrigatorios(dto);

			//RN4
			this.verificarExclusaoPrevia(dto);

			DAO<PerfilDTO> dao = daoFactory.getPerfilDAO();
			dtoPerfil = dao.update(daoFactory.getConnection(), dto);

			dtoPerfil = build( MSGCODE.PERFIL_ALTERADO_COM_SUCESSO, dtoPerfil, dtoPerfil.getCodigo());  

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {
			daoFactory.closeCommit();
		}

		return dtoPerfil;
	}

	/**
	 * Exclui a empresa que cont�m os dados informados no objeto de
	 * transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da empresa.
	 * @return Um objeto de transfer�ncia com os dados da empresa criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 */
	public /*final*/ PerfilDTO excluirPerfil(final PerfilDTO dto) throws GrowUpException {

		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		PerfilDTO dtoPerfil = null;

		try {
			this.validarCamposObrigatorios(dto);

			//RN4
			this.verificarExclusaoPrevia(dto);
			
			// RN1
			this.verificarUso(dto);

			DAO<PerfilDTO> dao = daoFactory.getPerfilDAO();
			dtoPerfil = dao.delete(daoFactory.getConnection(), dto);

			dtoPerfil = build( MSGCODE.PERFIL_EXCLUIDO_COM_SUCESSO, dtoPerfil, dtoPerfil.getCodigo());  

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {
			daoFactory.closeCommit();
		}

		return dtoPerfil;
	}

	/**
	 * M�todo adiciona um servi�o ao grupo de servidor
	 * 
	 * @param dto
	 *            Parametro do tipo PerfilPermissaoDTO
	 * @throws SigemException
	 *             Caso ocorra algum erro durante a opera��o.
	 * @return Retorna um objeto do tipo 'PerfilPermissaoDTO'
	 */
	public /*final*/ PerfilPermissaoItemDTO adicionarPerfilPermissao(final PerfilPermissaoItemDTO dto)
			throws GrowUpException {

		PerfilPermissaoItemDTO itemDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {

			this.validarCamposObrigatorios(dto);

			// Atualizar os dados do grupo
			if (this.procurar(dto.getPerfil()) == null) {
				throw new GrowUpException(MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO, build(dto.getPerfil().getCodigo()));
			}

			// Verificar se o item nao foi apagado
			PermissaoDTO permAntes = getPermissaoBO().procurar(dto.getPermissao());
			if (permAntes == null) {
				throw new GrowUpException(MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,  build(dto.getPermissao().getCodigo()));
			}
			dto.setPermissao(permAntes);

			this.verificarItemDuplicado(dto);

			DAO<PerfilPermissaoItemDTO> dao = daoFactory.getPerfilPermissaoDAO();
			itemDTO = dao.insert(daoFactory.getConnection(), dto);

			itemDTO = build( MSGCODE.PERFIL_PERMISSAO_INCLUIDO_COM_SUCESSO,
							itemDTO, 
							itemDTO.getPerfil().getCodigo(),  
							itemDTO.getPermissao().getCodigo());

			return itemDTO;

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			daoFactory.closeCommit();
		}
	}

	/**
	 * M�todo utilizado para excluir lista de servidores cujo o c�digo � igual
	 * ao informado no transfer object.
	 * 
	 * @param dto
	 *            Transfer object que cont�m o c�digo a ser excluido.
	 * @throws GrowUpException
	 *             Caso ocorra algum problema durante a opera��o de exclus�o.
	 * @return GrupoServidorDTO Objeto de transfer�ncia com os valores do grupo
	 *         servidor utilizado ou <code>null</code> caso ocorra algum erro
	 *         durante a exclus�o.
	 */
	public /*final*/ PerfilPermissaoItemDTO removerPerfilPermissao(final PerfilPermissaoItemDTO dto)
			throws GrowUpException {

		PerfilPermissaoItemDTO itemDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {

			this.validarCamposObrigatorios(dto);

			// Verificando se o item ainda existe.
			if (this.procurar(dto) == null) {
				throw new GrowUpException(MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO, build(dto.getPermissao().getCodigo()));
			}

			// Atualizar os dados do grupo
			PerfilDTO perfilAntes = this.procurar(dto.getPerfil());
			if (perfilAntes == null) {
				throw new GrowUpException(MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO, build(dto.getPerfil().getCodigo()));
			}
			dto.setPerfil(perfilAntes);

			DAO<PerfilPermissaoItemDTO> dao = daoFactory.getPerfilPermissaoDAO();
			itemDTO = dao.delete(daoFactory.getConnection(), dto);

			itemDTO = build( MSGCODE.EMPRESA_ITEM_REMOVIDO_COM_SUCESSO,
					itemDTO, 
					itemDTO.getPermissao().getCodigo(),  
					itemDTO.getPerfil().getCodigo());

			return itemDTO;
			
		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			daoFactory.closeCommit();
		}
	}

	/**
	 * Busca os perfis de acordo com o perfil do usu�rio
	 * 
	 * @param dto
	 * @return
	 * @throws GrowUpException
	 */
	public /*final*/ List<PerfilDTO> buscarPerfilUsuario(final PerfilDTO dto) throws GrowUpException {
		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		try {
			
			PerfilDAO<PerfilDTO> dao = daoFactory.getPerfilDAO();
			ParametroFactory param = dao.setParamPerfil(dto);
			return dao.selectAll(daoFactory.getConnection(), false,
					Constantes.DESCONSIDERAR_PAGINACAO, param);

		} catch (GrowUpDAOException ex) {
			throw ex;
		} finally {
			daoFactory.closeCommit();
		}
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public PerfilDTO verificarExclusaoPrevia(PerfilDTO dto) throws GrowUpException {
		PerfilDTO dtoAntes = null;

		dtoAntes = this.procurar(dto);

		if (dtoAntes == null) {
			throw new GrowUpException(
					MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,
					build(dto.getCodigo()));
		}

		return dtoAntes;
	}

}
