package br.com.growupge.business;

import java.util.List;

import br.com.growupge.exception.GrowUpException;

/**
 * Classe usada pelos objetos de neg�cios para consulta
 * 
 * @author Felipe
 * 
 */
public interface Consulta<DTO> {

	/**
	 * M�todo utilizado para pesquisar algum item especifico
	 * 
	 * @return O item solicitado pelo crit�rio de pesquisa.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a pesquisa.
	 */
	public DTO procurar(DTO dto) throws GrowUpException;

	/**
	 * M�todo utilizado para pesquisar todos os registros
	 * 
	 * @return Uma lista de todos os registros que atendem ao crit�rio de
	 *         pesquisa.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a pesquisa.
	 */
//	public List<?> buscarTodas() throws GrowUpException;
	public List<DTO> buscarTodas() throws GrowUpException;

	/**
	 * M�todo utilizado para pesquisar todos os registros
	 * 
	 * @param indiceInicial
	 *            Buscar� a quantidade de registros retornados a partir do
	 *            indice inicial
	 * @return Uma lista de todos os registros que atendem ao crit�rio de
	 *         pesquisa.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a pesquisa.
	 */
//	public List<?> buscarTodas(final Integer indiceInicial) throws GrowUpException;
	public List<DTO> buscarTodas(final Integer indiceInicial) throws GrowUpException;

	/**
	 * M�todo utilizado para pesquisar todos os registros que contenham as
	 * informa��es extraidas do dto.
	 * 
	 * @param dto
	 *            Transfer object que cont�m as informa��es de pesquisa.
	 * @return Uma lista de todos os registros que atendem ao crit�rio de
	 *         pesquisa.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a pesquisa.
	 * 
	 */
//	public List<?> buscarTodas(final DTO dto) throws GrowUpException;
	public List<DTO> buscarTodas(final DTO dto) throws GrowUpException;
	
	/**
	 * Nova implementacao recebendo a lista 
	 * @param dto
	 * @param lista
	 * @return
	 * @throws GrowUpException
	 */
	public <OUT> List<OUT> buscarTodas(final DTO dto, List<OUT> lista) throws GrowUpException;

	/**
	 * M�todo utilizado para pesquisar todos os registros que contenham as
	 * informa��es extraidas do dto.
	 * 
	 * @param dto
	 *            Transfer object que cont�m as informa��es de pesquisa.
	 * @param indiceInicial
	 *            Buscar� a quantidade de registros retornados a partir do
	 *            indice inicial
	 * @param <DTO>
	 *            M�todo parametrizado com algum dto
	 * @return Uma lista de todos os registros que atendem ao crit�rio de
	 *         pesquisa.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a pesquisa.
	 * 
	 * PS: Este m�todo n�o � final porque precisa ser sobreescrito nas classes
	 * de grupo
	 */
//	public List<?> buscarTodas(final DTO dto, final Integer indiceInicial) throws GrowUpException;
	public List<DTO> buscarTodas(final DTO dto, final Integer indiceInicial) throws GrowUpException;

	// Interface para o BuscarTotal

	/**
	 * M�todo que retorna a quantidade de registros baseado no dto
	 * 
	 * @param <DTO>
	 *            DTO parametrizado de qualquer tipo
	 * @param dto
	 *            Parametro do tipo DTO
	 * @return Retorna um primitivo do tipo long1
	 * @throws GrowUpException
	 *             Caso ocorra algum erro na opera��o.
	 */
	public Long buscarTotal(final DTO dto) throws GrowUpException;

	/**
	 * M�todo que retorna a quantidade de registros de acordo com objeto de
	 * negocio
	 * 
	 * @return Retorna um primitivo do tipo long
	 * @throws GrowUpException
	 *             Caso ocorra algum erro na opera��o.
	 */
	public Long buscarTotal() throws GrowUpException;

}
