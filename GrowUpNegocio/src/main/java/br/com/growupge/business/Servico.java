package br.com.growupge.business;

import static br.com.growupge.util.MensagemBuilder.build;
import static br.com.growupge.utility.Reflection.isCodigoValido;
import static org.apache.commons.lang3.StringUtils.isBlank;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.dto.ServicoDTO;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.stereotype.FirebirdStereoType;

/**
 * Business object para Servi�o
 * 
 * @author Felipe
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Servico extends NegocioBase<ServicoDTO> {

	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Servico() {}
	
	/**
	 * Construtor para esta classe, passando a sessao
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com os dados de Servi�o
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a atribui��o de valores.
	 */
	public Servico(final SessaoDTO dto) throws GrowUpException {
		this();
		this.setSessaodto(dto);
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getServicoDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}
	
	/**
	 * M�todo utilizado para verificar se o nome da Modelo existe.
	 * 
	 * @throws GrowUpException
	 *             Caso o nome da Modelo n�o exista.
	 */
	private /*final*/ void verificarNome(ServicoDTO dto) throws GrowUpException {

		// RN3
		ServicoDTO ldto = new ServicoDTO();
		ldto.setNome(dto.getNome());
		ldto = this.procurar(dto);

		// Nome existe
		if (ldto != null) {

			// Duplica��o ao inserir
			if (!isCodigoValido( dto ) || ldto.getCodigo() != dto.getCodigo()) {
				throw new GrowUpException(MSGCODE.SERVICO_EXISTENTE, build( ldto.getNome() ) );
			}
		}

	}

	/**
	 * Verificar se os dados da Modelo informada est�o cadastrados na base.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia que cont�m as informa��es da Servi�o
	 * @throws GrowUpException
	 *             Caso ocorra o dado n�o seja encontrado na base.
	 */
	private /*final*/ void verificarUso(final ServicoDTO dto) throws GrowUpException {
		// RN1
		PedidoVeiculoDTO ldto = new PedidoVeiculoDTO();
		ldto.setServico(dto);

		if (getPedidoBO().procurar(ldto) != null) {
			throw new GrowUpException(MSGCODE.SERVICO_EM_USO, build( dto.getNome() ));
		}
	}

	/**
	 * M�todo sobrescrito.
	 * 
	 * @see br.com.growupge.business.NegocioBase#validarCamposObrigatorios(java.lang.Object)
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final ServicoDTO dto) throws GrowUpException {

		// RN2
		if (isBlank(dto.getNome())) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}
	}

	/**
	 * Cria uma nova Modelo com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Servi�o
	 * @return Um objeto de transfer�ncia com os dados da Modelo criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de cria��o.
	 */
	public /*final*/ ServicoDTO criarServico(final ServicoDTO dto) throws GrowUpException {

		ServicoDTO retDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {
			// Se os valores forem v�lidos, atribui a classe.
			this.validarCamposObrigatorios(dto);

			// RN3
			this.verificarNome(dto);

			DAO<ServicoDTO> dao = daoFactory.getServicoDAO();
			retDTO = dao.insert(daoFactory.getConnection(), dto);

			retDTO = build( MSGCODE.SERVICO_INCLUIDO_COM_SUCESSO, 	
							retDTO, 
							retDTO.getNome()); 

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {
			daoFactory.closeCommit();
		}
		return retDTO;
	}

	/**
	 * Alterar a Modelo com os dados informados no objeto de transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Servi�o
	 * @return Um objeto de transfer�ncia com os dados da Modelo alterada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de altera��o.
	 */
	public /*final*/ ServicoDTO alterarServico(final ServicoDTO dto) throws GrowUpException {

		ServicoDTO retDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {

			// RN2
			this.validarCamposObrigatorios(dto);

			//RN4
			this.verificarExclusaoPrevia(dto);
			
			// RN3
			this.verificarNome(dto);

			DAO<ServicoDTO> dao = daoFactory.getServicoDAO();
			retDTO = dao.update(daoFactory.getConnection(), dto);

			retDTO = build( MSGCODE.SERVICO_ALTERADO_COM_SUCESSO, 	
					retDTO, 
					retDTO.getNome()); 

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {
			daoFactory.closeCommit();
		}

		return retDTO;
	}

	/**
	 * Exclui a Modelo que cont�m os dados informados no objeto de
	 * transfer�ncia.
	 * 
	 * @param dto
	 *            Objeto de transfer�ncia com as informa��es da Servi�o
	 * @return Um objeto de transfer�ncia com os dados da Modelo criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a opera��o de exclus�o.
	 */
	public /*final*/ ServicoDTO excluirServico(final ServicoDTO dto) throws GrowUpException {

		ServicoDTO retDTO = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();

		try {
			//RN4
			this.verificarExclusaoPrevia(dto);
			
			// RN1
			this.verificarUso(dto);

			DAO<ServicoDTO> dao = daoFactory.getServicoDAO();
			retDTO = dao.delete(daoFactory.getConnection(), dto);

			retDTO = build( MSGCODE.SERVICO_REMOVIDO_COM_SUCESSO, 	
					retDTO, 
					retDTO.getNome()); 
		
		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {
			daoFactory.closeCommit();
		}

		return retDTO;
	}

	/**
	 * M�todo sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public ServicoDTO verificarExclusaoPrevia(ServicoDTO dto) throws GrowUpException {
		ServicoDTO dtoAntes = null;

		dtoAntes = this.procurar(dto);

		if (dtoAntes == null) {
			throw new GrowUpException(
					MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,
					build (dto.getNome()));
		}

		return dtoAntes;
	}

}
