package br.com.growupge.business;

import static br.com.growupge.util.MensagemBuilder.build;

import java.util.Date;
import java.util.List;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.MensagemDTO;
import br.com.growupge.dto.SessaoDTO;
import br.com.growupge.dto.StatusPedido;
import br.com.growupge.dto.TramitacaoDTO;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.factory.DAOFactory;
import br.com.growupge.interfacesdao.DAO;
import br.com.growupge.stereotype.FirebirdStereoType;

/**
 * Business object para Tramitacao
 * 
 * @author Felipe
 * 
 */
@javax.enterprise.context.RequestScoped
@javax.inject.Named
public class Tramitacao extends NegocioBase<TramitacaoDTO> {


	/**
	 * Construtor default para uso do BeanManager
	 * 
	 */
	protected Tramitacao() throws GrowUpException {}

	/**
	 * Construtor copara esta classe.
	 * 
	 * @param dto
	 *            Objeto de transferência com os dados de Tramitacao
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a atribuição de valores.
	 */
	public Tramitacao(final SessaoDTO dto) throws GrowUpException {
		this();
		this.setSessaodto(dto);
	}
	
	/* (non-Javadoc)
	 * @see br.com.growupge.business.ConsultaBase#setDaoFactory(br.com.growupge.factory.DAOFactory)
	 */
	@Override
	@javax.inject.Inject
	protected void setDaoFactory(@FirebirdStereoType DAOFactory daofactory) {
		super.daoFactory = daofactory;
		super.crudDAO = daoFactory.getTramitacaoDAO();
		//super.crudDAO.setSessao(getSessaodto());;
	}

	/**
	 * Método sobrescrito.
	 * 
	 * @see br.com.growupge.business.NegocioBase#validarCamposObrigatorios(java.lang.Object)
	 */
	@Override
	protected /*final*/ void validarCamposObrigatorios(final TramitacaoDTO dto) throws GrowUpException {

		// RN2
		if (dto == null || dto.getObs() == null || dto.getObs().trim().length() == 0) {
			throw new GrowUpException(MSGCODE.CAMPOS_OBRIGATORIOS);
		}

		if (dto.getPedido() == null || dto.getPedido().getCodigo() == 0) {
			throw new GrowUpException(MSGCODE.TRAMITACAO_CRIADA_TELA_PEDIDO);
		}

	}

	/**
	 * Cria uma nova Modelo com os dados informados no objeto de transferência.
	 * 
	 * @param dto
	 *            Objeto de transferência com as informações da Tramitacao
	 * @return Um objeto de transferência com os dados da Modelo criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a operação de criação.
	 */
	public /*final*/ TramitacaoDTO criarTramitacao(final TramitacaoDTO dto, DAOFactory daofactory) throws GrowUpException {
		TramitacaoDTO retDTO = null;

		// Se os valores forem válidos, atribui a classe.
		this.validarCamposObrigatorios(dto);

		dto.setIsPendente(Constantes.SIM);
		
		DAO<TramitacaoDTO> dao = daoFactory.getTramitacaoDAO();
		retDTO = dao.insert(daoFactory.getConnection(), dto);

		// Verificando status do pedido
//		StatusPedido sts = StatusPedido.getEnum(retDTO.getPedido().getStatus());
		StatusPedido sts = retDTO.getPedido().getStatus();
		switch (sts) {
		case ENVIADO:
			throw new GrowUpException(
					MSGCODE.PEDIDO_JA_CONCLUIDO,
					build(retDTO.getPedido().getCodigo()));

		case CONCLUIDO:
//			retDTO.getPedido().setStatus(StatusPedido.FATURADO.toString()); // Caso o pedido esteja concluido
			sts.pagar( retDTO.getPedido() );
			retDTO.setPedido(	getPedidoBO().alterarPedidoVeiculo(retDTO.getPedido())  );
		default:
			break;
		}

		retDTO  = build(MSGCODE.TRAMITACAO_INCLUIDO_COM_SUCESSO,
						retDTO, retDTO.getCodigo()); 
		
		return retDTO;
	}
	
	/**
	 * Cria uma nova Modelo com os dados informados no objeto de transferência.
	 * 
	 * @param dto
	 *            Objeto de transferência com as informações da Tramitacao
	 * @return Um objeto de transferência com os dados da Modelo criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a operação de criação.
	 */
	public /*final*/ TramitacaoDTO criarTramitacao(final TramitacaoDTO dto) throws GrowUpException {

		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		
		try {
			return this.criarTramitacao(dto, daoFactory);

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {

			try {
				daoFactory.closeCommit();

			} catch (Exception e) {
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}
	}

	/**
	 * Alterar a pendencia com os dados informados no objeto de transferência.
	 * 
	 * @param dto
	 *            Objeto de transferência com as informações da Tramitacao
	 * @return Um objeto de transferência com os dados da Modelo alterada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a operação de alteração.
	 */
	public /*final*/ TramitacaoDTO alterarTramitacao(final TramitacaoDTO dto) throws GrowUpException {

		TramitacaoDTO retDTO = null;

		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		try {

			// RN2
			this.validarCamposObrigatorios(dto);

			//RN4
			this.verificarExclusaoPrevia(dto);

			dto.setUsuarioAlteracao(this.getSessao().getUsuario());
			
			DAO<TramitacaoDTO> dao = daoFactory.getTramitacaoDAO();
			retDTO = dao.update(daoFactory.getConnection(), dto);

			retDTO = build(MSGCODE.TRAMITACAO_ALTERADO_COM_SUCESSO,
							retDTO, retDTO.getCodigo());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());
		} finally {

			try {

				daoFactory.closeCommit();

			} catch (Exception e) {
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}

		return retDTO;
	}

	/**
	 * Conclui a pendencia com os dados informados no objeto de transferência.
	 * 
	 * @param dto
	 *            Objeto de transferência com as informações da Tramitacao
	 * @return Um objeto de transferência com os dados da Modelo alterada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a operação de alteração.
	 */
	public /*final*/ TramitacaoDTO concluirTramitacao(final TramitacaoDTO dto) throws GrowUpException {

		TramitacaoDTO retDTO = null;
//		TramitacaoDTO dtoAntes = null;
		//DAOFactory daofactory =  daoFactory.getDAOFactory();
		boolean isCriarNova = dto.isPendente();
		try {

			// RN2
			this.validarCamposObrigatorios(dto);

			PedidoVeiculo boPedido = getPedidoBO();
			
//			dtoAntes = this.procurar(dto);
			dto.setIsPendente(Constantes.NAO);
			dto.setUsuarioAlteracao(this.getSessao().getUsuario());
			// Buscando pedido para avaliar a possibilidade de altera-lo, para isso, preciso dos dados completos.
			dto.setPedido(boPedido.procurar(dto.getPedido()));

			if (dto.getPedido().getStatus().isEnviado() )
				throw new GrowUpException(MSGCODE.PEDIDO_JA_CONCLUIDO, build(dto.getPedido().getCodigo()));
			
			DAO<TramitacaoDTO> dao = daoFactory.getTramitacaoDAO();
			retDTO = dao.update(daoFactory.getConnection(), dto);

			retDTO = build(MSGCODE.TRAMITACAO_CONCLUIDA_COM_SUCESSO,
							retDTO, retDTO.getCodigo());
			

			//Significa criar uma próxima?
			if (isCriarNova) {

				TramitacaoDTO novoDTO = new TramitacaoDTO();
				novoDTO.setPedido(retDTO.getPedido());
				
				long qtdPendencias = buscarTotal(novoDTO); 
				
				novoDTO.setIsPendente(Constantes.SIM);
				novoDTO.setUsuarioCadastro(retDTO.getUsuarioAlteracao());
				novoDTO.setDataCadastro(new Date());
				
				String idioma =  getSessao().getUsuario().getIdioma();
				Mensagem boMsg = getMensagemBO();
				MensagemDTO msg = null;
				
				// Se somente existe a primeira pendencia suportamente Aguardando Pgto, trocar para pendente de conclusao.
				// E o pedido como faturado.
				if (qtdPendencias == 1) {
					msg = boMsg.procurar(new MensagemDTO(MSGCODE.TRAMITACAO_PENDENTE_CONCLUSAO));
					novoDTO.setObs(msg.getDescricao(idioma));
					
//					retDTO.getPedido().setStatus(StatusPedido.FATURADO.toString());
					retDTO.getPedido().getStatus().pagar( retDTO.getPedido() );
					boPedido.alterarPedidoVeiculo(retDTO.getPedido());
				} else {
					msg = boMsg.procurar(new MensagemDTO(MSGCODE.TRAMITACAO_AUTOMATICA));
					novoDTO.setObs(msg.getDescricao(idioma));
				}

				return this.criarTramitacao(novoDTO);
			} else { 
				//verifica se nao existem mais pendencias, caso contrario concluir o pedido.
				TramitacaoDTO paramDTO = new TramitacaoDTO();
				paramDTO.setPedido(retDTO.getPedido());
				paramDTO.setIsPendente(Constantes.SIM);

				List<TramitacaoDTO> pendencias = this.buscarTodas(paramDTO);
				if (pendencias.isEmpty()) {
//					retDTO.getPedido().setStatus(StatusPedido.CONCLUIDO.toString());
					retDTO.getPedido().getStatus().concluir( retDTO.getPedido() );
					boPedido.alterarPedidoVeiculo(retDTO.getPedido());
				}
			}

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e.getMessage());

		} finally {

			try {
				daoFactory.closeCommit();
			} catch (Exception e) {
				throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
			}
		}

		return retDTO;
	}

	/**
	 * Exclui a Modelo que contém os dados informados no objeto de
	 * transferência.
	 * 
	 * @param dto
	 *            Objeto de transferência com as informações da Tramitacao
	 * @return Um objeto de transferência com os dados da Modelo criada.
	 * @throws GrowUpException
	 *             Caso ocorra algum erro durante a operação de exclusão.
	 */
	public /*final*/ TramitacaoDTO excluirTramitacao(final TramitacaoDTO dto) throws GrowUpException {

		TramitacaoDTO retDTO = null;
		// DAOFactory daofactory = daoFactory.getDAOFactory() ;
		try {

			//RN4
			this.verificarExclusaoPrevia(dto);

			DAO<TramitacaoDTO> dao = daoFactory.getTramitacaoDAO();
			retDTO = dao.delete(daoFactory.getConnection(), dto);

			retDTO = build(MSGCODE.TRAMITACAO_REMOVIDA_COM_SUCESSO, retDTO, retDTO.getCodigo());

		} catch (GrowUpException e) {
			daoFactory.rollBack();
			throw e;
		} catch (Exception e) {
			daoFactory.rollBack();
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
		} finally {
			daoFactory.closeCommit();
		}

		return retDTO;
	}

	/**
	 * Método sobrescrito.
	 * @see br.com.growupge.business.NegocioBase#verificarExclusaoPrevia(java.lang.Object)
	 */
	@Override
	public TramitacaoDTO verificarExclusaoPrevia(TramitacaoDTO dto) throws GrowUpException {
		TramitacaoDTO dtoAntes = null;

		dtoAntes = this.procurar(dto);

		if (dtoAntes == null) {
			throw new GrowUpException(
					MSGCODE.REGISTRO_EXCLUIDO_POR_OUTRO_USUARIO,
					build(dto.getCodigo()));
		}

		return dtoAntes;
	}

}
