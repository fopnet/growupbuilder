/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 07/02/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 07/02/2007 - In�cio de tudo, por Felipe
 * 19/04/2007 - Modificado de Interface para classe abstrata, para implementa��o de rotinas de leitura de template. Por JCV
 *
 */
package br.com.growupge.template;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.exception.ErroInterno;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.global.GlobalStartup;
import br.com.growupge.utility.StringUtil;

/**
 * Classe Template que poder� ser de qualquer tipo
 * 
 * @author Felipe
 * 
 * @param <T>
 */
public abstract class HtmlTemplate {

	public static final String PARAM_USR = "{".concat(Constantes.PARAM_USUARIO.toUpperCase()).concat("}");
	public static final String PARAM_PWD = "{PWD}";
	public static final String PARAM_LINK = "{LINK}";
	public static final String PARAM_LINK_PEDIDO_USR = "{LINK_PEDIDO_USR}";
	public static final String PARAM_PERFIL = "{PERFIL}";
	public static final String PARAM_PLACA = "{PLACA}";
	public static final String PARAM_PRAZO = "{PRAZO}";
	public static final String PARAM_NIP = "{NIP}";
	public static final String PARAM_VALOR_UNITARIO = "{VALOR_UNITARIO}";
	public static final String PARAM_ASSUNTO = "{SUBJECT}";
	public static final String PARAM_EMPRESA = "{COMPANY}";
	public static final String PARAM_EMAIL = "{EMAIL}";
	public static final String PARAM_MSG = "{MESSAGE}";
	public static final String PARAM_CONTENT = "{CONTENT}";
	
	//Relatorio
	public static final String PARAM_PEDIDO = "{PEDIDO}";
	public static final String PARAM_PASTA = "{PASTA}";

	private StringBuffer htmlTemplate = null;

	private Map<String, Object> parametros = null;

	private String pathTemplate = null;

	public HtmlTemplate(String pathTemplate) throws GrowUpException {
		htmlTemplate = new StringBuffer();
		parametros = new HashMap<String, Object>();
		this.pathTemplate = pathTemplate;
	}

	public void adicionarParametro(String chave, Object conteudo) {
		this.parametros.put(chave, conteudo);
	}

	public String getHtmlTemplate() {
		return htmlTemplate.toString();
	}
	
	/**
	 * @return the parametros
	 */
	protected Map<String, Object> getParametros() {
		return parametros;
	}

	/**
	 * M�todo responsavel pela leitura do arquivo de template
	 * 
	 * @throws SigemException
	 *             Comentar aqui.
	 */
	public void execute() throws GrowUpException {
		BufferedReader inFile;
		// FileReader fr= null;
		InputStreamReader reader = null;

		// Executa a leitura do arquivo de template
		try {
			InputStream stream = GlobalStartup.getInstance().getResourceFile(this.pathTemplate);
			reader = new InputStreamReader(stream);
			// fr = new FileReader(this.pathTemplate);
		} catch (FileNotFoundException e1) {
			throw new GrowUpException(MSGCODE.ARQUIVO_TEMPLATE_DE_EMAIL_NAO_ENCONTRADO);
		} catch (IOException e1) {
			throw new GrowUpException(MSGCODE.ARQUIVO_TEMPLATE_DE_EMAIL_NAO_ENCONTRADO);
		} catch (Exception e1) {
			throw new GrowUpException(MSGCODE.ARQUIVO_TEMPLATE_DE_EMAIL_NAO_ENCONTRADO);
		}

		inFile = new BufferedReader(reader);

		try {
			String linha = null;

			while ((linha = inFile.readLine()) != null) {
				// Le a linha do template
				linha = linha.trim();

				// Executa a troca de parametros
				Set<String> chaves = parametros.keySet();
				Iterator<String> itChaves = chaves.iterator();

				while (itChaves.hasNext()) {
					String chave = itChaves.next();
					String conteudo = parametros.get(chave).toString();
					String chaveEscape = "\\".concat(chave).replace("}", "\\}");
					String regex = "(?mi)(".concat(chaveEscape).concat(")");
//					linha = StringUtil.replaceString(linha, chave, conteudo);
					linha = StringUtil.replaceRegex(linha, regex, conteudo);
				}

				htmlTemplate.append(linha);
				htmlTemplate.append("\n");

			} // while

		} catch (NullPointerException e) {
			
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
		} catch (IOException e) {
			
			throw new GrowUpException(ErroInterno.ERRO_DESCONHECIDO, e);
		}
	}
}
