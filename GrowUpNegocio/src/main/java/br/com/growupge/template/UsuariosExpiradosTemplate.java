/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 07/02/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 07/02/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.template;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.utility.DateUtil;

/**
 * @author Felipe
 * 
 */
public class UsuariosExpiradosTemplate extends HtmlTemplate {

	private StringBuilder lines;

	public UsuariosExpiradosTemplate() throws GrowUpException {
		super(Constantes.VARIAVEL_TMPLT_USUARIOS_EXPIRADOS);
		lines = new StringBuilder();
	}
	
	public void adicionarPedidoExpirado(PedidoVeiculoDTO dto, Long periodo) {
		lines.append("<tr>");
		
		lines.append("<td>")
				.append(dto.getCodigo())
			  .append("</td>");
		
		lines.append("<td>")
			.append(dto.getUsuarioCadastro().getCodigo())
			.append("</td>");

		lines.append("<td>")
		.append( DateUtil.getDateManagerInstance( dto.getDataPagamento() ) .getLongDate())
		.append("</td>");

		lines.append("<td>")
		.append( periodo.toString() )
		.append(" hr")
		.append("</td>");
		
	}

	/* (non-Javadoc)
	 * @see br.com.growupge.template.HtmlTemplate#execute()
	 */
	@Override
	public void execute() throws GrowUpException {
		adicionarParametro(PARAM_CONTENT, lines.toString());
		super.execute();
	}
	
	

}
