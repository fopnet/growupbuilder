/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : Apr 19, 2007
 *
 * Historico de Modifica��o:
 * =========================
 * Apr 19, 2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.template;

import br.com.growupge.exception.GrowUpException;

public class EmailTemplateTrocaSenha extends HtmlTemplate {

	public EmailTemplateTrocaSenha(String pathTemplate) throws GrowUpException {
		super(pathTemplate);
	}

}
