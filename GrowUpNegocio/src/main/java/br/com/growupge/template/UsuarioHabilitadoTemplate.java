/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 07/02/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 07/02/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.template;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.exception.GrowUpException;

/**
 * @author Felipe
 * 
 */
public class UsuarioHabilitadoTemplate extends HtmlTemplate {

	public UsuarioHabilitadoTemplate() throws GrowUpException {
		super(Constantes.VARIAVEL_TMPLT_USUARIO_HABILITADO);
	}

}
