/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : Apr 19, 2007
 *
 * Historico de Modifica��o:
 * =========================
 * Apr 19, 2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.template;


import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.io.FileUtils;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.constantes.MSGCODE;
import br.com.growupge.dto.PedidoVeiculoDTO;
import br.com.growupge.exception.GrowUpException;
import br.com.growupge.util.FileUtil;

public class RelatorioPedidoTemplate extends HtmlTemplate {

	public RelatorioPedidoTemplate() throws GrowUpException {
		super(Constantes.VARIAVEL_TMPLT_RESP_PEDIDO);
	}
	
	private File getPasta() {
		return (File) super.getParametros().get(PARAM_PASTA);
	}

	private PedidoVeiculoDTO getPedido() {
		return (PedidoVeiculoDTO) super.getParametros().get(PARAM_PEDIDO);
	}

	@Override
	public void execute() throws GrowUpException {
		super.execute();
		
		if (!getPasta().exists())
			getPasta().mkdir();	
		
	    JasperPrint jasperPrint = null;
    	Map<String, Object> jrParams = new HashMap<String, Object>();
    	jrParams.put("SUBREPORT_DIR", "reports");
    	jrParams.put("WEBAPP_DIR", FileUtil.getWebAppPath());
    	
    	try {
    		
	    	InputStream inputStream = getClass().getClassLoader().getResourceAsStream("reports/relatorioPedido.jasper");
	    	JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(Arrays.asList(getPedido()));
//	    	Caso ocorra erro de serialUID na proxima linha � porque foi compilado em outra versao do java, j� resolvida no maven 
			jasperPrint = JasperFillManager.fillReport(inputStream, jrParams, dataSource);
			JasperExportManager.exportReportToPdfFile(	jasperPrint, 
														FileUtils.getFile(getPasta(), "relatorio.pdf").getPath());

    	} catch (JRException e) {
    		throw new GrowUpException(MSGCODE.ERRO_GERAR_RELATORIO_PDF, 
    								e.getMessage(), 
    								e.getMessage(), 
    								e.getMessage());
		}
	}
	

}
