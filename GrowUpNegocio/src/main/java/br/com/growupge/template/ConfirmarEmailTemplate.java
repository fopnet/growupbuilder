/*
 * Diretos Reservados (c) Grow Up Gest�o Empresarial
 *
 * Este software � confidencial e a informa��o propriet�ria da
 * Grow Up Gest�o Empresarial.
 *
 * Projeto    : Site da empresa
 * Vers�o     : 4.0
 * Cliente    : Grow Up Gest�o Empresarial.
 * Fornecedor : Grow Up Gest�o Empresarial
 * Natureza: Aplicativo e site
 * Tecnologia : Java
 * Criado em  : 07/02/2007
 *
 * Historico de Modifica��o:
 * =========================
 * 07/02/2007 - In�cio de tudo, por Felipe
 *
 */
package br.com.growupge.template;

import br.com.growupge.constantes.Constantes;
import br.com.growupge.exception.GrowUpException;

/**
 * Classe de template de email O primeiro par�metro do tipo String � o retorno
 * do m�todo getTemplate que seria template html com o conteudo. O segundo
 * par�metro do tipo String � o conteudo do texto html.
 * 
 * @author Felipe
 * 
 */
public class ConfirmarEmailTemplate extends HtmlTemplate {

	public ConfirmarEmailTemplate() throws GrowUpException {
		super(Constantes.VARIAVEL_TMPLT_CONFIRMAR_EMAIL);
	}

}
