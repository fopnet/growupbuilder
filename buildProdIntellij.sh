export MAVEN_OPTS="-Xms512m -Xmx512m -XX:PermSize=512m -XX:MaxPermSize=1024m"

mvn install -DskipTests -P prod